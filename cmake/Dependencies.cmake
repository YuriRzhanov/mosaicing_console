# This list is required for static linking

set(Mosaicking_DEPENDENCY_LIBS "")
set(Mosaicking_EXTERNAL_DEPENDENCIES "")

# ---[ OpenCV
if(USE_OPENCV)
  # OpenCV 3
  find_package(OpenCV QUIET COMPONENTS core highgui imgproc imgcodecs)
  if(NOT OpenCV_FOUND)
    # OpenCV 2
    find_package(OpenCV QUIET COMPONENTS core highgui imgproc)
  endif()
  if(OpenCV_FOUND)
    include_directories(SYSTEM ${OpenCV_INCLUDE_DIRS})
    list(APPEND Mosaicking_DEPENDENCY_LIBS ${OpenCV_LIBS})
    message(STATUS "OpenCV found (${OpenCV_CONFIG_PATH})")
  else()
    message(WARNING "Not compiling with OpenCV. Suppress this warning with -DUSE_OPENCV=OFF")
    set(USE_OPENCV OFF)
  endif()
endif()

# ---[ FFMPEG
if(USE_FFMPEG)
  find_package(FFmpeg REQUIRED)
  if (FFMPEG_FOUND)
    message("Found FFMPEG/LibAV libraries")
    include_directories(SYSTEM ${FFMPEG_INCLUDE_DIR})
    list(APPEND Mosaicking_DEPENDENCY_LIBS ${FFMPEG_LIBRARIES})
  else ()
    message("Not compiling with FFmpeg. Suppress this warning with -DUSE_FFMPEG=OFF")
    set(USE_FFMPEG OFF)
  endif ()
endif()

# ---[ EIGEN
if(USE_EIGEN)
	#add_definitions(-DEIGEN_MPL2_ONLY)
	find_package(Eigen3 QUIET)
	if(EIGEN3_FOUND)
	  include_directories(SYSTEM ${EIGEN3_INCLUDE_DIRS})
	else()
    message("Not compiling with Eigen. Suppress this warning with -DUSE_EIGEN=OFF")
    set(USE_EIGEN OFF)
	endif()
endif()
