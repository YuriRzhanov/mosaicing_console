#include "stdafx.h"

void AddFrameToMosaic(BMPImageC* mosaic, BMPImageC* img, Hmgr& hm, Coord2& start)
{
	Coord2 c, tc;
		c.SetImage(img);
		tc.SetImage(img);
	Coord2 C;
	Y_Rect<int> mos_r = mosaic->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	int vld;
	Y_RGB<double> V;
	Coord2 pStart(int(start.X()+0.5), int(start.Y()+0.5));
	Point2<int> posM;
	/*{
		// Where does pixel (0,0) go in the mosaic space?
		c.Set(0,0);
		tc = hm.Map(c);
		C = pStart + tc;
		TRACE("Pixel (0,0) goes to (%lf,%lf)\n", C.X(), C.Y());
	}*/
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> im_r = img->GetRect();
	Y_Rect<int> r = hm.FrameRect(im_r, img->focal_length);

	for(int j = r.y-1; j<r.y+r.h+1; j++) {
		for(int i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Position in mosaic:
			C = pStart + c;
			if(C.Outside(mos_r)) continue; // Trying to fill outside mosaic bounds
			posM.Set(int(C.X()+0.5), int(C.Y()+0.5));
			// Fill from whatever pixel is valid.
			tc = inv_hm.Map(c);
			V = tc.Value2(img, &vld);
			if(vld)
				mosaic->set_pixel(posM, V);
		}
	}
}

// Save as AddFrameToMosaic(), but the area is filled with colour representing frame number
void AddPatch(BMPImageC* patches_image, int frame_n, BMPImageC* img, Hmgr& hm, Coord2& start, Y_RGB<u_char> bg)
{
	Coord2 c, tc;
		c.SetImage(img);
		tc.SetImage(img);
	Coord2 C;
	Y_Rect<int> mos_r = patches_image->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	int vld;
	int rc = frame_n/100;
	int gc = (frame_n-100*rc)/10; 
	int bc = (frame_n-100*rc-10*gc);
	Y_RGB<u_char> V(rc,gc,bc);
	Coord2 pStart(int(start.X()+0.5), int(start.Y()+0.5));
	Point2<int> posM;
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> im_r = img->GetRect();
	Y_Rect<int> r = hm.FrameRect(im_r, img->focal_length);

	for(int j = r.y-1; j<r.y+r.h+1; j++) {
		for(int i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Position in mosaic:
			C = pStart + c;
			if(C.Outside(mos_r)) continue; // Trying to fill outside mosaic bounds
			posM.Set(int(C.X()+0.5), int(C.Y()+0.5));
			// Fill from whatever pixel is valid.
			tc = inv_hm.Map(c);
			tc.Value2(img, &vld);
			if(vld)
				patches_image->set_pixel(posM, V);
		}
	}
}

// 'cut' comes classified in two categories: GC_FRAME and GC_MOSAIC.
// The latter stay unchanged. The former are checked for coming from within a frame,
// and if yes, are replaced with those.
void AddFrameToMosaicWithCut(BMPImageC* mosaic, BMPImageC* img, Hmgr& hm, Coord2& start,
	BMPImageG* cut, Point2<int>& top_left)
{
	Coord2 c, tc;
		c.SetImage(img);
		tc.SetImage(img);
	Coord2 C;
	Y_Rect<int> mos_r = mosaic->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	int vld;
	Y_RGB<double> V;
	Coord2 pStart(int(start.X()+0.5), int(start.Y()+0.5));
	Point2<int> posM;
	/*{
		// Where does pixel (0,0) go in the mosaic space?
		c.Set(0,0);
		tc = hm.Map(c);
		C = pStart + tc;
		TRACE("Pixel (0,0) goes to (%lf,%lf)\n", C.X(), C.Y());
	}*/
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> im_r = img->GetRect();
	Y_Rect<int> r = hm.FrameRect(im_r, img->focal_length);

	for(int j = r.y-1; j<r.y+r.h+1; j++) {
		for(int i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Position in mosaic:
			C = pStart + c;
			if(C.Outside(mos_r)) continue; // Trying to fill outside mosaic bounds
			posM.Set(int(C.X()+0.5), int(C.Y()+0.5));
			int xx = posM.X()-top_left.X();
			int yy = posM.Y()-top_left.Y();
			// If point is inside the cut, use labeling to fill in mosaic
			if(xx>=1 && xx<cut->w-1 && yy>=1 && yy<cut->h-1) {
				if(cut->get_pixel(xx, yy)==GC_FRAME) { // Fill from frame, if comes from within frame boundaries
					tc = inv_hm.Map(c);
					V = tc.Value2(img, &vld);
					if(vld)
						mosaic->set_pixel(posM, V);
				}
			} else { // Outside the cut. Fill only if mosaic pixel has not been set.
				if(!mosaic->valid(posM)) {
					tc = inv_hm.Map(c);
					V = tc.Value2(img, &vld);
					if(vld)
						mosaic->set_pixel(posM, V);
				}
			}
		}
	}
}

// Same as above, but pixels are altered only within the bounding box specified by 'top_left'
// and size of 'cut'. This is the only part that is guaranteed not to overlap with other
// graph cuts.
void AddFrameToMosaicWithCut_OnlyBox(BMPImageC* mosaic, BMPImageC* img, Hmgr& hm, Coord2& start,
	BMPImageG* cut, Point2<int>& top_left)
{
	Coord2 c, tc;
		c.SetImage(img);
		tc.SetImage(img);
	Coord2 C;
	Y_Rect<int> mos_r = mosaic->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	int vld;
	Y_RGB<double> V;
	Coord2 pStart(int(start.X()+0.5), int(start.Y()+0.5));
	Point2<int> posM;
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> im_r = img->GetRect();
	Y_Rect<int> r(top_left.x, top_left.y, cut->w, cut->h);

	for(int j = r.y; j<(r.y+r.h); j++) {
		for(int i = r.x; i<(r.x+r.w); i++) {
			c.Set(i, j);
			// Position in mosaic:
			C = pStart + c;
			if(C.Outside(mos_r)) continue; // Trying to fill outside mosaic bounds
			posM.Set(int(C.X()+0.5), int(C.Y()+0.5));
			int xx = posM.X()-top_left.X();
			int yy = posM.Y()-top_left.Y();
			// If point is inside the cut, use labeling to fill in mosaic
			if(xx>=1 && xx<cut->w-1 && yy>=1 && yy<cut->h-1) {
				if(cut->get_pixel(xx, yy)==0) { // Fill from frame
					tc = inv_hm.Map(c);
					V = tc.Value2(img, &vld);
					if(vld)
						mosaic->set_pixel(posM, V);
				}
			} else { // Outside the cut. Fill only if mosaic pixel has not been set.
				if(!mosaic->valid(posM)) {
					tc = inv_hm.Map(c);
					V = tc.Value2(img, &vld);
					if(vld)
						mosaic->set_pixel(posM, V);
				}
			}
		}
	}
}

// Same as AddFrameToMosaicWithCut(), but the area is filled with colour representing frame number
void AddPatchWithCut(BMPImageC* patches_image, int frame_n, BMPImageC* img, Hmgr& hm, Coord2& start,
	BMPImageG* cut, Point2<int>& top_left, Y_RGB<u_char> bg)
{
	Coord2 c, tc;
		c.SetImage(img);
		tc.SetImage(img);
	Coord2 C;
	Y_Rect<int> mos_r = patches_image->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	int vld;
	int rc = frame_n/100;
	int gc = (frame_n-100*rc)/10; 
	int bc = (frame_n-100*rc-10*gc);
	Y_RGB<u_char> V(rc,gc,bc), VP;
	Coord2 pStart(int(start.X()+0.5), int(start.Y()+0.5));
	Point2<int> posM;
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> im_r = img->GetRect();
	Y_Rect<int> r = hm.FrameRect(im_r, img->focal_length);

	for(int j = r.y-1; j<r.y+r.h+1; j++) {
		for(int i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Position in patches image:
			C = pStart + c;
			if(C.Outside(mos_r)) continue; // Trying to fill outside mosaic bounds
			posM.Set(int(C.X()+0.5), int(C.Y()+0.5));
			int xx = posM.X()-top_left.X();
			int yy = posM.Y()-top_left.Y();
			// If point is inside the cut, use labeling to fill in mosaic
			if(xx>=1 && xx<cut->w-1 && yy>=1 && yy<cut->h-1) {
				if(cut->get_pixel(xx, yy)==GC_FRAME) { // Fill from frame, if comes from within frame boundaries
					tc = inv_hm.Map(c);
					tc.Value2(img, &vld);
					if(vld)
						patches_image->set_pixel(posM, V);
				}
			} else { // Outside the cut. Fill only if mosaic pixel has not been set.
				VP = patches_image->get_pixel(posM);
				if(VP==bg) {
					tc = inv_hm.Map(c);
					tc.Value2(img, &vld);
					if(vld)
						patches_image->set_pixel(posM, V);
				}
			}
		}
	}
}

