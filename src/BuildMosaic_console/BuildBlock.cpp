#include "stdafx.h"
#include "graph.h"

using namespace libpolygon;

CPolygon* PolygonFromHmgr(Hmgr& hm, Y_Image* im, Coord2* start); // from Utilities
CPolygon* AddFootprintToMosaicPolygon(CPolygon* pFootprint, CPolygon* pMosaic); // from Utilities
int OptimalGraphCut(BMPImageG* pDif);
BMPImageG* ErrorSurface(BMPImageC* mosaic, int W, int H, Point2<int>& top_left, Coord2& start, 
	BMPImageC* img, Hmgr& hm);
void AddFrameToMosaic(BMPImageC* mosaic, BMPImageC* img, Hmgr& hm, Coord2& start);
void AddFrameToMosaicWithCut(BMPImageC* mosaic, BMPImageC* img, Hmgr& hm, Coord2& start,
	BMPImageG* cut, Point2<int>& top_left);
void CheckForHole(BMPImageG* dif, Point2<int>& top_left, Coord2& start, CPolygon* pMosaicPolygon);
void AddPatch(BMPImageC* patches_image, int frame_n, BMPImageC* img, Hmgr& hm, Coord2& start, Y_RGB<u_char> bg);
void AddPatchWithCut(BMPImageC* patches_image, int frame_n, BMPImageC* img, Hmgr& hm, Coord2& start,
	BMPImageG* cut, Point2<int>& top_left, Y_RGB<u_char> bg);

void BuildBlock(SCHEME_TYPE schemeType, Y_Movie64* pAVI, int N_frames, TransformRecord* pWorldtrans, int numAnchor, 
    Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, 
    Point2<int>& anchorPosition, TrustCoef* pTrustCoef, Y_RGB<u_char> invalidColor, BMPImageG* pValidMask,
    char* pMosaicName, char* block_id, bool whiteBackground, bool whole_mosaic)
{
	char *channel_stg = "a";
	int no_pixel_validation = 1;
    //bool validateFrame = (invalidColor!=Y_RGB<u_char>::BlackPixel);
    bool validateFrame = true;
    double layerwidth = 10.0;

	// Find order number of anchor frame
	// Break out of the loop as soon as found, because there could be two instances,
	// and second instance is not the one that anchor position was calculated for.
	int order_anchor_frame = TransformRecord::FindOrderNumberFromFrameNumber(pWorldtrans,
		N_frames, numAnchor);
	if(order_anchor_frame==-1) {
		cout << "Specify anchor frame number.\n";
		exit(1);
	}

	// Center of the first frame must coincide with 'start'. Find true
	// offset for all images.
	Coord2 c(0.5*frameSize.w, 0.5*frameSize.h); 
		c.SetSize(frameSize.w, frameSize.h);
		c.SetFocalLength(focalLength);
	Y_Image footprint(frameSize.w, frameSize.h);
	footprint.SetFocalLength(focalLength);
	// Image of the central point
	Hmgr anchor_hm = pWorldtrans[order_anchor_frame].GetHmgr();
	Coord2 c_image = anchor_hm.Map(c);
	// Point, where center of the 0th frame should go (user defined)
	Coord2 start(anchorPosition.x-c_image.X(), anchorPosition.y-c_image.Y());

	MosaicFramework *mfw = new MosaicFramework(1 /* colour */, (MosaicFramework::SCHEME_TYPE)schemeType);
	mfw->SetFocalLength(focalLength);
	mfw->SetMosaicSize(mosaicSize.w, mosaicSize.h, layerwidth);

	// Position images on mosaic
	Hmgr hm, hm_12, inv_hm1;
	bool must_add;
	TransformRecord *hm_record;
	char stg[32];
	BMPImageC *pBMPC;

	// If there are no trust coefficients, create them:
	if(!pTrustCoef) 
        pTrustCoef = new TrustCoef;
		
	// For all records in RAR file (within range, if specified)
	for(int i = 0, num = 0; i<N_frames; i++) {
		//if(pWorldtrans[i].f_n == 99)
		//	int kkk = 0;
		if(!pWorldtrans[i].essential) continue;
		hm_record = &pWorldtrans[i];
		hm = hm_record->GetHmgr();
		must_add = FrameOverlapsCanvas(mosaicSize, &footprint, hm, start);
		if(must_add) 
        {
			pAVI->ReadVideoFrame(&pBMPC, pWorldtrans[i].f_n);
			pBMPC->SetFocalLength(focalLength);
			// If some colour is declared as invalid, replace all pixels with
			// this colour with "officially" invalid colour Y_RGB<u_char>::INVALID_RGB:
			//InvalidateColour(imgC);
			if(validateFrame) 
                pBMPC->Validate();
			pBMPC->ValidateByMask(pValidMask);
			mfw->BlendImage(pBMPC, hm, start, pTrustCoef->Weight(pWorldtrans[i].f_n));			
			//mfw->SaveMosaic("M.bmp", nullptr);
			delete pBMPC;
			//if(whole_mosaic && Dlg->m_eachFrameOutput) { // Save mosaic after each addition. Works only for whole mosaic.
			//	sprintf_s(stg, 32, "M.%06d.bmp", num);
			//	BMPImageC* block = mfw->ToBMPC();
			//	int status = block->Save(stg);
			//	delete block;
			//}
			num++;
		}
		sprintf_s(stg, 32, "Tile %s: F %d", block_id, pWorldtrans[i].f_n);
	}
	if(!no_pixel_validation)
		mfw->ValidateMosaicPixels();
    BMPImageC* pMosaicC = mfw->ToBMPC();
    if(whiteBackground)
		pMosaicC->Substitute(Y_RGB<u_char>::BlackPixel, Y_RGB<u_char>::WhitePixel);

    if(pMosaicC->Save(pMosaicName)==-1)
    {
        cout << "Cannot save " << pMosaicName << endl;
    }

	delete pMosaicC;

	delete mfw;
}

void BuildBlock_Graphcut(Y_Movie64* pAVI, int N_frames, TransformRecord* pWorldtrans, int numAnchor, 
    Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, 
    Point2<int>& anchorPosition, TrustCoef* pTrustCoef, Y_RGB<u_char> invalidColor, BMPImageG* pValidMask,
    char* pMosaicName, char* block_id, bool whiteBackground, bool whole_mosaic)
{
	char *channel_stg = "a";
	int no_pixel_validation = 1;
    //bool validateFrame = (invalidColor!=Y_RGB<u_char>::BlackPixel);
    bool validateFrame = true;

	// Find order number of anchor frame
	// Break out of the loop as soon as found, because there could be two instances,
	// and second instance is not the one that anchor position was calculated for.
	int order_anchor_frame = TransformRecord::FindOrderNumberFromFrameNumber(pWorldtrans,
		N_frames, numAnchor);
	if(order_anchor_frame==-1) {
		cout << "Specify anchor frame number.\n";
		exit(1);
	}

	// Center of the first frame must coincide with 'start'. Find true
	// offset for all images.
	Coord2 c(0.5*frameSize.w, 0.5*frameSize.h); 
		c.SetSize(frameSize.w, frameSize.h);
		c.SetFocalLength(focalLength);
	Y_Image footprint(frameSize.w, frameSize.h);
	footprint.SetFocalLength(focalLength);
	// Image of the central point
	Hmgr anchor_hm = pWorldtrans[order_anchor_frame].GetHmgr();
	Coord2 c_image = anchor_hm.Map(c);
	// Point, where center of the 0th frame should go (user defined)
	Coord2 start(anchorPosition.x-c_image.X(), anchorPosition.y-c_image.Y());

	// Polygon holding current mosaic, possibly in several pieces.
	// This polygon could extend beyond the block boundaries.
	CPolygon* pMosaicPolygon = new CPolygon();
	
	// Position images on mosaic
	Hmgr hm, hm_12, inv_hm1;
	bool must_add;
	TransformRecord *hm_record;
	char stg[32];
	BMPImageC* pBlock = new BMPImageC(mosaicSize.w, mosaicSize.h);
	pBlock->SetBlack();
	//if(createPatchesImage) {
	//	pPatchesImage = new BMPImageC(mosaicSize.w, mosaicSize.h);
	//	pPatchesImage->SetColour(Y_RGB<u_char>::WhitePixel, 1);
	//}
	BMPImageC *pBMPC;

	// If there are no trust coefficients, create them:
	if(!pTrustCoef) 
        pTrustCoef = new TrustCoef;
		
	// For all records in RAR file (within range, if specified)
	for(int i = 0, num = 0; i<N_frames; i++) {
		//if(pWorldtrans[i].f_n == 99)
		//	int kkk = 0;
		if(!pWorldtrans[i].essential) continue;
		hm_record = &pWorldtrans[i];
		hm = hm_record->GetHmgr();
		must_add = FrameOverlapsCanvas(mosaicSize, &footprint, hm, start);
		if(must_add) 
        {
            pAVI->ReadVideoFrame(&pBMPC, pWorldtrans[i].f_n);
			pBMPC->SetFocalLength(focalLength);
			// If some colour is declared as invalid, replace all pixels with
			// this colour with "officially" invalid colour Y_RGB<u_char>::INVALID_RGB:
			//InvalidateColour(imgC);
			if(validateFrame) 
                pBMPC->Validate();
			pBMPC->ValidateByMask(pValidMask);
			CPolygon* pFootprint = PolygonFromHmgr(hm, &footprint, &start);
			CPolygon* pIntersection = libpolygon::Intersection(pFootprint, pMosaicPolygon);
			// Intersection is always a convex polygon, possibly with several holes.
			vector<CContour*> contours = pIntersection->GetContoursCopy();
			int n_contours = int(contours.size());
 			if(n_contours == 0) 
            { // Add without graph cut
				AddFrameToMosaic(pBlock, pBMPC, hm, start);
				//if(createPatchesImage)
				//	AddPatch(pPatchesImage, pWorldtrans[i].f_n, pBMPC, hm, start, Y_RGB<u_char>::WhitePixel); // white background!
			} 
		    else 
            {
				// Bounding box of the intersection:
				double bb_w, bb_h, bb_x, bb_y;
				CContour* BB = pIntersection->BoundingBox(&bb_w, &bb_h, &bb_x, &bb_y);
				int W = int(bb_w+0.5)+3, H = int(bb_h+0.5)+3; // To guarantee existence of boundary
				Point2<int> top_left(int(bb_x+0.5)-1, int(bb_y+0.5)-1);
				delete BB;
				// Fill in error surface. Boundaries are painted according to what they ovarlap with:
				// 255=GC_FRAME - new frame
				// 254=GC_MOSAIC - existing mosaic
				// 253=GC_NEITHER - pixels inside the bounding box that should not be classified
				BMPImageG* pDif = ErrorSurface(pBlock, W, H, top_left, start, pBMPC, hm);
				/*
				// Check for patching a hole. In this case, all the boundaries are GC_MOSAIC's, but the hole
				// exists inside, and we should paint it with GC_FRAME's.
				CheckForHole(pDif, top_left, start, pMosaicPolygon);
				*/
                //pDif->Save("Dif.bmp");
				if(OptimalGraphCut(pDif)==-1) {
					delete pBlock;
					cout << "Cannot do graph cut\n";
					exit(1);
				}
				// Add frame to mosaic.
				AddFrameToMosaicWithCut(pBlock, pBMPC, hm, start, pDif, top_left);
				//if(createPatchesImage)
				//	AddPatchWithCut(pPatchesImage, pWorldtrans[i].f_n, pBMPC, hm, start, dif, top_left, Y_RGB<u_char>::WhitePixel); // white background!
				delete pDif;

				for(vector<CContour*>::iterator ci = contours.begin(); ci!=contours.end(); ++ci)
					delete (*ci);
			}
			pMosaicPolygon = AddFootprintToMosaicPolygon(pFootprint, pMosaicPolygon);
			delete pBMPC;
			delete pFootprint;
			delete pIntersection;
			num++;
        }
   }
	if(whiteBackground) // Change all black pixels to white
		pBlock->Substitute(Y_RGB<u_char>::BlackPixel, Y_RGB<u_char>::WhitePixel);
	delete pMosaicPolygon;

    if(pBlock->Save(pMosaicName)==-1)
    {
        cout << "Cannot save " << pMosaicName << endl;
    }
	delete pBlock;
}

typedef struct _TrustArray {
	int i;
	double t;
} TrustArray;

struct Greater {
	
	bool operator()(TrustArray* ta1, TrustArray* ta2) 
	{
		return(ta1->t > ta2->t);
	}
};

int* OrderFramesAccordingToTrust(TrustCoef* trustCoef, int N, int debug)
{
	int i;
	vector<TrustArray*> v;
	vector<TrustArray*>::iterator v_iter;
	for(i = 0; i<N; i++) {
		TrustArray* ta = new TrustArray;
		ta->i = i; ta->t = trustCoef->Weight(i);
		v.push_back(ta);
	}
	// sort trust array in descending order
	sort(v.begin(), v.end(), Greater());
	int* order = new int[N];
	memset(order, 0, N*sizeof(int));

	if(debug) { // only print
		FILE* fp;
		if(fopen_s(&fp, "order.txt", "w")==0) {
			for(v_iter = v.begin(), i = 0; v_iter != v.end(); v_iter++, i++) {
				TrustArray* ta = *v_iter;
				fprintf(fp, "%04d\t%04d\t%8.4lf\n", i, ta->i, ta->t);
				order[i] = ta->i;
			}
			fclose(fp);
		}
	}
	// Save in 'order' and delete
	for(v_iter = v.begin(), i = 0; v_iter != v.end(); v_iter++, i++) {
		TrustArray* ta = *v_iter;
		order[i] = ta->i;
		delete ta;
	}
	v.clear();
	return(order);
}

void BuildBlock_GraphcutMin(Y_Movie64* pAVI, int N_frames, TransformRecord* pWorldtrans, int numAnchor, 
    Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, 
    Point2<int>& anchorPosition, TrustCoef* pTrustCoef, Y_RGB<u_char> invalidColor, BMPImageG* pValidMask,
    char* pMosaicName, char* block_id, bool whiteBackground, bool whole_mosaic)
{
	char *channel_stg = "a";
	int no_pixel_validation = 1;
    //bool validateFrame = (invalidColor!=Y_RGB<u_char>::BlackPixel);
    bool validateFrame = true;

	// Find order number of anchor frame
	// Break out of the loop as soon as found, because there could be two instances,
	// and second instance is not the one that anchor position was calculated for.
	int order_anchor_frame = TransformRecord::FindOrderNumberFromFrameNumber(pWorldtrans,
		N_frames, numAnchor);
	if(order_anchor_frame==-1) {
		cout << "Specify anchor frame number.\n";
		exit(1);
	}

	// Find biggest frame number
	int biggest_frame_number = -1;
	for(int k = 0; k<N_frames; k++) {
		biggest_frame_number = max(biggest_frame_number, pWorldtrans[k].base_f_n);
		biggest_frame_number = max(biggest_frame_number, pWorldtrans[k].f_n);
	}
	
	// Center of the first frame must coincide with 'start'. Find true
	// offset for all images.
	Coord2 c(0.5*frameSize.w, 0.5*frameSize.h); 
		c.SetSize(frameSize.w, frameSize.h);
		c.SetFocalLength(focalLength);
	Y_Image footprint(frameSize.w, frameSize.h);
	footprint.SetFocalLength(focalLength);
	// Image of the central point
	Hmgr anchor_hm = pWorldtrans[order_anchor_frame].GetHmgr();
	Coord2 c_image = anchor_hm.Map(c);
	//TRACE("C_image: %lf %lf\n", c_image.X(), c_image.Y());
	// Point, where center of the 0th frame should go (user defined)
	Coord2 start(anchorPosition.x-c_image.X(), anchorPosition.y-c_image.Y());

	// Polygon holding current mosaic, possibly in several pieces.
	// This polygon could extend beyond the block boundaries.
	CPolygon* pMosaicPolygon = new CPolygon();
	
	// Position images on mosaic
	Hmgr hm, hm_12, inv_hm1;
	bool must_add;
	TransformRecord *wrld_rec;
	char stg[32];
	BMPImageC* pBlock = new BMPImageC(mosaicSize.w, mosaicSize.h);
	pBlock->SetBlack();
	//if(createPatchesImage) {
	//	pPatchesImage = new BMPImageC(mosaicSize.w, mosaicSize.h);
	//	pPatchesImage->SetColour(Y_RGB<u_char>::WhitePixel, 1);
	//}
	BMPImageC *pBMPC;

	// If there are no trust coefficients, create them:
	if(!pTrustCoef) 
        pTrustCoef = new TrustCoef;

	// Trust coeffs are sorted in descending order, frames are being added in this order too.
	int* frame_order = OrderFramesAccordingToTrust(pTrustCoef, biggest_frame_number+1, 0);
		
	// For all records in logfile (within range, if specified)
	for(int i = 0, num = 0; i<N_frames; i++) {
		int frame_number = frame_order[i];
		double weight = pTrustCoef->Weight(frame_number);
		if(!pWorldtrans[i].essential) continue;
		if(EPSILON_EQ(pTrustCoef->Weight(frame_number), 0.0)) continue; // Don't add, if the weight is zero

		// Find world transform for specified frame:
		wrld_rec = TransformRecord::FindRecordFromFrameNumber(pWorldtrans, N_frames, frame_number);
		if(wrld_rec==nullptr) { // Inform and continue
			cout << "Cannot find record " << frame_number << "!\n";
		}
		hm = wrld_rec->GetHmgr();
		must_add = FrameOverlapsCanvas(mosaicSize, &footprint, hm, start);
		if(must_add) {
            pAVI->ReadVideoFrame(&pBMPC, pWorldtrans[i].f_n);
			pBMPC->SetFocalLength(focalLength);
			// If some colour is declared as invalid, replace all pixels with
			// this colour with "officially" invalid colour Y_RGB<u_char>::INVALID_RGB:
			//InvalidateColour(imgC);
			if(validateFrame) 
                pBMPC->Validate();
			pBMPC->ValidateByMask(pValidMask);
			CPolygon* pFootprint = PolygonFromHmgr(hm, &footprint, &start);
			CPolygon* pIntersection = libpolygon::Intersection(pFootprint, pMosaicPolygon);
			// Intersection is always a convex polygon, possibly with several holes.
			vector<CContour*> contours = pIntersection->GetContoursCopy();
			int n_contours = int(contours.size());

			if(n_contours == 0) 
            { // Add without graph cut
				AddFrameToMosaic(pBlock, pBMPC, hm, start);
				//if(createPatchesImage)
				//	AddPatch(pPatchesImage, pWorldtrans[i].f_n, pBMPC, hm, start, Y_RGB<u_char>::WhitePixel); // white background!
			} 
		    else 
            {
				// Bounding box of the intersection:
				double bb_w, bb_h, bb_x, bb_y;
				CContour* BB = pIntersection->BoundingBox(&bb_w, &bb_h, &bb_x, &bb_y);
				int W = int(bb_w+0.5)+3, H = int(bb_h+0.5)+3; // To guarantee existence of boundary
				Point2<int> top_left(int(bb_x+0.5)-1, int(bb_y+0.5)-1);
				delete BB;
				// Fill in error surface. Boundaries are painted according to what they ovarlap with:
				// 255=GC_FRAME - new frame
				// 254=GC_MOSAIC - existing mosaic
				// 253=GC_NEITHER - pixels inside the bounding box that should not be classified
				BMPImageG* pDif = ErrorSurface(pBlock, W, H, top_left, start, pBMPC, hm);
				/*
				// Check for patching a hole. In this case, all the boundaries are GC_MOSAIC's, but the hole
				// exists inside, and we should paint it with GC_FRAME's.
				CheckForHole(pDif, top_left, start, pMosaicPolygon);
				*/
				if(OptimalGraphCut(pDif)==-1) {
					delete pBlock;
					cout << "Cannot do graph cut\n";
					exit(1);
				}
				// Add frame to mosaic.
				AddFrameToMosaicWithCut(pBlock, pBMPC, hm, start, pDif, top_left);
				//if(createPatchesImage)
				//	AddPatchWithCut(pPatchesImage, pWorldtrans[i].f_n, pBMPC, hm, start, dif, top_left, Y_RGB<u_char>::WhitePixel); // white background!
				delete pDif;

				vector<CContour*>::iterator ci;
				for(ci=contours.begin();ci!=contours.end();++ci)
					delete (*ci);
			}
			pMosaicPolygon = AddFootprintToMosaicPolygon(pFootprint, pMosaicPolygon);
			delete pBMPC;
			delete pFootprint;
			delete pIntersection;

			num++;
		}
	}
	if(whiteBackground) // Change all black pixels to white
		pBlock->Substitute(Y_RGB<u_char>::BlackPixel, Y_RGB<u_char>::WhitePixel);
	delete pMosaicPolygon;
	delete [] frame_order;

	//if(createPatchesImage) {
	//	PaintBoundaries(patchesImage, Y_RGB<u_char>::WhitePixel);
	//	filename.Format(_T("%sPatches.bmp"), Dlg->m_workDir.GetBuffer(256));
	//	CT2CA tmp(filename);
	//	patchesImage->Save(tmp);
	//	delete patchesImage;
	//}
    if(pBlock->Save(pMosaicName)==-1)
    {
        cout << "Cannot save " << pMosaicName << endl;
    }
	delete pBlock;
}

#define K 100 // Belonging constant

int OptimalGraphCut(BMPImageG* pDif)
{
	int i, j;
	int vi, vip, vj, vjp;
	Graph::node_id* nodes = new Graph::node_id[pDif->w*pDif->h];
	Graph *g = new Graph();
	if(!nodes || !g)
		return(-1);

	// Do not allow 0 difference:
	pDif->Substitute((u_char)0, (u_char)1);

	for(j = 0; j<pDif->h; j++) {
		for(i = 0; i<pDif->w; i++) {
			nodes[j*pDif->w+i] = g->add_node();
			g->set_tweights(nodes[j*pDif->w+i], 0, 0);
			
			// All GC_MOSAIC's belong to SOURCE
			if(pDif->get_pixel(i, j)==GC_MOSAIC)
				g->set_tweights(nodes[j*pDif->w+i], K, 0);
			// All GC_FRAME's belong to SINK
			if(pDif->get_pixel(i, j)==GC_FRAME)
				g->set_tweights(nodes[j*pDif->w+i], 0, K);

			/*
			// All 255's: SOURCE
			if(dif->get_pixel(i, j)==255)
				g->set_tweights(nodes[j*pDif->w+i], K, 0);
			// Borders that are not 255: SINK
			if((i==0 || i==pDif->w-1 || j==0 || j==pDif->h-1) && pDif->get_pixel(i, j)!=255)
				g->set_tweights(nodes[j*pDif->w+i], 0, K);
			*/
		}
	}
	// Cheap edges are where both pixels are dark
	for(j = 0; j<pDif->h; j++) {
		for(i = 0; i<pDif->w-1; i++) {
			vi = int(pDif->get_pixel(i, j));
			vip = int(pDif->get_pixel(i+1, j));
			g->add_edge(nodes[j*pDif->w+i], nodes[j*pDif->w+i+1], (vi+vip)/2, (vi+vip)/2);
		}
	}
	for(j = 0; j<pDif->h-1; j++) {
		for(i = 0; i<pDif->w; i++) {
			vj = int(pDif->get_pixel(i, j));
			vjp = int(pDif->get_pixel(i, j+1));
			g->add_edge(nodes[j*pDif->w+i], nodes[(j+1)*pDif->w+i], (vj+vjp)/2, (vj+vjp)/2);
		}
	}

	Graph::flowtype flow = g -> maxflow();

	//printf("Flow = %d\n", flow);
	// Occasionally some pixels may be misclassified as SINK, although they are in the middle of SOURCE.
	// To avoid this...??
	for(j = 0; j<pDif->h; j++) {
		for(i = 0; i<pDif->w; i++) {
			if(g->what_segment(nodes[j*pDif->w+i]) == Graph::SINK) // SINK stays GC_FRAME
				pDif->set_pixel(i, j, GC_FRAME);
			else // SOURCE (GC_MOSAIC) is recoloured as black to be visible
				pDif->set_pixel(i, j, u_char(0));
		}
	}

	delete g;
	delete [] nodes;
	return(0);
}

// Fill in error surface. Boundaries are painted with the colours indication where they came from:
// 255=GC_FRAME - new added frame
// 254=GC_MOSAIC - existing mosaic
// 253=GC_NEITHER - should not be classified
// 0:252=(0:GC_VALID_MAX) - difference between images
BMPImageG* ErrorSurface(BMPImageC* pMosaicC, int W, int H, Point2<int>& top_left, Coord2& start, 
	BMPImageC* pBMPC, Hmgr& hm)
{
	BMPImageG* pDif = new BMPImageG(W, H);

	Y_RGB<u_char> V_m;
	Y_RGB<double> V_i;
	u_char v;
	int ii, jj, vld;
	Hmgr inv_hm = hm.Invert();
	Coord2 c, tc;
		c.SetImage(pBMPC);
		tc.SetImage(pBMPC);
	Coord2 C;
	Y_Rect<int> im_r = pBMPC->GetRect();

	for(int j = 0; j<pDif->h; j++) {
		for(int i = 0; i<pDif->w; i++) {
			// Positions in mosaic:
			ii = i+top_left.x;
			jj = j+top_left.y;
			c.Set(ii-start.x, jj-start.y);
			// From mosaic:
			V_m = pMosaicC->get_pixel(ii,jj);
			// From image:
			tc = inv_hm.Map(c);
			V_i = tc.Value2(pBMPC, &vld);
			if(!V_m.IsZero() && vld) { // Overlap
				// 253:255 (GC_NEITHER:GC_FRAME) are special values
				v = min((u_char) fabs(V_i.Greyscale()-V_m.Greyscale()), GC_VALID_MAX);
				pDif->set_pixel(i, j, v);
			} else if(!V_m.IsZero() && !vld) { // Only existing mosaic
				pDif->set_pixel(i, j, GC_MOSAIC);
			} else if(V_m.IsZero() && vld) { // Only new added frame
				pDif->set_pixel(i, j, GC_FRAME);
			} else {
				// Happens sometimes, neither frame nor mosaic
				pDif->set_pixel(i, j, GC_NEITHER);
			}
		}
	}
	return(pDif);
}

