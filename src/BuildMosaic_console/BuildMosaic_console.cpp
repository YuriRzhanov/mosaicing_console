// BuildMosaic_console.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Descr.h"

using namespace std;

char* program;


int main(int argc, char* argv[])
{
	program = argv[0];

	int debug = 0;
	char* pAVIName = nullptr;
	char* pRRAFileName = nullptr;
    char* pTrustName = nullptr;
    char* pMaskName = nullptr;
    char* pOutlinesName = nullptr;
    char* pMosaicName = nullptr;
    int numAnchor = 0;
    double startYaw = 0.0;
    bool noZoom = false;
    double startZoom = 1.0; // startZoom=0.5 results in half-size mosaic
	int frameFrom = 0, frameTo = 1000000000;
    int schemeType = GRAPHCUT_SCHEME;
    int invColor[3] = {0, 0, 0};
    bool whiteBackground = false;

	DFile f(argv[1], "BM.IN", 2);
	f.Reg("Debug", "Debug level", &debug, debug);
	f.Reg("AVIName", "AVI file name", &pAVIName, pAVIName);
	f.Reg("RRAName", "RRA file name", &pRRAFileName, pRRAFileName);
	f.Reg("TrustName", "Trust file name", &pTrustName, pTrustName);
	f.Reg("MaskName", "Mask file name (BMP)", &pMaskName, pMaskName);
	f.Reg("OutlinesName", "Outlines file name (BMP)", &pOutlinesName, pOutlinesName);
	f.Reg("MosaicName", "Mosaic file name (BMP)", &pMosaicName, pMosaicName);
	f.Reg("FrameRange", "Frames to use", &frameFrom, frameFrom, &frameTo, frameTo);
	f.Reg("BuildingScheme", "Mosaic building scheme", &schemeType, schemeType);
	f.Reg("InvalidColor", "Invalid color in frames", &invColor[0], invColor[0], 
        &invColor[1], invColor[1], &invColor[2], invColor[2]);
	f.Reg("WhiteBackground", "Make background white", &whiteBackground, whiteBackground);
	f.Process();

    Y_RGB<u_char> invalidColor;
    invalidColor.Set(u_char(invColor[0]), u_char(invColor[1]), u_char(invColor[2]));

	Y_Movie64* pAVI  = new Y_Movie64(pAVIName);
	if(pAVI->isOk()==false) {
		fprintf_s(stderr, "Cannot open %s.\n", pAVIName);
		exit(1);
	}
	int width = pAVI->GetWidth();
	int height = pAVI->GetHeight();
	frameTo = std::min(frameTo, int(pAVI->GetNumVideoFrames()-1));

    Hmgr startHmgr = Hmgr::FromEuler(1.0, startZoom, 0.0, 0.0, startYaw);
    TransformRecord* pWorldtrans = nullptr;
    double focalLength = 0.0;
    Y_Rect<int> frameSize;
    int N_max_frames = ReadRelativeRRA(pRRAFileName, startHmgr, numAnchor, noZoom, pWorldtrans, focalLength, frameSize);
	double FoV_h = Rad2Deg(2.0*atan2(0.5*double(width), focalLength));

    Y_Rect<int> mosaicSize;
    Point2<int> anchorPosition;
    EstimateSizeAndPosition(N_max_frames, pWorldtrans, frameFrom, frameTo, numAnchor, frameSize, focalLength,
        mosaicSize, anchorPosition);

    TrustCoef* pTrustCoef = nullptr;
    if(pTrustName)
    {
        pTrustCoef = new TrustCoef(pTrustName);
    }
	BMPImageG* pValidMask = nullptr;
    if(pMaskName)
    {
        pValidMask = new BMPImageG(pMaskName);
    }

    if(pOutlinesName)
    {
        BMPImageC *pOutlineBMP = new BMPImageC(mosaicSize.w, mosaicSize.h);
        pOutlineBMP->SetColour(255, 255, 255, 1);
	    // Find order number of anchor frame
	    // Break out of the loop as soon as found, because there could be two instances,
	    // and second instance is not the one that anchor position was calculated for.
	    int order_anchor_frame = TransformRecord::FindOrderNumberFromFrameNumber(pWorldtrans, N_max_frames, numAnchor);
	    if(order_anchor_frame==-1) {
		    delete pOutlineBMP;
		    cout << "Specify anchor frame number.\n";
		    exit(1);
	    }
	    // Center of the first frame must coinside with 'start'. Find true
	    // offset for all images.
	    Coord2 c(0.5*frameSize.w, 0.5*frameSize.h); 
		    c.SetSize(frameSize.w, frameSize.h);
		    c.SetFocalLength(focalLength);
	    // Image of the central point
	    Hmgr anchor_hm = pWorldtrans[order_anchor_frame].GetHmgr();
	    Coord2 c_image = anchor_hm.Map(c);
	    // Point, where center of the 0th frame should go (user defined)
	    Coord2 start(anchorPosition.x-c_image.X(), anchorPosition.y-c_image.Y());

	    Y_Image footprint(frameSize.w, frameSize.h);
	    footprint.SetFocalLength(focalLength);

	    int must_add = 1;

	    // If there are no trust coefficients, create them:
	    if(!pTrustCoef) 
            pTrustCoef = new TrustCoef;
	    // For all records in RRA file (within range, if specified)
	    for(int i = 0; i<N_max_frames; i++) {
		    if(!pWorldtrans[i].essential) continue;
		    if(EPSILON_EQ(pTrustCoef->Weight(pWorldtrans[i].f_n), 0.0)) continue; // Don't add, if the weight is zero
		    TransformRecord* hm_record = &pWorldtrans[i];
		    Hmgr hm = hm_record->GetHmgr();
		    must_add = FrameOverlapsCanvas(mosaicSize, &footprint, hm, start);
		    Y_RGB<u_char> clr = Frame2Colour(pWorldtrans[i].f_n);
		    DrawOutline(pOutlineBMP, frameSize.w, frameSize.h, hm_record->GetHmgr(), start, clr, focalLength);
	    }
        pOutlineBMP->Save(pOutlinesName);
        delete pOutlineBMP;
	    delete pAVI;
        delete [] pWorldtrans;
        return(0);
    }

    //	pBMP->Substitute(V_invalid, Y_RGB<u_char>::INVALID_RGB);
    switch(schemeType)
    {
    case CLOSEST_SCHEME:
        BuildBlock(CLOSEST_SCHEME, pAVI, N_max_frames, pWorldtrans, numAnchor, frameSize, focalLength,
            mosaicSize, anchorPosition, pTrustCoef, invalidColor, pValidMask, pMosaicName, "--", whiteBackground, true);
        break;
    case AVERAGE_SCHEME:
        BuildBlock(AVERAGE_SCHEME, pAVI, N_max_frames, pWorldtrans, numAnchor, frameSize, focalLength,
            mosaicSize, anchorPosition, pTrustCoef, invalidColor, pValidMask, pMosaicName, "--", whiteBackground, true);
        break;
    case MEDIAN_SCHEME:
        BuildBlock(MEDIAN_SCHEME, pAVI, N_max_frames, pWorldtrans, numAnchor, frameSize, focalLength,
            mosaicSize, anchorPosition, pTrustCoef, invalidColor, pValidMask, pMosaicName, "--", whiteBackground, true);
        break;
    case GRAPHCUT_SCHEME:
        BuildBlock_Graphcut(pAVI, N_max_frames, pWorldtrans, numAnchor, frameSize, focalLength, 
            mosaicSize, anchorPosition, pTrustCoef, invalidColor, pValidMask, pMosaicName, "--", whiteBackground, true);
    default:
        BuildBlock_GraphcutMin(pAVI, N_max_frames, pWorldtrans, numAnchor, frameSize, focalLength, 
            mosaicSize, anchorPosition, pTrustCoef, invalidColor, pValidMask, pMosaicName, "--", whiteBackground, true);
   }

	delete pAVI;
    delete [] pWorldtrans;

	return 0;
}

// Converted to relative hmgr's, then to world hmgr's
int ReadRelativeRRA(char* pRRAFileName, Hmgr startHmgr, int numAnchor, bool noZoom, TransformRecord*& pWorldtrans, 
    double& focalLength, Y_Rect<int>& frameSize)
{
    int N_records = 0;
    char* pInfo = nullptr;
	RARecord *rel_recs = RARecord::ReadRecs(pRRAFileName, &N_records, &pInfo);
	if(!rel_recs || !pInfo) {
		cout << "Cannot read RARs or info." << endl;
		exit(-1);
	}
	Y_Rect<int>* frame = Y_Utils::GetFrameSizeFromInfo(pInfo);
	if(!frame) {
		cout << "Cannot extract frame size from info." << endl;
		exit(-1);
	}
    frameSize = *frame;
	focalLength = Y_Utils::GetFocalLength(pInfo);
	double FoV = Rad2Deg(2.0*atan2(0.5*double(frame->w), focalLength));
	// Convert RARs to transforms
	TransformRecord* rel_trans = RRA2Trans(rel_recs, N_records, focalLength, noZoom);
	delete [] rel_recs;

	// World transform for the anchor frame is 'numAnchor'

	// Could be at most N_records+1 world records. Actual number is 
	// held in 'N_frames'.
	int N_max_frames = N_records+1;
	pWorldtrans = new TransformRecord[N_max_frames];
	int N_frames = ComputeWorldTransforms(rel_trans, N_records, pWorldtrans, N_max_frames,
		numAnchor, startHmgr, focalLength, nullptr, nullptr /*m_fpDebug*/);
	delete [] rel_trans;
    return N_max_frames;
}

void EstimateSizeAndPosition(int N_frames, TransformRecord* pWorldtrans, int frameFrom, int frameTo,
    int numAnchor, Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, Point2<int>& anchorPosition)
{
    // Prior to estimating extent, set essential flag only for those transforms that are needed.
	for(int i = 0; i<N_frames; i++) 
    {
		pWorldtrans[i].essential = (pWorldtrans[i].f_n>=frameFrom && pWorldtrans[i].f_n<=frameTo);
	}
	int status = EstimateExtentAndAnchorPosition(pWorldtrans, N_frames,
		numAnchor, frameSize, focalLength,
		mosaicSize, anchorPosition); // from Utilities
}


