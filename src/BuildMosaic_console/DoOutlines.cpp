#include "stdafx.h"

Y_RGB<u_char> Frame2Colour(int frame_n)
{
	int r = frame_n/100;
	int g = (frame_n-100*r)/10; 
	int b = (frame_n-100*r-10*g);
	return(Y_RGB<u_char>(r, g, b));
}

void DrawOutline(BMPImageC *o, int w, int h, Hmgr &hm, 
		Coord2 start, Y_RGB<u_char> clr, double focal_length)
{
	Y_Rect<int> mosaic_outline = o->GetRect();

	int i, n;
	Point2<int>* pts = new Point2<int>[10000];
	Coord2 c1; c1.SetSize(w, h); c1.SetFocalLength(focal_length);
	Coord2 c2; c2.SetSize(w, h); c2.SetFocalLength(focal_length);
	
	c1.Set(0, 0); c1 = hm.Map(c1); c1 += start;
	c2.Set(w-1, 0); c2 = hm.Map(c2); c2 += start;
	n = Y_Utils::bresenham(c1.ToIntPoint(), c2.ToIntPoint(), pts, 10000);
	for(i = 0; i<n; i++) {
		if(mosaic_outline.Inside(pts[i]))
			o->set_pixel(pts[i], clr);
	}
	
	c1.Set(w-1, 0); c1 = hm.Map(c1); c1 += start;
	c2.Set(w-1, h-1); c2 = hm.Map(c2); c2 += start;
	n = Y_Utils::bresenham(c1.ToIntPoint(), c2.ToIntPoint(), pts, 10000);
	for(i = 0; i<n; i++) {
		if(mosaic_outline.Inside(pts[i]))
			o->set_pixel(pts[i], clr);
	}
	
	c1.Set(w-1, h-1); c1 = hm.Map(c1); c1 += start;
	c2.Set(0, h-1); c2 = hm.Map(c2); c2 += start;
	n = Y_Utils::bresenham(c1.ToIntPoint(), c2.ToIntPoint(), pts, 10000);
	for(i = 0; i<n; i++) {
		if(mosaic_outline.Inside(pts[i]))
			o->set_pixel(pts[i], clr);
	}
	
	c1.Set(0, h-1); c1 = hm.Map(c1); c1 += start;
	c2.Set(0, 0); c2 = hm.Map(c2); c2 += start;
	n = Y_Utils::bresenham(c1.ToIntPoint(), c2.ToIntPoint(), pts, 10000);
	for(i = 0; i<n; i++) {
		if(mosaic_outline.Inside(pts[i]))
			o->set_pixel(pts[i], clr);
	}
	delete [] pts;
}
