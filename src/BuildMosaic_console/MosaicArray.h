#ifndef _MOSAICARRAY_H_DEFINED_
#define _MOSAICARRAY_H_DEFINED_

#include "MosaicCell.h"

template <class T>
class MosaicArray {
	public:
		MosaicArray(int _w, int _h, double _layerwidth = 0.0);
		~MosaicArray() { delete array; }
		void SetLayer(double _layerwidth) { layerwidth=_layerwidth; }
		void Set(int i, int j, double _d, T _v);
		/*
		void Set(int i, int j, double _d, T _v)
			{
				MosaicCell<T> *cell;
				if(!array->BadIndex(i, j)) {
					cell = array->GetP(i, j);
					cell->Insert(_d, _v);
				}
			}
			*/
		T Get(int i, int j);
		Y_Rect<int> GetRect() { return(Y_Rect<int>(0, 0, w, h)); }
		void Black();
		BMPImageG *ToBMPG();
		BMPImageC *ToBMPC();
		BMPImageG *ToHits(); // Show which pixels got hit
		BMPImageG *ToDif(); // Per-pixel difference
		int SaveImage(const char *name);
		int SaveHits(const char *name);
		int SaveDif(const char *name);
		void SetWatch(int x, int y) { watch[n_watch].Set(x, y); n_watch++; }
		void SetWatch(Point2<int> p) { SetWatch(p.X(), p.Y()); }
		int CheckWatch(int x, int y) {
			for(int i = 0; i<n_watch; i++) {
				if(watch[i].X()==x && watch[i].Y()==y) return(1);
			}
			return(0);
		}
		int CheckWatch(Point2<int> p) { return(CheckWatch(p.X(), p.Y())); }
	public:
		int w, h;
		int interp; // 0-linear; 1-tanh
		int n_watch;
		Point2<int> watch[10];
		int watch_print; // set when watch is hit
	private:
		double layerwidth;
		Array2D<MosaicCell<T> > *array;
};

#if defined(WIN32) || defined(WIN64)
#include "MosaicArray.include"
#endif

#endif

