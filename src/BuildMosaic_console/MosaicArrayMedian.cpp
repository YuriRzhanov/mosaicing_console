//#ifndef WIN32
#include "YUtils.h"
#include "MosaicArrayMedian.h"
//#endif

using namespace std;

MosaicArrayMedian::MosaicArrayMedian(int _w, int _h, int _depth, int _chan)
{
	w = _w; h = _h; depth = _depth;
	n_watch = 0; watch_print = 0;
	num = new Array2D<u_char>(w, h); num->Zero();
	if(_chan==1) {
		array = new Array3D<u_char>(w, h, depth); array->Zero();
		c_array = nullptr;
	} else {
		c_array = new Array4D<u_char>(w, h, depth, 3); c_array->Zero();
		array = nullptr;
	}
}

void MosaicArrayMedian::Set(int i, int j, u_char _v)
{
	if(array==nullptr) {
		cout << "Array has 3 channels!" << endl;
		exit(1);
	}
	if(!num->BadIndex(i,j)) {
		int k = num->Get(i,j); // Number of entries
		if(k<depth) {
			array->Set(i,j,k,_v);
			num->Set(i,j,k+1);
		} else { // Throw away two extremes
			vector<u_char> v;
			vector<u_char>::iterator v_iter;
			int l;
			for(l = 0; l<k; l++) {
				v.push_back(array->Get(i,j,l));
			}
			v.push_back(_v);
			sort(v.begin(), v.end());
			v.erase(v.begin());
			v.erase(v.end()-1);
			for(v_iter = v.begin(), l = 0; v_iter != v.end(); v_iter++, l++) {
				array->Set(i,j,l,*v_iter);
			}
			num->Set(i,j,depth-1);
		}
	}
	if(watch_print) {
		watch_print = 0;
		PrintCell(i,j);
	}
}

void MosaicArrayMedian::Set(int i, int j, Y_RGB<u_char> _V)
{
	if(c_array==nullptr) {
		cout << "Array has 1 channel!" << endl;
		exit(1);
	}
	if(!num->BadIndex(i,j)) {
		int k = num->Get(i,j); // Number of entries
		if(k<depth) {
			c_array->Set(i,j,k,0,_V.p[0]);
			c_array->Set(i,j,k,1,_V.p[1]);
			c_array->Set(i,j,k,2,_V.p[2]);
			num->Set(i,j,k+1);
		} else { // Throw away two extremes
			vector<Y_RGB<u_char> > v;
			vector<Y_RGB<u_char> >::iterator v_iter;
			int l;
			Y_RGB<u_char> V;
			for(l = 0; l<k; l++) {
				V.p[0] = c_array->Get(i,j,l,0);
				V.p[1] = c_array->Get(i,j,l,1);
				V.p[2] = c_array->Get(i,j,l,2);
				v.push_back(V);
			}
			v.push_back(_V);
			sort(v.begin(), v.end(), GreaterY_RGB());
			int a1 = int(v.size());
			v.erase(v.begin());
			int a2 = int(v.size());
			v.erase(v.end()-1);
			int a3 = int(v.size());
			for(v_iter = v.begin(), l = 0; v_iter != v.end(); ++v_iter, l++) {
				//Y_RGB<u_char> x = *v_iter;
				int kk = 0;
				c_array->Set(i,j,l,0,(*v_iter).p[0]);
				c_array->Set(i,j,l,1,(*v_iter).p[1]);
				c_array->Set(i,j,l,2,(*v_iter).p[2]);
			}
			num->Set(i,j,depth-1);
		}
	}
	if(watch_print) {
		watch_print = 0;
		PrintCell(i,j);
	}
}

u_char MosaicArrayMedian::Get(int i, int j)
{
	if(array==nullptr) {
		cout << "Array has 3 channels!" << endl;
		exit(1);
	}
	if(!num->BadIndex(i,j)) {
		int k = num->Get(i,j); // Number of entries
		if(k==0)
			return(0);
		if(k==1)
			return(array->Get(i,j,0));
		if(k==2) { // Averaging must be better than just taking one of the values
			return(u_char(0.5*array->Get(i,j,0)+0.5*array->Get(i,j,1)));
		}
		vector <u_char> v;
		vector <u_char>::iterator v_iter;
		
		for(int l = 0; l<k; l++) {
			v.push_back(array->Get(i,j,l));
		}
		sort(v.begin(), v.end());
		return(v.at(k/2));
	}
	return(0);
}

Y_RGB<u_char> MosaicArrayMedian::GetC(int i, int j)
{
	if(c_array==nullptr) {
		cout << "Array has 1 channel!" << endl;
		exit(1);
	}
	if(!num->BadIndex(i,j)) {
		int k = num->Get(i,j); // Number of entries
		if(k==0)
			return(Y_RGB<u_char>(0,0,0));
		if(k==1)
			return(Y_RGB<u_char>(c_array->Get(i,j,0,0),c_array->Get(i,j,0,1),c_array->Get(i,j,0,2)));
		if(k==2) {
			Y_RGB<u_char> v0(c_array->Get(i,j,0,0),c_array->Get(i,j,0,1),c_array->Get(i,j,0,2));
			Y_RGB<u_char> v1(c_array->Get(i,j,1,0),c_array->Get(i,j,1,1),c_array->Get(i,j,1,2));
			Y_RGB<u_char> v = v0/2+v1/2;
			return(v);
		}
		vector <Y_RGB<u_char> > v;
		vector <Y_RGB<u_char> >::iterator v_iter;
		
		Y_RGB<u_char> V;
		for(int l = 0; l<k; l++) {
			V.p[0] = c_array->Get(i,j,l,0);
			V.p[1] = c_array->Get(i,j,l,1);
			V.p[2] = c_array->Get(i,j,l,2);
			v.push_back(V);
		}
		sort(v.begin(), v.end(), GreaterY_RGB());
		return(v.at(k/2));
	}
	return(Y_RGB<u_char>(0,0,0));
}

u_char MosaicArrayMedian::GetAverage(int i, int j)
{
	if(array==nullptr) {
		cout << "Array has 3 channels!" << endl;
		exit(1);
	}
	if(!num->BadIndex(i,j)) {
		int k = num->Get(i,j); // Number of entries
		if(k==0)
			return(0);
		if(k==1)
			return(array->Get(i,j,0));
		int sum = 0;
		for(int l = 0; l<k; l++) {
			sum += int(array->Get(i,j,l));
		}
		return(u_char(sum/k));
	}
	return(0);
}

Y_RGB<u_char> MosaicArrayMedian::GetCAverage(int i, int j)
{
	if(c_array==nullptr) {
		cout << "Array has 1 channel!" << endl;
		exit(1);
	}
	if(!num->BadIndex(i,j)) {
		int k = num->Get(i,j); // Number of entries
		if(k==0)
			return(Y_RGB<u_char>(0,0,0));
		if(k==1)
			return(Y_RGB<u_char>(c_array->Get(i,j,0,0),c_array->Get(i,j,0,1),c_array->Get(i,j,0,2)));
		int r = 0, g = 0, b = 0;
		Y_RGB<u_char> V;
		for(int l = 0; l<k; l++) {
			r += c_array->Get(i,j,l,0);
			g += c_array->Get(i,j,l,1);
			b += c_array->Get(i,j,l,2);
		}
		return(Y_RGB<u_char>((u_char)(r/k),(u_char)(g/k),(u_char)(b/k)));
	}
	return(Y_RGB<u_char>(0,0,0));
}

void MosaicArrayMedian::Black()
{
	num->Zero();
	if(array) array->Zero();
	if(c_array) c_array->Zero();
}

BMPImageG *MosaicArrayMedian::ToBMPG(bool median_flattening)
{
	Y_Rect<int> mos_r = GetRect();
	u_char v;
	Y_RGB<u_char> V;
	BMPImageG *bmp = new BMPImageG(mos_r.w, mos_r.h);
	
	for(int j = 0; j<bmp->h; j++) {
		for(int i = 0; i<bmp->w; i++) {
			if(CheckWatch(i, j)) 
				watch_print = 1;
			if(array)
				v = (median_flattening) ? Get(i, j) : GetAverage(i, j);
			else {
				V = (median_flattening) ? GetC(i, j) : GetCAverage(i, j);
				v = V.Greyscale();
			}
			bmp->set_pixel(i, j, v);
		}
	}
	return(bmp);
}

BMPImageC *MosaicArrayMedian::ToBMPC(bool median_flattening)
{
	Y_Rect<int> mos_r = GetRect();
	u_char v;
	Y_RGB<u_char> V;
	BMPImageC *bmp = new BMPImageC(mos_r.w, mos_r.h);
	if(!bmp || bmp->fail)
		return(nullptr);
	
	for(int j = 0; j<bmp->h; j++) {
		for(int i = 0; i<bmp->w; i++) {
			if(CheckWatch(i, j)) 
				watch_print = 1;
			if(array) {
				v = (median_flattening) ? Get(i, j) : GetAverage(i, j);
				V.p[0] = V.p[1] = V.p[2] = v;
			} else
				V = (median_flattening) ? GetC(i, j) : GetCAverage(i, j);
			bmp->set_pixel(i, j, V);
		}
	}
	return(bmp);
}

BMPImageG *MosaicArrayMedian::ToHits()
{
	Y_Rect<int> mos_r = GetRect();
	BMPImageG *bmp = new BMPImageG(mos_r.w, mos_r.h);
	
	for(int j = 0; j<bmp->h; j++) {
		for(int i = 0; i<bmp->w; i++) {
			if(!num->BadIndex(i, j)) {
				if(CheckWatch(i, j)) 
					watch_print = 1;
				bmp->set_pixel(i, j, u_char(100*num->Get(i,j)));
			}
		}
	}
	return(bmp);
}

int MosaicArrayMedian::SaveImage(const char *name, bool median_flattening)
{
	int status;
	if(array) {
		BMPImageG *bmpG = ToBMPG(median_flattening);
		if(!bmpG)
			return(-1);
		status = bmpG->Save(name);
		delete bmpG;
	} else {
		BMPImageC *bmpC = ToBMPC(median_flattening);
		if(!bmpC)
			return(-1);
		status = bmpC->Save(name);
		delete bmpC;
	}
	return(status);
}

int MosaicArrayMedian::SaveHits(const char *name)
{
	BMPImageG *bmpG = ToHits();
	int status = bmpG->Save(name);
	delete bmpG;
	return(status);
}

void MosaicArrayMedian::PrintCell(int i, int j)
{
	cerr << "MosaicArrayMedian(" << i << "," << j << ")\n";
	int k = num->Get(i,j); // Number of entries
	if(array) {
		for(int l = 0; l<k; l++)
			fprintf(stderr, "%d ", array->Get(i,j,l));
	}
	if(c_array) {
		for(int l = 0; l<k; l++)
			cerr << "(" << c_array->Get(i,j,l,0) << "," << c_array->Get(i,j,l,1) << "," << c_array->Get(i,j,l,2) << ")\n";
	}
	cerr << "\n";
}

int MosaicArrayMedian::GetNumHits(int i, int j) // Number of values in the cell
{
	return(num->Get(i,j)); // Number of entries
}

vector<u_char> MosaicArrayMedian::GetHits(int i, int j, int& n) // all values in the cell
{
	vector<u_char> v;
	n = num->Get(i,j); // Number of entries
	for(int l = 0; l<n; l++)
		v.push_back(array->Get(i,j,l));
	return(v);
}
