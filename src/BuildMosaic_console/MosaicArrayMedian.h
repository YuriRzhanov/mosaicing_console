#ifndef _MOSAICARRAYMEDIAN_H_DEFINED_
#define _MOSAICARRAYMEDIAN_H_DEFINED_

#include "Array.h"
#include "BMPImageC.h"
#include <vector>
#include <list>
#include <algorithm>
#include <functional>      // For greater<int>( )
#include <iostream>

class MosaicArrayMedian {
	public:
		MosaicArrayMedian(int _w, int _h, int _depth, int _channels);
		~MosaicArrayMedian() { 
			delete num; delete array;
		}
		struct GreaterY_RGB {
			bool operator()(Y_RGB<u_char> p1, Y_RGB<u_char> p2) 
			{
				u_char t1 = p1.Greyscale();
				u_char t2 = p2.Greyscale();
				return(t1>t2);
			}
		};
		void Set(int i, int j, u_char _v);
		void Set(int i, int j, Y_RGB<u_char> _V);
		u_char Get(int i, int j); // Median value
		Y_RGB<u_char> GetC(int i, int j); // Median value
		u_char GetAverage(int i, int j); // Mean value
		Y_RGB<u_char> GetCAverage(int i, int j); // Mean value
		Y_Rect<int> GetRect() { return(Y_Rect<int>(0, 0, w, h)); }
		void Black();
		BMPImageG *ToBMPG(bool median_flattening = true);
		BMPImageC *ToBMPC(bool median_flattening = true);
		BMPImageG *ToHits(); // Show which pixels got hit
		int SaveImage(const char *name, bool median_flattening = true);
		int SaveHits(const char *name);
		void SetWatch(int x, int y) { watch[n_watch].Set(x, y); n_watch++; }
		void SetWatch(Point2<int> p) { SetWatch(p.X(), p.Y()); }
		int CheckWatch(int x, int y) {
			for(int i = 0; i<n_watch; i++) {
				if(watch[i].X()==x && watch[i].Y()==y) return(1);
			}
			return(0);
		}
		int CheckWatch(Point2<int> p) { return(CheckWatch(p.X(), p.Y())); }
		void PrintCell(int i, int j);
		int GetNumHits(int i, int j); // Number of values in the cell
		vector<u_char> GetHits(int i, int j, int& n); // all values in the cell
	public:
		int w, h, depth;
		int interp; // 0-linear; 1-tanh
		int n_watch;
		Point2<int> watch[10];
		int watch_print; // set when watch is hit
	private:
		Array2D<u_char> *num;
		Array3D<u_char> *array; // Greyscale
		Array4D<u_char> *c_array; // Colour (x,y,depth,channel)
};

#endif

