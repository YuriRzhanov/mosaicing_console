#ifndef _MOSAICCELL_H_DEFINED_
#define _MOSAICCELL_H_DEFINED_

// T could be float/double,Y_RGB<float> or Y_RGB<double>

#include "Array.h"
#include "YUtils.h"
#include "BMPImageC.h"

template <class T>
class MosaicCell {
	public:
		MosaicCell() { d[0]=d[1]=1.0e10; v[0]=v[1]=0.0; }
		void Insert(double _d, T _v);
		T ValueLinear(double layer);
		T ValueTanh(double layer);
		void Black(); // Return to original setting
		int Hits();
		double Dif();
		void Print() {
			cout << "  " << d[0] << "  " << v[0] << endl;
			cout << "  " << d[1] << "  " << v[1] << endl;
		}
	private:
		double d[2];
		T v[2];
};

#if defined(WIN32) || defined(WIN64)
#include "MosaicCell.include"
#endif

#endif
