#include "MosaicFramework.h"

using namespace std;

MosaicFramework::MosaicFramework(int _colour, SCHEME_TYPE _scheme)
{
	colour = _colour;
	//medianScheme = _medianScheme;
	scheme = _scheme;
	debug = 0;
	mosaicG = (MosaicArray<double> *) nullptr;
	mosaicC = (MosaicArray<Y_RGB<double> > *) nullptr;
	mosaicAM = (MosaicArrayMedian *) nullptr;
	dbg_img = nullptr;
	overlap_percent = -1.0;
	final_error = 0.0;
	focal_length = -1.0;
}

void MosaicFramework::SetMosaicSize(int w, int h, double _layer)
{
	if(focal_length<=0.0)
		fprintf(stderr, "Focal length is not set.\n");
	// By default, layer is size/100. May be overwritten manually
	if(colour) {
		switch(scheme) {
			case AVERAGE_SCHEME:
			case MEDIAN_SCHEME:
				mosaicAM = new MosaicArrayMedian(w, h, 11, 3);
				break;
			case CLOSEST_SCHEME:
				mosaicC = new MosaicArray<Y_RGB<double> >(w, h, _layer);
				break;
		}
	} else {
		switch(scheme) {
			case AVERAGE_SCHEME:
			case MEDIAN_SCHEME:
				mosaicAM = new MosaicArrayMedian(w, h, 11, 1);
				break;
			case CLOSEST_SCHEME:
				mosaicG = new MosaicArray<double>(w, h, _layer);
				break;
		}
	}
	mosaicError = new DblImageG(w, h);
}

void MosaicFramework::SetLayerWidth(double _layer)
{
	layer = _layer;
	if(mosaicC) mosaicC->SetLayer(layer);
	if(mosaicG) mosaicG->SetLayer(layer);
}

void MosaicFramework::SetWatch(int x, int y)
{
	if(mosaicC) mosaicC->SetWatch(x, y);
	if(mosaicG) mosaicG->SetWatch(x, y);
	if(mosaicAM) mosaicAM->SetWatch(x, y);
	else fprintf(stderr, "No mosaic set yet.\n");
}

int MosaicFramework::SaveMosaic(const char *name, const char *hits_name)
{
	int status = 0;
	if(name && mosaicG) status += mosaicG->SaveImage(name);
	if(name && mosaicC) status += mosaicC->SaveImage(name);
	if(name && mosaicAM) status += mosaicAM->SaveImage(name, scheme==MEDIAN_SCHEME);
	if(hits_name && mosaicG) status += mosaicG->SaveHits(hits_name);
	if(hits_name && mosaicC) status += mosaicC->SaveHits(hits_name);
	if(hits_name && mosaicAM) status += mosaicAM->SaveHits(hits_name);
	return(status);
}

int MosaicFramework::SaveHits(const char *hits_name)
{
	if(mosaicG) return(mosaicG->SaveHits(hits_name));
	if(mosaicC) return(mosaicC->SaveHits(hits_name));
	if(mosaicAM) return(mosaicAM->SaveHits(hits_name));
	return(-1);
}

int MosaicFramework::SaveError(const char *error_name, int rainbow)
{
	if(rainbow)
		mosaicError->SaveBMPRainbow(error_name);
	else
		mosaicError->SaveBMP(error_name, 0, 0); 
			// No rescale, don't ignore invalid

	// Calculate average per-pixel error - characteristic of mosaic quality
	double sum = 0.0;
	int count = 0;
	for(int j = 0; j<mosaicError->h; j++) {
		for(int i = 0; i<mosaicError->w; i++) {
			if(mosaicError->valid(i, j)) {
				sum += mosaicError->get_pixel(i, j);
				count++;
			}
		}
	}
	cerr << "Average per pixel error: " << sum/double(count) << "\n";
	return(0);
}

BMPImageG* MosaicFramework::ToBMP()
{
	BMPImageG* imgG = nullptr;
	if(mosaicG) imgG = mosaicG->ToBMPG();
	if(mosaicC) imgG = mosaicC->ToBMPG();
	if(mosaicAM) imgG = mosaicAM->ToBMPG(scheme==MEDIAN_SCHEME);
	return(imgG);
}

BMPImageC* MosaicFramework::ToBMPC()
{
	BMPImageC* imgC = nullptr;
	if(mosaicG) imgC = mosaicG->ToBMPC();
	if(mosaicC) imgC = mosaicC->ToBMPC();
	if(mosaicAM) imgC = mosaicAM->ToBMPC(scheme==MEDIAN_SCHEME);
	return(imgC);
}

// Replace 0-value pixels with 1-value.
void MosaicFramework::ValidateMosaicPixels()
{
/*
	double h;
	
	if(!colour) {
		for(int j = 0; j<mosaicG->h; j++) {
			for(int i = 0; i<mosaicG->w; i++) {
				if(mosaicG->Has(i, j) && mosaicG->Norm(i, j)<1.0) {
					h = mosaicG->Hits(i, j);
					mosaicG->lum->set_pixel(i, j, h*1.01); // So after normalization it'll be 1.
				}
					
			}
		}
	}
	if(colour) {
		Y_RGB<double> v;
		for(int j = 0; j<mosaicC->h; j++) {
			for(int i = 0; i<mosaicC->w; i++) {
				if(mosaicC->Has(i, j)==0) continue;
				v = mosaicC->Norm(i, j);
				if(v.ToDbl()<1.0) {
					h = mosaicC->Hits(i, j);
					mosaicC->r->set_pixel(i, j, h*1.01); // So after normalization it'll be 1.
				}	
			}
		}
	}
*/
}

// ####################################################################################################
// New homographies
int MosaicFramework::BlendImage(BMPImage* im, Hmgr& _hm,
	Coord2& start, double trust, char *dif_name)
{
	int status;

	if(im->IsColour()) {
		if(scheme==AVERAGE_SCHEME || scheme==MEDIAN_SCHEME)
			status = BlendImageMedian((BMPImageC*) im, _hm, start, trust, dif_name);
		else
			status = BlendImageClosest((BMPImageC*) im, _hm, start, trust, dif_name);
	} else {
		if(scheme==AVERAGE_SCHEME || scheme==MEDIAN_SCHEME)
			status = BlendImageMedian((BMPImageG*) im, _hm, start, trust, dif_name);
		else
			status = BlendImageClosest((BMPImageG*) im, _hm, start, trust, dif_name);
	}
	return(status);
}

// Note that now 3rd argument is a DIRECT (not inverse) transform.
// Difference with blend_dbl_image() is that frame_image() is calculated
// here, not outside. Should make blend_dbl_image() obsolete.
// Since Aug.2003 returns int to indicate if frame has been used.
int MosaicFramework::BlendImageClosest(BMPImageG *im, Hmgr& hm,
	Coord2& start, double trust, char *dif_name)
{
	Coord2 c, tc;
		c.SetImage(im);
		tc.SetImage(im);
	Coord2 C;
	Y_Rect<int> mos_r = mosaicG->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	double v, s, l_zoom;
	int i, j, ii, jj, vld;
	int count = 0;
	double err = 0.0;
	
	Hmgr inv_hm = hm.Invert();
	
	// Estimate of zoom:
	if(hm.IsRigidAffine()) {
		l_zoom = 1.0/sqrt(hm.h(1,1)*hm.h(1,1)+hm.h(2,1)*hm.h(2,1));
	} else {
		l_zoom = sqrt(2.0)/sqrt(hm.h(1,1)*hm.h(1,1)+hm.h(1,2)*hm.h(1,2)+
			hm.h(2,1)*hm.h(2,1)+hm.h(2,2)*hm.h(2,2));
	}
	
	// No difference calculations yet!
	BMPImageG *dif = nullptr;
	if(dif_name)
		dif = new BMPImageG(mos_r.w, mos_r.h);
		
	Y_Rect<int> r = FrameImage(im, hm);
	Y_Rect<int> im_r = im->GetRect();
	
	// Image of the central point should coinside with 'start'.
	// We go through all the pixels of mosaic inside the given rectangle, (as
	// a quadrilateral, that is image of the frame lies within it)
	// and position them on mosaic with the shift 'start'.
	// Note that forward transform maps point in 2nd image to point in 1st image.
	// Here we use inverse transform, as for each point in 1st image (same as mosaic)
	// we need to find corresponding point in the 2nd image.
	for(j = r.y-1; j<r.y+r.h+1; j++) {
		for(i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Positions in mosaic:
			ii = i+int(start.X()+0.5);
			jj = j+int(start.Y()+0.5);
			C.Set(ii, jj);
			if(mosaicG->CheckWatch(ii, jj))
				vld = 1;
			tc = inv_hm.Map(c);
			if(!tc.Outside(im_r)) {
				// Distance in pixels
				s = tc.FromCenter(im_r);
				//v = tc.pfValue(im, &vld);
				v = tc.Value2(im, &vld);
				// trust<1.0 effectively increases distance from the center
				// and hence reducing chance of pixel being used.
				// The larger the zoom, the wider trustworthy area.
				// zoom>1 effectively reduces distance from the center.
				if(vld && !C.Outside(mos_r)) {
					if(mosaicG->CheckWatch(ii, jj)) 
						mosaicG->watch_print = 1;
					mosaicG->Set(ii, jj, s/(l_zoom*trust), v);
				}
			}
		}
	}
	if(dif) {
		dif->Save(dif_name);
		delete dif;
	}
	if(count) err /= (double) count;
	else err = 0.0;
	//fprintf(stderr, "BlendImage: N=%d  E=%lf\n", count, err);
	
	im->ResetPrefilter();
	return(0);
}

// Note that now 3rd argument is a DIRECT (not inverse) transform.
// Difference with blend_dbl_image() is that frame_image() is calculated
// here, not outside. Should make blend_dbl_image() obsolete.
// Since Aug.2003 returns int to indicate if frame has been used.
int MosaicFramework::BlendImageClosest(BMPImageC *im, Hmgr& hm,
	Coord2& start, double trust, char *dif_name)
{
	Coord2 c, tc; 
		c.SetImage(im);
		tc.SetImage(im);
	Coord2 C; 
	Y_Rect<int> mos_r = mosaicC->GetRect();
		C.SetSize(mos_r.w, mos_r.h);
	Y_RGB<double> v;
	double s, l_zoom;
	int i, j, ii, jj, vld;
	int count = 0;
	double err = 0.0;
	
	Hmgr inv_hm = hm.Invert();
	
	// Estimate of zoom:
	if(hm.IsRigidAffine()) {
		l_zoom = 1.0/sqrt(hm.h(1,1)*hm.h(1,1)+hm.h(2,1)*hm.h(2,1));
	} else {
		l_zoom = sqrt(2.0)/sqrt(hm.h(1,1)*hm.h(1,1)+hm.h(1,2)*hm.h(1,2)+
			hm.h(2,1)*hm.h(2,1)+hm.h(2,2)*hm.h(2,2));
	}
	
	// No difference calculations yet!
	BMPImageG *dif = nullptr;
	if(dif_name)
		dif = new BMPImageG(mos_r.w, mos_r.h);
		
	Y_Rect<int> r = FrameImage(im, hm);
	Y_Rect<int> im_r = im->GetRect();
	
	// Image of the central point should coinside with 'start'.
	// We go through all the pixels of mosaic inside the given rectangle, (as
	// a quadrilateral, that is image of the frame lies within it)
	// and position them on mosaic with the shift 'start'.
	// Note that forward transform maps point in 2nd image to point in 1st image.
	// Here we use inverse transform, as for each point in 1st image (same as mosaic)
	// we need to find corresponding point in the 2nd image.
	for(j = r.y-1; j<r.y+r.h+1; j++) {
		for(i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Positions in mosaic:
			ii = i+int(start.X()+0.5);
			jj = j+int(start.Y()+0.5);
			C.Set(ii, jj);
			if(mosaicC->CheckWatch(ii, jj))
				vld = 1;
			tc = inv_hm.Map(c);
			if(!tc.Outside(im_r)) {
				// Distance in pixels
				s = tc.FromCenter(im_r);
				//v = tc.pfValue(im, &vld);
				v = tc.Value2(im, &vld);
				if(vld && !C.Outside(mos_r)) {
					if(mosaicC->CheckWatch(ii, jj)) 
						mosaicC->watch_print = 1;
					mosaicC->Set(ii, jj, s/(l_zoom*trust), v);
				}
			}
		}
	}
	if(dif) {
		dif->Save(dif_name);
		delete dif;
	}
	if(count) err /= (double) count;
	else err = 0.0;
	//fprintf(stderr, "BlendImage: N=%d  E=%lf\n", count, err);
	return(0);
}

//int MosaicFramework::BlendImage(DblImageG *im, Hmgr& hm,
//	Coord2& start, double trust, char *dif_name)
//{
//	fprintf(stderr, "Not implemenated yet.\n");
//	return(0);
//}

int MosaicFramework::BlendImageMedian(BMPImageG *im, Hmgr& hm,
	Coord2& start, double trust, char *dif_name)
{
	Coord2 c, tc;
		c.SetImage(im);
		tc.SetImage(im);
	Coord2 C;
	Y_Rect<int> mos_r = mosaicAM->GetRect();
	C.SetSize(mos_r.w, mos_r.h);
	double v;
	int i, j, ii, jj, vld;
	int count = 0;
	double err = 0.0;
	
	Hmgr inv_hm = hm.Invert();
	
	// No difference calculations yet!
	BMPImageG *dif = nullptr;
	if(dif_name)
		dif = new BMPImageG(mos_r.w, mos_r.h);
		
	Y_Rect<int> r = FrameImage(im, hm);
	Y_Rect<int> im_r = im->GetRect();
	
	// Image of the central point should coinside with 'start'.
	// We go through all the pixels of mosaic inside the given rectangle, (as
	// a quadrilateral, that is image of the frame lies within it)
	// and position them on mosaic with the shift 'start'.
	// Note that forward transform maps point in 2nd image to point in 1st image.
	// Here we use inverse transform, as for each point in 1st image (same as mosaic)
	// we need to find corresponding point in the 2nd image.
	for(j = r.y-1; j<r.y+r.h+1; j++) {
		for(i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Positions in mosaic:
			ii = i+int(start.X()+0.5);
			jj = j+int(start.Y()+0.5);
			C.Set(ii, jj);
			if(mosaicAM->CheckWatch(ii, jj))
				vld = 1;
			tc = inv_hm.Map(c);
			if(!tc.Outside(im_r)) {
				//v = tc.pfValue(im, &vld);
				v = tc.Value2(im, &vld);
				// trust<1.0 effectively increases distance from the center
				// and hence reducing chance of pixel being used.
				// The larger the zoom, the wider trustworthy area.
				// zoom>1 effectively reduces distance from the center.
				if(vld && !C.Outside(mos_r)) {
					if(mosaicAM->CheckWatch(ii, jj)) 
						mosaicAM->watch_print = 1;
					mosaicAM->Set(ii, jj, (u_char) v);
				}
			}
		}
	}
	if(dif) {
		dif->Save(dif_name);
		delete dif;
	}
	if(count) err /= (double) count;
	else err = 0.0;
	//fprintf(stderr, "BlendImage: N=%d  E=%lf\n", count, err);
	
	im->ResetPrefilter();
	return(0);
}

// Note that now 3rd argument is a DIRECT (not inverse) transform.
// Difference with blend_dbl_image() is that frame_image() is calculated
// here, not outside. Should make blend_dbl_image() obsolete.
// Since Aug.2003 returns int to indicate if frame has been used.
int MosaicFramework::BlendImageMedian(BMPImageC *im, Hmgr& hm,
	Coord2& start, double trust, char *dif_name)
{
	Coord2 c, tc; 
		c.SetImage(im);
		tc.SetImage(im);
	Coord2 C; 
	Y_Rect<int> mos_r = mosaicAM->GetRect();
	C.SetSize(mos_r.w, mos_r.h);
	C.SetSize(mos_r.w, mos_r.h);
	Y_RGB<double> v;
	Y_RGB<u_char> v_char;
	int i, j, ii, jj, vld;
	int count = 0;
	double err = 0.0;
	
	Hmgr inv_hm = hm.Invert();
	
	// No difference calculations yet!
	BMPImageG *dif = nullptr;
	if(dif_name)
		dif = new BMPImageG(mos_r.w, mos_r.h);
		
	Y_Rect<int> r = FrameImage(im, hm);
	Y_Rect<int> im_r = im->GetRect();
	
	// Image of the central point should coinside with 'start'.
	// We go through all the pixels of mosaic inside the given rectangle, (as
	// a quadrilateral, that is image of the frame lies within it)
	// and position them on mosaic with the shift 'start'.
	// Note that forward transform maps point in 2nd image to point in 1st image.
	// Here we use inverse transform, as for each point in 1st image (same as mosaic)
	// we need to find corresponding point in the 2nd image.
	for(j = r.y-1; j<r.y+r.h+1; j++) {
		for(i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Positions in mosaic:
			ii = i+int(start.X()+0.5);
			jj = j+int(start.Y()+0.5);
			C.Set(ii, jj);
			if(mosaicAM->CheckWatch(ii, jj))
				vld = 1;
			tc = inv_hm.Map(c);
			if(!tc.Outside(im_r)) {
				//v = tc.pfValue(im, &vld);
				v = tc.Value2(im, &vld);
				if(vld && !C.Outside(mos_r)) {
					if(mosaicAM->CheckWatch(ii, jj)) 
						mosaicAM->watch_print = 1;
					v_char.Set((u_char)v.p[0], (u_char)v.p[1], (u_char)v.p[2]);
					mosaicAM->Set(ii, jj, v_char);
				}
			}
		}
	}
	if(dif) {
		dif->Save(dif_name);
		delete dif;
	}
	if(count) err /= (double) count;
	else err = 0.0;
	//fprintf(stderr, "BlendImage: N=%d  E=%lf\n", count, err);
	return(0);
}


// Returns average error per pixel.
// Now supersedes ErrorVerbose, as setting 'verbose=1' gives this functionality.
// Non-mandatory arguments:
// Provision of 'rec' fills in its members (overlap, aver.error, max error).
double MosaicFramework::TransError(BMPImageG *im1, BMPImageG *im2, Hmgr& _hm, 
	TransformRecord *rec, int verbose)
{
	Y_Rect<int> r(100000, 100000, -100000, -100000);
	Coord2 c; 
		c.SetImage(im2); // Point in the 2nd image
	Coord2 tc; 
		tc.SetImage(im1); // Point in the 1st image
	double v_c, v_tc, v_dif;
	int vld_c, vld_tc;

	BMPImageG *dif = nullptr;
	if(verbose>1)
		dif = new BMPImageG(im1->w, im1->h);
	Y_Rect<int> r_1 = im1->GetRect();
	Y_Rect<int> r_2 = im2->GetRect();

	double totalError = 0.0; int count = 0, all_pixels = 0;
	double maxError = 0.0;
	for(int j = 0; j<im2->h; j++) { // Loop through pixels in the 2nd image
		for(int i = 0; i<im2->w; i++) {
			c.Set(i, j);
			if(c.Outside(r_2)) continue; // Could this ever happen?
			// Find trasformation of the point - location in 1st image
			tc = _hm.Map(c);
			//v_c = c.pfValue(im2, &vld_c);
			v_c = c.Value2(im2, &vld_c);
			if(!vld_c) continue;
			all_pixels++; // Count all valid pixels in 2nd image
			//v_tc = tc.pfValue(im1, &vld_tc);
			v_tc = tc.Value2(im1, &vld_tc);
			if(!tc.Outside(r_1) && vld_tc) {
				v_dif = fabs(v_tc-v_c);
				if(v_dif>maxError)
					maxError = v_dif;
				totalError += v_dif;
				count++; // Count all pixels in overlap
				if(verbose>1) {
					r.x = min(r.x, int(tc.X())); r.y = min(r.y, int(tc.Y()));
					r.w = max(r.w, int(tc.X())); r.h = max(r.h, int(tc.Y()));
					if(dif) dif->set_pixel(int(tc.X()), int(tc.Y()), v_dif);
				}
			}
		}
	}
	if(verbose>2) {
		// Reinstall meanings of r.w/h
		r.w -= r.x-1; r.h -= r.y-1;
		fprintf(stderr, "1st image overlap rect (fwd): X=%d  Y=%d  W=%d  H=%d\n", 
			r.x, r.y, r.w, r.h);
		if(dif) {
			BMPImageG *dif_sub = dif->CutSubimage(r);
			dif_sub->Save("ErrorDif.bmp");
			delete dif_sub;
		}
	}
	if(dif)
		delete dif;
	if(verbose) {
		fprintf(stderr, "Error=%lf  Count=%d  Total=%.2lf  MaxError=%.2lf  Overlap=%.2lf\n", 
			totalError/count, count, totalError, maxError, 100.0*count/all_pixels);
	}
// fprintf(stderr, ">>> %d %lf\n", count, totalError);
	//overlap_percent = 100.0*double(count)/double(im1->w*im1->h);
	if(rec) { // Fill in fields in record
		rec->overlap_percent = 100.0*count/all_pixels;
		rec->av_error = (count) ? totalError/count : DBL_MAX;
		rec->max_error = maxError;
	}
	return((count) ? totalError/count : DBL_MAX);
}

// Same for colour images
double MosaicFramework::TransError(BMPImageC *im1, BMPImageC *im2, Hmgr& _hm, 
	TransformRecord *rec, int verbose)
{
	Y_Rect<int> r(100000, 100000, -100000, -100000);
	Coord2 c; 
		c.SetImage(im2); // Point in the 2nd image
	Coord2 tc; 
		tc.SetImage(im1); // Point in the 1st image
	Y_RGB<double> v_c, v_tc, v_dif;
	double dif;
	int vld_c, vld_tc;

	Y_Rect<int> r_1 = im1->GetRect();
	Y_Rect<int> r_2 = im2->GetRect();

	double totalError = 0.0; int count = 0, all_pixels = 0;
	double maxError = 0.0;
	for(int j = 0; j<im2->h; j++) { // Loop through pixels in the 2nd image
		for(int i = 0; i<im2->w; i++) {
			c.Set(i, j);
			if(c.Outside(r_2)) continue; // Could this ever happen?
			// Find trasformation of the point - location in 1st image
			tc = _hm.Map(c);
			v_c = c.Value2(im2, &vld_c);
			if(!vld_c) continue;
			all_pixels++; // Count all valid pixels in 2nd image
			v_tc = tc.Value2(im1, &vld_tc);
			if(!tc.Outside(r_1) && vld_tc) {
				v_dif = v_tc-v_c;
				dif = v_dif.ToDbl()/3.0;
				if(dif>maxError)
					maxError = dif;
				totalError += dif;
				count++; // Count all pixels in overlap
			}
		}
	}
	if(verbose) {
		fprintf(stderr, "Error=%lf  Count=%d  Total=%.2lf  MaxError=%.2lf  Overlap=%.2lf\n", 
			totalError/count, count, totalError, maxError, 100.0*count/all_pixels);
	}
// fprintf(stderr, ">>> %d %lf\n", count, totalError);
	//overlap_percent = 100.0*double(count)/double(im1->w*im1->h);
	if(rec) { // Fill in fields in record
		rec->overlap_percent = 100.0*count/all_pixels;
		rec->av_error = (count) ? totalError/count : DBL_MAX;
		rec->max_error = maxError;
	}
	return((count) ? totalError/count : DBL_MAX);
}

// Returns overlap in percent.
double MosaicFramework::Overlap(BMPImageG *im1, BMPImageG *im2, Hmgr& _hm)
{
	Coord2 c; // Point in the 2nd image
		c.SetImage(im2); // Point in the 2nd image
	Coord2 tc; // Point in the 1st image
		tc.SetImage(im1); // Point in the 1st image

	int count = 0, all_pixels = 0;
	for(int j = 0; j<im2->h; j++) { // Loop through pixels in the 2nd image
		for(int i = 0; i<im2->w; i++) {
			if(im2->valid(i, j)==0) continue;
			all_pixels++; // Count all valid pixels in 2nd image
			c.Set(i, j);
			// Find trasformation of the point - location in 1st image
			tc = _hm.Map(c);
			if(im1->valid(int(tc.X()), int(tc.Y())))
				count++; // Count all pixels in overlap
		}
	}
	double overlap_percent = 100.0*count/all_pixels;
	return(overlap_percent);
}

// Return 4 corners of the projection for a given image and homography
void MosaicFramework::FrameCorners(BMPImage *img, Hmgr& _hm, Coord2 c[])
{
	Coord2 corner;
		corner.SetImage(img);
	
	corner.Set(0, 0);               c[0] = _hm.Map(corner);
	corner.Set(img->w-1, 0);        c[1] = _hm.Map(corner);
	corner.Set(0, img->h-1);        c[2] = _hm.Map(corner);
	corner.Set(img->w-1, img->h-1); c[3] = _hm.Map(corner);
}

// Return 4 corners of the projection for a given image and homography
void MosaicFramework::FrameCorners(Y_Rect<int> r, double focal_length, 
	Hmgr& _hm, Coord2 c[])
{
	Coord2 corner;
		corner.SetSize(r.w, r.h);
		corner.SetFocalLength(focal_length);
	
	corner.Set(r.x, r.y);     c[0] = _hm.Map(corner);
	corner.Set(r.w-1, r.y);   c[1] = _hm.Map(corner);
	corner.Set(r.x, r.h-1);   c[2] = _hm.Map(corner);
	corner.Set(r.w-1, r.h-1); c[3] = _hm.Map(corner);
}

// Find quadrilateral - image of the frame with given transformation.
// Size of the mosaic (where image of the frame goes) is not involved.
// If the transform is unit, frame gives image coinsiding with itself.
Y_Rect<int> MosaicFramework::FrameImage(Y_Image *img, Hmgr& _hm)
{
	Y_Rect<double> R(100000.0, 100000.0, -100000.0, -100000.0);
	// R.w and R.h have meaning of top limits here.
	
	Coord2 corner, corner_image;
		corner.SetImage(img);
	
	corner.Set(0, 0); corner_image = _hm.Map(corner);
	R.Encompass(corner_image);
	corner.Set(img->w-1, 0); corner_image = _hm.Map(corner);
	R.Encompass(corner_image);
	corner.Set(0, img->h-1); corner_image = _hm.Map(corner);
	R.Encompass(corner_image);
	corner.Set(img->w-1, img->h-1); corner_image = _hm.Map(corner);
	R.Encompass(corner_image);
	// Reinstall meanings of R.w/h
	Y_Rect<int> Ri(int(R.x), int(R.y), int(R.w-R.x+1.0), int(R.h-R.y+1.0));
	//R.w -= R.x-1; R.h -= R.y-1;
	return(Ri);
}

// Same as above, but instead of image, it's corners are passed as argument
Y_Rect<int> MosaicFramework::FrameImage(Y_Rect<int> r, double focal_length, Hmgr& _hm)
{
	Y_Rect<int> R(100000, 100000, -100000, -100000);
	// R.w and R.h have meaning of top limits here.
	int x, y;
	
	Coord2 corner, corner_image;
		corner.SetFocalLength(focal_length);
	
	corner.Set(r.x, r.y); corner_image = _hm.Map(corner);
	x = CEIL(corner_image.X()); y = CEIL(corner_image.Y());
	R.x = min(R.x, x); R.y = min(R.y, y);
	R.w = max(R.w, x); R.h = max(R.h, y);
	corner.Set(r.w-1, r.y); corner_image = _hm.Map(corner);
	x = CEIL(corner_image.X()); y = CEIL(corner_image.Y());
	R.x = min(R.x, x); R.y = min(R.y, y);
	R.w = max(R.w, x); R.h = max(R.h, y);
	corner.Set(r.x, r.h-1); corner_image = _hm.Map(corner);
	x = CEIL(corner_image.X()); y = CEIL(corner_image.Y());
	R.x = min(R.x, x); R.y = min(R.y, y);
	R.w = max(R.w, x); R.h = max(R.h, y);
	corner.Set(r.w-1, r.h-1); corner_image = _hm.Map(corner);
	x = CEIL(corner_image.X()); y = CEIL(corner_image.Y());
	R.x = min(R.x, x); R.y = min(R.y, y);
	R.w = max(R.w, x); R.h = max(R.h, y);
	// Reinstall meanings of R.w/h
	R.w -= R.x-1; R.h -= R.y-1;
	return(R);
}

BMPImageG *MosaicFramework::ApplyTransform(BMPImageG *img, Hmgr &hm)
{
	Coord2 c, tc, C;
		c.SetImage(img);
		tc.SetImage(img);
	int i, j, ii, jj, vld;
	double v;

	// Find images of frame corners
	Y_Rect<int> r = FrameImage(img, hm);
	// Make sure sizes are even
	r.w += (r.w%2);
	r.h += (r.h%2);
	//if(debug_here) cout << "Frame: " << r << endl;
	
	BMPImageG *img_out = new BMPImageG(r.w, r.h);
		img_out->SetFocalLength(img->focal_length);
		C.SetImage(img_out);

	Hmgr inv_hm = hm.Invert();
	Y_Rect<int> r_img = img->GetRect();
	Y_Rect<int> r_img_out = img_out->GetRect();

	// For each resulting pixel within rectangle, find corr. pixel in the image
	for(j = 0; j<r.h; j++) {
		for(i = 0; i<r.w; i++) {
			// Position in resulting image:
			ii = i+r.x;
			jj = j+r.y;
			c.Set(ii, jj);
			C.Set(i, j);
			tc = inv_hm.Map(c);
			if(!tc.Outside(r_img) && !C.Outside(r_img_out)) {
				//v = tc.pfValue(img, &vld);
				v = tc.Value2(img, &vld);
				if(vld) img_out->set_pixel(i, j, D_LIMIT_RANGE_CHAR(v));
			}
		}
	}
	img->ResetPrefilter();
	return(img_out);
}

BMPImageC *MosaicFramework::ApplyTransform(BMPImageC *img, Hmgr& hm)
{
	Coord2 c, tc, C;
		c.SetImage(img);
		tc.SetImage(img);
	int i, j, ii, jj, vld;
	Y_RGB<double> v;

	// Find images of frame corners
	Y_Rect<int> r = FrameImage(img, hm);
	// Make sure sizes are even
	r.w += (r.w%2);
	r.h += (r.h%2);
	//if(debug_here) cout << "Frame: " << r << endl;
	
	BMPImageC *img_out = new BMPImageC(r.w, r.h);
		img_out->SetFocalLength(img->focal_length);
		C.SetImage(img_out);
	
	Hmgr inv_hm = hm.Invert();
	Y_Rect<int> r_img = img->GetRect();
	Y_Rect<int> r_img_out = img_out->GetRect();

	// For each resulting pixel within rectangle, find corr. pixel in the image
	for(j = 0; j<r.h; j++) {
		for(i = 0; i<r.w; i++) {
			// Position in resulting image:
			ii = i+r.x;
			jj = j+r.y;
			c.Set(ii, jj);
			C.Set(i, j);
			tc = inv_hm.Map(c);
			if(!tc.Outside(r_img) && !C.Outside(r_img_out)) {
				v = tc.Value2(img, &vld);
				if(vld) img_out->set_pixel(i, j, v);
			}
		}
	}
	return(img_out);
}

void MosaicFramework::Transform(BMPImageG *dest, BMPImageG *src, Hmgr& hm, Coord2& start)
{
	Coord2 c, tc;
		c.SetImage(src);
		tc.SetImage(src);
	Coord2 C;
		C.SetImage(dest);
	double v;
	int i, j, ii, jj, vld;
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> r = FrameImage(src, hm);
	Y_Rect<int> src_r = src->GetRect();
	Y_Rect<int> dest_r = dest->GetRect();
	
	// Image of the central point should coinside with 'start'.
	// We go through all the pixels of mosaic inside the given rectangle, (as
	// a quadrilateral, that is image of the frame lies within it)
	// and position them on mosaic with the shift 'start'.
	// Note that forward transform maps point in 2nd image to point in 1st image.
	// Here we use inverse transform, as for each point in 1st image (same as mosaic)
	// we need to find corresponding point in the 2nd image.
	for(j = r.y-1; j<r.y+r.h+1; j++) {
		for(i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Positions in mosaic:
			ii = i+int(start.X()+0.5);
			jj = j+int(start.Y()+0.5);
				if(dest->n_watch && dest->CheckWatch(ii, jj))
					vld = 0;
			C.Set(ii, jj);
			tc = inv_hm.Map(c);
			if(!tc.Outside(src_r)) {
				//v = tc.pfValue(src, &vld);
				v = tc.Value2(src, &vld);
				if(vld && !C.Outside(dest_r))
					dest->set_pixel(ii, jj, v);
			}
		}
	}
	src->ResetPrefilter();
}

void MosaicFramework::Transform(BMPImageC *dest, BMPImageC *src, Hmgr& hm, Coord2& start)
{
	Coord2 c, tc;
		c.SetImage(src);
		tc.SetImage(src);
	Coord2 C;
		C.SetImage(dest);
	Y_RGB<double> v;
	int i, j, ii, jj, vld;
	
	Hmgr inv_hm = hm.Invert();
	
	Y_Rect<int> r = FrameImage(src, hm);
	Y_Rect<int> src_r = src->GetRect();
	Y_Rect<int> dest_r = dest->GetRect();
	
	// Image of the central point should coinside with 'start'.
	// We go through all the pixels of mosaic inside the given rectangle, (as
	// a quadrilateral, that is image of the frame lies within it)
	// and position them on mosaic with the shift 'start'.
	// Note that forward transform maps point in 2nd image to point in 1st image.
	// Here we use inverse transform, as for each point in 1st image (same as mosaic)
	// we need to find corresponding point in the 2nd image.
	for(j = r.y-1; j<r.y+r.h+1; j++) {
		for(i = r.x-1; i<r.x+r.w+1; i++) {
			c.Set(i, j);
			// Positions in mosaic:
			ii = i+int(start.X()+0.5);
			jj = j+int(start.Y()+0.5);
				if(dest->n_watch && dest->CheckWatch(ii, jj))
					vld = 0;
			C.Set(ii, jj);
			tc = inv_hm.Map(c);
			if(!tc.Outside(src_r)) {
				//v = tc.pfValue(src, &vld);
				v = tc.Value2(src, &vld);
				if(vld && !C.Outside(dest_r))
					dest->set_pixel(ii, jj, v);
			}
		}
	}
}
