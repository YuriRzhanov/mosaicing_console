#ifndef _MOSAICFRAMEWORK_H_DEFINED_
#define _MOSAICFRAMEWORK_H_DEFINED_

#include <io.h>
#include "MosaicArray.h"
#include "MosaicArrayMedian.h"
#include "Transform.h"

// Mosaic-building framework (functionality moved from OptFramework)
// Could be built of float, double, Y_RGB<float> or Y_RGB<double>
// 'float' versions could be used for lower accuracy?
// 'singleton' versions are for greyscale mosaics
// 'triplet' versions for colour mosaics

class MosaicFramework {
	public:
		// Average and median are built in the same way, but have different flattening procedures.
		typedef enum { AVERAGE_SCHEME = 0, MEDIAN_SCHEME = 1, CLOSEST_SCHEME = 2 } SCHEME_TYPE;
		MosaicFramework(int colour, SCHEME_TYPE _scheme);
		~MosaicFramework() 
			{ 
				if(mosaicG) delete mosaicG; 
				if(mosaicC) delete mosaicC; 
				if(mosaicAM) delete mosaicAM;
				delete mosaicError;
			}
		void SetFocalLength(double fl) { focal_length = fl; } // needed?
		void GetMosaicSize(int &_w, int &_h) 
			{ 
				if(mosaicC) { _w = mosaicC->w; _h = mosaicC->h; }
				if(mosaicG) { _w = mosaicG->w; _h = mosaicG->h; }
				if(mosaicAM) { _w = mosaicAM->w; _h = mosaicAM->h; }
			}
		Y_Rect<int> GetMosaicSize() 
			{
				if(mosaicC) return(Y_Rect<int>(0, 0, mosaicC->w, mosaicC->h));
				if(mosaicG) return(Y_Rect<int>(0, 0, mosaicG->w, mosaicG->h));
				if(mosaicAM) return(Y_Rect<int>(0, 0, mosaicAM->w, mosaicAM->h));
			}
		//void set_weight_scheme(int scheme, double sigma) 
		//	{weight_scheme = scheme; weight_sigma = sigma;}
		//static int make_name(char *name, int n, char *base, char *ext);
		//static char *create_name(char *base, int n, char *ext);
		void SetMosaicSize(int w, int h, double _layer = 0.0);
		void SetLayerWidth(double _layer);
		void SetWatch(int x, int y);
		void SetWatch(Point2<int> p) { SetWatch(p.X(), p.Y()); }
		int SaveMosaic(const char *name = nullptr, const char *hits_name = nullptr);
		int SaveHits(const char *hits_name);
		int SaveError(const char *error_name, int rainbow = 0);
		BMPImageG* ToBMP();
		BMPImageC* ToBMPC();
		void ValidateMosaicPixels(); // Replace 0-value pixels with 1-value.

		// New homographies
		// Common interface working for colour/greyscale and median/closest
		int BlendImage(BMPImage* im, Hmgr& _hm,
			Coord2& start, double trust=1.0, char *dif_name = nullptr);
		int BlendImageClosest(BMPImageG *im, Hmgr& _hm,
			Coord2& start, double trust=1.0, char *dif_name = nullptr);
		int BlendImageClosest(BMPImageC *im, Hmgr& _hm,
			Coord2& start, double trust=1.0, char *dif_name = nullptr);
		//int BlendImageClosest(DblImageG *im, Hmgr& _hm,
		//	Coord2& start, double trust=1.0, char *dif_name = nullptr);
		int BlendImageMedian(BMPImageG *im, Hmgr& _hm,
			Coord2& start, double trust=1.0, char *dif_name = nullptr);
		int BlendImageMedian(BMPImageC *im, Hmgr& _hm,
			Coord2& start, double trust=1.0, char *dif_name = nullptr);

		static double TransError(BMPImageG *im1, BMPImageG *im2, Hmgr& _hm,
			TransformRecord *rec=nullptr, int verbose=0);
		static double TransError(BMPImageC *im1, BMPImageC *im2, Hmgr& _hm,
			TransformRecord *rec=nullptr, int verbose=0);
		static double Overlap(BMPImageG *im1, BMPImageG *im2, Hmgr& _hm);
		static void FrameCorners(BMPImage *img, Hmgr& _hm, Coord2 c[]);
		static void FrameCorners(Y_Rect<int> r, double focal_length, 
			Hmgr& _hm, Coord2 c[]);
		static Y_Rect<int> FrameImage(Y_Image *img, Hmgr& _hm);
		static Y_Rect<int> FrameImage(Y_Rect<int> r, double focal_length, Hmgr& _hm);
		// The following return transformed image, compute rectangle internally
		static BMPImageG *ApplyTransform(BMPImageG *src, Hmgr &hm);
		static BMPImageC *ApplyTransform(BMPImageC *src, Hmgr &hm);
		// Same as BlendDblImage(0, without blending, just replacing pixels
		static void Transform(BMPImageG *dest, BMPImageG *src, Hmgr &hm, Coord2& start);
		static void Transform(BMPImageC *dest, BMPImageC *src, Hmgr &hm, Coord2& start);

protected:
		//bool medianScheme; // 'true' if median scheme, 'false' if closest-pixel scheme - obsolete
		SCHEME_TYPE scheme;
		int weight_scheme;
		double weight_sigma;
		BMPImageG *dbg_img;
	public:
		int colour, debug;
		double layer; // Transition layer for mosaic
		MosaicArray<double> *mosaicG;
		MosaicArray<Y_RGB<double> > *mosaicC;
		MosaicArrayMedian *mosaicAM; // For average and median schemes
		DblImageG *mosaicError;
		double focal_length;
		double overlap_percent, final_error;
};

//#include "MosaicFramework.cpp"

#endif

