#ifndef _TRUSTCOEF_INCLUDED_
#define _TRUSTCOEF_INCLUDED_

#include <stdio.h>

class TrustCoef {
public:
	TrustCoef() { nEntries = 0; fail = 0; entries = NULL; }
	TrustCoef(char *file) {
		nEntries = 0; fail = 0; entries = NULL;
		FILE *fp;
		if(fopen_s(&fp, file, "r")) {
			fail = 1;
		} else {
			char stg[256];
			while(fgets(stg, 255, fp) && strlen(stg)>2) nEntries++;
			rewind(fp);
			entries = new char*[nEntries];
			nEntries = 0;
			while(fgets(stg, 255, fp) && strlen(stg)>2) {
				entries[nEntries] = new char[strlen(stg)+1];
				strcpy_s(entries[nEntries], strlen(stg)+1, stg);
				nEntries++;
			}
		}
	}
	~TrustCoef() {
		if(entries) {
			for(int i = 0; i<nEntries; i++) {
				if(entries[i]) delete [] entries[i];
			}
			delete [] entries;
		}
	}
	double Weight(int f_n) {
		int k_from, k_to;
		double x;
		for(int i = 0; i<nEntries; i++) { // Check all entries
			if(strchr(entries[i], ':')) { // range given
				sscanf_s(entries[i], "%d:%d %lf", &k_from, &k_to, &x);
				if(f_n>=k_from && f_n<=k_to) return(x);
			} else {
				sscanf_s(entries[i], "%d %lf", &k_from, &x);
				if(f_n==k_from) return(x);
			}

		}
		return(1.0);
	}
public:
	int nEntries, fail;
	char **entries;
};

#endif
