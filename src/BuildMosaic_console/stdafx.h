// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include "PixelC.h"
#include "Hmgr.h"
#include "RAR.h"
#include "Transform.h"
#include "LibYMovie64.h"
#include "TrustCoef.h"
#include "MosaicFramework.h"
#include "LibPolygon.h"

typedef enum { AVERAGE_SCHEME = 0, MEDIAN_SCHEME = 1, CLOSEST_SCHEME = 2, GRAPHCUT_SCHEME = 3 } SCHEME_TYPE;

// Values used to indicate source in graph cut:
#define GC_VALID_MAX ((u_char)252)
#define GC_NEITHER ((u_char)253)
#define GC_MOSAIC ((u_char)254)
#define GC_FRAME ((u_char)255)

Y_RGB<u_char> Frame2Colour(int frame_n);
void DrawOutline(BMPImageC *o, int w, int h, Hmgr &hm, 
		Coord2 start, Y_RGB<u_char> clr, double focal_length);
int ReadRelativeRRA(char* pRRAFileName, Hmgr startHmgr, int numAnchor, bool noZoom, TransformRecord*& pWorldtrans, 
    double& focalLength, Y_Rect<int>& frameSize);
TransformRecord* RRA2Trans(RARecord rel_recs[], int N_records, double focal_length, bool no_zoom); // From Utilities
int ComputeWorldTransforms(TransformRecord *rel_trans, int N_links,
	TransformRecord *world_trans, int N_max_frames,
	int anchor_frame_n, Hmgr& anchor_hm, double focal_length,
	char* anchor_mark = nullptr, FILE* fp_debug = nullptr); // From Utilities
void EstimateSizeAndPosition(int N_frames, TransformRecord* pWorldtrans, int frameFrom, int frameTo,
    int numAnchor, Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, Point2<int>& anchorPosition);

bool FrameOverlapsCanvas(Y_Rect<int>& mosaic_size, Y_Image* im, Hmgr& hm, Coord2& start);
int EstimateExtentAndAnchorPosition(TransformRecord* worldtrans, int N_frames,
		int anchor_frame_n, Y_Rect<int>& frame_size, double focal_length,
		Y_Rect<int>& mosaic_size, Point2<int>& anchor_position);
void BuildBlock(SCHEME_TYPE schemeType, Y_Movie64* pAVI, int N_frames, TransformRecord* pWorldtrans, int numAnchor, 
    Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, 
    Point2<int>& anchorPosition, TrustCoef* pTrustCoef, Y_RGB<u_char> invalidColor, BMPImageG* pValidMask, 
    char* pMosaicName, char* block_id, bool whiteBackground, bool whole_mosaic);
void BuildBlock_Graphcut(Y_Movie64* pAVI, int N_frames, TransformRecord* pWorldtrans, int numAnchor, 
    Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, 
    Point2<int>& anchorPosition, TrustCoef* pTrustCoef, Y_RGB<u_char> invalidColor, BMPImageG* pValidMask,
    char* pMosaicName, char* block_id, bool whiteBackground, bool whole_mosaic);
void BuildBlock_GraphcutMin(Y_Movie64* pAVI, int N_frames, TransformRecord* pWorldtrans, int numAnchor, 
    Y_Rect<int> frameSize, double focalLength, Y_Rect<int>& mosaicSize, 
    Point2<int>& anchorPosition, TrustCoef* pTrustCoef, Y_RGB<u_char> invalidColor, BMPImageG* pValidMask,
    char* pMosaicName, char* block_id, bool whiteBackground, bool whole_mosaic);
