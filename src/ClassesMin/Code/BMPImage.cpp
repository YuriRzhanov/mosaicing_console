#include <stdlib.h>
#include <math.h>
#include "BMPImage.h"
#if defined(WIN32) || defined(WIN64)
#include <windows.h> // Contains BI_* constants
#else
#define BI_RGB  0
#define BI_RLE8 1
#define BI_RLE4 2
#endif

using namespace std;
char Y_Image::error[256];

Y_Image::Y_Image() 
	: w(0), h(0), fail(0), focal_length(-1.0), 
	conf_set(0), debug(0), prefilter(0), n_watch(0)
{ 
	get_endian();
}

Y_Image::Y_Image(int _w, int _h)
	: w(_w), h(_h), fail(0), focal_length(-1.0), 
	conf_set(0), debug(0), prefilter(0), n_watch(0)
{ 
	get_endian();
}

Y_Image::Y_Image(int _w, int _h, double f_length)
	: w(_w), h(_h), fail(0), focal_length(f_length), 
	conf_set(0), debug(0), prefilter(0), n_watch(0)
{ 
	get_endian();
}

// Find out image type from magic
Y_Image::IMAGE_TYPE Y_Image::DetermineType(const char* name)
{
	FILE *fi;
	if(fopen_s(&fi, name, "rb")) {
		sprintf_s(error, 256, "Cannot open %s\n", name);
		return(UNKNOWN_TYPE);
	}
	char magic[5];
	if(fread(magic, 1, 4, fi)!=4) {
		sprintf_s(error, 256, "Cannot read %s\n", name);
		return(UNKNOWN_TYPE);
	}
	fclose(fi);
	if(strncmp(magic, "BM", 2)==0)
		return(BMP_TYPE);
	if(strncmp(magic, "YINT", 4)==0)
		return(INT_TYPE);
	if(strncmp(magic, "YFLT", 4)==0)
		return(FLT_TYPE);
	if(strncmp(magic, "YDBL", 4)==0)
		return(DBL_TYPE);
	return(UNKNOWN_TYPE);
}

bool Y_Image::valid(double x, double y)
{
	// Outside values are not valid
	if(x<0.0 || x>double(w-1) || y<0.0 || y>double(h-1))
		return(false);
	return(valid((int)x, (int)y));
}

double Y_Image::pf_tau = 0.211325;

/*
Solves matrix equation [a].[z] = [b] for the 'z's. 
'a' is a sqare matrix of order 'n'. The 'z's are returned in [z]
*/
void Y_Image::gausselim(double a[MAXN][MAXN], double z[MAXN], 
	double b[MAXN], int n)
{
	int	temp1,temp2,r,row,col,j,p;
	double	t,bs,ma;

	for(col=1; col<n; col++) {
		r = 0;
		ma = fabs(a[col][col]);
		for(row=col; row<=n; row++) {
/* Find largest absolute element in row for pivot  */
			if (fabs(a[row][col]) > ma) {
				ma = fabs(a[row][col]);
				r = row;
			}
		}
		if (r != 0) {
			for(j=col; j<=n; j++) {
				t = a[col][j];
				a[col][j] = a[r][j];
				a[r][j] = t;
			}
			t = b[col];
			b[col] = b[r];
			b[r] = t;
		}

/* get row multipliers */
		for(row=(col+1); row<=n; row++)
			z[row] = -a[row][col]/a[col][col];

/* eliminate h */
		for(row=(col+1); row<=n; row++) {
			for(j=col; j<=n; j++)
				a[row][j] = a[col][j] * z[row] + a[row][j];
			b[row] = b[col] * z[row] + b[row];
		}
	}

/* back substitution */
	z[n] = b[n]/a[n][n];
	temp1 = n-1;
	for(p=temp1; p>=1; p--) {
		bs = 0;
		temp2 = p+1;
		for(r=n; r>=temp2; r--)
			bs = bs + a[p][r] * z[r];
		z[p] = (b[p]-bs)/a[p][p];
	}
}

void Y_Image::InitBicubicMatrix()
{
	M_bicubic.ReSize(16,16);
	M_bicubic<<1<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<
    0<<0<<0<<0<<1<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<
    -3<<3<<0<<0<<-2<<-1<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<< 
    2<<-2<<0<<0<<1<<1<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<
    0<<0<<0<<0<<0<<0<<0<<0<<1<<0<<0<<0<<0<<0<<0<<0<<
    0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<0<<1<<0<<0<<0<<
    0<<0<<0<<0<<0<<0<<0<<0<<-3<<3<<0<<0<<-2<<-1<<0<<0<< 
    0<<0<<0<<0<<0<<0<<0<<0<<2<<-2<<0<<0<<1<<1<<0<<0<<
    -3<<0<<3<<0<<0<<0<<0<<0<<-2<<0<<-1<<0<<0<<0<<0<<0<< 
    0<<0<<0<<0<<-3<<0<<3<<0<<0<<0<<0<<0<<-2<<0<<-1<<0<<
    9<<-9<<-9<<9<<6<<3<<-6<<-3<<6<<-6<<3<<-3<<4<<2<<2<<1<< 
    -6<<6<<6<<-6<<-3<<-3<<3<<3<<-4<<4<<-2<<2<<-2<<-2<<-1<<-1<< 
    2<<0<<-2<<0<<0<<0<<0<<0<<1<<0<<1<<0<<0<<0<<0<<0<< 
    0<<0<<0<<0<<2<<0<<-2<<0<<0<<0<<0<<0<<1<<0<<1<<0<<
    -6<<6<<6<<-6<<-4<<-2<<4<<2<<-3<<3<<-3<<3<<-2<<-1<<-2<<-1<<
    4<<-4<<-4<<4<<2<<2<<-2<<-2<<2<<-2<<2<<-2<<1<<1<<1<<1;
}



BMPImage::BMPImage()
	: Y_Image(), z(1), bpp(1), has_invalid(false), even_zoom(0), padw(0), padb(0)
{
	SetBmpHeader(0, 0, z);
}

BMPImage::BMPImage(int _w, int _h, int _z)
	: Y_Image(_w, _h), z(_z), bpp(1), has_invalid(false), even_zoom(0), padw(0), padb(0)
{
	SetBmpHeader(_w, _h, _z);
}

BMPImage::~BMPImage()
{
}

int BMPImage::Read(const char *file, int suppress_warnings)
{
	if(strstr(file, ".pgm") || strstr(file, ".PGM") || strstr(file, ".ppm") || strstr(file, ".PPM")) { // read it as 8-bit pgm or 24-bit ppm
		return(ReadPGM_PPM(file));
	}

	FILE *fi;
	if(fopen_s(&fi, file, "rb")) {
		sprintf_s(error, 256, "Cannot open %s\n", file);
		fail = 1;
		return(-1);
	}
	ReadBmpHeader(fi);
	if(bmphdr.typ != (unsigned short) 19778) {
		fail = 1;
		return(-1);
	}
	w = bmphdr.cols;
	h = bmphdr.rows;
	{
		int pp = h*((w+3) & ~3);
		pp += pp>>1;
		int k = 0;
	}
	
	if(bmphdr.bpp==8) {
		z = 1;
		if((bmphdr.isize+1024+54)!=bmphdr.fsize) {
			sprintf_s(error, 256, "Fsize != Isize+1024+54!\n");
			if(!suppress_warnings)
				fprintf(stderr, "Fsize != Isize+1024+54!\n");
			// In some cases it's not - so just warn.
			//fclose(fi); return(-1);
		}
		padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */
		//padb = 4-w%4; if(padb==4) padb = 0;
		// Same as below:
		padb = (4-w%4)&0x03;
		if(bmphdr.isize!=(unsigned int) ((w+padb)*h)) {
			sprintf_s(error, 256, "Wrong Isize.\n");
			if(!suppress_warnings)
				fprintf(stderr, "Wrong Isize.\n");
		}
	} else if(bmphdr.bpp==24) {
		z = 3;
		if((bmphdr.isize+54)!=bmphdr.fsize) {
			sprintf_s(error, 256, "Fsize != Isize+54!\n");
			if(!suppress_warnings)
				fprintf(stderr, "Fsize != Isize+54!\n");
		}
		padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
		padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */
		// However sometimes 'padb' is not needed...
		if((bmphdr.isize!=(unsigned int) ((3*w+padb)*h)) &&
			(bmphdr.isize!=(unsigned int) ((3*w+0)*h))) {
			sprintf_s(error, 256, "Wrong Isize.\n");
			if(!suppress_warnings)
				fprintf(stderr, "Wrong Isize.\n");
		}
	} else if(bmphdr.bpp==32) {
		z = 4;
		if((bmphdr.isize+54)!=bmphdr.fsize) {
			sprintf_s(error, 256, "Fsize != Isize+54!\n");
			if(!suppress_warnings)
				fprintf(stderr, "Fsize != Isize+54!\n");
		}
		padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
		padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */
		// However sometimes 'padb' is not needed...
		if((bmphdr.isize!=(unsigned int) ((4*w+padb)*h)) &&
			(bmphdr.isize!=(unsigned int) ((4*w+0)*h))) {
			sprintf_s(error, 256, "Wrong Isize.\n");
			if(!suppress_warnings)
				fprintf(stderr, "Wrong Isize.\n");
		}
	} else {
		sprintf_s(error, 256, "Not a greyscale or colour %s.\n", EXTN);
		if(!suppress_warnings)
			fprintf(stderr, "Not a greyscale or colour %s.\n", EXTN);
		z = 0;
	}
	fclose(fi);
	fail = 0;
	has_invalid = false; 
	return(0);
}

void BMPImage::SkipPGMPPMComments(FILE *fp)
{
	int ch;

	fscanf_s(fp," ");      // Skip white space.
	while ((ch = fgetc(fp)) == '#') {
		while ((ch = fgetc(fp)) != '\n'  &&  ch != EOF) ;
		fscanf_s(fp," ");
	}
	ungetc(ch, fp);      // Replace last character read.
}

int BMPImage::ReadPGM_PPM(const char *file)
{
	unsigned char char1, char2;
	int max, c1, c2, c3;

	FILE *fp;
	
	if(fopen_s(&fp, file, "rb")) {
		sprintf_s(error, 256, "Cannot open %s\n", file); 
		fail = 1;
		return(-1);
	}
	char1 = fgetc(fp); char2 = fgetc(fp); BMPImage::SkipPGMPPMComments(fp);
	c1 = fscanf_s(fp, "%d", &w); BMPImage::SkipPGMPPMComments(fp);
	c2 = fscanf_s(fp, "%d", &h); BMPImage::SkipPGMPPMComments(fp);
	c3 = fscanf_s(fp, "%d", &max);

	if (char1 != 'P' || (char2 != '5' && char2 != '6') || c1 != 1 || c2 != 1 || c3 != 1 || max > 255) {
		sprintf_s(error, 256, "Not a raw 8-bit PGM or 24-bit PPM.\n");
		fail = 1; fclose(fp); return(-1);
	}
	return(0);
}

// Save focal length as class member variable and res1/res2 in BMP header
void BMPImage::SetFocalLength(double f_length)
{
	focal_length = f_length;
	// This is because res1 and res2 are shorts, i.e. 2 bytes each
	float fl = float(focal_length);
	if(endian) Y_Utils::Swap4Byte((u_char *) &fl);
	memcpy(&bmphdr.res1, &fl, sizeof(float));
}


int BMPImage::WriteBmpHeader(FILE *fo)
{
	char hdr[54];
	fseek(fo, 0L, 0);
	hdr[0] = char(bmphdr.typ); hdr[1] = char(bmphdr.typ>>8);
	hdr[2] = char(bmphdr.fsize); hdr[3] = char(bmphdr.fsize>>8);
	hdr[4] = char(bmphdr.fsize>>16); hdr[5] = char(bmphdr.fsize>>24);
	hdr[6] = char(bmphdr.res1); hdr[7] = char(bmphdr.res1>>8);
	hdr[8] = char(bmphdr.res2); hdr[9] = char(bmphdr.res2>>8);
	hdr[10] = char(bmphdr.off); hdr[11] = char(bmphdr.off>>8);
	hdr[12] = char(bmphdr.off>>16); hdr[13] = char(bmphdr.off>>24);
	hdr[14] = char(bmphdr.hsize); hdr[15] = char(bmphdr.hsize>>8);
	hdr[16] = char(bmphdr.hsize>>16); hdr[17] = char(bmphdr.hsize>>24);
	hdr[18] = char(bmphdr.cols); hdr[19] = char(bmphdr.cols>>8);
	hdr[20] = char(bmphdr.cols>>16); hdr[21] = char(bmphdr.cols>>24);
	hdr[22] = char(bmphdr.rows); hdr[23] = char(bmphdr.rows>>8);
	hdr[24] = char(bmphdr.rows>>16); hdr[25] = char(bmphdr.rows>>24);
	hdr[26] = char(bmphdr.planes); hdr[27] = char(bmphdr.planes>>8);
	hdr[28] = char(bmphdr.bpp); hdr[29] = char(bmphdr.bpp>>8);
	hdr[30] = char(bmphdr.compress); hdr[31] = char(bmphdr.compress>>8);
	hdr[32] = char(bmphdr.compress>>16); hdr[33] = char(bmphdr.compress>>24);
	hdr[34] = char(bmphdr.isize); hdr[35] = char(bmphdr.isize>>8);
	hdr[36] = char(bmphdr.isize>>16); hdr[37] = char(bmphdr.isize>>24);
	hdr[38] = char(bmphdr.xpel); hdr[39] = char(bmphdr.xpel>>8);
	hdr[40] = char(bmphdr.xpel>>16); hdr[41] = char(bmphdr.xpel>>24);
	hdr[42] = char(bmphdr.ypel); hdr[43] = char(bmphdr.ypel>>8);
	hdr[44] = char(bmphdr.ypel>>16); hdr[45] = char(bmphdr.ypel>>24);
	hdr[46] = char(bmphdr.clrused); hdr[47] = char(bmphdr.clrused>>8);
	hdr[48] = char(bmphdr.clrused>>16); hdr[49] = char(bmphdr.clrused>>24);
	hdr[50] = char(bmphdr.clrimp); hdr[51] = char(bmphdr.clrimp>>8);
	hdr[52] = char(bmphdr.clrimp>>16); hdr[53] = char(bmphdr.clrimp>>24);
	int k = (int) fwrite(hdr, 1, 54, fo);
	//fprintf(stderr, "k=%d\n", k);
	return((k==54) ? 0 : -1);
}

int BMPImage::ReadBmpHeader(FILE *fi)
{
	unsigned char a,b,c,d;

	fseek(fi,0L,0);
/* identifier */
	b = getc(fi); a = getc(fi);
	bmphdr.typ = (a<<8) | b;
/* filesize */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.fsize = (a<<24) | (b<<16) | (c<<8) | d;
/* reserved */
	b = getc(fi); a = getc(fi);
	bmphdr.res1 = (a<<8) | b;
	b = getc(fi); a = getc(fi);
	bmphdr.res2 = (a<<8) | b;
	if(bmphdr.res1!=0) { // Try to interpret as focal length
		float fl;
		if(endian) {
			unsigned short tmp = bmphdr.res1;
			bmphdr.res1 = bmphdr.res2;
			bmphdr.res2 = tmp;
		}
		memcpy(&fl, &bmphdr.res1, sizeof(float));
		focal_length = double(fl);
	}
/* offset */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.off = (a<<24) | (b<<16) | (c<<8) | d;
/* header size */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.hsize = (a<<24) | (b<<16) | (c<<8) | d;
/* cols */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.cols = (a<<24) | (b<<16) | (c<<8) | d;
/* rows */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.rows = (a<<24) | (b<<16) | (c<<8) | d;
/* planes */
	b = getc(fi); a = getc(fi);
	bmphdr.planes = (a<<8) | b;
/* bpp */
	b = getc(fi); a = getc(fi);
	bmphdr.bpp = (a<<8) | b;
/* compression */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.compress = (a<<24) | (b<<16) | (c<<8) | d;
/* image size */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.isize = (a<<24) | (b<<16) | (c<<8) | d;
/* horizontal resolupion: pixels per ? */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.xpel = (a<<24) | (b<<16) | (c<<8) | d;
/* vertical resolupion: pixels per ? */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.ypel = (a<<24) | (b<<16) | (c<<8) | d;
/* clrused */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.clrused = (a<<24) | (b<<16) | (c<<8) | d;
/* clrimp */
	d = getc(fi); c = getc(fi); b = getc(fi); a = getc(fi);
	bmphdr.clrimp = (a<<24) | (b<<16) | (c<<8) | d;
	
	/* Error checking - from XV */
	if((bmphdr.bpp!=1 && bmphdr.bpp!=4 && bmphdr.bpp!=8 && bmphdr.bpp!=24 && bmphdr.bpp!=32)
					||
	   (bmphdr.planes!=1 || bmphdr.compress>BI_RLE4))
	   		return(-1);
	if(((bmphdr.bpp==1 || bmphdr.bpp==24) && bmphdr.compress != BI_RGB) ||
      (bmphdr.bpp==4 && bmphdr.compress==BI_RLE8) ||
      (bmphdr.bpp==8 && bmphdr.compress==BI_RLE4))
	   		return(-1);
	return(0);
}

void BMPImage::SetBmpHeader(int w, int h, int z)
{
	//bmphdr.typ = 19778;
	bmphdr.typ = 'B' + ('M' << 8);
		padw = ((z*w+3)/4)*4;
	bmphdr.fsize = (z==1) ? 54 + 1024 + (padw*h) : 54 + 3*(w*h);
	if(focal_length<=0.0) {
		bmphdr.res1 = 0;
		bmphdr.res2 = 0;
	} else {
		float fl = float(focal_length);
		memcpy(&bmphdr.res1, &fl, sizeof(float));
		if(endian) {
			unsigned short tmp = bmphdr.res1;
			bmphdr.res1 = bmphdr.res2;
			bmphdr.res2 = tmp;
		}
	}
	bmphdr.off = (z==1) ? 54+1024 : 54;
	bmphdr.hsize = 40;
	bmphdr.cols = w; 
	bmphdr.rows = h;
	bmphdr.planes = 1;
	bmphdr.bpp = z*8;
	bmphdr.compress = 0;
	bmphdr.isize = (z==1) ? padw*h : w*h*z;
	bmphdr.xpel = 0; bmphdr.ypel = 0;
	bmphdr.clrused = 0;
	bmphdr.clrimp = 0;
}

void BMPImage::PrintBmpHeader(FILE *fo)
{
	fprintf(fo, "type    =%d\n", bmphdr.typ);
	fprintf(fo, "fsize   =%ld\n", bmphdr.fsize);
	fprintf(fo, "res1    =%ld\n", bmphdr.res1);
	fprintf(fo, "res2    =%ld\n", bmphdr.res2);
	if(bmphdr.res1!=0) { // try to interpret as focal length
		fprintf(fo, "focal length =%lf\n", focal_length);
	}
	fprintf(fo, "off     =%ld\n", bmphdr.off);
	fprintf(fo, "hsize   =%ld\n", bmphdr.hsize);
	fprintf(fo, "cols    =%ld  rows=%ld\n", bmphdr.cols, bmphdr.rows);
	fprintf(fo, "planes  =%d\n", bmphdr.planes);
	fprintf(fo, "bpp     =%d\n", bmphdr.bpp);
	fprintf(fo, "compress=%ld\n", bmphdr.compress);
	fprintf(fo, "isize   =%ld\n", bmphdr.isize);
	fprintf(fo, "xpel    =%ld  ypel=%ld\n", bmphdr.xpel, bmphdr.ypel);
	fprintf(fo, "clrused =%ld\n", bmphdr.clrused);
	fprintf(fo, "clrimp  =%ld\n", bmphdr.clrimp);
}
