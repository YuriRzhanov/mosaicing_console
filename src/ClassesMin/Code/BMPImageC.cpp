#include <stdlib.h>
#include <math.h>
#ifndef CLASSESMIN
#include "BMPImageB.h"
#endif
#include "BMPImageC.h"

using namespace std;

BMPImageC::BMPImageC()
	: BMPImage(), im(nullptr)
{
	z = SPAN;
	SetBmpHeader(0, 0, z);
}

BMPImageC::BMPImageC(int _w, int _h)
	: BMPImage(_w, _h, SPAN), im(nullptr)
{
	z = SPAN;
	SetBmpHeader(_w, _h, z);
	padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
	padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */

	im = (u_char*) new char[SPAN*_w*_h];
	memset(im, 0, SPAN*_w*_h);
}

BMPImageC::BMPImageC(int _w, int _h, u_char *_im)
	: BMPImage(_w, _h, SPAN), im(nullptr)
{
	z = SPAN;
	SetBmpHeader(_w, _h, z);
	padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
	padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */

	im = (u_char*) new char[SPAN*_w*_h];
	memcpy(im, _im, SPAN*_w*_h);
}

BMPImageC::BMPImageC(int _w, int _h, u_char *r, u_char *g, u_char *b)
	: BMPImage(_w, _h, SPAN), im(nullptr)
{
	z = SPAN;
	SetBmpHeader(_w, _h, z);
	padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
	padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */

	im = (u_char*) new char[SPAN*_w*_h];
	for(int j = 0; j<_h; j++) {
		for(int i = 0; i<_w; i++) {
			im[SPAN*(_w*j+i)+0] = r[_w*j+i];
			im[SPAN*(_w*j+i)+1] = g[_w*j+i];
			im[SPAN*(_w*j+i)+2] = b[_w*j+i];
		}
	}
}

// Rainbow colouring
BMPImageC::BMPImageC(int _w, int _h, double *d)
	: BMPImage(_w, _h, SPAN), im(nullptr)
{
	z = SPAN;
	SetBmpHeader(_w, _h, z);
	padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
	padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */

	im = (u_char*) new char[SPAN*_w*_h];

	int i, j, k;
	double v;
	Y_RGB<u_char> vv;
	
	// Find range
	double a_max = -1.e9, a_min = 1.e9, scale;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			v = d[j*w+i];
			a_max = (a_max < v) ? v : a_max;
			a_min = (a_min > v) ? v : a_min;
		}
	}
	if(a_min>=a_max)
		scale = 1.0;
	else {
		// used range is from 0 to 4*256-1
		scale = (256.0*4-1.0)/(a_max-a_min);
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			k = int(scale*(d[j*w+i]-a_min));
			if(k<256) // Blue spectrum
				vv.Set(0, 0, k);
			else if(k<2*256) // Blue-red spectrum
				vv.Set(k-256, 0, 256-k);
			else if(k<3*256) // Red-yellow
				vv.Set(255, k-512);
			else // yellow-white
				vv.Set(255, 255, k-768);
			set_pixel(i, j, vv);
		}
	}
}

BMPImageC::BMPImageC(const char *file, int suppress_warnings)
	: BMPImage(), im(nullptr)
{
	z = SPAN;
	Read(file, suppress_warnings);
}

BMPImageC::BMPImageC(BMPImageG *imG)
	: BMPImage(imG->w, imG->h, SPAN), im(nullptr)
{
	z = SPAN;
	focal_length = imG->focal_length;
	im = (u_char*) new char[z*w*h];
	u_char v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = imG->im[w*j+i];
			im[SPAN*(w*j+i)+0] = v;
			im[SPAN*(w*j+i)+1] = v;
			im[SPAN*(w*j+i)+2] = v;
		}
	}
}

BMPImageC::BMPImageC(IntImageG *imG, bool scale)
	: BMPImage(imG->w, imG->h, SPAN), im(nullptr)
{
	z = SPAN;
	focal_length = imG->focal_length;
	im = (u_char*) new char[z*w*h];
	if(scale) {
		int v_min, v_max;
		imG->MinMax(v_min, v_max);
		double factor = 255.0/v_max;
		u_char v;
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				v = (u_char)(factor*imG->i_im[w*j+i]>255) ? 255 : factor*imG->i_im[w*j+i];
				im[SPAN*(w*j+i)+0] = v;
				im[SPAN*(w*j+i)+1] = v;
				im[SPAN*(w*j+i)+2] = v;
			}
		}
	} else { // clip
		u_char v;
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				v = (imG->i_im[w*j+i]>255) ? 255 : imG->i_im[w*j+i];
				im[SPAN*(w*j+i)+0] = v;
				im[SPAN*(w*j+i)+1] = v;
				im[SPAN*(w*j+i)+2] = v;
			}
		}
	}
}

BMPImageC::~BMPImageC()
{
	if(im) {
		delete [] im;
		im = nullptr;
	}
	fail = 1;
}

void BMPImageC::Dimensions(int _w, int _h)
{
	if(im) delete [] im;
	w = _w; h = _h; SetBmpHeader(_w, _h, z);
	padb = (4-((w*3)%4))&0x03; /* # of pad bytes to read at EOscanline */
	padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */
	
	im = (u_char*) new char[SPAN*w*h];
}

int BMPImageC::PutAt(BMPImageC *p, int x, int y)
{
	for(int j = 0; j<p->h; j++) {
		if((y+j)<0 || (y+j)>=h)
			continue;
		for(int i = 0; i<p->w; i++) {
			if((x+i)<0 || (x+i)>=w)
				continue;
			memcpy(&im[SPAN*((y+j)*w+(x+i))], &(p->im[SPAN*(j*p->w+i)]), SPAN);
		}
	}
	return(0);
}

BMPImageC *BMPImageC::Copy()
{
	BMPImageC *c = new BMPImageC(w, h, im);
	c->z = z;
	c->conf_set = conf_set;
	c->bpp = bpp;
	c->endian = endian;
	c->focal_length = focal_length;
	c->SetBmpHeader(w, h, z);
	return(c);
}

#ifndef CLASSESMIN
BMPImageB *BMPImageC::Copy(Y_RGB<u_char> set_clr)
{
	BMPImageB* pBin = new BMPImageB(w, h);
	pBin->UnsetImage();
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(get_pixel(i,j)==set_clr)
				pBin->set_pixel(i,j);
		}
	}
	return(pBin);
}
#endif

void BMPImageC::Copy(BMPImageC *s)
{
	if(w!=s->w || h!=s->h) {
		sprintf_s(error, 256, "Images have different dimensions!\n");
		return;
	}
	z = s->z;
	memcpy(im, s->im, z*w*h*sizeof(u_char));
	conf_set = s->conf_set;
	bpp = s->bpp;
	endian = s->endian;
	focal_length = s->focal_length;
	SetBmpHeader(s->w, s->h, s->z);
}

// Container is ready, just create colour on the basis of greyscale
void BMPImageC::Copy(BMPImageG *imG)
{
	if(w!=imG->w || h!=imG->h) {
		sprintf_s(error, 256, "Images have different dimensions!\n");
		return;
	}
	endian = imG->endian;
	focal_length = imG->focal_length;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			im[z*(w*j+i)+0] = imG->im[w*j+i];
			im[z*(w*j+i)+1] = imG->im[w*j+i];
			im[z*(w*j+i)+2] = imG->im[w*j+i];
		}
	}
}

BMPImageG *BMPImageC::Greyscale(char channel)
{
	BMPImageG *t = new BMPImageG(w, h);
	if(!t)
		return(t);
	t->focal_length = focal_length;
	int i, j, k;
	
	switch(channel) {
		case 'a':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->im[k] = (u_char) ToGreyscale(im[SPAN*k+0],im[SPAN*k+1],im[SPAN*k+2]);
				}
			}
			break;
		case 'r':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->im[k] = im[SPAN*k+0];
				}
			}
			break;
		case 'g':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->im[k] = im[SPAN*k+1];
				}
			}
			break;
		case 'b':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->im[k] = im[SPAN*k+2];
				}
			}
			break;
		default:
			sprintf_s(error, 256, "Channel must be 'a','r','g' or 'b'.\n");
			return(nullptr);
	}
	return(t);
}

void BMPImageC::MakeGreyscale(char channel)
{
	int i, j, k;
	
	switch(channel) {
		case 'a':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+0] = (u_char) ToGreyscale(im[SPAN*k+0],im[SPAN*k+1],im[SPAN*k+2]);
					im[SPAN*k+1] = im[SPAN*k+2] = im[SPAN*k+0];
				}
			}
			break;
		case 'r':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+1] = im[SPAN*k+2] = im[SPAN*k+0];
				}
			}
			break;
		case 'g':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+2] = im[SPAN*k+0] = im[SPAN*k+1];
				}
			}
			break;
		case 'b':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+0] = im[SPAN*k+1] = im[SPAN*k+2];
				}
			}
			break;
		default:
			sprintf_s(error, 256, "Channel must be 'a','r','g' or 'b'.\n");
	}
}

void BMPImageC::SetChannel(BMPImageG* ch, char channel)
{
	if(ch->w!=w || ch->h!=h) {
		sprintf_s(error, 256, "Incorrect image dimensions.\n");
		return;
	}
	int i, j, k;
	switch(channel) {
		case 'a':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+0] = ch->im[k];
					im[SPAN*k+1] = ch->im[k];
					im[SPAN*k+2] = ch->im[k];
				}
			}
			break;
		case 'r':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+0] = ch->im[k];
				}
			}
			break;
		case 'g':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+1] = ch->im[k];
				}
			}
			break;
		case 'b':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					im[SPAN*k+2] = ch->im[k];
				}
			}
			break;
		default:
			sprintf_s(error, 256, "Channel must be 'a','r','g' or 'b'.\n");
			return;
	}
}

void BMPImageC::SetChannel(DblImageG* dbl_ch, int rescale, char channel)
{
	BMPImageG* ch = dbl_ch->Greyscale(rescale);
	SetChannel(ch, channel);
	delete ch;
}

int BMPImageC::CombineGrayscales(BMPImageG* pR, BMPImageG* pG, BMPImageG* pB)
{
	if(pR->w!=w || pR->h!=h || pG->w!=w || pG->h!=h || pB->w!=w || pB->h!=h) {
		sprintf_s(error, 256, "Image dimensions do not match.\n");
		return(-1);
	}
	int k;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			k = j*w+i;
			im[SPAN*k+0] = pR->im[k];
			im[SPAN*k+1] = pG->im[k];
			im[SPAN*k+2] = pB->im[k];
		}
	}
	return(0);
}

int BMPImageC::Read(const char *file, int suppress_warnings)
{
	if(strstr(file, ".ppm") || strstr(file, ".PPM")) { // read it as 24-bit ppm
		return(ReadPPM(file));
	}

	unsigned char *buf;
	int i, j, k, ind;
	FILE *fi;
	
	if(fopen_s(&fi, file, "rb")) {
		sprintf_s(error, 256, "Can't open %s\n", file); 
		fail = 1;
		return(-1);
	}
	if(ReadBmpHeader(fi)) {
		sprintf_s(error, 256, "Not a colour %s.\n", EXTN);
		fail = 1;
		return(-1);
	}
	w = bmphdr.cols;
	h = bmphdr.rows;
	/* Sanity checks */
	if(bmphdr.bpp == 8 && bmphdr.clrused!=256) { // Read as greyscale
		fclose(fi);
		// Non-fatal warning
		sprintf_s(error, 256, "Not a colour %s.\n", EXTN);
		if(!suppress_warnings)
			fprintf(stderr, "Not a colour %s.\n", EXTN);
		BMPImageG *tmpG = new BMPImageG(file);
		if(!tmpG->fail) {
			if(im) delete [] im;
			im = (u_char*) new char[SPAN*w*h];
			for(int k = 0; k<w*h; k++) {
				im[SPAN*k+0] = im[SPAN*k+1] = im[SPAN*k+2] = tmpG->im[k];
			}
		}
		delete tmpG;
		return(0);
	}
	if(bmphdr.bpp == 8 && bmphdr.clrused==256) { // Fixed palette
		if(im) delete [] im;
		im = (u_char*) new char[SPAN*w*h];
		buf = (unsigned char *) new char[w*h];
		u_char* colormap = new u_char[4*bmphdr.clrused];
		for(i = 0; i<(int)bmphdr.clrused; i++) {
			if(fread(colormap+i*4, sizeof(char), 4, fi)!=4) {
				sprintf_s(error, 256, "Cannot read colormap.\n");
				delete [] colormap;
				fclose(fi); 
				return(-1);
			}
		}
		// Read colormapped image
		fseek(fi, bmphdr.off, SEEK_SET);
		if(fread(buf, sizeof(char), w*h, fi)!=w*h) {
			sprintf_s(error, 256, "Cannot read image.\n");
			delete [] colormap;
			fclose(fi); 
			return(-1);
		}
		// decode colormapping
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				im[SPAN*((h-j-1)*w+i)+0] = colormap[4*(int)buf[j*w+i]+2];
				im[SPAN*((h-j-1)*w+i)+1] = colormap[4*(int)buf[j*w+i]+1];
				im[SPAN*((h-j-1)*w+i)+2] = colormap[4*(int)buf[j*w+i]+0];
			}
		}
		delete [] buf;
		delete [] colormap;
		return(0);
	}
	if(bmphdr.bpp!=24 && bmphdr.bpp!=32) {
		sprintf_s(error, 256, "Not a colour %s.\n", EXTN);
		fclose(fi); 
		fail = 1;
		return(-1);
	}
	//if(bmphdr.off==0) // Just forgot to set it...
		bmphdr.off = 54; 
	if(bmphdr.isize==0) // Just forgot to set it...
		bmphdr.isize = bmphdr.fsize-bmphdr.off;
	if((bmphdr.isize+bmphdr.off)!=bmphdr.fsize) {
		sprintf_s(error, 256, "Fsize != Isize+%ul!.\n", bmphdr.off);
		fclose(fi); return(-1);
	}
	padb = (4-((w*(bmphdr.bpp/8))%4))&0x03;  /* # of pad bytes to read at EOscanline */
	padw = ((z*w+3)/4)*4; /* multiple of 4pix (32 bits) */
	if(bmphdr.isize != (unsigned int) (((bmphdr.bpp/8)*w+padb)*h)) {
		// Sometimes 'padb' is not needed...
		// Check for padb=0...
		/*
		Complete confusion...
		if(bmphdr.isize != (unsigned int) (SPAN*w*h)) {
			sprintf_s(error, 256, "Wrong Isize.\n");
			fclose(fi); return(-1);
		} else {
			padb = 0;
		}
		*/
	}
	if(im) delete [] im;
	im = (u_char*) new char[SPAN*w*h];
	buf = (unsigned char *) new char[(bmphdr.bpp/8)*w+padb];
	if(fail) {
		delete [] buf;
		return(-1);
	}
	if(bmphdr.bpp==24) {
		for(j=h-1; j>=0; j--) {
			fread(buf, sizeof(char), w*SPAN+padb, fi);
			for(k=0; k<w*SPAN; k+=SPAN) {
				ind = j*w+k/SPAN;
				//memcpy(&im[SPAN*ind], &buf[k], SPAN);
				im[SPAN*ind+2] = buf[k];
				im[SPAN*ind+1] = buf[k+1];
				im[SPAN*ind+0] = buf[k+2];
			}
		}
	} else { // bpp==32
		for(j=h-1; j>=0; j--) {
			fread(buf, sizeof(char), w*4+padb, fi);
			for(k=0; k<w*4; k+=4) {
				ind = j*w+k/4;
				//memcpy(&im[SPAN*ind], &buf[k], SPAN);
				im[SPAN*ind+2] = buf[k];
				im[SPAN*ind+1] = buf[k+1];
				im[SPAN*ind+0] = buf[k+2];
			}
		}
	}
	delete [] buf;
	fclose(fi); 
	return(0);
}

int BMPImageC::ReadPPM(const char *file)
{
	unsigned char char1, char2;
	int max, c1, c2, c3;

	FILE *fp;
	
	if(fopen_s(&fp, file, "rb")) {
		sprintf_s(error, 256, "Can't open %s\n", file); 
		fail = 1;
		return(-1);
	}
	char1 = fgetc(fp); char2 = fgetc(fp); BMPImage::SkipPGMPPMComments(fp);
	c1 = fscanf_s(fp, "%d", &w); BMPImage::SkipPGMPPMComments(fp);
	c2 = fscanf_s(fp, "%d", &h); BMPImage::SkipPGMPPMComments(fp);
	c3 = fscanf_s(fp, "%d", &max);

	if (char1 != 'P' || char2 != '6' || c1 != 1 || c2 != 1 || c3 != 1 || max > 255) {
		sprintf_s(error, 256, "Not a raw 24-bit PPM.\n");
		fail = 1; fclose(fp); return(-1);
	}

	fgetc(fp);  // Discard exactly one byte after header.
	if(im)
		delete [] im; // In case of re-reading in the same image
	im = (u_char*) new char[SPAN*w*h];

	char *tmp = new char[SPAN*w];
	
	for(int j = 0; j<h; j++) {
		fread(tmp, SPAN*w, 1, fp);
		memcpy(&im[SPAN*w*j], tmp, SPAN*w);
	}
	delete [] tmp;

	fclose(fp);
	return(0);
}

int BMPImageC::Save(const char *file)
{
	FILE *fo;
	int i, j, ind;
	
	if(fopen_s(&fo, file, "wb")) {
		sprintf_s(error, 256, "Cannot write to \"%s\"\n", file);
		return(-1);
	}
	padb = (4-((w*3)%4))&0x03;  
		/* # of pad bytes to write at EOscanline */
	unsigned char *line = (unsigned char *) new char[SPAN*w+padb];

	/*
	bmphdr.typ = ('M' << 8) + 'B';
	bmphdr.fsize = 54 + SPAN*(w*h);
	if(focal_length<=0.0) {
		bmphdr.res1 = 0;
		bmphdr.res2 = 0;
	} else {
		float fl = float(focal_length);
		memcpy(&bmphdr.res1, &fl, sizeof(float));
	}
	bmphdr.off = 54;
	bmphdr.hsize = 40;
	bmphdr.cols = w;
	bmphdr.rows = h;
	bmphdr.planes = 1;
	bmphdr.bpp = 24;
	bmphdr.compress = 0;
	bmphdr.isize = SPAN*w*h;
	bmphdr.xpel = 75 * 39;
	bmphdr.ypel = 75 * 39;
	bmphdr.clrused = 0;
	bmphdr.clrimp = 0;
	*/
	SetBmpHeader(w, h, SPAN);
	if(WriteBmpHeader(fo)==-1) { 
		fclose(fo); 
		delete [] line;
		return(-1); 
	}
	fseek(fo, bmphdr.off, 0);
	for(j = h-1; j>=0; j--) {
		for(i = 0; i<SPAN*w; i += SPAN) {
			ind = j*w+i/SPAN;
			line[i+2] = (u_char) im[SPAN*ind+0]; // ?? opposite order?
			line[i+1] = (u_char) im[SPAN*ind+1];
			line[i+0] = (u_char) im[SPAN*ind+2];
		}
		if(fwrite(line, sizeof(char), SPAN*w+padb, fo)!=(SPAN*w+padb)) {
			fclose(fo); delete [] line; return(-1);
		}
	}
	delete [] line;
	fclose(fo);
	return(0);
}

int BMPImageC::SavePPM(const char *file)
{
	char *tmp = new char[SPAN*w];
	
	FILE *fo;
	if(fopen_s(&fo, file, "wb")) {
		delete [] tmp;
		return(-1);
	}
	fprintf(fo, "P6\n%d %d\n255\n", w, h);
	for(int j = 0; j<h; j++) {
		memcpy(tmp, &im[SPAN*w*j], SPAN*w);
		fwrite(tmp, SPAN*w, 1, fo);
	}
	fclose(fo);
	
	delete [] tmp;
	return(0);	
}

// invalid, if any corner is invalid
bool BMPImageC::valid(double x, double y)
{
	int i = int(x), j = int(y);
	if(valid(i,j) && valid(i+1,j) && valid(i,j+1) && valid(i+1,j+1))
		return(true);
	return(false);
}

// valid pixel, neighbouring invalid
int BMPImageC::Border(int x, int y)
{
	if(!valid(x, y)) return(0);
	for(int j = y-1; j<=y+1; j++) {
		for(int i = x-1; i<=x+1; i++) {
			if(!valid(i, j)) return(1);
		}
	}
	return(0);
}

// Second argument is defaulted to INVALID_RGB.
// With both arguments used to validate pixels (say, substitute 0's with 1's).
// With one argument - make some colour invalid.
int BMPImageC::Substitute(Y_RGB<u_char> clr, Y_RGB<u_char> with, Y_Rect<int>* pRect)
{
	int n = 0;
	
	if(!pRect) {
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				if(get_pixel(i, j)==clr) {
					set_pixel(i, j, with);
					n++;
				}
			}
		}
	} else {
		for(int j = pRect->y; j<=(pRect->y+pRect->h); j++) {
			for(int i = pRect->x; i<=(pRect->x+pRect->w); i++) {
				if(get_pixel(i, j)==clr) {
					set_pixel(i, j, with);
					n++;
				}
			}
		}
	}
	return(n);
}

// If each component is bad, set to a good colour.
// If equivalent greyscale is bad, set to a good colour.
int BMPImageC::Validate(Y_RGB<u_char> c_inv, u_char g_inv)
{
	int n = 0;
	Y_RGB<u_char> good(1, 1, 1);
	Y_RGB<u_char> v;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = get_pixel(i,j);
			if(v==c_inv) {
				set_pixel(i,j,good);
				n++;
			} else if(ToGreyscale(v.p[0],v.p[1],v.p[2])==g_inv) {
				set_pixel(i,j,good);
				n++;
			}
		}
	}
	return(n);
}

void BMPImageC::SetColour(u_char r, u_char g, u_char b, int override_invalid)
{
	Y_RGB<u_char> clr(r, g, b);
	SetColour(clr, override_invalid);
}

BMPImageC** BMPImageC::Split(int factor)
{
	BMPImageC** pImgs = new BMPImageC*[factor];
	int i, j, new_w = w/factor, nw;
	for(int n = 0; n<factor; n++) {
		// Last image could have different number of columns from the rest
		nw = (n<(factor-1)) ? new_w : w-new_w*n;
		pImgs[n] = new BMPImageC(nw, h);
		for(j = 0; j<h; j++) {
			for(i = 0; i<nw; i++) {
				pImgs[n]->set_pixel(i, j, get_pixel(n*nw+i, j));
			}
		}
	}
	return(pImgs);
}

void BMPImageC::SetColour(Y_RGB<u_char> clr, int override_invalid)
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(override_invalid)
				set_pixel(i, j, clr);
			else if(get_pixel(i, j).Valid())
				set_pixel(i, j, clr);
		}
	}
}

void BMPImageC::SetInvalid()
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++)
			set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB); 
	}
}

void BMPImageC::CorrectIllum(BMPImageG *p)
{
	u_char r, g, b, vp;
	Y_RGB<u_char> v;
	Y_RGB<u_char> white(255, 255, 255);
	
	if(p->w!=w || p->h!=h) {
		sprintf_s(error, 256, "Correcting illumination with mask of wrong size.\n");
		return;
	}
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = get_pixel(i, j);
			if(!v.Valid()) continue;
			vp = p->get_pixel(i, j);
			if(vp) {
				r = (u_char) (255.0*v.p[0]/vp);
				if(r>255) r = 255;
				g = (u_char) (255.0*v.p[1]/vp);
				if(g>255) g = 255;
				b = (u_char) (255.0*v.p[2]/vp);
				if(b>255) b = 255;
				set_pixel(i, j, Y_RGB<u_char>(r, g, b));
			} else
				set_pixel(i, j, white);
		}
	}
}

void BMPImageC::CorrectIllum(BMPImageC *p)
{
	u_char r, g, b;
	Y_RGB<u_char> v, vp;
	
	if(p->w!=w || p->h!=h) {
		sprintf_s(error, 256, "Correcting illumination with mask of wrong size.\n");
		return;
	}
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = get_pixel(i, j);
			if(!v.Valid()) continue;
			vp = p->get_pixel(i, j);
			if(vp.p[0]) {
				r = (u_char) (255.0*v.p[0]/vp.p[0]);
				if(r>255) r = 255;
			} else
				r = 255;
			if(vp.p[1]) {
				g = (u_char) (255.0*v.p[1]/vp.p[1]);
				if(g>255) g = 255;
			} else
				g = 255;
			if(vp.p[2]) {
				b = (u_char) (255.0*v.p[2]/vp.p[2]);
				if(b>255) b = 255;
			} else
				b = 255;
			set_pixel(i, j, Y_RGB<u_char>(r, g, b));
		}
	}
}

void BMPImageC::ValidateByMask(BMPImageG *p)
{
	if(!p) return;
	if(p->w!=w || p->h!=h) {
		sprintf_s(error, 256, "Validating with mask of wrong size.\n");
		return;
	}
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(p->im[j*w+i]==Y_Utils::_INVALID)
				set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
		}
	}
}

/* Cut off subimage, taking care of image extent.
   at_x/y - top-left corner of cut
   sw/sh - requested size
   Area is initiated with black.
*/
BMPImageC *BMPImageC::CutSubimage(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	BMPImageC *s = new BMPImageC(sw, sh);
	s->SetBlack();
	s->focal_length = focal_length;
	
	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			memcpy(&(s->im[SPAN*(jj*sw+ii)]), &im[SPAN*(j*w+i)], SPAN);
		}
	}
	return(s);
}

void BMPImageC::Crop(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	u_char* im_ = (u_char*) new char[SPAN*sw*sh];

	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			memcpy(&(im_[SPAN*(jj*sw+ii)]), &im[SPAN*(j*w+i)], SPAN);
		}
	}
	delete [] im;
	im = im_;
	w = sw; h = sh;
}

double BMPImageC::AverageBrightness(Y_RGB<double> clr)
{
	double s = 0.0;
	int count = 0;
	Y_RGB<double> V;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V.Set(im[SPAN*(j*w+i)+0],im[SPAN*(j*w+i)+1],im[SPAN*(j*w+i)+2]);
			if(V!=clr) {
				s += V.Greyscale();
				count++;
			}
		}
	}
	return(s/double(count));
}

int BMPImageC::Replicate(int f)
{
	u_char *new_im = (u_char*) new char[f*w*f*h];
	int x, y;
	for(int j = 0; j<f*h; j++) {
		y = j/f;
		for(int i = 0; i<f*w; i++) {
			x = i/f;
			new_im[SPAN*(j*f*w+i)+0] = im[SPAN*(y*w+x)+0];
			new_im[SPAN*(j*f*w+i)+1] = im[SPAN*(y*w+x)+1];
			new_im[SPAN*(j*f*w+i)+2] = im[SPAN*(y*w+x)+2];
		}
	}
	w = f*w; h = f*h;
	delete [] im;
	im = new_im;
	return(0);
}


// Linear shift in brightness to match average and 'level'. Returns number of saturated pixels.
int BMPImageC::ShiftAverageTo(u_char level)
{
	int aver = (int)AverageBrightness();
	int shift = (int)level-aver;
	Y_RGB<u_char> V;
	int saturated = 0, s, r, g, b;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i, j);
			r = V.p[0]+shift;
			g = V.p[1]+shift;
			b = V.p[2]+shift;
			s = 0;
			if(r<0) {
				V.p[0] = 0; s = 1;
			} else if(r>255) {
				V.p[0] = 255; s = 1;
			} else {
				V.p[0] = (u_char)r;
			}
			if(g<0) {
				V.p[1] = 0; s = 1;
			} else if(g>255) {
				V.p[1] = 255; s = 1;
			} else {
				V.p[1] = (u_char)g;
			}
			if(b<0) {
				V.p[2] = 0; s = 1;
			} else if(b>255) {
				V.p[2] = 255; s = 1;
			} else {
				V.p[2] = (u_char)b;
			}
			if(s) saturated++;
		}
	}
	return(saturated);
}

int BMPImageC::ZoomToDest_Bilinear(int dest_w, int dest_h, bool show_progress)
{
	double I, J;
	int vld, status = 0;
	double h_factor = double(w-1)/double(dest_w-1);
	double v_factor = double(h-1)/double(dest_h-1);
	BMPImageC *tmp = new BMPImageC(dest_w, dest_h);
	Y_RGB<double> v;
	
	ConsoleProgress* pCP = new ConsoleProgress(dest_h*dest_w, 70);
	for(int j = 0; j<dest_h-1; j++) {
		J = v_factor*j;
		for(int i = 0; i<dest_w-1; i++) {
			I = h_factor*i;
			v = Bilinear(I, J, &vld);
			if(!vld) {
				tmp->set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
			} else {
				tmp->set_pixel(i, j, v);
			}
			if(show_progress) pCP->Next();
		}
		I = h_factor*(dest_w-1)-0.5;
		v = Bilinear(I, J, int(I), int(J));
		tmp->set_pixel(dest_w-1, j, v);
	}
	delete pCP;
	// Last row - enforce interpolation from above row
	J = v_factor*(dest_h-1)-0.5;
	for(int i = 0; i<dest_w-1; i++) {
		I = h_factor*i;
		v = Bilinear(I, J, int(I), int(J));
		tmp->set_pixel(i, dest_h-1, v);
	}
	I = h_factor*(dest_w-1)-0.5;
	v = Bilinear(I, J, int(I), int(J));
	tmp->set_pixel(dest_w-1, dest_h-1, v);
	delete [] im;
	im = new u_char[SPAN*dest_w*dest_h];
	memcpy(im, tmp->im, SPAN*dest_w*dest_h*sizeof(u_char));
	delete tmp;
	w = dest_w; h = dest_h;

	return(status);
}

int BMPImageC::Zoom_Bilinear(double zm, bool show_progress)
{
	int i, j, status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bilinear(dest_w, dest_h, show_progress);
	if(status==-1) {
		sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Erode image, removing pixels next to invalid pixels
	BMPImageC *cim = Copy();
	for(j = 1; j<dest_h-1; j++) {
		for(i = 1; i<dest_w-1; i++) {
			if(!cim->valid(i-1,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i  ,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i+1,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i-1,  j)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i+1,  j)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i-1,j+1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i  ,j+1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i+1,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
		}
	}
	delete cim;
	if(focal_length > 0.0)
		focal_length *= zm;

	return(status);
}

int BMPImageC::ZoomToDest_Bicubic(int dest_w, int dest_h, bool show_progress)
{
	BMPImageC* pBMPnew = new BMPImageC(dest_w, dest_h);

	BMPImageG* pR = Greyscale('r');
	if(pR) pR->ZoomToDest_Bicubic(dest_w, dest_h, show_progress);

	BMPImageG* pG = Greyscale('g');
	if(pG) pG->ZoomToDest_Bicubic(dest_w, dest_h, show_progress);

	BMPImageG* pB = Greyscale('b');
	if(pB) pB->ZoomToDest_Bicubic(dest_w, dest_h, show_progress);

	if(pR && pG && pB)
		pBMPnew->CombineGrayscales(pR, pG, pB);

	if(pR) delete pR;
	if(pG) delete pG;
	if(pB) delete pB;

	delete [] im;
	im = new u_char[SPAN*dest_w*dest_h];
	memcpy(im, pBMPnew->im, SPAN*dest_w*dest_h);
	w = dest_w; h = dest_h;

	delete pBMPnew;

	return(0);
}

int BMPImageC::Zoom_Bicubic(double zm, bool show_progress)
{
	int i, j, status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bicubic(dest_w, dest_h, show_progress);
	if(status==-1) {
		sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Erode image, removing pixels next to invalid pixels
	BMPImageC *cim = Copy();
	for(j = 1; j<dest_h-1; j++) {
		for(i = 1; i<dest_w-1; i++) {
			if(!cim->valid(i-1,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i  ,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i+1,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i-1,  j)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i+1,  j)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i-1,j+1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i  ,j+1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
			if(!cim->valid(i+1,j-1)) set_pixel(i,j,Y_RGB<u_char>::INVALID_RGB);
		}
	}
	delete cim;
	if(focal_length > 0.0)
		focal_length *= zm;

	return(status);
}

int BMPImageC::Translate_Bilinear(double X, double Y, Y_RGB<u_char> bg, bool show_progress)
{
	int i, j;
	//int ii, jj, x, y;
	double dx, dy;
	BMPImageC *tmp = Copy();
	this->SetColour(bg);

	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			// Positions as doubles
			dx = -X+i;
			dy = -Y+j;
			if(dx>=0.0 && dx<=(double) (w-1) && dy>=0.0 && 
					dy<=(double) (h-1)) {
				set_pixel(i, j, Bilinear(dx, dy));
			}
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	delete tmp;
	
	return(0);
}

int BMPImageC::Translate_Bicubic(double X, double Y, Y_RGB<u_char> bg, bool show_progress)
{
	int i, j;
	//int ii, jj, x, y;
	double dx, dy;
	BMPImageC *tmp = Copy();
	this->SetColour(bg);

	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			// Positions as doubles
			dx = -X+i;
			dy = -Y+j;
			if(dx>=0.0 && dx<=(double) (w-1) && dy>=0.0 && 
					dy<=(double) (h-1)) {
				set_pixel(i, j, Bicubic(dx, dy));
			}
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	delete tmp;
	
	return(0);
}

int BMPImageC::Rotate_Bilinear(double angle_degrees, Y_RGB<u_char> bg, bool show_progress)
{
	int i, j;
	double cos_a, sin_a;
	BMPImageC *tmp = Copy();
	this->SetColour(bg);

	double theta = Deg2Rad(-angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	Y_RGB<u_char> v;
	Y_RGB<double> vd;
	
	double dx, dy;
	int vld;
	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double) i - 0.5*w + 0.5;
			sin_a = (double) j - 0.5*h + 0.5;
			// Positions as doubles 
			dx = c*cos_a-s*sin_a + 0.5*w - 0.5;
			dy = s*cos_a+c*sin_a + 0.5*h - 0.5;
			vd = tmp->Bilinear(dx, dy, &vld);
			if(vld) set_pixel(i, j, vd);
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	delete tmp;
	
	return(0);
}

int BMPImageC::Rotate_Bicubic(double angle_degrees, Y_RGB<u_char> bg, bool show_progress)
{
	int i, j;
	double cos_a, sin_a;
	BMPImageC *tmp = Copy();
	this->SetColour(bg);

	double theta = Deg2Rad(-angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	Y_RGB<u_char> v;
	Y_RGB<double> vd;
	
	double dx, dy;
	int vld;
	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double) i - 0.5*w + 0.5;
			sin_a = (double) j - 0.5*h + 0.5;
			// Positions as doubles 
			dx = c*cos_a-s*sin_a + 0.5*w - 0.5;
			dy = s*cos_a+c*sin_a + 0.5*h - 0.5;
			//if(i>=363 && i<=364 && j>=424 && j<=425)
			//	sprintf_s(error, 256, "BMPImageC::rotate breakpoint\n");
			vd = tmp->Bicubic(dx, dy, &vld);
			if(vld) set_pixel(i, j, vd);
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	delete tmp;
	
	return(0);
}

// Returns new array of the same size after transformations
int BMPImageC::ZoomRotateTranslate_Bilinear(double zm, double a_d, 
	double X, double Y, int flags)
{
	int i, j;
	int status = 0;
	
	if(!EPSILON_EQ(zm,1.0)) {
		// Zooming
		int dest_w = (int) (zm*w);
		int dest_h = (int) (zm*h);
		if(even_zoom) {
			if(dest_w%2==1) dest_w++;
			if(dest_h%2==1) dest_h++;
		}
		int sw = (w-dest_w)/2;
		int sh = (h-dest_h)/2;
		
		// Keep values of dimensions
		int prev_w = w;
		int prev_h = h;
	
		status = ZoomToDest_Bilinear(dest_w, dest_h);
		if(status==-1) {
			sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
			exit(-1);
		}
		// Now 'im_r', etc. have dimensions dest_w*dest_h
	
		u_char *tmp = (u_char*) new char[SPAN*prev_w*prev_h];
	
		for(j = 0; j<prev_h; j++) {
			if(j-sh<0 || j+sh>=prev_h)
				continue;
			for(i = 0; i<prev_w; i++) {
				if(i-sw<0 || i+sw>=prev_w)
					continue;
				memcpy(&tmp[SPAN*(j*prev_w+i)], &im[SPAN*((j-sh)*dest_w+(i-sw))], SPAN);
			}
		}
		delete [] im; im = tmp;
		// Restore dimensions
		w = prev_w; h = prev_h;
	}
	
	if(!EPSILON_EQ(a_d,0.0)) {
		// Rotation
		status += Rotate_Bilinear(a_d, flags);
	}
		
	if(!EPSILON_EQ(X,0.0) || !EPSILON_EQ(Y,0.0)) {
		// Translation
		status += Translate_Bilinear(X, Y, flags);
	}
	return(status);
}

// Returns new array of the same size after transformations
int BMPImageC::ZoomRotateTranslate_Bicubic(double zm, double a_d, 
	double X, double Y, int flags)
{
	int i, j;
	int status = 0;
	
	if(!EPSILON_EQ(zm,1.0)) {
		// Zooming
		int dest_w = (int) (zm*w);
		int dest_h = (int) (zm*h);
		if(even_zoom) {
			if(dest_w%2==1) dest_w++;
			if(dest_h%2==1) dest_h++;
		}
		int sw = (w-dest_w)/2;
		int sh = (h-dest_h)/2;
		
		// Keep values of dimensions
		int prev_w = w;
		int prev_h = h;
	
		status = ZoomToDest_Bicubic(dest_w, dest_h);
		if(status==-1) {
			sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
			exit(-1);
		}
		// Now 'im_r', etc. have dimensions dest_w*dest_h
	
		u_char *tmp = (u_char*) new char[SPAN*prev_w*prev_h];
	
		for(j = 0; j<prev_h; j++) {
			if(j-sh<0 || j+sh>=prev_h)
				continue;
			for(i = 0; i<prev_w; i++) {
				if(i-sw<0 || i+sw>=prev_w)
					continue;
				memcpy(&tmp[SPAN*(j*prev_w+i)], &im[SPAN*((j-sh)*dest_w+(i-sw))], SPAN);
			}
		}
		delete [] im; im = tmp;
		// Restore dimensions
		w = prev_w; h = prev_h;
	}
	
	if(!EPSILON_EQ(a_d,0.0)) {
		// Rotation
		status += Rotate_Bicubic(a_d, flags);
	}
		
	if(!EPSILON_EQ(X,0.0) || !EPSILON_EQ(Y,0.0)) {
		// Translation
		status += Translate_Bicubic(X, Y, flags);
	}
	return(status);
}

// 1: by pi/2, 2: by pi, 3: by -pi/2
void BMPImageC::RotateQuarter(int nr_of_quarters)
{
	int new_w, new_h;
	if(nr_of_quarters == 2) {
		new_w = w; new_h = h;
	} else {
		new_w = h; new_h = w;
	}
	u_char *tmp = (u_char*)  new char[SPAN*new_w*new_h];
	int i, j;
	switch(nr_of_quarters) {
	case 1:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[SPAN*(j*new_w+i)], &im[SPAN*(i*w+(w-1-j))], SPAN);
		}}
		break;
	case 2:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[SPAN*(j*new_w+i)], &im[SPAN*((h-1-j)*w+(w-1-i))], SPAN);
		}}
		break;
	case 3:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[SPAN*(j*new_w+i)], &im[SPAN*((h-1-i)*w+j)], SPAN);
		}}
		break;
	}
	delete [] im;
	im = tmp;
	w = new_w; h = new_h;
}


/* Difference between two sub-images which are supposed to be equivalent.
   xs, ys - integer shift of the image 'i' with respect to this. 
   Saves result in the file called 'name' if second argument is given. */
double BMPImageC::DifferenceError(BMPImage *I, int xs, int ys, int *overlap,
	char *name)
{
	int i, j, ii, jj, n, vr, vir, vg, vig, vb, vib;
	double dif = 0.0;
	BMPImageC *ic = (BMPImageC *) I;
	int wi = ic->w, hi = ic->h;
	BMPImageC *d = nullptr;
	char stg[256];
	
	if(name)
		d = new BMPImageC(w, h);
	
	n = 0;
	// Each pixel of image I is checked.
	// It gives contribution to pixel (ii,jj) of image *this.
	for(j = 0; j<hi; j++) {
		jj = j-ys;
		if(jj<0 || jj>=h)
			continue;
		for(i = 0; i<wi; i++) {
			ii = i-xs;
			if(ii<0 || ii>=w)
				continue;
			if(ic->get_pixel(i, j).Valid()) { /* has something already */
				if(get_pixel(ii, jj).Valid()) {
					vir = (int) (ic->im[SPAN*(j*wi+i)+0]);
					vr = (int) (im[SPAN*(jj*w+ii)+0]);
					vig = (int) (ic->im[SPAN*(j*wi+i)+1]);
					vg = (int) (im[SPAN*(jj*w+ii)+1]);
					vib = (int) (ic->im[SPAN*(j*wi+i)+2]);
					vb = (int) (im[SPAN*(jj*w+ii)+2]);
					dif += (double) (abs(vir-vr)+abs(vig-vg)+abs(vib-vb));
					if(d) {
						d->im[SPAN*(j*w+i)+0] = (vr+vir)/2;
						d->im[SPAN*(j*w+i)+1] = (vg+vig)/2;
						d->im[SPAN*(j*w+i)+2] = (vb+vib)/2;
					}
					n++;
				} else {
					if(d) {
						d->im[SPAN*(j*w+i)+0] = ic->im[SPAN*(j*wi+i)+0];
						d->im[SPAN*(j*w+i)+1] = ic->im[SPAN*(j*wi+i)+1];
						d->im[SPAN*(j*w+i)+2] = ic->im[SPAN*(j*wi+i)+2];
					}
				}
			} else {
				if(d) {
					d->im[SPAN*(j*w+i)+0] = im[SPAN*(jj*w+ii)+0];
					d->im[SPAN*(j*w+i)+1] = im[SPAN*(jj*w+ii)+1];
					d->im[SPAN*(j*w+i)+2] = im[SPAN*(jj*w+ii)+2];
				}
			}
		}
	}
	if(debug)
		fprintf(stderr, "difference_error: total dif=%lf  pixels=%d\n", dif, n);
	if(n)
		dif /= (double)SPAN*n;
	else {
		sprintf_s(error, 256, "No overlap between images.\n");
		dif = 0.0;
	}
	if(debug) {
		fprintf(stderr, "  dif=%lf\n", dif);
		sprintf_s(stg, 256, "I1.%s", EXTN);
		Save(stg);		
		sprintf_s(stg, 256, "I2.%s", EXTN);
		ic->Save(stg);
		fprintf(stderr, "Images \"I1.%s\" and \"I2.%s\" saved.\n", EXTN, EXTN);
	}
		
	if(d) {
		d->Save(name);
		sprintf_s(error, 256, "Image \"%s\" saved.\n", name);
		delete d;
	}
	*overlap = n;
	return(dif); 
}

// Difference between two sub-images which are supposed to be equivalent.
//   xs, ys - shift of the image 'i' with respect to this. 
//   Saves result in the file called 'name' if second argument is given. 
// Uses bilinear interpolation - this doesn't matter.
double BMPImageC::DifferenceErrorDouble(BMPImage *I, double xs, double ys, 
	int *overlap, const char *name)
{
	int i, j, n, vld;
	Y_RGB<double> v, vi, r;
	double ii, jj;
	double dif = 0.0;
	BMPImageC *ic = (BMPImageC *) I;
	int wi = ic->w, hi = ic->h;
	BMPImageC *d = nullptr;
	char stg[256];
	
	if(name)
		d = new BMPImageC(w, h);
	
	n = 0;
	// Every pixel in *this image is checked.
	// Pixel (i,j) gets contribution from location (ii,jj) of image I.
	for(j = 0; j<h; j++) {
		jj = (double)j-ys;
		if(jj<0 || jj>=hi)
			continue;
		for(i = 0; i<w; i++) {
			ii = (double)i-xs;
			if(ii<0 || ii>=wi)
				continue;
			vi = ic->Bilinear(ii, jj, &vld);
			if(valid(i, j)) {
				v = get_pixel_D2(i, j);
				if(vld) {
					r = v-vi;
					dif += r.ToDbl();
					n++;
					if(d) {
						//r = (v+vi)/2;
						d->set_pixel(i, j, (v+vi)*0.5);
					}
				} else {
					if(d)
						d->set_pixel(i, j, v);
				}
			} else {
				if(vld && d)
					d->set_pixel(i, j, vi);
			}
		}
	}
	if(debug)
		fprintf(stderr, "difference_error_double: total dif=%lf  pixels=%d\n", dif, n);
	if(n)
		dif /= (double)SPAN*n;
	else {
		sprintf_s(error, 256, "No overlap between images.\n");
		dif = 0.0;
	}
	if(debug) {
		fprintf(stderr, "  dif=%lf\n", dif);
		
		sprintf_s(stg, 256, "I1.%s", EXTN);
		Save(stg);
		sprintf_s(stg, 256, "I2.%s", EXTN);
		ic->Save(stg);
		fprintf(stderr, "Images \"I1.%s\" and \"I2.%s\" saved.\n", EXTN, EXTN);
	}
		
	if(d) {
		d->Save(name);
		sprintf_s(error, 256, "Image \"%s\" saved.\n", name);
		delete d;
	}
	*overlap = n;
	return(dif); 
}

// If search leaves the determined rectangle, return -1.
int BMPImageC::SeedFill(int seedx, int seedy, Y_RGB<u_char> bound, Y_RGB<u_char> fill, int s_length, Y_Rect<int>* pR)
{
	int x, y, savex, savey, xleft, xright, num;
	int* s = new int[s_length];
	Y_RGB<u_char> col;

	num = 0;
	int slen = 0;

// push on the stack
	s[slen] = seedx; slen++; s[slen] = seedy; slen++;

	while(slen!=0) {
		// pop seed onto stack 
		x = s[slen-2]; y = s[slen-1]; slen -= 2;
		if(pR && pR->Inside(x,y)==0)
			return(-1);
		set_pixel(x, y, fill); ++num;
		// get extreme left and right members of fill scan line
		// fill to the right of the seed pixel 
		savex = x;
		x++;
		col = get_pixel(x, y);

		while(col!=bound && x<w) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			set_pixel(x, y, fill); ++num; x++;
			col = get_pixel(x, y);
		}
		xright = x - 1;
		x = savex;

		// fill to the left of the seed pixel
		x--;
		col = get_pixel(x, y);
		while(col!=bound && x>=0) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			set_pixel(x, y, fill); ++num; x--;
			col = get_pixel(x, y);
		}
		xleft = x + 1; x = savex;

		// check scan line above 
		savey = y; x = xleft;

		if(y<h-1) {
			y++;
			col = get_pixel(x, y);
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++; col = get_pixel(x, y);
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = get_pixel(x, y);
						}
						s[slen] = x-1; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);
			x = savex; y = savey;
		}

		// check scan line below
		y = savey; x = xleft;

		if(y>0) {
			y--;
			col = get_pixel(x, y);
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = get_pixel(x, y);
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = get_pixel(x, y);
						}
						s[slen] = x-1; s[slen+1] = y; slen += 2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);

			x = savex; y = savey;
		}
	}
	delete [] s;
	return(num);
}

// If search leaves the determined rectangle, return -1.
int BMPImageC::SeedFill(int seedx, int seedy, Y_RGB<u_char> bound, Y_RGB<u_char> fill, vector<Point2<int> >& locations, 
	int s_length, Y_Rect<int>* pR)
{
	int x, y, savex, savey, xleft, xright, num;
	int* s = new int[s_length];
	Y_RGB<u_char> col;

	num = 0;
	int slen = 0;

// push on the stack
	s[slen] = seedx; slen++; s[slen] = seedy; slen++;

	while(slen!=0) {
		// pop seed onto stack 
		x = s[slen-2]; y = s[slen-1]; slen -= 2;
		if(pR && pR->Inside(x,y)==0)
			return(-1);
		set_pixel(x, y, fill); ++num;
		locations.push_back(Point2<int>(x, y));
		// get extreme left and right members of fill scan line
		// fill to the right of the seed pixel 
		savex = x; x++;
		col = get_pixel(x, y);

		while(col!=bound && x<w) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			set_pixel(x, y, fill); ++num;
			locations.push_back(Point2<int>(x, y));
			x++;
			col = get_pixel(x, y);
		}
		xright = x - 1; x = savex;

		// fill to the left of the seed pixel
		x--;
		col = get_pixel(x, y);
		while(col!=bound && x>=0) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			set_pixel(x, y, fill); ++num; 
			locations.push_back(Point2<int>(x, y));
			x--;
			col = get_pixel(x, y);
		}
		xleft = x + 1; x = savex;

		// check scan line above 
		savey = y; x = xleft;

		if(y<h-1) {
			y++;
			col = get_pixel(x, y);
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = get_pixel(x, y);
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = get_pixel(x, y);
						}
						s[slen] = x-1; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);
			x = savex; y = savey;
		}

		// check scan line below
		y = savey; x = xleft;

		if(y>0) {
			y--;
			col = get_pixel(x, y);
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = get_pixel(x, y);
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = get_pixel(x, y);
						}
						s[slen] = x-1; s[slen+1] = y; slen += 2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);

			x = savex; y = savey;
		}
	}
	delete [] s;
	return(num);
}

// Starting with (x,y) colour all pixels with 'fill' until any other
int BMPImageC::SeedFill(int seedx, int seedy, Y_RGB<u_char> fill, int s_length, Y_Rect<int>* pR)
{
	int x, y, savex, savey, xleft, xright, num = 0;
	int* s = new int[s_length];
	Y_RGB<u_char> col;
	int slen = 0;
	Y_RGB<u_char> seedClr = get_pixel(seedx, seedy);

// push on the stack
	s[slen] = seedx; slen++; s[slen] = seedy; slen++;

	while(slen!=0) {
		// pop seed onto stack 
		x = s[slen-2]; y = s[slen-1]; slen -= 2;
		if(pR && pR->Inside(x,y)==0)
			return(-1);
		set_pixel(x, y, fill); ++num;
		// get extreme left and right members of fill scan line
		// fill to the right of the seed pixel 
		savex = x;
		x++;
		col = get_pixel(x,y);

		while(col==seedClr && x<w) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			set_pixel(x, y, fill); ++num; x++;
			col = get_pixel(x,y);
		}
		xright = x - 1; x = savex;

		// fill to the left of the seed pixel
		x--;
		col = get_pixel(x,y);
		while(col==seedClr && x>=0) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			set_pixel(x, y, fill); ++num; x--;
			col = get_pixel(x,y);
		}
		xleft = x + 1;
		x = savex;

		// check scan line above 
		savey = y;
		x = xleft;

		if(y<h-1) {
			y++;
			col = get_pixel(x,y);
			do {
				while(((col!=seedClr) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = get_pixel(x,y);
				}
				if(col==seedClr && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col==seedClr) && (x<=xright) && (x<w)) {
							x++;
							col = get_pixel(x,y);
						}
						s[slen] = x-1; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);
			x = savex; y = savey;
		}

		// check scan line below
		y = savey; x = xleft;

		if(y>0) {
			y--;
			col = get_pixel(x,y);
			do {
				while(((col!=seedClr) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = get_pixel(x,y);
				}
				if(col==seedClr && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col==seedClr) && (x<=xright) && (x<w)) {
							x++;
							col = get_pixel(x,y);
						}
						s[slen] = x-1; s[slen+1] = y; slen += 2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);

			x = savex; y = savey;
		}
	}
	delete [] s;
	return(num);
}

Y_RGB<double> BMPImageC::Bilinear(double dx, double dy, int *vld)
{
	int col, row, k;
	double v[SPAN];
	u_char s00[SPAN], s10[SPAN], s01[SPAN], s11[SPAN];
	
	if(vld) *vld = 0;
	// Check for outide image 
	if(dx<0.0 || dx>=double(w-1) || dy<0.0 || dy>=double(h-1)) 
		return(Y_RGB<double>::INVALID_RGBD);
	
	// Integer parts 
	col = FLOOR(dx); row = FLOOR(dy);
	k = row*w+col;
	// If interested in pixel validity, check it
	if(vld && !valid(col, row)) 
		return(Y_RGB<double>::INVALID_RGBD);
	// Fractional parts 
	dx -= (double) col; dy -= (double) row;
	if(dx<1.e-5 && dy<1.e-5) { // practically coinsides with a node
		if(vld) *vld = 1;
		//Y_RGB<double> p = get_pixel_D(col,row,1);
		Y_RGB<u_char> p = get_pixel(col,row);
		Y_RGB<double> V(p.p[0],p.p[1],p.p[2]);
		return(V);
	}

	// For the pixel to be marked as invalid, all SPAN components must be invalid
	s00[0] = im[SPAN*k+0]; s00[1] = im[SPAN*k+1]; s00[2] = im[SPAN*k+2];
	if(col!=w-1) {
		if(vld && !valid(col+1, row)) { *vld=0; return(Y_RGB<double>::INVALID_RGBD); }
		s10[0] = im[SPAN*(k+1)+0]; s10[1] = im[SPAN*(k+1)+1]; s10[2] = im[SPAN*(k+1)+2];
	} else {
		s10[0] = im[SPAN*k+0]; s10[1] = im[SPAN*k+1]; s10[2] = im[SPAN*k+2];
	}
	if(row!=h-1) {
		if(vld && !valid(col, row+1)) { *vld=0; return(Y_RGB<double>::INVALID_RGBD); }
		s01[0] = im[SPAN*(k+w)+0]; s01[1] = im[SPAN*(k+w)+1]; s01[2] = im[SPAN*(k+w)+2];
	} else {
		s01[0] = im[SPAN*k+0]; s01[1] = im[SPAN*k+1]; s01[2] = im[SPAN*k+2];
	}
	if(col!=(w-1) && row!=(h-1)) {
		if(vld && !valid(col+1, row+1)) { *vld=0; return(Y_RGB<double>::INVALID_RGBD); }
		s11[0] = im[SPAN*(k+w+1)+0]; s11[1] = im[SPAN*(k+w+1)+1]; s11[2] = im[SPAN*(k+w+1)+2];
	} else {
		s11[0] = im[SPAN*k+0]; s11[1] = im[SPAN*k+1]; s11[2] = im[SPAN*k+2];
	}
	if(vld) *vld = 1; // Validate pixel
	v[0] = (double) s00[0]+dx*(s10[0]-s00[0])+dy*(s01[0]-s00[0])+
		dx*dy*(s11[0]+s00[0]-s10[0]-s01[0]);
	v[1] = (double) s00[1]+dx*(s10[1]-s00[1])+dy*(s01[1]-s00[1])+
		dx*dy*(s11[1]+s00[1]-s10[1]-s01[1]);
	v[2] = (double) s00[2]+dx*(s10[2]-s00[2])+dy*(s01[2]-s00[2])+
		dx*dy*(s11[2]+s00[2]-s10[2]-s01[2]);
	Y_RGB<double> V(v[0],v[1],v[2]);
	return(V);
}

// Special case when all 4 nodes are guaranteed to exist
Y_RGB<double> BMPImageC::Bilinear(double dx, double dy, int tl_i, int tl_j)
{
	int k;
	double v[SPAN];
	u_char s00[SPAN], s10[SPAN], s01[SPAN], s11[SPAN];
	
	k = tl_j*w+tl_i;
	// Fractional parts 
	dx -= (double) tl_i; dy -= (double) tl_j;
	if(dx<1.e-5 && dy<1.e-5) // practically coinsides with a node
		return(get_pixel_D2(tl_i,tl_j));
	if(fabs(dx-1.0)<1.e-5 && dy<1.e-5) 
		return(get_pixel_D2(tl_i+1,tl_j));
	if(dx<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(get_pixel_D2(tl_i,tl_j+1));
	if(fabs(dx-1.0)<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(get_pixel_D2(tl_i+1,tl_j+1));

	s00[0] = im[SPAN*k+0]; s00[1] = im[SPAN*k+1]; s00[2] = im[SPAN*k+2];
	s10[0] = im[SPAN*(k+1)+0]; s10[1] = im[SPAN*(k+1)+1]; s10[2] = im[SPAN*(k+1)+2];
	s01[0] = im[SPAN*(k+w)+0]; s01[1] = im[SPAN*(k+w)+1]; s01[2] = im[SPAN*(k+w)+2];
	s11[0] = im[SPAN*(k+w+1)+0]; s11[1] = im[SPAN*(k+w+1)+1]; s11[2] = im[SPAN*(k+w+1)+2];
	v[0] = (double) s00[0]+dx*(s10[0]-s00[0])+dy*(s01[0]-s00[0])+
		dx*dy*(s11[0]+s00[0]-s10[0]-s01[0]);
	v[1] = (double) s00[1]+dx*(s10[1]-s00[1])+dy*(s01[1]-s00[1])+
		dx*dy*(s11[1]+s00[1]-s10[1]-s01[1]);
	v[2] = (double) s00[2]+dx*(s10[2]-s00[2])+dy*(s01[2]-s00[2])+
		dx*dy*(s11[2]+s00[2]-s10[2]-s01[2]);
	Y_RGB<double> V(v[0],v[1],v[2]);
	return(V);
}

Y_RGB<double> BMPImageC::Bicubic(double x, double y, int *vld)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	double v, X, Y;
	Y_RGB<double> V_out;

	// Surrounding pixels: (I,J)->(I+1,J+1)
	int If = (int)floor(x), Jf = (int)floor(y);
	int Ic = (int)ceil(x), Jc = (int)ceil(y);
		if(Jf<0) Jf = 0;
		if(Jc>h-1) Jc = h-1;
		if(If<0) If = 0;
		if(Ic>w-1) Ic = w-1;
	// If, Jf, Ic, Jc are guaranteed not to go out of range
	double V[16]; // 4 values, 4 X-derivatives, 4 Y-derivatives, 4 cross-derivatives
	if(vld) *vld = 1;
	NEWMAT::ColumnVector beta(16);
	int w3 = SPAN*w;

	for(int i = 0; i<SPAN; i++) {
		V[ 0] = (double)im[i+SPAN*(If+w*Jf)];
		V[ 1] = (double)im[i+SPAN*(Ic+w*Jf)];
		V[ 2] = (double)im[i+SPAN*(If+w*Jc)];
		V[ 3] = (double)im[i+SPAN*(Ic+w*Jc)];
		V[ 4] = (If<1)   ? 0.0 : 0.5*((double)im[i+SPAN*(Ic+w*Jf)]-im[i+SPAN*(If-1+w*Jf)]);
		V[ 5] = (Ic>=w-1)   ? 0.0 : 0.5*((double)im[i+SPAN*(Ic+1+w*Jf)]-im[i+SPAN*(If+w*Jf)]);
		V[ 6] = (If<1) ? 0.0 : 0.5*((double)im[i+SPAN*(Ic+w*Jc)]-im[i+SPAN*(If-1+w*Jc)]);
		V[ 7] = (Ic>=w-1) ? 0.0 : 0.5*((double)im[i+SPAN*(Ic+1+w*Jc)]-im[i+SPAN*(If+w*Jc)]);
		V[ 8] = (Jf<1) ? 0.0 : 0.5*((double)im[i+SPAN*(If+w*Jc)]-im[i+SPAN*(If+w*(Jf-1))]);
		V[ 9] = (Jf<1) ? 0.0 : 0.5*((double)im[i+SPAN*(Ic+w*Jc)]-im[i+SPAN*(Ic+w*(Jf-1))]);
		V[10] = (Jc>=h-1) ? 0.0 : 0.5*((double)im[i+SPAN*(If+w*(Jc+1))]-im[i+SPAN*(If+w*Jf)]);
		V[11] = (Jc>=h-1) ? 0.0 : 0.5*((double)im[i+SPAN*(Ic+w*(Jc+1))]-im[i+SPAN*(Ic+w*Jf)]);
		V[12] = (If<1 || Jf<1) ? 0.0 : 0.25*((double)im[i+SPAN*(Ic+w*Jc)]+im[i+SPAN*(If-1+w*(Jf-1))]-im[i+SPAN*(Ic+w*(Jf-1))]-im[i+SPAN*(If-1+w*Jc)]);
		V[13] = (Ic>=w-1 || Jf<1) ? 0.0 : 0.25*((double)im[i+SPAN*(Ic+1+w*Jc)]+im[i+SPAN*(If+w*(Jf-1))]-im[i+SPAN*(Ic+1+w*(Jf-1))]-im[i+SPAN*(If+w*Jc)]);
		V[14] = (If<1|| Jc>=h-1) ? 0.0 : 0.25*((double)im[i+SPAN*(Ic+w*(Jc+1))]+im[i+SPAN*(If-1+w*Jf)]-im[i+SPAN*(Ic+w*Jf)]-im[i+SPAN*(If-1+w*(Jc+1))]);
		V[15] = (Ic>=w-1 || Jc>=h-1) ? 0.0 : 0.25*((double)im[i+SPAN*(Ic+1+w*(Jc+1))]+im[i+SPAN*(If+w*Jf)]-im[i+SPAN*(Ic+1+w*Jf)]-im[i+SPAN*(If+w*(Jc+1))]);

		beta<<V[0]<<V[1]<<V[2]<<V[3]<<V[4]<<V[5]<<V[6]<<V[7]<<V[8]<<V[9]<<V[10]<<V[11]<<V[12]<<V[13]<<V[14]<<V[15];

		NEWMAT::ColumnVector alpha = M_bicubic*beta;

		X = x-floor(x);
		Y = y-floor(y);
		v = 0.0;
		for(int k = 0; k<16; k++) {
			v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
		}
		if(v<0.0) v = 0.0;
		if(v>255.0) v = 255.0;
		V_out.p[i] = v;
	}
	
	return(V_out);
}

void BMPImageC::MarkPoint(double x, double y, int rad, Y_RGB<u_char> Pixel)
{
	int I = (int) (x+0.5), J = (int) (y+0.5);
	double d2, r2 = (rad-1)*(rad-1)/4;

	if(rad<2) {
		this->set_pixel(I, J, Pixel);
	} else {
		for(int j = J-rad; j<=J+rad; j++) {
			for(int i = I-rad; i<=I+rad; i++) {
				d2 = (double) (i-I)*(i-I)+(j-J)*(j-J);
				if(d2<=r2) {
					this->set_pixel(i, j, Pixel);
				}
			}
		}
	}
}


/*
BMPImageC *BMPImageC::Undistort_A_Bilinear(Y_Camera *C)
{
	BMPImageC *ui = new BMPImageC(w, h);
	ui->focal_length = focal_length;
	Y_RGB<double> pd;
	Y_RGB<u_char> p;
	Point2<int> d;
	Point2<double> u;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d.Set(i, j);
			u = C->DistortPixel(d);
			if(r.Inside(u)) {
				pd = Bilinear(u.X(), u.Y());
				p = Y_RGB<u_char>(u_char(pd.p[0]), u_char(pd.p[1]), u_char(pd.p[2]));
			} else {
				p = Y_RGB<u_char>::INVALID_RGB;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

BMPImageC *BMPImageC::Distort_A_Bilinear(Y_Camera *C)
{
	BMPImageC *ui = new BMPImageC(w, h);
	ui->focal_length = focal_length;
	Y_RGB<double> pd;
	Y_RGB<u_char> p;
	Point2<int> u;
	Point2<double> d;
	bool fail;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			u.Set(i, j);
			d = C->UndistortPixel(u, &fail);
			if(!fail && r.Inside(d)) {
				pd = Bilinear(d.X(), d.Y());
				p = Y_RGB<u_char>(u_char(pd.p[0]), u_char(pd.p[1]), u_char(pd.p[2]));
			} else {
				p = Y_RGB<u_char>::INVALID_RGB;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

BMPImageC *BMPImageC::Undistort_A_Bicubic(Y_Camera *C)
{
	BMPImageC *ui = new BMPImageC(w, h);
	ui->focal_length = focal_length;
	Y_RGB<double> pd;
	Y_RGB<u_char> p;
	Point2<int> d;
	Point2<double> u;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d.Set(i, j);
			u = C->DistortPixel(d);
			if(r.Inside(u)) {
				pd = Bicubic(u.X(), u.Y());
				p = Y_RGB<u_char>(u_char(pd.p[0]), u_char(pd.p[1]), u_char(pd.p[2]));
			} else {
				p = Y_RGB<u_char>::INVALID_RGB;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

BMPImageC *BMPImageC::Distort_A_Bicubic(Y_Camera *C)
{
	BMPImageC *ui = new BMPImageC(w, h);
	ui->focal_length = focal_length;
	Y_RGB<double> pd;
	Y_RGB<u_char> p;
	Point2<int> u;
	Point2<double> d;
	bool fail;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			u.Set(i, j);
			d = C->UndistortPixel(u, &fail);
			if(!fail && r.Inside(d)) {
				pd = Bicubic(d.X(), d.Y());
				p = Y_RGB<u_char>(u_char(pd.p[0]), u_char(pd.p[1]), u_char(pd.p[2]));
			} else {
				p = Y_RGB<u_char>::INVALID_RGB;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}
*/

/*
##############################################
From Microsoft: YUVFormats.asp
Kr = 0.299
Kb = 0.114
L = Kr*R+Kb*B+(1-Kr-Kb)*G

Then ITU-R BT.601 definition:
Y = round[219*L/255] +16
U = round[224*0.5*(B-L)/((1-Kb)*255)]+128
V = round[224*0.5*(R-L)/((1-Kr)*255)]+128

RGB888 -> YUV 4:4:4
Y = (( 66*R+129*G+ 25*B+128)>>8)+ 16
U = ((-38*R- 74*G+112*B+128)>>8)+128
V = ((112*R- 94*G- 18*B+128)>>8)+128

8-bit YUV -> RGB888
C = Y-16
D = U-128
E = V-128
R = clip( round( 1.164383*C              + 1.596027*E ))
G = clip( round( 1.164383*C - 0.391762*D - 0.812968*E ))
B = clip( round( 1.164383*C + 2.017232*D              ))
###################################################
*/
/* 
  YU12 (aka YUV 4:2:0) uses 1 luminance pixel per RGB pixel
  and 1 Cr pixel and 1 Cb pixel per 4 RGB pixels.
  Storage for YUV frame is expected to be provided.
*/
void BMPImageC::Recode2YU12(u_char *y, u_char *u, u_char *v)
{
	Y_RGB<u_char> v0, v1, v2, v3, v4;
	u_char r, g, b;
	int l;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v0 = get_pixel(i, j);
			l = (66*v0.p[0]+129*v0.p[1]+25*v0.p[2]+128)>>8;
			*y = (u_char) (l+16); y++;
			if(i%2==0 && j%2==0) {
				v1 = get_pixel(i-1, j-1);
				v2 = get_pixel(i, j-1);
				v3 = get_pixel(i, j-1);
				v4 = get_pixel(i, j);
				r = (v1.p[0]+v2.p[0]+v3.p[0]+v4.p[0])/4;
				g = (v1.p[1]+v2.p[1]+v3.p[1]+v4.p[1])/4;
				b = (v1.p[2]+v2.p[2]+v3.p[2]+v4.p[2])/4;
				l = (-38*r-74*g+112*b+128)>>8;
				*u = (u_char) (l+128); u++;
				l = (112*r-94*g-18*b+128)>>8;
				*v = (u_char) (l+128); v++;
			}
		}
	}
}

