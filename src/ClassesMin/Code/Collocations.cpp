#include "Collocations.h"

Collocations::Collocations(int _w, int _h, double _fl) :
	w(_w), h(_h), fl(_fl), n(0), b(nullptr), d(nullptr)
{
}

Collocations::Collocations(int _n, int _w, int _h, double _fl) :
	w(_w), h(_h), fl(_fl), n(_n), b(nullptr), d(nullptr)
{
	if(n) {
		b = new Coord2[n];
		d = new Coord2[n];
	}
}

// Default set of collocations based on provided homography
Collocations::Collocations(int _n, Hmgr& _hm, int _w, int _h, double _fl) :
	w(_w), h(_h), fl(_fl), hm(_hm), n(0), b(nullptr), d(nullptr)
{
	Hmgr inv_hm = hm.Invert();
	if(_n==2) {
		n = 2;
		b = new Coord2[n];
		d = new Coord2[n];
		for(int i = 0; i<n; i++) {
			b[i].SetSize(w, h);
			b[i].SetFocalLength(fl);
			d[i].SetSize(w, h);
			d[i].SetFocalLength(fl);
		}
		// 'd' is where point 'b' in 'base' image appears in 'derived' image
		b[0].Set(0.25*w, 0.25*h); 
		d[0] = inv_hm.Map(b[0]);
		b[1].Set(0.75*w, 0.75*h); 
		d[1] = inv_hm.Map(b[1]);
	}
	if(_n==4) {
		n = 4;
		b = new Coord2[n];
		d = new Coord2[n];
		for(int i = 0; i<n; i++) {
			b[i].SetSize(w, h);
			b[i].SetFocalLength(fl);
			d[i].SetSize(w, h);
			d[i].SetFocalLength(fl);
		}
		// 'd' is where point 'b' in 'base' image appears in 'derived' image
		b[0].Set(0.25*w, 0.25*h); 
		d[0] = inv_hm.Map(b[0]);
		b[1].Set(0.75*w, 0.75*h); 
		d[1] = inv_hm.Map(b[1]);
		b[2].Set(0.75*w, 0.25*h); 
		d[2] = inv_hm.Map(b[2]);
		b[3].Set(0.25*w, 0.75*h); 
		d[3] = inv_hm.Map(b[3]);
	}
}

Collocations::Collocations(const Collocations& c)
{
	hm = c.hm;
	w = c.w; h = c.h;
	fl = c.fl;
	n = c.n; b = nullptr; d = nullptr;

	if(c.n) {
		b = new Coord2[n];
		d = new Coord2[n];
		for(int i = 0; i<n; i++) {
			b[i].Copy(c.b[i]);
			d[i].Copy(c.d[i]);
		}
	}
}

Collocations::~Collocations()
{
	if(b) delete [] b;
	if(d) delete [] d;
}

void Collocations::SetSizeFocalLength(int _w, int _h, double _fl)
{
	w = _w; h = _h; fl = _fl;
}

/* Delegated to TransformRecord amd RARecord
// Copy collocations from this to given RARecord
void Collocations::SetToRAR(RARecord& rar)
{
	if(rar.n_col!=n) {
		if(rar.col_b) delete [] rar.col_b;
		if(rar.col_d) delete [] rar.col_d;
		rar.n_col = n;
		rar.col_b = new Coord2[n];
		rar.col_d = new Coord2[n];
	}
	for(int j = 0; j<n; j++) {
		rar.col_b[j].Copy(b[j]);
		rar.col_d[j].Copy(d[j]);
	}
}

// Copy collocations from this to given TransformRecord
void Collocations::SetToTransform(TransformRecord& trans)
{
	if(trans.n_col!=n) {
		if(trans.col_b) delete [] trans.col_b;
		if(trans.col_d) delete [] trans.col_d;
		trans.n_col = n;
		trans.col_b = new Coord2[n];
		trans.col_d = new Coord2[n];
	}
	for(int j = 0; j<n; j++) {
		trans.col_b[j].Copy(b[j]);
		trans.col_d[j].Copy(d[j]);
	}
}
*/

void Collocations::Print(FILE* fp) 
{
	for(int i = 0; i<n; i++) {
		fprintf(fp, "B\t(%.2lf,%.2lf)\tD\t(%.2lf,%.2lf)\n", b[i].X(), b[i].Y(), d[i].X(), d[i].Y());
	}
}
