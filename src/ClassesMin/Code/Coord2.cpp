#include "Coord2.h"

using namespace std;

DblImageG *Coord2::kernel;

void Coord2::Copy(const Coord2& c)
{
	w = c.w; h = c.h; focal_length = c.focal_length;
	Set(c.x, c.y);
}

double Coord2::Value2(BMPImageG *im, int *vld) {
	return(im->Bilinear(x, y, vld)); }

//double Coord2::Value2(IntImageG *im, int *vld) {
//	return(im->Bilinear(x, y, vld)); }

Y_RGB<double> Coord2::Value2(BMPImageC *im, int *vld) {
	return(im->Bilinear(x, y, vld)); }

//float Coord2::Value2(FltImageG *im, int *vld) {
//	return(im->Bilinear(x, y, vld)); }

double Coord2::Value2(DblImageG *im, int *vld) {
	return(im->Bilinear(x, y, vld)); }

Y_RGB<double> Coord2::Value2(DblImageC *im, int *vld) {
	return(im->Bilinear(x, y, vld)); }

double Coord2::Value3(BMPImageG *im, int *vld) {
	return(im->Bicubic(x, y, vld)); }

Y_RGB<double> Coord2::Value3(BMPImageC *im, int *vld) {
	return(im->Bicubic(x, y, vld)); }

//float Coord2::Value3(FltImageG *im, int *vld) {
//	return(im->Bicubic(x, y, vld)); }

double Coord2::Value3(DblImageG *im, int *vld) {
	return(im->Bicubic(x, y, vld)); }

Y_RGB<double> Coord2::Value3(DblImageC *im, int *vld) {
	return(im->Bicubic(x, y, vld)); }


void Coord2::Grad_Bilinear(BMPImageG *im, double& dx, double& dy, double step, int *vld)
{
	int vld1, vld2;
	if(vld) *vld = 1; // Default
	
	dx = im->Bilinear(x+step,y,&vld1)-im->Bilinear(x-step,y,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dx *= focal_length/(255.0*2.0*step);
	dy = im->Bilinear(x,y+step,&vld1)-im->Bilinear(x,y-step,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dy *= focal_length/(255.0*2.0*step);
}

void Coord2::Grad_Bilinear(DblImageG *im, double& dx, double& dy, double step, int *vld)
{
	int vld1, vld2;
	if(vld) *vld = 1; // Default
	
	dx = im->Bilinear(x+step,y,&vld1)-im->Bilinear(x-step,y,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dx *= focal_length/(255.0*2.0*step);
	dy = im->Bilinear(x,y+step,&vld1)-im->Bilinear(x,y-step,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dy *= focal_length/(255.0*2.0*step);
}

void Coord2::Grad_Bicubic(BMPImageG *im, double& dx, double& dy, double step, int *vld)
{
	int vld1, vld2;
	if(vld) *vld = 1; // Default
	
	dx = im->Bicubic(x+step,y,&vld1)-im->Bicubic(x-step,y,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dx *= focal_length/(255.0*2.0*step);
	dy = im->Bicubic(x,y+step,&vld1)-im->Bicubic(x,y-step,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dy *= focal_length/(255.0*2.0*step);
}

void Coord2::Grad_Bicubic(DblImageG *im, double& dx, double& dy, double step, int *vld)
{
	int vld1, vld2;
	if(vld) *vld = 1; // Default
	
	dx = im->Bicubic(x+step,y,&vld1)-im->Bicubic(x-step,y,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dx *= focal_length/(255.0*2.0*step);
	dy = im->Bicubic(x,y+step,&vld1)-im->Bicubic(x,y-step,&vld2);
	if(vld && (!vld1 || !vld2))  { *vld = 0; return; }
	dy *= focal_length/(255.0*2.0*step);
}

// Convolution with the kernel at given point of the image
void Coord2::GaussGrad(BMPImageG *im, double& dx, double& dy, int *vld)
{
	if(!kernel) {
		fprintf(stderr, "Kernel is not set.\n");
		return;
	}
	dx = Convolve(int(x), int(y), im, 0);
	dx *= focal_length/255.0;
	dy = Convolve(int(x), int(y), im, 1);
	dy *= focal_length/255.0;
	if(vld)
		*vld = 1; // Always valid!
}

// Use same kernel for X- and Y-derivatives.
// For Y-derivative 'i' and 'j' are flipped.
double Coord2::Convolve(int I, int J, BMPImageG *im, int flip)
{
	int half = (kernel->w-1)/2;
	double s = 0.0;
	I -= half;
	J -= half;
	
	for(int j = 0; j<kernel->h; j++) {
		for(int i = 0; i<kernel->w; i++) {
			if((I+i)>=0 && (I+i)<im->w && (J+j)>=0 && (J+j)<im->h) {
				if(flip)
					s += kernel->get_pixel(j, i)*im->get_pixel(I+i, J+j);
				else
					s += kernel->get_pixel(i, j)*im->get_pixel(I+i, J+j);
			}
		}
	}
	return(s);
}

// Note that sign of the kernel elements has been changed for direct
// comparison with the old gradient definition.
// Shouldn't matter, as only magnitude is important.
void Coord2::SetGradKernel(int sz)
{
	// Prevent multiple recalculation:
	if(kernel) {
		if(sz==kernel->w)
			return;
		else
			delete kernel;
	}
	double sigma = (1.0/6.0)*sz, sig2 = sigma*sigma, v, d2;
	double half = 0.5*(sz-1);
	kernel = new DblImageG(sz, sz);
	int i, j;
	
	for(j = 0; j<sz; j++) {
		for(i = 0; i<sz; i++) {
			d2 = (half-i)*(half-i)+(half-j)*(half-j);
			v = exp(-d2/(2.0*sig2));
			kernel->set_pixel(i, j, -v*(half-i)/sig2);
		}
	}
}

// Apply transformation to *this - general solution.
// Suppose 'derived' is translated with respect to 'base' by (10,0), and fl=1
// Then found RRA is A=0,Z=1,X=10,Y=0, and HM:[1 0 10; 0 1 0; 0 0]
// Point (0,0) (in 'derived') maps with 'hm' to (10,0) (in 'base').
// Usually inv_hm is needed, because we take point in 'base' and find
// which point in 'derived' contributes there.
// To avoid dependence on Vecd: number of elements 
// and data are passed separately
Coord2 Coord2::Map(int n, double *t) 
{
	double dd, xx, yy;
	double xn = Xn(), yn = Yn();
	
	switch(n) {
		case 2: // Translation
			xx = Xn()+t[0]; yy = Yn()+t[1];
			break;
		case 6: // Affine
			xx = t[0]*Xn()+t[1]*Yn()+t[2];
			yy = t[3]*Xn()+t[4]*Yn()+t[5];
			break;
		case 8: // Perspective
			dd = t[6]*Xn()+t[7]*Yn()+1.0;
			xx = (t[0]*Xn()+t[1]*Yn()+t[2])/dd;
			yy = (t[3]*Xn()+t[4]*Yn()+t[5])/dd;
			break;
		default:
			cout << "Can deal only with 2,6 or 8-parameter transformations"
				<< endl;
			exit(1);
	}
	static Coord2 mp; 
	// To inherit from this:
	mp.SetSize(w, h);
	mp.SetFocalLength(focal_length);
	// Set pixel locations
	mp.Set(xx*focal_length+0.5*w, yy*focal_length+0.5*h);
	//cout << "Mapped: " << mp << endl;
	return(mp);
}
