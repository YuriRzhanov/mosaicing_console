#include <stdlib.h>
#include <math.h>
#include "DblImageG.h"

using namespace std;


double DblImageG::INVALID = Y_Utils::_INVALID_DOUBLE;

DblImageG::DblImageG(int _w, int _h, double *data)
	: DblImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 1;
	FillHeader();
	//d_im = new double[w*h];
	d_im = (double*) new char[w*h*sizeof(double)];
	if(data) memcpy(d_im, data, w*h*sizeof(double));
	if(!data) {
		for(int i = 0; i<w*h; i++)
			d_im[i] = Y_Utils::_INVALID_DOUBLE;
	}
}

DblImageG::DblImageG(int _w, int _h, double init)
	: DblImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 1;
	FillHeader();
	//d_im = new double[w*h];
	d_im = (double*) new char[w*h*sizeof(double)];
	for(int i = 0; i<w*h; i++)
		d_im[i] = init;
}

// Copy of everything excluding content
DblImageG::DblImageG(DblImageG *d)
{
	w = d->w; h = d->h; fail = d->fail; focal_length = d->focal_length; z = 1;
	//d_im = new double[w*h];
	d_im = (double*) new char[w*h*sizeof(double)];
	memcpy(&header, &d->header, sizeof(DblHeader));
	for(int i = 0; i<w*h; i++)
		d_im[i] = Y_Utils::_INVALID_DOUBLE;
}

DblImageG::DblImageG(BMPImageG *img, int hasInvalid)
{
	w = img->w; h = img->h; fail = img->fail; focal_length = img->focal_length; z = 1;
	ranges_set = 0; even_zoom = 0;
	FillHeader();
	//d_im = new double[w*h];
	d_im = (double*) new char[w*h*sizeof(double)];
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(hasInvalid && img->im[j*w+i]==Y_Utils::_INVALID)
				d_im[j*w+i] = Y_Utils::_INVALID_DOUBLE;
			else
				d_im[j*w+i] = (double) img->im[j*w+i];
		}
	}
}

DblImageG::DblImageG(BMPImageC *img, int hasInvalid)
{
	w = img->w; h = img->h; fail = img->fail; focal_length = img->focal_length; z = 1;
	ranges_set = 0; even_zoom = 0;
	d_im = (double*) new char[w*h*sizeof(double)];
	FillHeader();
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(hasInvalid && !img->valid(i, j))
				d_im[j*w+i] = Y_Utils::_INVALID_DOUBLE;
			else
				d_im[j*w+i] = img->get_grey_pixel(i, j);
		}
	}
}

DblImageG::DblImageG(IntImageG *img, int hasInvalid)
{
	w = img->w; h = img->h; fail = img->fail; focal_length = img->focal_length; z = 1;
	ranges_set = 0; even_zoom = 0;
	FillHeader();
	//d_im = new double[w*h];
	d_im = (double*) new char[w*h*sizeof(double)];
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(hasInvalid && img->i_im[j*w+i]==IntImageG::INVALID)
				d_im[j*w+i] = Y_Utils::_INVALID_DOUBLE;
			else
				d_im[j*w+i] = (double) img->i_im[j*w+i];
		}
	}
}

DblImageG::DblImageG(const char *name, bool old)
{
	d_im = nullptr; ranges_set = 0; even_zoom = 0; z = 1;
	if(Read(name, old)==-1)
		fail = 1;
}

DblImageG *DblImageG::Copy()
{
	DblImageG *c = new DblImageG(w, h);
	memcpy(c->d_im, d_im, w*h*sizeof(double));
	c->SetFocalLength(focal_length);
	c->ranges_set = ranges_set; c->v_min = v_min; c->v_max = v_max;
	c->even_zoom = even_zoom; c->z = 1;
	memcpy(&c->header, &header, sizeof(DblHeader));
	return(c);
}

void DblImageG::Copy(DblImageG *d)
{
	if(w!=d->w || h!=d->h) {
		delete [] d_im;
		d_im = new double[d->w*d->h];
		w = d->w; h = d->h;
	}
	memcpy(d_im, d->d_im, w*h*sizeof(double));
	ranges_set = d->ranges_set; v_min = d->v_min; v_max = d->v_max;
	even_zoom = d->even_zoom; z = 1;
	focal_length = d->focal_length;
}

int DblImageG::Read(const char *name, bool old)
{
	if(DblImage::Read(name, old)) // sets dimensions, z, name
		return(-1);
	FILE *fp;
	if(fopen_s(&fp, name, "rb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
	if(old) {
		if(ReadDblHeader_old(fp))
			return(-1);
	} else {
		if(ReadHeader(fp))
			return(-1);
	}
	if(d_im!=nullptr)
		delete [] d_im;
	d_im = new double[w*h];
	if(fread(d_im, w*h*sizeof(double), 1, fp)!=1) {
		sprintf_s(error, 256, "Read \"%s\" fails.\n", name); return(-1);
	}
	if((old && endian) || (!old && !endian)) { // Convert to the opposite byte order
		for(int i = 0; i<w*h; i++)
			Y_Utils::Swap8Byte((u_char *) &d_im[i]);
	}
	return(0);
}

int DblImageG::Save(const char *name, bool old)
{
	FILE *fp;
	if(fopen_s(&fp, name, "wb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
	if(old) {
		if(WriteDblHeader_old(fp))
			return(-1);
	} else {
		if(WriteHeader(fp))
			return(-1);
	}
	if((old && endian) || (!old && !endian)) { // Convert to the opposite byte order
		double *_d_im = new double[w*h];
		memcpy(_d_im, d_im, w*h*sizeof(double));
		for(int i = 0; i<w*h; i++)
			Y_Utils::Swap8Byte((u_char *) &_d_im[i]);
		if(fwrite(_d_im, w*h*sizeof(double), 1, fp)!=1) {
			sprintf_s(error, 256, "Cannot write to \"%s\".\n", name);
			return(-1);
		}
		delete [] _d_im;
	} else {
		if(fwrite(d_im, w*h*sizeof(double), 1, fp)!=1) {
			sprintf_s(error, 256, "Cannot write to \"%s\".\n", name);
			return(-1);
		}
	}
	fclose(fp);
	return(0);
}

int DblImageG::SaveASCII(const char* file, bool saveIndexes)
{
	FILE* fp;
	if(fopen_s(&fp, file, "w"))
		return(-1);
	int i, j;
	if(saveIndexes) {
		fprintf(fp, "            ");
		for(i = 0; i<w; i++)
			fprintf(fp, "%12d", i);
		for(j = 0; j<h; j++) {
			fprintf(fp, "%12d", j);
			for(i = 0; i<w; i++)
				fprintf(fp, "%12.4g", d_im[i+j*w]);
			fprintf(fp, "\n");
		}
	} else {
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++)
				fprintf(fp, "%12.4g", d_im[i+j*w]);
			fprintf(fp, "\n");
		}
	}
	fclose(fp);
	return(0);
}

void DblImageG::SetColour(double v)
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++)
			d_im[j*w+i] = v;
	}
}

// Make sum of all pixels equal to '_sum'
bool DblImageG::Normalize(double _sum)
{
	double sum = 0.0;
	int i, j;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++)
			sum += d_im[j*w+i];
	}
	if(EPSILON_EQ(sum,0.0)) return(false); // cannot do normalization
	sum = _sum/sum;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++)
			d_im[j*w+i] *= sum;
	}
	return(true);
}

int DblImageG::PutAt(DblImageG *p, int x, int y)
{
	for(int j = 0; j<p->h; j++) {
		if((y+j)<0 || (y+j)>h)
			continue;
		for(int i = 0; i<p->w; i++) {
			if((x+i)<0 || (x+i)>w)
				continue;
			d_im[(y+j)*w+(x+i)] = p->d_im[j*p->w+i];
		}
	}
	return(0);
}

// invalid, if any corner is invalid
int DblImageG::valid(double x, double y)
{
	int i = int(x), j = int(y);
	if(valid(i,j) && valid(i+1,j) && valid(i,j+1) && valid(i+1,j+1))
		return(1);
	return(0);
}

// valid pixel, neighbouring invalid
int DblImageG::border(int x, int y)
{
	if(!valid(x, y)) return(0);
	for(int j = y-1; j<=y+1; j++) {
		for(int i = x-1; i<=x+1; i++) {
			if(!valid(i, j)) return(1);
		}
	}
	return(0);
}

int DblImageG::SaveBMP(const char *name, int rescale, double ignore_value)
{
	BMPImageG *img = Greyscale(rescale, ignore_value);
	int status = img->Save(name);
	delete img;
	return(status);
}

int DblImageG::SaveBMP(const char *name, double min, double max)
{
	BMPImageG *img = Greyscale(min, max);
	int status = img->Save(name);
	delete img;
	return(status);
}

int DblImageG::SaveBMPRainbow(const char *name, int type)
{
	BMPImageC* pBMPC = BMPRainbow(type);
	int status = pBMPC->Save(name);
	delete pBMPC;
	return(status);
	/*
	int i, j, k;
	double v;
	BMPImageC img(w, h);
	Y_RGB<u_char> vv;
	
	// Find range
	double a_max = -1.e9, a_min = 1.e9, scale;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				a_max = (a_max < v) ? v : a_max;
				a_min = (a_min > v) ? v : a_min;
			}
		}
	}
	if(a_min>=a_max)
		scale = 1.0;
	else {
		// used range is from 0 to 4*256-1
		scale = (256.0*4-1.0)/(a_max-a_min);
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				img.set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
			else {
				v = get_pixel(i, j);
				if(type==1)
					k = int(scale*(v-a_min));
				else if(type==-1)
					k = int(scale*(a_max-v));
				if(k<256) // Black->Blue spectrum 0:255
					vv.Set(0, 0, k);
				else if(k<2*256) // Blue-red spectrum 256:511
					vv.Set(k-256, 0, 511-k);
				else if(k<3*256) // Red-yellow 512:767
					vv.Set(255, k-512, 0);
				else // yellow-white 768:1023
					vv.Set(255, 255, k-768); 
				img.set_pixel(i, j, vv);
			}
		}
	}
	return(img.Save(name));
	*/
}

BMPImageC* DblImageG::BMPRainbow(int type)
{
	int i, j, k = 0;
	double v;
	BMPImageC* pBMPC = new BMPImageC(w, h);
	Y_RGB<u_char> vv;
	
	// Find range
	double a_max = -1.e9, a_min = 1.e9, scale;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				a_max = (a_max < v) ? v : a_max;
				a_min = (a_min > v) ? v : a_min;
			}
		}
	}
	if(a_min>=a_max)
		scale = 1.0;
	else {
		// used range is from 0 to 4*256-1
		scale = (256.0*4-1.0)/(a_max-a_min);
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				pBMPC->set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
			else {
				v = get_pixel(i, j);
				if(type==1)
					k = int(scale*(v-a_min));
				else if(type==-1)
					k = int(scale*(a_max-v));
				if(k<256) // Black->Blue spectrum 0:255
					vv.Set(0, 0, k);
				else if(k<2*256) // Blue-red spectrum 256:511
					vv.Set(k-256, 0, 511-k);
				else if(k<3*256) // Red-yellow 512:767
					vv.Set(BMPImageG::m_white, k-512, 0);
				else // yellow-white 768:1023
					vv.Set(BMPImageG::m_white, BMPImageG::m_white, k-768); 
				pBMPC->set_pixel(i, j, vv);
			}
		}
	}
	return(pBMPC);
}

/* Cut off subimage, taking care of image extent.
   at_x/y - top-left corner of cut
   sw/sh - requested size
   Area is initiated with invalid.
*/
DblImageG *DblImageG::CutSubimage(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	DblImageG *s = new DblImageG(sw, sh);
	for(i = 0; i<sw*sh; i++)
		s->d_im[i] = Y_Utils::_INVALID_DOUBLE;
	s->SetFocalLength(focal_length);
	s->ranges_set = ranges_set; s->v_min = v_min; s->v_max = v_max;
		
	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			s->d_im[jj*sw+ii] = d_im[j*w+i];
		}
	}
	return(s);
}

void DblImageG::Crop(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	double* d_im_ = new double[sw*sh];

	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			d_im_[jj*sw+ii] = d_im[j*w+i];
		}
	}
	delete [] d_im;
	d_im = d_im_;
	w = sw; h = sh;
}

double DblImageG::Bilinear(double dx, double dy, int *vld)
{
	int col, row, k;
	double v, invD = (double) Y_Utils::_INVALID_DOUBLE;
	double s00, s10, s01, s11;
	
	if(vld) *vld = 0;
	// Check for outside of image 
	if(dx<0.0 || dx>double(w-1) || dy<0.0 || dy>double(h-1)) return(invD);
	
	/* Integer parts */
	col = FLOOR(dx); row = FLOOR(dy);
	k = row*w+col;
	/* Fractional parts */
	dx -= (double) col; dy -= (double) row;
	if(dx<1.e-5 && dy<1.e-5) { // practically coinsides with a node
		if(vld && !isnan(d_im[k])) *vld = 1;
		return(d_im[k]);
	}
	// return INVALID, if outside the rectangle
	if(row<0 || row>h-1 || col<0 || col>w-1) {
		if(vld) *vld = 0;
		return(invD);
	}
	// If interested in pixel validity, check validity of all 4 pixels
	if(vld && isnan(d_im[k])) { *vld = 0; return(invD); }
	s00 = d_im[k];
	if(col!=w-1) {
		if(vld && isnan(d_im[k+1])) {*vld = 0; return(invD); }
		s10 = d_im[k+1];
	} else {
		s10 = d_im[k];
	}
	if(row!=h-1) {
		if(vld && isnan(d_im[k+w])) {*vld = 0; return(invD); }
		s01 = d_im[k+w];
	} else {
		s01 = d_im[k];
	}
	if(col!=(w-1) && row!=(h-1)) {
		if(vld && isnan(d_im[k+w+1])) {*vld = 0; return(invD); }
		s11 = d_im[k+w+1];
	} else {
		s11 = d_im[k];
	}
	if(vld) *vld = 1; // Validate pixel
	v = s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}

// Special case when all 4 nodes are guaranteed to exist
double DblImageG::Bilinear(double dx, double dy, int tl_i, int tl_j)
{
	int k;
	double s00, s10, s01, s11, v;
	
	k = tl_j*w+tl_i;
	// Fractional parts 
	dx -= (double) tl_i; dy -= (double) tl_j;
	if(dx<1.e-5 && dy<1.e-5) // practically coinsides with a node
		return(d_im[k]);
	if(fabs(dx-1.0)<1.e-5 && dy<1.e-5) 
		return(d_im[k+1]);
	if(dx<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(d_im[k+w]);
	if(fabs(dx-1.0)<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(d_im[k+w+1]);

	s00 = d_im[k];
	s10 = d_im[k+1];
	s01 = d_im[k+w];
	s11 = d_im[k+w+1];
	v = s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}

double DblImageG::Bicubic(double x, double y, int *vld)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	double v = 0.0, X, Y;

	// Surrounding pixels: (I,J)->(I+1,J+1)
	int If = (int)floor(x), Jf = (int)floor(y);
	int Ic = (int)ceil(x), Jc = (int)ceil(y);
		if(Jf<0) Jf = 0;
		if(Jc>h-1) Jc = h-1;
		if(If<0) If = 0;
		if(Ic>w-1) Ic = w-1;
	// If, Jf, Ic, Jc are guaranteed not to go out of range
	double V[16]; // 4 values, 4 X-derivatives, 4 Y-derivatives, 4 cross-derivatives

	V[ 0] = d_im[If+w*Jf];
	V[ 1] = d_im[Ic+w*Jf];
	V[ 2] = d_im[If+w*Jc];
	V[ 3] = d_im[Ic+w*Jc];
	V[ 4] = (If<1)   ? 0.0 : 0.5*(d_im[Ic+w*Jf]-d_im[If-1+w*Jf]);
	V[ 5] = (Ic>=w-1)   ? 0.0 : 0.5*(d_im[Ic+1+w*Jf]-d_im[If+w*Jf]);
	V[ 6] = (If<1) ? 0.0 : 0.5*(d_im[Ic+w*Jc]-d_im[If-1+w*Jc]);
	V[ 7] = (Ic>=w-1) ? 0.0 : 0.5*(d_im[Ic+1+w*Jc]-d_im[If+w*Jc]);
	V[ 8] = (Jf<1) ? 0.0 : 0.5*(d_im[If+w*Jc]-d_im[If+w*(Jf-1)]);
	V[ 9] = (Jf<1) ? 0.0 : 0.5*(d_im[Ic+w*Jc]-d_im[Ic+w*(Jf-1)]);
	V[10] = (Jc>=h-1) ? 0.0 : 0.5*(d_im[If+w*(Jc+1)]-d_im[If+w*Jf]);
	V[11] = (Jc>=h-1) ? 0.0 : 0.5*(d_im[Ic+w*(Jc+1)]-d_im[Ic+w*Jf]);
	V[12] = (If<1 || Jf<1) ? 0.0 : 0.25*(d_im[Ic+w*Jc]+d_im[If-1+w*(Jf-1)]-d_im[Ic+w*(Jf-1)]-d_im[If-1+w*Jc]);
	V[13] = (Ic>=w-1 || Jf<1) ? 0.0 : 0.25*(d_im[Ic+1+w*Jc]+d_im[If+w*(Jf-1)]-d_im[Ic+1+w*(Jf-1)]-d_im[If+w*Jc]);
	V[14] = (If<1|| Jc>=h-1) ? 0.0 : 0.25*(d_im[Ic+w*(Jc+1)]+d_im[If-1+w*Jf]-d_im[Ic+w*Jf]-d_im[If-1+w*(Jc+1)]);
	V[15] = (Ic>=w-1 || Jc>=h-1) ? 0.0 : 0.25*(d_im[Ic+1+w*(Jc+1)]+d_im[If+w*Jf]-d_im[Ic+1+w*Jf]-d_im[If+w*(Jc+1)]);

	if(vld) *vld = 1;
	
	NEWMAT::ColumnVector beta(16);
	beta<<V[0]<<V[1]<<V[2]<<V[3]<<V[4]<<V[5]<<V[6]<<V[7]<<V[8]<<V[9]<<V[10]<<V[11]<<V[12]<<V[13]<<V[14]<<V[15];

	NEWMAT::ColumnVector alpha = M_bicubic*beta;

	X = x-floor(x);
	Y = y-floor(y);
	for(int k = 0; k<16; k++) {
		v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
	}
	if(v<0.0) v = 0.0;
	if(v>255.0) v = 255.0;
	
	return(v);
}

void DblImageG::Log()
{
	double v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if((v = get_pixel(i,j))<=0.0)
				set_pixel(i, j, INVALID);
			else
				set_pixel(i, j, log(v));
		}
	}
}

void DblImageG::ScaleShift(double gain, double offset)
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)*gain+offset);
		}
	}
}

DblImageG** DblImageG::Split(int factor)
{
	DblImageG** pImgs = new DblImageG*[factor];
	int i, j, new_w = w/factor, nw;
	for(int n = 0; n<factor; n++) {
		// Last image could have different number of columns from the rest
		nw = (n<(factor-1)) ? new_w : w-new_w*n;
		pImgs[n] = new DblImageG(nw, h);
		for(j = 0; j<h; j++) {
			for(i = 0; i<nw; i++) {
				pImgs[n]->set_pixel(i, j, get_pixel(n*nw+i, j));
			}
		}
	}
	return(pImgs);
}

bool DblImageG::Subtract(DblImageG* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)-pDbl->get_pixel(i,j));
		}
	}
	return(true);
}

bool DblImageG::Add(DblImageG* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)+pDbl->get_pixel(i,j));
		}
	}
	return(true);
}

bool DblImageG::Multiply(DblImageG* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)*pDbl->get_pixel(i,j));
		}
	}
	return(true);
}

bool DblImageG::Divide(DblImageG* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	double v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = pDbl->get_pixel(i,j);
			if(!EPSILON_EQ(v,0.0))
				set_pixel(i, j, get_pixel(i,j)/v);
			else
				set_pixel(i, j, INVALID);
		}
	}
	return(true);
}

bool DblImageG::Max(DblImageG* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	double v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = pDbl->get_pixel(i,j);
			set_pixel(i, j, YMAX(v, get_pixel(i,j)));
		}
	}
	return(true);
}

bool DblImageG::Min(DblImageG* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	double v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = pDbl->get_pixel(i,j);
			set_pixel(i, j, YMIN(v, get_pixel(i,j)));
		}
	}
	return(true);
}

BMPImageG *DblImageG::Greyscale(int rescale, double ignore_value)
{
	int i, j;
	double v;
	u_char vsh;
	BMPImageG *img = new BMPImageG(w, h);
	img->SetFocalLength(focal_length);
	
	// Find total range
	double a_max = -1.e9, a_min = 1.e9, scale = 1.0;
	if(rescale) {
		MinMax(a_min, a_max, ignore_value);
		if(a_min>=a_max)
			scale = 1.0;
		else {
			// used range is from 1 to 255, 0 is left for invalid pixels
			scale = (255.0-1.0)/(a_max-a_min);
		}
		//fprintf(stderr, "Range=[%lf:%lf] Scale=%lf\n", a_min, a_max, scale);
	} else {
		if(ranges_set) { // Use pre-set ranges
			a_min = v_min; a_max = v_max;
			scale = (255.0-1.0)/(a_max-a_min);
		}
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(EPSILON_EQ(get_pixel(i, j),ignore_value)) {
				// substitute with 0
				img->set_pixel(i, j, (u_char) 0);
			} else {
				if(!valid(i, j))
					img->set_pixel(i, j, Y_Utils::_INVALID_UCHAR);
				else {
					v = get_pixel(i, j);
					if(rescale || ranges_set)
						v = scale*(v-a_min);
					if(v<0.0)
						img->set_pixel(i, j, u_char(1)); // Shouldn't happen
					else {
						vsh = (u_char)D_LIMIT_RANGE_CHAR(v);
						if(vsh==0)
							vsh = 1;
						img->set_pixel(i, j, vsh);
					}
				}
			}
		}
	}
	return(img);
}

// Conversion to greyscale with given ranges.
// 'min' maps to 1, 'max' to 255
BMPImageG *DblImageG::Greyscale(double a_min, double a_max)
{
	int i, j;
	double v, scale;
	BMPImageG *img = new BMPImageG(w, h);
	img->SetFocalLength(focal_length);
	
	if(a_min>=a_max)
			scale = 1.0;
	else {
		// used range is from 1 to 255, 0 is left for invalid pixels
		scale = (255.0-1.0)/(a_max-a_min);
	}
	//fprintf(stderr, "Range=[%lf:%lf] Scale=%lf\n", a_min, a_max, scale);
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				img->set_pixel(i, j, Y_Utils::_INVALID_UCHAR);
			else {
				v = scale*(get_pixel(i, j)-a_min);
				if(v>=0.0 && v<=255.0)
					img->set_pixel(i, j, (u_char)(1+v));
				else if(v>255.0)
					img->set_pixel(i, j, BMPImageG::m_white);
				else if(v<0.0)
					img->set_pixel(i, j, (u_char)1);
				else
					sprintf_s(error, 256, "DblImageG::Greyscale: pixel (%d,%d) out of range: %.2lf\n",
						i, j, v);
			}
		}
	}
	return(img);
}

void DblImageG::ValidateAll()
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(!valid(i, j))
				set_pixel(i, j, 0.0);
		}
	}
}

double DblImageG::AverageBrightness(double invalid_clr)
{
	double sum = 0.0, v;
	int count = 0;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = get_pixel(i,j);
			if(!EPSILON_EQ(v,invalid_clr)) {
				sum += v;
				count++;
			}
		}
	}
	return(sum/count);
}

int DblImageG::ZoomToDest_Bilinear(int dest_w, int dest_h)
{
	//int status = zoom_dbl_image(&im, w, h, dest_w, dest_h);
	double I, J;
	int vld, status = 0;
	double h_factor = double(w-1)/double(dest_w-1);
	double v_factor = double(h-1)/double(dest_h-1);
	double *Im = new double[dest_w*dest_h];
	for(int j = 0; j<dest_h-1; j++) {
		J = v_factor*j;
		for(int i = 0; i<dest_w-1; i++) {
			I = h_factor*i;
			Im[j*dest_w+i] = Bilinear(I, J, &vld);
			if(!vld) Im[j*dest_w+i] = Y_Utils::_INVALID_DOUBLE;
		}
		I = h_factor*(dest_w-1)-0.5;
		Im[j*dest_w+dest_w-1] = Bilinear(I, J, int(I), int(J));
	}
	// Last row - enforce interpolation from above row
	J = v_factor*(dest_h-1)-0.5;
	for(int i = 0; i<dest_w-1; i++) {
		I = h_factor*i;
		Im[(dest_h-1)*dest_w+i] = Bilinear(I, J, int(I), int(J));
	}
	I = h_factor*(dest_w-1)-0.5;
	Im[(dest_h-1)*dest_w+dest_w-1] = Bilinear(I, J, int(I), int(J));
	w = dest_w; h = dest_h;
	delete [] d_im;
	d_im = Im;
	return(status);
}

int DblImageG::Zoom_Bilinear(double zm, int flags)
{
	int status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bilinear(dest_w, dest_h);
	if(status==-1) {
		sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Pixels affected by invalid neighbours should be invalid
	// Now 'd_im' has dimensions dest_w*dest_h
	return(status);
}

int DblImageG::ZoomToDest_Bicubic(int dest_w, int dest_h)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	NEWMAT::ColumnVector alpha(16), beta(16);

	double *d_Im = new double[dest_w*dest_h];

	double x_scale = (double)dest_w/w;
	double y_scale = (double)dest_h/h;

	DblImageG* pDblXd = new DblImageG(w, h);
	DblImageG* pDblYd = new DblImageG(w, h);
	DblImageG* pDblXYd = new DblImageG(w, h);

	pDblXd->SetBlack();
	pDblYd->SetBlack();
	pDblXYd->SetBlack();

	int i, j, k;

	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(i==0 || i==w-1)
				pDblXd->set_pixel(i,j,0.0);
			else
				pDblXd->set_pixel(i,j,0.5*(get_pixel(i+1,j)-get_pixel(i-1,j)));
			if(j==0 || j==h-1)
				pDblYd->set_pixel(i,j,0.0);
			else
				pDblYd->set_pixel(i,j,0.5*(get_pixel(i,j+1)-get_pixel(i,j-1)));
			if(i==0 || i==w-1 || j==0 || j==h-1)
				pDblXYd->set_pixel(i,j,0.0);
			else
				pDblXYd->set_pixel(i,j,0.25*(get_pixel(i-1,j-1)+get_pixel(i+1,j+1)-get_pixel(i-1,j+1)+get_pixel(i+1,j-1)));
		}
	}

	// Normalized distances:
	double X, Y, I, J, fI, fJ, cI, cJ;
	Point2<int> I11, I21, I12, I22;
	double v;
	for(j = 0; j<dest_h; j++) {
		J = (double)j/y_scale;
		fJ = floor(J); cJ = ceil(J);
		if(fJ<0) fJ = 0;
		if(cJ>h-1) cJ = h-1;
		Y = J-fJ; // W
		for(i = 0; i<dest_w; i++) {
			I = (double)i/x_scale;
			fI = floor(I); cI = ceil(I);
			if(fI<0) fI = 0;
			if(cI>w-1) cI = w-1;
			X = I-fI; // H
			// Indexes of nearest pixels:
			I11.Set((int)fI, (int)fJ);
			I21.Set((int)cI, (int)fJ);
			I12.Set((int)fI, (int)cJ);
			I22.Set((int)cI, (int)cJ);
			// Beta vector:
			beta<<get_pixel(I11)<<get_pixel(I21)<<get_pixel(I12)<<get_pixel(I22)
				<<pDblXd->get_pixel(I11)<<pDblXd->get_pixel(I21)<<pDblXd->get_pixel(I12)<<pDblXd->get_pixel(I22)
				<<pDblYd->get_pixel(I11)<<pDblYd->get_pixel(I21)<<pDblYd->get_pixel(I12)<<pDblYd->get_pixel(I22)
				<<pDblXYd->get_pixel(I11)<<pDblXYd->get_pixel(I21)<<pDblXYd->get_pixel(I12)<<pDblXYd->get_pixel(I22);
			alpha = M_bicubic*beta;
			v = 0.0;
			for(k = 0; k<16; k++) {
				v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
			}
			if(v<0.0) v = 0.0;
			if(v>255.0) v = 255.0;
			d_Im[i+dest_w*j] = v;
		}
	}

	delete pDblXd;
	delete pDblYd;
	delete pDblXYd;

	w = dest_w; h = dest_h;
	delete [] d_im;
	d_im = d_Im;
	return(0);
}

int DblImageG::Zoom_Bicubic(double zm, int flags)
{
	int status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bicubic(dest_w, dest_h);
	if(status==-1) {
		sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Pixels affected by invalid neighbours should be invalid
	// Now 'd_im' has dimensions dest_w*dest_h
	return(status);
}

int DblImageG::Rotate_Bilinear(double angle_degrees, int flags)
{
	int i, j;
	double cos_a, sin_a, v;
	DblImageG *tmp = Copy();
	SetInvalid();

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bilinear(dx, dy, &vld);
			if(vld) set_pixel(i, j, v);
		}
	}
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

int DblImageG::Rotate_Bicubic(double angle_degrees, int flags)
{
	int i, j;
	double cos_a, sin_a, v;
	DblImageG *tmp = Copy();
	SetInvalid();

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bicubic(dx, dy, &vld);
			if(vld) set_pixel(i, j, v);
		}
	}
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

// 1: by pi/2, 2: by pi, 3: by -pi/2
void DblImageG::RotateQuarter(int nr_of_quarters)
{
	int new_w, new_h;
	if(nr_of_quarters == 2) {
		new_w = w; new_h = h;
	} else {
		new_w = h; new_h = w;
	}
	double *tmp = (double*)  new double[3*new_w*new_h];
	int i, j, n = sizeof(double);
	switch(nr_of_quarters) {
	case 1:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[j*new_w+i], &d_im[i*w+(w-1-j)], n);
		}}
		break;
	case 2:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[j*new_w+i], &d_im[(h-1-j)*w+(w-1-i)], n);
		}}
		break;
	case 3:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[j*new_w+i], &d_im[(h-1-i)*w+j], n);
		}}
		break;
	}
	delete [] d_im;
	d_im = tmp;
	w = new_w; h = new_h;
}

void DblImageG::Blur(int kernel, int times)
{
	if(kernel<3) {
		sprintf_s(error, 256, "Blur kernel<3, returning.\n");
		return;
	}
	if(times<1) {
		sprintf_s(error, 256, "Blur times<1, returning.\n");
		return;
	}
	if(kernel%2==0) {
		kernel++;
		sprintf_s(error, 256, "Kernel even, set to %d.\n", kernel);
	}
	int i, j, ii, jj, half = (kernel-1)/2, count, NaN;
	double sum;
	
	DblImageG *tmp = new DblImageG(w, h);
	
	for(int t = 0; t<times; t++) {
		tmp->Copy(this);
	
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				sum = 0; count = 0; NaN = 0;
				for(jj = j-half; jj<=j+half; jj++) {
					if(jj<0 || jj>=h) continue;
					for(ii = i-half; ii<=i+half; ii++) {
						if(ii<0 || ii>=w) continue;
						if(!(tmp->valid(ii, jj))) NaN++;
						sum += tmp->get_pixel(ii, jj);
						count++;
					}
				}
				if(!NaN)
					set_pixel(i, j, sum/count);
				else
					set_pixel(i, j, Y_Utils::_INVALID_DOUBLE);
			}
		}
	}
	delete tmp;
}

/* Difference between two sub-images which are supposed to be equivalent.
   xs, ys - shift of the image 'i' with respect to this. 
   Saves result in the file called 'name' if second argument is given. */
double DblImageG::difference_error_double(DblImageG *ig, double xs, double ys, 
	int *overlap, char *name)
{
	int i, j, n, vld;
	double dif = 0.0, v, vi;
	double ii, jj;
	int wi = ig->w, hi = ig->h;
	DblImageG *d = nullptr;
	char stg[256];
	
	if(name)
		d = new DblImageG(w, h);
	
	n = 0;
	// Every pixel in *this image is checked.
	// Pixel (i,j) gets contribution from location (ii,jj) of image I.
	for(j = 0; j<h; j++) {
		jj = (double)j+ys;
		if(jj<0.0 || jj>=hi)
			continue;
		for(i = 0; i<w; i++) {
			ii = (double)i+xs;
			if(ii<0.0 || ii>=wi)
				continue;
			//vi = ig->pfBilinear(ii, jj, &vld);
			vi = ig->Bilinear(ii, jj, &vld);
			if(valid(i, j)) {
				v = d_im[j*w+i];
				if(vld) {
					dif += fabs(v-vi);
					n++;
					if(d)
						d->d_im[j*w+i] = 0.5*(v+vi);
				} else {
					if(d)
						d->d_im[j*w+i] = v;
				}
			} else {
				if(vld && d)
					d->d_im[j*w+i] = vi;
			}
		}
	}
	if(debug)
		fprintf(stderr, "difference_error_double: total dif=%lf pixels=%d\n", dif, n);
	if(n)
		dif /= (double) n;
	else {
		fprintf(stderr, "No overlap between images.\n");
		dif = 0.0;
	}
	if(debug) {
		fprintf(stderr, "  dif=%lf\n", dif);
		sprintf_s(stg, 256, "I1.%s", DEXTN);
		Save(stg);
		sprintf_s(stg, 256, "I2.%s", DEXTN);
		ig->Save(stg);
		fprintf(stderr, "Images \"I1.dbl\" and \"I2.dbl\" saved.\n");
	}
		
	if(d) {
		d->Save(name);
		fprintf(stderr, "Image \"%s\" saved.\n", name);
		delete d;
	}
	*overlap = n;
	//ig->ResetPrefilter();
	
	return(dif); 
}


void DblImageG::MinMax(double& v_min, double& v_max, double ignore_value)
{
	double v;
	v_min = 1.e29; v_max = -1.e29;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(EPSILON_EQ(get_pixel(i, j),ignore_value))
				continue; // as if it does not exist
			if(valid(i, j)) {
				v = get_pixel(i, j);
				v_min = YMIN(v_min, v);
				v_max = YMAX(v_max, v);
			}
		}
	}
}

void DblImageG::MinMax(double& v_min, Point2<int>& p_min, double& v_max, Point2<int>& p_max, double ignore_value)
{
	double v;
	v_min = 1.e29; v_max = -1.e29;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(EPSILON_EQ(get_pixel(i, j),ignore_value))
				continue; // as if it does not exist
			if(valid(i, j)) {
				v = get_pixel(i, j);
				if(v<v_min) {
					v_min = v;
					p_min.Set(i, j);
				}
				if(v>v_max) {
					v_max = v;
					p_max.Set(i, j);
				}
			}
		}
	}
}

void DblImageG::Scale(double set_min, double set_max)
{
	double min, max, v;
	MinMax(min, max);
	double scale = (set_max-set_min)/(max-min);
	double isept = (set_min*max-set_max*min)/(max-min);

	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				set_pixel(i, j, scale*v+isept);
			}
		}
	}
}

void DblImageG::CutOffPercentile(double& min, double& max, double percentile)
{
	if(EPSILON_EQ(percentile,100.0)) return;
	
	int i, j, m, L = 1000, accum;
	double v;
	
	// Number of valid points:
	int valid_pts = 0;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++)
			if(valid(i, j)) valid_pts++;
	}
	int remove_pts = int((1.0-percentile/100.0)*valid_pts/2.0);
	// Divide into 0.1% bins
	int D[10000];
	memset(D, 0, L*sizeof(int));
	double step = (max-min)/(L-1);
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				m = int((v-min)/step);
				if(m<0 || m>=L) {
					fprintf(stderr, "M=%d\n", m);
					m = L-1;
				}
				++D[m];
			}
		}
	}
	accum = 0;
	m = 0;
	while(accum<remove_pts) {
		accum += D[m];
		m++;
	}
	double _min = min+(m-1)*step;
	accum = 0;
	m = L-1;
	while(accum<remove_pts) {
		accum += D[m];
		m--;
	}
	max = min+(m+1)*step;
	min = _min;
}

// April 2006: Detrending is moved to class Detrend
/*
void DblImageG::DetrendLaurie(int order, int verbose)
{
	int j, k;
	int px, py, nterms, order2, order2py, orderpy;
	double pix;
	double	s[2*MAXN+1], sz[MAXN];
	double	a[MAXN][MAXN], z[MAXN], b[MAXN], x, y, dx, dy, zz;
	double	aa[MAXN][MAXN], bb[MAXN];

	if(order<1 || order>2) {
		sprintf_s(error, 256, "Order of de-trending could be 1 or 2 only.\n");
		return;
	}
	
	nterms = (order+1)*(order+1);
	order2 = 2*order;
	
	// zero the sums
	for(py=0; py<=order2; py++) {
		order2py = (order2+1) * py;
		for(px=0; px<=order2; px++) {
			s[order2py + px] = 0.0;
		}
	}
	for(py=0; py<=order; py++) {
		orderpy = (order+1) * py;
		for(px=0; px<=order; px++) {
			sz[orderpy + px] = 0.0;
		}
	}
	// assume image is in range -1..1 in x and y 
	dy = 2.0/(h-1); dx=2.0/(w-1);
	y = -1.0;
	for(j=0; j<h; j++) {
		x = -1.0;
		for(k=0; k<w; k++) {
			if(!valid(k, h-1-j)) continue;
			pix = d_im[(h-1-j)*w+k];
			for(py=0; py<=order2; py++) {
				order2py = (order2+1) * py;
				for(px=0; px<=order2; px++) {
					s[order2py + px] += pow(x,px)*pow(y,py);
				}
			}
			for(py=0; py<=order; py++) {
				orderpy = (order+1) * py;
				for(px=0; px<=order; px++) {
					sz[orderpy+px] += pix*pow(x,px)*pow(y,py);
				}
			}
			x += dx;
		}
		y += dy;
	}

	// that all the sums, now form the equations to be solved 
	// max is 3rd order which means a (3+1)*(3+1)=16 * 16 square matrix
	switch(order) {
		case 0: a[0][0]=s[0]; b[0]=sz[0];
			break;
		case 1: a[0][0]=s[0]; a[0][1]=s[1]; a[0][2]=s[3]; a[0][3]=s[4];
			b[0]=sz[0]; 
			a[1][0]=s[1]; a[1][1]=s[2]; a[1][2]=s[4]; a[1][3]=s[5];
			b[1]=sz[1]; 
			a[2][0]=s[3]; a[2][1]=s[4]; a[2][2]=s[6]; a[2][3]=s[7];
			b[2]=sz[2]; 
			a[3][0]=s[4]; a[3][1]=s[5]; a[3][2]=s[7]; a[3][3]=s[8];
			b[3]=sz[3];
			break;
		case 2: a[0][0]=s[0]; a[0][1]=s[1]; a[0][2]=s[2]; a[0][3]=s[5];
			a[0][4]=s[6]; a[0][5]=s[7]; a[0][6]=s[10]; a[0][7]=s[11];
			a[0][8]=s[12]; b[0]=sz[0];
			a[1][0]=s[1]; a[1][1]=s[2]; a[1][2]=s[3]; a[1][3]=s[6];
			a[1][4]=s[7]; a[1][5]=s[8]; a[1][6]=s[11]; a[1][7]=s[12];
			a[1][8]=s[13]; b[1]=sz[1];
			a[2][0]=s[2]; a[2][1]=s[3]; a[2][2]=s[4]; a[2][3]=s[7];
			a[2][4]=s[8]; a[2][5]=s[9]; a[2][6]=s[12]; a[2][7]=s[13];
			a[2][8]=s[14]; b[2]=sz[2];
			a[3][0]=s[5]; a[3][1]=s[6]; a[3][2]=s[7]; a[3][3]=s[10];
			a[3][4]=s[11]; a[3][5]=s[12]; a[3][6]=s[15]; a[3][7]=s[16];
			a[3][8]=s[17]; b[3]=sz[3];
			a[4][0]=s[6]; a[4][1]=s[7]; a[4][2]=s[8]; a[4][3]=s[11];
			a[4][4]=s[12]; a[4][5]=s[13]; a[4][6]=s[16]; a[4][7]=s[17];
			a[4][8]=s[18]; b[4]=sz[4];
			a[5][0]=s[7]; a[5][1]=s[8]; a[5][2]=s[9]; a[5][3]=s[12];
			a[5][4]=s[13]; a[5][5]=s[14]; a[5][6]=s[17]; a[5][7]=s[18];
			a[5][8]=s[19]; b[5]=sz[5];
			a[6][0]=s[10]; a[6][1]=s[11]; a[6][2]=s[12]; a[6][3]=s[15];
			a[6][4]=s[16]; a[6][5]=s[17]; a[6][6]=s[20]; a[6][7]=s[21];
			a[6][8]=s[22]; b[6]=sz[6];
			a[7][0]=s[11]; a[7][1]=s[12]; a[7][2]=s[13]; a[7][3]=s[16];
			a[7][4]=s[17]; a[7][5]=s[18]; a[7][6]=s[21]; a[7][7]=s[22];
			a[7][8]=s[23]; b[7]=sz[7];
			a[8][0]=s[12]; a[8][1]=s[13]; a[8][2]=s[14]; a[8][3]=s[17];
			a[8][4]=s[18]; a[8][5]=s[19]; a[8][6]=s[22]; a[8][7]=s[23];
			a[8][8]=s[24]; b[8]=sz[8];
			break;
		case 3: fprintf(stderr, "Not computed yet\n"); exit(1);
			break;
	}

	// shift arrays to start at element 1 for gausselim()
	for(j=0; j<nterms; j++) {
		bb[j+1] = b[j];
		for(k=0; k<nterms; k++) {
			aa[j+1][k+1] = a[j][k];
		}
	}
	// solve the equations
	gausselim(aa, z, bb, nterms);
	
	// Trend surface
	double *ts = new double[w*h];
	double mean = 0.0, max_detrended = 0.0, res;
	y = -1.0;
	for(j=0; j<h; j++) {
		x = -1.0;
		for(k=0; k<w; k++) {
			if(!valid(k, h-1-j)) continue;
			pix = d_im[(h-1-j)*w+k];
			zz = 0.0;
			for(py=0; py<=order; py++) {
				orderpy = (order+1) * py;
				for(px=0; px<=order; px++) {
					zz += z[1+orderpy+px]*pow(x,px)*pow(y,py);
				}
			}
			ts[(h-1-j)*w+k] = zz-z[1];
			res = d_im[(h-1-j)*w+k]-zz+z[1];
			mean += res;
			if(res>max_detrended)
				max_detrended = res;
			x += dx;
		}
		y += dy;
	}
	mean /= double(w*h);

	// Detrending with surface found
	//int below = 0, above = 0;
	for(j = 0; j<h; j++) {
		for(k = 0; k<w; k++) {
			res = d_im[(h-1-j)*w+k]-ts[(h-1-j)*w+k];
			d_im[(h-1-j)*w+k] = res;
		}
	}
	if(verbose)
		fprintf(stderr, "Mean=%.3lf\n", mean);
	return;
}

NEWMAT::ColumnVector DblImageG::GetTrend(int detrend_order)
{
	int i, j;
	double v, x, y, xy, x2, y2;
	Coord2 c; c.SetImage(this);
	NEWMAT::ColumnVector r;
	
	if(detrend_order==0) {
		double sum = 0.0;
		int count = 0;
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(valid(i, j)) {
					sum += get_pixel(i, j);
					++count;
				}
			}
		}
		sum /= (double)count; // De-trending surface: horizontal plane
		r.ReSize(1);
		r(1) = sum;
	}
	if(detrend_order==1) {
		//fprintf(stderr, "Check whether matrix a should be transposed!\n");
		NEWMAT::Matrix a(3,3); a = 0.0;
		NEWMAT::ColumnVector b(3); b = 0.0;
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(valid(i, j)) {
					v = get_pixel(i, j);
					c.Set(i, j); x = c.Xn(); y = c.Yn();
					a(1,1) += 1.0; a(1,2) += x;   a(1,3) += y;
					a(2,1) += x;   a(2,2) += x*x; a(2,3) += x*y;
					a(3,1) += y;   a(3,2) += x*y; a(3,3) += y*y;
					b(1)   += v;   b(2)   += v*x; b(3)   += v*y;
				}
			}
		}
		//cout << a << endl;
		//cout << b << endl;
		r.ReSize(3);
		r = a.i()*b; // De-trending surface: tilted plane
		//cout << "Coeffs: " << r << endl;
	}
	if(detrend_order==2) {
		NEWMAT::Matrix a(6,6); a = 0.0;
		NEWMAT::ColumnVector b(6); b = 0.0;
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(valid(i, j)) {
					v = get_pixel(i, j);
					c.Set(i, j); x = c.Xn(); y = c.Yn(); xy = x*y; x2 = x*x; y2 = y*y;
					a(1,1) += 1.0; a(1,2) += x;    a(1,3) += y;    a(1,4) += x2;    a(1,5) += y2;    a(1,6) += xy;
					a(2,1) += x;   a(2,2) += x2;   a(2,3) += xy;   a(2,4) += x2*x;  a(2,5) += x*y2;  a(2,6) += x2*y;
					a(3,1) += y;   a(3,2) += xy;   a(3,3) += y2;   a(3,4) += x2*y;  a(3,5) += y2*y;  a(3,6) += x*y2;
					a(4,1) += x2;  a(4,2) += x2*x; a(4,3) += x2*y; a(4,4) += x2*x2; a(4,5) += x2*y2; a(4,6) += x2*xy;
					a(5,1) += y2;  a(5,2) += x*y2; a(5,3) += y2*y; a(5,4) += x2*y2; a(5,5) += y2*y2; a(5,6) += xy*y2;
					a(6,1) += xy;  a(6,2) += x2*y; a(6,3) += x*y2; a(6,4) += x2*xy; a(6,5) += xy*y2; a(6,6) += x2*y2;
					b(1)   += v;   b(2)   += v*x;  b(3)   += v*y;  b(4)   += v*x2;   b(5)   += v*y2; b(6)   += v*xy;
				}
			}
		}
		//cout << a << endl;
		//cout << b << endl;
		r.ReSize(6);
		r = a.i()*b; // De-trending surface: tilted plane
		//cout << "Coeffs: " << r << endl;
	}
	return(r);
}

// Negative values are allowed - no need to calculate the lowest value
void DblImageG::ApplyTrend(NEWMAT::ColumnVector& r)
{
	int i, j;
	double v, dt, x, y;
	Coord2 c; c.SetImage(this);

	if(r.Nrows()==1) { // detrend_order==0
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(valid(i, j)) {
					v = get_pixel(i, j)+r(1);
					set_pixel(i, j, v);
				} else
					set_pixel(i, j, Y_Utils::_INVALID_DOUBLE);
			}
		}
	}
	if(r.Nrows()==3) { // detrend_order==1
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(valid(i, j)) {
					c.Set(i, j);
					dt = r(1)+r(2)*c.Xn()+r(3)*c.Yn();
					v = get_pixel(i, j)+dt;
					set_pixel(i, j, v);
				} else
					set_pixel(i, j, Y_Utils::_INVALID_DOUBLE);
			}
		}
	}
	if(r.Nrows()==6) { // detrend_order==2
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(valid(i, j)) {
					c.Set(i, j); x = c.Xn(); y = c.Yn();
					dt = r(1)+r(2)*x+r(3)*y+r(4)*x*x+r(5)*y*y+r(6)*x*y;
					v = get_pixel(i, j)+dt;
					set_pixel(i, j, v);
				} else
					set_pixel(i, j, Y_Utils::_INVALID_DOUBLE);
			}
		}
	}
}
*/

// Find and set range which suits both images
void DblImageG::SetMutualRange(DblImageG *d1, DblImageG *d2, double percentile)
{
	double v_min_1 = 1.e9, v_min_2 = 1.e9, v_max_1 = -1.e9, v_max_2 = -1.e9;
	d1->MinMax(v_min_1, v_max_1);
	d2->MinMax(v_min_2, v_max_2);
	d1->CutOffPercentile(v_min_1, v_max_1, percentile);
	d2->CutOffPercentile(v_min_2, v_max_2, percentile);
	
	v_min_1 = YMIN(v_min_1, v_min_2);
	v_max_1 = YMAX(v_max_1, v_max_2);
	d1->SetRanges(v_min_1, v_max_1);
	d2->SetRanges(v_min_1, v_max_1);
}

DblImageG* DblImageG::GaussianKernel(int sz)
{
	double sigma = 0.5*sz/sqrt(6.0), d2, half, v;
	double sig2 = sigma*sigma;

	DblImageG* kernel = new DblImageG(sz, sz);
	half = 0.5*(sz-1);
	
	for(int j = 0; j<sz; j++) {
		for(int i = 0; i<sz; i++) {
			d2 = (half-i)*(half-i)+(half-j)*(half-j);
			v = exp(-d2/(2.0*sig2));
			kernel->set_pixel(i, j, v);
		}
	}
	return(kernel);
}

// Assuming cutoff at 0.05, deviation from center is = sigma*sqrt(6.0)
DblImageG* DblImageG::GaussianKernel(double sigma)
{
	int kernel_size = int(2.0*sigma*sqrt(6.0));
	if(kernel_size%2==0) kernel_size++;	
	return(GaussianKernel(kernel_size));
}

DblImageG* DblImageG::GaussianKernel(int sz, double sigma)
{
	double d2, half, v;
	double sig2 = sigma*sigma;

	DblImageG* kernel = new DblImageG(sz, sz);
	half = 0.5*(sz-1);
	
	for(int j = 0; j<sz; j++) {
		for(int i = 0; i<sz; i++) {
			d2 = (half-i)*(half-i)+(half-j)*(half-j);
			v = exp(-d2/(2.0*sig2));
			kernel->set_pixel(i, j, v);
		}
	}
	return(kernel);
}

DblImageG* DblImageG::HannKernel(int sz)
{
	DblImageG* kernel = new DblImageG(sz, sz);
	double vx, vy;
	for(int j = 0; j<sz; j++) {
		vy = 0.5*(1.0-cos(2.0*M_PI*j/(sz-1)));
		for(int i = 0; i<sz; i++) {
			vx = 0.5*(1.0-cos(2.0*M_PI*i/(sz-1)));
			kernel->set_pixel(i, j, vx*vy);
		}
	}
	return(kernel);
}
DblImageG* DblImageG::HammingKernel(int sz)
{
	DblImageG* kernel = new DblImageG(sz, sz);
	double vx, vy;
	for(int j = 0; j<sz; j++) {
		vy = 0.54-0.46*cos(2.0*M_PI*j/(sz-1));
		for(int i = 0; i<sz; i++) {
			vx = 0.54-0.46*cos(2.0*M_PI*i/(sz-1));
			kernel->set_pixel(i, j, vx*vy);
		}
	}
	return(kernel);
}
DblImageG* DblImageG::TukeyKernel(int sz, double alphaX, double alphaY)
{
	DblImageG* kernel = new DblImageG(sz, sz);
	double vx, vy;
	for(int j = 0; j<sz; j++) {
		if(j<=(int)(0.5*alphaY*sz))
			vy = 0.5*(1.0+cos(M_PI*(2.0*j/(alphaY*sz)-1.0)));
		else if(j<=(int)((1.0-0.5*alphaY)*sz))
			vy = 1.0;
		else
			vy = 0.5*(1.0+cos(M_PI*(2.0*j/(alphaY*sz)-2.0/alphaY+1.0)));
		for(int i = 0; i<sz; i++) {
			if(i<=(int)(0.5*alphaX*sz))
				vx = 0.5*(1.0+cos(M_PI*(2.0*i/(alphaX*sz)-1.0)));
			else if(i<=(int)((1.0-0.5*alphaX)*sz))
				vx = 1.0;
			else
				vx = 0.5*(1.0+cos(M_PI*(2.0*i/(alphaX*sz)-2.0/alphaX+1.0)));
			kernel->set_pixel(i, j, vx*vy);
		}
	}
	return(kernel);
}

double DblImageG::Convolve(int I, int J, DblImageG *kernel, int *n)
{
	int half = (kernel->w-1)/2, l = 0;
	double s = 0.0, v, vk;
	I -= half;
	J -= half;
	
	for(int j = 0; j<kernel->h; j++) {
		for(int i = 0; i<kernel->w; i++) {
			if(((I+i)>=0) && ((I+i)<=(w-1)) && ((J+j)>=0) && ((J+j)<=(h-1))) {
				v = get_pixel(I+i, J+j);
				vk = kernel->get_pixel(i, j);
				if(!isnan(v) && !isnan(vk)) {
					s += v*vk;
					l++;
				}
			}
		}
	}
	if(n) *n = l; // Number of valid pixels
	return(s);
}

double DblImageG::Convolve(double X, double Y, DblImageG *kernel, int *n)
{
	int half = (kernel->w-1)/2, l = 0;
	double s = 0.0, v, vk;
	X -= (double)half;
	Y -= (double)half;
	
	for(int j = 0; j<kernel->h; j++) {
		for(int i = 0; i<kernel->w; i++) {
			if(((X+i)>=0.0) && ((X+i)<=(double)(w-1)) && ((Y+j)>=0.0) && ((Y+j)<=(double)(h-1))) {
				v = get_pixel_D2(X+i, Y+j);
				vk = kernel->get_pixel(i, j);
				if(!isnan(v) && !isnan(vk)) {
					s += v*vk;
					l++;
				}
			}
		}
	}
	if(n) *n = l; // Number of valid pixels
	return(s);
}

DblImageG* DblImageG::Convolve(DblImageG *kernel, bool normalize)
{
	DblImageG *result = new DblImageG(w, h);
	result->SetBlack(); // don't need NaN's
	//int kw = (kernel->w-1)/2, kh = (kernel->h-1)/2;
	double r;
	int n;

	if(normalize) {
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				r = Convolve(i, j, kernel, &n);
				result->set_pixel(i, j, r/n);
			}
		}
	} else {
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				r = Convolve(i, j, kernel);
				result->set_pixel(i, j, r);
			}
		}
	}
	return(result);
}

bool DblImageG::LocalMaximum_4(int i, int j)
{
	int k = j*w+i;
	double v = d_im[k];
	if(v>=d_im[k-w] && v>=d_im[k-1] && v>=d_im[k+1] && v>=d_im[k+w])
		return(true);
	return(false);
}

bool DblImageG::LocalMaximum_8(int i, int j)
{
	int k = j*w+i;
	double v = d_im[k];
	if(v>=d_im[k-w-1] && v>=d_im[k-w] && v>=d_im[k-w+1] && v>=d_im[k-1] &&
		v>=d_im[k+1] && v>=d_im[k+w-1] && v>=d_im[k+w] && v>=d_im[k+w+1])
		return(true);
	return(false);
}

/*
DblImageG *DblImageG::Undistort_A_Bilinear(Y_Camera *C)
{
	DblImageG *ui = new DblImageG(w, h);
	ui->focal_length = focal_length;
	double p;
	Point2<int> d;
	Point2<double> u;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d.Set(i, j);
			u = C->DistortPixel(d);
			if(r.Inside(u)) {
				//p = pfBilinear(u.X(), u.Y());
				p = Bilinear(u.X(), u.Y());
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

DblImageG *DblImageG::Distort_A_Bilinear(Y_Camera *C)
{
	DblImageG *ui = new DblImageG(w, h);
	ui->focal_length = focal_length;
	double p;
	Point2<int> u;
	Point2<double> d;
	bool fail;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			u.Set(i, j);
			d = C->UndistortPixel(u, &fail);
			if(!fail && r.Inside(d)) {
				//p = pfBilinear(d.X(), d.Y());
				p = Bilinear(d.X(), d.Y());
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}
*/

DblImageG* DblImageG::operator *=(double s)
{
	double r;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			r = s*get_pixel(i,j);
			set_pixel(i, j, r);
		}
	}
	return(this);
}

/*
DblImageG* DblImageG::operator /=(DblImageG* dbl, double s)
{
	double r;
	s = 1.0/s;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			r = s*dbl->get_pixel(i,j);
			set_pixel(i, j, r);
		}
	}
	return(this);
}

DblImageG* DblImageG::operator +(DblImageG* dbl0, DblImageG* dbl)
{
	if(w!=dbl->w || h!=dbl->h) {
		sprintf_s(error, 256, "Images have different dimensions.\n");
		return(nullptr);
	}
	DblImageG* sum = new DblImageG(w, h);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			sum->set_pixel(i, j, dbl0->get_pixel(i,j)+dbl->get_pixel(i,j));
		}
	}
	return(sum);
}
*/



