#include "Hmgr.h"

bool Hmgr::IsUnit()
{
	if(h(1,1)!=1.0||h(2,2)!=1.0||h(3,3)!=1.0) return(0);
	if(h(1,2)!=0.0||h(1,3)!=0.0||h(2,1)!=0.0) return(0);
	if(h(2,3)!=0.0||h(3,1)!=0.0||h(3,2)!=0.0) return(0);
	return(1);
}

bool Hmgr::IsSingular()
{
	return(fabs(h.Determinant()) < HMGR_EPS);
}

void Hmgr::Set(double *d)
{
	for(int i = 0; i<3; i++) {
		for(int j = 0; j<3; j++)
			h(j+1,i+1) = d[3*i+j];
	}
}

void Hmgr::Set(double a00, double a01, double a02,
	double a10, double a11, double a12, double a20, double a21, double a22)
{
	h(1,1)=a00; h(1,2)=a01; h(1,3)=a02;
	h(2,1)=a10; h(2,2)=a11; h(2,3)=a12;
	h(3,1)=a20; h(3,2)=a21; h(3,3)=a22;
}

int Hmgr::Sane()
{
	for(int j = 0; j<3; j++) {
		for(int i = 0; i<3; i++) {
			if(isnan(h(j+1,i+1))) return(0);
			if(h(j+1,i+1)!=0.0 && fabs(h(j+1,i+1))<1.0e-300) return(0);
		}
	}
	return(1);
}

Hmgr Hmgr::Invert()
{
	if(IsSingular()) { // Should have checked prior to this!...
		fprintf(stderr, "Singular homography!\n");
		Hmgr unit;
		return(unit);
	}
	NEWMAT::Matrix inv_h = h.i();
	inv_h /= inv_h(3,3);
	Hmgr inv_hm(inv_h);
	return(inv_hm);
}

double Hmgr::DifEuclidean(const Hmgr& hm)
{
	double s = 0.0;
	for(int j = 0; j<3; j++) {
		for(int i = 0; i<3; i++)
			s += (h(j+1,i+1)-hm.h(j+1,i+1))*(h(j+1,i+1)-hm.h(j+1,i+1));
	}
	return(sqrt(s));
}

void Hmgr::Save(FILE *fp, char *name)
{
	if(name)
		fprintf(fp,
	"%20s [%12.8lf %12.8lf %12.8lf %12.8lf %12.8lf %12.8lf %12.8lf %12.8lf]\n",
		name, h(1,1), h(1,2), h(1,3), h(2,1), h(2,2), h(2,3), h(3,1), h(3,2));
	else
		fprintf(fp,
	"%20s [%12.8lf %12.8lf %12.8lf %12.8lf %12.8lf %12.8lf %12.8lf %12.8lf]\n",
		"-", h(1,1), h(1,2), h(1,3), h(2,1), h(2,2), h(2,3), h(3,1), h(3,2));
}

void Hmgr::Save(ofstream* of, char *name)
{
	if(name)
		*of<<setw(20)<<name<<" ["<<setw(13)<<setprecision(8)<<h(1,1)<<" "<<h(1,2)<<" "<<h(1,3)<<
			" "<<h(2,1)<<" "<<h(2,2)<<" "<<h(2,3)<<" "<<h(3,1)<<" "<<h(3,2)<<"]"<<endl;
	else
		*of<<setw(20)<<"-"<<" ["<<setw(13)<<setprecision(8)<<h(1,1)<<" "<<h(1,2)<<" "<<h(1,3)<<
			" "<<h(2,1)<<" "<<h(2,2)<<" "<<h(2,3)<<" "<<h(3,1)<<" "<<h(3,2)<<"]"<<endl;
}

void Hmgr::Read(FILE *fp, char *name)
{
	fscanf_s(fp, "%s %*c %lf %lf %lf %lf %lf %lf %lf %lf",
		name, (int)strlen(name)-1, &h(1,1), &h(1,2), &h(1,3), &h(2,1), &h(2,2), &h(2,3), &h(3,1), &h(3,2));
	h(3,3) = 1.0;
	if(!Sane()) {
		cout << "Insane homography: " << *this << endl;
		exit(1);
	}
}

int Hmgr::SaveStraight(const char *name)
{
	ofstream ofs(name);
	if(ofs.fail())
		return(-1);
	ofs << *this << endl;
	ofs.close();
	return(0);
}

// Binary format: *(13 chars for name)[(8*8 bytes)](2*8 bytes)=96 bytes
void Hmgr::SaveBin(FILE *fp, const char *name)
{
	char stg[16]; memset(stg, 0, 16); stg[0] = '*';
	if(name)
		strcpy_s(stg+1, 15, name);
	if(fwrite(stg, 13, 1, fp)!=1) { fprintf(stderr, "Cannot save Hmgr.\n"); exit(1);}
	if(fwrite("[", 1, 1, fp)!=1) { fprintf(stderr, "Cannot save Hmgr.\n"); exit(1);}
	for(int j = 0; j<3; j++) {
		for(int i = 0; i<3; i++) {
			if(j==2 && i==2) continue;
			if(fwrite(&h(j+1,i+1), sizeof(double), 1, fp)!=1) { fprintf(stderr, "Cannot save Hmgr.\n"); exit(1);}
		}
	}
	if(fwrite("]", 1, 1, fp)!=1) { fprintf(stderr, "Cannot save Hmgr.\n"); exit(1);}		
}

void Hmgr::ReadBin(FILE *fp, char *name)
{
	fseek(fp, 1, SEEK_CUR);
	if(fread(name, 13, 1, fp)!=1) { fprintf(stderr, "Cannot read Hmgr.\n"); exit(1);}
	fseek(fp, 1, SEEK_CUR);
	for(int j = 0; j<3; j++) {
		for(int i = 0; i<3; i++) {
			if(j==2 && i==2) continue;
			if(fread(&h(j+1,i+1), sizeof(double), 1, fp)!=1) { fprintf(stderr, "Cannot read Hmgr.\n"); exit(1);}
		}
	}
	if(!Sane()) {
		cout << "Insane homography: " << *this << endl;
		exit(1);
	}
}

int Hmgr::SaveBinFile(char *name)
{
	FILE *fp;
	if(fopen_s(&fp, name, "w")) {
		fprintf(stderr, "Cannot open %s for writing.\n", name);
		return(-1);
	}
	SaveBin(fp);
	fclose(fp);
	return(0);
}

// Check if satisfies conditions for RAR. Homography could be a result
// of calculation, so equality may not be exact.
int Hmgr::IsRigidAffine()
{
	if(fabs(h(3,1))>HMGR_EPS || fabs(h(3,2))>HMGR_EPS) return(0);
	if(fabs(h(1,1)-h(2,2))>HMGR_EPS) return(0);
	if(fabs(h(2,1)+h(1,2))>HMGR_EPS) return(0);
	return(1);
}

// Substitute for OptFramework::frame_image()
// Find quadrilateral - image of the rectangle with given transformation.
// Size of the mosaic (where image of the frame goes) is not involved.
// If the transform is unit, frame gives image coinciding with itself.
Y_Rect<int> Hmgr::FrameRect(Y_Rect<int> r, double focal_length)
{
	Y_Rect<double> R(100000.0, 100000.0, -100000.0, -100000.0);
	// R.w and R.h have meaning of top limits here.
	Coord2 corner, corner_image;
		corner.SetFocalLength(focal_length);
		corner.SetSize(r.w, r.h);
	corner.Set(r.x, r.y); corner_image = this->Map(corner);
	R.Encompass(corner_image);
	corner.Set(r.x+r.w-1, r.y); corner_image = this->Map(corner);
	R.Encompass(corner_image);
	corner.Set(r.x, r.y+r.h-1); corner_image = this->Map(corner);
	R.Encompass(corner_image);
	corner.Set(r.x+r.w-1, r.y+r.h-1); corner_image = this->Map(corner);
	R.Encompass(corner_image);
	// Reinstall meanings of R.w/h
	Y_Rect<int> Ri(int(R.x), int(R.y), int(R.w-R.x+1.0), int(R.h-R.y+1.0));
	return(Ri);
}

// Replacement for OptFramework::Overlap()
double Hmgr::Overlap(BMPImage* im1, BMPImage* im2)
{
	Coord2 c; // Point in the 2nd image
		c.SetImage(im2); // Point in the 2nd image
	Coord2 tc; // Point in the 1st image
		tc.SetImage(im1); // Point in the 1st image

	int count = 0, all_pixels = 0;
	for(int j = 0; j<im2->h; j++) { // Loop through pixels in the 2nd image
		for(int i = 0; i<im2->w; i++) {
			if(im2->valid(i, j)==0) continue;
			all_pixels++; // Count all valid pixels in 2nd image
			c.Set(i, j);
			// Find trasformation of the point - location in 1st image
			tc = this->Map(c);
			if(im1->valid(tc.X(), tc.Y()))
				count++; // Count all pixels in overlap
		}
	}
	double overlap_percent = 100.0*count/all_pixels;
	return(overlap_percent);
}

/* these two routines are performed by an external library GenPolyClipLib
	and are defined in a separate file Utilities/PolygonStuff.cpp
CPolygon* Hmgr::Polygon(Y_Image* im, Coord2* start)
{
	Coord2 c; 
	c.SetImage(im);
	CPolygon* p = new CPolygon();
	c.Set(0,             0); c = Map(c); 
	if(start) c += *start; 
	p->PushBack(c.X(), c.Y());
	c.Set(im->w-1,       0); c = Map(c); 
	if(start) c += *start; 
	p->PushBack(c.X(), c.Y());
	c.Set(im->w-1, im->h-1); c = Map(c); 
	if(start) c += *start; 
	p->PushBack(c.X(), c.Y());
	c.Set(0,       im->h-1); c = Map(c); 
	if(start) c += *start; 
	p->PushBack(c.X(), c.Y());
	return(p);
}

double Hmgr::OverlapPoly(Y_Image* im1, Y_Image* im2)
{
	Hmgr unit;
	CPolygon* Base = unit.Polygon(im1);
	CPolygon* Deri = Polygon(im2);
	vector<CPolygon*> Intersection;
	CAlgorithms::BooleanOp(CAlgorithms::Intersection, Base, Deri, &Intersection);
	CPolygon* pP = Intersection.at(0); // Could not be more than 1 polygon!

	double overlap_percent = 100.0*pP->Area()/Base->Area();
	delete Base;
	delete Deri;
	return(overlap_percent);
}
*/

// Replacement for OptFramework::TransError()
double Hmgr::AverageError(BMPImageG* im1, BMPImageG* im2, int verbose)
{
	Coord2 c; // Point in the 2nd image
		c.SetImage(im2); // Point in the 2nd image
	Coord2 tc; // Point in the 1st image
		tc.SetImage(im1); // Point in the 1st image

	BMPImageG *dif_im;
	if(verbose)
		dif_im = new BMPImageG(im1->w, im1->h);

	int count = 0, all_pixels = 0, vld_c, vld_tc;
	double v_c, v_tc, v_dif, totalError = 0.0;
	for(int j = 0; j<im2->h; j++) { // Loop through pixels in the 2nd image
		for(int i = 0; i<im2->w; i++) {
			c.Set(i, j);
			v_c = c.Value2(im2, &vld_c);
			if(vld_c) {
				all_pixels++; // Count all valid pixels in 2nd image
				// Find trasformation of the point - location in 1st image
				tc = this->Map(c);
				v_tc = tc.Value2(im1, &vld_tc);
				if(vld_tc) {
					v_dif = fabs(v_tc-v_c);
					totalError += v_dif;
					count++; // Count all pixels in overlap
					if(verbose) {
						dif_im->set_pixel(int(tc.X()), int(tc.Y()), v_dif);
					}
				}
			}
		}
	}
	if(verbose) {
		dif_im->Save("ErrorDif.bmp");
		delete dif_im;
	}
	return((count) ? totalError/count : 0.0);
}

// Replacement for OptFramework::TransError()
double Hmgr::AverageError(BMPImageC* im1, BMPImageC* im2, int verbose)
{
	Coord2 c; // Point in the 2nd image
		c.SetImage(im2); // Point in the 2nd image
	Coord2 tc; // Point in the 1st image
		tc.SetImage(im1); // Point in the 1st image

	BMPImageC *dif_im;
	if(verbose)
		dif_im = new BMPImageC(im1->w, im1->h);

	int count = 0, all_pixels = 0, vld_c, vld_tc;
	Y_RGB<double> v_c, v_tc, v_dif;
	double dif, totalError = 0.0;
	for(int j = 0; j<im2->h; j++) { // Loop through pixels in the 2nd image
		for(int i = 0; i<im2->w; i++) {
			c.Set(i, j);
			v_c = c.Value2(im2, &vld_c);
			if(vld_c) {
				all_pixels++; // Count all valid pixels in 2nd image
				// Find trasformation of the point - location in 1st image
				tc = this->Map(c);
				v_tc = tc.Value2(im1, &vld_tc);
				if(vld_tc) {
					v_dif = v_tc-v_c;
					dif = v_dif.ToDbl()/3.0;
					totalError += dif;
					count++; // Count all pixels in overlap
					if(verbose) {
						dif_im->set_pixel(int(tc.X()), int(tc.Y()), v_dif);
					}
				}
			}
		}
	}
	if(verbose) {
		dif_im->Save("ErrorDif.bmp");
		delete dif_im;
	}
	return((count) ? totalError/count : 0.0);
}

Hmgr operator *(Hmgr& a, Hmgr& b)
{
	NEWMAT::Matrix c = a.h * b.h;
	c /= c(3,3); // Normalize by last element
	Hmgr res(c);
	return(res);
}

Hmgr operator /(Hmgr& a, Hmgr& b)
{
	NEWMAT::Matrix c = a.h * b.h.i();
	c /= c(3,3); // Normalize by last element
	Hmgr res(c);
	return(res);
}

Hmgr operator +(const Hmgr& a, const Hmgr& b)
{
	Hmgr res;
	res.h = a.h + b.h;
	return(res);
}

Hmgr operator -(const Hmgr& a, const Hmgr& b)
{
	Hmgr res;
	res.h = a.h - b.h;
	return(res);
}

// Does not compile
//Hmgr& Hmgr::operator =(const Hmgr& v)
//{
//	h = v.h;
//	return(*this);
//}

// No horizontal translations, reprojection on the horizontal surface
// This function is called with 'focal_length'=1.0 and 'height' plays role of '1/zoom'.
// So I don't understand why I use these variables...
// 'PatchMap' however uses 'focal_length' different from 1, to accomodate for variable camera altitude.
// The lower the altitude, the smaller the footprint.
// Homography is in fact equivalent to the rotation matrix that converts 'down' into 'cameralook' vector,
// but with rearranged elements:
//        (R(3,3) R(3,1) R(3,2))
// Hmgr = (R(1,3) R(1,1) R(1,2))
//        (R(2,3) R(2,1) R(2,2))
Hmgr Hmgr::FromEuler(double focal_length, double height, 
	double pitch, double roll, double yaw, char *name)
{
	double sP, cP, sR, cR, sY, cY;
		
	pitch = Deg2Rad(pitch);
	roll = Deg2Rad(roll);
	yaw = Deg2Rad(yaw);
	sP = sin(pitch); cP = cos(pitch);
	sR = sin(roll); cR = cos(roll);
	sY = sin(yaw); cY = cos(yaw);
	
	Hmgr hm(
	height*(-sP*sY*sR+cY*cR), height*(-cP*sY), height*focal_length*(sP*sY*cR+cY*sR),
	height*(sP*cY*sR+sY*cR),  height*(cP*cY),  height*focal_length*(-sP*cY*cR+sY*sR),
	(-cP*sR),                 sP,              focal_length*cP*cR);
	// Normalization:
	hm.h = hm.h / hm.h(3,3);
	
	if(name) {
		hm.SaveStraight(name);
		fprintf(stderr, "Transform is saved in \"%s\".\n", name);		
		cout << "Transform:" << hm << endl;
	}
	return(hm);
}

// All 6 degrees of freedom, reprojection on a horizontal surface
Hmgr Hmgr::FromEuler(double focal_length, double height, 
	double pitch, double roll, double yaw, double Xs, double Ys, char *name)
{
	double sP, cP, sR, cR, sY, cY;
		
	pitch = Deg2Rad(pitch);
	roll = Deg2Rad(roll);
	yaw = Deg2Rad(yaw);
	sP = sin(pitch); cP = cos(pitch);
	sR = sin(roll); cR = cos(roll);
	sY = sin(yaw); cY = cos(yaw);
	
	Hmgr hm(
	height*(-sP*sY*sR+cY*cR), height*(-cP*sY), height*focal_length*(sP*sY*cR+cY*sR),
	height*(sP*cY*sR+sY*cR),  height*(cP*cY),  height*focal_length*(-sP*cY*cR+sY*sR),
	(-cP*sR),                 sP,              focal_length*(cP*cR));

	// Additions due to translation according to after-projection theory:
	hm.h(1,1) += Xs*(-cP*sR);
	hm.h(1,2) += Xs*(sP);
	hm.h(1,3) += Xs*focal_length*(cP*cR);
	hm.h(2,1) += Ys*(-cP*sR);
	hm.h(2,2) += Ys*(sP);
	hm.h(2,3) += Ys*focal_length*(cP*cR);
	
	// Normalization
	hm.h /= hm.h(3,3);
	
	if(name) {
		hm.SaveStraight(name);
		fprintf(stderr, "Transform is saved in \"%s\".\n", name);		
		cout << "Transform:" << hm << endl;
	}
	return(hm);
}

// Translational components in pixels (scaled internally)
//	Z is a scale factor
Hmgr Hmgr::FromEuler_Unscaled(double Z, double pitch, double roll, double yaw, double Xs, double Ys, 
	double focal_length, char *name)
{
	double sP, cP, sR, cR, sY, cY;
		
	pitch = Deg2Rad(pitch);
	roll = Deg2Rad(roll);
	yaw = Deg2Rad(yaw);
	sP = sin(pitch); cP = cos(pitch);
	sR = sin(roll); cR = cos(roll);
	sY = sin(yaw); cY = cos(yaw);
	Xs /= focal_length;
	Ys /= focal_length;
	
	Hmgr hm(
	(-sP*sY*sR+cY*cR)/Z, (-cP*sY)/Z, (sP*sY*cR+cY*sR)/Z,
	(sP*cY*sR+sY*cR)/Z,  (cP*cY)/Z,  (-sP*cY*cR+sY*sR)/Z,
	(-cP*sR),            sP,         (cP*cR));

	// Additions due to translation according to after-projection theory:
	hm.h(1,1) += Xs*(-cP*sR);
	hm.h(1,2) += Xs*(sP);
	hm.h(1,3) += Xs*(cP*cR);
	hm.h(2,1) += Ys*(-cP*sR);
	hm.h(2,2) += Ys*(sP);
	hm.h(2,3) += Ys*(cP*cR);
	
	// Normalization
	hm.h /= hm.h(3,3);

	if(name) {
		hm.SaveStraight(name);
		fprintf(stderr, "Transform is saved in \"%s\".\n", name);		
		cout << "Transform:" << hm << endl;
	}
	return(hm);
}

// Error function for 8-parameter homography (perspective transformation)
double Hmgr::T2E_Error(NEWMAT::ColumnVector Sol, NEWMAT::ColumnVector& E, double focal_length)
{
	double cY, sY, cP, sP, cR, sR;
	double Z = Sol(1), P = Sol(2), R = Sol(3), Y = Sol(4), 
		DX = Sol(5), DY = Sol(6);
	
	cP = cos(P); sP = sin(P);
	cR = cos(R); sR = sin(R);
	cY = cos(Y); sY = sin(Y);
	double U = cP*cR;
	
	E(1) = ((-sP*sY*sR+cY*cR)/Z+DX*(-cP*sR))/U - h(1,1)/focal_length;
	E(2) = ((-cP*sY)/Z+DX*(sP))/U              - h(1,2)/focal_length;
	E(3) = ((sP*sY*cR+cY*sR)/Z)/U+DX           - h(1,3);
	E(4) = ((sP*cY*sR+sY*cR)/Z+DY*(-cP*sR))/U  - h(2,1)/focal_length;
	E(5) = ((cY*cP)/Z+DY*(sP))/U               - h(2,2)/focal_length;
	E(6) = ((-sP*cY*cR+sY*sR)/Z)/U+DY          - h(2,3);
	E(7) = -sR/(cR)                            - h(3,1)/focal_length;
	E(8) = sP/(cP*cR)                          - h(3,2)/focal_length;
	
	double error = E(1)*E(1)+E(2)*E(2)+E(3)*E(3)+E(4)*E(4)+
		E(5)*E(5)+E(6)*E(6)+E(7)*E(7)+E(8)*E(8);
	return(error);
}

double Hmgr::PerspectiveParams(double focal_length, double& zoom, double& P, double& R, 
	double& Y, double& dx, double& dy)
{
	double cY, sY, cP, sP, cR, sR, U;
	int iter = 0;
	double Lambda = 1.000, prevError, currError;
	NEWMAT::Matrix dEdM(8, 6);
	NEWMAT::ColumnVector E(8), B(6);
	NEWMAT::Matrix A(6, 6);
	NEWMAT::ColumnVector Sol(6);
	
	// Initiate solution:
	Sol(1) = zoom; Sol(2) = P;  Sol(3) = R; 
	Sol(4) = Y;    Sol(5) = dx; Sol(6) = dy;
	
	// Estimate error at starting point
	prevError = T2E_Error(Sol, E, focal_length);

ITER:
	// Set gradient vector and Hessian matrix to zeroes.
	B = 0.0;
	A = 0.0;
	cY = cos(Sol(4)); sY = sin(Sol(4));
	cP = cos(Sol(2)); sP = sin(Sol(2));
	cR = cos(Sol(3)); sR = sin(Sol(3));
	U = cP*cR;
	
	T2E_Error(Sol, E, focal_length);
	
	// dEdM[k][0] => d(Pk)/d(zoom), dEdM[k][1] => d(Pk)/d(Pitch), dEdM[k][2] => d(Pk)/d(Roll),
	// dEdM[k][3] => d(Pk)/d(Yaw), dEdM[k][4] => d(Pk)/d(X), dEdM[k][5] => d(Pk)/d(Y),
	dEdM(1,1) = -(-sP*sY*sR+cY*cR)/(U*Sol(1)*Sol(1));
	dEdM(1,2) = (((-cP*sY*sR)/Sol(1)+Sol(5)*(sP*sR))*U-((-sP*sY*sR+cY*cR)/Sol(1)+Sol(5)*(-cP*sR))*(-sP*cR))/(U*U);
	dEdM(1,3) = (((-sP*sY*cR-cY*sR)/Sol(1)+Sol(5)*(-cP*cR))*U-((-sP*sY*sR+cY*cR)/Sol(1)+Sol(5)*(-cP*sR))*(cP*sR))/(U*U);
	dEdM(1,4) = (-sP*cY*sR-sY*cR)/(U*Sol(1));
	dEdM(1,5) = -sR/(cR);
	dEdM(1,6) = 0.0;
	
	dEdM(2,1) = sY/(cR*Sol(1)*Sol(1));
	dEdM(2,2) = (((sP*sY)/Sol(1)+Sol(5)*sP)*U-((-cP*sY)/Sol(1)+Sol(5)*(sP))*(-sP*cR))/(U*U);
	dEdM(2,3) = -((-cP*sY)/Sol(1)+Sol(5)*(sP))*(-sP*cR)/(U*U);
	dEdM(2,4) = -cY/(Sol(1)*cR);
	dEdM(2,5) = sP/(U);
	dEdM(2,6) = 0.0;
	
	dEdM(3,1) = -(sP*sY*cR+cY*sR)/(U*Sol(1)*Sol(1));
	dEdM(3,2) = ((1.0/Sol(1))*(cP*sY*cR)*U-(1.0/Sol(1))*(sP*sY*cR+cY*sR)*(-sP*cR))/(U*U);
	dEdM(3,3) = ((1.0/Sol(1))*(-sP*sY*sR+cY*cR)*U-(1.0/Sol(1))*(sP*sY*cR+cY*sR)*(-cP*sR))/(U*U);
	dEdM(3,4) = ((1.0/Sol(1))*(sP*sY*cR+cY*sR))/U;
	dEdM(3,5) = 1.0;
	dEdM(3,6) = 0.0;
	
	dEdM(4,1) = -(sP*cY*sR+sY*cR)/(U*Sol(1)*Sol(1));
	dEdM(4,2) = (((cP*cY*sR)/Sol(1)+Sol(6)*(sP*sR))*U-((sP*cY*sR+sY*cR)/Sol(1)+Sol(6)*(-cP*sR))*(-sP*cR))/(U*U);
	dEdM(4,3) = (((sP*cY*cR-sY*sR)/Sol(1)+Sol(6)*(-cP*cR))*U-((sP*cY*sR+sY*cR)/Sol(1)+Sol(6)*(-cP*sR))*(-cP*sR))/(U*U);
	dEdM(4,4) = (-sP*sY*sR+cY*cR)/(U*Sol(1));
	dEdM(4,5) = 0.0;
	dEdM(4,6) = -sR/(cR);
	
	dEdM(5,1) = -cY/(cR*Sol(1)*Sol(1));
	dEdM(5,2) = (((-cY*sP)/Sol(1)+Sol(6)*cP)*U-((cY*cP)/Sol(1)+Sol(6)*(sP))*(-sP*cR))/(U*U);
	dEdM(5,3) = -((cY*cP)/Sol(1)+Sol(6)*(sP))*(-cP*sR)/(U*U);
	dEdM(5,4) = -sY/(cR*Sol(1));
	dEdM(5,5) = 0.0;
	dEdM(5,6) = sP/(U);
	
	dEdM(6,1) = -(-sP*cY*cR+sY*sR)/(U*Sol(1)*Sol(1));
	dEdM(6,2) = ((1.0/Sol(1))*(-cP*cY*cR)*U-(1.0/Sol(1))*(-sP*cY*cR+sY*sR)*(-sP*cR))/(U*U);
	dEdM(6,3) = ((1.0/Sol(1))*(sP*cY*sR+sY*cR)*U-(1.0/Sol(1))*(-sP*cY*cR+sY*sR)*(-cP*sR))/(U*U);
	dEdM(6,4) = (1.0/Sol(1))*(sP*sY*cR+cY*sR)/U;
	dEdM(6,5) = 0.0;
	dEdM(6,6) = 1.0;
	
	dEdM(7,1) = 0.0;
	dEdM(7,2) = 0.0;
	dEdM(7,3) = -1.0/(cR*cR);
	dEdM(7,4) = 0.0;
	dEdM(7,5) = 0.0;
	dEdM(7,6) = 0.0;

	dEdM(8,1) = 0.0;
	dEdM(8,2) = 1.0/(cR*cP*cP);
	dEdM(8,3) = sP*sR/(cP*cR*cR);
	dEdM(8,4) = 0.0;
	dEdM(8,5) = 0.0;
	dEdM(8,6) = 0.0;
	
	// Accumulate in the Hessian matrix and gradient vector
	for(int k = 0; k<6; k++) {
		for(int i = 0; i<8; i++) {
			B(k+1) -= 2.0*E(i+1)*dEdM(i+1,k+1);
		}
		for(int l = 0; l<6; l++) {
			for(int i = 0; i<8; i++) {
				A(k+1,l+1) += dEdM(i+1,k+1)*dEdM(i+1,l+1);
				A(l+1,k+1) += dEdM(i+1,k+1)*dEdM(i+1,l+1);
			}
		}
	}
	//SaveMatrices("M.txt", B, A);
	
AGAIN:
	// Instead of matrix A, use At, which has increased diagonal elements
	NEWMAT::Matrix At = A;
	for(int i = 0; i<6; i++) {
		At(i+1,i+1) = A(i+1,i+1)*(1.0+Lambda);
	}
	NEWMAT::ColumnVector Res = At.i()*B;
	//double residual = SolveOverRelax(At, Res, B, 1.e-12);
	NEWMAT::ColumnVector updTr = Sol+Res;
	currError = T2E_Error(updTr, E, focal_length);
	
	if(Res.SumSquare()<1.e-12*Sol.SumSquare()) {
		zoom = Sol(1); P   = Sol(2);  R = Sol(3); 
		Y = Sol(4);    dx  = Sol(5); dy = Sol(6);
		if(Y>M_PI) Y -= 2.0*M_PI;
		if(Y<-M_PI) Y += 2.0*M_PI;
		return(currError);
	}
	// Update transformation
	if(currError<prevError) {
		Lambda /= 10.0;
		Sol += Res;
	} else {
		Lambda *= 10.0;
		goto AGAIN;
	}
	prevError = currError;
	iter++;

	goto ITER;
}

// Error function for 6-parameter homography (rigid affine transformation)
double Hmgr::T2RA_Error(NEWMAT::ColumnVector Sol, NEWMAT::ColumnVector& E)
{
	double cY = cos(Sol(2)), sY = sin(Sol(2));
	
	E(1) =  cY/Sol(1) - h(1,1);
	E(2) = -sY/Sol(1) - h(1,2);
	E(3) =  Sol(3)    - h(1,3);
	E(4) =  sY/Sol(1) - h(2,1);
	E(5) =  cY/Sol(1) - h(2,2);
	E(6) =  Sol(4)    - h(2,3);
	
	double error = E(1)*E(1)+E(2)*E(2)+E(3)*E(3)+E(4)*E(4)+
		E(5)*E(5)+E(6)*E(6);
	return(error);
}

double Hmgr::RigidAffineParams(double& zoom, double& Y, 
	double& dx, double& dy)
{
	// If the record is rigid affine, don't use optimization. Result is exact.
	if(IsRigidAffine()) {
		zoom = 1.0/sqrt(h(1,1)*h(1,1)+h(1,2)*h(1,2));
		Y = atan2(-h(1,2), h(1,1));
		dx = h(1,3);
		dy = h(2,3);
		return(0.0);
	}
	
	double cY, sY;
	int iter = 0;
	double Lambda = 1.000, prevError, currError;
	NEWMAT::Matrix dEdM(6, 4);
	NEWMAT::ColumnVector E(6), B(4);
	NEWMAT::Matrix A(4, 4);
	NEWMAT::ColumnVector Sol(4);
	
	// Initiate solution:
	Sol(1) = zoom; Sol(2) = Y;
	Sol(3) = dx;   Sol(4) = dy;
	
	// Estimate error at starting point
	prevError = T2RA_Error(Sol, E);

ITER:
	// Set gradient vector and Hessian matrix to zeroes.
	B = 0.0;
	A = 0.0;
	cY = cos(Sol(2)); sY = sin(Sol(2));
	
	T2RA_Error(Sol, E);

	dEdM(1,1) = -cY/(Sol(1)*Sol(1));
	dEdM(1,2) = -sY/Sol(1);
	dEdM(1,3) = 0.0;
	dEdM(1,4) = 0.0;
	
	dEdM(2,1) = sY/(Sol(1)*Sol(1));
	dEdM(2,2) = -sY/Sol(1);
	dEdM(2,3) = 0.0;
	dEdM(2,4) = 0.0;
	
	dEdM(3,1) = 0.0;
	dEdM(3,2) = 0.0;
	dEdM(3,3) = 1.0;
	dEdM(3,4) = 0.0;
	
	dEdM(4,1) = -sY/(Sol(1)*Sol(1));
	dEdM(4,2) = cY/Sol(1);
	dEdM(4,3) = 0.0;
	dEdM(4,4) = 0.0;
	
	dEdM(5,1) = -cY/(Sol(1)*Sol(1));
	dEdM(5,2) = -sY/Sol(1);
	dEdM(5,3) = 0.0;
	dEdM(5,4) = 0.0;
	
	dEdM(6,1) = 0.0;
	dEdM(6,2) = 0.0;
	dEdM(6,3) = 0.0;
	dEdM(6,4) = 1.0;
	
	// Accumulate in the Hessian matrix and gradient vector
	for(int k = 0; k<4; k++) {
		for(int i = 0; i<6; i++) {
			B(k+1) -= 2.0*E(i+1)*dEdM(i+1,k+1);
		}
		for(int l = 0; l<4; l++) {
			for(int i = 0; i<6; i++) {
				A(k+1,l+1) += dEdM(i+1,k+1)*dEdM(i+1,l+1);
				A(l+1,k+1) += dEdM(i+1,k+1)*dEdM(i+1,l+1);
			}
		}
	}
	
AGAIN:
	// Instead of matrix A, use At, which has increased diagonal elements
	NEWMAT::Matrix At = A;
	for(int i = 0; i<4; i++) {
		At(i+1,i+1) = A(i+1,i+1)*(1.0+Lambda);
	}
	NEWMAT::ColumnVector Res = At.i()*B;
	//double residual = SolveOverRelax(At, Res, B, 1.e-12);
	NEWMAT::ColumnVector updTr = Sol+Res;
	currError = T2RA_Error(updTr, E);

	if(Res.SumSquare()<1.e-12*Sol.SumSquare()) {
		zoom = Sol(1); Y = Sol(2); dx  = Sol(3); dy = Sol(4);
		if(Y>M_PI) Y -= 2.0*M_PI;
		if(Y<-M_PI) Y += 2.0*M_PI;
		return(currError);
	}
	// Update transformation
	if(currError<prevError) {
		Lambda /= 10.0;
		Sol += Res;
	} else {
		Lambda *= 10.0;
		goto AGAIN;
	}
	prevError = currError;
	iter++;

	goto ITER;
}

Coord2 Hmgr::Map(Coord2& c) 
{
	double dd, xx, yy;
	double xn = c.Xn(), yn = c.Yn();
	
	dd = h(3,1)*xn+h(3,2)*yn+1.0;
	xx = (h(1,1)*xn+h(1,2)*yn+h(1,3))/dd;
	yy = (h(2,1)*xn+h(2,2)*yn+h(2,3))/dd;
	static Coord2 mp; 
	// To inherit from this:
	mp.SetSize(c.w, c.h);
	mp.SetFocalLength(c.focal_length);
	// Set pixel locations
	mp.Set(xx*c.focal_length+0.5*c.w, yy*c.focal_length+0.5*c.h);
	//cout << "Mapped: " << mp << endl;
	return(mp);
}





// --- Input & Output -----------------------------------------------------
ostream &operator << (ostream &s, const Hmgr &v)
{
	s << setw(16) << setprecision(12) << "[ " << v.h(1,1) << ' ' << v.h(1,2) << ' ' << v.h(1,3) 
		<< ' ' << v.h(2,1) << ' ' << v.h(2,2) << ' ' << v.h(2,3) 
		<< ' ' << v.h(3,1) << ' ' << v.h(3,2)  << " ]";
	
	return(s);
}

istream &operator >> (istream &s, Hmgr &v)
{
	s >> v.h(1,1) >> v.h(1,2) >> v.h(1,3) 
		>> v.h(2,1) >> v.h(2,2) >> v.h(2,3) 
		>> v.h(3,1) >> v.h(3,2);
	v.h(3,3) = 1.0;
	
	if(!v.Sane()) {
		cout << "Insane homography: " << v << endl;
		exit(1);
	}

    return(s);
}

