#include "ImageSequence.h"

char ImageRecord::stg[1024];
char ImageRecord::buf[1024];

void ImageRecord::Copy(ImageRecord& rec)
{
	k = rec.k;
	SetTimestamp(rec.timestamp);
	SetNav(rec.x, rec.y, rec.alt, rec.depth);
	SetAtt(rec.pitch, rec.roll, rec.yaw);
	SetFullPath(rec.full_path);
}

void ImageRecord::SetFromString(const char* _stg)
{
	memset(buf, 0, 1024);
	sscanf_s(_stg, "%d %s %s %lf %lf %lf %lf %lf %lf %lf", 
		&k, timestamp, 12, buf, 1024, &x, &y, &alt, &depth, &pitch, &roll, &yaw);
	full_path = buf;
}

int ImageRecord::Read(FILE* fp)
{
	// Read until the line is not mark
	do {
		if(fgets(stg, 1023, fp)==nullptr) // End of logfile
			return(-100);
	} while(stg[0]=='#' || strlen(stg)<3);
	
	SetFromString(stg);
	return(k);
}

char* ImageRecord::SaveStg()
{
	sprintf_s(stg, (size_t) 1024, "%06d %11s %80s %8.2lf %8.2lf %8.2lf %8.2lf %8.2lf %8.2lf %8.2lf", 
		k, timestamp, full_path, x, y, alt, depth, pitch, roll, yaw);
	return(stg);
}

int ImageRecord::Save(FILE* fp)
{
	fprintf(fp, "%s\n", SaveStg());
	return(0);
}

// Assuming that file name is "path/prefixDDDDDD.bmp", insert a modifier after prefix
void ImageRecord::InsertModifier(char* modifier)
{
	strcpy_s(buf, 1024, full_path.c_str());
	buf[strlen(buf)-10] = 0;
	char* p = strrchr(buf, '.')-6;
	sprintf_s(stg, (size_t) 1024, "%s%s%s", buf, modifier, p);
	full_path = stg;
}

// Insert just before dot and extension
void ImageRecord::InsertModifierBeforeDot(char* modifier)
{
	strcpy_s(buf, 1024, full_path.c_str());
	char* p = strrchr(buf, '.');
	char extension[16];
	strcpy_s(extension, 16, p+1);
	*p = '\0';
	sprintf_s(stg, (size_t) 1024, "%s%s.%s", buf, modifier, extension);
	SetFullPath(stg);
}

// Assuming filename standard, set number to the given
void ImageRecord::SetNumber(int num)
{
	strcpy_s(buf, 1024, full_path.c_str());
	buf[strlen(buf)-10] = 0;
	char* p = strrchr(buf, '.');
	char stg[1024];
	sprintf_s(stg, 1024, "%s%06d%s", buf, num, p);
	full_path = buf;
}


//##############################################################
char ImageSequence::buf[1024];

ImageSequence::ImageSequence(const char* filename)
	: n(0), fail(false), w(0), h(0), fl(-1.0), FoV_h(0.0)
{
	if(SetMetadata(filename)==false) {
		sprintf_s(buf, (size_t) 1024, "Cannot set metadata.\n");
		n = -1;
		fail = true;
		return;
	}

	FILE *fp_log;
	if(fopen_s(&fp_log, filename, "r")) {
		fprintf(stderr, "Cannot open %s.\n", filename);
		n = -1;
		fail = true;
		return;
	}
	ImageRecord rec;
	n = 0;
	while(rec.Read(fp_log)>-1)
		n++;
	fprintf(stderr, "Number of records in the ImageSequence = %d\n", n);
	records = new ImageRecord[n];
	rewind(fp_log);
	for(int i = 0; i<n; i++) {
		records[i].Read(fp_log);
	}
	fclose(fp_log);
}

int ImageSequence::ReadRecords(const char* filename)
{
	if(records)
		delete [] records;
	fail = false;

	if(SetMetadata(filename)==false) {
		sprintf_s(buf, (size_t) 1024, "Cannot set metadata.\n");
		n = -1;
		fail = true;
		return(0);
	}

	FILE *fp;
	if(fopen_s(&fp, filename, "r")) {
		fprintf(stderr, "Cannot open %s.\n", filename);
		fail = true;
		return(0);
	}
	ImageRecord rec;
	n = 0;
	while(rec.Read(fp)>-1)
		n++;
	fprintf(stderr, "Number of records in the ImageSequence = %d\n", n);
	records = new ImageRecord[n];
	rewind(fp);
	for(int i = 0; i<n; i++) {
		records[i].Read(fp);
	}
	fclose(fp);
	
	return(n);
}

bool ImageSequence::SetMetadata(const char* filename)
{
	FILE *fp;
	if(fopen_s(&fp, filename, "r")) {
		sprintf_s(buf, (size_t) 1024, "Cannot open %s.\n", filename);
		return(false);
	}
	char stg[256];
	do {
		if(fgets(stg, 255, fp)==nullptr) return(false);
	} while(SetMetadataFromString(stg)==false);
	fclose(fp);
	return(true);
}

bool ImageSequence::SetMetadataFromString(const char* stg)
{
	bool success = true;
	char* p;
	if(p = strstr((char*)stg, "W=")) sscanf_s(p+2, "%d", &w);
	else { w = -1; success = false; }
	if(p = strstr((char*)stg, "H=")) sscanf_s(p+2, "%d", &h);
	else { h = -1; success = false; }
	if(p = strstr((char*)stg, "FL=")) sscanf_s(p+3, "%lf", &fl);
	else { fl = -1.0; }
	if(p = strstr((char*)stg, "FOVH=")) sscanf_s(p+5, "%lf", &FoV_h);
	else { FoV_h = 0.0; }
	//if(p = strstr(stg, "N="))
	//	sscanf_s(p+2, "%d", &n);
	//else {
	//	n = -1;
	//	success = false;
	//}
	return(success);
}

// Assuming that file name is "path/prefixDDDDDD.bmp", insert a modifier after prefix
void ImageSequence::InsertModifier(char* filename, char* modifier)
{
	char stg[256], prefix[256];
	strcpy_s(prefix, 256, filename);
	prefix[strlen(prefix)-10] = 0;
	char* p = strrchr(filename, '.')-6;
	sprintf_s(stg, (size_t) 1024, "%s%s%s", prefix, modifier, p);
	delete [] filename;
	filename = new char[strlen(stg)+1];
	strcpy_s(filename, strlen(stg)+1, stg);
}

	// Assuming filename standard, set number to the given
void ImageSequence::SetNumber(char* filename, int num)
{
	char prefix[256];
	strcpy_s(prefix, 256, filename);
	prefix[strlen(prefix)-10] = 0;
	char* p = strrchr(filename, '.');
	sprintf_s(filename, (size_t) strlen(filename), "%s%06d%s", prefix, num, p);
}

int ImageSequence::Save(const char* filename)
{
	// Prior to saving, check whether all the required records were used, because
	// it may be that there were more allocated than used
	CleanSequence();

	FILE* fp;
	if(fopen_s(&fp, filename, "w")) {
		sprintf_s(buf, (size_t) 1024, "Cannot open \"%s\" for writing.\n", filename);
		return(-1);
	}
	fprintf(fp, "# W=%d H=%d N=%d FL=%8.2lf FOVH=%8.2lf\n", w, h, n, fl, FoV_h);
	for(int i = 0; i<n; i++)
		records[i].Save(fp);
	fclose(fp);
	return(0);
}

void ImageSequence::CleanSequence()
{
	// Count records with k!=-1
	int i, _n = n;
	for(i = 0; i<n; i++) {
		if(records[i].k==-1) {
			_n = i;
			break;
		}
	}
	if(_n==n) return; // all records have been used

	ImageRecord* _records = new ImageRecord[_n];
	for(i = 0; i<_n; i++)
		_records[i].Copy(records[i]);
	n = _n;
	delete [] records;
	records = _records;
}

char* ImageSequence::Info(const char* name)
{
	sprintf_s(buf, (size_t) 1024, "#Info \"%s\":\n#   Sequence of BMPs.\n"
		"#   Track length=%d   Frame size: (%d*%d)\n"
		"#   Type: %s   Compression: %s\n"
		"#   Comment: <FL=%.3lf  FoV=%.2lf>\n", 
		name, n, w, h, 
		"BMP", "RGB", 
		fl, FoV_h);
	return(buf);
}

