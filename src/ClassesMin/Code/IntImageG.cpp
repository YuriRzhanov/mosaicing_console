#include <stdlib.h>
#include <math.h>
#include "IntImageG.h"

using namespace std;


int IntImageG::INVALID = Y_Utils::_INVALID_INT;

IntImageG::IntImageG(int _w, int _h, int *data)
	: IntImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 1;
	FillHeader();
	i_im = (int*) new char[w*h*sizeof(int)];
	if(data) memcpy(i_im, data, w*h*sizeof(int));
	if(!data) {
		for(int i = 0; i<w*h; i++)
			i_im[i] = INVALID;
	}
}

IntImageG::IntImageG(int _w, int _h, int init)
	: IntImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 1;
	FillHeader();
	i_im = (int*) new char[w*h*sizeof(int)];
	for(int i = 0; i<w*h; i++)
		i_im[i] = init;
}

// Copy of everything excluding content
IntImageG::IntImageG(IntImageG *i)
{
	w = i->w; h = i->h; fail = i->fail; focal_length = i->focal_length; z = 1;
	i_im = (int*) new char[w*h*sizeof(int)];
	memcpy(&header, &i->header, sizeof(IntHeader));
	for(int i = 0; i<w*h; i++)
		i_im[i] = INVALID;
}

IntImageG::IntImageG(BMPImageG *img, int hasInvalid)
{
	w = img->w; h = img->h; fail = img->fail; focal_length = img->focal_length; z = 1;
	ranges_set = 0; even_zoom = 0;
	FillHeader();
	i_im = (int*) new char[w*h*sizeof(int)];
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(hasInvalid && img->im[j*w+i]==Y_Utils::_INVALID)
				i_im[j*w+i] = INVALID;
			else
				i_im[j*w+i] = (int) img->im[j*w+i];
		}
	}
}

IntImageG::IntImageG(BMPImageC *img, int hasInvalid)
{
	w = img->w; h = img->h; fail = img->fail; focal_length = img->focal_length; z = 1;
	ranges_set = 0; even_zoom = 0;
	i_im = (int*) new char[w*h*sizeof(int)];
	FillHeader();
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(hasInvalid && !img->valid(i, j))
				i_im[j*w+i] = INVALID;
			else
				i_im[j*w+i] = (int)img->get_grey_pixel_D2((double) i, (double) j);
		}
	}
}

IntImageG::IntImageG(const char *name)
{
	i_im = nullptr; ranges_set = 0; even_zoom = 0; z = 1;
	if(Read(name)==-1)
		fail = 1;
}

IntImageG *IntImageG::Copy()
{
	IntImageG *c = new IntImageG(w, h);
	memcpy(c->i_im, i_im, w*h*sizeof(int));
	c->SetFocalLength(focal_length);
	c->ranges_set = ranges_set; c->v_min = v_min; c->v_max = v_max;
	c->even_zoom = even_zoom; c->z = 1;
	memcpy(&c->header, &header, sizeof(IntHeader));
	return(c);
}

void IntImageG::Copy(IntImageG *i)
{
	if(w!=i->w || h!=i->h) {
		delete [] i_im;
		i_im = new int[i->w*i->h];
		w = i->w; h = i->h;
	}
	memcpy(i_im, i->i_im, w*h*sizeof(int));
	ranges_set = i->ranges_set; v_min = i->v_min; v_max = i->v_max;
	even_zoom = i->even_zoom; z = 1;
	focal_length = i->focal_length;
}

int IntImageG::Read(const char *name)
{
	if(IntImage::Read(name)) // sets dimensions, z, name
		return(-1);
	FILE *fp;
	if(fopen_s(&fp, name, "rb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
	if(ReadHeader(fp))
		return(-1);
	if(i_im!=nullptr)
		delete [] i_im;
	i_im = new int[w*h];
	if(fread(i_im, w*h*sizeof(int), 1, fp)!=1) {
		sprintf_s(error, 256, "Read \"%s\" fails.\n", name); return(-1);
	}
	if(!endian) { // Convert to the opposite byte order
		for(int i = 0; i<w*h; i++)
			Y_Utils::Swap4Byte((u_char *) &i_im[i]);
	}
	return(0);
}

int IntImageG::Save(const char *name)
{
	FILE *fp;
	if(fopen_s(&fp, name, "wb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
	if(WriteHeader(fp))
		return(-1);

	if(!endian) { // Convert to the opposite byte order
		int *_i_im = new int[w*h];
		memcpy(_i_im, i_im, w*h*sizeof(int));
		for(int i = 0; i<w*h; i++)
			Y_Utils::Swap4Byte((u_char *) &_i_im[i]);
		if(fwrite(_i_im, w*h*sizeof(int), 1, fp)!=1) {
			sprintf_s(error, 256, "Cannot write to \"%s\".\n", name);
			return(-1);
		}
		delete [] _i_im;
	} else {
		if(fwrite(i_im, w*h*sizeof(int), 1, fp)!=1) {
			sprintf_s(error, 256, "Cannot write to \"%s\".\n", name);
			return(-1);
		}
	}
	fclose(fp);
	return(0);
}

int IntImageG::SaveASCII(const char* file, bool saveIndexes)
{
	FILE* fp;
	if(fopen_s(&fp, file, "w"))
		return(-1);
	int i, j;
	if(saveIndexes) {
		fprintf(fp, "            ");
		for(i = 0; i<w; i++)
			fprintf(fp, "%12d", i);
		for(j = 0; j<h; j++) {
			fprintf(fp, "%12d", j);
			for(i = 0; i<w; i++)
				fprintf(fp, "%12d", i_im[i+j*w]);
			fprintf(fp, "\n");
		}
	} else {
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++)
				fprintf(fp, "%12d", i_im[i+j*w]);
			fprintf(fp, "\n");
		}
	}
	fclose(fp);
	return(0);
}

void IntImageG::SetColour(int v)
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++)
			i_im[j*w+i] = v;
	}
}

int IntImageG::PutAt(IntImageG *p, int x, int y)
{
	for(int j = 0; j<p->h; j++) {
		if((y+j)<0 || (y+j)>h)
			continue;
		for(int i = 0; i<p->w; i++) {
			if((x+i)<0 || (x+i)>w)
				continue;
			i_im[(y+j)*w+(x+i)] = p->i_im[j*p->w+i];
		}
	}
	return(0);
}

// invalid, if any corner is invalid
int IntImageG::valid(double x, double y)
{
	int i = int(x), j = int(y);
	if(valid(i,j) && valid(i+1,j) && valid(i,j+1) && valid(i+1,j+1))
		return(1);
	return(0);
}

// valid pixel, neighbouring invalid
int IntImageG::border(int x, int y)
{
	if(!valid(x, y)) return(0);
	for(int j = y-1; j<=y+1; j++) {
		for(int i = x-1; i<=x+1; i++) {
			if(!valid(i, j)) return(1);
		}
	}
	return(0);
}

IntImageG** IntImageG::Split(int factor)
{
	IntImageG** pImgs = new IntImageG*[factor];
	int i, j, new_w = w/factor, nw;
	for(int n = 0; n<factor; n++) {
		// Last image could have different number of columns from the rest
		nw = (n<(factor-1)) ? new_w : w-new_w*n;
		pImgs[n] = new IntImageG(nw, h);
		for(j = 0; j<h; j++) {
			for(i = 0; i<nw; i++) {
				pImgs[n]->set_pixel(i, j, get_pixel(n*nw+i, j));
			}
		}
	}
	return(pImgs);
}

int IntImageG::SaveBMP(const char *name, int rescale, int ignore_value)
{
	BMPImageG *img = Greyscale(rescale, ignore_value);
	int status = img->Save(name);
	delete img;
	return(status);
}

int IntImageG::SaveBMPRange(const char *name, int min, int max)
{
	BMPImageG *img = Greyscale(min, max);
	int status = img->Save(name);
	delete img;
	return(status);
}

int IntImageG::SaveBMPRainbow(const char *name, int type)
{
	BMPImageC* pBMPC = BMPRainbow(type);
	int status = pBMPC->Save(name);
	delete pBMPC;
	return(status);
}

BMPImageC* IntImageG::BMPRainbow(int type)
{
	int i, j, k = 0;
	int v;
	BMPImageC* pBMPC = new BMPImageC(w, h);
	Y_RGB<u_char> vv;
	
	// Find range
	int a_max = INT_MIN, a_min = INT_MAX;
	double scale;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				a_max = (a_max < v) ? v : a_max;
				a_min = (a_min > v) ? v : a_min;
			}
		}
	}
	if(a_min>=a_max)
		scale = 1.0;
	else {
		// used range is from 0 to 4*256-1
		scale = (256.0*4-1.0)/(a_max-a_min);
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				pBMPC->set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
			else {
				v = get_pixel(i, j);
				if(type==1)
					k = int(scale*(v-a_min));
				else if(type==-1)
					k = int(scale*(a_max-v));
				if(k<256) // Black->Blue spectrum 0:255
					vv.Set(0, 0, k);
				else if(k<2*256) // Blue-red spectrum 256:511
					vv.Set(k-256, 0, 511-k);
				else if(k<3*256) // Red-yellow 512:767
					vv.Set(BMPImageG::m_white, k-512, 0);
				else // yellow-white 768:1023
					vv.Set(BMPImageG::m_white, BMPImageG::m_white, k-768); 
				pBMPC->set_pixel(i, j, vv);
			}
		}
	}
	return(pBMPC);
}

/* Cut off subimage, taking care of image extent.
   at_x/y - top-left corner of cut
   sw/sh - requested size
   Area is initiated with invalid.
*/
IntImageG *IntImageG::CutSubimage(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	IntImageG *s = new IntImageG(sw, sh);
	for(i = 0; i<sw*sh; i++)
		s->i_im[i] = INVALID;
	s->SetFocalLength(focal_length);
	s->ranges_set = ranges_set; s->v_min = v_min; s->v_max = v_max;
		
	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			s->i_im[jj*sw+ii] = i_im[j*w+i];
		}
	}
	return(s);
}

void IntImageG::Crop(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	int* i_im_ = new int[sw*sh];

	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			i_im_[jj*sw+ii] = i_im[j*w+i];
		}
	}
	delete [] i_im;
	i_im = i_im_;
	w = sw; h = sh;
}

double IntImageG::Bilinear(double dx, double dy, int *vld)
{
	int col, row, k;
	double v, invD = (double) Y_Utils::_INVALID_DOUBLE;
	double s00, s10, s01, s11;
	
	if(vld) *vld = 0;
	// Check for outside of image 
	if(dx<0.0 || dx>double(w-1) || dy<0.0 || dy>double(h-1)) return(invD);
	
	/* Integer parts */
	col = FLOOR(dx); row = FLOOR(dy);
	k = row*w+col;
	/* Fractional parts */
	dx -= (double) col; dy -= (double) row;
	if(dx<1.e-5 && dy<1.e-5) { // practically coinsides with a node
		if(vld) *vld = 1;
		return(i_im[k]);
	}
	// return INVALID, if outside the rectangle
	if(row<0 || row>h-1 || col<0 || col>w-1) {
		if(vld) *vld = 0;
		return(invD);
	}
	// If interested in pixel validity, check validity of all 4 pixels
	if(vld && i_im[k]==INVALID) { *vld = 0; return(invD); }
	s00 = i_im[k];
	if(col!=w-1) {
		if(vld && i_im[k+1]==INVALID) {*vld = 0; return(invD); }
		s10 = i_im[k+1];
	} else {
		s10 = i_im[k];
	}
	if(row!=h-1) {
		if(vld && i_im[k+w]==INVALID) {*vld = 0; return(invD); }
		s01 = i_im[k+w];
	} else {
		s01 = i_im[k];
	}
	if(col!=(w-1) && row!=(h-1)) {
		if(vld && i_im[k+w+1]==INVALID) {*vld = 0; return(invD); }
		s11 = i_im[k+w+1];
	} else {
		s11 = i_im[k];
	}
	if(vld) *vld = 1; // Validate pixel
	v = s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}

// Special case when all 4 nodes are guaranteed to exist
double IntImageG::Bilinear(double dx, double dy, int tl_i, int tl_j)
{
	int k;
	double s00, s10, s01, s11, v;
	
	k = tl_j*w+tl_i;
	// Fractional parts 
	dx -= (double) tl_i; dy -= (double) tl_j;
	if(dx<1.e-5 && dy<1.e-5) // practically coinsides with a node
		return(i_im[k]);
	if(fabs(dx-1.0)<1.e-5 && dy<1.e-5) 
		return(i_im[k+1]);
	if(dx<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(i_im[k+w]);
	if(fabs(dx-1.0)<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(i_im[k+w+1]);

	s00 = i_im[k];
	s10 = i_im[k+1];
	s01 = i_im[k+w];
	s11 = i_im[k+w+1];
	v = s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}

double IntImageG::Bicubic(double x, double y, int *vld)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	double v = 0.0, X, Y;

	// Surrounding pixels: (I,J)->(I+1,J+1)
	int If = (int)floor(x), Jf = (int)floor(y);
	int Ic = (int)ceil(x), Jc = (int)ceil(y);
		if(Jf<0) Jf = 0;
		if(Jc>h-1) Jc = h-1;
		if(If<0) If = 0;
		if(Ic>w-1) Ic = w-1;
	// If, Jf, Ic, Jc are guaranteed not to go out of range
	double V[16]; // 4 values, 4 X-derivatives, 4 Y-derivatives, 4 cross-derivatives

	V[ 0] = i_im[If+w*Jf];
	V[ 1] = i_im[Ic+w*Jf];
	V[ 2] = i_im[If+w*Jc];
	V[ 3] = i_im[Ic+w*Jc];
	V[ 4] = (If<1)   ? 0.0 : 0.5*(i_im[Ic+w*Jf]-i_im[If-1+w*Jf]);
	V[ 5] = (Ic>=w-1)   ? 0.0 : 0.5*(i_im[Ic+1+w*Jf]-i_im[If+w*Jf]);
	V[ 6] = (If<1) ? 0.0 : 0.5*(i_im[Ic+w*Jc]-i_im[If-1+w*Jc]);
	V[ 7] = (Ic>=w-1) ? 0.0 : 0.5*(i_im[Ic+1+w*Jc]-i_im[If+w*Jc]);
	V[ 8] = (Jf<1) ? 0.0 : 0.5*(i_im[If+w*Jc]-i_im[If+w*(Jf-1)]);
	V[ 9] = (Jf<1) ? 0.0 : 0.5*(i_im[Ic+w*Jc]-i_im[Ic+w*(Jf-1)]);
	V[10] = (Jc>=h-1) ? 0.0 : 0.5*(i_im[If+w*(Jc+1)]-i_im[If+w*Jf]);
	V[11] = (Jc>=h-1) ? 0.0 : 0.5*(i_im[Ic+w*(Jc+1)]-i_im[Ic+w*Jf]);
	V[12] = (If<1 || Jf<1) ? 0.0 : 0.25*(i_im[Ic+w*Jc]+i_im[If-1+w*(Jf-1)]-i_im[Ic+w*(Jf-1)]-i_im[If-1+w*Jc]);
	V[13] = (Ic>=w-1 || Jf<1) ? 0.0 : 0.25*(i_im[Ic+1+w*Jc]+i_im[If+w*(Jf-1)]-i_im[Ic+1+w*(Jf-1)]-i_im[If+w*Jc]);
	V[14] = (If<1|| Jc>=h-1) ? 0.0 : 0.25*(i_im[Ic+w*(Jc+1)]+i_im[If-1+w*Jf]-i_im[Ic+w*Jf]-i_im[If-1+w*(Jc+1)]);
	V[15] = (Ic>=w-1 || Jc>=h-1) ? 0.0 : 0.25*(i_im[Ic+1+w*(Jc+1)]+i_im[If+w*Jf]-i_im[Ic+1+w*Jf]-i_im[If+w*(Jc+1)]);

	if(vld) *vld = 1;
	
	NEWMAT::ColumnVector beta(16);
	beta<<V[0]<<V[1]<<V[2]<<V[3]<<V[4]<<V[5]<<V[6]<<V[7]<<V[8]<<V[9]<<V[10]<<V[11]<<V[12]<<V[13]<<V[14]<<V[15];

	NEWMAT::ColumnVector alpha = M_bicubic*beta;

	X = x-floor(x);
	Y = y-floor(y);
	for(int k = 0; k<16; k++) {
		v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
	}
	if(v<0.0) v = 0.0;
	if(v>255.0) v = 255.0;
	
	return(v);
}

void IntImageG::Log()
{
	int v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if((v = get_pixel(i,j))<=0.0)
				set_pixel(i, j, INVALID);
			else
				set_pixel(i, j, (int)log((double)v));
		}
	}
}

void IntImageG::ScaleShift(double gain, double offset)
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, (int)(gain*get_pixel(i,j)+offset));
		}
	}
}

bool IntImageG::Subtract(IntImageG* pInt)
{
	if(w!=pInt->w || h!=pInt->h)
		return(false);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)-pInt->get_pixel(i,j));
		}
	}
	return(true);
}

bool IntImageG::Add(IntImageG* pInt)
{
	if(w!=pInt->w || h!=pInt->h)
		return(false);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)+pInt->get_pixel(i,j));
		}
	}
	return(true);
}

bool IntImageG::Multiply(IntImageG* pInt)
{
	if(w!=pInt->w || h!=pInt->h)
		return(false);
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			set_pixel(i, j, get_pixel(i,j)*pInt->get_pixel(i,j));
		}
	}
	return(true);
}

bool IntImageG::Divide(IntImageG* pInt)
{
	if(w!=pInt->w || h!=pInt->h)
		return(false);
	double v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = (double)pInt->get_pixel(i,j);
			if(!EPSILON_EQ(v,0.0))
				set_pixel(i, j, (int)((double)get_pixel(i,j)/v));
			else
				set_pixel(i, j, INVALID);
		}
	}
	return(true);
}

bool IntImageG::Max(IntImageG* pInt)
{
	if(w!=pInt->w || h!=pInt->h)
		return(false);
	int v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = pInt->get_pixel(i,j);
			set_pixel(i, j, YMAX(v, get_pixel(i,j)));
		}
	}
	return(true);
}

bool IntImageG::Min(IntImageG* pInt)
{
	if(w!=pInt->w || h!=pInt->h)
		return(false);
	int v;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			v = pInt->get_pixel(i,j);
			set_pixel(i, j, YMIN(v, get_pixel(i,j)));
		}
	}
	return(true);
}

BMPImageG *IntImageG::Greyscale(int rescale, int ignore_value)
{
	int i, j;
	double v;
	u_char vsh;
	BMPImageG *img = new BMPImageG(w, h);
	img->SetFocalLength(focal_length);
	
	// Find total range
	int a_max = INT_MIN, a_min = INT_MIN;
	double scale = 1.0;
	if(rescale) {
		MinMax(a_min, a_max, ignore_value);
		if(a_min>=a_max)
			scale = 1.0;
		else {
			// used range is from 1 to 255, 0 is left for invalid pixels
			scale = (255.0-1.0)/(a_max-a_min);
		}
		//fprintf(stderr, "Range=[%lf:%lf] Scale=%lf\n", a_min, a_max, scale);
	} else {
		if(ranges_set) { // Use pre-set ranges
			a_min = v_min; a_max = v_max;
			scale = (255.0-1.0)/(a_max-a_min);
		}
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(get_pixel(i, j) == ignore_value) {
				// substitute with 0
				img->set_pixel(i, j, (u_char) 0);
			} else {
				if(!valid(i, j))
					img->set_pixel(i, j, Y_Utils::_INVALID_UCHAR);
				else {
					v = get_pixel(i, j);
					if(rescale || ranges_set)
						v = scale*(v-a_min);
					if(v<0.0)
						img->set_pixel(i, j, u_char(1)); // Shouldn't happen
					else {
						vsh = (u_char)D_LIMIT_RANGE_CHAR(v);
						if(vsh==0)
							vsh = 1;
						img->set_pixel(i, j, vsh);
					}
				}
			}
		}
	}
	return(img);
}

// Conversion to greyscale with given ranges.
// 'min' maps to 1, 'max' to 255
BMPImageG *IntImageG::GreyscaleRange(int a_min, int a_max)
{
	int i, j;
	double v, scale;
	BMPImageG *img = new BMPImageG(w, h);
	img->SetFocalLength(focal_length);
	
	if(a_min>=a_max)
			scale = 1.0;
	else {
		// used range is from 1 to 255, 0 is left for invalid pixels
		scale = (255.0-1.0)/(a_max-a_min);
	}
	//fprintf(stderr, "Range=[%lf:%lf] Scale=%lf\n", a_min, a_max, scale);
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				img->set_pixel(i, j, Y_Utils::_INVALID_UCHAR);
			else {
				v = scale*(get_pixel(i, j)-a_min);
				if(v>=0.0 && v<=255.0)
					img->set_pixel(i, j, (u_char)(1+v));
				else if(v>255.0)
					img->set_pixel(i, j, BMPImageG::m_white);
				else if(v<0.0)
					img->set_pixel(i, j, (u_char)1);
				else
					sprintf_s(error, 256, "IntImageG::Greyscale: pixel (%d,%d) out of range: %.2lf\n",
						i, j, v);
			}
		}
	}
	return(img);
}

void IntImageG::ValidateAll()
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(!valid(i, j))
				set_pixel(i, j, 0);
		}
	}
}

int IntImageG::AverageBrightness(int invalid_clr)
{
	__int64 sum = 0;
	int v, count = 0;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if((v = get_pixel(i, j))!=invalid_clr) {
				sum += (__int64)v;
				count++;
			}
		}
	}
	return((int)(sum/count));
}

int IntImageG::ZoomToDest_Bilinear(int dest_w, int dest_h)
{
	double I, J;
	int vld, status = 0;
	double h_factor = double(w-1)/double(dest_w-1);
	double v_factor = double(h-1)/double(dest_h-1);
	int *Im = new int[dest_w*dest_h];
	for(int j = 0; j<dest_h-1; j++) {
		J = v_factor*j;
		for(int i = 0; i<dest_w-1; i++) {
			I = h_factor*i;
			Im[j*dest_w+i] = (int)Bilinear(I, J, &vld);
			if(!vld) Im[j*dest_w+i] = INVALID;
		}
		I = h_factor*(dest_w-1)-0.5;
		Im[j*dest_w+dest_w-1] = (int)Bilinear(I, J, int(I), int(J));
	}
	// Last row - enforce interpolation from above row
	J = v_factor*(dest_h-1)-0.5;
	for(int i = 0; i<dest_w-1; i++) {
		I = h_factor*i;
		Im[(dest_h-1)*dest_w+i] = (int)Bilinear(I, J, int(I), int(J));
	}
	I = h_factor*(dest_w-1)-0.5;
	Im[(dest_h-1)*dest_w+dest_w-1] = (int)Bilinear(I, J, int(I), int(J));
	w = dest_w; h = dest_h;
	delete [] i_im;
	i_im = Im;
	return(status);
}

int IntImageG::Zoom_Bilinear(double zm, int flags)
{
	int status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bilinear(dest_w, dest_h);
	if(status==-1) {
		sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Pixels affected by invalid neighbours should be invalid
	// Now 'i_im' has dimensions dest_w*dest_h
	return(status);
}

int IntImageG::ZoomToDest_Bicubic(int dest_w, int dest_h)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	NEWMAT::ColumnVector alpha(16), beta(16);

	int *i_Im = new int[dest_w*dest_h];

	double x_scale = (double)dest_w/w;
	double y_scale = (double)dest_h/h;

	IntImageG* pDblXd = new IntImageG(w, h);
	IntImageG* pDblYd = new IntImageG(w, h);
	IntImageG* pDblXYd = new IntImageG(w, h);

	pDblXd->SetBlack();
	pDblYd->SetBlack();
	pDblXYd->SetBlack();

	int i, j, k;

	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(i==0 || i==w-1)
				pDblXd->set_pixel(i,j,0);
			else
				pDblXd->set_pixel(i,j,(get_pixel(i+1,j)-get_pixel(i-1,j))/2);
			if(j==0 || j==h-1)
				pDblYd->set_pixel(i,j,0);
			else
				pDblYd->set_pixel(i,j,(get_pixel(i,j+1)-get_pixel(i,j-1))/2);
			if(i==0 || i==w-1 || j==0 || j==h-1)
				pDblXYd->set_pixel(i,j,0);
			else
				pDblXYd->set_pixel(i,j,(get_pixel(i-1,j-1)+get_pixel(i+1,j+1)-get_pixel(i-1,j+1)+get_pixel(i+1,j-1))/4);
		}
	}

	// Normalized distances:
	double X, Y, I, J, fI, fJ, cI, cJ;
	Point2<int> I11, I21, I12, I22;
	double v;
	for(j = 0; j<dest_h; j++) {
		J = (double)j/y_scale;
		fJ = floor(J); cJ = ceil(J);
		if(fJ<0) fJ = 0;
		if(cJ>h-1) cJ = h-1;
		Y = J-fJ; // W
		for(i = 0; i<dest_w; i++) {
			I = (double)i/x_scale;
			fI = floor(I); cI = ceil(I);
			if(fI<0) fI = 0;
			if(cI>w-1) cI = w-1;
			X = I-fI; // H
			// Indexes of nearest pixels:
			I11.Set((int)fI, (int)fJ);
			I21.Set((int)cI, (int)fJ);
			I12.Set((int)fI, (int)cJ);
			I22.Set((int)cI, (int)cJ);
			// Beta vector:
			beta<<(double)get_pixel(I11)<<(double)get_pixel(I21)<<(double)get_pixel(I12)<<(double)get_pixel(I22)
				<<pDblXd->get_pixel(I11)<<pDblXd->get_pixel(I21)<<pDblXd->get_pixel(I12)<<pDblXd->get_pixel(I22)
				<<pDblYd->get_pixel(I11)<<pDblYd->get_pixel(I21)<<pDblYd->get_pixel(I12)<<pDblYd->get_pixel(I22)
				<<pDblXYd->get_pixel(I11)<<pDblXYd->get_pixel(I21)<<pDblXYd->get_pixel(I12)<<pDblXYd->get_pixel(I22);
			alpha = M_bicubic*beta;
			v = 0.0;
			for(k = 0; k<16; k++) {
				v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
			}
			if(v<0.0) v = 0.0;
			if(v>255.0) v = 255.0;
			i_Im[i+dest_w*j] = (int)(v+0.5);
		}
	}

	delete pDblXd;
	delete pDblYd;
	delete pDblXYd;

	w = dest_w; h = dest_h;
	delete [] i_im;
	i_im = i_Im;
	return(0);
}

int IntImageG::Zoom_Bicubic(double zm, int flags)
{
	int status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bicubic(dest_w, dest_h);
	if(status==-1) {
		sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Pixels affected by invalid neighbours should be invalid
	// Now 'i_im' has dimensions dest_w*dest_h
	return(status);
}

int IntImageG::Rotate_Bilinear(double angle_degrees)
{
	int i, j;
	double cos_a, sin_a, v;
	IntImageG *tmp = Copy();
	SetInvalid();

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bilinear(dx, dy, &vld);
			if(vld) set_pixel(i, j, (int)v);
		}
	}
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

int IntImageG::Rotate_Bicubic(double angle_degrees)
{
	int i, j;
	double cos_a, sin_a, v;
	IntImageG *tmp = Copy();
	SetInvalid();

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bicubic(dx, dy, &vld);
			if(vld) set_pixel(i, j, (int)v);
		}
	}
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

// 1: by pi/2, 2: by pi, 3: by -pi/2
void IntImageG::RotateQuarter(int nr_of_quarters)
{
	int new_w, new_h;
	if(nr_of_quarters == 2) {
		new_w = w; new_h = h;
	} else {
		new_w = h; new_h = w;
	}
	int *tmp = (int*)  new int[3*new_w*new_h];
	int i, j, n = sizeof(int);
	switch(nr_of_quarters) {
	case 1:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[j*new_w+i], &i_im[i*w+(w-1-j)], n);
		}}
		break;
	case 2:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[j*new_w+i], &i_im[(h-1-j)*w+(w-1-i)], n);
		}}
		break;
	case 3:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[j*new_w+i], &i_im[(h-1-i)*w+j], n);
		}}
		break;
	}
	delete [] i_im;
	i_im = tmp;
	w = new_w; h = new_h;
}

void IntImageG::Blur(int kernel, int times)
{
	if(kernel<3) {
		sprintf_s(error, 256, "Blur kernel<3, returning.\n");
		return;
	}
	if(times<1) {
		sprintf_s(error, 256, "Blur times<1, returning.\n");
		return;
	}
	if(kernel%2==0) {
		kernel++;
		sprintf_s(error, 256, "Kernel even, set to %d.\n", kernel);
	}
	int i, j, ii, jj, half = (kernel-1)/2, count, NaN;
	int sum;
	
	IntImageG *tmp = new IntImageG(w, h);
	
	for(int t = 0; t<times; t++) {
		tmp->Copy(this);
	
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				sum = 0; count = 0; NaN = 0;
				for(jj = j-half; jj<=j+half; jj++) {
					if(jj<0 || jj>=h) continue;
					for(ii = i-half; ii<=i+half; ii++) {
						if(ii<0 || ii>=w) continue;
						if(!(tmp->valid(ii, jj))) NaN++;
						sum += tmp->get_pixel(ii, jj);
						count++;
					}
				}
				if(!NaN)
					set_pixel(i, j, (int)(sum/count));
				else
					set_pixel(i, j, INVALID);
			}
		}
	}
	delete tmp;
}

void IntImageG::MinMax(int& v_min, int& v_max, int ignore_value)
{
	int v;
	v_min = INT_MAX; v_max = INT_MIN;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(get_pixel(i, j) == ignore_value)
				continue; // as if it does not exist
			if(valid(i, j)) {
				v = get_pixel(i, j);
				v_min = YMIN(v_min, v);
				v_max = YMAX(v_max, v);
			}
		}
	}
}

void IntImageG::MinMax(int& v_min, Point2<int>& p_min, int& v_max, Point2<int>& p_max, int ignore_value)
{
	int v;
	v_min = INT_MAX; v_max = INT_MIN;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(get_pixel(i, j) == ignore_value)
				continue; // as if it does not exist
			if(valid(i, j)) {
				v = get_pixel(i, j);
				if(v<v_min) {
					v_min = v;
					p_min.Set(i, j);
				}
				if(v>v_max) {
					v_max = v;
					p_max.Set(i, j);
				}
			}
		}
	}
}

void IntImageG::Scale(int set_min, int set_max)
{
	int min, max, v;
	MinMax(min, max);
	double scale = (double)(set_max-set_min)/(max-min);
	double isept = (double)(set_min*max-set_max*min)/(max-min);

	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				set_pixel(i, j, (int)(scale*v+isept));
			}
		}
	}
}

void IntImageG::CutOffPercentile(int& min, int& max, double percentile)
{
	if(EPSILON_EQ(percentile,100.0)) return;
	
	int i, j, m, L = 1000, accum;
	int v;
	
	// Number of valid points:
	int valid_pts = 0;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++)
			if(valid(i, j)) valid_pts++;
	}
	int remove_pts = int((1.0-percentile/100.0)*valid_pts/2.0);
	// Divide into 0.1% bins
	int D[10000];
	memset(D, 0, L*sizeof(int));
	int step = (int)((double)(max-min)/(L-1));
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				m = int((v-min)/step);
				if(m<0 || m>=L) {
					fprintf(stderr, "M=%d\n", m);
					m = L-1;
				}
				++D[m];
			}
		}
	}
	accum = 0;
	m = 0;
	while(accum<remove_pts) {
		accum += D[m];
		m++;
	}
	int _min = min+(m-1)*step;
	accum = 0;
	m = L-1;
	while(accum<remove_pts) {
		accum += D[m];
		m--;
	}
	max = min+(m+1)*step;
	min = _min;
}

bool IntImageG::LocalMaximum_4(int i, int j)
{
	int k = j*w+i;
	int v = i_im[k];
	if(v>=i_im[k-w] && v>=i_im[k-1] && v>=i_im[k+1] && v>=i_im[k+w])
		return(true);
	return(false);
}

bool IntImageG::LocalMaximum_8(int i, int j)
{
	int k = j*w+i;
	int v = i_im[k];
	if(v>=i_im[k-w-1] && v>=i_im[k-w] && v>=i_im[k-w+1] && v>=i_im[k-1] &&
		v>=i_im[k+1] && v>=i_im[k+w-1] && v>=i_im[k+w] && v>=i_im[k+w+1])
		return(true);
	return(false);
}

/*
IntImageG *IntImageG::Undistort_A_Bilinear(Y_Camera *C)
{
	IntImageG *ui = new IntImageG(w, h);
	ui->focal_length = focal_length;
	double p;
	Point2<int> d;
	Point2<double> u;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d.Set(i, j);
			u = C->DistortPixel(d);
			if(r.Inside(u)) {
				//p = pfBilinear(u.X(), u.Y());
				p = Bilinear(u.X(), u.Y());
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

IntImageG *IntImageG::Distort_A_Bilinear(Y_Camera *C)
{
	IntImageG *ui = new IntImageG(w, h);
	ui->focal_length = focal_length;
	double p;
	Point2<int> u;
	Point2<double> d;
	bool fail;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			u.Set(i, j);
			d = C->UndistortPixel(u, &fail);
			if(!fail && r.Inside(d)) {
				//p = pfBilinear(d.X(), d.Y());
				p = Bilinear(d.X(), d.Y());
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}
*/
IntImageG* IntImageG::operator *=(int s)
{
	int r;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			r = s*get_pixel(i,j);
			set_pixel(i, j, r);
		}
	}
	return(this);
}


