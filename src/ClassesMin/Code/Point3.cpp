#include "Point3.h"

using namespace std;

template <class T>
int Point3<T>::IsZero() const
{
	if(typeid(T)==typeid(int))
		return(x==0 && y==0 && z==0);
	else
		return(DOUBLE_EQ(0.0, length()));
}

template <class T>
int Point3<T>::IsUnit() const
{
	if(typeid(T)==typeid(int))
		return(1==Length());
	else
		return(DOUBLE_EQ(1.0, Length()));
}


template <class T>
void Point3<T>::Rotate(NEWMAT::Matrix& M)
{
	if(M.Ncols()!=3 || M.Nrows()!=3) {
		fprintf(stderr, "Matrix is not 3x3.\n");
		return;
	}
	NEWMAT::ColumnVector p(3);
	p<<x<<y<<z;
	p = M*p;
	x = p(1); y = p(2); z = p(3);
}
