#include <math.h>
#include <Windows.h>
#include "RAR.h"
#include "Hmgr.h"

using namespace std;

char RARecord::buf[1024];

RARecord::RARecord()
{
	memset(mark, 0, 8); 
	strcpy_s(timecode, 16, "00:00:00:00"); timecode[11] = 0;
	file[0] = '-'; prev_file[0] = '-'; work_dir[0] = '-'; mark[0] = '-';
	f_n = -1; base_f_n = -1; uniqueTag = 0;
	w = 0; h = 0; focal_length = 0.0;
	zoom = 1.0; angle = 0.0; // Degrees
	x_shift = y_shift = 0.0;
	n_col = 0; col_b = nullptr; col_d = nullptr;
	av_error = -1.0; max_error = 0.0; overlap_percent = 0.0;
	force_rar = nullptr;
	memset(PeakParams, 0, 6*sizeof(double));
}

RARecord::~RARecord() 
{ 
	if(force_rar) delete force_rar;
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
}

void RARecord::Reset() 
{ 
	SetNums(-1,-1); Set(1.0,0.0,0.0,0.0); MarkSet("b"); 
	if(col_b) {
		delete [] col_b;
		col_b = nullptr;
	}
	if(col_d) {
		delete [] col_d;
		col_d = nullptr;
	}
}

void RARecord::Copy(const RARecord& r)
{
	w = r.w; h = r.h; focal_length = r.focal_length;
	strcpy_s(timecode, 16, r.timecode);
	file = r.file; prev_file = r.prev_file; work_dir = r.work_dir; strcpy_s(mark, 8, r.mark); 
	f_n = r.f_n; base_f_n = r.base_f_n; uniqueTag = r.uniqueTag;
	zoom = r.zoom; angle = r.angle;
	x_shift = r.x_shift; y_shift = r.y_shift;
	n_col = r.n_col; 
	if(n_col>0) {
		if(col_b) delete [] col_b;
		if(col_d) delete [] col_d;
		col_b = new Coord2[n_col];
		col_d = new Coord2[n_col];
		for(int i = 0; i<n_col; i++) {
			col_b[i].Copy(r.col_b[i]);
			col_d[i].Copy(r.col_d[i]);
		}
	}
	overlap_percent = r.overlap_percent; av_error = r.av_error; max_error = r.max_error;
	if(r.force_rar)
		force_rar = new RAForceRecord(r.force_rar);
	memcpy(PeakParams, r.PeakParams, 6*sizeof(double));
}

void RARecord::Copy(RARecord* r)
{
	w = r->w; h = r->h; focal_length = r->focal_length;
	strcpy_s(timecode, 12, r->timecode);
	file = r->file; prev_file = r->prev_file; work_dir = r->work_dir; strcpy_s(mark, r->mark); 
	f_n = r->f_n; base_f_n = r->base_f_n; uniqueTag = r->uniqueTag;
	zoom = r->zoom; angle = r->angle;
	x_shift = r->x_shift; y_shift = r->y_shift;
	n_col = r->n_col; 
	if(n_col>0) {
		if(col_b) delete [] col_b;
		if(col_d) delete [] col_d;
		col_b = new Coord2[n_col];
		col_d = new Coord2[n_col];
		for(int i = 0; i<n_col; i++) {
			col_b[i].Copy(r->col_b[i]);
			col_d[i].Copy(r->col_d[i]);
		}
	}
	overlap_percent = r->overlap_percent; av_error = r->av_error; max_error = r->max_error;
	if(r->force_rar)
		force_rar = new RAForceRecord(r->force_rar);
	memcpy(PeakParams, r->PeakParams, 6*sizeof(double));
}

int RARecord::MarkAdd(char *s)
{
	char stg[16];
	strcpy_s(stg, 16, s);
	strcat_s(stg, 16, this->mark);
	if(strlen(stg)<8)
		return(strcpy_s(mark, 8, stg)!=0);
	return(-1);
}

int RARecord::MarkAdd(char s)
{
	char stg[16];
	sprintf_s(stg, (size_t) 16, "%c", s);
	strcat_s(stg, 16, this->mark);
	if(strlen(stg)<8)
		return(strcpy_s(mark, 8, stg)!=0);
	return(-1);
}

// has 'S'/'F'/'C' or doesn't have 'B'/'X'/'E' and does have 's'
bool RARecord::MarkGood()
{
	bool status =   strstr(mark, "S")!=nullptr || 
					strstr(mark, "F")!=nullptr || 
					strstr(mark, "s")!=nullptr || 
					strstr(mark, "C")!=nullptr 
							&& 
					(strstr(mark, "B")==nullptr && 
					strstr(mark, "X")==nullptr && 
					strstr(mark, "E")==nullptr);
	return(status);
}

// has 'B'/'X'/'E'
// if has no capital letters, then has 'b'
// if marked bad, returns 'true'
bool RARecord::MarkBad()
{
	if(strstr(mark, "B")==nullptr && strstr(mark, "X")==nullptr && 
		strstr(mark, "E")==nullptr && strstr(mark, "S")==nullptr && 
		strstr(mark, "F")==nullptr)
			return(strstr(mark, "b")!=nullptr);
	bool status = strstr(mark, "B")!=nullptr || strstr(mark, "X")!=nullptr || 
		strstr(mark, "E")!=nullptr;
	return(status);
}

int RARecord::ReadStg(const char *stg)
{
	double z;
	char tag[16];
	sscanf_s(stg, "%s", tag, 16);
	if(tag[0]<'0' || tag[0]>'9') {
		sprintf_s(RARecord::buf, (size_t) 1024, "Record has incorrect tag.");
		return(RAR_FATAL);
	}
	if(strchr(tag, ':')) { // Probably RRA with timecode
		sprintf_s(RARecord::buf, (size_t) 1024, "Record has incorrect tag (probably timecode).");
		return(RAR_FATAL);
	}
	
	if(strstr(stg, "<-")) { // Explicit specification of both frames
							// Can be used for cross-reg's
		sscanf_s(stg, 
		"  %d<-%d	%lf %lf  %lf %lf (%lf,%lf) %s (%lf %lf %lf %lf %lf %lf)", 
		&base_f_n, &f_n, &x_shift, &y_shift, &z, &angle, 
		&overlap_percent, &av_error, mark, 8, 
		&PeakParams[0], &PeakParams[1], &PeakParams[2], 
		&PeakParams[3], &PeakParams[4], &PeakParams[5]);
	} else { // Old specification. 'base_f_n is assumed to be 'f_n-1'
		sscanf_s(stg, 
		"  %d	%lf %lf   %lf %lf (%lf,%lf) %s (%lf %lf %lf %lf %lf %lf)", 
		&f_n, &x_shift, &y_shift, &z, &angle,
		&overlap_percent, &av_error, mark, 8, 
		&PeakParams[0], &PeakParams[1], &PeakParams[2], 
		&PeakParams[3], &PeakParams[4], &PeakParams[5]);
		base_f_n = f_n-1;
	}
	uniqueTag = UNIQUETAG(base_f_n,f_n);

	// If stg has ':', it must have a timecode. Read it then.
	char *p = strchr((char*)stg, ':');
	if(p!=nullptr)
		sscanf_s(p-2, "%s", timecode, 12);
	zoom = exp(z);
	// Try to make the rotation angle look sensible
	while(angle>180.0) angle -= 360.0;
	while(angle<-180.0) angle += 360.0;
	return(f_n);
}

int RARecord::ReadFileName(FILE *log)
{
	// Read until the line is not mark
	do {
		if(fgets(buf, 1023, log)==nullptr) { // End of logfile
			sprintf_s(RARecord::buf, (size_t) 1024, "End of file reached.");
			return(RAR_FATAL);
		}
	} while(buf[0]=='#' || strlen(buf)<3);
	
	//sscanf_s(buf, "%s", file, 40);
	file = buf;
	return(0);
}

int RARecord::ReadPrevFileName(FILE *log)
{
	// Read until the line is not mark
	do {
		if(fgets(buf, 1023, log)==nullptr) { // End of logfile
			sprintf_s(RARecord::buf, (size_t) 1024, "End of file reached.");
			return(RAR_FATAL);
		}
	} while(buf[0]=='#' || strlen(buf)<3);
	
	sscanf_s(buf, " %*s %s", prev_file.c_str(), 40);
	return(0);
}

// By default, collocations are read
int RARecord::Read(FILE *log)
{
	// Read until the line is not marked as comment or collocation string
	do {
		if(fgets(buf, 1023, log)==nullptr) { // End of logfile
			sprintf_s(RARecord::buf, (size_t) 1024, "End of file reached.");
			return(RAR_FATAL);
		}
		if(buf[0]=='%') // Read collocation string
			ReadCollocations(buf);
	} while(buf[0]=='#' || buf[0]=='%' || strlen(buf)<3);

	f_n = ReadStg(buf); // May indicate failure to read
	return(f_n);
}

int RARecord::ReadCollocations(const char* stg)
{
	char stg2[1024];

	if(stg[0]!='%') return(-1); // Not a collocation string
	sscanf_s(stg, "%*c %d", &n_col); //  Could be 0, or >=2
	if(col_b) { delete [] col_b; col_b = nullptr; }
	if(col_d) { delete [] col_d; col_d = nullptr; }
	if(n_col==0)
		return(0);

	col_b = new Coord2[n_col];
	col_d = new Coord2[n_col];

	istrstream *ist = new istrstream(stg);
	do {
		ist->getline(stg2, 1024, ' ');
	} while(strlen(stg2)==0);
	do {
		ist->getline(stg2, 1024, ' ');
	} while(strlen(stg2)==0);
	for(int k = 0; k<n_col; k++) {
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_b[k].x));
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_b[k].y));
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_d[k].x));
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_d[k].y));

		// Assuming that RAR has frame size and focal length set:
		col_b[k].SetSizeFocalLength(this->w, this->h, this->focal_length);
		col_d[k].SetSizeFocalLength(this->w, this->h, this->focal_length);
	}
	delete ist;
	return(0);
}

int RARecord::ReadBin(FILE *fp)
{
	if(fread(buf, 120, 1, fp)!=1) {
		sprintf_s(RARecord::buf, (size_t) 1024, "Cannot read binary record.");
		return(RAR_FATAL);
	}
	if(buf[0]!='*' || buf[46]!='(' || buf[63]!=')') return(RAR_FATAL-1);
	sscanf_s(buf+1, "%d<-%d", &base_f_n, &f_n);
	uniqueTag = UNIQUETAG(base_f_n,f_n);

	int p = 14;
	memcpy(&x_shift, buf+p, sizeof(double)); p+=sizeof(double);
	memcpy(&y_shift, buf+p, sizeof(double)); p+=sizeof(double);
	memcpy(&zoom, buf+p, sizeof(double)); p+=sizeof(double);
	zoom = exp(zoom);
	memcpy(&angle, buf+p, sizeof(double)); p+=sizeof(double)+1;
	memcpy(&overlap_percent, buf+p, sizeof(double)); p+=sizeof(double);
	memcpy(&av_error, buf+p, sizeof(double)); p+=sizeof(double)+1;
	memcpy(mark, buf+p, 8); p+=8;
	for(int i = 0; i<6; i++) {
		memcpy(&PeakParams[i], buf+p, sizeof(double)); 
		p+=sizeof(double);
	}
	return(f_n);
}

// BaseFrame/Frame/X-shift/Y-shift/LogZoom/Angle/Overlap-%/Error/PeakParams
// BaseFrame/Frame/X-shift/Y-shift/LogZoom/Angle/Overlap-%/Error/PeakParams
#define REC "  %04d<-%04d   %12.7lf %12.7lf   %12.9lf %12.7lf (%6.2lf,%8.3lf) \
%8s (%9.6lf %9.6lf %9.4lf  %9.6lf %9.6lf %9.4lf)\n"
#define RECSkip "# %04d<-%04d   %12.7lf %12.7lf   %12.9lf %12.7lf (%6.2lf,%8.3lf) \
%8s (%9.6lf %9.6lf %9.4lf  %9.6lf %9.6lf %9.4lf)\n"
#define RECTIMECODE "  %04d<-%04d   %12.7lf %12.7lf   %12.9lf %12.7lf (%6.2lf,%8.3lf) \
%8s (%9.6lf %9.6lf %9.4lf  %9.6lf %9.6lf %9.4lf) %10s\n"
#define RECTIMECODESkip "# %04d<-%04d   %12.7lf %12.7lf   %12.9lf %12.7lf (%6.2lf,%8.3lf) \
%8s (%9.6lf %9.6lf %9.4lf  %9.6lf %9.6lf %9.4lf) %10s\n"

void RARecord::Write(FILE *log, int _f_n, int _base_f_n, int hush)
{
	double z = logl(zoom);
	// If the record is required to be commented out, or both frame numbers
	// are negative, hush is included
	if(hush || (_base_f_n<0 && _f_n<0))
		fprintf(log, RECTIMECODESkip, _base_f_n, _f_n, x_shift, y_shift, 
			z, angle, overlap_percent, av_error, mark,
			PeakParams[0], PeakParams[1], PeakParams[2], 
			PeakParams[3], PeakParams[4], PeakParams[5], timecode);
	else
		fprintf(log, RECTIMECODE, _base_f_n, _f_n, x_shift, y_shift,
			z, angle, overlap_percent, av_error, mark,
			PeakParams[0], PeakParams[1], PeakParams[2], 
			PeakParams[3], PeakParams[4], PeakParams[5], timecode);
	fflush(log);
}

int RARecord::WriteBin(FILE *fp, int hush)
{
	memset(buf, 0, 120);
	sprintf_s(buf, (size_t) 1024, "* %04d<-%04d", base_f_n, f_n);
	// If the record is required to be commented out, or both frame numbers
	// are negative, hush is included
	if(hush || (base_f_n<0 && f_n<0)) buf[0] = '#';
	int p = 14;
	memcpy(buf+p, &x_shift, sizeof(double)); p+=sizeof(double);
	memcpy(buf+p, &y_shift, sizeof(double)); p+=sizeof(double);
	double z = logl(zoom);
	memcpy(buf+p, &z, sizeof(double)); p+=sizeof(double);
	memcpy(buf+p, &angle, sizeof(double)); p+=sizeof(double);
	buf[p] = '('; p+=1;
	memcpy(buf+p, &overlap_percent, sizeof(double)); p+=sizeof(double);
	memcpy(buf+p, &av_error, sizeof(double)); p+=sizeof(double);
	buf[p] = ')'; p+=1;
	memcpy(buf+p, mark, 8); p+=8;
	for(int i = 0; i<6; i++) {
		memcpy(buf+p, &PeakParams[i], sizeof(double)); 
		p+=sizeof(double);
	}
	memcpy(buf+p, timecode, 12); p+=12;
	return((fwrite(buf, 132, 1, fp)==1) ? 0 : RAR_NONFATAL);
}

char *RARecord::WriteStg(int hush)
{
	double z = logl(zoom);
	// If the record is required to be commented out, or both frame numbers
	// are negative, hush is included
	if(hush || (base_f_n<0 && f_n<0))
		sprintf_s(buf, (size_t) 1024, RECTIMECODESkip, base_f_n, f_n, x_shift, y_shift, 
			z, angle, overlap_percent, av_error, mark,
			PeakParams[0], PeakParams[1], PeakParams[2], 
			PeakParams[3], PeakParams[4], PeakParams[5], timecode);
	else
		sprintf_s(buf, (size_t) 1024, RECTIMECODE, base_f_n, f_n, x_shift, y_shift,
			z, angle, overlap_percent, av_error, mark,
			PeakParams[0], PeakParams[1], PeakParams[2], 
			PeakParams[3], PeakParams[4], PeakParams[5], timecode);
	return(buf);
}

char *RARecord::WriteBinStg(int hush)
{
	memset(buf, 0, 120);
	sprintf_s(buf, (size_t) 1024, "* %04d<-%04d", base_f_n, f_n);
	// If the record is required to be commented out, or both frame numbers
	// are negative, hush is included
	if(hush || (base_f_n<0 && f_n<0)) buf[0] = '#';
	int p = 14;
	memcpy(buf+p, &x_shift, sizeof(double)); p+=sizeof(double);
	memcpy(buf+p, &y_shift, sizeof(double)); p+=sizeof(double);
	double z = logl(zoom);
	memcpy(buf+p, &z, sizeof(double)); p+=sizeof(double);
	memcpy(buf+p, &angle, sizeof(double)); p+=sizeof(double);
	buf[p] = '('; p+=1;
	memcpy(buf+p, &overlap_percent, sizeof(double)); p+=sizeof(double);
	memcpy(buf+p, &av_error, sizeof(double)); p+=sizeof(double);
	buf[p] = ')'; p+=1;
	memcpy(buf+p, mark, 8); p+=8;
	for(int i = 0; i<6; i++) {
		memcpy(buf+p, &PeakParams[i], sizeof(double)); 
		p+=sizeof(double);
	}
	memcpy(buf+p, timecode, 12); p+=12;
	return(buf);
}

Hmgr& RARecord::GetHmgr(double _fl)
{
	double fl = (EPSILON_EQ(_fl,0.0)) ? focal_length : _fl;
	double a = Deg2Rad(angle);
	static Hmgr hm;
	hm.Set(cos(a)/zoom, -sin(a)/zoom, x_shift/fl,
		   sin(a)/zoom,  cos(a)/zoom, y_shift/fl,
		      0.0,          0.0,        1.0);
	return(hm);
}

void RARecord::Save(ofstream* of, int hush)
{
	char* stg = WriteStg(hush);
	*of<<stg;
}

void RARecord::SetCollocations(Collocations* pCol)
{
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
	n_col = pCol->n;
	col_b = new Coord2[n_col];
	col_d = new Coord2[n_col];
	for(int i = 0; i<n_col; i++) {
		col_b[i].Copy(pCol->b[i]);
		col_d[i].Copy(pCol->d[i]);
	}
}

Collocations* RARecord::GetCollocations()
{
	Collocations* pCol = new Collocations(n_col, w, h, focal_length);
	for(int i = 0; i<n_col; i++) {
		pCol->b[i].Copy(col_b[i]);
		pCol->d[i].Copy(col_d[i]);
	}
	return(pCol);
}

char* RARecord::WriteCollocations()
{
	//sprintf_s(buf, (size_t) 1024, "%% %2d  %8.2lf %8.2lf  %8.2lf %8.2lf  %8.2lf %8.2lf  %8.2lf %8.2lf\r\n",
	//		2, col_b[0].X(), col_b[0].Y(), col_d[0].X(), col_d[0].Y(),
	//		col_b[1].X(), col_b[1].Y(), col_d[1].X(), col_d[1].Y());

	// Variable number of collocations
	sprintf_s(buf, (size_t) 1024, "%% %2d  ", n_col);
	int len = (int)strlen(buf);

	for(int i = 0; i<n_col; i++) {
		sprintf_s(buf+len, (size_t) (1024-len), "%8.2lf %8.2lf  %8.2lf %8.2lf  ",
			col_b[i].X(), col_b[i].Y(), col_d[i].X(), col_d[i].Y());
		len = (int)strlen(buf);
	}
	sprintf_s(buf+len, (size_t) (1024-len), "\n");
	return(buf);
}

void RARecord::DefaultCollocations()
{
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
	n_col = 2;
	col_b = new Coord2[n_col];
	col_d = new Coord2[n_col];
		// 'col_d' is where point 'col_b' in 'base' image appears in 'derived' image
	//Hmgr hm = Hmgr::FromRecord(*this, focal_length);
	Hmgr hm = GetHmgr();
	Hmgr inv_hm = hm.Invert();
	col_b[0].Set(0.25*w, 0.25*h); 
	col_b[1].Set(0.75*w, 0.75*h); 

	for(int i = 0; i<n_col; i++) {
		col_b[i].SetSize(w, h);
		col_b[i].SetFocalLength(focal_length);
		col_d[i].SetSize(w, h);
		col_d[i].SetFocalLength(focal_length);
		col_d[i] = inv_hm.Map(col_b[i]);
	}
}

RARecord *RARecord::FindRARFromFrameNumber(RARecord *recs, int nrec, int fn)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].f_n==fn)
			return(&recs[i]);
	}
	return(nullptr);
}

RARecord *RARecord::FindRARFromBaseFrameNumber(RARecord *recs, int nrec, int bfn)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_f_n==bfn)
			return(&recs[i]);
	}
	return(nullptr);
}

int RARecord::FindOrderNumberFromFrameNumber(RARecord *recs, int nrec, int fn)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].f_n==fn)
			return(i);
	}
	return(RAR_NONFATAL);
}

int RARecord::FindOrderNumberFromNumbers(RARecord *recs, int nrec, int bfn, int fn)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_f_n==bfn && recs[i].f_n==fn) // Exact match
			return(i);
		// Inverse match has to be found by inverse inquiry
	}
	return(RAR_NONFATAL);
}

RARecord *RARecord::FindRARFromNumbers(RARecord *recs, int nrec, int bfn, int fn)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_f_n==bfn && recs[i].f_n==fn) // Exact match
			return(&recs[i]);
		// Inverse match has to be found by inverse inquiry
	}
	return(nullptr);
}

ostream &operator<<(ostream &ostr, RARecord &v)
{
	ostr<<"(Z="<<v.zoom<<", A="<<v.angle<<", X="<<v.x_shift<<", Y="<<v.y_shift<<")";
	return(ostr);
}

// Reads both binary and ASCII records
RARecord *RARecord::ReadRecs(char *logfile, int *nrec, char **_ppInfo)
{
	FILE *fp_log;
	if(fopen_s(&fp_log, logfile, "r")) {
		//fprintf(stderr, "Cannot open %s.\n", logfile);
		sprintf_s(buf, (size_t) 1024, "Cannot open \"%s\".", logfile);
		return(nullptr);
	}
	// Check for being binary and having info
	char c[5];
	fread(c, 5, 1, fp_log);
	fclose(fp_log);
	if(c[0]=='*') { // Binary
		if(strncmp(c, "*Info", 5)==0) { // Has info
			char* pInfo = nullptr;
			RARecord *records = ReadBinRecs(logfile, nrec, &pInfo);
			if(_ppInfo) 
				*_ppInfo = pInfo;
			else
				delete [] pInfo;
			return(records);
		} else { // Doesn't have info
			RARecord *records = ReadBinRecs(logfile, nrec);
			if(_ppInfo) *_ppInfo = nullptr;
			return(records);
		}
	} else { // ASCII
		if(_ppInfo) { // Check for info
			if(Y_Utils::HasInfo(logfile))
				*_ppInfo = Y_Utils::GetInfo(logfile);
			else
				*_ppInfo = nullptr;
		}
	}
	Y_Rect<int> *pFrame = nullptr;
	double fl = -1.0;
	if(_ppInfo && *_ppInfo) {
		pFrame = Y_Utils::GetFrameSizeFromInfo(*_ppInfo);
		fl = Y_Utils::GetFocalLength(*_ppInfo);
		if(!pFrame || EPSILON_EQ(fl,-1.0)) { // Requested but not found
			sprintf_s(buf, (size_t) 1024, "Absent or incomplete info.\n");
			return(nullptr);
		}
	}

	fopen_s(&fp_log, logfile, "r");
	int n = 0;
	RARecord rec;
	// Don't read collocations
	while(rec.Read(fp_log)>RAR_FATAL)
		n++;
	//fprintf(stderr, "Number of RRA records = %d\n", n);
	if(n<=0) {
		fclose(fp_log);
		*nrec = 0;
		sprintf_s(buf, (size_t) 1024, "File has no records.");
		return(nullptr);
	}
	rewind(fp_log);
	RARecord *records = new RARecord[n];
	for(int i = 0; i<n; i++) {
		if(pFrame) {
			records[i].w = pFrame->w;
			records[i].h = pFrame->h;
		}
		records[i].focal_length = fl;
		// Reading sets w/h/fl of collocations
		records[i].Read(fp_log);
	}
	*nrec = n;
	
	fclose(fp_log);
	delete pFrame;
	return(records);
}

// Info in not nullptr only if it is known that file has info.
RARecord *RARecord::ReadBinRecs(char *logfile, int *nrec, char **ppInfo)
{
	FILE *fp_log;
	if(fopen_s(&fp_log, logfile, "r")) {
		//fprintf(stderr, "Cannot open %s.\n", logfile);
		sprintf_s(buf, (size_t) 1024, "Cannot open \"%s\".", logfile);
		return(nullptr);
	}
	if(ppInfo) {
		if(*ppInfo==nullptr) *ppInfo = new char[1024];
		fread(*ppInfo, 1024, 1, fp_log);
	}
	int n = 0;
	RARecord rec;
	while(rec.ReadBin(fp_log)>RAR_FATAL)
		n++;
	if(n<=0) {
		fclose(fp_log);
		*nrec = 0;
		sprintf_s(buf, (size_t) 1024, "File has no records.");
		return(nullptr);
	}
	fprintf(stderr, "Number of RAR records = %d\n", n);
	rewind(fp_log);
	if(ppInfo)
		fread(*ppInfo, 1024, 1, fp_log);
	RARecord *records= new RARecord[n];
	
	for(int i = 0; i<n; i++) {
		records[i].ReadBin(fp_log);
	}
	*nrec = n;
	
	fclose(fp_log);
	return(records);
}

// Same as above but with file info
int RARecord::SaveRecs(char *logfile, RARecord *recs, int nrec, char *pInfo)
{
	FILE *fp;
	if(fopen_s(&fp, logfile, "w")) {
		sprintf_s(buf, (size_t) 1024, "Cannot save to a file.");
		return(RAR_NONFATAL);
	}
	if(pInfo)
		fprintf(fp, "%s", pInfo);
	for(int i = 0; i<nrec; i++) {
		recs[i].WriteCollocations(fp);
		recs[i].Write(fp);
	}
	fclose(fp);
	
	return(0);
}

// Same as above but binary and with file info
int RARecord::SaveBinRecs(char *logfile, RARecord *recs, int nrec, char *pInfo)
{
	FILE *fp;
	if(fopen_s(&fp, logfile, "w")) {
		sprintf_s(buf, (size_t) 1024, "Cannot save to a file.");
		return(RAR_NONFATAL);
	}
	if(pInfo) {
		memset(buf, 0, 1024);
		strcpy_s(buf, 1024, pInfo);
		// Guarantee bin format
		buf[0] = '*';
		if(fwrite(buf, 1024, 1, fp)!=1) {
			sprintf_s(buf, (size_t) 1024, "Cannot write to a file.");
			return(RAR_NONFATAL);
		}
	}
	for(int i = 0; i<nrec; i++)
		recs[i].WriteBin(fp);
	fclose(fp);
	
	return(0);
}

// Add the record to an array of records. Returns order number of inserted record.
int RARecord::InsertRecord(RARecord*& recs, int& nrec, RARecord* record)
{
	// New storage
	RARecord* new_recs = new RARecord[nrec+1];
	int after = -1;
	if(record->Sequential()) { 
		// Find first sequential record with base_f_n larger than that of record,
		// or first non-sequential record
		for(int i = 0; i<nrec; i++) {
			if(recs[i].Sequential() && (recs[i].base_f_n <= record->base_f_n)) {
				new_recs[i].Copy(recs[i]);
				after = i;
			} else
				new_recs[i+1].Copy(recs[i]);
		}
		new_recs[after+1].Copy(*record);
	} else {
		// Find first non-seq record with base_f_n larger than that of record
		for(int i = 0; i<nrec; i++) {
			if(recs[i].Sequential() || (recs[i].base_f_n <= record->base_f_n)) {
				new_recs[i].Copy(recs[i]);
				after = i;
			} else
				new_recs[i+1].Copy(recs[i]);
		}
		new_recs[after+1].Copy(*record);
	}
	delete [] recs;
	recs = new_recs;
	nrec++;
	return(after+1);
}

// Delete record from an array of records. Returns number of records left.
int RARecord::DeleteRecord(RARecord*& recs, int& nrec, int k)
{
	// New storage
	RARecord* new_recs = new RARecord[nrec-1];
	for(int i = 0; i<nrec; i++) {
		if(i<k)
			new_recs[i].Copy(recs[i]);
		else if(i>k)
			new_recs[i-1].Copy(recs[i]);
	}
	delete [] recs;
	recs = new_recs;
	nrec--;
	return(nrec);
}

int RARecord::UniqueFrames(RARecord*& recs, int& nrec)
{
	// Find max number:
	int i, max_frame = 0;
	for(i = 0; i<nrec; i++) {
		max_frame = YMAX(max_frame, recs[i].base_f_n);
		max_frame = YMAX(max_frame, recs[i].f_n);
	}
	int* has_frame = new int[max_frame+1];
	memset(has_frame, 0, sizeof(int)*(max_frame+1));
	// set 1's where frame exists:
	for(i = 0; i<nrec; i++) {
		has_frame[recs[i].base_f_n] = 1;
		has_frame[recs[i].f_n] = 1;
	}
	// count 1's
	int unique_frames = 0;
	for(i = 0; i<max_frame+1; i++) {
		unique_frames += has_frame[i];
	}
	return(unique_frames);
}

int RARecord::AddRecordToFile(char* filename, RARecord& rec, bool insist)
{
	FILE* fp;
	if(fopen_s(&fp, filename, "a")) { // Cannot add, probably file is open
		if(!insist) return(-1);
		while(!fp) {
			fprintf(stderr, "Cannot add record. Please close the file, if it is open.\n");
			Sleep(10000);
			fopen_s(&fp, filename, "a");
		}
	}
	fseek(fp, 0, SEEK_END);
	rec.WriteCollocations(fp);
	rec.Write(fp);
	fclose(fp);
	return(0);
}

int RARecord::AddRecordToFile(FILE* fp, RARecord& rec, bool insist)
{
	rec.WriteCollocations(fp);
	rec.Write(fp);
	return(0);
}

