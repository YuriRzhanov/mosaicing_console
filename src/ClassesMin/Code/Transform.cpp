#include "Transform.h"
#include <Windows.h>

using namespace std;

char TransformRecord::buf[1024];

TransformRecord::TransformRecord()
{
	type = 'r'; f_n = base_f_n = -1; uniqueTag = 0; anchored = 0; memset(mark, 0, 8); 
	mark[0] = '-';
	w = 0; h = 0; focal_length = 0.0;
	n_col = 0; col_b = nullptr; col_d = nullptr;
	D.Unit();
	av_error = max_error = -1.0; overlap_percent = 0.0;
	essential = false;
	memset(PeakParams, 0, 6*sizeof(double));
}

TransformRecord::TransformRecord(const TransformRecord& r)
{
	type = r.type; f_n=r.f_n; base_f_n=r.base_f_n; uniqueTag=r.uniqueTag;
	anchored = r.anchored; strcpy_s(mark, 8, r.mark);
	w = r.w; h = r.h; focal_length = r.focal_length;
	hmgr = r.hmgr;
	n_col = r.n_col;
	if(n_col>0) {
		col_b = new Coord2[n_col];
		col_d = new Coord2[n_col];
		for(int i = 0; i<n_col; i++) {
			col_b[i].Copy(r.col_b[i]);
			col_d[i].Copy(r.col_d[i]);
		}
	}
	D = r.D;
	av_error = r.av_error;
	max_error = r.max_error; 
	overlap_percent = r.overlap_percent;
	essential = r.essential;
	memcpy(PeakParams, r.PeakParams, 6*sizeof(double));
}

TransformRecord::~TransformRecord() 
{ 
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
}

void TransformRecord::Reset() 
{ 
	SetAttr(); hmgr.Unit(); MarkSet("b"); 
	if(col_b) {
		delete [] col_b;
		col_b = nullptr;
	}
	if(col_d) {
		delete [] col_d;
		col_d = nullptr;
	}
}


void TransformRecord::Copy(const TransformRecord& r)
{
	type = r.type; f_n=r.f_n; base_f_n=r.base_f_n; uniqueTag=r.uniqueTag;
	anchored = r.anchored; strcpy_s(mark, 8, r.mark);
	w = r.w; h = r.h; focal_length = r.focal_length;
	hmgr = r.hmgr;
	n_col = r.n_col; 
	if(n_col>0) {
		col_b = new Coord2[n_col];
		col_d = new Coord2[n_col];
		for(int i = 0; i<n_col; i++) {
			col_b[i].Copy(r.col_b[i]);
			col_d[i].Copy(r.col_d[i]);
		}
	}
	D = r.D;
	av_error = r.av_error;
	max_error = r.max_error; 
	overlap_percent = r.overlap_percent;
	essential = r.essential;
	memcpy(PeakParams, r.PeakParams, 6*sizeof(double));
}

void TransformRecord::Copy(TransformRecord* r)
{
	type = r->type; f_n=r->f_n; base_f_n=r->base_f_n; uniqueTag=r->uniqueTag;
	anchored = r->anchored; strcpy_s(mark, 8, r->mark);
	w = r->w; h = r->h; focal_length = r->focal_length;
	hmgr = r->hmgr;
	n_col = r->n_col; 
	if(n_col>0) {
		col_b = new Coord2[n_col];
		col_d = new Coord2[n_col];
		for(int i = 0; i<n_col; i++) {
			col_b[i].Copy(r->col_b[i]);
			col_d[i].Copy(r->col_d[i]);
		}
	}
	D = r->D;
	av_error = r->av_error;
	max_error = r->max_error; 
	overlap_percent = r->overlap_percent;
	essential = r->essential;
	memcpy(PeakParams, r->PeakParams, 6*sizeof(double));
}

int TransformRecord::MarkAdd(char *s)
{
	char stg[16];
	strcpy_s(stg, 16, s);
	strcat_s(stg, 16, this->mark);
	if(strlen(stg)<8)
		return(strcpy_s(mark, 8, stg)!=0);
	return(-1);
}

int TransformRecord::MarkAdd(char s)
{
	char stg[16];
	sprintf_s(stg, (size_t) 16, "%c", s);
	strcat_s(stg, 16, this->mark);
	if(strlen(stg)<8)
		return(strcpy_s(mark, 8, stg)!=0);
	return(-1);
}

// has 'S'/'F' or doesn't have 'B'/'X'/'E' and does have 's'
bool TransformRecord::MarkGood()
{
	bool status = strstr(mark, "S")!=0 || strstr(mark, "F")!=0 ||
		(strstr(mark, "s")!=0 && strstr(mark, "B")==0 && strstr(mark, "X")==0 && strstr(mark, "E")==0);
	return(status);
}

// has 'B'/'X'/'E'
// if has no capital letters, then has 'b'
// if marked bad, returns 'true'
bool TransformRecord::MarkBad()
{
	if(strstr(mark, "B")==0 && strstr(mark, "X")==0 && strstr(mark, "E")==0 && strstr(mark, "S")==0 && strstr(mark, "F")==0)
		return(strstr(mark, "b")!=0);
	bool status = strstr(mark, "B")!=0 || strstr(mark, "X")!=0 || strstr(mark, "E")!=0;
	return(status);
}

void TransformRecord::SetAttr(int _base_f_n, int _f_n, double _error, double _overlap)
{
	if(_f_n>=0) f_n = _f_n; 
	if(_base_f_n>=0) base_f_n = _base_f_n; 
	uniqueTag = UNIQUETAG(base_f_n,f_n);
	if(_error>=0.0) av_error = _error; 
	if(_overlap>=0.0) overlap_percent = _overlap; 
}

// Lines starting with # indicate comment
// Starting with D indicates second derivative estimate
// By default, collocations are read
#include <assert.h>
int TransformRecord::Read(FILE *fp, char t, int read_collocations)
{
	char tag[64], *p;
	
	// Read until the line is not comment
	do {
		if(fgets(buf, 255, fp)==nullptr) { // End of logfile
			sprintf_s(TransformRecord::buf, (size_t) 1024, "End of file reached.");
			return(TRANS_FATAL);
		}
		if(buf[0]=='%' && read_collocations) // Read collocation string
			ReadCollocations(buf);
	} while(buf[0]=='#' || buf[0]=='%' || buf[0]=='D' || strlen(buf)<3);
	
	ReadHmgrStg(buf, hmgr); // Checks for insanity
	type = t;
	f_n = -1; // Not set by default
	sscanf_s(buf, "%s", tag, 64);
	if(tag[0]=='F') { // World hm with frame number
		sscanf_s(tag, "F%d", &f_n);
		base_f_n = f_n;
		if(type=='r') {
			//fprintf(stderr, "Interpreting world as relative!\n");
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Interpreting world as relative!");
			return(TRANS_FATAL);
		}
	//} else if(tag[0]=='<') { // timecode set
	//	strcpy_s(timecode, 12, file+1);
	//	if((p = strchr(timecode, '>'))!=nullptr)
	//		*p = '\0';
	} else if(strstr(buf, "<-")) { // Relative hm
		sscanf_s(buf, "%d<-%d", &base_f_n, &f_n);
		uniqueTag = UNIQUETAG(base_f_n,f_n);
		if(type=='w') {
			//fprintf(stderr, "Interpreting relative as world!\n");
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Interpreting relative as world!");
			return(TRANS_FATAL);
		}
		// Check for average error etc.
		if(t=='r' && (p = strchr(buf, ']'))!=nullptr) {
			// Next '(':
			p = strchr(p, '(');
			if(p) {
				// Start reading from the next character
				sscanf_s(++p, "%lf %lf", &av_error, &overlap_percent);
				p = strchr(p, ')');
				// Read mark (comment)
				sscanf_s(++p, "%8s", mark, 8);
				// Next '(':
				p = strchr(p, '(');
				sscanf_s(++p, "%lf %lf %lf %lf %lf %lf", &PeakParams[0], &PeakParams[1], 
					&PeakParams[2], &PeakParams[3], &PeakParams[4], &PeakParams[5]);
			}
		}
	}
	
	return(f_n);
}

int TransformRecord::ReadBin(FILE *fp, char t)
{
	int status;
	// Skip until '*' is found:
	do {
		if(fread(buf, 1, 1, fp)!=1) {
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot read binart record.");
			return(TRANS_FATAL);
		}
	} while(buf[0]!='*');
	// Read the rest 151 chars:
	fread(buf+1, 151, 1, fp);
	type = t;
	if(t=='w') {
		if(buf[4]!='F' && buf[6]=='<') {
			//fprintf(stderr, "Reading relative as world.\n");
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Reading relative as world!");
			return(TRANS_FATAL);
		}
		//status = ReadBinHmgrStg(buf, hm); // Checks for insanity
		//for(int k = 0; k<8; k++) {
		//	memcpy(&x, buf+15+k*sizeof(double), sizeof(double));
		//	int j = k/3;
		//	int i = k-3*j;
		//	hmgr.h(j+1,i+1) = x;
		//} Same as below:
		for(int j = 0; j<3; j++) {
			for(int i = 0; i<3; i++) {
				if(i==2 && j==2) continue;
				memcpy(&hmgr.h(j+1,i+1), buf+15+(j*3+i)*sizeof(double), sizeof(double));
			}
		}
		if(!hmgr.Sane()) {
			//cout << "Insane homography: " << hmgr << endl;
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Insane homography.");
			return(TRANS_FATAL);
		}
		status = 0;
		// Must have frame number
		sscanf_s(buf+5, "%d", &f_n); base_f_n = f_n;
		uniqueTag = UNIQUETAG(base_f_n,f_n);
	} else {
		if(buf[4]=='F' && buf[6]!='<') {
			//fprintf(stderr, "Reading world as relative.\n");
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Reading world as relative!");
			return(TRANS_FATAL);
		}
		//for(int k = 0; k<8; k++) {
		//	memcpy(&x, buf+15+k*sizeof(double), sizeof(double));
		//	int j = k/3;
		//	int i = k-3*j;
		//	hmgr.h(j+1,i+1) = x;
		//} Same as below:
		for(int j = 0; j<3; j++) {
			for(int i = 0; i<3; i++) {
				if(i==2 && j==2) continue;
				memcpy(&hmgr.h(j+1,i+1), buf+15+(j*3+i)*sizeof(double), sizeof(double));
			}
		}
		if(!hmgr.Sane()) {
			//cout << "Insane homography: " << hmgr << endl;
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Insane homography.");
			return(TRANS_FATAL);
		}
		status = 0;
		// Must have two frame numbers
		sscanf_s(buf+1, "%d<-%d", &base_f_n, &f_n);
		uniqueTag = UNIQUETAG(base_f_n,f_n);
		// also error and overlap
		memcpy(&av_error, buf+16+8*sizeof(double), sizeof(double));
		memcpy(&overlap_percent, buf+16+9*sizeof(double), sizeof(double));
		// Mark
		memcpy(mark, buf+16+10*sizeof(double), 8);
		// Peak params:
		memcpy(PeakParams, buf+16+10*sizeof(double)+8, 6*sizeof(double));
	}
	return(status);
}

int TransformRecord::ReadCollocations(const char* stg)
{
	char stg2[1024];

	if(stg[0]!='%') return(-1); // Not a collocation string
	sscanf_s(stg, "%*c %d", &n_col); //  Could be 2, 4, etc.
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
	col_b = new Coord2[n_col];
	col_d = new Coord2[n_col];

	istrstream *ist = new istrstream(stg);
	do {
		ist->getline(stg2, 1024, ' ');
	} while(strlen(stg2)==0);
	do {
		ist->getline(stg2, 1024, ' ');
	} while(strlen(stg2)==0);
	for(int k = 0; k<n_col; k++) {
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_b[k].x));
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_b[k].y));
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_d[k].x));
		do {
			ist->getline(stg2, 1024, ' ');
		} while(strlen(stg2)==0);
		sscanf_s(stg2, "%lf", &(col_d[k].y));

		// Assuming that transform has frame size and focal length set:
		col_b[k].SetSizeFocalLength(this->w, this->h, this->focal_length);
		col_d[k].SetSizeFocalLength(this->w, this->h, this->focal_length);
	}
	delete ist;
	return(0);
}

int TransformRecord::Write(FILE *fp, char t)
{
	if(!t) t = type; // Use internal type if not overridden
	if(t=='w') {
		// Tag for world hm is frame number
		if(f_n>-1)
			fprintf(fp, "  F%05d      [", f_n);
		else
			fprintf(fp, "              [");
		fprintf(fp, 
			"%15.12lf %15.12lf %15.12lf %15.12lf %15.12lf %15.12lf %15.12lf %15.12lf",
			hmgr.h(1,1), hmgr.h(1,2), hmgr.h(1,3), hmgr.h(2,1), hmgr.h(2,2), hmgr.h(2,3), 
			hmgr.h(3,1), hmgr.h(3,2));
		fprintf(fp, "]\n");
	} else {
		// Tag for relative hm has both frame numbers
		fprintf(fp, "  %04d<-%04d  [", base_f_n, f_n);
		fprintf(fp, 
			"%15.12lf %15.12lf %15.12lf %15.12lf %15.12lf %15.12lf %15.12lf %15.12lf",
			hmgr.h(1,1), hmgr.h(1,2), hmgr.h(1,3), hmgr.h(2,1), hmgr.h(2,2), hmgr.h(2,3), 
			hmgr.h(3,1), hmgr.h(3,2));
		fprintf(fp, "] (%8.4lf %8.4lf) %8s (%9.6lf %9.6lf %9.4lf  %9.6lf %9.6lf %9.4lf)\n", 
			av_error, overlap_percent, mark, 
			PeakParams[0], PeakParams[1], PeakParams[2],
			PeakParams[3], PeakParams[4], PeakParams[5]);
	}
	return(0);
}

int TransformRecord::WriteHmgr(FILE *fp, char t)
{
	char tag[64];
	sprintf_s(tag, (size_t) 16, "  F%05d", f_n);
	hmgr.Save(fp, tag);
	return(0);
}

int TransformRecord::WriteBinHmgr(FILE *fp, char t)
{
	memset(buf, 0, 136);
	if(t=='w') {
		// Tag for world hm has frame number
		if(f_n>-1)
			sprintf_s(buf, (size_t) 1024, "*   F%05d    [", f_n);
		else
			sprintf_s(buf, (size_t) 1024, "*             [");
		//for(int k = 0; k<8; k++) {
		//	int j = k/3;
		//	int i = k-j*3;
		//	memcpy(buf+15+i*sizeof(double), &hmgr.h(j+1,i+1), sizeof(double));
		//} Same as below:
		for(int j = 0; j<3; j++) {
			for(int i = 0; i<3; i++) {
				if(i==2 && j==2) continue;
				memcpy(buf+15+(j*3+i)*sizeof(double), &hmgr.h(j+1,i+1), sizeof(double));
			}
		}
		buf[15+8*sizeof(double)] = ']';
	} else {
		// Tag for relative hm has both frame numbers
		sprintf_s(buf, (size_t) 1024, "* %04d<-%04d  [", base_f_n, f_n);
		//for(int k = 0; k<8; k++) {
		//	int j = k/3;
		//	int i = k-j*3;
		//	memcpy(buf+15+i*sizeof(double), &hmgr.h(j+1,i+1), sizeof(double));
		//} Same as below:
		for(int j = 0; j<3; j++) {
			for(int i = 0; i<3; i++) {
				if(i==2 && j==2) continue;
				memcpy(buf+15+(j*3+i)*sizeof(double), &hmgr.h(j+1,i+1), sizeof(double));
			}
		}
		buf[15+8*sizeof(double)] = ']';
		memcpy(buf+16+8*sizeof(double), &av_error, sizeof(double));
		memcpy(buf+16+9*sizeof(double), &overlap_percent, sizeof(double));
		// Mark
		memcpy(buf+16+10*sizeof(double), mark, 8);
		// Peak params:
		memcpy(buf+16+10*sizeof(double)+8, PeakParams, 4*sizeof(double));
	}
	return((int)fwrite(buf, 136, 1, fp));
}

char *TransformRecord::WriteStg(int digits)
{
	char format[1024];
	if(digits<0) digits = 12;
	sprintf_s(format, (size_t) 1024, "  %%04d<-%%04d  [%%%d.%dlf %%%d.%dlf %%%d.%dlf %%%d.%dlf"
		" %%%d.%dlf %%%d.%dlf %%%d.%dlf %%%d.%dlf] (%%8.4lf %%8.4lf) %%8s "
		"(%%9.6lf %%9.6lf %%9.4lf  %%9.6lf %%9.6lf %%9.4lf)\n",
		digits+3, digits, digits+3, digits, digits+3, digits, digits+3, digits, 
		digits+3, digits, digits+3, digits, digits+3, digits, digits+3, digits);

	// Tag for relative hm has both frame numbers
	sprintf_s(buf, (size_t) 1024, format, 
		base_f_n, f_n, 
		hmgr.h(1,1), hmgr.h(1,2), hmgr.h(1,3), hmgr.h(2,1), hmgr.h(2,2), hmgr.h(2,3), 
		hmgr.h(3,1), hmgr.h(3,2), 
		av_error, overlap_percent, mark, 
		PeakParams[0], PeakParams[1], PeakParams[2],
		PeakParams[3], PeakParams[4], PeakParams[5]);	
	return(buf);
}

void TransformRecord::SetCollocations(Collocations* pCol)
{
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
	n_col = pCol->n;
	col_b = new Coord2[n_col];
	col_d = new Coord2[n_col];
	for(int i = 0; i<n_col; i++) {
		col_b[i].Copy(pCol->b[i]);
		col_d[i].Copy(pCol->d[i]);
	}
}

Collocations* TransformRecord::GetCollocations()
{
	Collocations* pCol = new Collocations(n_col, w, h, focal_length);
	for(int i = 0; i<n_col; i++) {
		pCol->b[i].Copy(col_b[i]); // Copying also passes w,h,focal_length
		pCol->d[i].Copy(col_d[i]);
	}
	return(pCol);
}

char* TransformRecord::WriteCollocations()
{
	//sprintf_s(buf, (size_t) 1024, "%% %2d  %8.2lf %8.2lf  %8.2lf %8.2lf  %8.2lf %8.2lf  %8.2lf %8.2lf\r\n",
	//		2, col_b[0].X(), col_b[0].Y(), col_d[0].X(), col_d[0].Y(),
	//		col_b[1].X(), col_b[1].Y(), col_d[1].X(), col_d[1].Y());

	// Variable number of collocations
	sprintf_s(buf, (size_t) 1024, "%% %2d  ", n_col);
	int len = (int)strlen(buf);

	for(int i = 0; i<n_col; i++) {
		sprintf_s(buf+len, (size_t) (1024-len), "%8.2lf %8.2lf  %8.2lf %8.2lf  ",
			col_b[i].X(), col_b[i].Y(), col_d[i].X(), col_d[i].Y());
		len = (int)strlen(buf);
	}
	sprintf_s(buf+len, (size_t) (1024-len), "\n");
	return(buf);
}

void TransformRecord::DefaultCollocations()
{
	if(col_b) delete [] col_b;
	if(col_d) delete [] col_d;
	n_col = 4;
	col_b = new Coord2[n_col];
	col_d = new Coord2[n_col];
		// 'col_d' is where point 'col_b' in 'base' image appears in 'derived' image
	Hmgr inv_hmgr = hmgr.Invert();
	col_b[0].Set(0.25*w, 0.25*h); 
	col_b[1].Set(0.75*w, 0.75*h); 
	col_b[2].Set(0.75*w, 0.25*h); 
	col_b[3].Set(0.25*w, 0.75*h); 

	for(int i = 0; i<n_col; i++) {
		col_b[i].SetSize(w, h);
		col_b[i].SetFocalLength(focal_length);
		col_d[i].SetSize(w, h);
		col_d[i].SetFocalLength(focal_length);
		col_d[i] = inv_hmgr.Map(col_b[i]);
	}
}

int TransformRecord::ReadHmgr(const char *file)
{
	FILE *fp;
	if(fopen_s(&fp, file, "r")) {
		//fprintf(stderr, "Cannot open file \"%s\"", file);
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot open file \"%s\"", file);
		return(TRANS_FATAL);
	}
	int status = ReadHmgr(fp);
	fclose(fp);
	return(status);
}

int TransformRecord::ReadHmgr(FILE *fp)
{
	if(fgets(buf, 255, fp)==nullptr) { // End of logfile
		sprintf_s(TransformRecord::buf, (size_t) 1024, "End of file reached.");
		return(TRANS_FATAL);
	}
	return(ReadHmgrStg(buf, hmgr));
}

int TransformRecord::ReadHmgrStg(const char *stg, Hmgr& _hmgr)
{
	char *p = (char*)stg;
	
	if((p = strchr((char*)stg, '[')))
		p++; // Read from this character

	//istrstream ist(p); &&& Doesn't always work on Windows!
	istrstream *ist = new istrstream(p);
	for(int j = 0; j<3; j++) {
		for(int i = 0; i<3; i++) {
			if(i==2 && j==2) continue;
			*ist >> _hmgr.h(j+1,i+1);
		}
	}
	delete ist;

	if(!_hmgr.Sane()) {
		//cout << "Insane homography: " << _hmgr << endl;
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Insane homography.");
		return(TRANS_FATAL);
	}
	return(0);
}

int TransformRecord::ReadBinHmgrStg(const char *stg, Hmgr& _hmgr)
{
	char *p;
	if((p = strchr((char*)stg, '['))==nullptr) {
		//fprintf(stderr, "Cannot find start of homography.\n");
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot find start of homography.");
		return(TRANS_FATAL);
	}
	//for(int k = 0; k<8; k++) {
	//	int j = k/3;
	//	int i = k-3*j;
	//	memcpy(&_hmgr.h(j+1,i+1), p+1+k*sizeof(double), sizeof(double));
	//} Same as below:
	for(int j = 0; j<3; j++) {
		for(int i = 0; i<3; i++) {
			if(i==2 && j==2) continue;
			memcpy(&_hmgr.h(j+1,i+1), p+1+(j*3+i)*sizeof(double), sizeof(double));
		}
	}
	if(!_hmgr.Sane()) {
		//cout << "Insane homography: " << _hmgr << endl;
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Insane homography.");
		return(TRANS_FATAL);
	}
	return(0);
}

void TransformRecord::Invert()
{
	Hmgr inv_hmgr = hmgr.Invert(); SetHmgr(inv_hmgr);
	int tmp = base_f_n; base_f_n = f_n; f_n = tmp; // swap base and derived
	uniqueTag = UNIQUETAG(base_f_n,f_n);
}

// Reads both binary and ASCII records. Returns frame size and focal length
// (which is obsolete, because each record has this info).
TransformRecord *TransformRecord::ReadRecs(char *hmfile, int *nrec, 
	int *_W, int *_H, double *_focal_length, char t)
{
	int w = 0, h = 0;
	double fl = 0.0;

	FILE *fp_log;
	if(fopen_s(&fp_log, hmfile, "r")) {
		//fprintf(stderr, "Cannot open %s.\n", hmfile);
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot open \"%s\".\n", hmfile);
		return(nullptr);
	}
	// Check for being binary and having info
	char c[5];
	fread(c, 5, 1, fp_log);
	fclose(fp_log);
	if(c[0]=='*') { // Binary
		if(strncmp(c, "*Info", 5)==0) { // Has info. w/h/fl are set for each record.
			char *pInfo = nullptr;
			TransformRecord *pRecords = ReadBinRecs(hmfile, nrec, &pInfo, t);
			if(!pRecords)
				return(nullptr);
			if(_W) *_W = pRecords[0].w;
			if(_H) *_H = pRecords[0].h;
			if(_focal_length) *_focal_length = pRecords[0].focal_length;
			delete pInfo;
			return(pRecords);
		} else { // Doesn't have info. w/h/fl are not set.
			TransformRecord *pRecords = ReadBinRecs(hmfile, nrec, nullptr, t);
			return(pRecords);
		}
	} else { // ASCII
		if(Y_Utils::HasInfo(hmfile)) { // Has info
			char *pInfo = Y_Utils::GetInfo(hmfile);
			Y_Rect<int> *pFrame = Y_Utils::GetFrameSize(hmfile);
			if(pFrame) {
				w = pFrame->w;
				if(_W) *_W = pFrame->w;
				h = pFrame->h;
				if(_H) *_H = pFrame->h;
			}
			fl = Y_Utils::GetFocalLength(pInfo);
			if(_focal_length) *_focal_length = fl;
			delete pFrame;
			delete [] pInfo;
		} else { // no info
			if(_W) *_W = 0;
			if(_H) *_H = 0;
			if(_focal_length) *_focal_length = -1.0;
		} 
	}
	fopen_s(&fp_log, hmfile, "r");
	int n = 0;
	TransformRecord rec;
	// Don't read collocations
	while(rec.Read(fp_log, t, 0)>TRANS_FATAL)
		n++;
	fprintf(stderr, "Number of HM records = %d\n", n);
	if(n<=0) {
		fclose(fp_log);
		*nrec = 0;
		sprintf_s(TransformRecord::buf, (size_t) 1024, "File has no records.");
		return(nullptr);
	}
	rewind(fp_log);
	TransformRecord *pRecords = new TransformRecord[n];
	for(int i = 0; i<n; i++) {
		pRecords[i].SetSizeFocalLength(w, h, fl);
		pRecords[i].Read(fp_log, t);
	}
	*nrec = n;
	
	fclose(fp_log);
	return(pRecords);
}

TransformRecord *TransformRecord::ReadRecs(char *hmfile, int *nrec, char **_ppInfo, char t)
{
	int w = 0, h = 0;
	double fl = 0.0;

	FILE *fp_log;
	if(fopen_s(&fp_log, hmfile, "r")) {
		//fprintf(stderr, "Cannot open %s.\n", hmfile);
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot open \"%s\".\n", hmfile);
		return(nullptr);
	}
	// Check for being binary and having info
	char c[5];
	fread(c, 5, 1, fp_log);
	fclose(fp_log);
	if(c[0]=='*') { // Binary
		if(strncmp(c, "*Info", 5)==0) { // Has info
			char* pInfo = nullptr;
			TransformRecord *pRecords = ReadBinRecs(hmfile, nrec, &pInfo, t);
			if(_ppInfo)
				*_ppInfo = pInfo;
			else
				delete [] pInfo;
			return(pRecords);
		} else { // Doesn't have info
			TransformRecord *pRecords = ReadBinRecs(hmfile, nrec, nullptr, t);
			if(_ppInfo) *_ppInfo = nullptr;
			return(pRecords);
		}
	} else { // ASCII
		if(Y_Utils::HasInfo(hmfile)) { // w/h/fl will be set
			char *pInfo = Y_Utils::GetInfo(hmfile);
			Y_Rect<int> *pFrame = Y_Utils::GetFrameSize(hmfile);
			w = pFrame->w;
			h = pFrame->h;
			fl = Y_Utils::GetFocalLength(pInfo);
			delete pFrame;
			if(_ppInfo) 
				*_ppInfo = pInfo;
			else
				delete [] pInfo;
		} else { // w/h/fl will not be set
			if(_ppInfo) *_ppInfo = nullptr;
		}
	}
	fopen_s(&fp_log, hmfile, "r");
	int n = 0;
	// Don't read collocations
	TransformRecord rec;
	while(rec.Read(fp_log, t, 0)>TRANS_FATAL)
		n++;
	fprintf(stderr, "Number of HM records = %d\n", n);
	if(n<=0) {
		fclose(fp_log);
		*nrec = 0;
		sprintf_s(TransformRecord::buf, (size_t) 1024, "File has no records.");
		return(nullptr);
	}
	rewind(fp_log);
	TransformRecord *pRecords = new TransformRecord[n];
	for(int i = 0; i<n; i++) {
		pRecords[i].SetSizeFocalLength(w, h, fl);
		pRecords[i].Read(fp_log, t);
	}
	*nrec = n;
	
	fclose(fp_log);
	return(pRecords);
}

TransformRecord *TransformRecord::ReadBinRecs(char *hmfile, int *nrec, char **_ppInfo, char t)
{
	int w = 0, h = 0;
	double fl = 0.0;
	long offset = 0;

	FILE *fp_log;
	if(fopen_s(&fp_log, hmfile, "r")) {
		//fprintf(stderr, "Cannot open %s.\n", hmfile);
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot open \"%s\".\n", hmfile);
		return(nullptr);
	}
	// Check whether file has info
	char Info[INFO_SIZE];
	fread(Info, INFO_SIZE, 1, fp_log);

	if(strncmp(Info, "*Info", 5)==0) { // Has info. w/h/fl are set for each record.
		Y_Rect<int> *pFrame = Y_Utils::GetFrameSizeFromInfo(Info);
		if(pFrame) {
			w = pFrame->w; h = pFrame->h;
			delete pFrame;
		}
		fl = Y_Utils::GetFocalLength(Info);
		if(_ppInfo) { // Info if requested
			// If points to nullptr, allocate new
			if(!*_ppInfo) *_ppInfo = new char[INFO_SIZE];
			memcpy(*_ppInfo, Info, INFO_SIZE);
		}
		offset = INFO_SIZE;
	} else { // No info, rewind file
		if(_ppInfo) *_ppInfo = nullptr;
		fseek(fp_log, 0, SEEK_SET);
	}
	int n = 0;
	TransformRecord rec;
	while(rec.ReadBin(fp_log, t)>TRANS_FATAL)
		n++;
	fprintf(stderr, "Number of HM records = %d\n", n);
	if(n<=0) {
		fclose(fp_log);
		*nrec = 0;
		sprintf_s(TransformRecord::buf, (size_t) 1024, "File has no records.");
		return(nullptr);
	}
	fseek(fp_log, offset, SEEK_SET); // 'offset' is either INFO_SIZE or 0.
	TransformRecord *pRecords = new TransformRecord[n];
	
	for(int i = 0; i<n; i++) {
		pRecords[i].SetSizeFocalLength(w, h, fl);
		pRecords[i].ReadBin(fp_log, t);
	}
	*nrec = n;
	
	fclose(fp_log);
	return(pRecords);
}

int TransformRecord::SaveRecs(char *hmfile, TransformRecord *recs, int nrec, 
	char *_pInfo, char t)
{
	if(!t) t = recs[0].type; // Use internal type
	if(t!='r' && t!='w') {
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Unknown type qualifier \'%c\'.", t);
		//fprintf(stderr, "Unknown type qualifier \'%c\'.\n", t);
		return(TRANS_NONFATAL);
	}
	FILE *fp;
	if(fopen_s(&fp, hmfile, "w")) {
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot save to \"%s\".\n", hmfile);
		return(TRANS_NONFATAL);
	}
	if(_pInfo) {
		// Guarantee ASCII format
		_pInfo[0] = '#';
		fprintf(fp, "%s", _pInfo);
	}
	for(int i = 0; i<nrec; i++) {
		if(recs[i].n_col) {
			fprintf(fp, "%s", recs[i].WriteCollocations());
			fflush(fp);
		}
		recs[i].Write(fp, t);
	}
	fclose(fp);
	fprintf(stderr, "\"%s\": Saved %d records.\n", hmfile, nrec);
	return(0);
}

int TransformRecord::SaveBinRecs(char *hmfile, TransformRecord *recs, int nrec, 
	char *_pInfo, char t)
{
	if(!t) t = recs[0].type; // Use internal type
	if(t!='r' && t!='w') {
		//fprintf(stderr, "Unknown qualifier \'%c\'.\n", t);
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Unknown type qualifier \'%c\'.", t);
		return(TRANS_NONFATAL);
	}
	FILE *fp;
	if(fopen_s(&fp, hmfile, "w")) {
		sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot save to \"%s\".\n", hmfile);
		return(TRANS_NONFATAL);
	}
	if(_pInfo) {
		memset(buf, 0, INFO_SIZE);
		strcpy_s(buf, 1024, _pInfo);
		// Guarantee bin format
		buf[0] = '*';
		if(fwrite(buf, INFO_SIZE, 1, fp)!=1) {
			sprintf_s(TransformRecord::buf, (size_t) 1024, "Cannot write to file.");
			return(-1);
		}
	}
	for(int i = 0; i<nrec; i++) {
		//recs[i].WriteBinHomography(fp, t);
		recs[i].WriteBinHmgr(fp, t);
	}
	fclose(fp);
	//fprintf(stderr, "\"%s\": Saved %d records.\n", hmfile, nrec);
	return(0);
}

TransformRecord *TransformRecord::FindRecordFromFrameNumber(TransformRecord *recs, 
	int nrec, int _f_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].f_n==_f_n)
			return(&recs[i]);
	}
	return(nullptr);
}

TransformRecord *TransformRecord::FindRecordFromBaseFrameNumber(TransformRecord *recs, 
	int nrec, int _b_f_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_f_n==_b_f_n)
			return(&recs[i]);
	}
	return(nullptr);
}

TransformRecord *TransformRecord::FindRecordFromNumbers(TransformRecord *recs, 
	int nrec, int _b_f_n, int _f_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_f_n==_b_f_n && recs[i].f_n==_f_n)
			return(&recs[i]);
	}
	return(nullptr);
}

int TransformRecord::FindOrderNumberFromFrameNumber(TransformRecord *recs, int nrec, int _f_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].f_n==_f_n)
			return(i);
	}
	return(TRANS_FATAL);
}

int TransformRecord::FindOrderNumberFromNumbers(TransformRecord *recs, int nrec,
		int _b_f_n, int _f_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_f_n==_b_f_n && recs[i].f_n==_f_n)
			return(i);
	}
	return(TRANS_FATAL);
}

// Create new array on the same records
TransformRecord* TransformRecord::CopyAll(TransformRecord *recs, int nrec)
{
	TransformRecord* new_recs = new TransformRecord[nrec];
	for(int i = 0; i<nrec; i++) {
		new_recs[i].Copy(recs[i]);
	}
	return(new_recs);
}

// Set all essential fields to 'mark'
void TransformRecord::MarkAllEssential(TransformRecord *recs, int nrec, bool mark)
{
	for(int i = 0; i<nrec; i++) {
		recs[i].essential = mark;
	}
}

// Add the record to an array of records. Returns order number of inserted record.
int TransformRecord::InsertRecord(TransformRecord*& recs, int& nrec, TransformRecord* record)
{
	// New storage
	TransformRecord* new_recs = new TransformRecord[nrec+1];
	int after = -1;
	if(record->type=='r') { // relative records
		// Find first sequential record with base_f_n larger than that of record,
		// or first non-sequential record
		for(int i = 0; i<nrec; i++) {
			if(recs[i].Sequential() && (recs[i].base_f_n <= record->base_f_n)) {
				new_recs[i].Copy(recs[i]);
				after = i;
			} else
				new_recs[i+1].Copy(recs[i]);
		}
		new_recs[after+1].Copy(*record);
	} else { // world records
		for(int i = 0; i<nrec; i++) {
			if(recs[i].Sequential() || (recs[i].base_f_n <= record->base_f_n)) {
				new_recs[i].Copy(recs[i]);
				after = i;
			} else
				new_recs[i+1].Copy(recs[i]);
		}
		new_recs[after+1].Copy(*record);
	}
	delete [] recs;
	recs = new_recs;
	nrec++;
	return(after+1);
}

// Delete record from an array of records. Returns number of records.
int TransformRecord::DeleteRecord(TransformRecord*& recs, int& nrec, int k)
{
	// New storage
	TransformRecord* new_recs = new TransformRecord[nrec-1];
	for(int i = 0; i<nrec; i++) {
		if(i<k)
			new_recs[i].Copy(recs[i]);
		else if(i>k)
			new_recs[i-1].Copy(recs[i]);
	}
	delete [] recs;
	recs = new_recs;
	nrec--;
	return(nrec);
}

int TransformRecord::AddRecordToFile(const char* filename, TransformRecord& rec, bool insist)
{
	FILE* fp;
	if(fopen_s(&fp, filename, "a")) { // Cannot add, probably file is open
		if(!insist) return(-1);
		while(!fp) {
			fprintf(stderr, "Cannot add record. Please close the file, if it is open.\n");
			Sleep(10000);
			fopen_s(&fp, filename, "a");
		}
	}
	fseek(fp, 0, SEEK_END);
	if(rec.n_col>0)
		rec.WriteCollocations(fp);
	rec.Write(fp);
	fclose(fp);
	return(0);
}

int TransformRecord::AddRecordToFile(FILE* fp, TransformRecord& rec, bool insist)
{
	if(rec.n_col>0)
		rec.WriteCollocations(fp);
	rec.Write(fp);
	return(0);
}




TransformRecordList::TransformRecordList(int s_N)
{
	N = s_N; 
	start = new TransformRecord*[N+2]; 
	current = *start; c = 0;
	memset(start, 0, N*sizeof(TransformRecord*));
	n = 0;
}

TransformRecordList::TransformRecordList(char *file)
{
	fprintf(stderr, "Not implemented yet.\n");
	exit(1);
}


TransformRecordList::~TransformRecordList()
{
	for(int k = 0; k<N; k++) {
		if(start[k]) delete start[k];
	}
	delete [] start;
}

// Find relative transform relating two given frames
TransformRecord *TransformRecordList::Find(int _derived_n, int _base_n)
{
	for(int k = 0; k<N; k++) {
		if(start[k] && start[k]->f_n==_derived_n && start[k]->base_f_n==_base_n)
			return(start[k]);
	}
	return(nullptr);
}

// Find relative transform with given base frame.
// By default start with 0-th relative transform
TransformRecord *TransformRecordList::FindBase(int _base_n, int *from)
{
	int k_start = 0;
	if(from) k_start = *from;
	for(int k = k_start; k<N; k++) {
		if(start[k] && start[k]->base_f_n==_base_n) {
			if(from) *from = k+1;
			return(start[k]);
		}
	}
	return(nullptr);
}

// Find relative transform with given derived frame
TransformRecord *TransformRecordList::FindDerived(int _derived_n, int *from)
{
	int k_start = 0;
	if(from) k_start = *from;
	for(int k = k_start; k<N; k++) {
		if(start[k] && start[k]->f_n==_derived_n) {
			if(from) *from = k+1;
			return(start[k]);
		}
	}
	return(nullptr);
}



void TransformRecordList::Print(const char *filename)
{
	FILE *fp;
	if(fopen_s(&fp, filename, "w")) {
		fprintf(stderr, "Cannot open \"%s\" for writing.\n", filename);
		return;
	}
	fprintf(fp, "%d records, type \"%c\"\n", N, start[0]->type);
	for(int k = 0; k<N; k++) {
		fprintf(fp, "%04d %04d<-%04d %c\n", k,  start[k]->base_f_n, start[k]->f_n,
			(start[k]->anchored) ? 'A' : 'N');
	}
	fclose(fp);
}
