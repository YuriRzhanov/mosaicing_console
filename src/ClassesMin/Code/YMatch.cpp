// Added Dec 17, 2006

#include "YMatch.h"

char Y_Match::buf[1024];
int Y_Match::w = 0;
int Y_Match::h = 0;
double Y_Match::focal_length = -1.0;

void Y_Match::GetFromString(char *stg, int len)
{
	double xl, yl, xr, yr;
	if(strstr(stg, "<->"))
		sscanf_s(stg, "%lf %lf %*s %lf %lf", &xl, &yl, &xr, &yr);
	else
		sscanf_s(stg, "%lf %lf %lf %lf", &xl, &yl, &xr, &yr);
	left.Set(xl, yl);
	right.Set(xr, yr);
}

void Y_Match::Scale(double factor)
{
	left *= factor;
	right *= factor;
}

int Y_Match::Read(FILE* fp)
{
	// Read until the line is not mark
	do {
		if(fgets(buf, 1023, fp)==nullptr) // End of logfile
			return(-100);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	GetFromString(buf, 1023);
	return(0);
}

Y_Match* Y_Match::ReadMatches(const char* name, int& N_matches, int& w, int& h, double focal_length)
{
	FILE *fp_log;
	if(fopen_s(&fp_log, name, "r")) {
		fprintf(stderr, "Cannot open %s.\n", name);
		return(nullptr);
	}
	char stg[256];
	int n = 0;
	while(fgets(stg, 256, fp_log)) {
		if(stg[0]=='#' && strstr(stg, "W="))
			Y_Utils::GetFrameSize(stg, w, h);
		else if(strlen(stg)>5)
			n++;
	}
	fprintf(stderr, "Number of matches = %d\n", n);
	N_matches = n;
	Y_Match *matches = new Y_Match[N_matches];
	rewind(fp_log);
	n = 0;
	while(fgets(stg, 256, fp_log) && strlen(stg)>5) {
		if(stg[0]!='#') {
			matches[n].GetFromString(stg, 256);
			matches[n].SetSize(w, h);
			if(!EPSILON_EQ(focal_length,0.0))
				matches[n].SetFocalLength(focal_length);
			n++;
		}
	}
	fclose(fp_log);
	
	return(matches);
}

int Y_Match::WriteMatches(const char* name, Y_Match* pMatches, int N_matches, bool writeScore, int _w, int _h)
{
	FILE *fp_log;
	if(fopen_s(&fp_log, name, "w")) {
		fprintf(stderr, "Cannot open %s.\n", name);
		return(-1);
	}
	if(_w!=0)
		fprintf(fp_log, "# W=%d H=%d\n", _w, _h);
	if(writeScore) {
		for(int i = 0; i<N_matches; i++) {
			if(pMatches[i].good)
				fprintf(fp_log, "%.4lf\t%.4lf\t%.4lf\t%.4lf\t%.4lf\n", 
					pMatches[i].left.x, pMatches[i].left.y, 
					pMatches[i].right.x, pMatches[i].right.y, pMatches[i].score);
		}
	} else {
		for(int i = 0; i<N_matches; i++) {
			if(pMatches[i].good)
				fprintf(fp_log, "%.4lf\t%.4lf\t%.4lf\t%.4lf\n", 
					pMatches[i].left.x, pMatches[i].left.y, 
					pMatches[i].right.x, pMatches[i].right.y);
		}
	}

	fclose(fp_log);
	return(0);
}

vector<Y_Match> Y_Match::ReadMatches(const char* name, int& w, int& h, double focal_length)
{
	vector<Y_Match> matches;
	FILE *fp;
	if(fopen_s(&fp, name, "r")) {
		fprintf(stderr, "Cannot open %s.\n", name);
		return(matches);
	}
	char stg[256];
	int n = 0;
	while(fgets(stg, 256, fp)) {
		if(stg[0]=='#' && strstr(stg, "W="))
			Y_Utils::GetFrameSize(stg, w, h);
		else if(strlen(stg)>5)
			n++;
	}
	fprintf(stderr, "Number of matches = %d\n", n);
	rewind(fp);
	Y_Match match;
	while(fgets(stg, 256, fp) && strlen(stg)>5) {
		if(stg[0]!='#') {
			match.GetFromString(stg, 256);
			match.SetSize(w, h);
			if(!EPSILON_EQ(focal_length,0.0))
				match.SetFocalLength(focal_length);
			matches.push_back(match);
		}
	}
	fclose(fp);
	return(matches);
}

int Y_Match::WriteMatches(const char* name, vector<Y_Match>& matches, bool writeScore, int _w, int _h)
{
	int N_matches = (int)matches.size();
	FILE *fp;
	if(fopen_s(&fp, name, "w")) {
		fprintf(stderr, "Cannot open %s.\n", name);
		return(-1);
	}
	if(_w!=0)
		fprintf(fp, "# W=%d H=%d\n", _w, _h);
	if(writeScore) {
		for(int i = 0; i<N_matches; i++) {
			if(matches[i].good)
				fprintf(fp, "%.4lf\t%.4lf\t%.4lf\t%.4lf\t%.4lf\n", 
					matches[i].left.x, matches[i].left.y, 
					matches[i].right.x, matches[i].right.y, matches[i].score);
		}
	} else {
		for(int i = 0; i<N_matches; i++) {
			if(matches[i].good)
				fprintf(fp, "%.4lf\t%.4lf\t%.4lf\t%.4lf\n", 
					matches[i].left.x, matches[i].left.y, 
					matches[i].right.x, matches[i].right.y);
		}
	}
	fclose(fp);
	return(0);
}

int Y_Match::AddMatches(const char* name, vector<Y_Match> matches, char* pHeader)
{
	FILE* fp;
	if(fopen_s(&fp, name, "a")) {
		return(-1);
	}
	fseek(fp, 0, SEEK_END);
	if(pHeader)
		fprintf(fp, "%s", pHeader);
	for(int i = 0; i<(int)matches.size(); i++) {
		if(matches[i].good)
			fprintf(fp, "%10.3lf %10.3lf  <->  %10.3lf %10.3lf\n", matches[i].left.x, matches[i].left.y,
				matches[i].right.x, matches[i].right.y);
	}
	fclose(fp);
	return(0);
}


