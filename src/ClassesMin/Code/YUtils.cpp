#include <limits.h>
#include "YUtils.h"

using namespace std;

u_short Y_Utils::consoleTextAttribute = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN;
u_char Y_Utils::_INVALID_UCHAR = 0;
u_short Y_Utils::_INVALID = 0;
u_short Y_Utils::_INVALID_USHORT = 35565;
int Y_Utils::_INVALID_INT = INT_MIN;
float Y_Utils::_INVALID_FLOAT = (float) log(-1.0);
double Y_Utils::_INVALID_DOUBLE = log(-1.0);

bool Y_Utils::IncreaseIntBufferIfNeeded(int*& buffer, int& allocatedLength, int currentLength, int slack, int factor)
{
	if((allocatedLength-currentLength)>slack) // no need
		return(true);
	int* s = new int[allocatedLength*factor];
	if(s==nullptr)
		return(false);
	memset(s, 0, allocatedLength*factor*sizeof(int));
	memcpy(s, buffer, currentLength*sizeof(int));
	delete [] buffer;
	buffer = s;
	allocatedLength *= factor;
	return(true);
}

void Y_Utils::CheckValid(int day, int mon, int year)
{
	time_t l_clock = time(&l_clock);
	int nday, nmon, nyear;
	
	struct tm d_date;
	gmtime_s(&d_date, &l_clock);
	nday = d_date.tm_mday;
	nmon = d_date.tm_mon+1;
	nyear = 1900+d_date.tm_year;
	
	//fprintf(stdout, "%s", ctime(&l_clock));
	//fprintf(stdout, "%4d  %4d  %4d\n", d_date.tm_mday, d_date.tm_mon+1,
	//	1900+d_date.tm_year);
	
	if(nyear>year || 
			(nyear==year && nmon>mon) ||
			(nyear==year && nmon==mon && nday>day)) {
		abort();
	}
/*
#ifndef WIN32
#else
	time_t l_clock;  // C run-time time (defined in <time.h>)
	int nday, nmon, nyear;

	time(&l_clock);  // Get the current time from the operating system.
	struct tm d_date;
	gmtime_s(&d_date, &l_clock);
	nday = d_date.tm_mday;
	nmon = d_date.tm_mon+1;
	nyear = 1900+d_date.tm_year;
	
	//fprintf(stdout, "%s", ctime(&l_clock));
	//fprintf(stdout, "%4d  %4d  %4d\n", d_date.tm_mday, d_date.tm_mon+1,
	//	1900+d_date.tm_year);
	
	if(nyear>year || 
			(nyear==year && nmon>mon) ||
			(nyear==year && nmon==mon && nday>day)) {
		abort();
	}
	//CTime t(osBinaryTime);
	//if(t.GetYear()>year || 
	//		(t.GetYear()==year && t.GetMonth()>mon) ||
	//		(t.GetYear()==year && t.GetMonth()==mon && t.GetDay()>day)) {
	//	abort();
	//}
#endif
	*/
}

char *Y_Utils::ExtractParam(const char *stg, const char *tag)
{
	if(!stg) return(nullptr);
	char *p;
	if((p = strstr((char*)stg, tag))!=nullptr) {
		return(p+strlen(tag));
	}
	return(nullptr);
}

// Aliased line
// 'N' - size of allocated buffer of Point2's
int Y_Utils::bresenham(Point2<int> start, Point2<int> end, Point2<int> *pp, int N)
{
	int x0 = start.x, y0 = start.y;
	int x1 = end.x, y1 = end.y;
	int x = x0, y = y0;
	int dx = abs(x1 - x0), dy = abs(y1 - y0);
	int m, s1, s2, temp, swapped = 0, e;	
	int count = 0;
	
	pp[count].x = x;
	pp[count].y = y;
	++count;
	s1 = (x1 > x0) ? 1 : (x1 < x0) ? -1 : 0;
	s2 = (y1 > y0) ? 1 : (y1 < y0) ? -1 : 0;
	if(dy > dx) { 
		temp = dx; dx = dy; dy = temp; swapped = 1; 
	}
	e = 2*dy-dx;
	for(m = 1; m <= dx; ++m) {
		while (e >= 0) {
			if(swapped) 
				x += s1; 
			else 
				y += s2;
			e -= 2*dx;
			pp[count].x = x;
			pp[count].y = y;
			++count;
			if(count==N-1) {
				fprintf(stderr, "Insufficient buffer %d.\n", N);
				return(-1);
			}
		}
		if(swapped) 
			y += s2; 
		else 
			x += s1;
		e += 2*dy;
		pp[count].x = x;
		pp[count].y = y;
		++count;
		if(count==N-1) {
			fprintf(stderr, "Insufficient buffer %d.\n", N);
			return(-1);
		}
	}
	return(count);
}

// Aliased circle
int Y_Utils::brescirc(Point2<int> center, int radius, Y_Rect<int> R, Point2<int> *pP, int N)
{
	int	ret, i;
	int total_pts = 0; // All points in circle
	Point2<int> pp[8], pt;

	pt.x = 0;
	pt.y = radius;
	int	d = 3-(radius+radius);
	while(pt.x<pt.y) {
		ret = plotpts(center, pt, R, pp, 8);
		if(total_pts+ret>N) return(-1);
		if(ret) {
			for(i = total_pts; i<total_pts+ret; i++) {
				pP[i].x = pp[i-total_pts].x;
				pP[i].y = pp[i-total_pts].y;
			}
			total_pts += ret;
		}
		if(d<0)
			d = d+(4*pt.x)+6;
		else {
			d = d+(4*(pt.x-pt.y))+10;
			pt.y--;
		}
		pt.x++;
	}
	if(pt.x==pt.y) {
		ret = plotpts(center, pt, R, pp, 8);
		if(total_pts+ret>N) return(-1);
		if(ret) {
			for(i = total_pts; i<total_pts+ret; i++) {
				pP[i].x = pp[i-total_pts].x;
				pP[i].y = pp[i-total_pts].y;
			}
			total_pts += ret;
		}
	}	
	return(total_pts);
}

// Antialiased line
// Array pFactor determines share of circle's color, being mixed with the background color.

#define ipart_(X) ((int)(X))
#define round_(X) ((int)(((double)(X))+0.5))
#define fpart_(X) (((double)(X))-(double)ipart_(X))
#define rfpart_(X) (1.0-fpart_(X))
//#define swap_(a, b) do{ __typeof__(a) tmp;  tmp = a; a = b; b = tmp; } while(0)
#define swap_(a, b, tmp) do{ tmp = a; a = b; b = tmp; } while(0)

int Y_Utils::XiaolinWuLine(Point2<int> start, Point2<int> end, Point2<int> *pP, double* pFactor, int N)
{
	int n = 0, tmp;
	double dx = (double)end.x - (double)start.x;
	double dy = (double)end.y - (double)start.y;
	if ( fabs(dx) > fabs(dy) ) {
		if ( end.x < start.x ) {
			swap_(start.x, end.x, tmp);
			swap_(start.y, end.y, tmp);
		}
		double gradient = dy / dx;
		double xend = round_(start.x);
		double yend = start.y + gradient*(xend - start.x);
		double xgap = rfpart_(start.x + 0.5);
		int xpxl1 = xend;
		int ypxl1 = ipart_(yend);
		//plot_(xpxl1, ypxl1, rfpart_(yend)*xgap);
		//plot_(xpxl1, ypxl1+1, fpart_(yend)*xgap);
		pP[n].Set(xpxl1, ypxl1); pFactor[n] = rfpart_(yend)*xgap; n++;
		pP[n].Set(xpxl1, ypxl1+1); pFactor[n] = fpart_(yend)*xgap; n++;
		double intery = yend + gradient;
 
		xend = round_(end.x);
		yend = end.y + gradient*(xend - end.x);
		xgap = fpart_(end.x+0.5);
		int xpxl2 = xend;
		int ypxl2 = ipart_(yend);
		//plot_(xpxl2, ypxl2, rfpart_(yend) * xgap);
		//plot_(xpxl2, ypxl2 + 1, fpart_(yend) * xgap);
		pP[n].Set(xpxl2, ypxl2); pFactor[n] = rfpart_(yend)*xgap; n++;
		pP[n].Set(xpxl2, ypxl2+1); pFactor[n] = fpart_(yend)*xgap; n++;
 
		for(int x = xpxl1+1; x<=(xpxl2-1); x++) {
			//plot_(x, ipart_(intery), rfpart_(intery));
			//plot_(x, ipart_(intery) + 1, fpart_(intery));
			pP[n].Set(x, ipart_(intery)); pFactor[n] = rfpart_(intery); n++;
			pP[n].Set(x, ipart_(intery)+1); pFactor[n] = fpart_(intery); n++;
			intery += gradient;
		}
	} else {
		if ( end.y < start.y ) {
			swap_(start.x, end.x, tmp);
			swap_(start.y, end.y, tmp);
		}
		double gradient = dx / dy;
		double yend = round_(start.y);
		double xend = start.x + gradient*(yend - start.y);
		double ygap = rfpart_(start.y + 0.5);
		int ypxl1 = yend;
		int xpxl1 = ipart_(xend);
		//plot_(xpxl1, ypxl1, rfpart_(xend)*ygap);
		//plot_(xpxl1, ypxl1+1, fpart_(xend)*ygap);
		pP[n].Set(xpxl1, ypxl1); pFactor[n] = rfpart_(xend)*ygap; n++;
		pP[n].Set(xpxl1, ypxl1+1); pFactor[n] = fpart_(xend)*ygap; n++;
		double interx = xend + gradient;
 
		yend = round_(end.y);
		xend = end.x + gradient*(yend - end.y);
		ygap = fpart_(end.y+0.5);
		int ypxl2 = yend;
		int xpxl2 = ipart_(xend);
		//plot_(xpxl2, ypxl2, rfpart_(xend) * ygap);
		//plot_(xpxl2, ypxl2 + 1, fpart_(xend) * ygap);
		pP[n].Set(xpxl2, ypxl2); pFactor[n] = rfpart_(xend)*ygap; n++;
		pP[n].Set(xpxl2, ypxl2+1); pFactor[n] = fpart_(xend)*ygap; n++;
 
		for(int y = ypxl1+1; y<=(ypxl2-1); y++) {
			//plot_(ipart_(interx), y, rfpart_(interx));
			//plot_(ipart_(interx) + 1, y, fpart_(interx));
			pP[n].Set(ipart_(interx), y); pFactor[n] = rfpart_(interx); n++;
			pP[n].Set(ipart_(interx), y+1); pFactor[n] = fpart_(interx); n++;
			interx += gradient;
		}
	}
	return(n);
}

// Antialiased circle. pFactor determines share of circle's color, being mixed with the background color.
int Y_Utils::XiaolinWuCircle(Point2<int> center, int radius, Y_Rect<int> R, Point2<int> *pP, double* pFactor, int N)
{
	int n = 0;
	double yt, xt, distance;
	for (int xs = -radius; xs<=radius; xs++)
	{
		yt = (double)(radius * sqrt(1.0-double(xs*xs)/double(radius*radius)));
		distance = fpart_(yt);
		pP[n].Set(center.x+xs, center.y+int(yt)); pFactor[n] = distance; n++;
		pP[n].Set(center.x+xs, center.y+int(yt)+1); pFactor[n] = 1.0-distance; n++;
		pP[n].Set(center.x-xs, center.y-int(yt)); pFactor[n] = distance; n++;
		pP[n].Set(center.x-xs, center.y-int(yt)-1); pFactor[n] = 1.0-distance; n++;
	}
	for (int ys = -radius; ys<=radius; ys++)
	{
		xt = (double)(radius * sqrt(1.0-double(ys*ys)/double(radius*radius)));
		distance = fpart_(xt);
		pP[n].Set(center.x+int(xt), center.y+ys); pFactor[n] = distance; n++;
		pP[n].Set(center.x+int(xt)+1, center.y+ys); pFactor[n] = 1.0-distance; n++;
		pP[n].Set(center.x-int(xt), center.y-ys); pFactor[n] = distance; n++;
		pP[n].Set(center.x-int(xt)-1, center.y-ys-1); pFactor[n] = 1.0-distance; n++;
	}
	return(n);
}
#undef swap_
#undef plot_
#undef ipart_
#undef fpart_
#undef round_
#undef rfpart_

int Y_Utils::plotpts(Point2<int> center, Point2<int> pt, Y_Rect<int> R, Point2<int> *p, int N)
{
	int	xcpx,xcpy,xcmx,xcmy;
	int	ycpx,ycpy,ycmx,ycmy;
	int found = 0;

	xcpx = center.x + pt.x;
	xcmx = center.x - pt.x;
	xcpy = center.x + pt.y;
	xcmy = center.x - pt.y;
	ycpy = center.y + pt.y;
	ycmy = center.y - pt.y;
	ycpx = center.y + pt.x;
	ycmx = center.y - pt.x;

/* this bit does not allow for wraparound */
	if((xcpx >= R.x) && (ycpy >= R.y) && (xcpx < R.w) && (ycpy < R.h)) {
		p[found].x = xcpx;
		p[found].y = ycpy;
		++found;
	}
	if((xcmx >= R.x) && (ycpy >= R.y) && (xcmx < R.w) && (ycpy < R.h)) {
		p[found].x = xcmx;
		p[found].y = ycpy;
		++found;
	}
	if((xcpx >= R.x) && (ycmy >= R.y) && (xcpx < R.w) && (ycmy < R.h)) {
		p[found].x = xcpx;
		p[found].y = ycmy;
		++found;
	}
	if((xcmx >= R.x) && (ycmy >= R.y) && (xcmx < R.w) && (ycmy < R.h)) {
		p[found].x = xcmx;
		p[found].y = ycmy;
		++found;
	}
	if((xcpy >= R.y) && (ycpx >= R.x) && (xcpy < R.w) && (ycpx < R.h)) {
		p[found].x = xcpy;
		p[found].y = ycpx;
		++found;
	}
	if((xcmy >= R.y) && (ycpx >= R.x) && (xcmy < R.w) && (ycpx < R.h)) {
		p[found].x = xcmy;
		p[found].y = ycpx;
		++found;
	}
	if((xcpy >= R.y) && (ycmx >= R.x) && (xcpy < R.w) && (ycmx < R.h)) {
		p[found].x = xcpy;
		p[found].y = ycmx;
		++found;
	}
	if((xcmy >= R.y) && (ycmx >= R.x) && (xcmy < R.w) && (ycmx < R.h)) {
		p[found].x = xcmy;
		p[found].y = ycmx;
		++found;
	}
	return(found);
}

int Y_Utils::GetReadings(const char *message, double& pitch, double& roll, double& yaw)
{
	if(!message) {
		pitch = 0.0; roll = 0.0; yaw = 0.0;
		return(-1);
	}
	char *p = strchr((char*)message, 'C');
	if(!p) {
		fprintf(stdout, "Wrong message - does not have 'C'.\n");
		return(-1);
	}
	sscanf_s(p+1, "%lf", &yaw);
	p = strchr((char*)message, 'P');
	if(!p) {
		fprintf(stdout, "Wrong message - does not have 'P'.\n");
		return(-1);
	}
	sscanf_s(p+1, "%lf", &pitch);
	p = strchr((char*)message, 'R');
	if(!p) {
		fprintf(stdout, "Wrong message - does not have 'R'.\n");
		return(-1);
	}
	sscanf_s(p+1, "%lf", &roll);
	return(0);
}
	
int Y_Utils::GetPosition(const char *gps, double& x, double& y, double& z)
{
	char *tmp[100], *next_token;
	int tmp_i;
	char *ms = _strdup(gps);
	memset(tmp, 0, 100);
	tmp_i = 0;
	tmp[tmp_i] = (char *) strtok_s(ms, ",", &next_token);
	while(tmp[tmp_i] = (char *) strtok_s(nullptr, ",", &next_token)) ++tmp_i;
	sscanf_s(tmp[1], "%lf", &x);
	sscanf_s(tmp[3], "%lf", &y);
	sscanf_s(tmp[5], "%lf", &z);
	return(0);
}

long Y_Utils::TimecodeToFrame(const char *tc)
{
	int h, m, s, f;
	sscanf_s(tc, "%x:%x:%x:%x", &h, &m, &s, &f);
	long fr = BCDTOINT(f)+30*(BCDTOINT(s)+60*(BCDTOINT(m)+60*BCDTOINT(h)));
	return(fr);
}

char *Y_Utils::FrameToTimecode(long fr)
{
	int h, m, s, f;
	f = fr%30; fr = (fr-f)/30;
	s = fr%60; fr = (fr-s)/60;
	m = fr%60; h = (fr-m)/60;
	static char tc[16];
	sprintf_s(tc, (size_t) 16, "%01d:%02d:%02d:%02d", h, m, s, f);
	return(tc);
}

// Taking into account 2 frames dropped every 10 minutes 
// (see ~/VID-MOS/TimecodeToFrame.txt)
long Y_Utils::TimecodeToFrameDropped(const char *tc)
{
	int h, m, s, f;
	sscanf_s(tc, "%x:%x:%x:%x", &h, &m, &s, &f);
	h = BCDTOINT(h); m = BCDTOINT(m); s = BCDTOINT(s); f = BCDTOINT(f);
	//long fr = ((h*60+m)*60+s)*30+f-((h*60+m)-int((h*60+m)/10))*2;
	int TensOfMinutes = h*6+m/10;
	int RestOfMinutes = h*60+m-10*TensOfMinutes;
	long fr = ((h*60+m)*60+s)*30+f;
	fr -= TensOfMinutes*18;
	if(RestOfMinutes)
		fr -= (RestOfMinutes-1)*2;
	return(fr);
}

char *Y_Utils::FrameToTimecodeDropped(long fr)
{
	int h, m, s, f;
	int TensOfMinutes = int(fr/17982);
	int rest = fr-TensOfMinutes*17982;
	
	if(rest<1800) {
		m = 10*TensOfMinutes;
	} else {
		m = 10*TensOfMinutes+1+int(rest-1800)/1798;
		rest -= 1800+(m-1)*1798;
	}
	s = int(rest/30);
	rest -= s*30;
	f = rest;
	h = int(m/60);
	m -= h*60;

	static char tc[16];
	sprintf_s(tc, (size_t) 16, "%01d:%02d:%02d:%02d", h, m, s, f);
	return(tc);
}

void Y_Utils::PrintResponse(int i, int& printout, char s)
{
	if(s)
		fprintf(stderr, "%c%5d ", s, i);
	else
		fprintf(stderr, "%5d ", i); 
	printout++; 
	fflush(stdout);
	if(printout%10==0) fprintf(stderr, "\n");
}

void Y_Utils::ToLower(unsigned char* stg)
{
	for(int i = 0; i<(int)strlen((const char*)stg); i++) {
		if(stg[i]>='A' && stg[i]<='Z')
			stg[i] = tolower(stg[i]);
	}
}

void Y_Utils::ToUpper(unsigned char* stg)
{
	for(int i = 0; i<(int)strlen((const char*)stg); i++) {
		if(stg[i]>='a' && stg[i]<='z')
			stg[i] = toupper(stg[i]);
	}
}

unsigned char Y_Utils::Dec2HexDigit(unsigned int d)
{
	if(d<10)
		return('0'+d);
	return('a'+(int)d-10);
}

unsigned int Y_Utils::HexDigit2Dec(unsigned char h)
{
	if(!(h>='0' && h<='9') && !(h>='a' && h<='z')) // outside two possible ranges
		return(0);
	if(h<='9')
		return(h-'0');
	return(10+h-'a');
}

int Y_Utils::bin2dec(const unsigned char *b)
{
	int mult = 1, d = 0;
	for(int i = (int)strlen((const char*)b) - 1; i >= 0; i--) {
		d += (b[i] == '1') ? mult : 0;
		mult *= 2;
	}
	return(d);
}

// Returned string has to be freed
unsigned char *Y_Utils::hex2bin(const unsigned char *h, unsigned char *storage)
{
	int l = (int)strlen((const char*)h), d, div;
	unsigned char *b;
	
	if(storage) 
		b = storage;
	else
		b = new unsigned char[4*l+1];
	
	b[4 * l] = 0;
	
	for(int i = 0; i < l; i++) {
		d = HexDigit2Dec(h[i]);
		div = 1;
		for(int j = i*4+3; j >= i*4; j--) {
			b[j] = ((d/div)%2) ? '1' : '0';
			div *= 2;
		}
	}
	return(b);
}

unsigned int Y_Utils::hex2dec(const unsigned char *h)
{
	unsigned int dec = 0, d;
	int l = (int)strlen((const char*)h);
	for(int i = l-1, j = 1; i>=0; i--, j*=16) {
		d = HexDigit2Dec(h[i]);
		dec += d*j;
	}
	return(dec);
}

// Returned string has to be freed
unsigned char *Y_Utils::dec2hex(unsigned int d, unsigned char *storage)
{
	int l = 1, mult = 16;
	
	while(d/mult) {
		l++;
		mult *= 16;
	}
	unsigned char *h;
	
	if(storage) 
		h = storage;
	else
		h = new unsigned char[l+1];
	h[l] = 0;
	int div = 1;
	for(int i = l-1; i >= 0; i--) {
		h[i] = Dec2HexDigit((d/div)%16);
		div *= 16;
	}
	return(h);
}

// Returned string has to be freed
unsigned char *Y_Utils::dec2bin(unsigned int d, unsigned char *storage)
{
	int i;
	unsigned char *b;
	
	if(storage) 
		b = storage;
	else
		b = new unsigned char[9];
	
	b[8] = '\0';
	for(i = 0; i<8; i++) {
		b[7-i] = ((((1<<i)&d)>>i)==1) ? '1' : '0';
	}	  
	return(b);
}

// Inverse transform is NOT scaled!
// To scale, divide output of inverse transform by 'nn'.
#define SWAP(a,b) tempr=(a); (a) = (b); (b) = tempr
void Y_Utils::four1(double data[], int nn, int isign)
{
	int n,mmax,m,j,istep,i;
	double wtemp,wr,wpr,wpi,wi,theta;
	double tempr,tempi;

	n = nn << 1;
	j = 1;
	for(i=1; i<n; i+=2) {
		if(j > i) {
			SWAP(data[j],data[i]);
			SWAP(data[j+1],data[i+1]);
		}
		m = n >> 1;
		while(m >= 2 && j > m) {
			j -= m;
			m >>= 1;
		}
		j += m;
	}
	mmax = 2;
	while(n > mmax) {
		istep = 2 * mmax;
		theta = 6.28318530717959 / ((double)isign * mmax);
		wtemp = sin(0.5 * theta);
		wpr = (-2.0) * wtemp * wtemp;
		wpi = sin(theta);
		wr = 1.0;
		wi = 0.0;
		for(m=1; m<mmax; m+=2) {
			for(i=m; i<=n; i+=istep) {
				j = i + mmax;
				tempr = wr * data[j] - wi * data[j+1];
				tempi = wr * data[j + 1] + wi * data[j];
				data[j] = data[i] - tempr;
				data[j+1] = data[i+1] - tempi;
				data[i] += tempr;
				data[i+1] += tempi;
			}
			wr = (wtemp = wr) * wpr - wi * wpi + wr;
			wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
	}
}
#undef SWAP

// 2D fast Fourier transform
// -1 for forward FFT, +1 for backward FFT
void Y_Utils::fourn(double data[], int nn[], int ndim, int isign)
{
	int i1,i2,i3,i2rev,i3rev,ibit,idim;
	int ip1,ip2,ip3,ifp1,ifp2,k1,k2,n;
	int ii1,ii2,ii3;
	int nprev,nrem,ntot;
	double tempi,tempr;
	double theta,wi,wpi,wpr,wr,wtemp;

	ntot = 1;
	for (idim=1; idim<=ndim; idim++)
		ntot = ntot*nn[idim];
	nprev = 1;
	for (idim=1; idim<=ndim; idim++) {
		n = nn[idim];
		nrem = ntot/(n*nprev);
		ip1 = 2*nprev;
		ip2 = ip1*n;
		ip3 = ip2*nrem;
		i2rev = 1;
		for (ii2=0; ii2<=((ip2-1)/ip1); ii2++) {
			i2 = 1+ii2*ip1;
			if (i2 < i2rev) {
				for (ii1=0; ii1<=((ip1-2)/2); ii1++) {
					i1 = i2+ii1*2;
					for (ii3=0; ii3<=((ip3-i1)/ip2); ii3++) {
						i3 = i1+ii3*ip2;
						i3rev = i2rev+i3-i2;
						tempr = data[i3];
						tempi = data[i3+1];
						data[i3] = data[i3rev];
						data[i3+1] = data[i3rev+1];
						data[i3rev] = tempr;
						data[i3rev+1] = tempi;
					}
				}
			}
			ibit = ip2/2;
			while ((ibit >= ip1) && (i2rev > ibit)) {
				i2rev = i2rev-ibit;
				ibit = ibit/2;
			}
			i2rev = i2rev+ibit;
		}
		ifp1 = ip1;
		while (ifp1 < ip2) {
			ifp2 = 2*ifp1;
			theta = isign*2.0*M_PI/(ifp2/ip1);
			wpr = -2.0*(sin(0.5*theta)*sin(0.5*theta));
			wpi = sin(theta);
			wr = 1.0;
			wi = 0.0;
			for (ii3=0; ii3<=((ifp1-1)/ip1); ii3++) {
				i3 = 1+ii3*ip1;
				for (ii1=0; ii1<=((ip1-2)/2); ii1++) {
					i1 = i3+ii1*2;
					for (ii2=0; ii2<=((ip3-i1)/ifp2); ii2++) {
						i2 = i1+ii2*ifp2;
						k1 = i2;
						k2 = k1+ifp1;
						tempr = wr*data[k2] - wi*data[k2+1];
						tempi = wr*data[k2+1] + wi*data[k2];
						data[k2] = data[k1]-tempr;
						data[k2+1] = data[k1+1]-tempi;
						data[k1] = data[k1]+tempr;
						data[k1+1] = data[k1+1]+tempi;
					}
				}
				wtemp = wr;
				wr = wr*wpr-wi*wpi+wr;
				wi = wi*wpr+wtemp*wpi+wi;
			}
			ifp1 = ifp2;
		}
		nprev = n*nprev;
	}
}

double Y_Utils::GammaFunction(double x)
{
    int i, k, m;
    double ga, gr, r = 1.0, z;

    static double g[] = {
        1.0,
        0.5772156649015329,
       -0.6558780715202538,
       -0.420026350340952e-1,
        0.1665386113822915,
       -0.421977345555443e-1,
       -0.9621971527877e-2,
        0.7218943246663e-2,
       -0.11651675918591e-2,
       -0.2152416741149e-3,
        0.1280502823882e-3,
       -0.201348547807e-4,
       -0.12504934821e-5,
        0.1133027232e-5,
       -0.2056338417e-6,
        0.6116095e-8,
        0.50020075e-8,
       -0.11812746e-8,
        0.1043427e-9,
        0.77823e-11,
       -0.36968e-11,
        0.51e-12,
       -0.206e-13,
       -0.54e-14,
        0.14e-14};

    if(x > 171.0) return 1e308;    // This value is an overflow flag.
    if(EPSILON_EQ(x,(int)x)) {
        if(x > 0.0) {
            ga = 1.0;               // use factorial
            for(i = 2; i<x; i++) {
               ga *= i;
            }
         } else
            ga = 1e308;
     } else {
        if(fabs(x)>1.0) {
            z = fabs(x);
            m = (int)z;
            r = 1.0;
            for(k = 1; k<=m; k++) {
                r *= (z-k);
            }
            z -= m;
        } else
            z = x;
        gr = g[24];
        for(k = 23; k>=0; k--) {
            gr = gr*z+g[k];
        }
        ga = 1.0/(gr*z);
        if(fabs(x)>1.0) {
            ga *= r;
            if(x<0.0) {
                ga = -M_PI/(x*ga*sin(M_PI*x));
            }
        }
    }
    return ga;
}

void Y_Utils::Seed(unsigned int seed_value)
{
	if(seed_value==0)
		seed_value = (unsigned int) time(nullptr);
	srand(seed_value);
}

double Y_Utils::d_rand(double r_min, double r_max)
{
#if defined(WIN32) || defined(WIN64)
	double r = (double) rand(), s;
	s = r_min + (r_max - r_min) * r / (double) RAND_MAX;
#else
	double r = (double) random(), s;
	s = r_min + (r_max - r_min) * r / (double) LONG_MAX;
#endif
	return(s);
}

int Y_Utils::i_rand(int i_min, int i_max)
{
	double r = d_rand((double) i_min, (double) (i_max + 1));
	return((int) r);
}

double Y_Utils::gauss(double mean, double stdev)
{
	double	fac, r, v1, v2;
	static double gasdev1, gasdev2;
	static int flag = 0;

	if(flag==0) {
		do {
			v1 = d_rand(-1.0, 1.0);
			v2 = d_rand(-1.0, 1.0);
			r = (v1 * v1) + (v2 * v2);
		} while(r>=1.0);
		fac = sqrt(-2.0*log(r)/r);
		gasdev1 = v1*fac;
		gasdev2 = v2*fac;
		flag = 1;
		return(mean+stdev*gasdev1);
	} else {
		flag = 0;
		return(mean+stdev*gasdev2);
	}
}

bool Y_Utils::randomSet(int setLen, int totalLen, int* pSet)
{
	if(totalLen<setLen)
		return(false);
	int k = 0;
	double ratio = double(totalLen-1)/RAND_MAX;
	while(k<setLen) {
		// Pick random number from 0 to totalLen-1
		int r = (int)(ratio*rand());
		// Check that it hasn't been picked yet
		bool picked = false;
		for(int i = 0; i<k; i++) {
			if(pSet[i]==r) 
				picked = true;
		}
		if(!picked) {
			pSet[k] = r;
			k++;
		}
	}
	return(true);
}

#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp
#define M 7
#define NSTACK 50
void Y_Utils::sort(long n, int *arr)
{
	long i, ir = n, j, k, l = 1;
	int jstack=0, *istack;
	int a, temp;

	istack = (int *) malloc(NSTACK * sizeof(int));
	for (;;) {
		if (ir-l < M) {
			for (j=l+1;j<=ir;j++) {
				a=arr[j];
				for (i=j-1;i>=1;i--) {
					if (arr[i] <= a) break;
					arr[i+1]=arr[i];
				}
				arr[i+1]=a;
			}
			if (jstack == 0) break;
			ir=istack[jstack--];
			l=istack[jstack--];
		} else {
			k=(l+ir) >> 1;
			SWAP(arr[k],arr[l+1]);
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir]);
			}
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir]);
			}
			if (arr[l+1] > arr[l]) {
				SWAP(arr[l+1],arr[l]);
			}
			i=l+1;
			j=ir;
			a=arr[l];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j]);
			}
			arr[l]=arr[j];
			arr[j]=a;
			jstack += 2;
			if (jstack > NSTACK) {
				fprintf(stderr, "NSTACK is too small in sort.\n");
				exit(1);
			}
			if (ir-i+1 >= j-l) {
				istack[jstack]=ir;
				istack[jstack-1]=i;
				ir=j-1;
			} else {
				istack[jstack]=j-1;
				istack[jstack-1]=l;
				l=i;
			}
		}
	}
	free(istack);
}
void Y_Utils::sort_double(long n, double *arr)
{
	long i, ir = n, j, k, l = 1;
	int jstack=0, *istack;
	double a, temp;

	istack = (int *) malloc(NSTACK * sizeof(int));
	for (;;) {
		if (ir-l < M) {
			for (j=l+1;j<=ir;j++) {
				a=arr[j];
				for (i=j-1;i>=1;i--) {
					if (arr[i] <= a) break;
					arr[i+1]=arr[i];
				}
				arr[i+1]=a;
			}
			if (jstack == 0) break;
			ir=istack[jstack--];
			l=istack[jstack--];
		} else {
			k=(l+ir) >> 1;
			SWAP(arr[k],arr[l+1]);
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir]);
			}
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir]);
			}
			if (arr[l+1] > arr[l]) {
				SWAP(arr[l+1],arr[l]);
			}
			i=l+1;
			j=ir;
			a=arr[l];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j]);
			}
			arr[l]=arr[j];
			arr[j]=a;
			jstack += 2;
			if (jstack > NSTACK) {
				fprintf(stderr, "NSTACK is too small in sort.\n");
				exit(1);
			}
			if (ir-i+1 >= j-l) {
				istack[jstack]=ir;
				istack[jstack-1]=i;
				ir=j-1;
			} else {
				istack[jstack]=j-1;
				istack[jstack-1]=l;
				l=i;
			}
		}
	}
	free(istack);
}
#undef M
#undef NSTACK
#undef SWAP

// Substitute all appearances of 'from' in 'stg' to 'with'.
// Return number of appearances.
int Y_Utils::SubstituteChars(char* stg, char from, char with)
{
	int len = (int)strlen(stg), n = 0;
	for(int i = 0; i<len; i++) {
		if(stg[i]==from) {
			stg[i] = with;
			n++;
		}
	}
	return(n);
}

// Extract frame size from the logfile header produced by movie
Y_Rect<int> *Y_Utils::GetFrameSize(const char *filename)
{
	FILE *fp;
	if(fopen_s(&fp, filename, "r"))
		return(nullptr);
	char stg[INFO_SIZE], *p;
	int w, h;
	
	do {
		if(fgets(stg, INFO_SIZE-1, fp)==nullptr) {
			fprintf(stderr, "Could not find frame size.\n");
			fclose(fp);
			return(nullptr);
		}
	} while(strstr(stg, "Frame size")==nullptr);
	fclose(fp);
	if(p = strchr(stg, '(')) {
		sscanf_s(p+1, "%d*%d", &w, &h);
	} else {
		w = h = 0;
	}
		
	//sscanf_s(stg, "%*c %*s %*s=%*d %*s %*s: (%d*%d)", &w, &h);
	//sscanf_s(stg, "%*d %d %d", &w, &h);
	Y_Rect<int> *r = new Y_Rect<int>(0, 0, w, h);
	return(r);
}

// Extract frame size from the logfile header produced by movie
Y_Rect<int> *Y_Utils::GetFrameSizeFromInfo(const char *pInfo)
{
	char *p;
	
	if((p = strstr((char*)pInfo, "Frame size"))==nullptr) {
		fprintf(stderr, "Could not find frame size.\n");
		return(nullptr);
	}
	if(p = strchr(p, '(')) {
		int w, h;
		sscanf_s(p+1, "%d*%d", &w, &h);
		Y_Rect<int> *r = new Y_Rect<int>(0, 0, w, h);
		return(r);
	}
	fprintf(nullptr, "Could not find frame size.\n");
	return(nullptr);
}

// Check if the file has information in it
int Y_Utils::HasInfo(const char *filename)
{
	char *pInfo = GetInfo(filename);
	Y_Rect<int> *pFrame = GetFrameSize(filename);
	int k = (pInfo && pFrame) ? 1 : 0;
	delete [] pInfo;
	delete pFrame;
	return(k);
}

char *Y_Utils::GetInfo(const char *filename)
{
	FILE *fp;
	if(fopen_s(&fp, filename, "r"))
		return(nullptr);
	char stg[INFO_SIZE];
	// Count number of chars in lines starting with #
	int n_chars = 0;
	while(fgets(stg, INFO_SIZE-1, fp)!=nullptr) {
		if(stg[0]!='#') break;
		n_chars += (int)strlen(stg)+1;
	}
	if(n_chars>(INFO_SIZE-1)) {
		fprintf(stderr, "Info longer than 1K bytes.\n");
		return(nullptr);
	}
	//char *info = new char[n_chars+1];
	// Get 1K bytes in any way
	char *pInfo = new char[INFO_SIZE];
	rewind(fp);
	char *p = pInfo;
	while(fgets(stg, INFO_SIZE-1, fp)!=nullptr) {
		if(stg[0]!='#') break;
		memcpy(p, stg, strlen(stg));
		p += strlen(stg);
	}
	*p = '\0';
	fclose(fp);
	//fprintf(stderr, "Allocated %d chars\n", n_chars+1);
	//fprintf(stderr, "###\n%s###\n", info);
	return(pInfo);
}

// Extract comment from the logfile header produced by movie.
char *Y_Utils::GetComment(const char *filename)
{
	FILE *fp;
	if(fopen_s(&fp, filename, "r"))
		return(nullptr);
	static char stg[INFO_SIZE];
	
	do {
		if(fgets(stg, INFO_SIZE-1, fp)==nullptr) {
			fprintf(stderr, "Could not find comment.\n");
			fclose(fp);
			return(nullptr);
		}
	} while(strstr(stg, "Comment")==nullptr);
	fclose(fp);
	return(stg);
}

// Extract movie name from Info, allocate storage.
char* Y_Utils::GetMovieName(const char *pInfo)
{
	char *p1, *p2;
	if((p1 = strchr((char*)pInfo, '\"')) && (p2 = strchr(p1+1, '\"'))) {
		int len = (int)(p2-p1);
		char *name = new char[len+1];
		strncpy_s(name, len, p1+1, len);
		name[len-1] = 0;
		return(name);
	}
	return(nullptr);
}

double Y_Utils::GetFocalLength(const char *pInfo)
{
	char *p;
	double focal_length;
	if(p = strstr((char*)pInfo, "FL=")) {
		sscanf_s(p+3, "%lf", &focal_length);
		return(focal_length);
	}
	return(-1.0);
}

int Y_Utils::GetTrackLength(const char *pInfo)
{
	char *p;
	int track_length;
	if(p = strstr((char*)pInfo, "Track length=")) {
		sscanf_s(p+13, "%d", &track_length);
		return(track_length);
	}
	return(0);
}

double Y_Utils::GetFoV(const char *pInfo)
{
	char *p;
	double FoV;
	if(p = strstr((char*)pInfo, "FoV=")) {
		char *p2 = strchr(p, '>');
		if(p2)
			*p2 = 0;
		sscanf_s(p+4, "%lf", &FoV);
		return(FoV);
	}
	return(-1.0);
}

void Y_Utils::GetFrameSize(const char* stg, int& w, int& h)
{
	w = ExtractIntValue(stg, "W=");
	h = ExtractIntValue(stg, "H=");
	if(w==0 || h==0) // some info is absent
		w = h = 0;
}

int Y_Utils::ExtractIntValue(const char *stg, const char* tag)
{
	char *p;
	int value;
	if(p = strstr((char*)stg, tag)) {
		sscanf_s(p+strlen(tag), "%d", &value);
		return(value);
	}
	return(0);
}

double Y_Utils::ExtractDoubleValue(const char *stg, const char* tag)
{
	char *p;
	double value;
	if(p = strstr((char*)stg, tag)) {
		sscanf_s(p+strlen(tag), "%lf", &value);
		return(value);
	}
	return(0.0);
}

// Returns -1, if total length exceeds INFO_SIZE, otherwise 0.
int Y_Utils::AddCommentToInfo(char* pInfo, const char* comment)
{
	int l_info = (int) strlen(pInfo);
	int l_comment = (int) strlen(comment);
	if((l_info+l_comment) > (INFO_SIZE-1)) return(-1);

	char* p = strchr(pInfo, '\0');
	memcpy(p, comment, l_comment);
	*(p+l_comment) = '\0';
	return(0);
}

int Y_Utils::AddProducedLineToInfo(char* pInfo, const char* progname)
{
	char comment[512], *p, *local_progname = new char[strlen(progname)+1];
	memcpy(local_progname, progname, strlen(progname)+1);
	// Keep only program name, no path
	p = strrchr(local_progname, '\\');
	if(p) p = p+1;
	else {
		p = strrchr(local_progname, '/');
		if(p) p = p+1;
		else p = local_progname;
	}

	//sprintf_s(comment, (size_t) 64, "#  Produced by %s, %s\n", p, TimeMeasure::Timestamp());
	sprintf_s(comment, (size_t) 64, "#  Produced by %s\n", p);
	delete [] local_progname;
	return(AddCommentToInfo(pInfo, comment));
}

void Y_Utils::WriteInfo(char *pInfo, const char* movieName, int trackLength, int width, int height,
			double focalLength, double FoV)
{
	char line0[256], line1[256], line2[256], line3[256], line4[256];

	sprintf_s(line0, (size_t) 256, "#Info \"%s\":\n", movieName);
	sprintf_s(line1, (size_t) 256, "# \t Microsoft AVI file format.\n");
	sprintf_s(line2, (size_t) 256, "# \t Track length=%d   Frame size: (%d*%d)\n", 
			trackLength, width, height);
	sprintf_s(line3, (size_t) 256, "# \t Type: vids   Compression: DIB\n");
	sprintf_s(line4, (size_t) 256, "# \t Comment: <FL=%.2lf FoV=%.2lf>\n", focalLength, FoV);
	sprintf_s(pInfo, (size_t) strlen(pInfo), "%s%s%s%s%s", line0, line1, line2, line3, line4);
}

int Y_Utils::CountTokens(char* str, char start_char, char stop_char)
{
	int count = 0;
	char stg[2048];
	strcpy_s(stg, 2048, str);
	char* p = strchr(stg, stop_char);
	if(p) *p = '\0';
	p = strchr(stg, start_char);
	if(!p)
		return(0);
	char *tmp[100], *next_token;
	tmp[count] = (char *) strtok_s(p+1, " \t", &next_token);
	++count;
	while(tmp[count] = (char *) strtok_s(nullptr, " \t", &next_token)) ++count;

	return(count);
}

u_short Y_Utils::GetConsoleTextAttribute(HANDLE hConsole)
{
	if(!hConsole)
		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	if(!hConsole) return(0);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	consoleTextAttribute = csbi.wAttributes;
	return(consoleTextAttribute);
}

void Y_Utils::ConsoleWhite()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if(!hStdout) return;
	SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
}

void Y_Utils::ConsoleRed()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if(!hStdout) return;
	SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
}

void Y_Utils::ConsoleGreen()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if(!hStdout) return;
	SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
}

void Y_Utils::ConsoleBlue()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if(!hStdout) return;
	SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
}

void Y_Utils::ResetConsole()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if(!hStdout) return;
	SetConsoleTextAttribute(hStdout, consoleTextAttribute);
}

char Y_VMRecord::buf[1024];

Y_VMRecord::Y_VMRecord()
{
	has_record = 0;
	file = '-'; prev_file = '-'; comment = '-';
	frame_n = -1; base_frame_n = -1;
	x_rot = y_rot = 0.0;
	zoom = put_zoom = 1.0;
	angle = put_angle = 0.0; // Degrees
	zoom_rot_robust = transl_robust = x_shift = y_shift = x_put = y_put = 0.0;
	error = -1.0;
	overlap_percent = -1.0;
	centre.Set(0.0, 0.0);
	force_record = nullptr;
	memset(PeakParams, 0, 4*sizeof(double));
}

void Y_VMRecord::Set(double z, double a, double x, double y)
{
	zoom = z;
	angle = a; // Degrees
	x_shift = x;
	y_shift = y;
}

void Y_VMRecord::SetRobust(double x, double y, double rz, double t, double o)
{
	x_rot = x;
	y_rot = y;
	zoom_rot_robust = rz;
	transl_robust = t;
	overlap_percent = o;
}

void Y_VMRecord::SetPut(double z, double a, double x, double y)
{
	put_zoom = z;
	put_angle = a; // Degrees
	x_put = x; // Duplicating 'centre'!?
	y_put = y;
	centre.Set(x, y);
}

void Y_VMRecord::Copy(const Y_VMRecord r)
{
	has_record = r.has_record;
	w = r.w; h = r.h;
	file = r.file; prev_file = r.prev_file; comment = r.comment; 
	frame_n = r.frame_n; base_frame_n = r.base_frame_n;
	zoom = r.zoom; angle = r.angle;
	x_shift = r.x_shift; y_shift = r.y_shift;
	put_zoom = r.put_zoom; put_angle = r.put_angle;
	overlap_percent = r.overlap_percent; error = r.error;
	//centre.Copy(r.centre);
	memcpy(PeakParams, r.PeakParams, 4*sizeof(double));
}

int Y_VMRecord::ReadStg(char *stg)
{
	double x, y;
	
	if(strstr(stg, "<-")) { // Explicit specification of both frames
							// Can be used for cross-reg's
		sscanf_s(stg, 
		"  %d<-%d	%lf %lf (%lf)  %lf %lf (%lf) (%lf,%lf) [%lf,%lf] %s "
		"(%lf %lf %lf %lf)", 
		&base_frame_n, &frame_n, &x_shift, &y_shift, &transl_robust,
		&zoom, &angle, &zoom_rot_robust,
		&overlap_percent, &error,
		&x, &y, comment.c_str(), 64,
		&PeakParams[0], &PeakParams[1], &PeakParams[2], &PeakParams[3]);
	} else { // Old specification. 'base_frame_n is assumed to be 'frame_n-1'
		sscanf_s(stg, 
		"  %d	%lf %lf (%lf)  %lf %lf (%lf) (%lf,%lf) [%lf,%lf] %s "
		"(%lf %lf %lf %lf)", 
		&frame_n, &x_shift, &y_shift, &transl_robust,
		&zoom, &angle, &zoom_rot_robust,
		&overlap_percent, &error,
		&x, &y, comment.c_str(), 64,
		&PeakParams[0], &PeakParams[1], &PeakParams[2], &PeakParams[3]);
		base_frame_n = frame_n-1;
	}
	// Try to make the rotation angle look sensible
	if(angle>180.0) angle -= 360.0;
	if(angle<-180.0) angle += 360.0;
	centre.Set(x, y);
	return(frame_n);
}

int Y_VMRecord::ReadFileName(FILE *log)
{
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(VMREC_FATAL);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	sscanf_s(buf, "%s", file, 64);
	return(0);
}

int Y_VMRecord::ReadPrevFileName(FILE *log)
{
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(VMREC_FATAL);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	sscanf_s(buf, " %*s %s", prev_file.c_str(), 64);
	return(0);
}

int Y_VMRecord::Read(FILE *log, double resize)
{
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(VMREC_FATAL);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	ReadStg(buf);
	x_shift *= resize; y_shift *= resize;
	centre *= resize;
	return(frame_n);
}

// Now obsolete
int Y_VMRecord::ReadFull(FILE *log, double resize)
{
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(VMREC_FATAL);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	ReadStg(buf);
	x_shift *= resize; y_shift *= resize;
	centre *= resize;
	return(frame_n);
}

// Old-style
// Frame/X-shift/Y-shift/Transl-robust/Zoom/Angle/ZR-robust/
//			Overlap-%/Error/X-center/Y-center
#define REC_o "  %3d	%7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) \
[%6.1lf,%6.1lf] %4s (%9.6lf %9.6lf  %9.6lf %9.6lf)\n"
#define RECSkip_o "# %3d	%7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) \
[%6.1lf,%6.1lf] %4s (%9.6lf %9.6lf  %9.6lf %9.6lf)\n"
void Y_VMRecord::WriteOld(FILE *log, int _frame_n, int hush)
{
	if(hush==0)
		fprintf(log, REC_o, _frame_n, x_shift, y_shift, transl_robust, 
			zoom, angle, zoom_rot_robust, overlap_percent, error,
			centre.X(), centre.Y(), comment,
			PeakParams[0], PeakParams[1], PeakParams[2], PeakParams[3]);
	else
		fprintf(log, RECSkip_o, _frame_n, x_shift, y_shift, transl_robust, 
			zoom, angle, zoom_rot_robust, overlap_percent, error,
			centre.X(), centre.Y(), comment,
			PeakParams[0], PeakParams[1], PeakParams[2], PeakParams[3]);
	fflush(log);
}

// New style, includes 'base_frame_n'
// Frame/X-shift/Y-shift/Transl-robust/Zoom/Angle/ZR-robust/
//			Overlap-%/Error/X-center/Y-center
#define REC "  %3d<-%3d	%7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) \
[%6.1lf,%6.1lf] %4s (%9.6lf %9.6lf  %9.6lf %9.6lf)\n"
#define RECSkip "# %3d<-%3d	%7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) \
[%6.1lf,%6.1lf] %4s (%9.6lf %9.6lf  %9.6lf %9.6lf)\n"
void Y_VMRecord::Write(FILE *log, int _frame_n, int _base_frame_n, int hush)
{
	if(hush==0)
		fprintf(log, REC, _base_frame_n, _frame_n, x_shift, y_shift, transl_robust, 
			zoom, angle, zoom_rot_robust, overlap_percent, error,
			centre.X(), centre.Y(), comment,
			PeakParams[0], PeakParams[1], PeakParams[2], PeakParams[3]);
	else
		fprintf(log, RECSkip, _base_frame_n, _frame_n, x_shift, y_shift, transl_robust, 
			zoom, angle, zoom_rot_robust, overlap_percent, error,
			centre.X(), centre.Y(), comment,
			PeakParams[0], PeakParams[1], PeakParams[2], PeakParams[3]);
	fflush(log);
}

int Y_VMRecord::ReadList(FILE *log, double resize)
{
	double x, y;
		
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(-1);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	sscanf_s(buf, 
		"%20s %20s	%lf %lf (%lf)  %lf %lf (%lf) (%lf,%lf) [%lf,%lf] %s "
		"(%lf %lf %lf %lf)", 
		file, prev_file, &x_shift, &y_shift, &transl_robust, &zoom, &angle, 
		&zoom_rot_robust, &overlap_percent, &error, &x, &y, comment, 64,
		&PeakParams[0], &PeakParams[1], &PeakParams[2], &PeakParams[3]);
	x_shift *= resize; y_shift *= resize;
	centre.Set(x*resize, y*resize);
	// Each successful read increases frame_number by 1
	frame_n++;
	return(frame_n-1);
}

// File/PrevFile/X-shift/Y-shift/Transl-robust/Zoom/Angle/ZR-robust/
//			Overlap-%/Error/X-center/Y-center
#define RECList "%20s %20s	%7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) \
[%6.1lf,%6.1lf] %4s (%9.6lf %9.6lf  %9.6lf %9.6lf)\n"
#define RECListSkip "#%20s %20s	%7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) \
[%6.1lf,%6.1lf] %4s (%9.6lf %9.6lf  %9.6lf %9.6lf)\n"
void Y_VMRecord::WriteList(FILE *log, int hush)
{
	if(hush==0)
		fprintf(log, RECList, file, prev_file, x_shift, y_shift, transl_robust, 
			zoom, angle, zoom_rot_robust, overlap_percent, error,
			centre.X(), centre.Y(), comment,
			PeakParams[0], PeakParams[1], PeakParams[2], PeakParams[3]);
	else
		fprintf(log, RECListSkip, file, prev_file, x_shift, y_shift, transl_robust, 
			zoom, angle, zoom_rot_robust, overlap_percent, error,
			centre.X(), centre.Y(), comment,
			PeakParams[0], PeakParams[1], PeakParams[2], PeakParams[3]);
	fflush(log);
}

int Y_VMRecord::ReadShort(FILE *log, double resize)
{
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(-1);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	sscanf_s(buf, 
		"%20s %20s %lf %lf	%lf %lf (%*lf)", file, prev_file, 64,
		&x_shift, &y_shift, &zoom, &angle);
	x_shift *= resize; y_shift *= resize;
	// Each successful read increases frame_number by 1
	base_frame_n = frame_n-1;
	frame_n++;
	return(frame_n-1);
}

void Y_VMRecord::WriteShort(FILE *log)
{
	fprintf(log, "%20s %20s %7.2lf %7.2lf	  %8.5lf %8.3lf (%8.3lf)\n",
		file, prev_file, x_shift, y_shift, zoom, angle, 
		error);
	fflush(log);
}

// Obsolete?
int Y_VMRecord::ReadFinal(FILE *log, double resize)
{
	double x, y;
	
	// Read until the line is not comment
	do {
		if(fgets(buf, 1023, log)==nullptr) // End of logfile
			return(-1);
	} while(buf[0]=='#' || strlen(buf)<3);
	
	sscanf_s(buf, 
		"%20s  %lf %lf	%lf %lf", file, 64,
		&zoom, &angle, &x, &y);
	centre.Set(x*resize, y*resize);
	// Each successful read increases frame_number by 1
	base_frame_n = frame_n-1;
	frame_n++;
	return(frame_n-1);
}

Y_VMRecord *Y_VMRecord::FindRecordFromFrameNumber(Y_VMRecord *recs, int nrec, int _frame_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].frame_n==_frame_n)
			return(&recs[i]);
	}
	return(nullptr);
}

Y_VMRecord *Y_VMRecord::FindRecordFromBaseFrameNumber(Y_VMRecord *recs, int nrec, 
	int _base_frame_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].base_frame_n==_base_frame_n)
			return(&recs[i]);
	}
	return(nullptr);
}

int Y_VMRecord::FindOrderNumberFromFrameNumber(Y_VMRecord *recs, int nrec, int _frame_n)
{
	for(int i = 0; i<nrec; i++) {
		if(recs[i].frame_n==_frame_n)
			return(i);
	}
	return(-1);
}



ostream &operator<<(ostream &ostr, Y_VMRecord &v)
{
	ostr<<"(Z="<<v.zoom<<", A="<<v.angle<<", X="<<v.x_shift<<", Y="<<v.y_shift<<")";
	ostr<<"(.)="<<v.centre;
	return(ostr);
}

// requires BMPImage, which requires Y_VMRecord... Bad idea...
//int Y_VMRecord::GetImageInfo()
//{
//	if(strlen(file)<2)
//		return(-1); // Probably file name is not set
//	BMPImage *img(file);
//	w = img->w; h = img->h;
//	return(0);
//}

Y_VMRecord *Y_VMRecord::ReadRecs(const char *logfile, int *nrec, double resize)
{
	FILE *fp_log;
	if(fopen_s(&fp_log, logfile, "r")) {
		fprintf(stderr, "Cannot open %s.\n", logfile);
		return(nullptr);
	}
	int n = 0;
	Y_VMRecord rec;
	while(rec.Read(fp_log)>VMREC_FATAL)
		n++;
	fprintf(stderr, "Number of RAR records = %d\n", n);
	Y_VMRecord *records = new Y_VMRecord[n];
	rewind(fp_log);
	for(int i = 0; i<n; i++) {
		records[i].Read(fp_log, resize);
	}
	fclose(fp_log);
	*nrec = n;
	
	return(records);
}

// Old-style, no base_frame_n, consecutive numbers
int Y_VMRecord::SaveRecs(const char *logfile, Y_VMRecord *recs, int nrec)
{
	FILE *fp;
	if(fopen_s(&fp, logfile, "w"))
		return(-1);
	for(int i = 0; i<nrec; i++) {
		recs[i].WriteOld(fp, i);
	}
	fclose(fp);
	
	return(0);
}

// Same as ReadRecs(), but reading peak params as well
Y_VMRecord *Y_VMRecord::ReadRecsFull(const char *logfile, int *nrec, double resize)
{
	FILE *fp_log;
	if(fopen_s(&fp_log, logfile, "r")) {
		fprintf(stderr, "Cannot open %s.\n", logfile);
		return(nullptr);
	}
	int n = 0;
	Y_VMRecord rec;
	while(rec.Read(fp_log)>VMREC_FATAL)
		n++;
	fprintf(stderr, "Number of RAR records = %d\n", n);
	Y_VMRecord *records = new Y_VMRecord[n];
	rewind(fp_log);
	for(int i = 0; i<n; i++) {
		records[i].ReadFull(fp_log, resize);
	}
	fclose(fp_log);
	*nrec = n;
	
	return(records);
}

// Same as SaveRecs but original frame numbers
int Y_VMRecord::SaveRecsFull(const char *logfile, Y_VMRecord *recs, int nrec)
{
	FILE *fp;
	if(fopen_s(&fp, logfile, "w"))
		return(-1);
	for(int i = 0; i<nrec; i++) {
		recs[i].Write(fp, recs[i].frame_n, recs[i].base_frame_n);
	}
	fclose(fp);
	
	return(0);
}

// Same as above but with file info
int Y_VMRecord::SaveRecsFullInfo(const char *logfile, Y_VMRecord *pRecs, int nrec, char *pInfo)
{
	FILE *fp;
	if(fopen_s(&fp, logfile, "w"))
		return(-1);
	fprintf(fp, "%s", pInfo);
	for(int i = 0; i<nrec; i++) {
		pRecs[i].Write(fp, pRecs[i].frame_n, pRecs[i].base_frame_n);
	}
	fclose(fp);
	
	return(0);
}




Y_VMPredictor::Y_VMPredictor(int frame)
{
	nrecords = 0;
	for(int i = 0; i<5; i++) {
		frames[i] = 0; 
		overlaps[i] = 0.0;
	}
	frames[0] = frame;
}

// Keep previous result
int Y_VMPredictor::PredictAccept(int frame, double overlap)
{
	nrecords++;
	if(nrecords>4) nrecords = 4;
	// Shift values
	for(int i = 3; i>=0; i--) {
		frames[i+1] = frames[i];
		overlaps[i+1] = overlaps[i];
	}
	frames[0] = frame;
	overlaps[0] = overlap;
	int new_step = (int) ((100.0-80.0)/(100.0-overlap)*(frame-frames[1]));
	if((float) new_step>1.5*(frame-frames[1]))
		new_step = int(1.5*(frame-frames[1]));
	if(new_step==(frame-frames[1])) // Increase by 1
		new_step++;
	if(!new_step) new_step = 1;

	//fprintf(stdout, "Predicted step=%d\n", new_step);
	//fprintf(stdout, "\t %3d %3d %3d %3d %3d\n", frames[0],
	//	frames[1], frames[2], frames[3], frames[4]);
	//fprintf(stdout, "\t %3.0lf %3.0lf %3.0lf %3.0lf %3.0lf\n", overlaps[0],
	//	overlaps[1], overlaps[2], overlaps[3], overlaps[4]);

	return(new_step);
}

// Ignore previous result
int Y_VMPredictor::PredictIgnore(int frame, double overlap)
{
	int new_step = (int) ((100.0-80.0)/(100.0-overlap)*(frame-frames[0]));
	// If overlap is very big, linear model tends to overshoot. Smooth it.
	if((float) new_step>1.5*(frame-frames[0]))
		new_step = int(1.5*(frame-frames[0]));
	if(new_step==(frame-frames[0])) // Increase by 1
		new_step++;
	if(!new_step) new_step = 1;

	//fprintf(stdout, "Predicted step=%d\n", new_step);
	//fprintf(stdout, "\t %3d %3d %3d %3d %3d\n", frames[0],
	//	frames[1], frames[2], frames[3], frames[4]);
	//fprintf(stdout, "\t %3.0lf %3.0lf %3.0lf %3.0lf %3.0lf\n", overlaps[0],
	//	overlaps[1], overlaps[2], overlaps[3], overlaps[4]);

	return(new_step);
}



Y_TrajRecord::Y_TrajRecord(int fn, 
		double zm, double pm, double rm, double ym, double dxm, double dym, 
		double pc, double rc, double yc, double zc, double dxc, double dyc, 
		double err)
{
	frame_n = fn;
	zoom_m = zm; pitch_m = pm; roll_m = rm; yaw_m = ym;
	dx_m = dxm; dy_m = dym;
	zoom_c = zc; pitch_c = pc; roll_c = rc; yaw_c = yc;
	dx_c = dxc; dy_c = dyc;
	error = err; 
}

void Y_TrajRecord::SetM(int fn, double zm, double pm, double rm, double ym,
			double dxm, double dym)
{
	frame_n = fn;
	zoom_m = zm; pitch_m = pm; roll_m = rm; yaw_m = ym;
	dx_m = dxm; dy_m = dym;
}

void Y_TrajRecord::SetC(int fn, double zc, double pc, double rc, double yc,
			double dxc, double dyc, double err)
{
	frame_n = fn;
	zoom_c = zc; pitch_c = pc; roll_c = rc; yaw_c = yc;
	dx_c = dxc; dy_c = dyc;
	error = err; 
}



