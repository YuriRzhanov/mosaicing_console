#ifndef _ARRAY_H_DEFINED_
#define _ARRAY_H_DEFINED_

#include "stdafx.h"

#include <stdio.h>
#include <limits.h>
#include <string>
#include "YUtils.h"
using namespace std;

// N-dimensional array class

template <class T>
class ArrayND {
public:
	typedef enum { A_COMPLEX = 0, A_BOOL = 1, A_UCHAR = 2, A_CHAR = 3, A_SHORT = 4, A_INT = 5, A_FLOAT = 6, A_DOUBLE = 7 } TYPE;
	public:
		ArrayND(int k) {N=k; n=new int[N]; steps=new double[N]; origin=new double[N]; data=nullptr; }
		~ArrayND() {delete [] n; delete [] steps; delete [] origin; if(data) delete [] data; }
		void SetType(TYPE _type) { type=_type; }
		void SetDim(int i, int L) { n[i]=L; }
		//void SetDimensions(int* L) { for(int i = 0; i<N; i++) n[i]=L[i]; }
		void SetDimensions(int* L) { memcpy(n, L, N*sizeof(int)); }
		void SetStep(int i, double _step) { steps[i]=_step; }
		//void SetSteps(double* s) { for(int i = 0; i<N; i++) steps[i]=s[i]; }
		void SetSteps(double* s) { memcpy(steps, s, N*sizeof(double)); }
		//void SetOrigin(double* o) { for(int i = 0; i<N; i++) origin[i]=o[i]; }
		void SetOrigin(double* o) { memcpy(origin, o, N*sizeof(double)); }
		int Dim() { return(N); }
		double GetStep(int i) { return(steps[i]); }
		double GetOrigin(int i) { return(origin[i]); }
		int Save(const char* name, int metadataSize, void* metadata);
		int Read(const char* name, int& metadataSize, void* metadata);
	public:
		TYPE type;
		int cellSize;
		int N, *n; // Number of dimensions and numbers of cells in all dimensions
		double* steps; // cell sizes in all dimensions
		double* origin; // point that steps are counted from
		//static int iNaN; replaced by Y_Utils::_INVALID_INT
		//static double dNaN; replaced by Y_Utils::_INVALID_DOUBLE
		T* data;
};

template <class T>
class Array1D : public ArrayND<T> {
	public:
		Array1D(int _n1);
		Array1D(Array1D* pA);
		Array1D(const char* name) { Read(name); }
		void Zero() { memset(data, 0, n[0]*sizeof(T)); }
		void Invalidate();
		int Valid(int i);
		int Valid(int dims[]) {
			return(Valid(dims[0]));
		}
		int BadIndex(int i);
		int Index(int i) {return(i);}
		int Set(int i, T v);
		int Set(int* pN, T v);
		T Get(int i, bool* valid = nullptr); // Return i-th element
		T Get(int* pN, bool* valid = nullptr); // Return i-th element
		T *GetP(int i); // Return pointer to i-th element
		T *GetP(int* pN); // Return pointer to i-th element
		void SetTo(T v);
		void GetDimensions(int* N) { N[0] = n[0]; }
		void Increase(int i, T by) { data[i] += by; }
		void Increase(int* pN, T by) { data[pN[0]] += by; }
		//void operator ++(int i) { data[i] += (T)1; }
	public:
};

template <class T>
class Array2D : public ArrayND<T> {
	public:
		Array2D(int _n1, int _n2);
		Array2D(int* n);
		Array2D(Array2D* pA);
		Array2D(const char* name) { Read(name); }
		void Zero() { memset(data, 0, n[0]*n[1]*sizeof(T)); }
		void Invalidate();
		int Valid(int i, int j);
		int Valid(int dims[]) {
			return(Valid(dims[0], dims[1]));
		}
		int BadIndex(int i, int j);
		int Index(int i, int j) {return(j*n[0]+i);}
		void Indexes(int N, int& i, int& j) {
			j = N/n[0]; i = N-j*n[0];
		}
		int Set(int i, int j, T v);
		int Set(int* pN, T v);
		T Get(int i, int j, bool* valid = nullptr);
		T Get(int* pN, bool* valid = nullptr);
		T *GetP(int i, int j);
		T *GetP(int* pN);
		void SetTo(T v);
		void GetDimensions(int* N) { memcpy(N, n, 2*sizeof(int)); }
		void Increase(int i, int j, T by) { data[j*n[0]+i] += by; }
		void Increase(int* pN, T by) { data[pN[1]*n[0]+pN[0]] += by; }
		//void operator ++(int i, int j) { data[j*n[0]+i] += (T)1; }
	public:
};

template <class T>
class Array3D : public ArrayND<T> {
	public:
		Array3D(int _n1, int _n2, int _n3);
		Array3D(int* n);
		Array3D(Array3D* pA);
		Array3D(const char* name) { Read(name); }
		void Zero() { memset(data, 0, n[0]*n[1]*n[2]*sizeof(T)); }
		void Invalidate();
		int Valid(int i, int j, int k);
		int Valid(int dims[]) {
			return(Valid(dims[0], dims[1], dims[2]));
		}		
		int BadIndex(int i, int j, int k);
		int Index(int i, int j, int k) {return((k*n[1]+j)*n[0]+i);}
		void Indexes(int N, int& i, int& j, int& k) {
			k = N/(n[0]*n[1]); j = (N-k*n[0]*n[1])/n[0]; i = N-(k*n[1]+j)*n[0];
		}
		int Set(int i, int j, int k, T v); 
		int Set(int* pN, T v); 
		T Get(int i, int j, int k, bool* valid = nullptr);
		T Get(int* pN, bool* valid = nullptr);
		T *GetP(int i, int j, int k);
		T *GetP(int* pN);
		void SetTo(T v);
		void GetDimensions(int* N) { memcpy(N, n, 3*sizeof(int)); }
		void Increase(int i, int j, int k, T by) { data[(k*n[1]+j)*n[0]+i] += by; }
		void Increase(int* pN, T by) { data[(pN[2]*n[1]+pN[1])*n[0]+pN[0]] += by; }
		//void operator ++(int i, int j, int k) { data[(k*n[1]+j)*n[0]+i] += (T)1; }
	public:
};

template <class T>
class Array4D : public ArrayND<T> {
	public:
		Array4D(int _n1, int _n2, int _n3, int _n4);
		Array4D(int* n);
		Array4D(Array4D* pA);
		Array4D(const char* name) { Read(name); }
		void Zero() { memset(data, 0, n[0]*n[1]*n[2]*n[3]*sizeof(T)); }
		void Invalidate();
		int Valid(int i, int j, int k, int l);
		int Valid(int dims[]) {
			return(Valid(dims[0], dims[1], dims[2], dims[3]));
		}
		int BadIndex(int i, int j, int k, int l);
		int Index(int i, int j, int k, int l) 
			{return(((l*n[2]+k)*n[1]+j)*n[0]+i);}
		int Set(int i, int j, int k, int l, T v);
		int Set(int* pN, T v);
		T Get(int i, int j, int k, int l, bool* valid = nullptr);
		T Get(int* pN, bool* valid = nullptr);
		T *GetP(int i, int j, int k, int l);
		T *GetP(int* pN);
		void SetTo(T v);
		void GetDimensions(int* N) { memcpy(N, n, 4*sizeof(int)); }
		void Increase(int i, int j, int k, int l, T by) { data[((l*n[2]+k)*n[1]+j)*n[0]+i] += by; }
		void Increase(int* pN, T by) { data[((pN[3]*n[2]+pN[2])*n[1]+pN[1])*n[0]+pN[0]] += by; }
		//void operator ++(int i, int j, int k, int l) { data[((l*n[2]+k)*n[1]+j)*n[0]+i] += (T)1; }
	public:
};

// Source is included in *.h
#if defined(WIN32) || defined(WIN64)
#include "Array.include"
#endif

#endif
