#ifndef _BMPIMAGEG_H
#define _BMPIMAGEG_H

class PixelC;
class BMPImageG;
class FltImageG;
class DblImageG;

#include <vector>
#include "Point2.h"
#include "PixelC.h"
#include "DblImageG.h"
#include "BMPImage.h"
#include "YHistogram.h"

class BMPImageG : public BMPImage {
	public:
		BMPImageG();
		BMPImageG(int w, int h);
		BMPImageG(int w, int h, u_char *im);
		BMPImageG(int w, int h, u_short *im);
		BMPImageG(int w, int h, float *im);
		BMPImageG(int w, int h, double *im);
		BMPImageG(const char *file, int suppress_warnings = 1);
		~BMPImageG();
		void Dimensions(int w, int h);
		void GetMinMax(u_char& min, u_char& max);
		BMPImageG *Copy();
		bool set_pixel(Point2<int> P, u_char p) {
			return(set_pixel(P.X(), P.Y(), p)); 
		}
		bool set_pixel(int pos, u_char p) {
			if(!Y_Image::valid(pos)) 
				return(false);
			im[pos] = p; 
			return(true); 
		}
		bool set_pixel(int x, int y, float p) {
			return(set_pixel(x, y, u_char(p))); 
		}
		bool set_pixel(int x, int y, double p) {
			return(set_pixel(x, y, u_char(p))); 
		}
		bool set_pixel(int x, int y, Y_RGB<u_char> p) {
			return(set_pixel(x, y, p.Greyscale())); 
		}
		bool set_pixel(int x, int y, Y_RGB<float> p) {
			return(set_pixel(x, y, p.Greyscale())); 
		}
		bool set_pixel(int x, int y, Y_RGB<double> p) {
			return(set_pixel(x, y, p.Greyscale())); 
		}
		bool set_pixel(int x, int y, u_char p) {
			if(!Y_Image::valid(x,y)) 
				return(false);
			im[y*w+x] = p; 
			return(true); 
		}
		u_char get_pixel(Point2<int> P, int *valid = nullptr) {
			return(get_pixel(P.X(), P.Y(), valid)); 
		}
		u_char get_pixel(int pos, int *valid = nullptr) {
			if(!Y_Image::valid(pos)) {
				if(valid) *valid = 0;
				return(INVALID);
			}
			if(valid) *valid = 1;
			return(im[pos]); 
		}
		u_char get_pixel(int x, int y, int *valid = nullptr) {
			if(!Y_Image::valid(x,y)) {
				if(valid) *valid = 0;
				return(INVALID);
			}
			if(valid) *valid = 1;
			return(im[y*w+x]); 
		}
		double get_pixel_D2(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D2(P.X(), P.Y(), valid)); 
		}
		double get_pixel_D3(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D3(P.X(), P.Y(), valid)); 
		}
		double get_pixel_D2(double x, double y, int *valid = nullptr) {
			return(Bilinear(x, y, valid)); 
		}
		double get_pixel_D3(double x, double y, int *valid = nullptr) {
			return(Bicubic(x, y, valid)); 
		}
		// operator versions: indices start from 0
		u_char& operator () (int x, int y) {
			if(!Y_Image::valid(x,y)) 
				return(INVALID);
			return(im[y*w+x]); 
		}
		//u_char operator () (int x, int y) const {
		//	if(!Y_Image::valid(x,y)) return(INVALID);
		//	return(im[y*w+x]);
		//}
		int PutAt(BMPImageG *p, int x, int y);
		void Copy(BMPImageG *s);
		void InvalidateColour(u_char clr);
		int Substitute(u_char clr, u_char with, Y_Rect<int>* pRect = nullptr); // Change pixels of "clr" with pixels of "with". Returns No of substituted.
		int LeaveOnly(u_char leave, u_char with); // Change all pixels except for 'leave' with pixels of 'with'. Returns No of substituted.
		int CountPixels(u_char clr);
		int Validate(u_char g_invalid = INVALID); // make sure none of pixels give g_invalid
		void SetColour(u_char clr, int override_invalid = 1); // Set image to homogeneous colour. If 'has_invalid'=true, invalid colour are preserved.
		void SetBlack() { memset(im, 0, w*h*sizeof(u_char)); }
		void SetInvalid() { 
			for(int j = 0; j<w*h; j++) im[j] = INVALID; 
		} // Same as above, but slower
		BMPImageG** Split(int factor);
		Y_Histogram<int>* CalculateHistogram(bool has_invalid = false, int low = 0, int high = 255);
		int ShiftAverageTo(u_char level); // Linear shift in brightness to match average and 'level'. Returns number of saturated pixels.
		void CorrectIllum(BMPImageG *p);
		void ValidateByMask(BMPImageG *p);
		int Read(const char *file, int suppress_warnings = 0);
		int Save(const char *file, int bpp = 1);
		int SaveASCII(const char* file, bool saveIndexes = false);
		int ReadPGM(const char *file);
		int SavePGM(const char *file);
		bool valid(Point2<int>& P) {
			return(valid(P.X(),P.Y()));
		}
		bool valid(int x, int y) {
			return(Y_Image::valid(x, y) && get_pixel(x, y)!=INVALID);
		}
		int valid(double x, double y);
		int Border(int x, int y);
		BMPImageG *CutSubimage(int at_x, int at_y, int sw, int sh);
		BMPImageG *CutSubimage(Y_Rect<int> r) { return(CutSubimage(r.x, r.y, r.w, r.h)); }
		void Crop(int at_x, int at_y, int sw, int sh);
		void Crop(Y_Rect<int> r) { return(Crop(r.x, r.y, r.w, r.h)); }
		double AverageBrightness(int invalid_clr = -1);
		int Replicate(int factor);
		int ZoomToDest_Bilinear(int dest_w, int dest_h, bool show_progress = false);
		int Zoom_Bilinear(double zoom, bool show_progress = false);
		int ZoomToDest_Bicubic(int dest_w, int dest_h, bool show_progress = false);
		int Zoom_Bicubic(double zoom, bool show_progress = false);
		int Translate_Bilinear(double X, double Y, u_char bg = m_black, bool show_progress = false);
		int Translate_Bicubic(double X, double Y, u_char bg = m_black, bool show_progress = false);
		int Rotate_Bilinear(double angle_degrees, u_char bg = m_black, bool show_progress = false);
		int Rotate_Bicubic(double angle_degrees, u_char bg = m_black, bool show_progress = false);
		int ZoomRotateTranslate_Bilinear(double zoom, double angle_degrees,
			double X, double Y, int flags = DO_ALL);
		int ZoomRotateTranslate_Bicubic(double zoom, double angle_degrees,
			double X, double Y, int flags = DO_ALL);
		// April 2006: Detrending is moved to class Detrend
		//void DetrendOld(int detrend_order);
		//void DetrendLaurie(int detrend_order, double shift = 0.0, int verbose = 0);
		//void Detrend(int detrend_order);
		// Jan 2006: instead of Detrend(), use GetTrend()/DeTrend() sequence
		// Order=0 -> Dim=1; Order=1 -> Dim=3; Order=2 -> Dim=6
		//NEWMAT::ColumnVector GetTrend(int detrend_order);
		//void DeTrend(NEWMAT::ColumnVector& r, TREND_TYPE trend_type = TREND_STRETCH); // Dimension of 'r' determins detrend_order 
		void RotateQuarter(int nr_of_quarters); // 1: by pi/2, 2: by pi, 3: by -pi/2
		double DifferenceErrorDouble(BMPImage *i, double xs, double ys, 
			int *overlap, const char *name = nullptr);
		void Blur_1(int kernel, int times = 1); // 'times' - number of times
		void Blur_2(int kernel, int times = 1); // 'times' - number of times
		void Blur(int kernel, int times = 1); // 'times' - number of times
		double Bilinear(double dx, double dy, int *valid = nullptr);
		// Enforce interpolation assuming that top-left corner is given
		double Bilinear(double dx, double dy, int tl_i, int tl_j);
		double Bicubic(double dx, double dy, int *valid = nullptr);
		Y_Rect<int> DetectMargins();
		// Starting with (x,y) colour all pixels with 'fill' until 'bound'
		int SeedFill(int seedx, int seedy, u_char bound, u_char fill, int bufferLength = 16384, Y_Rect<int>* pR = nullptr);
		// Starting with (x,y) colour all pixels with 'fill' until any other
		int SeedFill(int seedx, int seedy, u_char fill, int bufferLength = 16384, Y_Rect<int>* pR = nullptr);
		// Starting with (x,y) colour all pixels with 'fill' until 'bound' returning vector of locations
		int SeedFill(int seedx, int seedy, u_char bound, u_char fill, vector<Point2<int> >& locations, int bufferLength = 16384, Y_Rect<int>* pR = nullptr);
		void MarkPoint(double x, double y, int rad, u_char clr);

		// Based on given principal point and coefficients
		//BMPImageG *Undistort_A_Bilinear(Y_Camera *C);
		//BMPImageG *Distort_A_Bilinear(Y_Camera *C);
		//BMPImageG *Undistort_A_Bicubic(Y_Camera *C);
		//BMPImageG *Distort_A_Bicubic(Y_Camera *C);
	protected: // Is called on first neccessity
		void Prefilter(); // fills in 'pf_data' and sets 'prefilter'
	public: // Done from outside when needed
		double pfBilinear(double dx, double dy, int *valid = nullptr);
	public: // Done from outside when is not likely to be needed
		void ResetPrefilter() {
			if(prefilter) { delete [] pf_data; prefilter = 0; } }
	public:
		u_char *im;
		static u_char INVALID;
		static u_char m_white, m_black;
	protected:
		double *pf_data; // temporary data for bilinear prefiltering
};


#endif
