#ifndef _BMPIMAGE_H
#define _BMPIMAGE_H

#define EXTN	"bmp"
#define DEXTN	"dbl"

#include <iostream>
#include <string>

#define use_namespace
#define WANT_STREAM                  // include.h will get stream fns
#define WANT_MATH                    // include.h will get math fns
                                     // newmatap.h will get include.h
#include "newmatap.h"                // need matrix applications
//#include "newmatio.h"                // need matrix output routines
//using namespace NEWMAT;              // access NEWMAT namespace

#include "YUtils.h"
#include "ConsoleProgress.h"
#ifndef CMIN
#include "Array.h"
#endif

// Used for colour and greyscale
int zoom_image(u_char **r, u_char **g, u_char **b, int w, int h,
	int dest_w, int dest_h, int z);
// Only colour, triple-wise storage (since Feb 2005)
int zoom_imageC(u_char **im, int w, int h, int dest_w, int dest_h);
int zoom_dbl_image(double **v, int w, int h, int dest_w, int dest_h);
// Jan 2007 - new field 'has_invalid' is set to false by default.
// If manually set to true, pixels with certain value mean nodata.

#define MAXN	12

/* bmp header structure */
typedef struct {
	unsigned short	typ;
	unsigned long	fsize;
	unsigned short	res1;
	unsigned short	res2;
	unsigned long	off;
	unsigned long	hsize;
	unsigned long	cols;
	unsigned long	rows;
	unsigned short	planes;
	unsigned short	bpp;
	unsigned long	compress;
	unsigned long	isize;
	unsigned long	xpel;
	unsigned long	ypel;
	unsigned long	clrused;
	unsigned long	clrimp;
} bmpheader;

// 2-dimensional array - generic image
class Y_Image {
	public:
		typedef enum { UNKNOWN_TYPE=-1, BMP_TYPE=0, INT_TYPE=1, FLT_TYPE=2, DBL_TYPE=3, JPEG_TYPE=4, PNG_TYPE=5, TIFF_TYPE=6  } IMAGE_TYPE;
		Y_Image();
		Y_Image(int _w, int _h);
		Y_Image(int _w, int _h, double f_length);
		static IMAGE_TYPE DetermineType(const char* name);
		void SetFocalLength(double f_length) {focal_length=f_length;}
		Y_Rect<int> GetRect() {return(Y_Rect<int>(0, 0, w, h));}
		static void SetPFtau(double tau) {Y_Image::pf_tau = tau;}
		void SetConfRect(int conf_x, int conf_y, int conf_w, int conf_h)
			{conf.Set(conf_x, conf_y, conf_w, conf_h);}
		void SetConfRect(Y_Rect<int> r) {conf = r;}
		Y_Rect<int> GetConfRect() {return(conf);}
		bool valid(int pos) {
			return(pos>=0 && pos<w*h);
		}
		bool valid(int x, int y) {
			return(x>=0 && x<w && y>=0 && y<h);
		}
		bool valid(double x, double y);
		void SetWatch(int x, int y) { watch[n_watch].Set(x, y); n_watch++; }
		void SetWatch(Point2<int> p) { SetWatch(p.X(), p.Y()); }
		int CheckWatch(int x, int y) {
			for(int i = 0; i<n_watch; i++) {
				if(watch[i].X()==x && watch[i].Y()==y) return(1);
			}
			return(0);
		}
		int CheckWatch(Point2<int> p) { return(CheckWatch(p.X(), p.Y())); }
	protected:
		// For SGI/Sun/Apple (Most significant byte first) endian=0 (*s=256)
		// For Intel (Least significant byte first) endian=1 (*s=1)
		void get_endian() { char c[2]; u_short *s; c[0] = 1; c[1] = 0;
			s = (u_short *) c; endian = (*s == 1); }
		void gausselim(double a[MAXN][MAXN], double z[MAXN], 
			double b[MAXN], int n);
		void InitBicubicMatrix();
	public:
		int w, h, endian, fail, prefilter, conf_set, debug;
		double focal_length;
		Y_Rect<int> conf; // Confidence rectangle
	protected:
		static double pf_tau;
		NEWMAT::Matrix M_bicubic;
	public:
		int n_watch;
		Point2<int> watch[10];
		static char error[256];
};


class BMPImage : public Y_Image {
	public:
		BMPImage();
		BMPImage(int _w, int _h, int _z);
		BMPImage(const char *file, int suppress_warnings = 1) { 
			Read(file); 
		}
		~BMPImage();
		int Read(const char *file, int suppress_warnings = 1);
		int ReadPGM_PPM(const char *file);
		static void SkipPGMPPMComments(FILE *fp);
		bool IsColour() { return(z==3); }
		bool IsRGBA() { return(z==4); }
		int GetNumPlanes() { return z; }
		void SetFocalLength(double f_length);
	protected:
		int ReadBmpHeader(FILE *fi);
		int WriteBmpHeader(FILE *fo);
		void SetBmpHeader(int w, int h, int z);
	public:
		void PrintBmpHeader(FILE *fo);
	public:
		bmpheader bmphdr;
		unsigned int z, bpp, padw, padb;
		bool has_invalid; // if true, pixels with certain value mean nodata
		int even_zoom; // If set to 1, zoomed image always has even dimensions 
};

#endif
