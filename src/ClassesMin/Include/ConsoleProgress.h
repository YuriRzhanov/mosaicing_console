#ifndef CONSOLEPROGRESS_H
#define CONSOLEPROGRESS_H

#include <stdio.h>
#include "YUtils.h"

class ConsoleProgress {
public:
	ConsoleProgress(int total, int length = 70)
		: m_total(total), m_count(0), m_num(0)
	{
		m_share = m_total/length;
	}
	~ConsoleProgress() { fprintf(stdout, "\n"); }
	void Next() {
		m_count++;
		if(m_count>m_share) {
			Y_Utils::GetConsoleTextAttribute();
			Y_Utils::ConsoleGreen();
			fprintf(stdout, "%d", m_num%10);
			Y_Utils::ResetConsole();
			m_count = 0; m_num++;
		}
	}
	int Num() { return(m_num); }
private:
	int m_total, m_share, m_count, m_num;
};

#endif
