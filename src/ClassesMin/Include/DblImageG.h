#ifndef _DBLIMAGEG_H
#define _DBLIMAGEG_H

class BMPImageG;
class BMPImageC;
class IntImageG;
class DblImageG;

#include "Coord2.h"
#include "DblImage.h"

class DblImageG : public DblImage {
	public:
		DblImageG(int _w, int _h, double *data = nullptr);
		DblImageG(int _w, int _h, double init);
		DblImageG(DblImageG *d);
		DblImageG(BMPImageG *img, int hasInvalid = 0);
			// if 'hasInvalid' is set to 1, BMPImageG has invalid pixels marked with 0's
		DblImageG(BMPImageC *img, int hasInvalid = 0);
			// if 'hasInvalid' is set to 1, BMPImageC has invalid pixels INVALID_RGB
		DblImageG(IntImageG *img, int hasInvalid = 0);
		DblImageG(const char *name, bool old = false);
		~DblImageG() { 
			if(d_im) {
				delete [] d_im;
				d_im = nullptr;
			}
			fail = 1;
		}
		void SetRanges(double _min, double _max) { v_min=_min; v_max=_max;
			ranges_set=1; }
		DblImageG *Copy();
		void Copy(DblImageG *img);
		int Read(const char *name, bool old = false);
		int Save(const char *name, bool old = false);
		int SaveASCII(const char* file, bool saveIndexes = false);
		// Reset to zeroes
		void SetBlack() { memset(d_im, 0, w*h*sizeof(double)); }
		void SetColour(double v);
		bool Normalize(double sum = 1.0); // Make sum of all pixels equal to 'sum'
		bool set_pixel(Point2<int> P, double p) {
			return(set_pixel(P.X(), P.Y(), p)); }
		bool set_pixel(int pos, double p) {
			if(!Y_Image::valid(pos)) return(false);
			d_im[pos] = p; return(true); }
		bool set_pixel(int x, int y, double p) {
			if(!Y_Image::valid(x,y)) return(false);
			d_im[y*w+x] = p; return(true); }
		double get_pixel(Point2<int> P, int *valid = nullptr) {
			return(get_pixel(P.X(), P.Y(), valid)); }
		double get_pixel(int pos, int *valid = nullptr) {
			if(!Y_Image::valid(pos)) {
				if(valid) *valid = 0;
				return(Y_Utils::_INVALID_DOUBLE);
			}
			if(valid) *valid = 1;
			return(d_im[pos]); }
		double get_pixel(int x, int y, int *valid = nullptr) {
			if(!Y_Image::valid(x,y)) {
				if(valid) *valid = 0;
				return(Y_Utils::_INVALID_DOUBLE);
			}
			return(d_im[y*w+x]); }
		double get_pixel_D2(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D2(P.X(), P.Y(), valid)); }
		double get_pixel_D2(double x, double y, int *valid = nullptr) {
			return(Bilinear(x, y, valid)); }
		double get_pixel_D3(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D3(P.X(), P.Y(), valid)); }
		double get_pixel_D3(double x, double y, int *valid = nullptr) {
			return(Bicubic(x, y, valid)); }
		// operator versions: indices start from 0
		double& operator () (int x, int y) {
			if(!Y_Image::valid(x,y)) return(Y_Utils::_INVALID_DOUBLE);
			return(d_im[y*w+x]);
		}
		int PutAt(DblImageG *p, int x, int y);
		int border(int x, int y);
		int valid(int x, int y) { return(Y_Image::valid(x, y) && !isnan(d_im[y*w+x])); }
		int valid(double x, double y);
		int valid(Point2<double> P) { return(valid(P.x, P.y)); }
		void SetInvalid() { for(int j = 0; j<w*h; j++) d_im[j] = Y_Utils::_INVALID_DOUBLE; }
		int SaveBMP(const char *name, int rescale=0, double ignore_value=1.e39);
		int SaveBMP(const char *name, double min, double max);
		int SaveBMPRainbow(const char *name, int type = 1);
		BMPImageC* BMPRainbow(int type = 1);
		DblImageG *CutSubimage(int at_x, int at_y, int sw, int sh);
		DblImageG *CutSubimage(Y_Rect<int> r) { return(CutSubimage(r.x, r.y, r.w, r.h)); }
		void Crop(int at_x, int at_y, int sw, int sh);
		void Crop(Y_Rect<int> r) { return(Crop(r.x, r.y, r.w, r.h)); }
		double Bilinear(double dx, double dy, int *valid = nullptr);
		double Bicubic(double dx, double dy, int *valid = nullptr);
		// Enforce interpolation assuming that top-left corner is given
		double Bilinear(double dx, double dy, int tl_i, int tl_j);
		void Log(); // Zeroes become INVALID
		void ScaleShift(double gain, double offset);
		DblImageG** Split(int factor);
		bool Subtract(DblImageG* pDbl);
		bool Add(DblImageG* pDbl);
		bool Multiply(DblImageG* pDbl);
		bool Divide(DblImageG* pDbl);
		bool Max(DblImageG* pDbl); // per-pixel maximum values
		bool Min(DblImageG* pDbl); // per-pixel minimum values
		BMPImageG *Greyscale(int rescale=0, double ignore_value=1.e39);
		BMPImageG *Greyscale(double min, double max);
		void ValidateAll();
		double AverageBrightness(double invalid_clr = DblImageG::INVALID);
		int ZoomToDest_Bilinear(int dest_w, int dest_h);
		int Zoom_Bilinear(double zoom, int flags = DO_ALL);
		int ZoomToDest_Bicubic(int dest_w, int dest_h);
		int Zoom_Bicubic(double zoom, int flags = DO_ALL);
		void RotateQuarter(int nr_of_quarters); // 1: by pi/2, 2: by pi, 3: by -pi/2
		int Rotate_Bilinear(double angle_degrees, int flags = DO_ALL);
		int Rotate_Bicubic(double angle_degrees, int flags = DO_ALL);
		double difference_error_double(DblImageG *i, double xs, double ys, 
			int *overlap, char *name = nullptr);
		void Blur(int kernel, int times = 1); // 'times' - number of times
		// Finds min and max, if value is ignore_value, it is ignored
		void MinMax(double& v_min, double& v_max, double ignore_value=1.e39);
		// ... also returns location of extremums
		void MinMax(double& v_min, Point2<int>& p_min, double& v_max, Point2<int>& p_max, double ignore_value=1.e39);
		void Scale(double min, double max); 
		void CutOffPercentile(double& min, double& max, double percentile);
		// April 2006: Detrending is moved to class Detrend
		//void DetrendLaurie(int detrend_order, int verbose = 0);
		// Jan 2006: instead of Detrend(), use GetTrend()/ApplyTrend() sequence
		// Order=0 -> Dim=1; Order=1 -> Dim=3; Order=2 -> Dim=6
		//NEWMAT::ColumnVector GetTrend(int detrend_order);
		//void ApplyTrend(int detrend_order, NEWMAT::ColumnVector& r);
		//void ApplyTrend(NEWMAT::ColumnVector& r); // Dimension of 'r' determins detrend_order 
		static void SetMutualRange(DblImageG *d1, DblImageG *d2, double percentile = 100.0);
		static DblImageG* GaussianKernel(int sz);
		static DblImageG* GaussianKernel(double sigma);
		static DblImageG* GaussianKernel(int sz, double sigma);
		static DblImageG* HannKernel(int sz);
		static DblImageG* HammingKernel(int sz);
		static DblImageG* TukeyKernel(int sz, double alphaX, double alphaY);
		double Convolve(int I, int J, DblImageG *kernel, int *n = nullptr);
		double Convolve(double X, double Y, DblImageG *kernel, int *n = nullptr);
		DblImageG* Convolve(DblImageG *kernel, bool normalize = false);
		bool LocalMaximum_4(int i, int j);
		bool LocalMaximum_8(int i, int j);
		// Based on given principal point and coefficients
		//DblImageG *Undistort_A_Bilinear(Y_Camera *C);
		//DblImageG *Distort_A_Bilinear(Y_Camera *C);
		DblImageG* operator *=(double s);
		//DblImageG* operator /=(DblImageG* dbl, double s);
		//DblImageG* operator +(DblImageG* d, DblImageG* dbl);
	public:
		double *d_im;
		static double INVALID;
};

#endif
