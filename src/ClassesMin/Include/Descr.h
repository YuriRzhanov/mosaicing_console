#ifndef _DESCR_H
#define _DESCR_H

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <math.h>
#ifdef _MSC_VER
#include <tchar.h>
#else
#include <string.h>
#define _TCHAR char
#define _tcslen strlen
#define _tcsdup strdup
#define _tcstok_s strtok
#define _tcsnccmp strncmp
#define _tcschr strchr
#define _ftprintf_s fprintf
#define _stscanf_s sscanf
#define _fgetts fgets
#define _T(X) (X)
#define _tcscpy_s(X,Y,Z) strcpy(X,Z)
#endif
using namespace std;

// Command-line options-processing class
typedef enum { O_NONE, O_INT, O_FLT, O_DBL, O_STR, O_BOOL, O_CHAR } O_TYPE;

class OptStringType  {
	public:	
		OptStringType() {Init(O_NONE, nullptr, -1, nullptr, nullptr, nullptr);}
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, bool *_value, bool _deflt);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, _TCHAR *_value, _TCHAR _deflt);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt,
			int *_value2, int _deflt2);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt,
			int *_value2, int _deflt2, int *_value3, int _deflt3);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt,
			int *_value2, int _deflt2, int *_value3, int _deflt3, int *_value4, int _deflt4);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, float *_value, float _deflt);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, float *_value, float _deflt,
			float *_value2, float _deflt2);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, double *_value, double _deflt);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, double *_value, double _deflt,
			double *_value2, double _deflt2);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, double *_value, double _deflt,
			double *_value2, double _deflt2, double *_value3, double _deflt3);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt);
		OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt,
			_TCHAR **_value2, _TCHAR *_deflt2);
		~OptStringType();
		void Init(O_TYPE _t, _TCHAR *_tag, int _args, _TCHAR *_d, void *_p, void *_p2 = nullptr, void *_p3 = nullptr, void *_p4 = nullptr);
	public:	
		_TCHAR *tag;
		int args;
		_TCHAR *descr;
		O_TYPE o_type;
		int set;
		void *ptr1, *ptr2, *ptr3, *ptr4;
		bool b_deflt1;
		_TCHAR c_deflt1;
		int i_deflt1, i_deflt2, i_deflt3, i_deflt4;
		float f_deflt1, f_deflt2;
		double d_deflt1, d_deflt2, d_deflt3;
		_TCHAR *s_deflt1, *s_deflt2;
};


class OptStrings {
	public:
		OptStrings();
		~OptStrings();
		void PrintHelp();
		int CheckCorrectness(_TCHAR* tag, int args, int expected_args);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, bool *value, bool deflt = false);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, _TCHAR *value, _TCHAR deflt = '\0');
		void Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt,
			int *value2, int deflt2);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt,
			int *value2, int deflt2, int *value3, int deflt3);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt,
			int *value2, int deflt2, int *value3, int deflt3, int *value4, int deflt4);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, float *value, float deflt);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, float *value, float deflt,
			float *value2, float deflt2);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, double *value, double deflt);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, double *value, double deflt,
			double *value2, double deflt2);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, double *value, double deflt,
			double *value2, double deflt2, double *value3, double deflt3);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt);
		void Case(_TCHAR *tag, int args, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt,
			_TCHAR **value2, _TCHAR *deflt2);
		int Process(int *argc, _TCHAR ***argv);
	private:
		_TCHAR *getopt_Y(int argc, _TCHAR *argv[], _TCHAR *optstring, _TCHAR* bad_option);
		int NumSeparators(_TCHAR* stg, _TCHAR separator);
	private:
		int n_opts, n_optstring, help, optind;
		OptStringType *opts_stg[1024];
		_TCHAR optstring[256], *optarg;
	public:
		static int error;
		static _TCHAR error_stg[256];
};



// Description-file processing class
class DFileType {
	public:
		DFileType() {Init(O_NONE, 0, -1, nullptr, nullptr, nullptr);}
		DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt);	// One int
		DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt,
			int *_value2, int _deflt2);									// Two ints
		DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt,
			int *_value2, int _deflt2, int *_value3, int _deflt3);		// Three ints
		DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt,
			int *_value2, int _deflt2, int *_value3, int _deflt3,
			int *_value4, int _deflt4);									// Four ints
		DFileType(_TCHAR *_tag, _TCHAR *_descr, float *_value, float _deflt);	// One float
		DFileType(_TCHAR *_tag, _TCHAR *_descr, float *_value, float _deflt,
			float *_value2, float _deflt2);									// Two floats
		DFileType(_TCHAR *_tag, _TCHAR *_descr, double *_value, double _deflt);	// One double
		DFileType(_TCHAR *_tag, _TCHAR *_descr, double *_value, double _deflt,
			double *_value2, double _deflt2);									// Two doubles
		DFileType(_TCHAR *_tag, _TCHAR *_descr, double *_value, double _deflt,
			double *_value2, double _deflt2, double *_value3, double _deflt3);	// Three doubles
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt);	// One string
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt,
			_TCHAR **_value2, _TCHAR *_deflt2);									// Two strings
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt,
			_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3);	// Three strings
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt,
			_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3,
			_TCHAR **_value4, _TCHAR *_deflt4);									// Four strings
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt,
			_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3,
			_TCHAR **_value4, _TCHAR *_deflt4, _TCHAR **_value5, _TCHAR *_deflt5);	// Five strings
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt,
			_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3,
			_TCHAR **_value4, _TCHAR *_deflt4, _TCHAR **_value5, _TCHAR *_deflt5,
			_TCHAR **_value6, _TCHAR *_deflt6);									// Six strings
		DFileType(_TCHAR *_tag, _TCHAR *_descr, bool *_value, bool _deflt);	// One bool
		DFileType(_TCHAR *_tag, _TCHAR *_descr, bool *_value, bool _deflt,
			bool *_value2, bool _deflt2);									// Two bool
		DFileType(_TCHAR *_tag, _TCHAR *_descr, bool *_value, bool _deflt,
			bool *_value2, bool _deflt2, bool *_value3, bool _deflt3);		// Three bool
		DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR *_value, _TCHAR _deflt);	// One _TCHAR
		~DFileType();
		void Init(O_TYPE _t, _TCHAR *_tag, int _args, _TCHAR *_d, void *_p, 
			void *_p2 = nullptr, void *_p3 = nullptr, void *_p4 = nullptr, 
			void *_p5 = nullptr, void *_p6 = nullptr);
	public:	
		_TCHAR *tag;
		int args;
		_TCHAR *descr;
		O_TYPE o_type;
		int set;
		void *ptr1, *ptr2, *ptr3, *ptr4, *ptr5, *ptr6;
		int i_deflt1, i_deflt2, i_deflt3, i_deflt4;
		float f_deflt1, f_deflt2, f_deflt3;
		double d_deflt1, d_deflt2, d_deflt3;
		_TCHAR *s_deflt1, *s_deflt2, *s_deflt3, *s_deflt4, *s_deflt5, *s_deflt6;
		bool b_deflt1, b_deflt2, b_deflt3;
		_TCHAR c_deflt1, c_deflt2, c_deflt3;
};

class DFile {
	public:
		// If arg is not given (n_arg=1), prompt for use of default_file.
		// If arg is given (n_arg=2), don't prompt, just use.
		DFile(_TCHAR *file_to_use, _TCHAR *default_file, int n_arg=1);
		~DFile();
		void Reg(_TCHAR *tag, _TCHAR *descr, int *value, int deflt) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt); }
		void Reg(_TCHAR *tag, _TCHAR *descr, int *value, int deflt,
				int *value2, int deflt2) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, 
				value2, deflt2); }
		void Reg(_TCHAR *tag, _TCHAR *descr, int *value, int deflt,
				int *value2, int deflt2, int *value3, int deflt3) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, 
				value2, deflt2, value3, deflt3); }
		void Reg(_TCHAR *tag, _TCHAR *descr, int *value, int deflt,
				int *value2, int deflt2, int *value3, int deflt3, int *value4, int deflt4) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, 
				value2, deflt2, value3, deflt3, value4, deflt4); }
		void Reg(_TCHAR *tag, _TCHAR *descr, float *value, float deflt) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt); }
		void Reg(_TCHAR *tag, _TCHAR *descr, float *value, float deflt,
				float *value2, float deflt2) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2); }
		void Reg(_TCHAR *tag, _TCHAR *descr, double *value, double deflt) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt); }
		void Reg(_TCHAR *tag, _TCHAR *descr, double *value, double deflt,
				double *value2, double deflt2) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2); }
		void Reg(_TCHAR *tag, _TCHAR *descr, double *value, double deflt,
				double *value2, double deflt2, double *value3, double deflt3) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, 
				value2, deflt2, value3, deflt3); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt,
				_TCHAR **value2, _TCHAR *deflt2) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt,
				_TCHAR **value2, _TCHAR *deflt2, _TCHAR **value3, _TCHAR *deflt3) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2,
				value3, deflt3); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt,
				_TCHAR **value2, _TCHAR *deflt2, _TCHAR **value3, _TCHAR *deflt3, 
				_TCHAR **value4, _TCHAR *deflt4) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2,
				value3, deflt3, value4, deflt4); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt,
				_TCHAR **value2, _TCHAR *deflt2, _TCHAR **value3, _TCHAR *deflt3, 
				_TCHAR **value4, _TCHAR *deflt4, _TCHAR **value5, _TCHAR *deflt5) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2,
				value3, deflt3, value4, deflt4, value5, deflt5); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt,
				_TCHAR **value2, _TCHAR *deflt2, _TCHAR **value3, _TCHAR *deflt3, 
				_TCHAR **value4, _TCHAR *deflt4, _TCHAR **value5, _TCHAR *deflt5,
				_TCHAR **value6, _TCHAR *deflt6) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, value2, deflt2,
				value3, deflt3, value4, deflt4, value5, deflt5, value6, deflt6); }
		void Reg(_TCHAR *tag, _TCHAR *descr, bool *value, bool deflt) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt); }
		void Reg(_TCHAR *tag, _TCHAR *descr, bool *value, bool deflt,
				bool *value2, bool deflt2) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, 
				value2, deflt2); }
		void Reg(_TCHAR *tag, _TCHAR *descr, bool *value, bool deflt,
				bool *value2, bool deflt2, bool *value3, bool deflt3) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt, 
				value2, deflt2, value3, deflt3); }
		void Reg(_TCHAR *tag, _TCHAR *descr, _TCHAR *value, _TCHAR deflt) {
			regs[n_regs++] = new DFileType(tag, descr, value, deflt); }
		int Process();
		void PrintHelp();
		void Print(); // for debugging
		void PrepareDescrFile(_TCHAR *name);
	private:
		int NumSeparators(_TCHAR* stg, _TCHAR separator);
	private:
		_TCHAR *file, *def_file;
		int n_regs, help, fail, use_default_file, need_help, create_template_file;
		DFileType *regs[1024];
		static _TCHAR fileToUse[1024], defaultFile[1024], buf1[64], buf2[64], buf3[64], buf4[64], buf5[64], buf6[64];
};

#endif
