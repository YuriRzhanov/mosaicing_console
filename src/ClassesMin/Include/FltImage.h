#ifndef _FLTIMAGE_H
#define _FLTIMAGE_H

// Since September 2006 FltImage's have header and Intel architecture becomes native (does not require byte-swapping)

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <memory.h>

#include "defs.h"

#define use_namespace
#define WANT_STREAM                  // include.h will get stream fns
#define WANT_MATH                    // include.h will get math fns
                                     // newmatap.h will get include.h
#include "newmatap.h"                // need matrix applications
//#include "newmatio.h"                // need matrix output routines
//using namespace NEWMAT;              // access NEWMAT namespace

#define MAGIC_FLT "YFLT"
#define MAGIC_FLT_SIZE 4

#include "BMPImage.h"

typedef struct {
	char magic[MAGIC_FLT_SIZE]; // YFLT
	int w, h, z; // width/height/numchannels
	int ranges_set, wordsize; // wordsize is 4 for float, 8 for double
	float v_min, v_max; // ranges
} FltHeader;

class FltImage : public Y_Image {
	public:
		FltImage() : Y_Image() { z = 0; }
		FltImage(int _w, int _h) : Y_Image(_w, _h) { z = 0; }
		FltImage(const char *file) {
			Read(file); 
		}
		~FltImage() {}
		int Read(const char *file, bool old = false) {
			name = file;
			int status = -1;
			FILE *fp;
			if(fopen_s(&fp, file, "rb")) {
				fprintf(stderr, "Cannot open \"%s\".\n", file);
				return(-1);
			} else {
				status = ReadHeader(fp);
				fclose(fp);
				IsColour(); // sets "z"
			}
			return(status);
		}
		int IsColour() { // Depends on file size, if read. If created, then 'z' is set already.
			if(z==0) { // go through determination process
				struct __stat64 buf;
				int result = _stat64(name.c_str(), &buf);
				if(result) // Cannot get info?
					return(0);
				if(buf.st_size==(sizeof(FltHeader)+w*h*sizeof(float)))
					z = 1;
				else if(buf.st_size==(sizeof(FltHeader)+3*w*h*sizeof(float)))
					z = 3;
			}
			return((z==3) ? 1 : 0);
		}
		void SetFocalLength(double f_length) {
			focal_length = f_length;
		}
	protected:
		void FillHeader() {
			memcpy(header.magic, MAGIC_FLT, MAGIC_FLT_SIZE);
			header.w = w; header.h = h; header.z = z;
			header.ranges_set = ranges_set; header.wordsize = sizeof(float);
			if(ranges_set) {
				header.v_min = v_min; header.v_max = v_max;
			} else {
				header.v_min = 0.0f; header.v_max = 0.0f;
			}
		}
		void UnfillHeader() {
			w = header.w; h = header.h; z = header.z;
			ranges_set = header.ranges_set;
			if(ranges_set) {
				v_min = header.v_min; v_max = header.v_max;
			} else {
				v_min = 0.0f; v_max = 0.0f;
			}
		}
		int ReadHeader(FILE* fi) {
			FltHeader _header;
			if(fread(&_header, sizeof(FltHeader), 1, fi)!=1) { 
				fprintf(stderr, "Read \"%s\" fails.\n", name.c_str()); return(-1);
			}
			if(memcmp(_header.magic, MAGIC_FLT, MAGIC_FLT_SIZE)!=0) {
				fprintf(stderr, "Wrong magic in \"%s\".\n", name.c_str()); return(-1);
			}
			if(!endian) { // Convert to LSBF-order
				Y_Utils::Swap4Byte((u_char *) &_header.w);
				Y_Utils::Swap4Byte((u_char *) &_header.h);
				Y_Utils::Swap4Byte((u_char *) &_header.z);
				Y_Utils::Swap4Byte((u_char *) &_header.ranges_set);
				Y_Utils::Swap4Byte((u_char *) &_header.wordsize);
				Y_Utils::Swap4Byte((u_char *) &_header.v_min);
				Y_Utils::Swap4Byte((u_char *) &_header.v_max);
			}
			memcpy(&header, &_header, sizeof(FltHeader));
			UnfillHeader(); // fill in member vars from header
			return(0);
		}
		int WriteHeader(FILE* fi) {
			FillHeader();
			FltHeader _header;
			memcpy(&_header, &header, sizeof(FltHeader));
			if(!endian) { // Convert to LSBF-order
				Y_Utils::Swap4Byte((u_char *) &_header.w);
				Y_Utils::Swap4Byte((u_char *) &_header.h);
				Y_Utils::Swap4Byte((u_char *) &_header.z);
				Y_Utils::Swap4Byte((u_char *) &_header.ranges_set);
				Y_Utils::Swap4Byte((u_char *) &_header.wordsize);
				Y_Utils::Swap4Byte((u_char *) &_header.v_min);
				Y_Utils::Swap4Byte((u_char *) &_header.v_max);
			}
			if(fwrite(&_header, sizeof(FltHeader), 1, fi)!=1) { 
				fprintf(stderr, "Write \"%s\" fails.\n", name.c_str()); return(-1);
			}
			return(0);
		}
		int ReadDblHeader_old(FILE *fi) {
			int _w, _h;
			if(fread(&_w, sizeof(int), 1, fi)!=1) { 
				fprintf(stderr, "Read \"%s\" fails.\n", name.c_str()); return(-1);
			}
			if(fread(&_h, sizeof(int), 1, fi)!=1) { 
				fprintf(stderr, "Read \"%s\" fails.\n", name.c_str()); return(-1);
			}
			if(endian) { // Convert to MSBF-order
				Y_Utils::Swap4Byte((u_char *) &_w);
				Y_Utils::Swap4Byte((u_char *) &_h);
			}
			w = _w; h = _h;
			ranges_set = 0; v_min = v_max = 0.0f;
			FillHeader(); // To set header for old DblImages
			return(0);
		}
		int WriteFltHeader_old(FILE *fo) {
			int _w = w, _h = h;
			if(endian) { // Convert to MSBF-order
				Y_Utils::Swap4Byte((u_char *) &_w);
				Y_Utils::Swap4Byte((u_char *) &_h);
			}
			if(fwrite(&_w, sizeof(int), 1, fo)!=1) {
				fprintf(stderr, "Write \"%s\" fails.\n", name.c_str()); return(-1);
			}
			if(fwrite(&_h, sizeof(int), 1, fo)!=1) {
				fprintf(stderr, "Write \"%s\" fails.\n", name.c_str()); return(-1);
			}
			return(0);
		}
	public:
		void PrintFltHeader(FILE *fo) {
			fprintf(fo, "Type: double %s\n", (z==1) ? "greyscale" : "colour");
			fprintf(fo, "Width: %d\n", w);
			fprintf(fo, "Height: %d\n", h);
		}
	public:
		FltHeader header;
		unsigned int z;
		int ranges_set;
		int even_zoom; // If set to 1, zoomed image always has even dimensions 
		float v_min, v_max; // Ranges for greyscale conversion
		std::string name;
};

#endif
