// Homography based on NewMat10 representation of matrix.
// Matrix is 3*3 (not 8-long as old homography), but externally only 8 elements are saved

#ifndef _HMGR_H_
#define _HMGR_H_

#define use_namespace
#define WANT_STREAM                  // include.h will get stream fns
#define WANT_MATH                    // include.h will get math fns
                                     // newmatap.h will get include.h

#include "newmatap.h"                // need matrix applications
#include "newmatio.h"                // need matrix output routines
//using namespace NEWMAT;              // access NEWMAT namespace

//#include "VMUtils.h"
//#include "RAR.h"
//#include "BMPImageG.h"
#include "BMPImageC.h"

using namespace std;
//#include "PolygonClipping/PolygonClipping.h"
//using namespace PolygonClipping;

#define HMGR_EPS	1.e-10

class Hmgr {
	public:
		NEWMAT::Matrix h;
	public:
		Hmgr() { Init(); }
		Hmgr(double *d) { 
			Init(); Set(d); 
		}
		Hmgr(const NEWMAT::Matrix& M) { h = M; }
		Hmgr(double a00, double a10, double a20,
		     double a01, double a11, double a21,
			 double a02, double a12, double a22) {
				 Init(); Set(a00, a10, a20, a01, a11, a21, a02, a12, a22); 
		}
		~Hmgr() { 
			int k = 0; 
		}
		void Init() {
			h.ReSize(3,3);
			Unit();
		}
		void Zero() { 
			for(int i = 0; i<3; i++) {
				for(int j = 0; j<3; j++)
					h(j+1,i+1) = 0.0; 
			}
		}
		void Unit() { 
			for(int i = 0; i<3; i++) {
				for(int j = 0; j<3; j++)
					h(j+1,i+1) = 0.0;
				h(i+1,i+1) = 1.0;
			}
		}
		bool IsUnit();
		bool IsSingular();
		void Set(double *d);
		void Set(double a00, double a10, double a20,
				 double a01, double a11, double a21,
				 double a02, double a12, double a22);
		int Sane(); // Check normalcy
		Hmgr Invert();
		Hmgr operator =(const Hmgr& v) {
			h = v.h;
			return(*this);
		}

		double DifEuclidean(const Hmgr& hm);
		void Save(FILE *fp, char *name = nullptr);
		void Save(ofstream* of, char *name = nullptr);
		void Read(FILE *fp, char *name);
		int SaveStraight(const char *name);
		void SaveBin(FILE *fp, const char *name = nullptr);
		void ReadBin(FILE *fp, char *name);
		int SaveBinFile(char *name);
		int IsRigidAffine();
		Y_Rect<int> FrameRect(Y_Rect<int> r, double focal_length);
		double Overlap(BMPImage* im1, BMPImage* im2); // Pixel-based. Replacement for OptFramework::Overlap()
		// Polygon for this image with this homography. Optional shift by 'start'.
		//CPolygon* Polygon(Y_Image* im, Coord2* start = nullptr);
		//double OverlapPoly(Y_Image* im1, Y_Image* im2); // Polygon-based. Requires only frame sizes
		double AverageError(BMPImageG* im1, BMPImageG* im2, int verbose = 0); // Replacement for OptFramework::TransError()
		double AverageError(BMPImageC* im1, BMPImageC* im2, int verbose = 0); // Replacement for OptFramework::TransError()
		// Conversion from Euler params to Homography
		static Hmgr FromEuler(double focal_length, double height, 
			double pitch, double roll, double yaw, char *name = nullptr);
		static Hmgr FromEuler(double focal_length, double height, 
			double pitch, double roll, double yaw, 
			double Xs, double Ys, char *name = nullptr);
		static Hmgr FromEuler(double focal_length, double height, 
			double pitch, double roll, double yaw, 
			double Xs, double Ys, double nx, double nz,
			char *name = nullptr);
		static Hmgr FromEuler_Unscaled(double Z, double pitch, double roll, double yaw, double Xs, double Ys, 
			double focal_length, char *name = nullptr);
		// From rigid affine conventional record to Homography: delegated to RARecord (August 2007)
		//static Hmgr FromRecord(const RARecord& rec, double focal_length);
		// Conversion from Homography to Euler params
	protected:
		double T2E_Error(NEWMAT::ColumnVector Sol, NEWMAT::ColumnVector& E, double focal_length);
		double T2RA_Error(NEWMAT::ColumnVector Sol, NEWMAT::ColumnVector& E);
	public:
		double PerspectiveParams(double focal_length, double& zoom, double& P, double& R, 
			double& Y, double& dx, double& dy);
		double RigidAffineParams(double& zoom, double& Y, double& dx, double& dy);
		Coord2 Map(Coord2& c); // Instead of having in Coord2 class
};

Hmgr operator *(Hmgr& a, Hmgr& b);
Hmgr operator /(Hmgr& a, Hmgr& b); // a*inv(b)
Hmgr operator +(const Hmgr& a, const Hmgr& b);
Hmgr operator -(const Hmgr& a, const Hmgr& b);
//Hmgr& operator =(const Hmgr& v);

// --- Input & Output -----------------------------------------------------
ostream	 &operator << (ostream &s, const Hmgr &v);
//istream	 &operator >> (istream &s, Hmgr &v);

#endif
