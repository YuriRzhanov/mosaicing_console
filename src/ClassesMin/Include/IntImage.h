#ifndef _INTIMAGE_H
#define _INTIMAGE_H

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <memory.h>

#include "defs.h"

#define use_namespace
#define WANT_STREAM                  // include.h will get stream fns
#define WANT_MATH                    // include.h will get math fns
                                     // newmatap.h will get include.h
#include "newmatap.h"                // need matrix applications
//#include "newmatio.h"                // need matrix output routines
//using namespace NEWMAT;              // access NEWMAT namespace

#define MAGIC_INT "YINT"
#define MAGIC_INT_SIZE 4

#include "BMPImage.h"

typedef struct {
	char magic[MAGIC_INT_SIZE]; // YINT
	int w, h, z; // width/height/numchannels
	int ranges_set, wordsize; // wordsize=sizeof(int)
	int v_min, v_max; // ranges
} IntHeader;

class IntImage : public Y_Image {
	public:
		IntImage() : Y_Image() { z = 0; }
		IntImage(int _w, int _h) : Y_Image(_w, _h) { z = 0; v_min = v_max = 0; }
		IntImage(const char *file) {
			Read(file); 
		}
		~IntImage() {}
		int Read(const char *file, bool old = false) {
			name = file;
			FILE *fp;
			if(fopen_s(&fp, file, "rb")) {
				sprintf_s(error, 256, "Cannot open \"%s\".\n", file);
				return(-1);
			}
			int status = ReadHeader(fp);
			if(fp) fclose(fp);
			IsColour(); // sets "z"
			return(status);
		}
		int IsColour() { // Depends on file size, if read. If created, then 'z' is set already.
			if(z==0) { // go through determination process
				struct __stat64 buf;
				int result = _stat64(name.c_str(), &buf);
				if(result) // Cannot get info?
					return(0);
				if(buf.st_size==(sizeof(IntHeader)+w*h*sizeof(int)))
					z = 1;
				else if(buf.st_size==(sizeof(IntHeader)+3*w*h*sizeof(int)))
					z = 3;
			}
			return((z==3) ? 1 : 0);
		}
		void SetFocalLength(double f_length) {
			focal_length = f_length;
		}
	protected:
		void FillHeader() {
			memcpy(header.magic, MAGIC_INT, MAGIC_INT_SIZE);
			header.w = w; header.h = h; header.z = z;
			header.ranges_set = ranges_set; header.wordsize = sizeof(int);
			if(ranges_set) {
				header.v_min = v_min; header.v_max = v_max;
			} else {
				header.v_min = 0; header.v_max = 0;
			}
		}
		void UnfillHeader() {
			w = header.w; h = header.h; z = header.z;
			ranges_set = header.ranges_set;
			if(ranges_set) {
				v_min = header.v_min; v_max = header.v_max;
			} else {
				v_min = 0; v_max = 0;
			}
		}
		int ReadHeader(FILE* fi) {
			IntHeader _header;
			if(fread(&_header, sizeof(IntHeader), 1, fi)!=1) { 
				sprintf_s(error, 256, "Read \"%s\" fails.\n", name.c_str()); return(-1);
			}
			if(memcmp(_header.magic, MAGIC_INT, MAGIC_INT_SIZE)!=0) {
				sprintf_s(error, 256, "Wrong magic in \"%s\".\n", name.c_str()); return(-1);
			}
			if(!endian) { // Convert to LSBF-order
				Y_Utils::Swap4Byte((u_char *) &_header.w);
				Y_Utils::Swap4Byte((u_char *) &_header.h);
				Y_Utils::Swap4Byte((u_char *) &_header.z);
				Y_Utils::Swap4Byte((u_char *) &_header.ranges_set);
				Y_Utils::Swap4Byte((u_char *) &_header.wordsize);
				Y_Utils::Swap4Byte((u_char *) &_header.v_min);
				Y_Utils::Swap4Byte((u_char *) &_header.v_max);
			}
			memcpy(&header, &_header, sizeof(IntHeader));
			UnfillHeader(); // fill in member vars from header
			return(0);
		}
		int WriteHeader(FILE* fi) {
			FillHeader();
			IntHeader _header;
			memcpy(&_header, &header, sizeof(IntHeader));
			if(!endian) { // Convert to LSBF-order
				Y_Utils::Swap4Byte((u_char *) &_header.w);
				Y_Utils::Swap4Byte((u_char *) &_header.h);
				Y_Utils::Swap4Byte((u_char *) &_header.z);
				Y_Utils::Swap4Byte((u_char *) &_header.ranges_set);
				Y_Utils::Swap4Byte((u_char *) &_header.wordsize);
				Y_Utils::Swap4Byte((u_char *) &_header.v_min);
				Y_Utils::Swap4Byte((u_char *) &_header.v_max);
			}
			if(fwrite(&_header, sizeof(IntHeader), 1, fi)!=1) { 
				sprintf_s(error, 256, "Write \"%s\" fails.\n", name.c_str()); return(-1);
			}
			return(0);
		}
	public:
		void PrintDblHeader(FILE *fo) {
			fprintf(fo, "Type: int %s\n", (z==1) ? "greyscale" : "colour");
			fprintf(fo, "Width: %d\n", w);
			fprintf(fo, "Height: %d\n", h);
		}
	public:
		IntHeader header;
		unsigned int z;
		int ranges_set;
		int even_zoom; // If set to 1, zoomed image always has even dimensions 
		int v_min, v_max; // Ranges for greyscale conversion
		std::string name;
};

#endif
