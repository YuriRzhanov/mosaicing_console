#ifndef _INTIMAGEG_H
#define _INTIMAGEG_H

class BMPImageG;
class BMPImageC;

#include "IntImage.h"
#include "BMPImageG.h"
#include "BMPImageC.h"

class IntImageG : public IntImage {
	public:
		IntImageG(int _w, int _h, int *data = nullptr);
		IntImageG(int _w, int _h, int init);
		IntImageG(IntImageG* i);
		IntImageG(BMPImageG *img, int hasInvalid = 0);
			// if 'hasInvalid' is set to 1, BMPImageG has invalid pixels marked with 0's
		IntImageG(BMPImageC *img, int hasInvalid = 0);
			// if 'hasInvalid' is set to 1, BMPImageC has invalid pixels INVALID_RGB
		IntImageG(const char *name);
		~IntImageG() { 
			if(i_im) {
				delete [] i_im;
				i_im = nullptr;
			}
			fail = 1;
		}
		void SetRanges(int _min, int _max) { v_min=_min; v_max=_max; ranges_set=1; }
		IntImageG *Copy();
		void Copy(IntImageG *img);
		int Read(const char *name);
		int Save(const char *name);
		int SaveASCII(const char* file, bool saveIndexes = false);
		// Reset to zeroes
		void SetBlack() { memset(i_im, 0, w*h*sizeof(int)); }
		void SetColour(int v);
		bool set_pixel(Point2<int> P, int p) {
			return(set_pixel(P.X(), P.Y(), p)); }
		bool set_pixel(int pos, int p) {
			if(!Y_Image::valid(pos)) return(false);
			i_im[pos] = p; return(true); }
		bool set_pixel(int x, int y, int p) {
			if(!Y_Image::valid(x,y)) return(false);
			i_im[y*w+x] = p; return(true); }
		bool set_pixel(int x, int y, double p) {
			if(!Y_Image::valid(x,y)) return(false);
			i_im[y*w+x] = (int)(p+0.5); return(true); }
		int get_pixel(Point2<int> P) {
			return(get_pixel(P.X(), P.Y())); }
		int get_pixel(int pos, int *valid = nullptr) {
			if(!Y_Image::valid(pos)) {
				if(valid) *valid = 0;
				return(INVALID);
			}
			if(valid) *valid = 1;
			return(i_im[pos]); }
		int get_pixel(int x, int y) {
			if(!Y_Image::valid(x,y)) return(INVALID);
			return(i_im[y*w+x]); }
		int get_pixel(int x, int y, int* valid) {
			if(!Y_Image::valid(x,y)) {
				if(valid) *valid = 0;
				return(INVALID);
			}
			if(valid) *valid = 1;
			return(i_im[y*w+x]); }
		double get_pixel_D2(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D2(P.X(), P.Y(), valid)); }
		double get_pixel_D2(double x, double y, int *valid = nullptr) {
			return(Bilinear(x, y, valid)); }
		double get_pixel_D3(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D3(P.X(), P.Y(), valid)); }
		double get_pixel_D3(double x, double y, int *valid = nullptr) {
			return(Bicubic(x, y, valid)); }
		// operator versions: indices start from 0
		int& operator () (int x, int y) {
			if(!Y_Image::valid(x,y)) return(INVALID);
			return(i_im[y*w+x]);
		}
		int PutAt(IntImageG *p, int x, int y);
		int border(int x, int y);
		int valid(int x, int y) { return(Y_Image::valid(x, y)); }
		int valid(double x, double y);
		int valid(Point2<double> P) { return(valid(P.x, P.y)); }
		void SetInvalid() { for(int j = 0; j<w*h; j++) i_im[j] = INVALID; }
		IntImageG** Split(int factor);
		int SaveBMP(const char *name, int rescale=0, int ignore_value=INVALID);
		int SaveBMPRange(const char *name, int min, int max);
		int SaveBMPRainbow(const char *name, int type = 1);
		BMPImageC* BMPRainbow(int type = 1);
		IntImageG *CutSubimage(int at_x, int at_y, int sw, int sh);
		IntImageG *CutSubimage(Y_Rect<int> r) { return(CutSubimage(r.x, r.y, r.w, r.h)); }
		void Crop(int at_x, int at_y, int sw, int sh);
		void Crop(Y_Rect<int> r) { return(Crop(r.x, r.y, r.w, r.h)); }
		double Bilinear(double dx, double dy, int *valid = nullptr);
		double Bicubic(double dx, double dy, int *valid = nullptr);
		// Enforce interpolation assuming that top-left corner is given
		double Bilinear(double dx, double dy, int tl_i, int tl_j);
		void Log(); // Zeroes become INVALID
		void ScaleShift(double gain, double offset);
		bool Subtract(IntImageG* pInt);
		bool Add(IntImageG* pInt);
		bool Multiply(IntImageG* pInt);
		bool Divide(IntImageG* pInt);
		bool Max(IntImageG* pInt); // per-pixel maximum values
		bool Min(IntImageG* pInt); // per-pixel minimum values
		BMPImageG *Greyscale(int rescale=0, int ignore_value=INVALID);
		BMPImageG *GreyscaleRange(int min, int max);
		void ValidateAll();
		int AverageBrightness(int invalid_clr = INVALID);
		int ZoomToDest_Bilinear(int dest_w, int dest_h);
		int Zoom_Bilinear(double zoom, int flags = DO_ALL);
		int ZoomToDest_Bicubic(int dest_w, int dest_h);
		int Zoom_Bicubic(double zoom, int flags = DO_ALL);
		void RotateQuarter(int nr_of_quarters); // 1: by pi/2, 2: by pi, 3: by -pi/2
		int Rotate_Bilinear(double angle_degrees);
		int Rotate_Bicubic(double angle_degrees);
		void Blur(int kernel, int times = 1); // 'times' - number of times
		// Finds min and max, if value is ignore_value, it is ignored
		void MinMax(int& v_min, int& v_max, int ignore_value=INVALID);
		// ... also returns location of extremums
		void MinMax(int& v_min, Point2<int>& p_min, int& v_max, Point2<int>& p_max, int ignore_value=INVALID);
		void Scale(int min, int max); 
		void CutOffPercentile(int& min, int& max, double percentile);
		bool LocalMaximum_4(int i, int j);
		bool LocalMaximum_8(int i, int j);
		IntImageG* operator *=(int s);
	public:
		int *i_im;
		static int INVALID;
};

#endif
