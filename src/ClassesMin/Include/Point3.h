// Class to be used instead of: Vertex3
// No relation to any image here

#pragma once

#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <assert.h>

#define WANT_STREAM                  // include.h will get stream fns
#define WANT_MATH                    // include.h will get math fns
                                     // newmatap.h will get include.h
#include "newmatap.h"                // need matrix applications
//#include "newmatio.h"                // need matrix output routines
//using namespace NEWMAT;              // access NEWMAT namespace

using namespace std;

template <class T>
class Point3 {
	public:
		Point3() {x=T(0); y=T(0); z=T(0); w=T(1); idx=-1; pData=nullptr;}
		Point3(T _x, T _y, T _z) {x=_x; y=_y; z=_z; w=T(1); idx=-1; pData=nullptr;}
		Point3(T _x, T _y, T _z, int _idx, void* _pData) {x=_x; y=_y; z=_z; w=T(1); idx=_idx; pData=_pData;}
		Point3(const Point3& p) {x=p.x; y=p.y; z=p.z; w=p.w; idx=p.idx; pData=p.pData;}
		void Set(const Point3& p) {x=p.x; y=p.y; z=p.z; w=p.w;; idx=p.idx; pData=p.pData;}
		void Set(T _x, T _y, T _z) {x=_x; y=_y; z=_z; w=T(1);}
		void Set(T _x, T _y, T _z, int _idx, void* _pData) {x=_x; y=_y; z=_z; w=T(1); idx=_idx; pData=_pData;}
		void Set(T _x, T _y, T _z, T _w) {x=_x; y=_y; z=_z; w=_w;}
		void Set(T _x, T _y, T _z, T _w, int _idx, void* _pData) {x=_x; y=_y; z=_z; w=_w; idx=_idx; pData=_pData;}
		T X() const {return(x);}
		T Y() const {return(y);}
		T Z() const {return(z);}
		T W() const {return(w);}
		void Copy(const Point3& p) {x=p.x; y=p.y; z=p.z; w=p.w; idx=p.idx; pData=p.pData;}
		Point3 Copy() {return(Point3(*this));}
		Point3<double> Double() { return(Point3<double>((double)x,(double)y,(double)z,double(w))); }
		Point3<float> Float() { return(Point3<float>((float)x,(float)y,float(z),float(w))); }
		bool Equal(const Point3& p) {return(x==p.x && y==p.y && z==p.z);}
		//void Negate(const Point3& p) {x=-p.x; y=-p.y; z=-p.z;}
		void Negate() {x=-x; y=-y; z=-z;}
		T Length() const {return(T(sqrt(x*x+y*y+z*z)));}
		int IsZero() const;
		int IsUnit() const;
		static T DotProduct(Point3& v1, Point3& v2) {
			return(v1.x*v2.x+v1.y*v2.y+v1.z*v2.z);
		}
		static Point3 CrossProduct(Point3& v1, Point3& v2) {
			return(Point3<T>(v1.y*v2.z-v2.y*v1.z, v1.z*v2.x-v2.z*v1.x, v1.x*v2.y-v2.x*v1.y));
		}
		//int IsParallel(const Point3& v) const;
		void Normalize() { T d = Length(); x/=d; y/=d; z/=d;}
		//void Scale(T s, const Point3& p) {x=s*p.x; y=s*p.y; z=s*p.z;}
		void Scale(T s) {x=s*x; y=s*y; z=s*z;}
		T Distance(const Point3& p) 
			{ return(T(sqrt((x-p.x)*(x-p.x)+(y-p.y)*(y-p.y)+(z-p.z)*(z-p.z))));}
		double DDistance(const Point3& p) 
			{ return(sqrt(double(x-p.x)*(x-p.x)+(y-p.y)*(y-p.y)+(z-p.z)*(z-p.z)));}
		friend ostream &operator<<(ostream &ostr, Point3 &p)
			{ ostr<<"("<<p.x<<","<<p.y<<","<<p.z<<")"; return(ostr); }
		// Operators
		bool operator ==(const Point3& p) {return Equal(p);}
		bool operator !=(const Point3& p) {return !Equal(p);}
		Point3 operator -() const {return(Point3(-x,-y,-z));}
		Point3 operator +(const Point3& p) const
			{ return Point3(x+p.x,y+p.y,z+p.z); }
		Point3 operator -(const Point3& p) const
			{ return Point3(x-p.x,y-p.y,z-p.z); }
		Point3 operator *(T s)
			{ return Point3(s*x,s*y,s*z); }
		Point3 operator /(T s)
			{ return Point3(x/s,y/s,z/s); }
		static Point3 CrossProd(Point3 pt1, Point3 pt2) 
			{ return Point3(pt1.y*pt2.z-pt1.z*pt2.y, pt1.z*pt2.x-pt1.x*pt2.z, pt1.x*pt2.y-pt1.y*pt2.x); }
		void Rotate(NEWMAT::Matrix& M);
		// The following does not work on Linux:
#ifndef Linux
		friend inline Point3<T> operator *(T s, const Point3<T>& p);
		friend inline Point3<T> operator *(const Point3<T>& p, T s);
		friend inline Point3<T> operator /(const Point3<T>& p, T s);
#endif
		// Assignment operators
		Point3& operator =(const Point3& p)
			{ x=(T)p.x; y=(T)p.y; z=(T)p.z; return *this; }
		Point3& operator *=(double s)
			{ x*=s; y*=s; z*=s; return *this; }
		Point3& operator /=(double s)
			{ s=1.0/s; return *this*=s; }
		Point3& operator +=(const Point3& p)
			{ x+=p.x; y+=p.y; z+=p.z; return *this; }
		Point3& operator -=(const Point3& p)
			{ x-=p.x; y-=p.y; z-=p.z; return *this; }
		//static Point3 Direction(const Point3& pc, const Point3& p);
		//static Point3 Interpolate(const Point3& pc, const Point3& p, double t);
	public:
		T x, y, z, w;
		int idx;
		void* pData;
};

template <class T>
inline Point3<T> operator *(T s, const Point3<T>& p)
	{ return Point3<T>(p.x*s, p.y*s, p.z*s); }
template <class T>
inline Point3<T> operator *(const Point3<T>& p, T s)
	{ return Point2<T>(p.x*s, p.y*s, p.z*s); }
template <class T>
inline Point3<T> operator /(const Point3<T>& p, T s)
	{ return Point3<T>(p.x/s, p.y/s, p.z/s); }


