#ifndef _RAR_H_DEFINED_
#define _RAR_H_DEFINED_

// Oct 2005: ReadRecsInfo and ReadRecs are replaced with ReadRecs, where Info could be nullptr.
//           Same for SaveRecs, etc.
// June 2006: timecode is included as a member variable

#include <stdio.h>
#include <fstream>
#include <string>

#if defined(WIN32) || defined(WIN64)
#include <strstream>
#else
#include <unistd.h>
#include <strstream.h>
#endif

#include "defs.h"
//#include "YRect.h"
#include "YUtils.h" // includes YRect.h
//#include "Coord2.h"
#include "Collocations.h" // includes Coord2.h
using namespace std;

#define RAR_FATAL -100
#define RAR_NONFATAL -1
#define UNIQUETAG(x,y) (10000*YMIN(x,y)+YMAX(x,y))

// Forced values for RARecord:  value of -9999.0 means unconstrained value
class RAForceRecord {
	public:
		RAForceRecord() { base_f_n=f_n=-1; zoom=angle=x_shift=y_shift=-9999.0; }
		RAForceRecord(RAForceRecord *fr) 
			{ base_f_n=fr->base_f_n; f_n=fr->f_n;
			zoom=fr->zoom; angle=fr->angle; x_shift=fr->x_shift; y_shift=fr->y_shift; }
		void Set(int bfn, int fn, double z, double a, double x, double y)
			{ base_f_n = bfn; f_n = fn; zoom=z; angle=a; x_shift=x; y_shift=y; }
		void Copy(RAForceRecord *fr)
			{ base_f_n=fr->base_f_n; f_n=fr->f_n;
			zoom=fr->zoom; angle=fr->angle; x_shift=fr->x_shift; y_shift=fr->y_shift; }
		void Read(FILE *fp) 
			{ char stg[256]; fgets(stg, 255, fp); 
			sscanf_s(stg, "%d<-%d %lf %lf %lf %lf", &base_f_n, &f_n, &x_shift, &y_shift, &zoom, &angle); }
		static int IsForced(double p) { return(!EPSILON_EQ(p,-9999.0)); }
	public:
		int base_f_n, f_n;
		// angle in degrees
		double zoom, angle, x_shift, y_shift;
};

class RARecord {
	public:
		RARecord();
		~RARecord();
		void Reset();
		void SetNums(int bfn, int fn) { base_f_n=bfn; f_n=fn; }
		void Set(double z, double a, double x, double y) {
			zoom = z; angle = a; x_shift = x; y_shift = y; }
		void SetSizeFocalLength(int _w, int _h, double _fl) {
			w = _w; h= _h; focal_length = _fl; }
		void SetForceRecord(RAForceRecord *fr) { force_rar = new RAForceRecord(fr); }
		void Copy(const RARecord& r);
		void Copy(RARecord* r);
		void MarkSet(char *s) { strcpy_s(mark, 8, s); }
		// Equivalence of comment and mark:
		int MarkIs(char *s) { return(strcmp(mark, s)==0); }
		// Containment of mark in comment:
		int MarkHas(char *s) { return(strstr(mark, s)!=nullptr); }
		// Check for lesser importance:
		int MarkLess(char *s) { return(mark[0]<s[0]); }
		// Add to existing mark, in front of it
		int MarkAdd(char *s);
		int MarkAdd(char s);
		bool MarkGood(); // has 'S'/'F' or doesn't have 'B'/'X'/'E' and does have 's'
		bool MarkBad(); // has 'B'/'X'/'E'
		int ReadStg(const char *stg); // Read from the string
		int ReadFileName(FILE *log); // Only name of the image file
		int ReadPrevFileName(FILE *log); // Only name of previous image file
		int Read(FILE *log); // Full record. Reads collocations line, even if there are none
			// Full record, setting hush=1 comments record out
		void Write(FILE *log, int hush = 0) {
			Write(log, f_n, base_f_n, hush); 
		}
		void Write(FILE *log, int _f_n, int _base_f_n, int hush = 0); 
		int ReadBin(FILE *log);
		int WriteBin(FILE *log, int hush = 0);
		char *WriteStg(int hush = 0);
		char *WriteBinStg(int hush = 0);
		Hmgr& GetHmgr(double _fl = 0.0); // Moved here from Hmgr class
		void SetCollocations(Collocations* pCol);
		Collocations* GetCollocations();
		int ReadCollocations(const char* stg);
		char* WriteCollocations();
		void WriteCollocations(FILE* fp) {
			fprintf(fp, "%s", WriteCollocations());
		}
		void DefaultCollocations();
		void Save(ofstream* of, int hush = 0);
		void Save(FILE* log, int hush = 0) { // for simplicity
			Write(log, f_n, base_f_n, hush);
		}
		void ZoomAngle(double *z, double *a) { *z=zoom; *a=angle; }
		void Shifts(double *x, double *y) { *x=x_shift; *y=y_shift; }
		void SetTimecode(char* _timecode) { memcpy(timecode, _timecode, strlen(_timecode)); }
		void SetWorkDir(const char *_wd) { work_dir = _wd; }
		void SetFile(const char *_file) { file = _file; }
		void SetPrevFile(const char *_prev_file) { prev_file = _prev_file; }
		const char *File() { return(file.c_str()); }
		const char *PrevFile() { return(prev_file.c_str()); }
		const char *WorkDir() { return(work_dir.c_str()); }
		const char *Path() { 
			std::string path = work_dir + "/" + file;
			return(path.c_str());
		}
		const char *PrevPath() { 
			std::string path = work_dir + "/" + prev_file;
			return(path.c_str());
		}
		double Z() { return(zoom); }
		double A() { return(angle); } // Degrees
		double X() { return(x_shift); }
		double Y() { return(y_shift); }
		int Sequential() { return(abs(f_n-base_f_n)==1); }
		friend ostream &operator<<(ostream &ostr, RARecord &v);
		static RARecord *ReadRecs(char *logfile, int *nrec, char **ppInfo = nullptr);
		static RARecord *ReadBinRecs(char *logfile, int *nrec, char **ppInfo = nullptr);
		static int SaveRecs(char *logfile, RARecord *recs, int nrec, char *pInfo = nullptr);
		static int SaveBinRecs(char *logfile, RARecord *recs, int nrec, char *pInfo = nullptr);
		static RARecord *FindRARFromFrameNumber(RARecord *recs, int nrec, int fn);
		static RARecord *FindRARFromBaseFrameNumber(RARecord *recs, int nrec, int bfn);
		static int FindOrderNumberFromFrameNumber(RARecord *recs, int nrec, int fn);
		static int FindOrderNumberFromNumbers(RARecord *recs, int nrec, int bfn, int fn);
		static RARecord *FindRARFromNumbers(RARecord *recs, int nrec, int bfn, int fn);
		static int InsertRecord(RARecord*& recs, int& nrec, RARecord* record);
		static int DeleteRecord(RARecord*& recs, int& nrec, int k);
		static int UniqueFrames(RARecord*& recs, int& nrec);
		static char* GetError() { return(buf); }
		static int AddRecordToFile(char* filename, RARecord& rec, bool insist = false);
		static int AddRecordToFile(FILE* fp, RARecord& rec, bool insist = false);
	protected:
	public:
		char mark[8];
		std::string file, prev_file, work_dir;
		char timecode[12];
		int w, h;
		double focal_length;
		int f_n, base_f_n, uniqueTag;
			// For sequential records describes
			// mapping of frame 'f_n' to the previous frame, which is
			// 'base_f_n=f_n-1'. For cross-regs, 'base_f_n' could be any number.
		// angle in degrees
		double zoom, angle, x_shift, y_shift;
		int n_col; // Number of collocations used for calculation of this transform
		Coord2* col_b, *col_d; // Collocations: base and derived
		double overlap_percent, av_error, max_error;
		RAForceRecord *force_rar;
		double PeakParams[6]; // Extent of pixels above 80% and 95% level
			// normalized by peak image size + height/average ratio
			// In case of feature matching contains maxError/meanError/stdevError/numMatches
		static char buf[1024]; // Used as error string too
};

#endif
