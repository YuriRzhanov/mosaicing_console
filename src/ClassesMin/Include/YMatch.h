// Added Dec 17, 2006

#pragma once

// T could be float/double,Y_RGB<float> or Y_RGB<double>

#include "stdio.h"
#include "Coord2.h"

class Y_Match {
	public:
		Y_Match() : num(-1), good(false), score(0.0), mark(' ')
		{
			left.w = 0; left.h = 0; left.Set(-1.0, -1.0); 
			right.w = 0; right.h = 0; right.Set(-1.0, -1.0); }
		Y_Match(const Y_Match& m) : num(m.num), good(m.good), score(m.score), mark(m.mark)
		{
			left.w = m.left.w, left.h = m.left.h; left = m.left; 
			right.w = m.right.w; right.h = m.right.h; right = m.right; }
		Y_Match(int _num) : num(_num), good(false), score(0.0), mark(' ')
		{
			left.w = 0, left.h = 0; left.Set(-1.0, -1.0); 
			right.w = 0, right.h = 0; right.Set(-1.0, -1.0); }
		void Copy(const Y_Match& m) {
			num = m.num; good = m.good; score = m.score; mark = m.mark;
			left = m.left; right = m.right;
		}
		static void SetSize(int _w, int _h) { w = _w; h = _h; }
		static void SetFocalLength(double fl) { focal_length = fl; }
		void SetLeft(double x, double y) { left.Set(x, y); left.SetSize(w, h); left.SetFocalLength(focal_length); }
		void SetRight(double x, double y) { right.Set(x, y); right.SetSize(w, h); right.SetFocalLength(focal_length); }
		void GetFromString(char *stg, int len);
		void Scale(double factor);
		int Read(FILE* fp);
		static Y_Match* ReadMatches(const char* name, int& N_matches, int& w, int& h, double focal_length = -1.0);
		static int WriteMatches(const char* name, Y_Match* pMatches, int N_matches, bool writeScore = false, int _w = 0, int _h = 0);
		static vector<Y_Match> ReadMatches(const char* name, int& w, int& h, double focal_length = -1.0);
		static int WriteMatches(const char* name, vector<Y_Match>& matches, bool writeScore = false, int _w = 0, int _h = 0);
		static int AddMatches(const char* name, vector<Y_Match> matches, char* pHeader = nullptr);
		// Compare the ZNCC correlation score.
		inline bool operator< (const Y_Match& match) const {
			return (score > match.score);
		}
		inline bool operator> (const Y_Match& match) const {
			return (score <= match.score);
		}

	public:
		int num;
		bool good;
		char mark;
		static int w, h;
		static double focal_length;
		double score;
		Coord2 left, right;
		static char buf[1024];
};

