#ifndef _VMRECT_H_DEFINED_
#define _VMRECT_H_DEFINED_

#include <stdio.h>
#include <fstream>
#include "defs.h"
#include "Point2.h"
#include "Point3.h"
using namespace std;

template <class T>
class Y_Rect {
	public:
		Y_Rect() {x=0;y=0;w=0;h=0;}
		Y_Rect(T _x, T _y, T _w, T _h) {x=_x;y=_y;w=_w;h=_h;}
		void Set(T _x, T _y, T _w, T _h) {x=_x;y=_y;w=_w;h=_h;}
		int Inside(T i, T j) 
			{return(i>=x && i<(x+w) && j>=y && j<(y+h));}
		int Inside(Point2<int> v) 
			{return(v.X()>=x && v.X()<(x+w) && v.Y()>=y && v.Y()<(y+h));}
		int Inside(Point2<double> v) 
			{return(v.X()>=x && v.X()<(x+w) && v.Y()>=y && v.Y()<(y+h));}
		int Inside(Point2<T> v[], int n)
			{
				for(int i  = 0; i<n; i++) {
					if(Inside(c[i].X(), c[i].Y()))
						return(1);
				}
				return(0);
			}
		int Inside(Y_Rect<T>& R)
			{ // return 1, if all conditions are true, and this is fully inside R
				return(x>=R.x && y>=R.y && (x+w)<=(R.x+R.w) && (y+h)<=(R.y+R.h));
			}
		int Overlap(Y_Rect<T>& R) // return 1, if overlap exists
			{
				if((x+w)<R.x || x>(R.x+R.w)) return(0);
				if((y+h)<R.y || y>(R.y+R.h)) return(0);
				return(1);
			}
		void Encompass(T i, T j)
			{
				x = YMIN(x, i); w = YMAX(w, i); 
				y = YMIN(y, j); h = YMAX(h, j); 
			}
		void StartEncompassing()
			{
				if(typeid(T)==typeid(int)) {
					x = y = INT_MAX ;
					w = h = -INT_MAX ;
				} else if(typeid(T)==typeid(float)) {
					x = y = FLT_MAX ;
					w = h = -FLT_MAX ;
				} else if(typeid(T)==typeid(double)) {
					x = y = DBL_MAX ;
					w = h = -DBL_MAX ;
				} else {
					x = y = LONG_MAX ;
					w = h = -LONG_MAX ;
				}
			}
		void Encompass(Point2<T> v) { Encompass(v.X(), v.Y()); }
		void FinishEncompassing() {
			w -= x; h -= y;
		}		
		// Does not work on Linux
#ifndef Linux
		friend ostream &operator<<(ostream &ostr, Y_Rect<T>& r);
#endif
		Y_Rect& operator =(const Y_Rect& r) 
			{x=r.x; y=r.y; w=r.w; h=r.h; return *this;}
		bool operator ==(const Y_Rect& r) {
			//if(SameType<T,int>::result==1)
			if(typeid(T)==typeid(int))
				return(x==r.x && y==r.y && w==r.w && h==r.h);
			else
				return(EPSILON_EQ((double)x,r.x) && EPSILON_EQ((double)y,r.y) && EPSILON_EQ((double)w,r.w) && EPSILON_EQ((double)h,r.h));
		}
		bool operator !=(const Y_Rect& r) {
			//if(SameType<T,int>::result==1)
			if(typeid(T)==typeid(int))
				return(x!=r.x || y!=r.y || w!=r.w || h!=r.h);
			else
				return(!EPSILON_EQ((double)x,r.x) || !EPSILON_EQ((double)y,r.y) || !EPSILON_EQ((double)w,r.w) || !EPSILON_EQ((double)h,r.h));
		}
		void Print(FILE *fp) {
			//if(SameType<T,int>::result==1)
			if(typeid(T)==typeid(int))
					fprintf(fp, "(X=%d,Y=%d)->[W=%d,H=%d]\n", x, y, w, h);
				else
					fprintf(fp, "(X=%.2lf,Y=%.2lf)->[W=%.2lf,H=%.2lf]\n", 
						(double)x, (double)y, (double)w, (double)h);
		}
	private:
	public:
		T x, y, w, h;
};

#ifndef Linux
template <class T>
ostream &operator<<(ostream &ostr, Y_Rect<T>& r) 
{ 
	ostr<<"(X="<<r.x<<",Y="<<r.y<<")->"<<"[W="<<r.w<<",H="<<r.h<<"]"; 
	return(ostr); 
}
#endif

// Equivalent of Y_Rect in 3D
template <class T>
class Y_Volume {
	public:
		Y_Volume() {x=0;y=0;z=0;w=0;h=0;d=0;}
		Y_Volume(T _x, T _y, T _z, T _w, T _h, T _d) {x=_x;y=_y;z=_z;w=_w;h=_h;d=_d;}
		void Set(T _x, T _y, T _z, T _w, T _h, T _d) {x=_x;y=_y;z=_z;w=_w;h=_h;d=_d;}
		int Inside(T i, T j, T k) 
			{return(i>=x && i<(x+w) && j>=y && j<(y+h) && k>=z && k<(z+d));}
		int Inside(Point3<int> v) {
			return(v.x>=x && v.x<(x+w) && v.y>=y && v.y<(y+h) && v.z>=z && v.z<(z+d));
		}
		int Inside(Point3<double> v) {
			return(v.x>=x && v.x<(x+w) && v.y>=y && v.y<(y+h) && v.z>=z && v.z<(z+d));
		}
		int Inside(Point3<T> v[], int n) {
			for(int i = 0; i<n; i++) {
				if(Inside(c[i].x, c[i].y, c[i].z))
					return(1);
			}
			return(0);
		}
		int Inside(Y_Volume<T>& C) { 
			// return 1, if all conditions are true, and this is fully inside C
			return(x>=C.x && y>=C.y && z>=C.z && 
				(x+w)<=(C.x+C.w) && (y+h)<=(C.y+C.h) && (z+d)<=(C.z+C.d));
		}
		int Overlap(Y_Volume<T>& C) { 
			// return 1, if overlap exists
			if((x+w)<C.x || x>(C.x+C.w)) return(0);
			if((y+h)<C.y || y>(C.y+C.h)) return(0);
			if((z+d)<C.z || z>(C.z+C.d)) return(0);
			return(1);
		}
		void StartEncompassing()
			{
				if(typeid(T)==typeid(int)) {
					x = y = z = INT_MAX ;
					w = h = d = -INT_MAX ;
				} else if(typeid(T)==typeid(float)) {
					x = y = z = FLT_MAX ;
					w = h = d = -FLT_MAX ;
				} else if(typeid(T)==typeid(double)) {
					x = y = z = DBL_MAX ;
					w = h = d = -DBL_MAX ;
				} else {
					x = y = z = LONG_MAX ;
					w = h = d = -LONG_MAX ;
				}
			}
		void Encompass(T i, T j, T k) {
			x = YMIN(x, i); w = YMAX(w, i); 
			y = YMIN(y, j); h = YMAX(h, j); 
			z = YMIN(z, k); d = YMAX(d, k); 
		}
		void Encompass(Point3<T> v) { 
			Encompass(v.x, v.y, v.z); 
		}
		void FinishEncompassing() {
			w -= x; h -= y; d -= z;
		}
		void Scale(T factor) {
			x -= w; y -= h; z -= d; w *= factor; h *= factor; d *= factor; 
		}
		// Does not work on Linux
#ifndef Linux
		friend ostream &operator<<(ostream &ostr, Y_Volume<T>& r);
#endif
		Y_Volume& operator =(const Y_Volume& c) 
			{x=c.x; y=c.y; z=c.z; w=c.w; h=c.h; d=c.d; return *this;}
		bool operator ==(const Y_Volume& c) {
			//if(SameType<T,int>::result==1)
			if(typeid(T)==typeid(int))
				return(x==c.x && y==c.y && z==c.z && w==c.w && h==c.h && d==c.d);
			else
				return(EPSILON_EQ((double)x,c.x) && EPSILON_EQ((double)y,c.y) && EPSILON_EQ((double)z,c.z) && 
					EPSILON_EQ((double)w,c.w) && EPSILON_EQ((double)h,c.h) && EPSILON_EQ((double)d,c.d));
		}
		bool operator !=(const Y_Volume& c) {
			//if(SameType<T,int>::result==1)
			if(typeid(T)==typeid(int))
				return(x!=c.x || y!=c.y || z!=c.z || w!=c.w || h!=c.h || d!=c.d);
			else
				return(!EPSILON_EQ((double)x,c.x) || !EPSILON_EQ((double)y,c.y) || !EPSILON_EQ((double)z,c.z) || 
					!EPSILON_EQ((double)w,c.w) || !EPSILON_EQ((double)h,c.h) || !EPSILON_EQ((double)d,c.d));
		}
		void Print(FILE *fp) {
			//if(SameType<T,int>::result==1)
			if(typeid(T)==typeid(int))
				fprintf(fp, "(X=%d,Y=%d,Z=%d)->[W=%d,H=%d,D=%d]\n", x, y, z, w, h, d);
			else
				fprintf(fp, "(X=%.2lf,Y=%.2lf,Z=%.2lf)->[W=%.2lf,H=%.2lf,D=%.2lf]\n", 
					(double)x, (double)y, (double)z, (double)w, (double)h, (double)d);
		}
	private:
	public:
		T x, y, z, w, h, d;
};

#ifndef Linux
template <class T>
ostream &operator<<(ostream &ostr, Y_Volume<T>& c) 
{ 
	ostr<<"(X="<<c.x<<",Y="<<c.y<<",Z="<<c.z<<")->"<<"[W="<<c.w<<",H="<<c.h<<",D"<<c.d<<"]"; 
	return(ostr); 
}
#endif

#endif
