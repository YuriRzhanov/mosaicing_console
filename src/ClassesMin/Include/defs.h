#ifndef _DEFS_H
#define _DEFS_H

#include "stdafx.h"
#include <float.h>
//#include <amp.h>

#define _USE_MATH_DEFINES
#include <math.h>
//using namespace std;
//using namespace Concurrency::direct3d

#define RESET      "\033[0m"   /* Reset */

#define BG_black   "\033[40m"  /* Black background */
#define BG_red     "\033[41m"  /* Red background */
#define BG_green   "\033[42m"  /* Green background */
#define BG_yellow  "\033[43m"  /* Yellow background */
#define BG_blue    "\033[44m"  /* Blue background */
#define BG_magenta "\033[45m"  /* Magenta background */
#define BG_cyan    "\033[46m"  /* Cyan background */
#define BG_white   "\033[47m"  /* White background */

#define FG_black   "\033[30m"  /* Black foreground */
#define FG_red     "\033[31m"  /* Red foreground */
#define FG_green   "\033[32m"  /* Green foreground */
#define FG_yellow  "\033[33m"  /* Yellow foreground */
#define FG_blue    "\033[34m"  /* Blue foreground */
#define FG_magenta "\033[35m"  /* Magenta foreground */
#define FG_cyan    "\033[36m"  /* Cyan foreground */
#define FG_white   "\033[37m"  /* White foreground */

#ifndef Y_INT_DEFINED
#define Y_INT_DEFINED
//#if defined(WIN32)
//#define Y_INT int
#if defined(WIN64)
#define Y_INT __int64
#else
#define Y_INT int
#endif
#endif

#ifndef YMIN
#define YMIN(a,b)	(((a) < (b)) ? (a) : (b))
#endif
#ifndef YMAX
#define YMAX(a,b)	(((a) > (b)) ? (a) : (b))
#endif
// no need with _USE_MATH_DEFINES
#ifndef M_PI
#define M_PI 3.1415926
#endif
#ifndef M_PI_2
#define M_PI_2 3.1415926/2.0
#endif

#ifndef Deg2Rad
#define Deg2Rad(a)	((a)*M_PI/180.0)
#endif
#ifndef Rad2Deg
#define Rad2Deg(a)	((a)*180.0/M_PI)
#endif

#ifndef EPSILON
#define EPSILON DBL_EPSILON
// double equality test
#define DOUBLE_EQ(x,v)	(((v)-EPSILON)<(x) && (x)<((v)+EPSILON)) 
#define EPSILON_EQ(x1,x2) (fabs((x1)-(x2))<EPSILON)
#endif

#ifndef SLERP_THRESHOLD
#define SLERP_THRESHOLD 1.0e-5
#endif

// Substituted by function clamp(value,minV, maxV) for floats and ints

#ifndef LIMIT_RANGE
// limits a value to low and high
#define LIMIT_RANGE(low, value, high) (value<low)?low:((value>high)?high:value)
#endif

#ifndef LIMIT_RANGE_CHAR
#define LIMIT_RANGE_CHAR(value)	(value<0)?0:((value>255)?255:value)
#endif

#ifndef D_LIMIT_RANGE_CHAR
#define D_LIMIT_RANGE_CHAR(value) ((double)value<0.0)?0.0:(((double)value>255.0)?255.0:(double)value)
#endif

#ifndef F_LIMIT_RANGE_CHAR
#define F_LIMIT_RANGE_CHAR(value) ((float)value<0.0f)?0.0f:(((float)value>255.0f)?255.0f:(float)value)
#endif


#define FOREACH_PIXEL_START(IMG,I,J) for(int J = 0; J<IMG->h; J++) { for(int I = 0; I<IMG->w; I++) {
#define FOREACH_PIXEL_END }}

#define FOREACH_PIXEL_START_NOBORDERS(IMG,I,J,K) for(int J = K; J<IMG->h-K; J++) { for(int I = K; I<IMG->w-K; I++) {
#define FOREACH_PIXEL_END }}

#if !defined(SGI)
typedef struct { 
	double re; 
	double im; 
} zomplex;
#endif

// Definitions happen in this order:
#if defined(INTEL_LIBS_10)
typedef struct {
    double re;
    double im;
} mkl_double_complex;
#include <mkl_dfti.h>
#define FFT_DEFINED
#endif

#if defined(INTEL_LIBS_8)
#include <mkl_fft.h>
#include <mkl_cblas.h>
#define FFT_DEFINED
#endif

#if defined(INTEL_LIBS)
#include <mkl_fft.h>
#define FFT_DEFINED
#endif

#if defined(SGI)
#if defined(NEW_COMPLIB) && !defined(FFT_DEFINED)
#include <scsl_fft.h>
#define FFT_DEFINED
#endif
#endif

#if defined(COMPLIB) && !defined(FFT_DEFINED)
#include <fft.h>
#define FFT_DEFINED
#endif

#if defined(FFTW) && !defined(FFT_DEFINED)
#include <fftw.h>
#define FFT_DEFINED
#endif

// If, at this point, FFT_DEFINED is not defined, use Numerical Recipies FFT

#if defined(FFT_DEFINED)
#define Re(D,i,j,sz)	D[(j)*sz+(i)].re
#define Im(D,i,j,sz)	D[(j)*sz+(i)].im
#else
#define Re(D,i,j,sz)	D[2*((j)*sz+(i))]
#define Im(D,i,j,sz)	D[2*((j)*sz+(i))+1]
#endif

#define Mag(D,i,j,sz)	sqrt(Re(D,(i),(j),sz)*Re(D,(i),(j),sz)+\
						Im(D,(i),(j),sz)*Im(D,(i),(j),sz))
#define Pha(D,i,j,sz)	atan2(Im(D,(i),(j),sz),Re(D,(i),(j),sz))

#define FLOOR_W(x) ((x>0.0) ? (int) floor(x+0.5) : -(int) floor(-x+0.5))
#define CEIL_W(x) ((x>0.0) ? (int) ceil(x+0.5) : -(int) ceil(-x+0.5))

// The above must be a grave mistake...:-(
#define FLOOR(x) (int) floor(x)
#define CEIL(x) (int) ceil(x)

typedef unsigned short u_short;
typedef unsigned char u_char;

/* Format for writing and reading co-reg records */
/* Frame/X-shift/Y-shift/Transl-robust/Zoom/Angle/ZR-robust/X-center/Y-center
*/
#define FMT "  %3d   %7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) [%6.1lf,%6.1lf]\n"
#define FMT2 "# %3d   %7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) [%6.1lf,%6.1lf]\n"

/* Frame/X-shift/Y-shift/Transl-robust/Zoom/Angle/ZR-robust/
 			Overlap-%/Error/X-center/Y-center
*/
#define FMTn "  %3d   %7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) [%6.1lf,%6.1lf]\n"
#define FMTn2 "# %3d   %7.2lf %7.2lf (%8.3lf)  %8.5lf %8.3lf (%8.3lf) (%6.2lf,%8.3lf) [%6.1lf,%6.1lf]\n"

// '0.01' is added to guarantee that that result is not rounded incorrectly
#define ToGreyscale(r,g,b)	(0.299*(r)+0.587*(g)+0.114*(b)+0.01)

// Min and max radii used for cart-to-polar transformation
#define CTP_Rad_Min		10.0
#define CTP_Rad_Max		double(size/2)

// Field of view to use, if nothing else is specified
#define FOV_DEFAULT	42.5

typedef enum { NO_MASK = 0, X_MASK, Y_MASK, XY_MASK } MASK_TYPE;

typedef enum { DO_TRANS = 1, DO_ROT = 2, DO_ZOOM = 4, DO_BILINEAR = 8,
	DO_INVALID = 16, DO_ALL = 31 } DO_FLAGS;
typedef enum { FULL_PROC = 0, JUST_MAX = 1 } PROC_MAX_TYPE;

// 'debug' option is encoded in 2-nd 8 bits:
// flags |= debug<<8; 
// Method of extraction of maximum is encoded in 3-rd 8 bits:
// flags |= JUST_MAX<<16;

// For compile time determination of type
// Could be replaced with if(typeid(T)==typeid(float)), etc.
template<typename X, typename Y>
struct SameType
{
   enum { result = 0 };
};
 
template<typename T>
struct SameType<T, T>
{
   enum { result = 1 };
};

#endif
