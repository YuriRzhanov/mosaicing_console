#include <stdlib.h>
#include <math.h>
#include "BMPImageG.h"

using namespace std;

u_char BMPImageG::INVALID = Y_Utils::_INVALID_UCHAR;
u_char BMPImageG::m_white = (u_char)255;
u_char BMPImageG::m_black = (u_char)0;

BMPImageG::BMPImageG()
	: BMPImage(), im(nullptr), pf_data(nullptr)
{
	//fprintf(stderr, "endian=%d\n", endian);
}

BMPImageG::BMPImageG(int _w, int _h)
	: BMPImage(_w, _h, 1), im(nullptr), pf_data(nullptr)
{
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	//padb = 4-w%4; if(padb==4) padb = 0;
	// Same as below:
	padb = (4-w%4)&0x03;
	im = (u_char*) new char[w*h];
	memset(im, 0, w*h);
}

BMPImageG::BMPImageG(int _w, int _h, u_char *_im)
	: BMPImage(_w, _h, 1), im(nullptr), pf_data(nullptr)
{
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	//padb = 4-w%4; if(padb==4) padb = 0;
	// Same as below:
	padb = (4-w%4)&0x03;
	im = (u_char*) new char[w*h];
	memcpy(im, _im, w*h);
}

BMPImageG::BMPImageG(int _w, int _h, u_short *_im)
	: BMPImage(_w, _h, 1), im(nullptr), pf_data(nullptr)
{
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	//padb = 4-w%4; if(padb==4) padb = 0;
	// Same as below:
	padb = (4-w%4)&0x03;
	im = (u_char*) new char[w*h];
	for(int i = 0; i<w*h; i++)
		im[i] = (u_char) _im[i];
}

BMPImageG::BMPImageG(int _w, int _h, float *_im)
	: BMPImage(_w, _h, 1), im(nullptr), pf_data(nullptr)
{
	int k;
	float a_max = 0.0, a_min = 1.e9, scale;
	for(k = 0; k<w*h; k++) {
		a_max = (a_max < _im[k]) ? _im[k] : a_max;
		a_min = (a_min > _im[k]) ? _im[k] : a_min;
	}
	if(a_min>=a_max)
		scale = 1.0;
	else
		scale = (255.0f-0.0f)/(a_max-a_min);
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	padb = 4-w%4;
	if(padb==4) padb = 0;
	im = (u_char*) new char[w*h];
	for(k = 0; k<w*h; k++)
		im[k] = (u_char) (scale*(_im[k]-a_min));
}

BMPImageG::BMPImageG(int _w, int _h, double *_im)
	: BMPImage(_w, _h, 1), im(nullptr), pf_data(nullptr)
{
	int k;
	double a_max = -1.e9, a_min = 1.e9, scale;
	for(k = 0; k<w*h; k++) {
		a_max = (a_max < _im[k]) ? _im[k] : a_max;
		a_min = (a_min > _im[k]) ? _im[k] : a_min;
	}
	if(a_min>=a_max)
		scale = 1.0;
	else
		scale = (255.0-0.0)/(a_max-a_min);
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	//padb = 4-w%4; if(padb==4) padb = 0;
	// Same as below:
	padb = (4-w%4)&0x03;
	im = (u_char*) new char[w*h];
	for(k = 0; k<w*h; k++)
		im[k] = (u_char) (scale*(_im[k]-a_min));
}

BMPImageG::BMPImageG(const char *file, int suppress_warnings)
	: im(nullptr), pf_data(nullptr)
{
	if(Read(file, suppress_warnings)==-1)
		fail = 1;
}

BMPImageG::~BMPImageG()
{
	if(im) {
		delete [] im;
		im = nullptr;
	}
	if(pf_data) {
		delete [] pf_data;
		pf_data = nullptr;
	}
	fail = 1;
}

void BMPImageG::Dimensions(int _w, int _h)
{
	if(im) delete [] im;
	w = _w; h = _h; SetBmpHeader(w, h, z);
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	//padb = 4-w%4; if(padb==4) padb = 0;
	// Same as below:
	padb = (4-w%4)&0x03;
	im = (u_char*) new char[w*h];
	prefilter = 0; 
	if(pf_data) {
		delete [] pf_data;
		pf_data = nullptr;
	}
}

void BMPImageG::GetMinMax(u_char& v_min, u_char& v_max)
{
	for(int k = 0; k<w*h; k++) {
		v_min = YMIN(v_min, im[k]);
		v_max = YMAX(v_max, im[k]);
	}
}

int BMPImageG::PutAt(BMPImageG *p, int x, int y)
{
	for(int j = 0; j<p->h; j++) {
		if((y+j)<0 || (y+j)>=h)
			continue;
		for(int i = 0; i<p->w; i++) {
			if((x+i)<0 || (x+i)>=w)
				continue;
			im[(y+j)*w+(x+i)] = p->im[j*p->w+i];
		}
	}
	return(0);
}

BMPImageG *BMPImageG::Copy()
{
	BMPImageG *c = new BMPImageG(w, h, im);
	c->z = z;
	c->conf_set = conf_set;
	c->bpp = bpp;
	c->endian = endian;
	c->SetFocalLength(focal_length);
	c->SetBmpHeader(w, h, z);
	c->prefilter = prefilter;
	if(prefilter) {
		c->pf_data = new double[w*h];
		memcpy(c->pf_data, pf_data, w*h*sizeof(double));
	}
	return(c);
}

void BMPImageG::Copy(BMPImageG *s)
{
	if(w!=s->w || h!=s->h) {
		//sprintf_s(error, 256, "Images have different dimensions!\n");
		return;
	}
	memcpy(im, s->im, w*h*sizeof(u_char));
	z = s->z;
	conf_set = s->conf_set;
	bpp = s->bpp;
	endian = s->endian;
	SetFocalLength(s->focal_length);
	SetBmpHeader(s->w, s->h, s->z);
	prefilter = s->prefilter;
	if(prefilter) {
		pf_data = new double[w*h];
		memcpy(pf_data, s->pf_data, w*h*sizeof(double));
	}
}

void BMPImageG::InvalidateColour(u_char clr)
{
	has_invalid = true;
	for(int j = 0; j<w*h; j++) {
		if(im[j]==clr)
			im[j] = BMPImageG::INVALID;
	}
}

int BMPImageG::Substitute(u_char clr, u_char with, Y_Rect<int>* pRect)
{
	int n = 0;
	if(!pRect) {
		for(int j = 0; j<w*h; j++) {
			if(im[j]==clr) {
				im[j] = with;
				n++;
			}
		}
	} else {
		for(int j = pRect->y; j<=(pRect->y+pRect->h); j++) {
			for(int i = pRect->x; i<=(pRect->x+pRect->w); i++) {
				if(im[i+w*j]==clr) {
					im[i+w*j] = with;
					n++;
				}
			}
		}
	}
	return(n);
}

int BMPImageG::LeaveOnly(u_char leave, u_char with)
{
	int n = 0;
	for(int j = 0; j<w*h; j++) {
		if(im[j]!=leave) {
			im[j] = with;
			n++;
		}
	}
	return(n);
}

int BMPImageG::CountPixels(u_char clr)
{
	int n = 0;
	for(int j = 0; j<w*h; j++) {
		if(im[j]==clr)
			n++;
	}
	return(n);
}

int BMPImageG::Validate(u_char g_invalid) 
{
	return(Substitute(g_invalid, g_invalid+1));
}

// If has_invalid, preserve INVALID pixels. The rest are set to 'clr'
void BMPImageG::SetColour(u_char clr, int override_invalid)
{
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(override_invalid)
				set_pixel(i, j, clr);
			else if(valid(i, j))
				set_pixel(i, j, clr);
		}
	}
}

BMPImageG** BMPImageG::Split(int factor)
{
	BMPImageG** pImgs = new BMPImageG*[factor];
	int i, j, new_w = w/factor, nw;
	for(int n = 0; n<factor; n++) {
		// Last image could have different number of columns from the rest
		nw = (n<(factor-1)) ? new_w : w-new_w*n;
		pImgs[n] = new BMPImageG(nw, h);
		for(j = 0; j<h; j++) {
			for(i = 0; i<nw; i++) {
				pImgs[n]->set_pixel(i, j, get_pixel(n*nw+i, j));
			}
		}
	}
	return(pImgs);
}

Y_Histogram<int>* BMPImageG::CalculateHistogram(bool has_invalid, int low, int high)
{
	Y_Histogram<int>* pHist = new Y_Histogram<int>(256);
	pHist->SetLimits(low, high);
	if(has_invalid) {
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				if(valid(i,j))
					pHist->Add((int)get_pixel(i,j));
			}
		}
	} else {
		for(int j = 0; j<h; j++) {
			for(int i = 0; i<w; i++) {
				pHist->Add((int)get_pixel(i,j));
			}
		}
	}
	return(pHist);
}

// Linear shift in brightness to match average and 'level'. Returns number of saturated pixels.
int BMPImageG::ShiftAverageTo(u_char level)
{
	int aver = (int)AverageBrightness();
	int j, saturated = 0, v;
	int shift = (int)level-aver;
	for(j = 0; j<w*h; j++) {
		v = (int)im[j]+shift;
		if(v<0) {
			im[j] = m_black;
			saturated++;
		} else if(v>255) {
			im[j] = m_white;
			saturated++;
		} else {
			im[j] = (u_char)v;
		}
	}
	return(saturated);
}

// Preserve validity of pixels.
void BMPImageG::CorrectIllum(BMPImageG *p)
{
	if(p->w!=w || p->h!=h) {
		//sprintf_s(error, 256, "Correcting illumination with mask of wrong size.\n");
		return;
	}
	double v;
	for(int j = 0; j<w*h; j++) {
		if(has_invalid && im[j]==BMPImageG::INVALID) continue;
		if(p->im[j]) {
			v = 255.0*im[j]/p->im[j];
			if(v>255.0) im[j] = m_white;
		} else
			im[j] = m_white;
	}
}

void BMPImageG::ValidateByMask(BMPImageG *p)
{
	if(!p) return;
	if(p->w!=w || p->h!=h) {
		//sprintf_s(error, 256, "Validating with mask of wrong size.\n");
		return;
	}
	for(int j = 0; j<w*h; j++) {
		if(p->im[j]==INVALID)
			im[j] = INVALID;
	}
}

int BMPImageG::Read(const char *file, int suppress_warnings)
{
	if(strstr(file, ".pgm") || strstr(file, ".PGM")) { // read it as 8-bit pgm
		return(ReadPGM(file));
	}

	int i, j, k, ind;
	unsigned char *buf;
	FILE *fi;
#ifdef _MSC_VER
	if(fopen_s(&fi, file, "rb")) {
		sprintf_s(error, 256, "Cannot open %s\n", file); 
		fail = 1;
		return(-1);
	}
#else
  fi = fopen(file, "rb");
	if(!fi) {
		sprintf(error, "Cannot open %s\n", file); 
		fail = 1;
		return(-1);
	}
#endif
	ReadBmpHeader(fi);
	w = bmphdr.cols;
	h = bmphdr.rows;
	padw = ((w+3)/4)*4; /* multiple of 4pix (32 bits) */
	padb = (4-w%4)&0x03;
	
	{
		int pp = h*((w+3) & ~3);
		pp += pp>>1;
		int k = 0;
	}
	/* Sanity checks */
	if(bmphdr.bpp != 8) { // Read as colour first
		fclose(fi);
		// Non-fatal warning
		//sprintf_s(error, 256, "Not a greyscale %s.\n", EXTN);
		if(!suppress_warnings)
			fprintf(stderr, "Not a greyscale %s.\n", EXTN);
		BMPImageC *tmpC = new BMPImageC;
		if(tmpC->Read(file)==0) {
			BMPImageG *tmpG = tmpC->Greyscale('a');
			im = (u_char*) new char[w*h];
			if(fail) {
				delete tmpC;
				delete tmpG;
				return(-1);
			}
			Copy(tmpG);
			delete tmpG;
			delete tmpC;
			return(0);
		} else {
			fail = 1;
			fclose(fi);
			delete tmpC;
			return(-1);
		}
		delete tmpC;
	}
	if((bmphdr.isize+1024+54)!=bmphdr.fsize && !suppress_warnings) {
		//sprintf_s(error, 256, "Fsize != Isize+1024+54!\n");
		// In some cases it's not - so just warn.
		// fail = 1; fclose(fi); return(-1);
	}
	if(bmphdr.isize != (unsigned int)(padw*h) && !suppress_warnings) {
		//sprintf_s(error, 256, "Wrong Isize.\n");
		// Photoshop creates differently (!?) - so just warn.
		// fail = 1; fclose(fi); return(-1);
	}
	if(im)
		delete [] im; // In case of re-reading in the same image
	im = (u_char*)new char[w*h];
	buf = (unsigned char *) new char[padw];

	if(bmphdr.clrused!=0) { // Read colourmap
		unsigned char *cmap = (unsigned char *) new char[bmphdr.clrused*4];
		fread(cmap, sizeof(char), bmphdr.clrused*4, fi);
		// Check that it's greyscale
		for(i = 0; i<(int) bmphdr.clrused; i++) {
			if(cmap[i*4+0]!=cmap[i*4+1] || cmap[i*4+0]!=cmap[i*4+2]) {
				//sprintf_s(error, 256, "Not greyscale BMP.\n");
				fail = 1;
				fclose(fi);
				delete [] buf;
				return(-1);
			}
		}
		// Read values, using colourmap
		fseek(fi, bmphdr.off, 0);
		for(j=h-1; j>=0; j--) {
			fread(buf, sizeof(char), padw, fi);
			for(k=0; k<w; k++) {
				ind = j*w+k;
				im[ind] = cmap[buf[k]*4];
			}
		}
		delete [] cmap;
	} else {
		// Don't read colormap
		fseek(fi, bmphdr.off, 0);
		for(j=h-1; j>=0; j--) {
			fread(buf, sizeof(char), padw, fi);
			for(k=0; k<w; k++) {
				ind = j*w+k;
				im[ind] = buf[k];
			}
		}
	}
	fclose(fi);
	delete [] buf;
	return(0);
}

int BMPImageG::Save(const char *file, int s_bpp)
{
	FILE *fo;
	int i, j;
#ifdef _MSC_VER
	if(fopen_s(&fo, file, "wb")) {
		sprintf_s(error, 256, "Cannot write to \"%s\"\n", file);
		return(-1);
	}
#else
  fo = fopen(file, "wb");
	if(!fo) {
		sprintf(error, "Cannot write to \"%s\"\n", file);
		return(-1);
	}
#endif
	padw = ((w+3)/4)*4; /* 'w' padded to a multiple of 4pix (32 bits) */
	unsigned char *line = (unsigned char *) new char[padw];

	/*
	bmphdr.typ = ('M' << 8) + 'B';
	bmphdr.fsize = 54 + 1024 + (padw*h);
	if(focal_length<=0.0) {
		bmphdr.res1 = 0;
		bmphdr.res2 = 0;
	} else {
		float fl = float(focal_length);
		memcpy(&bmphdr.res1, &fl, sizeof(float));
	}
	bmphdr.off = 54 + 1024;
	bmphdr.hsize = 40;
	bmphdr.cols = w;
	bmphdr.rows = h;
	bmphdr.planes = 1;
	bmphdr.bpp = 8;
	bmphdr.compress = 0;
	bmphdr.isize = padw*h;
	bmphdr.xpel = 0;
	bmphdr.ypel = 0;
	bmphdr.clrused = 0;
	bmphdr.clrimp = 0;
	*/
	SetBmpHeader(w, h, 1);
	if(WriteBmpHeader(fo)==-1) { 
		fclose(fo); 
		delete [] line;
		return(-1); 
	}
	/* Output colourmap */
	char cmap[4*256];
	for(j=0; j<256; j++) {
		if(endian) {
			cmap[j*4+1] = cmap[j*4+2] = cmap[j*4+3] = j;
			cmap[j*4] = 0;
			cmap[j*4] = cmap[j*4+1] = cmap[j*4+2] = j;
			cmap[j*4+3] = 0;
		} else {
			cmap[j*4] = cmap[j*4+1] = cmap[j*4+2] = j;
			cmap[j*4+3] = 0;
		}
	}
	if(fwrite(cmap, 1, 4*256, fo)!=4*256) { fclose(fo); return(-1); }
	fseek(fo, bmphdr.off, 0);
	for(j = h-1; j>=0; j--) {
		for(i = 0; i<w; i++) {
			line[i] = (char) im[j*w+i];
		}
		if(fwrite(line, sizeof(char), padw, fo)!=padw) { fclose(fo); return(-1); }
	}
	fclose(fo);
	delete [] line;
	return(0);
}

int BMPImageG::SaveASCII(const char* file, bool saveIndexes)
{
	FILE* fp;
#ifdef _MSC_VER
	if(fopen_s(&fp, file, "w"))
		return(-1);
#else
  fp = fopen(file, "w");
	if(!fp)
		return(-1);
#endif
	int i, j;
	if(saveIndexes) {
		fprintf(fp, "    ");
		for(i = 0; i<w; i++)
			fprintf(fp, "%4d", i);
		for(j = 0; j<h; j++) {
			fprintf(fp, "%4d", j);
			for(i = 0; i<w; i++)
				fprintf(fp, "%4d", im[i+j*w]);
			fprintf(fp, "\n");
		}
	} else {
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++)
				fprintf(fp, "%4d", im[i+j*w]);
			fprintf(fp, "\n");
		}
	}
	fclose(fp);
	return(0);
}

int BMPImageG::ReadPGM(const char *file)
{
	unsigned char char1, char2;
	int max, c1, c2, c3, i, j;

	FILE *fp;
#ifdef _MSC_VER
	if(fopen_s(&fp, file, "rb")) {
		sprintf_s(error, 256, "Cannot open %s\n", file); 
		fail = 1;
		return(-1);
	}
#else
  fp = fopen(file, "rb");
	if(!fp) {
		sprintf(error, "Cannot open %s\n", file); 
		fail = 1;
		return(-1);
	}
#endif
	char1 = fgetc(fp); char2 = fgetc(fp); BMPImage::SkipPGMPPMComments(fp);
#ifdef _MSC_VER
	c1 = fscanf_s(fp, "%d", &w); BMPImage::SkipPGMPPMComments(fp);
	c2 = fscanf_s(fp, "%d", &h); BMPImage::SkipPGMPPMComments(fp);
	c3 = fscanf_s(fp, "%d", &max);
#else
	c1 = fscanf(fp, "%d", &w); BMPImage::SkipPGMPPMComments(fp);
	c2 = fscanf(fp, "%d", &h); BMPImage::SkipPGMPPMComments(fp);
	c3 = fscanf(fp, "%d", &max);
#endif

	if (char1 != 'P' || char2 != '5' || c1 != 1 || c2 != 1 || c3 != 1 || max > 255) {
		//sprintf_s(error, 256, "Not a raw 8-bit PGM.\n");
		fail = 1; fclose(fp); return(-1);
	}

	fgetc(fp);  // Discard exactly one byte after header.
	if(im)
		delete [] im; // In case of re-reading in the same image
	im = (u_char*) new char[w*h];

	// Create floating point image with pixels in range [0,1].
	for(j = 0; j < h; j++) {
		for (i = 0; i < w; i++)
			set_pixel(i, j, (u_char) fgetc(fp));
	}
	fclose(fp);
	return(0);
}

int BMPImageG::SavePGM(const char *file)
{
	FILE *fo;
#ifdef _MSC_VER
	if(fopen_s(&fo, file, "wb"))
		return(-1);
#else
  fo = fopen(file, "wb");
	if(!fo)
		return(-1);
#endif

	char *tmp = new char[w], *p;
	
	fprintf(fo, "P5\n%d %d\n255\n", w, h);
	for(int j = 0; j<h; j++) {
		p = tmp;
		for(int i = 0; i<w; i++) {
			*p = (char) im[j*w+i]; p++;
		}
		fwrite(tmp, w, 1, fo);
	}
	fclose(fo);
	
	delete [] tmp;
	return(0);	
}

// invalid, if any corner is invalid
int BMPImageG::valid(double x, double y)
{
	int i = int(x), j = int(y);
	if(valid(i,j) && valid(i+1,j) && valid(i,j+1) && valid(i+1,j+1))
		return(1);
	return(0);
}

// valid pixel, neighbouring invalid
int BMPImageG::Border(int x, int y)
{
	if(!valid(x, y)) return(0);
	for(int j = y-1; j<=y+1; j++) {
		for(int i = x-1; i<=x+1; i++) {
			if(!valid(i, j)) return(1);
		}
	}
	return(0);
}

// Cut off subimage, taking care of image extent.
//   at_x/y - top-left corner of cut
//   sw/sh - requested size
//   Area is initiated with black.
BMPImageG *BMPImageG::CutSubimage(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	BMPImageG *s = new BMPImageG(sw, sh);
	s->SetBlack();
	s->SetFocalLength(focal_length);
	
	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			s->im[jj*sw+ii] = im[j*w+i];
		}
	}
	return(s);
}

void BMPImageG::Crop(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	u_char* im_ = (u_char*) new char[sw*sh];

	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			im_[jj*sw+ii] = im[j*w+i];
		}
	}
	delete [] im;
	im = im_;
	w = sw; h = sh;
}

double BMPImageG::AverageBrightness(int invalid_clr)
{
	double s = 0.0;
	int count = 0;
	for(int i = 0; i<w*h; i++) {
		if((int) im[i] != invalid_clr) {
			s += im[i]; count++;
		}
	}
	return(s/double(count));
}

int BMPImageG::Replicate(int f)
{
	u_char *new_im = (u_char*) new char[f*w*f*h];
	int x, y;
	for(int j = 0; j<f*h; j++) {
		y = j/f;
		for(int i = 0; i<f*w; i++) {
			x = i/f;
			new_im[j*f*w+i] = im[y*w+x];
		}
	}
	w = f*w; h = f*h;
	delete [] im;
	im = new_im;
	return(0);
}

int BMPImageG::ZoomToDest_Bilinear(int dest_w, int dest_h, bool show_progress)
{
	double I, J;
	int vld, status = 0;
	double h_factor = double(w-1)/double(dest_w-1);
	double v_factor = double(h-1)/double(dest_h-1);
	u_char *Im = new u_char[dest_w*dest_h];

	ConsoleProgress* pCP = new ConsoleProgress(dest_h*dest_w, 70);
	for(int j = 0; j<dest_h-1; j++) {
		J = v_factor*j;
		for(int i = 0; i<dest_w-1; i++) {
			I = h_factor*i;
			Im[j*dest_w+i] = (u_char) Bilinear(I, J, &vld);
			if(has_invalid && !vld) Im[j*dest_w+i] = INVALID;
			if(show_progress) pCP->Next();
		}
		I = h_factor*(dest_w-1)-0.5;
		Im[j*dest_w+dest_w-1] = (u_char) Bilinear(I, J, int(I), int(J));
	}
	delete pCP;
	// Last row - enforce interpolation from above row
	J = v_factor*(dest_h-1)-0.5;
	for(int i = 0; i<dest_w-1; i++) {
		I = h_factor*i;
		Im[(dest_h-1)*dest_w+i] = (u_char) Bilinear(I, J, int(I), int(J));
	}
	I = h_factor*(dest_w-1)-0.5;
	Im[(dest_h-1)*dest_w+dest_w-1] = (u_char) Bilinear(I, J, int(I), int(J));
	w = dest_w; h = dest_h;
	delete [] im;
	im = Im;

	return(status);
}

// Result has different size!
int BMPImageG::Zoom_Bilinear(double zm, bool show_progress)
{
	int i, j, k, status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bilinear(dest_w, dest_h, show_progress);
	if(status==-1) {
		//sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	
	if(has_invalid) {
		// Erode image, removing pixels next to invalid pixels
		u_char *cim = (u_char*) new char[dest_w*dest_h];
		memcpy(cim, im, dest_w*dest_h*sizeof(u_char));
		for(j = 1; j<dest_h-1; j++) {
			for(i = 1; i<dest_w-1; i++) {
				k = j*dest_w+i;
				if(cim[k-dest_w-1]==INVALID) im[k] = INVALID;
				if(cim[k-dest_w]==INVALID) im[k] = INVALID;
				if(cim[k-dest_w+1]==INVALID) im[k] = INVALID;
				if(cim[k-1]==INVALID) im[k] = INVALID;
				if(cim[k+1]==INVALID) im[k] = INVALID;
				if(cim[k+dest_w-1]==INVALID) im[k] = INVALID;
				if(cim[k+dest_w]==INVALID) im[k] = INVALID;
				if(cim[k+dest_w+1]==INVALID) im[k] = INVALID;
			}
		}
		delete [] cim;
	}

	if(focal_length > 0.0)
		focal_length *= zm;
	// Now 'im' has dimensions dest_w*dest_h
	return(status);
}

// Result has different size!
int BMPImageG::ZoomToDest_Bicubic(int dest_w, int dest_h, bool show_progress)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	NEWMAT::ColumnVector alpha(16), beta(16);

	u_char *Im = new u_char[dest_w*dest_h];

	double x_scale = (double)dest_w/w;
	double y_scale = (double)dest_h/h;

	DblImageG* pDblXd = new DblImageG(w, h);
	DblImageG* pDblYd = new DblImageG(w, h);
	DblImageG* pDblXYd = new DblImageG(w, h);

	pDblXd->SetBlack();
	pDblYd->SetBlack();
	pDblXYd->SetBlack();

	int i, j, k;

	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(i==0 || i==w-1)
				pDblXd->set_pixel(i,j,0.0);
			else
				pDblXd->set_pixel(i,j,0.5*((double)get_pixel(i+1,j)-get_pixel(i-1,j)));
			if(j==0 || j==h-1)
				pDblYd->set_pixel(i,j,0.0);
			else
				pDblYd->set_pixel(i,j,0.5*((double)get_pixel(i,j+1)-get_pixel(i,j-1)));
			if(i==0 || i==w-1 || j==0 || j==h-1)
				pDblXYd->set_pixel(i,j,0.0);
			else
				pDblXYd->set_pixel(i,j,0.25*((double)get_pixel(i-1,j-1)+get_pixel(i+1,j+1)-get_pixel(i-1,j+1)+get_pixel(i+1,j-1)));
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;

	// Normalized distances:
	double X, Y, I, J, fI, fJ, cI, cJ;
	Point2<int> I11, I21, I12, I22;
	double v;
	for(j = 0; j<dest_h; j++) {
		J = (double)j/y_scale;
		fJ = floor(J); cJ = ceil(J);
		if(fJ<0) fJ = 0;
		if(cJ>h-1) cJ = h-1;
		Y = J-fJ; // W
		for(i = 0; i<dest_w; i++) {
			I = (double)i/x_scale;
			fI = floor(I); cI = ceil(I);
			if(fI<0) fI = 0;
			if(cI>w-1) cI = w-1;
			X = I-fI; // H
			// Indexes of nearest pixels:
			I11.Set((int)fI, (int)fJ);
			I21.Set((int)cI, (int)fJ);
			I12.Set((int)fI, (int)cJ);
			I22.Set((int)cI, (int)cJ);
			// Beta vector:
			beta<<(double)get_pixel(I11)<<(double)get_pixel(I21)<<(double)get_pixel(I12)<<(double)get_pixel(I22)
				<<pDblXd->get_pixel(I11)<<pDblXd->get_pixel(I21)<<pDblXd->get_pixel(I12)<<pDblXd->get_pixel(I22)
				<<pDblYd->get_pixel(I11)<<pDblYd->get_pixel(I21)<<pDblYd->get_pixel(I12)<<pDblYd->get_pixel(I22)
				<<pDblXYd->get_pixel(I11)<<pDblXYd->get_pixel(I21)<<pDblXYd->get_pixel(I12)<<pDblXYd->get_pixel(I22);
			alpha = M_bicubic*beta;
			v = 0.0;
			for(k = 0; k<16; k++) {
				v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
			}
			if(v<0.0) v = 0.0;
			if(v>255.0) v = 255.0;
			Im[i+dest_w*j] = (u_char)v;
		}
	}

	delete pDblXd;
	delete pDblYd;
	delete pDblXYd;

	w = dest_w; h = dest_h;
	delete [] im;
	im = Im;

	return(0);
}

// Result has different size!
int BMPImageG::Zoom_Bicubic(double zm, bool show_progress)
{
	int i, j, k, status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bicubic(dest_w, dest_h, show_progress);
	if(status==-1) {
		//sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	
	if(has_invalid) {
		// Erode image, removing pixels next to invalid pixels
		u_char *cim = (u_char*) new char[dest_w*dest_h];
		memcpy(cim, im, dest_w*dest_h*sizeof(u_char));
		for(j = 1; j<dest_h-1; j++) {
			for(i = 1; i<dest_w-1; i++) {
				k = j*dest_w+i;
				if(cim[k-dest_w-1]==INVALID) im[k] = INVALID;
				if(cim[k-dest_w]==INVALID) im[k] = INVALID;
				if(cim[k-dest_w+1]==INVALID) im[k] = INVALID;
				if(cim[k-1]==INVALID) im[k] = INVALID;
				if(cim[k+1]==INVALID) im[k] = INVALID;
				if(cim[k+dest_w-1]==INVALID) im[k] = INVALID;
				if(cim[k+dest_w]==INVALID) im[k] = INVALID;
				if(cim[k+dest_w+1]==INVALID) im[k] = INVALID;
			}
		}
		delete [] cim;
	}

	if(focal_length > 0.0)
		focal_length *= zm;
	// Now 'im' has dimensions dest_w*dest_h
	return(status);
}

#ifdef _MSC_VER
#include <amp.h>
#endif

int BMPImageG::Translate_Bilinear(double X, double Y, u_char bg, bool show_progress)
{
	int i, j;
	double dx, dy, v;
	
	BMPImageG *tmp = Copy();
	this->SetColour(bg);

	int vld; // For validity check
	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			// Positions as doubles
			dx = -X+i;
			dy = -Y+j;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bilinear(dx, dy, &vld);
			if(vld) set_pixel(i, j, D_LIMIT_RANGE_CHAR(v));
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	//tmp->ResetPrefilter(); // Not needed
	delete tmp;
	
	return(0);
}

int BMPImageG::Translate_Bicubic(double X, double Y, u_char bg, bool show_progress)
{
	int i, j;
	double dx, dy, v;
	
	BMPImageG *tmp = Copy();
	this->SetColour(bg);

	int vld; // For validity check
	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			// Positions as doubles
			dx = -X+i;
			dy = -Y+j;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bicubic(dx, dy, &vld);
			if(vld) set_pixel(i, j, D_LIMIT_RANGE_CHAR(v));
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	//tmp->ResetPrefilter(); // Not needed
	delete tmp;
	
	return(0);
}

int BMPImageG::Rotate_Bilinear(double angle_degrees, u_char bg, bool show_progress)
{
	int i, j;
	double cos_a, sin_a, v;
	BMPImageG *tmp = Copy();
	this->SetColour(bg);

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bilinear(dx, dy, &vld);
			if(vld) set_pixel(i, j, D_LIMIT_RANGE_CHAR(v));
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

int BMPImageG::Rotate_Bicubic(double angle_degrees, u_char bg, bool show_progress)
{
	int i, j;
	double cos_a, sin_a, v;
	BMPImageG *tmp = Copy();
	this->SetColour(bg);

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	ConsoleProgress* pCP = new ConsoleProgress(h*w, 70);
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bicubic(dx, dy, &vld);
			if(vld) set_pixel(i, j, D_LIMIT_RANGE_CHAR(v));
			if(show_progress) pCP->Next();
		}
	}
	delete pCP;
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

// Returns new array of the same size after transformations
int BMPImageG::ZoomRotateTranslate_Bilinear(double zm, double a_d, 
	double X, double Y, int flags)
{
	int i, j;
	int status = 0;
	
	if(!EPSILON_EQ(zm,1.0)) {
		// Zooming
		int dest_w = (int) (zm*w);
		int dest_h = (int) (zm*h);
		if(even_zoom) {
			if(dest_w%2==1) dest_w++;
			if(dest_h%2==1) dest_h++;
		}
		int sw = (w-dest_w)/2;
		int sh = (h-dest_h)/2;

		// Keep values of dimensions
		int prev_w = w;
		int prev_h = h;
	
		status = ZoomToDest_Bilinear(dest_w, dest_h);
		if(status==-1) {
			//sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
			exit(-1);
		}
		// Now 'im' has dimensions dest_w*dest_h
	
		u_char *tmp = (u_char*) new char[prev_w*prev_h];
		for(j = 0; j<prev_h; j++) {
			if(j-sh<0 || j+sh>=prev_h)
				continue;
			for(i = 0; i<prev_w; i++) {
				if(i-sw<0 || i+sw>=prev_w)
					continue;
				tmp[j*prev_w+i] = im[(j-sh)*dest_w+(i-sw)];
			}
		}
		delete [] im; im = tmp;
		
		// Restore dimensions
		w = prev_w; h = prev_h;
	}
	
	if(!EPSILON_EQ(a_d,0.0)) {
		// Rotation
		status += Rotate_Bilinear(a_d, flags);
	}
	
	if(!EPSILON_EQ(X,0.0) || !EPSILON_EQ(Y,0.0)) {
		// Translation
		status += Translate_Bilinear(X, Y, flags);
	}
	return(status);
}

// Returns new array of the same size after transformations
int BMPImageG::ZoomRotateTranslate_Bicubic(double zm, double a_d, 
	double X, double Y, int flags)
{
	int i, j;
	int status = 0;
	
	if(!EPSILON_EQ(zm,1.0)) {
		// Zooming
		int dest_w = (int) (zm*w);
		int dest_h = (int) (zm*h);
		if(even_zoom) {
			if(dest_w%2==1) dest_w++;
			if(dest_h%2==1) dest_h++;
		}
		int sw = (w-dest_w)/2;
		int sh = (h-dest_h)/2;

		// Keep values of dimensions
		int prev_w = w;
		int prev_h = h;
	
		status = ZoomToDest_Bicubic(dest_w, dest_h);
		if(status==-1) {
			//sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
			exit(-1);
		}
		// Now 'im' has dimensions dest_w*dest_h
	
		u_char *tmp = (u_char*) new char[prev_w*prev_h];
		for(j = 0; j<prev_h; j++) {
			if(j-sh<0 || j+sh>=prev_h)
				continue;
			for(i = 0; i<prev_w; i++) {
				if(i-sw<0 || i+sw>=prev_w)
					continue;
				tmp[j*prev_w+i] = im[(j-sh)*dest_w+(i-sw)];
			}
		}
		delete [] im; im = tmp;
		
		// Restore dimensions
		w = prev_w; h = prev_h;
	}
	
	if(!EPSILON_EQ(a_d,0.0)) {
		// Rotation
		status += Rotate_Bicubic(a_d, flags);
	}
	
	if(!EPSILON_EQ(X,0.0) || !EPSILON_EQ(Y,0.0)) {
		// Translation
		status += Translate_Bicubic(X, Y, flags);
	}
	return(status);
}

// Difference between two sub-images which are supposed to be equivalent.
//   xs, ys - shift of the image 'i' with respect to this. 
//   Saves result in the file called 'name' if second argument is given.
// Uses bilinear interpolation.
double BMPImageG::DifferenceErrorDouble(BMPImage *I, double xs, double ys, 
	int *overlap, const char *name)
{
	int i, j, n, vld;
	double dif = 0.0, v, vi;
	double ii, jj;
	BMPImageG *ig = (BMPImageG *) I;
	int wi = ig->w, hi = ig->h;
	BMPImageG *d = nullptr;
	//char stg[256];
	
	if(name)
		d = new BMPImageG(w, h);
	
	n = 0;
	// Every pixel in *this image is checked.
	// Pixel (i,j) gets contribution from location (ii,jj) of image I.
	for(j = 0; j<h; j++) {
		jj = (double)j+ys;
		if(jj<0.0 || jj>=hi)
			continue;
		for(i = 0; i<w; i++) {
			ii = (double)i+xs;
			if(ii<0.0 || ii>=wi)
				continue;
			//vi = ig->pfBilinear(ii, jj, &vld);
			vi = ig->Bilinear(ii, jj, &vld);
			if(valid(i, j)) {
				v = (double) im[j*w+i];
				if(vld) {
					dif += fabs(v-vi);
					n++;
					if(d)
						d->im[j*w+i] = (u_char) (0.5*(v+vi));
				} else {
					if(d)
						d->im[j*w+i] = (u_char) v;
				}
			} else {
				if(vld && d)
					d->im[j*w+i] = (u_char) vi;
			}
		}
	}
	if(debug)
		fprintf(stderr, "difference_error_double: total dif=%lf pixels=%d\n", dif, n);
	if(n)
		dif /= (double) n;
	else {
		fprintf(stderr, "No overlap between images.\n");
		dif = 0.0;
	}
	if(debug) {
		//fprintf(stderr, "  dif=%lf\n", dif);
		//sprintf_s(stg, 256, "I1.%s", EXTN);
		//Save(stg);
		//sprintf_s(stg, 256, "I2.%s", EXTN);
		//ig->Save(stg);
		//fprintf(stderr, "Images \"I1.bmp\" and \"I2.bmp\" saved.\n");
	}
		
	if(d) {
		d->Save(name);
		//sprintf_s(error, 256, "Image \"%s\" saved.\n", name);
		delete d;
	}
	*overlap = n;
	ig->ResetPrefilter();
	
	return(dif); 
}

// 1: by pi/2, 2: by pi, 3: by -pi/2
void BMPImageG::RotateQuarter(int nr_of_quarters)
{
	int new_w, new_h;
	if(nr_of_quarters == 2) {
		new_w = w; new_h = h;
	} else {
		new_w = h; new_h = w;
	}
	u_char *tmp = (u_char*) new char[new_w*new_h];
	int i, j;
	switch(nr_of_quarters) {
	case 1:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			tmp[j*new_w+i] = im[i*w+(w-1-j)];
		}}
		break;
	case 2:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			tmp[j*new_w+i] = im[(h-1-j)*w+(w-1-i)];
		}}
		break;
	case 3:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			tmp[j*new_w+i] = im[(h-1-i)*w+j];
		}}
		break;
	}
	delete [] im;
	im = tmp;
	w = new_w; h = new_h;
}

void BMPImageG::Blur_1(int kernel, int times)
{
	int i, j, ii, jj, half = (kernel-1)/2;
	long sum, size = kernel*kernel;
	
	u_char *tmp = (u_char*) new char[w*h];
	
	for(int t = 0; t<times; t++) {
		memcpy(tmp, im, sizeof(u_char)*w*h);
	
		for(j = half; j<h-half; j++) {
			for(i = half; i<w-half; i++) {
				sum = 0;
				for(jj = j-half; jj<=j+half; jj++) {
					for(ii = i-half; ii<=i+half; ii++) {
						sum += tmp[jj*w+ii];
					}
				}
				im[j*w+i] = u_char(sum/size);
			}
		}
	}
	
	delete [] tmp;
}

void BMPImageG::Blur_2(int kernel, int times)
{
	if(kernel<3) {
		//sprintf_s(error, 256, "Blur kernel<3, returning.\n");
		return;
	}
	if(times<1) {
		//sprintf_s(error, 256, "Blur times<1, returning.\n");
		return;
	}
	if(kernel%2==0) {
		kernel++;
   cout << "Kernel even, set to " << kernel << endl;
		//sprintf_s(error, 256, "Kernel even, set to %d.\n", kernel);
	}
	int i, j, ii, jj, half = (kernel-1)/2, count;
	long sum;
	
	u_char *tmp = (u_char*) new char[w*h];
	
	for(int t = 0; t<times; t++) {
		memcpy(tmp, im, sizeof(u_char)*w*h);
	
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				sum = 0; count = 0;
				for(jj = j-half; jj<=j+half; jj++) {
					if(jj<0 || jj>=h) continue;
					for(ii = i-half; ii<=i+half; ii++) {
						if(ii<0 || ii>=w) continue;
						sum += tmp[jj*w+ii];
						count++;
					}
				}
				im[j*w+i] = u_char(sum/count);
			}
		}
	}
	
	delete [] tmp;
}

void BMPImageG::Blur(int kernel, int times)
{
	if(kernel<3) {
		fprintf(stderr, "Blur kernel<3, returning.\n");
		return;
	}
	if(times<1) {
		fprintf(stderr, "Blur times<1, returning.\n");
		return;
	}
	if(kernel%2==0) {
		kernel++;
		//sprintf_s(error, 256, "Kernel even, set to %d.\n", kernel);
	}
	int i, j, ii, jj, half = (kernel-1)/2, count;
	long sum;
	
	u_char *tmp = (u_char*) new char[w*h];
	
	for(int t = 0; t<times; t++) {
		memcpy(tmp, im, sizeof(u_char)*w*h);
	
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				if(tmp[j*w+i]==INVALID) continue;
				sum = 0; count = 0;
				for(jj = j-half; jj<=j+half; jj++) {
					if(jj<0 || jj>=h) continue;
					for(ii = i-half; ii<=i+half; ii++) {
						if(ii<0 || ii>=w) continue;
						if(tmp[jj*w+ii]==INVALID) continue;
						sum += tmp[jj*w+ii];
						count++;
					}
				}
				im[j*w+i] = u_char(sum/count);
			}
		}
	}
	
	delete [] tmp;
}

// Takes image, floating position of the point of interest,
// returns bilinearly interpolated value cast to u_char.
// Position is guaranteed to be within the image. 
// Sep 2001: return value is now double
double BMPImageG::Bilinear(double dx, double dy, int *vld)
{
	int col, row, k;
	double v, invD = (double) INVALID;
	u_char s00, s10, s01, s11;
	
	if(vld) *vld = 0;
	// Check for outside of image 
	if(dx<0.0 || dx>double(w-1) || dy<0.0 || dy>double(h-1)) return(invD);
	
	// Integer parts 
	col = FLOOR(dx); row = FLOOR(dy);
	// If interested in pixel validity, check it
	if(vld && !valid(col, row)) 
		return(invD);
	k = row*w+col;
	// Fractional parts 
	dx -= (double) col; dy -= (double) row;
	if(dx<1.e-5 && dy<1.e-5) { // practically coinsides with a node
		if(vld && im[k]!=INVALID) *vld = 1;
		return((double) im[k]);
	}

	// If interested in pixel validity, check validity of all 4 pixels
	if(vld && im[k]==INVALID) return(invD);
	s00 = im[k];
	if(col!=w-1) {
		if(vld && im[k+1]==INVALID) {*vld=0; return(invD);}
		s10 = im[k+1];
	} else {
		s10 = im[k];
	}
	if(row!=h-1) {
		if(vld && im[k+w]==INVALID) {*vld=0; return(invD);}
		s01 = im[k+w];
	} else {
		s01 = im[k];
	}
	if(col!=(w-1) && row!=(h-1)) {
		if(vld && im[k+w+1]==INVALID) {*vld=0; return(invD);}
		s11 = im[k+w+1];
	} else {
		s11 = im[k];
	}
	if(vld) *vld = 1; // Validate pixel
	v = (double) s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}

// Special case when all 4 nodes are guaranteed to exist
double BMPImageG::Bilinear(double dx, double dy, int tl_i, int tl_j)
{
	int k;
	double v;
	u_char s00, s10, s01, s11;
	
	k = tl_j*w+tl_i;
	// Fractional parts 
	dx -= (double) tl_i; dy -= (double) tl_j;
	if(dx<1.e-5 && dy<1.e-5) // practically coinsides with a node
		return((double) im[k]);
	if(fabs(dx-1.0)<1.e-5 && dy<1.e-5) 
		return((double) im[k+1]);
	if(dx<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return((double) im[k+w]);
	if(fabs(dx-1.0)<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return((double) im[k+w+1]);

	s00 = im[k];
	s10 = im[k+1];
	s01 = im[k+w];
	s11 = im[k+w+1];
	v = (double) s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}

double BMPImageG::Bicubic(double x, double y, int *vld)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	double v = 0.0, X, Y;

	// Surrounding pixels: (I,J)->(I+1,J+1)
	int If = (int)floor(x), Jf = (int)floor(y);
	int Ic = (int)ceil(x), Jc = (int)ceil(y);
		if(Jf<0) Jf = 0;
		if(Jc>h-1) Jc = h-1;
		if(If<0) If = 0;
		if(Ic>w-1) Ic = w-1;
	// If, Jf, Ic, Jc are guaranteed not to go out of range
	double V[16]; // 4 values, 4 X-derivatives, 4 Y-derivatives, 4 cross-derivatives

	V[ 0] = (double)im[If+w*Jf];
	V[ 1] = (double)im[Ic+w*Jf];
	V[ 2] = (double)im[If+w*Jc];
	V[ 3] = (double)im[Ic+w*Jc];
	V[ 4] = (If<1)   ? 0.0 : 0.5*((double)im[Ic+w*Jf]-im[If-1+w*Jf]);
	V[ 5] = (Ic>=w-1)   ? 0.0 : 0.5*((double)im[Ic+1+w*Jf]-im[If+w*Jf]);
	V[ 6] = (If<1) ? 0.0 : 0.5*((double)im[Ic+w*Jc]-im[If-1+w*Jc]);
	V[ 7] = (Ic>=w-1) ? 0.0 : 0.5*((double)im[Ic+1+w*Jc]-im[If+w*Jc]);
	V[ 8] = (Jf<1) ? 0.0 : 0.5*((double)im[If+w*Jc]-im[If+w*(Jf-1)]);
	V[ 9] = (Jf<1) ? 0.0 : 0.5*((double)im[Ic+w*Jc]-im[Ic+w*(Jf-1)]);
	V[10] = (Jc>=h-1) ? 0.0 : 0.5*((double)im[If+w*(Jc+1)]-im[If+w*Jf]);
	V[11] = (Jc>=h-1) ? 0.0 : 0.5*((double)im[Ic+w*(Jc+1)]-im[Ic+w*Jf]);
	V[12] = (If<1 || Jf<1) ? 0.0 : 0.25*((double)im[Ic+w*Jc]+im[If-1+w*(Jf-1)]-im[Ic+w*(Jf-1)]-im[If-1+w*Jc]);
	V[13] = (Ic>=w-1 || Jf<1) ? 0.0 : 0.25*((double)im[Ic+1+w*Jc]+im[If+w*(Jf-1)]-im[Ic+1+w*(Jf-1)]-im[If+w*Jc]);
	V[14] = (If<1|| Jc>=h-1) ? 0.0 : 0.25*((double)im[Ic+w*(Jc+1)]+im[If-1+w*Jf]-im[Ic+w*Jf]-im[If-1+w*(Jc+1)]);
	V[15] = (Ic>=w-1 || Jc>=h-1) ? 0.0 : 0.25*((double)im[Ic+1+w*(Jc+1)]+im[If+w*Jf]-im[Ic+1+w*Jf]-im[If+w*(Jc+1)]);

	if(vld) *vld = 1;
	
	NEWMAT::ColumnVector beta(16);
	beta<<V[0]<<V[1]<<V[2]<<V[3]<<V[4]<<V[5]<<V[6]<<V[7]<<V[8]<<V[9]<<V[10]<<V[11]<<V[12]<<V[13]<<V[14]<<V[15];

	NEWMAT::ColumnVector alpha = M_bicubic*beta;

	X = x-floor(x);
	Y = y-floor(y);
	for(int k = 0; k<16; k++) {
		v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
	}
	if(v<0.0) v = 0.0;
	if(v>255.0) v = 255.0;
	
	return(v);
}

Y_Rect<int> BMPImageG::DetectMargins()
{
	int i, j, has;
	Y_Rect<int> r;
	 
	for(i = 0; i<w; i++) {
		for(j = 0, has = 0; j<h; j++)
			has += (get_pixel(i, j)!=INVALID);
		if(has) break;
	}
	r.x = i;
	
	for(i = w-1; i>=0; i--) {
		for(j = 0, has = 0; j<h; j++)
			has += (get_pixel(i, j)!=INVALID);
		if(has) break;
	}
	r.w = i;
	
	for(j = 0; j<h; j++) {
		for(i = 0, has = 0; i<w; i++)
			has += (get_pixel(i, j)!=INVALID);
		if(has) break;
	}
	r.y = j;
	
	for(j = h-1; j>=0; j--) {
		for(i = 0, has = 0; i<w; i++)
			has += (get_pixel(i, j)!=INVALID);
		if(has) break;
	}
	r.h = j;
	
	r.w += -r.x+1;
	r.h += -r.y+1;
	
	return(r);
}

// Starting with (x,y) colour all pixels with 'fill' until any other
int BMPImageG::SeedFill(int seedx, int seedy, u_char fill, int s_length, Y_Rect<int>* pR)
{
	int x, y, savex, savey, xleft, xright, num = 0;
	int* s = new int[s_length];
	u_char col;
	int slen = 0;
	u_char seedClr = im[seedy*w+seedx];

// push on the stack
	s[slen] = seedx; slen++; s[slen] = seedy; slen++;

	while(slen!=0) {
		// pop seed onto stack 
		x = s[slen-2]; y = s[slen-1]; slen -= 2;
		if(pR && pR->Inside(x,y)==0)
			return(-1);
		im[y*w+x] = fill; ++num;
		// get extreme left and right members of fill scan line
		// fill to the right of the seed pixel 
		savex = x;
		x++;
		col = im[y*w+x];

		while(col==seedClr && x<w) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			im[y*w+x] = fill; ++num; x++;
			col = im[y*w+x];
		}
		xright = x - 1; x = savex;

		// fill to the left of the seed pixel
		x--;
		col = im[y*w+x];
		while(col==seedClr && x>=0) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			im[y*w+x] = fill; ++num; x--;
			col = im[y*w+x];
		}
		xleft = x + 1;
		x = savex;

		// check scan line above 
		savey = y;
		x = xleft;

		if(y<h-1) {
			y++;
			col = im[y*w+x];
			do {
				while(((col!=seedClr) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = im[y*w+x];
				}
				if(col==seedClr && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col==seedClr) && (x<=xright) && (x<w)) {
							x++;
							col = im[y*w+x];
						}
						s[slen] = x-1; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);
			x = savex; y = savey;
		}

		// check scan line below
		y = savey; x = xleft;

		if(y>0) {
			y--;
			col = im[y*w+x];
			do {
				while(((col!=seedClr) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = im[y*w+x];
				}
				if(col==seedClr && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col==seedClr) && (x<=xright) && (x<w)) {
							x++;
							col = im[y*w+x];
						}
						s[slen] = x-1; s[slen+1] = y; slen += 2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);

			x = savex; y = savey;
		}
	}
	delete [] s;
	return(num);
}

// Starting with (x,y) colour all pixels with 'fill' until 'bound'
int BMPImageG::SeedFill(int seedx, int seedy, u_char bound, u_char fill, int s_length, Y_Rect<int>* pR)
{
	int x, y, savex, savey, xleft, xright, num = 0;
	int* s = new int[s_length];
	u_char col;
	int slen = 0;

// push on the stack
	s[slen] = seedx; slen++; s[slen] = seedy; slen++;

	while(slen!=0) {
		// pop seed onto stack 
		x = s[slen-2]; y = s[slen-1]; slen -= 2;
		if(pR && pR->Inside(x,y)==0)
			return(-1);
		im[y*w+x] = fill; ++num;
		// get extreme left and right members of fill scan line
		// fill to the right of the seed pixel 
		savex = x;
		x++;
		col = im[y*w+x];

		while(col!=bound && x<w) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			im[y*w+x] = fill; ++num; x++;
			col = im[y*w+x];
		}
		xright = x - 1; x = savex;

		// fill to the left of the seed pixel
		x--;
		col = im[y*w+x];
		while(col!=bound && x>=0) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			im[y*w+x] = fill; ++num; x--;
			col = im[y*w+x];
		}
		xleft = x + 1;
		x = savex;

		// check scan line above 
		savey = y;
		x = xleft;

		if(y<h-1) {
			y++;
			col = im[y*w+x];
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = im[y*w+x];
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = im[y*w+x];
						}
						s[slen] = x-1; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);
			x = savex; y = savey;
		}

		// check scan line below
		y = savey; x = xleft;

		if(y>0) {
			y--;
			col = im[y*w+x];
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = im[y*w+x];
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = im[y*w+x];
						}
						s[slen] = x-1; s[slen+1] = y; slen += 2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);

			x = savex; y = savey;
		}
	}
	delete [] s;
	return(num);
}

int BMPImageG::SeedFill(int seedx, int seedy, u_char bound, u_char fill, vector<Point2<int> >& locations, int s_length, Y_Rect<int>* pR)
{
	int x, y, savex, savey, xleft, xright, num = 0;
	int* s = new int[s_length];
	u_char col;
	int slen = 0;

// push on the stack
	s[slen] = seedx; slen++; s[slen] = seedy; slen++;

	while(slen!=0) {
		// pop seed onto stack 
		x = s[slen-2]; y = s[slen-1]; slen -= 2;
		if(pR && pR->Inside(x,y)==0)
			return(-1);
		im[y*w+x] = fill; ++num;
		locations.push_back(Point2<int>(x, y));

		// get extreme left and right members of fill scan line
		// fill to the right of the seed pixel 
		savex = x; x++;
		col = im[y*w+x];

		while(col!=bound && x<w) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			im[y*w+x] = fill; ++num;
			locations.push_back(Point2<int>(x, y));
			x++;
			col = im[y*w+x];
		}
		xright = x - 1;
		x = savex;

		// fill to the left of the seed pixel
		x--;
		col = im[y*w+x];
		while(col!=bound && x>=0) {
			if(pR && pR->Inside(x,y)==0)
				return(-1);
			im[y*w+x] = fill; ++num;
			locations.push_back(Point2<int>(x, y));
			x--;
			col = im[y*w+x];
		}
		xleft = x + 1; x = savex;

		// check scan line above 
		savey = y; x = xleft;

		if(y<h-1) {
			y++;
			col = im[y*w+x];
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = im[y*w+x];
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = im[y*w+x];
						}
						s[slen] = x-1; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);
			x = savex; y = savey;
		}

		// check scan line below
		y = savey; x = xleft;

		if(y>0) {
			y--;
			col = im[y*w+x];
			do {
				while(((col==bound) || (col==fill)) && (x<w-1) && (x<xright)) {
					x++;
					col = im[y*w+x];
				}
				if(col!=bound && col!=fill) {
					if(x==xright || x==w-1) {
						s[slen] = x; s[slen+1] = y; slen+=2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
						break;
					} else {
						while((col!=bound) && (x<=xright) && (x<w)) {
							x++;
							col = im[y*w+x];
						}
						s[slen] = x-1; s[slen+1] = y; slen += 2;
						Y_Utils::IncreaseIntBufferIfNeeded(s, s_length, slen, 16, 4);
					}
				}
			}
			while(x<xright);

			x = savex; y = savey;
		}
	}
	delete [] s;
	return(num);
}

void BMPImageG::MarkPoint(double x, double y, int rad, u_char clr)
{
	int I = (int) (x+0.5), J = (int) (y+0.5);
	double d2, r2 = (rad-1)*(rad-1)/4;

	if(rad<2) {
		this->set_pixel(I, J, clr);
	} else {
		for(int j = J-rad; j<=J+rad; j++) {
			for(int i = I-rad; i<=I+rad; i++) {
				d2 = (double) (i-I)*(i-I)+(j-J)*(j-J);
				if(d2<=r2) {
					this->set_pixel(i, j, clr);
				}
			}
		}
	}
}

/*
BMPImageG *BMPImageG::Undistort_A_Bilinear(Y_Camera *C)
{
	BMPImageG *ui = new BMPImageG(w, h);
	ui->focal_length = focal_length;
	u_char p;
	Point2<int> d;
	Point2<double> u;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d.Set(i, j);
			u = C->DistortPixel(d);
			if(r.Inside(u)) {
				//p = u_char(pfBilinear(u.X(), u.Y()));
				p = u_char(Bilinear(u.X(), u.Y()));
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

BMPImageG *BMPImageG::Distort_A_Bilinear(Y_Camera *C)
{
	BMPImageG *ui = new BMPImageG(w, h);
	ui->focal_length = focal_length;
	u_char p;
	Point2<int> u;
	Point2<double> d;
	bool fail;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			u.Set(i, j);
			d = C->UndistortPixel(u, &fail);
			if(!fail && r.Inside(d)) {
				//p = u_char(pfBilinear(d.X(), d.Y()));
				p = u_char(Bilinear(d.X(), d.Y()));
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

BMPImageG *BMPImageG::Undistort_A_Bicubic(Y_Camera *C)
{
	BMPImageG *ui = new BMPImageG(w, h);
	ui->focal_length = focal_length;
	u_char p;
	Point2<int> d;
	Point2<double> u;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d.Set(i, j);
			u = C->DistortPixel(d);
			if(r.Inside(u)) {
				//p = u_char(pfBilinear(u.X(), u.Y()));
				p = (u_char)Bicubic(u.X(), u.Y());
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}

BMPImageG *BMPImageG::Distort_A_Bicubic(Y_Camera *C)
{
	BMPImageG *ui = new BMPImageG(w, h);
	ui->focal_length = focal_length;
	u_char p;
	Point2<int> u;
	Point2<double> d;
	bool fail;
	
	Y_Rect<int> r = GetRect();
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			u.Set(i, j);
			d = C->UndistortPixel(u, &fail);
			if(!fail && r.Inside(d)) {
				//p = u_char(pfBilinear(d.X(), d.Y()));
				p = (u_char)Bicubic(d.X(), d.Y());
			} else {
				p = INVALID;
			}
			ui->set_pixel(i, j, p);
		}
	}
	return(ui);
}
*/

void BMPImageG::Prefilter()
{
	int i, j;
	double tau = BMPImage::pf_tau, b = 1.0/(1.0-tau), v;
	
	pf_data = new double[w*h];
	
	for(j = 0; j<h; j++) {
		v = get_pixel(0, j);
		pf_data[j*w+0] = b*(get_pixel(0, j)-tau*v);
		for(i = 1; i<w; i++) {
			v = pf_data[j*w+i-1];
			pf_data[j*w+i] = b*(get_pixel(i, j)-tau*v);
		}
	}
	
	for(i = 0; i<w; i++) {
		v = get_pixel(i, 0);
		pf_data[0*w+i] = b*(pf_data[0*w+i]-tau*v);
		for(j = 1; j<h; j++) {
			v = pf_data[(j-1)*w+i];
			pf_data[j*w+i] = b*(pf_data[j*w+i]-tau*v);
		}
	}
	prefilter = 1; // ready for bilinear interpolation
}

double BMPImageG::pfBilinear(double dx, double dy, int *vld)
{
	// Called only first time. Valid until done outside.
	if(!prefilter)
		Prefilter();
	
	int col, row, k;
	double v;
	double s00, s10, s01, s11;
	
	dx -= BMPImage::pf_tau; dy -= BMPImage::pf_tau;
	
	/* Integer parts */
	col = FLOOR(dx); row = FLOOR(dy);
	/* Fractional parts */
	dx -= (double) col; dy -= (double) row;
	k = row*w+col;
	// return _INVALID_DOUBLE, if outside the rectangle
	if(row<0 || row>h-1 || col<0 || col>w-1) {
		if(vld) *vld = 0;
		return(Y_Utils::_INVALID_DOUBLE);
	}
	// If interested in pixel validity, check validity of all 4 pixels
	if(vld && isnan(pf_data[k])) { *vld = 0; return(Y_Utils::_INVALID_DOUBLE); }
	s00 = pf_data[k];
	if(col!=w-1) {
		if(vld && isnan(pf_data[k+1])) {*vld = 0; return(Y_Utils::_INVALID_DOUBLE); }
		s10 = pf_data[k+1];
	} else {
		s10 = pf_data[k];
	}
	if(row!=h-1) {
		if(vld && isnan(pf_data[k+w])) {*vld = 0; return(Y_Utils::_INVALID_DOUBLE); }
		s01 = pf_data[k+w];
	} else {
		s01 = pf_data[k];
	}
	if(col!=(w-1) && row!=(h-1)) {
		if(vld && isnan(pf_data[k+w+1])) {*vld = 0; return(Y_Utils::_INVALID_DOUBLE); }
		s11 = pf_data[k+w+1];
	} else {
		s11 = pf_data[k];
	}
	if(vld) *vld = 1; // Validate pixel
	v = (double) s00;
	v += dx*(s10-s00);
	v += dy*(s01-s00);
	v += dx*dy*(s11+s00-s10-s01);
	return(v);
}




