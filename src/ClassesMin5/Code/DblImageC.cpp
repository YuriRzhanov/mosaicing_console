#include <stdlib.h>
#include <math.h>
#include "DblImageC.h"

using namespace std;



DblImageC::DblImageC(int _w, int _h)
	: DblImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 3;
	d_im = new double[3*w*h];
	for(int i = 0; i<3*w*h; i++) {
		d_im[i] = Y_Utils::_INVALID_DOUBLE;
	}
}

DblImageC::DblImageC(int _w, int _h, double* _d_im)
	: DblImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 3;
	d_im = new double[3*w*h];
	memcpy(d_im, _d_im, 3*w*h*sizeof(double));
}

DblImageC::DblImageC(int _w, int _h, double *_r, double *_g, double *_b)
	: DblImage(_w, _h)
{
	ranges_set = 0; even_zoom = 0; z = 3;
	d_im = new double[3*w*h];
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			d_im[3*(j*w+i)+0] = _r[j*w+i];
			d_im[3*(j*w+i)+1] = _g[j*w+i];
			d_im[3*(j*w+i)+2] = _b[j*w+i];
		}
	}
}

// Copy of everything excluding content
DblImageC::DblImageC(DblImageC *d)
{
	w = d->w; h = d->h; fail = d->fail; focal_length = d->focal_length;
	d_im = new double[3*w*h];
	ranges_set = d->ranges_set; v_min = d->v_min; v_max = d->v_max;
	even_zoom = d->even_zoom; z = 3;
	for(int i = 0; i<3*w*h; i++) {
		d_im[i] = Y_Utils::_INVALID_DOUBLE;
	}
}

DblImageC::DblImageC(BMPImageC *img, int hasInvalid)
{
	w = img->w; h = img->h; fail = img->fail; focal_length = img->focal_length;
	d_im = new double[3*w*h];
	ranges_set = 0; even_zoom = 0; z = 3;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(hasInvalid && img->valid(i, j)==0) {
				d_im[3*(j*w+i)+0] = Y_Utils::_INVALID;
				d_im[3*(j*w+i)+1] = Y_Utils::_INVALID;
				d_im[3*(j*w+i)+2] = Y_Utils::_INVALID;
			} else {
				d_im[3*(j*w+i)+0] = (double) img->im[3*(j*w+i)+0];
				d_im[3*(j*w+i)+1] = (double) img->im[3*(j*w+i)+1];
				d_im[3*(j*w+i)+2] = (double) img->im[3*(j*w+i)+2];
			}
		}
	}
}

DblImageC::DblImageC(const char *name, bool old)
{
	d_im = nullptr; ranges_set = 0; even_zoom = 0; z = 3;
	if(Read(name, old)==-1)
		fail = 1;
}

DblImageC *DblImageC::Copy()
{
	DblImageC *c = new DblImageC(w, h);
	memcpy(c->d_im, d_im, 3*w*h*sizeof(double));
	c->SetFocalLength(focal_length);
	c->ranges_set = ranges_set; c->v_min = v_min; c->v_max = v_max;
	c->even_zoom = even_zoom; c->z = 3;
	return(c);
}

void DblImageC::Copy(DblImageC *d)
{
	if(w!=d->w || h!=d->h) {
		delete [] d_im;
		d_im = new double[d->w*d->h];
		w = d->w; h = d->h;
	}
	memcpy(d_im, d->d_im, 3*w*h*sizeof(double));
	ranges_set = d->ranges_set; v_min = d->v_min; v_max = d->v_max;
	even_zoom = d->even_zoom; z = 3;
	focal_length = d->focal_length;
}

int DblImageC::Read(const char *name, bool old)
{
	if(DblImage::Read(name, old)) // sets dd_imensions, z, name
		return(-1);
	FILE *fp;
#ifdef _MSC_VER
	if(fopen_s(&fp, name, "rb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
#else
  fp = fopen(name, "rb");
	if(!fp) {
		sprintf(error, "Cannot open \"%s\".\n", name);
		return(-1);
	}
#endif
	if(old) {
		if(ReadDblHeader_old(fp))
			return(-1);
	} else {
		if(ReadHeader(fp))
			return(-1);
	}
	if(d_im!=nullptr)
		delete [] d_im;
	d_im = new double[3*w*h];
	if(fread(d_im, 3*w*h*sizeof(double), 1, fp)!=1) {
		//sprintf_s(error, 256, "Read \"%s\" fails.\n", name); 
    return(-1);
	}
	if((old && endian) || (!old && !endian)) { // Convert to the opposite byte order
		for(int i = 0; i<3*w*h; i++)
			Y_Utils::Swap8Byte((u_char *) &d_im[i]);
	}
	return(0);
}

// Prior to Feb 2005, storage was: allR-allG-allB
int DblImageC::ReadOld(const char *name)
{
	int _w, _h;
	FILE *fp;
#ifdef _MSC_VER
	if(fopen_s(&fp, name, "rb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
#else
  fp = fopen(name, "rb");
	if(!fp) {
		sprintf(error, "Cannot open \"%s\".\n", name);
		return(-1);
	}
#endif
	if(fread(&_w, sizeof(int), 1, fp)!=1) { 
		//sprintf_s(error, 256, "Read \"%s\" fails.\n", name); 
        return(-1);
	}
	if(fread(&_h, sizeof(int), 1, fp)!=1) { 
		//sprintf_s(error, 256, "Read \"%s\" fails.\n", name); 
        return(-1);
	}
	if(d_im!=nullptr && ((w!=_w) || (h!=_h))) {
		delete [] d_im;
	}
	w = _w; h = _h;
	d_im = new double[3*w*h];
	double *buf = new double[w*h];
	if(fread(buf, w*h*sizeof(double), 1, fp)!=1) {
		//sprintf_s(error, 256, "Read \"%s\" fails.\n", name); 
        return(-1);
	}
	int i, j;
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			d_im[3*(j*w+i)+0] = buf[j*w+i];
		}
	}
	if(fread(buf, w*h*sizeof(double), 1, fp)!=1) {
		//sprintf_s(error, 256, "Read \"%s\" fails.\n", name); 
        return(-1);
	}
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			d_im[3*(j*w+i)+1] = buf[j*w+i];
		}
	}
	if(fread(buf, w*h*sizeof(double), 1, fp)!=1) {
		//sprintf_s(error, 256, "Read \"%s\" fails.\n", name); 
        return(-1);
	}
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			d_im[3*(j*w+i)+2] = buf[j*w+i];
		}
	}
	delete [] buf;
	return(0);
}

int DblImageC::Save(const char *name, bool old)
{
	FILE *fp;
#ifdef _MSC_VER
	if(fopen_s(&fp, name, "wb")) {
		sprintf_s(error, 256, "Cannot open \"%s\".\n", name);
		return(-1);
	}
#else
  fp = fopen(name, "wb");
	if(!fp) {
		sprintf(error, "Cannot open \"%s\".\n", name);
		return(-1);
	}
#endif
	if(old) {
		if(WriteDblHeader_old(fp))
			return(-1);
	} else {
		if(WriteHeader(fp))
			return(-1);
	}
	if((old && endian) || (!old && !endian)) { // Convert to the opposite byte order
		double *_d_im = new double[3*w*h];
		memcpy(_d_im, d_im, 3*w*h*sizeof(double));
		for(int i = 0; i<3*w*h; i++)
			Y_Utils::Swap8Byte((u_char *) &_d_im[i]);
		if(fwrite(_d_im, 3*w*h*sizeof(double), 1, fp)!=1) {
			//sprintf_s(error, 256, "Cannot write to \"%s\".\n", name);
			return(-1);
		}
		delete [] _d_im;
	} else {
		if(fwrite(d_im, 3*w*h*sizeof(double), 1, fp)!=1) {
			//sprintf_s(error, 256, "Cannot write to \"%s\".\n", name);
			return(-1);
		}
	}
	fclose(fp);
	return(0);
}

int DblImageC::PutAt(DblImageC *p, int x, int y)
{
	for(int j = 0; j<p->h; j++) {
		if((y+j)<0 || (y+j)>h)
			continue;
		for(int i = 0; i<p->w; i++) {
			if((x+i)<0 || (x+i)>w)
				continue;
			memcpy(&d_im[3*((y+j)*w+(x+i))], &(p->d_im[3*(j*p->w+i)]), 3*sizeof(double));
		}
	}
	return(0);
}

// invalid, if any corner is invalid
int DblImageC::valid(double x, double y)
{
	int i = int(x), j = int(y);
	if(valid(i,j) && valid(i+1,j) && valid(i,j+1) && valid(i+1,j+1))
		return(1);
	return(0);
}

int DblImageC::valid(int x, int y) 
{
	return(Y_Image::valid(x, y) && !isnan(d_im[3*(y*w+x)+0]) && 
		!isnan(d_im[3*(y*w+x)+1]) && !isnan(d_im[3*(y*w+x)+2]));
}

// valid pixel, neighbouring invalid
int DblImageC::border(int x, int y)
{
	if(!valid(x, y)) return(0);
	for(int j = y-1; j<=y+1; j++) {
		for(int i = x-1; i<=x+1; i++) {
			if(!valid(i, j)) return(1);
		}
	}
	return(0);
}

void DblImageC::SetInvalid() 
{ 
	for(int j = 0; j<3*w*h; j++) {
		d_im[j] = Y_Utils::_INVALID_DOUBLE;
	}
}

int DblImageC::SaveBMP(const char *name, int rescale)
{
	BMPImageC *img = Colour(rescale);
	int status = img->Save(name);
	delete img;
	return(status);
}

int DblImageC::SaveBMP(const char *name, double min, double max)
{
	BMPImageC *img = Colour(min, max);
	int status = img->Save(name);
	delete img;
	return(status);
}

/* Cut off subimage, taking care of image extent.
   at_x/y - top-left corner of cut
   sw/sh - requested size
   Area is initiated with invalid.
*/
DblImageC *DblImageC::CutSubimage(int at_x, int at_y, int sw, int sh)
{
	int i, j, ii, jj;
	DblImageC *s = new DblImageC(sw, sh);
	for(i = 0; i<3*sw*sh; i++)
		s->d_im[i] = Y_Utils::_INVALID_DOUBLE;
	s->SetFocalLength(focal_length);
	s->ranges_set = ranges_set; s->v_min = v_min; s->v_max = v_max;
		
	for(j = 0; j<h; j++) {
		jj = j-at_y;
		if(jj<0 || jj>sh-1)
			continue;
		for(i = 0; i<w; i++) {
			ii = i-at_x;
			if(ii<0 || ii>sw-1)
				continue;
			memcpy(&(s->d_im[3*(jj*sw+ii)]), &d_im[3*(j*w+i)], 3*sizeof(double));
		}
	}
	return(s);
}

DblImageC *DblImageC::CutSubimage(Y_Rect<int> r)
{
	return(CutSubimage(r.x, r.y, r.w, r.h));
}

Y_RGB<double> DblImageC::Bilinear(double dx, double dy, int *vld)
{
	int col, row, k;
	double v[3];
	double s00[3], s10[3], s01[3], s11[3];
	
	if(vld) *vld = 0;
	/* Integer parts */
	col = FLOOR(dx); row = FLOOR(dy);
	/* Fractional parts */
	dx -= (double) col; dy -= (double) row;
	k = row*w+col;
	// return INVALID, if outside the rectangle
	if(row<0 || row>h-1 || col<0 || col>w-1) {
		if(vld) *vld = 0;
		return(Y_RGB<double>::INVALID_RGBD);
	}
	// If interested in pixel validity, check validity of all 4 pixels
	if(vld && valid(col,row)==0) { *vld = 0; return(Y_RGB<double>::INVALID_RGBD); }
	s00[0] = d_im[3*k+0]; s00[1] = d_im[3*k+1]; s00[2] = d_im[3*k+2];
	if(col!=w-1) {
		if(vld && valid(col+1,row)==0) {*vld = 0; return(Y_RGB<double>::INVALID_RGBD); }
		s10[0] = d_im[3*(k+1)+0]; s10[1] = d_im[3*(k+1)+1]; s10[2] = d_im[3*(k+1)+2];
	} else {
		s10[0] = d_im[3*k+0]; s10[1] = d_im[3*k+1]; s10[2] = d_im[3*k+2];
	}
	if(row!=h-1) {
		if(vld && valid(col,row+1)==0) {*vld = 0; return(Y_RGB<double>::INVALID_RGBD); }
		s01[0] = d_im[3*(k+w)+0]; s01[1] = d_im[3*(k+w)+1]; s01[2] = d_im[3*(k+w)+2];
	} else {
		s01[0] = d_im[3*k+0]; s01[1] = d_im[3*k+1]; s01[2] = d_im[3*k+2];
	}
	if(col!=(w-1) && row!=(h-1)) {
		if(vld && valid(col+1,row+1)==0) {*vld = 0; return(Y_RGB<double>::INVALID_RGBD); }
		s11[0] = d_im[3*(k+w+1)+0]; s11[1] = d_im[3*(k+w+1)+1]; s11[2] = d_im[3*(k+w+1)+2];
	} else {
		s11[0] = d_im[3*k+0]; s11[1] = d_im[3*k+1]; s11[2] = d_im[3*k+2];
	}
	if(vld) *vld = 1; // Validate pixel
	v[0] = (double) s00[0]+dx*(s10[0]-s00[0])+dy*(s01[0]-s00[0])+
		dx*dy*(s11[0]+s00[0]-s10[0]-s01[0]);
	v[1] = (double) s00[1]+dx*(s10[1]-s00[1])+dy*(s01[1]-s00[1])+
		dx*dy*(s11[1]+s00[1]-s10[1]-s01[1]);
	v[2] = (double) s00[2]+dx*(s10[2]-s00[2])+dy*(s01[2]-s00[2])+
		dx*dy*(s11[2]+s00[2]-s10[2]-s01[2]);
	Y_RGB<double> V(v[0],v[1],v[2]);
	return(V);
}

// Special case when all 4 nodes are guaranteed to exist
Y_RGB<double> DblImageC::Bilinear(double dx, double dy, int tl_i, int tl_j)
{
	int k;
	double v[3];
	double s00[3], s10[3], s01[3], s11[3];
	
	k = tl_j*w+tl_i;
	// Fractional parts 
	dx -= (double) tl_i; dy -= (double) tl_j;
	if(dx<1.e-5 && dy<1.e-5) // practically coinsides with a node
		return(get_pixel(tl_i,tl_j));
	if(fabs(dx-1.0)<1.e-5 && dy<1.e-5) 
		return(get_pixel(tl_i+1,tl_j));
	if(dx<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(get_pixel(tl_i,tl_j+1));
	if(fabs(dx-1.0)<1.e-5 && fabs(dy-1.0)<1.e-5) 
		return(get_pixel(tl_i+1,tl_j+1));

	s00[0] = d_im[3*k+0]; s00[1] = d_im[3*k+1]; s00[2] = d_im[3*k+2];
	s10[0] = d_im[3*(k+1)+0]; s10[1] = d_im[3*(k+1)+1]; s10[2] = d_im[3*(k+1)+2];
	s01[0] = d_im[3*(k+w)+0]; s01[1] = d_im[3*(k+w)+1]; s01[2] = d_im[3*(k+w)+2];
	s11[0] = d_im[3*(k+w+1)+0]; s11[1] = d_im[3*(k+w+1)+1]; s11[2] = d_im[3*(k+w+1)+2];
	v[0] = (double) s00[0]+dx*(s10[0]-s00[0])+dy*(s01[0]-s00[0])+
		dx*dy*(s11[0]+s00[0]-s10[0]-s01[0]);
	v[1] = (double) s00[1]+dx*(s10[1]-s00[1])+dy*(s01[1]-s00[1])+
		dx*dy*(s11[1]+s00[1]-s10[1]-s01[1]);
	v[2] = (double) s00[2]+dx*(s10[2]-s00[2])+dy*(s01[2]-s00[2])+
		dx*dy*(s11[2]+s00[2]-s10[2]-s01[2]);
	Y_RGB<double> V(v[0],v[1],v[2]);
	return(V);
}

Y_RGB<double> DblImageC::Bicubic(double x, double y, int *vld)
{
	if(M_bicubic.Ncols()==0)
		InitBicubicMatrix();

	double v, X, Y;
	Y_RGB<double> V_out;

	// Surrounding pixels: (I,J)->(I+1,J+1)
	int If = (int)floor(x), Jf = (int)floor(y);
	int Ic = (int)ceil(x), Jc = (int)ceil(y);
		if(Jf<0) Jf = 0;
		if(Jc>h-1) Jc = h-1;
		if(If<0) If = 0;
		if(Ic>w-1) Ic = w-1;
	// If, Jf, Ic, Jc are guaranteed not to go out of range
	double V[16]; // 4 values, 4 X-derivatives, 4 Y-derivatives, 4 cross-derivatives
	if(vld) *vld = 1;
	NEWMAT::ColumnVector beta(16);
	int w3 = 3*w;

	for(int i = 0; i<3; i++) {
		V[ 0] = d_im[i+3*(If+w*Jf)];
		V[ 1] = d_im[i+3*(Ic+w*Jf)];
		V[ 2] = d_im[i+3*(If+w*Jc)];
		V[ 3] = d_im[i+3*(Ic+w*Jc)];
		V[ 4] = (If<1)   ? 0.0 : 0.5*(d_im[i+3*(Ic+w*Jf)]-d_im[i+3*(If-1+w*Jf)]);
		V[ 5] = (Ic>=w-1)   ? 0.0 : 0.5*(d_im[i+3*(Ic+1+w*Jf)]-d_im[i+3*(If+w*Jf)]);
		V[ 6] = (If<1) ? 0.0 : 0.5*(d_im[i+3*(Ic+w*Jc)]-d_im[i+3*(If-1+w*Jc)]);
		V[ 7] = (Ic>=w-1) ? 0.0 : 0.5*(d_im[i+3*(Ic+1+w*Jc)]-d_im[i+3*(If+w*Jc)]);
		V[ 8] = (Jf<1) ? 0.0 : 0.5*(d_im[i+3*(If+w*Jc)]-d_im[i+3*(If+w*(Jf-1))]);
		V[ 9] = (Jf<1) ? 0.0 : 0.5*(d_im[i+3*(Ic+w*Jc)]-d_im[i+3*(Ic+w*(Jf-1))]);
		V[10] = (Jc>=h-1) ? 0.0 : 0.5*(d_im[i+3*(If+w*(Jc+1))]-d_im[i+3*(If+w*Jf)]);
		V[11] = (Jc>=h-1) ? 0.0 : 0.5*(d_im[i+3*(Ic+w*(Jc+1))]-d_im[i+3*(Ic+w*Jf)]);
		V[12] = (If<1 || Jf<1) ? 0.0 : 0.25*(d_im[i+3*(Ic+w*Jc)]+d_im[i+3*(If-1+w*(Jf-1))]-d_im[i+3*(Ic+w*(Jf-1))]-d_im[i+3*(If-1+w*Jc)]);
		V[13] = (Ic>=w-1 || Jf<1) ? 0.0 : 0.25*(d_im[i+3*(Ic+1+w*Jc)]+d_im[i+3*(If+w*(Jf-1))]-d_im[i+3*(Ic+1+w*(Jf-1))]-d_im[i+3*(If+w*Jc)]);
		V[14] = (If<1|| Jc>=h-1) ? 0.0 : 0.25*(d_im[i+3*(Ic+w*(Jc+1))]+d_im[i+3*(If-1+w*Jf)]-d_im[i+3*(Ic+w*Jf)]-d_im[i+3*(If-1+w*(Jc+1))]);
		V[15] = (Ic>=w-1 || Jc>=h-1) ? 0.0 : 0.25*(d_im[i+3*(Ic+1+w*(Jc+1))]+d_im[i+3*(If+w*Jf)]-d_im[i+3*(Ic+1+w*Jf)]-d_im[i+3*(If+w*(Jc+1))]);

		beta<<V[0]<<V[1]<<V[2]<<V[3]<<V[4]<<V[5]<<V[6]<<V[7]<<V[8]<<V[9]<<V[10]<<V[11]<<V[12]<<V[13]<<V[14]<<V[15];

		NEWMAT::ColumnVector alpha = M_bicubic*beta;

		X = x-floor(x);
		Y = y-floor(y);
		v = 0.0;
		for(int k = 0; k<16; k++) {
			v += alpha(k+1)*pow(X, (double)(k%4))*pow(Y, floor(0.25*k));
		}
		if(v<0.0) v = 0.0;
		if(v>255.0) v = 255.0;
		V_out.p[i] = v;
	}
	return(V_out);
}

void DblImageC::Log()
{
	Y_RGB<double> V;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i,j);
			set_pixel(i, j, V.Log());
		}
	}
}

void DblImageC::ScaleShift(double gain, double offset)
{
	Y_RGB<double> V;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i,j);
			V.Set(V.p[0]*gain+offset, V.p[1]*gain+offset, V.p[2]*gain+offset);
			set_pixel(i, j, V);
		}
	}
}

DblImageC** DblImageC::Split(int factor)
{
	DblImageC** pImgs = new DblImageC*[factor];
	int i, j, new_w = w/factor, nw;
	for(int n = 0; n<factor; n++) {
		// Last image could have different number of columns from the rest
		nw = (n<(factor-1)) ? new_w : w-new_w*n;
		pImgs[n] = new DblImageC(nw, h);
		for(j = 0; j<h; j++) {
			for(i = 0; i<nw; i++) {
				pImgs[n]->set_pixel(i, j, get_pixel(n*nw+i, j));
			}
		}
	}
	return(pImgs);
}

bool DblImageC::Subtract(DblImageC* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	Y_RGB<double> V, V_;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i,j);
			V_ = pDbl->get_pixel(i,j);
			set_pixel(i, j, V-V_);
		}
	}
	return(true);
}

bool DblImageC::Add(DblImageC* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	Y_RGB<double> V, V_;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i,j);
			V_ = pDbl->get_pixel(i,j);
			set_pixel(i, j, V+V_);
		}
	}
	return(true);
}

bool DblImageC::Multiply(DblImageC* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	Y_RGB<double> V, V_, Vr;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i,j);
			V_ = pDbl->get_pixel(i,j);
			Vr.Set(V.p[0]*V_.p[0], V.p[1]*V_.p[1], V.p[2]*V_.p[2]);
			set_pixel(i, j, Vr);
		}
	}
	return(true);
}

bool DblImageC::Divide(DblImageC* pDbl)
{
	if(w!=pDbl->w || h!=pDbl->h)
		return(false);
	Y_RGB<double> V, V_, Vr;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			V = get_pixel(i,j);
			V_ = pDbl->get_pixel(i,j);
			if(V_.p[0]==0.0 || V_.p[1]==0.0 || V_.p[2]==0.0)
				Vr = Y_RGB<double>::INVALID_RGBD;
			else
				Vr.Set(V.p[0]/V_.p[0], V.p[1]/V_.p[1], V.p[2]/V_.p[2]);
			set_pixel(i, j, Vr);
		}
	}
	return(true);
}

DblImageG *DblImageC::Greyscale(char channel)
{
	DblImageG *t = new DblImageG(w, h);
	if(!t)
		return(t);
	t->focal_length = focal_length;
	int i, j, k;
	
	switch(channel) {
		case 'a':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->d_im[k] = ToGreyscale(d_im[3*k+0],d_im[3*k+1],d_im[3*k+2]);
				}
			}
			break;
		case 'r':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->d_im[k] = d_im[3*k+0];
				}
			}
			break;
		case 'g':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->d_im[k] = d_im[3*k+1];
				}
			}
			break;
		case 'b':
			for(j = 0; j<h; j++) {
				for(i = 0; i<w; i++) {
					k = j*w+i;
					t->d_im[k] = d_im[3*k+2];
				}
			}
			break;
		default:
			//sprintf_s(error, 256, "Channel must be 'a','r','g' or 'b'.\n");
			return(nullptr);
	}
	return(t);
}

int DblImageC::CombineGrayscales(DblImageG* pR, DblImageG* pG, DblImageG* pB)
{
	if(pR->w!=w || pR->h!=h || pG->w!=w || pG->h!=h || pB->w!=w || pB->h!=h) {
		//sprintf_s(error, 256, "Image dimensions do not match.\n");
		return(-1);
	}
	int k;
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			k = j*w+i;
			d_im[3*k+0] = pR->d_im[k];
			d_im[3*k+1] = pG->d_im[k];
			d_im[3*k+2] = pB->d_im[k];
		}
	}
	return(0);
}

BMPImageC *DblImageC::Colour(int rescale)
{
	int i, j;
	Y_RGB<double> v;
	Y_RGB<u_char> vsh;
	BMPImageC *img = new BMPImageC(w, h);
	
	// Find total range
	double a_max = -1.e9, a_min = 1.e9, scale = 1.0;
	if(rescale) {
		MinMax(a_min, a_max);
		if(a_min>=a_max)
			scale = 1.0;
		else {
			// used range is from 1 to 255, 0 is left for invalid pixels
			scale = (255.0-1.0)/(a_max-a_min);
		}
		//fprintf(stderr, "Range=[%lf:%lf] Scale=%lf\n", a_min, a_max, scale);
	} else {
		if(ranges_set) { // Use pre-set ranges
			a_min = v_min; a_max = v_max;
			scale = (255.0-1.0)/(a_max-a_min);
		}
	}
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				img->set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
			else {
				v = get_pixel(i, j);
				if(rescale || ranges_set) {
					v.p[0] = scale*(v.p[0]-a_min);
					v.p[1] = scale*(v.p[1]-a_min);
					v.p[2] = scale*(v.p[2]-a_min);
				}
				if(v.ToInt()<0.0)
					img->set_pixel(i, j, Y_RGB<u_char>(1,1,1)); // Shouldn't happen
				else {
					vsh = Y_RGB<u_char>((u_char)D_LIMIT_RANGE_CHAR(v.p[0]), (u_char)D_LIMIT_RANGE_CHAR(v.p[1]), (u_char)D_LIMIT_RANGE_CHAR(v.p[2]));
					if(vsh.ToInt()==0)
						vsh = Y_RGB<u_char>(1,1,1);
					img->set_pixel(i, j, vsh);
				}
			}
		}
	}
	return(img);
}

// Conversion to greyscale with given ranges.
// 'min' maps to 1, 'max' to 255
BMPImageC *DblImageC::Colour(double a_min, double a_max)
{
	int i, j;
	double scale;
	Y_RGB<double> v;
	Y_RGB<u_char> vsh;
	BMPImageC *img = new BMPImageC(w, h);
	
	if(a_min>=a_max)
			scale = 1.0;
	else {
		// used range is from 1 to 255, 0 is left for invalid pixels
		scale = (255.0-1.0)/(a_max-a_min);
	}
	//fprintf(stderr, "Range=[%lf:%lf] Scale=%lf\n", a_min, a_max, scale);
	
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			if(!valid(i, j))
				img->set_pixel(i, j, Y_RGB<u_char>::INVALID_RGB);
			else {
				v = get_pixel(i, j);
				v.p[0] = scale*(v.p[0]-a_min);
				v.p[1] = scale*(v.p[1]-a_min);
				v.p[2] = scale*(v.p[2]-a_min);
				if(v.ToInt()<0.0)
					img->set_pixel(i, j, Y_RGB<u_char>(1,1,1)); // Shouldn't happen
				else {
					vsh = Y_RGB<u_char>((u_char)D_LIMIT_RANGE_CHAR(v.p[0]), (u_char)D_LIMIT_RANGE_CHAR(v.p[1]), (u_char)D_LIMIT_RANGE_CHAR(v.p[2]));
					if(vsh.ToInt()==0)
						vsh = Y_RGB<u_char>(1,1,1);
					img->set_pixel(i, j, vsh);
				}
			}
		}
	}
	return(img);
}

int DblImageC::ZoomToDest_Bilinear(int dest_w, int dest_h)
{
	//int status = zoom_dbl_image(&im, w, h, dest_w, dest_h);
	int i, j;
	double I, J;
	int vld, status = 0;
	double h_factor = double(w-1)/double(dest_w-1);
	double v_factor = double(h-1)/double(dest_h-1);
	double *R = new double[dest_w*dest_h];
	double *G = new double[dest_w*dest_h];
	double *B = new double[dest_w*dest_h];
	Y_RGB<double> V;
	for(j = 0; j<dest_h-1; j++) {
		J = v_factor*j;
		for(i = 0; i<dest_w-1; i++) {
			I = h_factor*i;
			V = Bilinear(I, J, &vld);
			if(!vld) {
				R[j*dest_w+i] = Y_Utils::_INVALID_DOUBLE;
				G[j*dest_w+i] = Y_Utils::_INVALID_DOUBLE;
				B[j*dest_w+i] = Y_Utils::_INVALID_DOUBLE;
			} else {
				R[j*dest_w+i] = V.p[0];
				G[j*dest_w+i] = V.p[1];
				B[j*dest_w+i] = V.p[2];
			}
		}
		I = h_factor*(dest_w-1)-0.5;
		V = Bilinear(I, J, int(I), int(J));
		R[j*dest_w+dest_w-1] = V.p[0];
		G[j*dest_w+dest_w-1] = V.p[1];
		B[j*dest_w+dest_w-1] = V.p[2];
	}
	// Last row - enforce interpolation from above row
	J = v_factor*(dest_h-1)-0.5;
	for(i = 0; i<dest_w-1; i++) {
		I = h_factor*i;
		V = Bilinear(I, J, int(I), int(J));
		R[(dest_h-1)*dest_w+i] = V.p[0];
		G[(dest_h-1)*dest_w+i] = V.p[1];
		B[(dest_h-1)*dest_w+i] = V.p[2];
	}
	I = h_factor*(dest_w-1)-0.5;
	V = Bilinear(I, J, int(I), int(J));
	R[(dest_h-1)*dest_w+dest_w-1] = V.p[0];
	G[(dest_h-1)*dest_w+dest_w-1] = V.p[1];
	B[(dest_h-1)*dest_w+dest_w-1] = V.p[2];
	w = dest_w; h = dest_h;
	delete [] d_im;
	d_im = new double[3*w*h];
	for(j = 0; j<h; j++) {
		for(i = 0; i<w; i++) {
			d_im[3*(j*w+i)+0] = R[j*w+i];
			d_im[3*(j*w+i)+1] = G[j*w+i];
			d_im[3*(j*w+i)+2] = B[j*w+i];
		}
	}
	delete [] R; delete [] G; delete [] B;
	return(status);
}

int DblImageC::Zoom_Bilinear(double zm, int flags)
{
	int status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bilinear(dest_w, dest_h);
	if(status==-1) {
		//sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Pixels affected by invalid neighbours should be invalid
	// Now 'd_im' has dimensions dest_w*dest_h
	return(status);
}

int DblImageC::ZoomToDest_Bicubic(int dest_w, int dest_h)
{
	DblImageC* pDblnew = new DblImageC(dest_w, dest_h);

	DblImageG* pR = Greyscale('r');
	if(pR) pR->ZoomToDest_Bicubic(dest_w, dest_h);

	DblImageG* pG = Greyscale('g');
	if(pG) pG->ZoomToDest_Bicubic(dest_w, dest_h);

	DblImageG* pB = Greyscale('b');
	if(pB) pB->ZoomToDest_Bicubic(dest_w, dest_h);

	if(pR && pG && pB)
		pDblnew->CombineGrayscales(pR, pG, pB);

	if(pR) delete pR;
	if(pG) delete pG;
	if(pB) delete pB;

	delete [] d_im;
	d_im = new double[3*dest_w*dest_h];
	memcpy(d_im, pDblnew->d_im, 3*dest_w*dest_h*sizeof(double));
	w = dest_w; h = dest_h;

	delete pDblnew;

	return(0);
}

int DblImageC::Zoom_Bicubic(double zm, int flags)
{
	int status;
	
	// Zooming
	int dest_w = (int) (zm*w);
	int dest_h = (int) (zm*h);
	if(even_zoom) {
		if(dest_w%2==1) dest_w++;
		if(dest_h%2==1) dest_h++;
	}
	status = ZoomToDest_Bicubic(dest_w, dest_h);
	if(status==-1) {
		//sprintf_s(error, 256, "Requested zoom=%lf\n", zm);
		exit(-1);
	}
	// Pixels affected by invalid neighbours should be invalid
	// Now 'd_im' has dimensions dest_w*dest_h
	return(status);
}

int DblImageC::Rotate_Bilinear(double angle_degrees, int flags)
{
	int i, j;
	double cos_a, sin_a;
	Y_RGB<double> v;
	DblImageC *tmp = Copy();
	SetInvalid();

	double theta = Deg2Rad(angle_degrees); // ??? + or -?
	double c = cos(theta);
	double s = sin(theta);
	double c_x = 0.5*w-0.5; // Center position
	double c_y = 0.5*h-0.5;
	
	double dx, dy;
	int vld;
	for(j=0; j<h; j++) {
		for(i=0; i<w; i++) {
			cos_a = (double)i-c_x;
			sin_a = (double)j-c_y;
			// Positions as doubles.
			// Signs for sin are changed due to inverted system of coordinates
			dx = c*cos_a+s*sin_a+c_x;
			dy =-s*cos_a+c*sin_a+c_y;
			//v = tmp->pfBilinear(dx, dy, &vld);
			v = tmp->Bilinear(dx, dy, &vld);
			if(vld) set_pixel(i, j, v);
		}
	}
	//tmp->ResetPrefilter(); // Not needed as deleted anyway
	delete tmp;
	
	return(0);
}

// 1: by pi/2, 2: by pi, 3: by -pi/2
void DblImageC::RotateQuarter(int nr_of_quarters)
{
	int new_w, new_h;
	if(nr_of_quarters == 2) {
		new_w = w; new_h = h;
	} else {
		new_w = h; new_h = w;
	}
	double *tmp = (double*)  new double[3*new_w*new_h];
	int i, j, n = 3*sizeof(double);
	switch(nr_of_quarters) {
	case 1:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[3*(j*new_w+i)], &d_im[3*(i*w+(w-1-j))], n);
		}}
		break;
	case 2:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[3*(j*new_w+i)], &d_im[3*((h-1-j)*w+(w-1-i))], n);
		}}
		break;
	case 3:
		for(j = 0; j<new_h; j++) {
		for(i = 0; i<new_w; i++) {
			memcpy(&tmp[3*(j*new_w+i)], &d_im[3*((h-1-i)*w+j)], n);
		}}
		break;
	}
	delete [] d_im;
	d_im = tmp;
	w = new_w; h = new_h;
}

void DblImageC::Blur(int kernel, int times)
{
	if(kernel<3) {
		//sprintf_s(error, 256, "Blur kernel<3, returning.\n");
		return;
	}
	if(times<1) {
		//sprintf_s(error, 256, "Blur times<1, returning.\n");
		return;
	}
	if(kernel%2==0) {
		kernel++;
		//sprintf_s(error, 256, "Kernel even, set to %d.\n", kernel);
	}
	int i, j, ii, jj, half = (kernel-1)/2, count, NaN;
	Y_RGB<double> sum;
	
	DblImageC *tmp = new DblImageC(w, h);
	
	for(int t = 0; t<times; t++) {
		tmp->Copy(this);
	
		for(j = 0; j<h; j++) {
			for(i = 0; i<w; i++) {
				sum.Set(0.0, 0.0, 0.0);
				count = 0; NaN = 0;
				for(jj = j-half; jj<=j+half; jj++) {
					if(jj<0 || jj>=h) continue;
					for(ii = i-half; ii<=i+half; ii++) {
						if(ii<0 || ii>=w) continue;
						if(!(tmp->valid(ii, jj))) NaN++;
						sum += tmp->get_pixel(ii, jj);
						count++;
					}
				}
				if(count) {
					sum.p[0] /= count; sum.p[1] /= count; sum.p[2] /= count; 
				}
				if(!NaN)
					set_pixel(i, j, sum);
				else
					set_pixel(i, j, Y_RGB<double>::INVALID_RGBD);
			}
		}
	}
	delete tmp;
}

/*
// Difference between two sub-images which are supposed to be equivalent.
//   xs, ys - shift of the image 'i' with respect to this. 
//   Saves result in the file called 'name' if second argument is given.
double DblImageC::difference_error_double(DblImageC *ig, double xs, double ys, 
	int *overlap, char *name)
{
	int i, j, n, vld;
	double dif = 0.0, v, vi;
	double ii, jj;
	int wi = ig->w, hi = ig->h;
	DblImageC *d;
	
	if(name)
		d = new DblImageC(w, h);
	
	n = 0;
	// Every pixel in *this image is checked.
	// Pixel (i,j) gets contribution from location (ii,jj) of image I.
	for(j = 0; j<h; j++) {
		jj = (double)j+ys;
		if(jj<0.0 || jj>=hi)
			continue;
		for(i = 0; i<w; i++) {
			ii = (double)i+xs;
			if(ii<0.0 || ii>=wi)
				continue;
			//vi = ig->pfBilinear(ii, jj, &vld);
			vi = ig->Bilinear(ii, jj, &vld);
			if(valid(i, j)) {
				v = d_im[j*w+i];
				if(vld) {
					dif += fabs(v-vi);
					n++;
					if(name)
						d->d_im[j*w+i] = 0.5*(v+vi);
				} else {
					if(name)
						d->d_im[j*w+i] = v;
				}
			} else {
				if(vld && name)
					d->d_im[j*w+i] = vi;
			}
		}
	}
	if(debug)
		fprintf(stderr, "difference_error_double: total dif=%lf pixels=%d\n", dif, n);
	if(n)
		dif /= (double) n;
	else {
		fprintf(stderr, "No overlap between images.\n");
		dif = 0.0;
	}
	if(debug) {
		fprintf(stderr, "  dif=%lf\n", dif);
		Save("I1."DEXTN);		
		ig->Save("I2."DEXTN);
		fprintf(stderr, "Images \"I1.dbl\" and \"I2.dbl\" saved.\n");
	}
		
	if(name) {
		d->Save(name);
		fprintf(stderr, "Image \"%s\" saved.\n", name);
		delete d;
	}
	*overlap = n;
	//ig->ResetPrefilter();
	
	return(dif); 
}
*/

void DblImageC::MinMax(double& v_min, double& v_max)
{
	Y_RGB<double> v;
	
	for(int j = 0; j<h; j++) {
		for(int i = 0; i<w; i++) {
			if(valid(i, j)) {
				v = get_pixel(i, j);
				v_min = YMIN(v_min, v.p[0]); v_max = YMAX(v_max, v.p[0]);
				v_min = YMIN(v_min, v.p[1]); v_max = YMAX(v_max, v.p[1]);
				v_min = YMIN(v_min, v.p[2]); v_max = YMAX(v_max, v.p[2]);
			}
		}
	}
}


