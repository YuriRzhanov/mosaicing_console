#if defined(WIN32) || defined(WIN64)
#include <io.h>
#else
#include <unistd.h>
#endif

#include "Descr.h"

using namespace std;

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, bool *_value, bool _deflt)
{
	Init(O_BOOL, _tag, _args, _descr, (void *) _value);
	b_deflt1 = _deflt;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, _TCHAR *_value, _TCHAR _deflt)
{
	Init(O_CHAR, _tag, _args, _descr, (void *) _value);
	c_deflt1 = _deflt;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt)
{
	Init(O_INT, _tag, _args, _descr, (void *) _value);
	i_deflt1 = _deflt;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt,
	int *_value2, int _deflt2)
{
	Init(O_INT, _tag, _args, _descr, (void *) _value, (void *) _value2);
	i_deflt1 = _deflt; i_deflt2 = _deflt2;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt,
	int *_value2, int _deflt2, int *_value3, int _deflt3)
{
	Init(O_INT, _tag, _args, _descr, (void *) _value, (void *) _value2, (void *) _value3);
	i_deflt1 = _deflt; i_deflt2 = _deflt2; i_deflt3 = _deflt3;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, int *_value, int _deflt,
	int *_value2, int _deflt2, int *_value3, int _deflt3, int *_value4, int _deflt4)
{
	Init(O_INT, _tag, _args, _descr, (void *) _value, (void *) _value2, (void *) _value3, (void *) _value4);
	i_deflt1 = _deflt; i_deflt2 = _deflt2; i_deflt3 = _deflt3;  i_deflt4 = _deflt4;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, float *_value, float _deflt)
{
	Init(O_FLT, _tag, _args, _descr, (void *) _value);
	f_deflt1 = _deflt;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, float *_value, float _deflt,
	float *_value2, float _deflt2)
{
	Init(O_FLT, _tag, _args, _descr, (void *) _value, (void *) _value2);
	f_deflt1 = _deflt; f_deflt2 = _deflt2;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, double *_value, double _deflt)
{
	Init(O_DBL, _tag, _args, _descr, (void *) _value);
	d_deflt1 = _deflt;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, double *_value, double _deflt,
	double *_value2, double _deflt2)
{
	Init(O_DBL, _tag, _args, _descr, (void *) _value, (void *) _value2);
	d_deflt1 = _deflt; d_deflt2 = _deflt2;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, double *_value, double _deflt,
	double *_value2, double _deflt2, double *_value3, double _deflt3)
{
	Init(O_DBL, _tag, _args, _descr, (void *) _value, (void *) _value2, (void *) _value3);
	d_deflt1 = _deflt; d_deflt2 = _deflt2; d_deflt3 = _deflt3;
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1)
{
	Init(O_STR, _tag, _args, _descr, (void *) _value);
#ifdef _MSC_VER
	if(_deflt1) { s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1); }
#else
	if(_deflt1) { s_deflt1 = new char[strlen(_deflt1)+1]; strcpy(s_deflt, _deflt); }
#endif
}

OptStringType::OptStringType(_TCHAR *_tag, int _args, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1,
	_TCHAR **_value2, _TCHAR *_deflt2)
{
	Init(O_STR, _tag, _args, _descr, (void *) _value, (void *) _value2);
	if(_deflt1) {
#ifdef _MSC_VER
		s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1);
#else
		s_deflt1 = new char[strlen(_deflt1)+1]; strcpy(s_deflt1, _deflt1);
#endif
	}
	if(_deflt2) {
#ifdef _MSC_VER
		s_deflt2 = new _TCHAR[_tcslen(_deflt2)+1]; _tcscpy_s(s_deflt2, _tcslen(_deflt2)+1, _deflt2);
#else
		s_deflt2 = new char[strlen(_deflt2)+1]; strcpy(s_deflt2, _deflt2);
#endif
	}
}

OptStringType::~OptStringType()
{
	if(tag!=nullptr) delete [] tag;
	if(descr!=nullptr) delete [] descr;
	if(o_type==O_STR && s_deflt1!=nullptr) delete [] s_deflt1;
	if(o_type==O_STR && s_deflt2!=nullptr) delete [] s_deflt2;
	//if(o_type==O_STR && (*(_TCHAR *) ptr)!=nullptr) delete [] *(_TCHAR **) ptr;
	// XXXXX
	if(o_type==O_STR && (*(_TCHAR *) ptr1)!=NULL) delete [] *(_TCHAR **) ptr1;
}

void OptStringType::Init(O_TYPE _t, _TCHAR *_tag, int _args, _TCHAR *_d, void *_p1, void *_p2, void *_p3, void *_p4)
{
	o_type = _t;
#ifdef _MSC_VER
	tag = new _TCHAR[_tcslen(_tag)+1]; _tcscpy_s(tag, _tcslen(_tag)+1, _tag);
#else
	tag = new char[strlen(_tag)+1]; strcpy(tag, _tag);
#endif
	args = _args;
#ifdef _MSC_VER
	if(_d) { descr = new _TCHAR[_tcslen(_d)+1]; _tcscpy_s(descr, _tcslen(_d)+1, _d); } else descr = nullptr;
#else
	if(_d) { descr = new char[strlen(_d)+1]; strcpy(descr, _d); } else descr = nullptr;
#endif
	ptr1 = _p1; ptr2 = _p2; ptr3 = _p3; ptr4 = _p4;
	s_deflt1 = nullptr; s_deflt2 = nullptr;
	set = 0;
}


int OptStrings::error = 0;
_TCHAR OptStrings::error_stg[256];

OptStrings::OptStrings()
{
	n_opts = 0; n_optstring = 0; optind = 1;
	memset(optstring, 0, 256);
}

OptStrings::~OptStrings()
{
	for(int i = 0; i<n_opts; i++)
		delete opts_stg[i];
}

#ifdef _MSC_VER
void OptStrings::PrintHelp()
{
	_ftprintf_s(stderr, _T("OptStrings help:\n"));
	for(int i = 0; i<n_opts; i++) {
		switch(opts_stg[i]->args) {
			case 0:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, Dflt: %s\n"), opts_stg[i]->tag, opts_stg[i]->descr, 
					opts_stg[i]->args, (opts_stg[i]->b_deflt1) ? "true" : "false");
				break;
			case 1:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_CHAR:
						_ftprintf_s(stderr, _T("Dflt: %c\n"), opts_stg[i]->c_deflt1);
						break;
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d\n"), opts_stg[i]->i_deflt1);
						break;
					case O_FLT:
						_ftprintf_s(stderr, _T("Dflt: %f\n"), opts_stg[i]->f_deflt1);
						break;
					case O_DBL:
						_ftprintf_s(stderr, _T("Dflt: %lf\n"), opts_stg[i]->d_deflt1);
						break;
					case O_STR:
						_ftprintf_s(stderr, _T("Dflt: \"%s\"\n"), opts_stg[i]->s_deflt1);
						break;
				}
				break;
			case 2:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d, "), opts_stg[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %d\n"), opts_stg[i]->i_deflt2);
						break;
					case O_FLT:
						_ftprintf_s(stderr, _T("Dflt: %f, "), opts_stg[i]->f_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %f\n"), opts_stg[i]->f_deflt2);
						break;
					case O_DBL:
						_ftprintf_s(stderr, _T("Dflt: %lf, "), opts_stg[i]->d_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %lf\n"), opts_stg[i]->d_deflt2);
						break;
					case O_STR:
						_ftprintf_s(stderr, _T("Dflt: \"%s\", "), opts_stg[i]->s_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: \"%s\"\n"), opts_stg[i]->s_deflt2);
						break;
				}
				break;
			case 3:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt1: %d, "), opts_stg[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %d, "), opts_stg[i]->i_deflt2);
						_ftprintf_s(stderr, _T("Dflt3: %d\n"), opts_stg[i]->i_deflt3);
						break;
				}
				break;
			case 4:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt1: %d, "), opts_stg[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %d, "), opts_stg[i]->i_deflt2);
						_ftprintf_s(stderr, _T("Dflt3: %d, "), opts_stg[i]->i_deflt3);
						_ftprintf_s(stderr, _T("Dflt4: %d\n"), opts_stg[i]->i_deflt4);
						break;
				}
				break;
		}
	}
}
#else
void OptStrings::PrintHelp()
{
	fprintf(stderr, "OptStrings help:\n");
	for(int i = 0; i<n_opts; i++) {
		switch(opts_stg[i]->args) {
			case 0:
				fprintf(stderr, "<%s> %s: %d vars, Dflt1: %s\n", opts_stg[i]->tag, opts_stg[i]->descr, 
					opts_stg[i]->args, (opts_stg[i]->b_deflt1) ? "true" : "false");
				break;
			case 1:
				fprintf(stderr, "<%s> %s: %d vars, ", opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_CHAR:
						fprintf(stderr, "Dflt1: %c\n", opts_stg[i]->c_deflt1);
						break;
					case O_INT:
						fprintf(stderr, "Dflt1: %d\n", opts_stg[i]->i_deflt1);
						break;
					case O_FLT:
						fprintf(stderr, "Dflt1: %f\n", opts_stg[i]->f_deflt1);
						break;
					case O_DBL:
						fprintf(stderr, "Dflt1: %lf\n", opts_stg[i]->d_deflt1);
						break;
					case O_STR:
						fprintf(stderr, "Dflt1: \"%s\"\n", opts_stg[i]->s_deflt1);
						break;
				}
				break;
			case 2:
				fprintf(stderr, "<%s> %s: %d vars, ", opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_INT:
						fprintf(stderr, "Dflt1: %d, ", opts_stg[i]->i_deflt1);
						fprintf(stderr, "Dflt2: %d\n", opts_stg[i]->i_deflt2);
						break;
					case O_FLT:
						fprintf(stderr, "Dflt1: %f, ", opts_stg[i]->f_deflt1);
						fprintf(stderr, "Dflt2: %f\n", opts_stg[i]->f_deflt2);
						break;
					case O_DBL:
						fprintf(stderr, "Dflt1: %lf, ", opts_stg[i]->d_deflt1);
						fprintf(stderr, "Dflt2: %lf\n", opts_stg[i]->d_deflt2);
						break;
					case O_STR:
						fprintf(stderr, "Dflt1: \"%s\", ", opts_stg[i]->s_deflt1);
						fprintf(stderr, "Dflt2: \"%s\"\n", opts_stg[i]->s_deflt2);
						break;
				}
				break;
			case 3:
				fprintf(stderr, "<%s> %s: %d vars, ", opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_INT:
						fprintf(stderr, "Dflt1: %d, ", opts_stg[i]->i_deflt1);
						fprintf(stderr, "Dflt2: %d, ", opts_stg[i]->i_deflt2);
						fprintf(stderr, "Dflt3: %d\n", opts_stg[i]->i_deflt3);
						break;
				}
				break;
			case 4:
				fprintf(stderr, "<%s> %s: %d vars, ", opts_stg[i]->tag, opts_stg[i]->descr, opts_stg[i]->args);
				switch(opts_stg[i]->o_type) {
					case O_INT:
						fprintf(stderr, "Dflt1: %d, ", opts_stg[i]->i_deflt1);
						fprintf(stderr, "Dflt2: %d, ", opts_stg[i]->i_deflt2);
						fprintf(stderr, "Dflt3: %d, ", opts_stg[i]->i_deflt3);
						fprintf(stderr, "Dflt4: %d\n", opts_stg[i]->i_deflt4);
						break;
				}
				break;
		}
	}
}
#endif

int OptStrings::CheckCorrectness(_TCHAR* tag, int args, int expected_args)
{
	if(!tag) {
		//_stprintf_s(error_stg, (size_t) 256, _T("No tag given in OptStrings.\n")); 
		error = 1;
		return(-1);
	}
	if(expected_args==1 && (args!=0 && args!=1)) {
		//_stprintf_s(error_stg, (size_t) 256, _T("Wrong number of arguments for %s: %d.\n"), tag, args);
		error = 2;
		return(-1);
	}
	if(expected_args==2 && args!=2) {
		//_stprintf_s(error_stg, (size_t) 256, _T("Wrong number of arguments for %s: %d.\n"), tag, args);
		error = 2;
		return(-1);
	}
	// Check uniqueness of tag
	for(int k = 0; k<n_opts; k++) {
		if((_tcsnccmp(tag, opts_stg[k]->tag, _tcslen(tag))==0) && (_tcslen(tag)==_tcslen(opts_stg[k]->tag))) {
			//_stprintf_s(error_stg, (size_t) 256, _T("Non-unique tag \"%s\".\n"), tag);
			error = 3;
			return(-1);
		}
	}
	return(0);
}


void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, bool *value, bool deflt1)
{
	if(CheckCorrectness(tag, args, 1) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, _TCHAR *value, _TCHAR deflt1)
{
	if(CheckCorrectness(tag, args, 1) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt1)
{
	if(CheckCorrectness(tag, args, 1) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt1,
	int *value2, int deflt2)
{
	if(CheckCorrectness(tag, args, 2) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt1,
	int *value2, int deflt2, int *value3, int deflt3)
{
	if(CheckCorrectness(tag, args, 3) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2, value3, deflt3);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, int *value, int deflt1,
	int *value2, int deflt2, int *value3, int deflt3, int *value4, int deflt4)
{
	if(CheckCorrectness(tag, args, 4) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2, value3, deflt3, value4, deflt4);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, float *value, float deflt1)
{
	if(CheckCorrectness(tag, args, 1) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, float *value, float deflt1,
	float *value2, float deflt2)
{
	if(CheckCorrectness(tag, args, 2) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, double *value, double deflt1)
{
	if(CheckCorrectness(tag, args, 1) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, double *value, double deflt1,
	double *value2, double deflt2)
{
	if(CheckCorrectness(tag, args, 2) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, double *value, double deflt1,
	double *value2, double deflt2, double *value3, double deflt3)
{
	if(CheckCorrectness(tag, args, 3) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2, value3, deflt3);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt1)
{
	if(CheckCorrectness(tag, args, 1) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

void OptStrings::Case(_TCHAR *tag, int args, _TCHAR *descr, _TCHAR **value, _TCHAR *deflt1,
	_TCHAR **value2, _TCHAR *deflt2)
{
	if(CheckCorrectness(tag, args, 2) == -1)
		return;
	opts_stg[n_opts++] = new OptStringType(tag, args, descr, value, deflt1, value2, deflt2);
	for(int i = 0; i<(int) _tcslen(tag); i++)
		optstring[n_optstring++] = tag[i];
	if(args) optstring[n_optstring++] = ':'; // Argument indicator
	optstring[n_optstring++] = ';'; // Separator
}

// Returns the next option tag in 'argv' that matches a tag in 'optstring'
_TCHAR *OptStrings::getopt_Y(int argc, _TCHAR *argv[], _TCHAR *optstring, _TCHAR* bad_option)
{
	_TCHAR *l_optstring = _tcsdup(optstring);
	int has_colon = 0;
	static _TCHAR tag[1024];
	_TCHAR* next_token;
	for(int i = optind; i<argc; i++) {
		if(argv[i][0]=='-' && argv[i][1]=='-') { // Special delimiter
			optarg = nullptr;
			optind++;
			break;
		}
		if(argv[i][0]!='-') continue;
		// Found option
#ifdef _MSC_VER
		_TCHAR *p = _tcstok_s(l_optstring, _T(";"), &next_token); // There must be at least one ';'
		do {
			// remove possible ':', make note of it
			int h = int(_tcslen(p));
			if(p[h-1]==':') {
				p[h-1] = 0; has_colon = 1;
			} else has_colon = 0;
			if(_tcsnccmp(p, (argv[i]+1), h)==0) {
				optind++;
				// If 'p' contains ':', point 'optarg' to the next argument
				if(has_colon) {
					optarg = argv[i+1]; 
					optind++;
				} else optarg = nullptr;
				_tcscpy_s(tag, 16, p);
				free(l_optstring); 
				return(tag);
			}
		} while(p = _tcstok_s(nullptr, _T(";"), &next_token));
		// Found option that is not in optstring
		_tcsncpy_s(bad_option, 16, argv[i]+1, _tcslen(argv[i]+1));
#else
		char *p = strtok(l_optstring, ";"); // There must be at least one ';'
		do {
			// remove possible ':', make note of it
			int h = strlen(p);
			if(p[h-1]==':') {
				p[h-1] = 0; has_colon = 1;
			} else has_colon = 0;
			if(strncmp(p, (argv[i]+1), h)==0) {
				optind++;
				// If 'p' contains ':', point 'optarg' to the next argument
				if(has_colon) {
					optarg = argv[i+1]; 
					optind++;
				} else optarg = NULL;
				strcpy(tag, p);
				free(l_optstring); 
				return(tag);
			}
		} while(p = strtok(NULL, ";"));
#endif
		free(l_optstring);
		return(_T("?"));
	}
	free(l_optstring); 
	return(nullptr);
}

int OptStrings::NumSeparators(_TCHAR* stg, _TCHAR separator)
{
	int n = 0, len = (int)_tcslen(stg);
	for(int i = 0; i<len; i++) {
		if(stg[i]==separator) n++;
	}
	return(n);
}

int OptStrings::Process(int *argc, _TCHAR ***argv)
{
//#ifndef WIN32
	_TCHAR *tag;
	int len;
	if(_tcschr(optstring, ';')==nullptr) return(0);

	_TCHAR bad_option[1024];
	while((tag = getopt_Y(*argc, *argv, optstring, bad_option))!=nullptr) {
		if(tag[0]=='?') {
			_ftprintf_s(stderr, _T("Unrecognized option \"%s\".\n"), bad_option); 
			*argc = -1; return(-1);
			//exit(1);
		}
		for(int i = 0; i<n_opts; i++) {
			if(_tcsnccmp(tag, opts_stg[i]->tag, _tcslen(tag))!=0) continue;
			// Check number of characters:
			if(_tcslen(tag) != _tcslen(opts_stg[i]->tag)) continue;
			// Verify correct number of following arguments:
			if(opts_stg[i]->args>0 && NumSeparators(optarg, ',')!=(opts_stg[i]->args-1)) {
				_ftprintf_s(stderr, _T("Option \"%s\" has wrong number of arguments.\n"), tag);
				*argc = -1; return(-1);
				//exit(1);
			}
			// Different parsing for different arguments.
			switch(opts_stg[i]->args) {
				case 0: // No arguments: boolean variable
					switch(opts_stg[i]->o_type) {
						case O_BOOL:
							*((bool *) opts_stg[i]->ptr1) = !(opts_stg[i]->b_deflt1);
							break;
						case O_INT:
							*((int *) opts_stg[i]->ptr1) = !(opts_stg[i]->i_deflt1);
							break;
					}
					break;
				case 1:
					switch(opts_stg[i]->o_type) {
						case O_CHAR:
							_stscanf_s(optarg, _T("%c"), ((_TCHAR *) opts_stg[i]->ptr1), 1); // Yuri: '1' is required by VS2015
							break;
						case O_INT:
							_stscanf_s(optarg, _T("%d"), ((int *) opts_stg[i]->ptr1));
							break;
						case O_FLT:
							_stscanf_s(optarg, _T("%f"), ((float *) opts_stg[i]->ptr1));
							break;
						case O_DBL:
							_stscanf_s(optarg, _T("%lf"), ((double *) opts_stg[i]->ptr1));
							break;
						case O_STR:
							*((_TCHAR **)(opts_stg[i]->ptr1)) = new _TCHAR[_tcslen(optarg)+1];
							_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr1)), _tcslen(optarg)+1, optarg);
							break;
					}
					break;
				case 2:
					switch(opts_stg[i]->o_type) {
						case O_INT:
							_stscanf_s(optarg, _T("%d,%d"), ((int *) opts_stg[i]->ptr1), ((int *) opts_stg[i]->ptr2));
							break;
						case O_FLT:
							_stscanf_s(optarg, _T("%f,%f"), ((float *) opts_stg[i]->ptr1), ((float *) opts_stg[i]->ptr2));
							break;
						case O_DBL:
							_stscanf_s(optarg, _T("%lf,%lf"), ((double *) opts_stg[i]->ptr1), ((double *) opts_stg[i]->ptr2));
							break;
						case O_STR:
							len = (int)_tcslen(optarg)+1;
							*((_TCHAR **)(opts_stg[i]->ptr1)) = new _TCHAR[len];
							*((_TCHAR **)(opts_stg[i]->ptr2)) = new _TCHAR[len];
#ifdef _MSC_VER
							{
								_TCHAR* next_token;
								_TCHAR *p = _tcstok_s(optarg, _T(","), &next_token);
								_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr1)), len, p);
								p = _tcstok_s(nullptr, _T(","), &next_token);
								_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr2)), len, p);
							}
#else
							sscanf(optarg, "%s,%s", *((_TCHAR **)(opts_stg[i]->ptr)), *((_TCHAR **)(opts_stg[i]->ptr2)));
#endif
							break;
					}
					break;
				case 3:
					switch(opts_stg[i]->o_type) {
						case O_INT:
							_stscanf_s(optarg, _T("%d,%d,%d"), ((int *) opts_stg[i]->ptr1), ((int *) opts_stg[i]->ptr2), ((int *) opts_stg[i]->ptr3));
							break;
						case O_DBL:
							_stscanf_s(optarg, _T("%lf,%lf,%lf"), ((double *) opts_stg[i]->ptr1), ((double *) opts_stg[i]->ptr2), ((double *) opts_stg[i]->ptr3));
							break;
						case O_STR:
							len = (int)_tcslen(optarg)+1;
							*((_TCHAR **)(opts_stg[i]->ptr1)) = new _TCHAR[len];
							*((_TCHAR **)(opts_stg[i]->ptr2)) = new _TCHAR[len];
							*((_TCHAR **)(opts_stg[i]->ptr3)) = new _TCHAR[len];
#ifdef _MSC_VER
							{
								_TCHAR* next_token;
								_TCHAR *p = _tcstok_s(optarg, _T(","), &next_token);
								_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr1)), len, p);
								p = _tcstok_s(nullptr, _T(","), &next_token);
								_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr2)), len, p);
								p = _tcstok_s(nullptr, _T(","), &next_token);
								_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr3)), len, p);
							}
#else
							sscanf(optarg, "%s,%s", *((_TCHAR **)(opts_stg[i]->ptr)), *((_TCHAR **)(opts_stg[i]->ptr2)), *((_TCHAR **)(opts_stg[i]->ptr3)));
#endif
							break;
					}
					break;
				case 4:
					switch(opts_stg[i]->o_type) {
						case O_INT:
							_stscanf_s(optarg, _T("%d,%d,%d,%d"), ((int *) opts_stg[i]->ptr1), ((int *) opts_stg[i]->ptr2), ((int *) opts_stg[i]->ptr3), ((int *) opts_stg[i]->ptr4));
							break;
					}
					break;
			}
			opts_stg[i]->set = 1; // Mark as set
		}
	}
	// Set all unset values to provided defaults
	for(int i = 0; i<n_opts; i++) {
		if(opts_stg[i]->set) continue; // Do nothing
		switch(opts_stg[i]->args) {
			case 0: // No arguments: boolean variable
				switch(opts_stg[i]->o_type) {
					case O_BOOL:
						*((bool *) opts_stg[i]->ptr1) = opts_stg[i]->b_deflt1;
						break;
					case O_INT:
						*(int *) opts_stg[i]->ptr1 = opts_stg[i]->i_deflt1;
						break;
				}
				break;
			case 1:
				switch(opts_stg[i]->o_type) {
					case O_CHAR:
						*(_TCHAR *) opts_stg[i]->ptr1 = opts_stg[i]->c_deflt1;
						break;
					case O_INT:
						*(int *) opts_stg[i]->ptr1 = opts_stg[i]->i_deflt1;
						if(opts_stg[i]->ptr2) *(int *) opts_stg[i]->ptr2 = opts_stg[i]->i_deflt2;
						break;
					case O_FLT:
						*(float *) opts_stg[i]->ptr1 = opts_stg[i]->f_deflt1;
						if(opts_stg[i]->ptr2) *(float *) opts_stg[i]->ptr2 = opts_stg[i]->f_deflt2;
						break;
					case O_DBL:
						*(double *) opts_stg[i]->ptr1 = opts_stg[i]->d_deflt1;
						if(opts_stg[i]->ptr2) *(double *) opts_stg[i]->ptr2 = opts_stg[i]->d_deflt2;
						break;
					case O_STR:
						if(opts_stg[i]->s_deflt1) {
							*((_TCHAR **)(opts_stg[i]->ptr1)) = new _TCHAR[_tcslen(opts_stg[i]->s_deflt1)+1];
							_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr1)), _tcslen(opts_stg[i]->s_deflt1)+1, opts_stg[i]->s_deflt1);
						} else
							*(_TCHAR **) (opts_stg[i]->ptr1) = nullptr;
						/*
						if(opts_stg[i]->ptr2 && opts_stg[i]->s_deflt2) {
							*((_TCHAR **)(opts_stg[i]->ptr2)) = new _TCHAR[_tcslen(opts_stg[i]->s_deflt2)+1];
							_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr2)), _tcslen(opts_stg[i]->s_deflt2)+1, opts_stg[i]->s_deflt2);
						} else
							*((_TCHAR **) opts_stg[i]->ptr2) = nullptr;
							*/
						break;
				}
				break;
			case 2:
				switch(opts_stg[i]->o_type) {
					case O_INT:
						*(int *) opts_stg[i]->ptr1 = opts_stg[i]->i_deflt1;
						*(int *) opts_stg[i]->ptr2 = opts_stg[i]->i_deflt2;
						break;
					case O_FLT:
						*(float *) opts_stg[i]->ptr1 = opts_stg[i]->f_deflt1;
						*(float *) opts_stg[i]->ptr2 = opts_stg[i]->f_deflt2;
						break;
					case O_DBL:
						*(double *) opts_stg[i]->ptr1 = opts_stg[i]->d_deflt1;
						*(double *) opts_stg[i]->ptr2 = opts_stg[i]->d_deflt2;
						break;
					case O_STR:
						if(opts_stg[i]->s_deflt1) {
							*((_TCHAR **)(opts_stg[i]->ptr1)) = new _TCHAR[_tcslen(opts_stg[i]->s_deflt1)+1];
							_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr1)), _tcslen(opts_stg[i]->s_deflt1)+1, opts_stg[i]->s_deflt1);
						} else
							*(_TCHAR **) (opts_stg[i]->ptr1) = nullptr;
						if(opts_stg[i]->s_deflt2) {
							*((_TCHAR **)(opts_stg[i]->ptr2)) = new _TCHAR[_tcslen(opts_stg[i]->s_deflt2)+1];
							_tcscpy_s(*((_TCHAR **)(opts_stg[i]->ptr2)), _tcslen(opts_stg[i]->s_deflt2)+1, opts_stg[i]->s_deflt2);
						} else
							*(_TCHAR **) (opts_stg[i]->ptr2) = nullptr;
						break;
				}
				break;
			case 3:
				switch(opts_stg[i]->o_type) {
					case O_INT:
						*(int *) opts_stg[i]->ptr1 = opts_stg[i]->i_deflt1;
						*(int *) opts_stg[i]->ptr2 = opts_stg[i]->i_deflt2;
						*(int *) opts_stg[i]->ptr3 = opts_stg[i]->i_deflt3;
						break;
					case O_DBL:
						*(double *) opts_stg[i]->ptr1 = opts_stg[i]->d_deflt1;
						*(double *) opts_stg[i]->ptr2 = opts_stg[i]->d_deflt2;
						*(double *) opts_stg[i]->ptr3 = opts_stg[i]->d_deflt3;
						break;
				} 
				break;
			case 4:
				switch(opts_stg[i]->o_type) {
					case O_INT:
						*(int *) opts_stg[i]->ptr1 = opts_stg[i]->i_deflt1;
						*(int *) opts_stg[i]->ptr2 = opts_stg[i]->i_deflt2;
						*(int *) opts_stg[i]->ptr3 = opts_stg[i]->i_deflt3;
						*(int *) opts_stg[i]->ptr4 = opts_stg[i]->i_deflt4;
						break;
				}
				break;
		}
	}
	*argc -= optind-1;
	_TCHAR* progname = *argv[0]; // remember before shifting
	*argv += optind-1;
	*argv[0] = progname; // return back
	return(optind);
//#else
//	return(0);
//#endif
}





DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt1)
{
	Init(O_INT, _tag, 1, _descr, (void *) _value);
	i_deflt1 = _deflt1;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt1,
	int *_value2, int _deflt2)
{
	Init(O_INT, _tag, 2, _descr, (void *) _value, (void *) _value2);
	i_deflt1 = _deflt1; i_deflt2 = _deflt2;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt1,
	int *_value2, int _deflt2, int *_value3, int _deflt3)
{
	Init(O_INT, _tag, 3, _descr, (void *) _value, (void *) _value2, (void *) _value3);
	i_deflt1 = _deflt1; i_deflt2 = _deflt2; i_deflt3 = _deflt3; 
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, int *_value, int _deflt1,
	int *_value2, int _deflt2, int *_value3, int _deflt3, int *_value4, int _deflt4)
{
	Init(O_INT, _tag, 4, _descr, (void *) _value, (void *) _value2, (void *) _value3, (void *) _value4);
	i_deflt1 = _deflt1; i_deflt2 = _deflt2; i_deflt3 = _deflt3; i_deflt4 = _deflt4;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, float *_value, float _deflt1)
{
	Init(O_FLT, _tag, 1, _descr, (void *) _value);
	f_deflt1 = _deflt1;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, float *_value, float _deflt1,
	float *_value2, float _deflt2)
{
	Init(O_FLT, _tag, 2, _descr, (void *) _value, (void *) _value2);
	f_deflt1 = _deflt1; f_deflt2 = _deflt2;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, double *_value, double _deflt1)
{
	Init(O_DBL, _tag, 1, _descr, (void *) _value);
	d_deflt1 = _deflt1;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, double *_value, double _deflt1,
	double *_value2, double _deflt2)
{
	Init(O_DBL, _tag, 2, _descr, (void *) _value, (void *) _value2);
	d_deflt1 = _deflt1; d_deflt2 = _deflt2;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, double *_value, double _deflt1,
	double *_value2, double _deflt2, double *_value3, double _deflt3)
{
	Init(O_DBL, _tag, 3, _descr, (void *) _value, (void *) _value2, (void *) _value3);
	d_deflt1 = _deflt1; d_deflt2 = _deflt2; d_deflt3 = _deflt3;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1)
{
	Init(O_STR, _tag, 1, _descr, (void *) _value);
	if(_deflt1) { s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1); }
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1,
	_TCHAR **_value2, _TCHAR *_deflt2)
{
	Init(O_STR, _tag, 2, _descr, (void *) _value, (void *) _value2);
	if(_deflt1) { 
		s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1);
		s_deflt2 = new _TCHAR[_tcslen(_deflt2)+1]; _tcscpy_s(s_deflt2, _tcslen(_deflt2)+1, _deflt2);
	}
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1,
	_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3)
{
	Init(O_STR, _tag, 3, _descr, (void *) _value, (void *) _value2, (void *) _value3);
	if(_deflt1) { 
		s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1);
		s_deflt2 = new _TCHAR[_tcslen(_deflt2)+1]; _tcscpy_s(s_deflt2, _tcslen(_deflt2)+1, _deflt2);
		s_deflt3 = new _TCHAR[_tcslen(_deflt3)+1]; _tcscpy_s(s_deflt3, _tcslen(_deflt3)+1, _deflt3);
	}
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1,
	_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3, _TCHAR **_value4, _TCHAR *_deflt4)
{
	Init(O_STR, _tag, 4, _descr, (void *) _value, (void *) _value2, (void *) _value3, (void *) _value4);
	if(_deflt1) { 
		s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1);
		s_deflt2 = new _TCHAR[_tcslen(_deflt2)+1]; _tcscpy_s(s_deflt2, _tcslen(_deflt2)+1, _deflt2);
		s_deflt3 = new _TCHAR[_tcslen(_deflt3)+1]; _tcscpy_s(s_deflt3, _tcslen(_deflt3)+1, _deflt3);
		s_deflt4 = new _TCHAR[_tcslen(_deflt4)+1]; _tcscpy_s(s_deflt4, _tcslen(_deflt4)+1, _deflt4);
	}
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1,
	_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3, _TCHAR **_value4, _TCHAR *_deflt4,
	_TCHAR **_value5, _TCHAR *_deflt5)
{
	Init(O_STR, _tag, 5, _descr, (void *) _value, (void *) _value2, (void *) _value3, (void *) _value4, (void *) _value5);
	if(_deflt1) { 
		s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1);
		s_deflt2 = new _TCHAR[_tcslen(_deflt2)+1]; _tcscpy_s(s_deflt2, _tcslen(_deflt2)+1, _deflt2);
		s_deflt3 = new _TCHAR[_tcslen(_deflt3)+1]; _tcscpy_s(s_deflt3, _tcslen(_deflt3)+1, _deflt3);
		s_deflt4 = new _TCHAR[_tcslen(_deflt4)+1]; _tcscpy_s(s_deflt4, _tcslen(_deflt4)+1, _deflt4);
		s_deflt5 = new _TCHAR[_tcslen(_deflt5)+1]; _tcscpy_s(s_deflt5, _tcslen(_deflt5)+1, _deflt5);
	}
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR **_value, _TCHAR *_deflt1,
	_TCHAR **_value2, _TCHAR *_deflt2, _TCHAR **_value3, _TCHAR *_deflt3, _TCHAR **_value4, _TCHAR *_deflt4,
	_TCHAR **_value5, _TCHAR *_deflt5, _TCHAR **_value6, _TCHAR *_deflt6)
{
	Init(O_STR, _tag, 6, _descr, (void *) _value, (void *) _value2, (void *) _value3, (void *) _value4, (void *) _value5, (void *) _value6);
	if(_deflt1) { 
		s_deflt1 = new _TCHAR[_tcslen(_deflt1)+1]; _tcscpy_s(s_deflt1, _tcslen(_deflt1)+1, _deflt1);
		s_deflt2 = new _TCHAR[_tcslen(_deflt2)+1]; _tcscpy_s(s_deflt2, _tcslen(_deflt2)+1, _deflt2);
		s_deflt3 = new _TCHAR[_tcslen(_deflt3)+1]; _tcscpy_s(s_deflt3, _tcslen(_deflt3)+1, _deflt3);
		s_deflt4 = new _TCHAR[_tcslen(_deflt4)+1]; _tcscpy_s(s_deflt4, _tcslen(_deflt4)+1, _deflt4);
		s_deflt5 = new _TCHAR[_tcslen(_deflt5)+1]; _tcscpy_s(s_deflt5, _tcslen(_deflt5)+1, _deflt5);
		s_deflt6 = new _TCHAR[_tcslen(_deflt6)+1]; _tcscpy_s(s_deflt6, _tcslen(_deflt6)+1, _deflt6);
	}
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, bool *_value, bool _deflt1)
{
	Init(O_BOOL, _tag, 1, _descr, (void *) _value);
	b_deflt1 = _deflt1;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, bool *_value, bool _deflt1,
	bool *_value2, bool _deflt2)
{
	Init(O_BOOL, _tag, 2, _descr, (void *) _value, (void *) _value2);
	b_deflt1 = _deflt1; b_deflt2 = _deflt2;
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, bool *_value, bool _deflt1,
	bool *_value2, bool _deflt2, bool *_value3, bool _deflt3)
{
	Init(O_BOOL, _tag, 3, _descr, (void *) _value, (void *) _value2, (void *) _value3);
	b_deflt1 = _deflt1; b_deflt2 = _deflt2; b_deflt3 = _deflt3; 
}

DFileType::DFileType(_TCHAR *_tag, _TCHAR *_descr, _TCHAR *_value, _TCHAR _deflt1)
{
	Init(O_CHAR, _tag, 1, _descr, (void *) _value);
	c_deflt1 = _deflt1;
}

DFileType::~DFileType()
{
	//if(tag!=nullptr) delete [] tag;
	//if(descr!=nullptr) delete [] descr;
	if(o_type==O_STR && s_deflt1!=nullptr) delete [] s_deflt1;
	if(o_type==O_STR && s_deflt2!=nullptr) delete [] s_deflt2;
	if(o_type==O_STR && s_deflt3!=nullptr) delete [] s_deflt3;
	if(o_type==O_STR && s_deflt4!=nullptr) delete [] s_deflt4;
	if(o_type==O_STR && s_deflt5!=nullptr) delete [] s_deflt5;
	if(o_type==O_STR && s_deflt6!=nullptr) delete [] s_deflt6;
	// The following causes memory leaks, but premature deletion
	// also invalidates strings important for application
	//if(o_type==O_STR && ptr!=nullptr) delete [] *(_TCHAR **) ptr;
}

void DFileType::Init(O_TYPE _t, _TCHAR *_tag, int _args, _TCHAR *_d, void *_p1, 
	void *_p2, void *_p3, void *_p4, void *_p5, void *_p6)
{
	o_type = _t;
	if(_tag) { tag = new _TCHAR[_tcslen(_tag)+1]; _tcscpy_s(tag, _tcslen(_tag)+1, _tag); } else tag = nullptr;
	args = _args;
	if(_d) { descr = new _TCHAR[_tcslen(_d)+1]; _tcscpy_s(descr, _tcslen(_d)+1, _d); } else descr = nullptr;
	ptr1 = _p1; ptr2 = _p2; ptr3 = _p3; ptr4 = _p4; ptr5 = _p5; ptr6 = _p6;
	s_deflt1 = nullptr; s_deflt2 = nullptr; s_deflt3 = nullptr; s_deflt4 = nullptr;
	s_deflt5 = nullptr; s_deflt6 = nullptr;
	set = 0;
}


_TCHAR DFile::fileToUse[1024];
_TCHAR DFile::defaultFile[1024];
_TCHAR DFile::buf1[64];
_TCHAR DFile::buf2[64];
_TCHAR DFile::buf3[64];
_TCHAR DFile::buf4[64];
_TCHAR DFile::buf5[64];
_TCHAR DFile::buf6[64];

DFile::DFile(_TCHAR *file_to_use, _TCHAR *default_file, int n_arg)
    : create_template_file(0)
{
	if(file_to_use==nullptr) {
		_ftprintf_s(stderr, _T("No input file given.\n"));
        create_template_file = 1;
	    n_regs = 0;
	    fail = 0;
	    if(default_file)
		    _tcscpy_s(defaultFile, 1024*sizeof(_TCHAR), default_file);
        return;
		//PrintHelp();
		//if(default_file)
		//	PrepareDescrFile(default_file);
		//exit(1);
	}
	if(file_to_use)
		_tcscpy_s(fileToUse, 1024*sizeof(_TCHAR), file_to_use);
	else
		memset(fileToUse, 0, 1024*sizeof(_TCHAR));
	if(default_file)
		_tcscpy_s(defaultFile, 1024*sizeof(_TCHAR), default_file);
	else
		memset(defaultFile, 0, 1024*sizeof(_TCHAR));

	need_help = (n_arg>1 && _tcsnccmp(file_to_use, _T("-h"), 2)==0);
	def_file = default_file;
	
	if(n_arg==1) { // No arg given, use default_file
		if(!default_file) {
			_ftprintf_s(stderr, _T("No input file given or default input file specified.\n"));
			exit(1);
		}
		file = new _TCHAR[_tcslen(default_file)+1]; _tcscpy_s(file, _tcslen(default_file)+1, default_file);
		use_default_file = 1;
	} else {
		file = new _TCHAR[_tcslen(file_to_use)+1]; _tcscpy_s(file, _tcslen(file_to_use)+1, file_to_use);
		use_default_file = 0;
	}
	// File existence check is postponed till processing
	n_regs = 0;
	fail = 0;
}

DFile::~DFile()
{
	for(int i = 0; i<n_regs; i++)
		delete regs[i];
	delete [] file;
}

int DFile::NumSeparators(_TCHAR* stg, _TCHAR separator)
{
	int n = 0, len = (int)_tcslen(stg);
	for(int i = 0; i<len; i++) {
		if(stg[i]==separator) n++;
	}
	return(n);
}


int DFile::Process()
{
	if(create_template_file) {
		//_ftprintf_s(stderr, _T("No input file given.\n"));
		PrintHelp();
		if(defaultFile)
			PrepareDescrFile(defaultFile);
		exit(1);
	}
	if(need_help) {
		PrintHelp();
		exit(1);
	}

	_TCHAR stg[256], rest[256], *p;
	int i, j, len;
	
	// Check for existence of file
#if defined(WIN32) || defined(WIN64)
	if(_taccess(file, 0)==-1) {
#else
	if(access(file, F_OK)==-1) {
#endif
		_ftprintf_s(stderr, _T("File \"%s\" does not exist.\n"), file);
		if(def_file)
			// Prepare blank description file
			PrepareDescrFile(def_file);
		//PrintHelp();
		exit(1);
	}
	if(use_default_file) { // Request manual confirmation to use default file
		_ftprintf_s(stderr, _T("Using default file \"%s\". OK? (\"n\" creates template) "), def_file); 
		if(getchar()=='n') {
			if(def_file)
				// Prepare blank description file
				PrepareDescrFile(def_file);
			exit(1);
		}
	}
	//ifstream* fp = new ifstream(file);
	FILE *fp;
#ifdef _MSC_VER
	if(_tfopen_s(&fp, file, _T("r"))) {
		_ftprintf_s(stderr, _T("File \"%s\" cannot be opened.\n"), file);
#else
  fp = fopen(file, "r");
	if(!fp) {
		fprintf(stderr, "File \"%s\" cannot be opened.\n", file);
#endif
		if(def_file)
			// Prepare blank description file
			PrepareDescrFile(def_file);
		//PrintHelp();
		exit(1);
	}
	rest[0] = 0;
	fail = 0;
	//do {
		//fp->getline(stg, 256);

	while(_fgetts(stg, 255, fp)) {
		if(_tcslen(stg)<2)
			continue;
		// Rid of \10's at the end in case of Windows-like input
		while(stg[_tcslen(stg)-1]==10 || stg[_tcslen(stg)-1]==13)
			stg[_tcslen(stg)-1] = 0;

		// Nothing is read after #:
		if(p = _tcschr(stg, '#')) {
			// Also ignore whitespaces before #
			p--;
			while(p>stg && (*p==' ' || *p=='\t'))
				*(p--) = '\0';
		}
		// Skip leading whitespaces
		for(i = 0, p = stg; i<(int) _tcslen(stg); i++, p++) {
			if(stg[i]!=' ' && stg[i]!='\t') break;
		}
		// Check against all registered tags
		for(i = 0; i<n_regs; i++) {
			// Probably we'd like to compare the whole tag
			if(_tcsnccmp(p, regs[i]->tag, _tcslen(regs[i]->tag)) == 0) {
			//if(strcmp(p, regs[i]->tag) == 0) {
				// Check that the rest of line has something at all
				//for(j = _tcslen(regs[i]->tag)+1; j<256; j++) {
				//}
				memset(rest, 0, 256);
				_tcscpy_s(rest, 256, p+_tcslen(regs[i]->tag)+1);
				// Skip whitespaces
				for(j = (int) _tcslen(regs[i]->tag)+1; j<(int) _tcslen(p); j++) {
					if(stg[j]!=' ' && stg[j]!='\t') {
						_tcscpy_s(rest, 256, stg+j); 
						break;
					}
				}
				// Substitute whitespaces by spaces
				for(j = 0; j<(int) _tcslen(rest); j++) {
					if(rest[j]=='\t') rest[j] = ' ';
				}
				// Verify correct number of following arguments: - Not clear how to deal with the trailing spaces
				//if(regs[i]->args>0 && NumSeparators(rest, ' ')!=(regs[i]->args-1)) {
				//	_ftprintf_s(stderr, L"Option has wrong number of arguments.\n"); *argc = -1; return(0);
				//}
				switch(regs[i]->args) {
					case 1:
						switch(regs[i]->o_type) {
							case O_CHAR:
								_stscanf_s(rest, _T("%c"), ((_TCHAR *) regs[i]->ptr1), 1); // Yuri: '1' is required by VS2015
								break;
							case O_INT:
								_stscanf_s(rest, _T("%d"), ((int *) regs[i]->ptr1));
								break;
							case O_FLT:
								_stscanf_s(rest, _T("%f"), ((float *) regs[i]->ptr1));
								break;
							case O_DBL:
								_stscanf_s(rest, _T("%lf"), ((double *) regs[i]->ptr1));
								break;
							case O_STR:
								*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[_tcslen(rest)+1];
								_tcscpy_s(*((_TCHAR **)(regs[i]->ptr1)), _tcslen(rest)+1, rest);
								break;
							case O_BOOL:
								_stscanf_s(rest, _T("%s"), buf1, 64);
								*((bool*)(regs[i]->ptr1)) = (buf1[0]=='t' || buf1[0]=='T' || buf1[0]=='1');
								break;
						}
						break;
					case 2:
						switch(regs[i]->o_type) {
							case O_INT:
								_stscanf_s(rest, _T("%d %d"), ((int *) regs[i]->ptr1), ((int *) regs[i]->ptr2));
								break;
							case O_FLT:
								_stscanf_s(rest, _T("%f %f"), ((float *) regs[i]->ptr1), ((float *) regs[i]->ptr2));
								break;
							case O_DBL:
								_stscanf_s(rest, _T("%lf %lf"), ((double *) regs[i]->ptr1), ((double *) regs[i]->ptr2));
								break;
							case O_STR:
								len = (int)_tcslen(rest)+1;
								*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr2)) = new _TCHAR[len];
								_stscanf_s(rest, _T("%s %s"), *((_TCHAR **)(regs[i]->ptr1)), len, *((_TCHAR **)(regs[i]->ptr2)), len);
								break;
							case O_BOOL:
								_stscanf_s(rest, _T("%s %s"), buf1, 64, buf2, 64);
								*((bool*)(regs[i]->ptr1)) = (buf1[0]=='t' || buf1[0]=='T' || buf1[0]=='1');
								*((bool*)(regs[i]->ptr2)) = (buf2[0]=='t' || buf2[0]=='T' || buf2[0]=='1');
								break;
						}
						break;
					case 3:
						switch(regs[i]->o_type) {
							case O_INT:
								_stscanf_s(rest, _T("%d %d %d"), ((int *) regs[i]->ptr1), 
									((int *) regs[i]->ptr2), ((int *) regs[i]->ptr3));
								break;
							case O_FLT:
								_stscanf_s(rest, _T("%f %f %f"), ((float *) regs[i]->ptr1), ((float *) regs[i]->ptr2), ((float *) regs[i]->ptr3));
								break;
							case O_DBL:
								_stscanf_s(rest, _T("%lf %lf %lf"), ((double *) regs[i]->ptr1), ((double *) regs[i]->ptr2), ((double *) regs[i]->ptr3));
								break;
							case O_STR:
								len = (int)_tcslen(rest)+1;
								*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr2)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr3)) = new _TCHAR[len];
								_stscanf_s(rest, _T("%s %s %s"), *((_TCHAR **)(regs[i]->ptr1)), len, *((_TCHAR **)(regs[i]->ptr2)), len,
									*((_TCHAR **)(regs[i]->ptr3)), len);
								break;
							case O_BOOL:
								_stscanf_s(rest, _T("%s %s %s"), buf1, 64, buf2, 64, buf3, 64);
								*((bool*)(regs[i]->ptr1)) = (buf1[0]=='t' || buf1[0]=='T' || buf1[0]=='1');
								*((bool*)(regs[i]->ptr2)) = (buf2[0]=='t' || buf2[0]=='T' || buf2[0]=='1');
								*((bool*)(regs[i]->ptr3)) = (buf3[0]=='t' || buf3[0]=='T' || buf3[0]=='1');
								break;
						}
						break;
					case 4:
						switch(regs[i]->o_type) {
							case O_INT:
								_stscanf_s(rest, _T("%d %d %d %d"), ((int *) regs[i]->ptr1), 
									((int *) regs[i]->ptr2), ((int *) regs[i]->ptr3), ((int *) regs[i]->ptr4));
								break;
							case O_STR:
								len = (int)_tcslen(rest)+1;
								*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr2)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr3)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr4)) = new _TCHAR[len];
								_stscanf_s(rest, _T("%s %s %s %s"), *((_TCHAR **)(regs[i]->ptr1)), len, *((_TCHAR **)(regs[i]->ptr2)), len,
									*((_TCHAR **)(regs[i]->ptr3)), len, *((_TCHAR **)(regs[i]->ptr4)), len);
								break;
						}
						break;
					case 5:
						switch(regs[i]->o_type) {
							case O_STR:
								len = (int)_tcslen(rest)+1;
								*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr2)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr3)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr4)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr5)) = new _TCHAR[len];
								_stscanf_s(rest, _T("%s %s %s %s %s"), *((_TCHAR **)(regs[i]->ptr1)), len, *((_TCHAR **)(regs[i]->ptr2)), len,
									*((_TCHAR **)(regs[i]->ptr3)), len, *((_TCHAR **)(regs[i]->ptr4)), len,
									*((_TCHAR **)(regs[i]->ptr5)), len);
								break;
						}
						break;
					case 6:
						switch(regs[i]->o_type) {
							case O_STR:
								len = (int)_tcslen(rest)+1;
								*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr2)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr3)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr4)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr5)) = new _TCHAR[len];
								*((_TCHAR **)(regs[i]->ptr6)) = new _TCHAR[len];
								_stscanf_s(rest, _T("%s %s %s %s %s %s"), *((_TCHAR **)(regs[i]->ptr1)), len, *((_TCHAR **)(regs[i]->ptr2)), len,
									*((_TCHAR **)(regs[i]->ptr3)), len, *((_TCHAR **)(regs[i]->ptr4)), len,
									*((_TCHAR **)(regs[i]->ptr5)), len, *((_TCHAR **)(regs[i]->ptr6)), len);
								break;
						}
						break;
				}
				regs[i]->set = 1; // Mark as set
			}
		}
	//} while(!fp->fail());
	}
	fail = 1;
	//fp->close();
	//delete fp; //Dies when used with MFC
	fclose(fp);
	
	// Set all unset values to provided defaults
	for(i = 0; i<n_regs; i++) {
		if(regs[i]->set) continue; // Do nothing
		switch(regs[i]->o_type) {
			case O_CHAR:
				*(_TCHAR *) regs[i]->ptr1 = regs[i]->c_deflt1;
				break;
			case O_INT:
				*(int *) regs[i]->ptr1 = regs[i]->i_deflt1;
				if(regs[i]->ptr2) *(int *) regs[i]->ptr2 = regs[i]->i_deflt2;
				if(regs[i]->ptr3) *(int *) regs[i]->ptr3 = regs[i]->i_deflt3;
				if(regs[i]->ptr4) *(int *) regs[i]->ptr4 = regs[i]->i_deflt4;
				break;
			case O_FLT:
				*(float *) regs[i]->ptr1 = regs[i]->f_deflt1;
				if(regs[i]->ptr2) *(float *) regs[i]->ptr2 = regs[i]->f_deflt2;
				break;
			case O_DBL:
				*(double *) regs[i]->ptr1 = regs[i]->d_deflt1;
				if(regs[i]->ptr2) *(double *) regs[i]->ptr2 = regs[i]->d_deflt2;
				break;
			case O_STR:
				if(regs[i]->s_deflt1) {
					*((_TCHAR **)(regs[i]->ptr1)) = new _TCHAR[_tcslen(regs[i]->s_deflt1)+1];
					_tcscpy_s(*((_TCHAR **)(regs[i]->ptr1)), _tcslen(regs[i]->s_deflt1)+1, regs[i]->s_deflt1);
				} else { // Default is nullptr
					*((_TCHAR **)(regs[i]->ptr1)) = nullptr;
				}
				if(regs[i]->ptr2) {
					if(regs[i]->s_deflt2) {
						*((_TCHAR **)(regs[i]->ptr2)) = new _TCHAR[_tcslen(regs[i]->s_deflt2)+1];
						_tcscpy_s(*((_TCHAR **)(regs[i]->ptr2)), _tcslen(regs[i]->s_deflt2)+1, regs[i]->s_deflt2);
					} else { // Default is nullptr
						*((_TCHAR **)(regs[i]->ptr2)) = nullptr;
					}
				}
				if(regs[i]->ptr3) {
					if(regs[i]->s_deflt3) {
						*((_TCHAR **)(regs[i]->ptr3)) = new _TCHAR[_tcslen(regs[i]->s_deflt3)+1];
						_tcscpy_s(*((_TCHAR **)(regs[i]->ptr3)), _tcslen(regs[i]->s_deflt3)+1, regs[i]->s_deflt3);
					} else { // Default is nullptr
						*((_TCHAR **)(regs[i]->ptr3)) = nullptr;
					}
				}
				if(regs[i]->ptr4) {
					if(regs[i]->s_deflt4) {
						*((_TCHAR **)(regs[i]->ptr4)) = new _TCHAR[_tcslen(regs[i]->s_deflt4)+1];
						_tcscpy_s(*((_TCHAR **)(regs[i]->ptr4)), _tcslen(regs[i]->s_deflt4)+1, regs[i]->s_deflt4);
					} else { // Default is nullptr
						*((_TCHAR **)(regs[i]->ptr4)) = nullptr;
					}
				}
				if(regs[i]->ptr5) {
					if(regs[i]->s_deflt5) {
						*((_TCHAR **)(regs[i]->ptr5)) = new _TCHAR[_tcslen(regs[i]->s_deflt5)+1];
						_tcscpy_s(*((_TCHAR **)(regs[i]->ptr5)), _tcslen(regs[i]->s_deflt5)+1, regs[i]->s_deflt5);
					} else { // Default is nullptr
						*((_TCHAR **)(regs[i]->ptr5)) = nullptr;
					}
				}
				if(regs[i]->ptr6) {
					if(regs[i]->s_deflt6) {
						*((_TCHAR **)(regs[i]->ptr6)) = new _TCHAR[_tcslen(regs[i]->s_deflt6)+1];
						_tcscpy_s(*((_TCHAR **)(regs[i]->ptr6)), _tcslen(regs[i]->s_deflt6)+1, regs[i]->s_deflt6);
					} else { // Default is nullptr
						*((_TCHAR **)(regs[i]->ptr6)) = nullptr;
					}
				}
				break;
			case O_BOOL:
				*(bool*) regs[i]->ptr1 = regs[i]->b_deflt1;
				if(regs[i]->ptr2) *(bool*) regs[i]->ptr2 = regs[i]->b_deflt2;
				if(regs[i]->ptr3) *(bool*) regs[i]->ptr3 = regs[i]->b_deflt3;
				break;
		}
	}
	
	return(0);
}

void DFile::PrintHelp()
{
	_ftprintf_s(stderr, _T("Description file help:\n"));
	for(int i = 0; i<n_regs; i++) {
		switch(regs[i]->args) {
			case 1:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), regs[i]->tag, regs[i]->descr, regs[i]->args);
				switch(regs[i]->o_type) {
					case O_CHAR:
						_ftprintf_s(stderr, _T("Dflt: %c\n"), regs[i]->c_deflt1);
						break;
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d\n"), regs[i]->i_deflt1);
						break;
					case O_FLT:
						_ftprintf_s(stderr, _T("Dflt: %f\n"), regs[i]->f_deflt1);
						break;
					case O_DBL:
						_ftprintf_s(stderr, _T("Dflt: %lf\n"), regs[i]->d_deflt1);
						break;
					case O_STR:
						_ftprintf_s(stderr, _T("Dflt: \"%s\"\n"), regs[i]->s_deflt1);
						break;
				}
				break;
			case 2:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), regs[i]->tag, regs[i]->descr, regs[i]->args);
				switch(regs[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d "), regs[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt1: %d\n"), regs[i]->i_deflt2);
						break;
					case O_FLT:
						_ftprintf_s(stderr, _T("Dflt: %f "), regs[i]->f_deflt1);
						_ftprintf_s(stderr, _T("Dflt1: %f\n"), regs[i]->f_deflt2);
						break;
					case O_DBL:
						_ftprintf_s(stderr, _T("Dflt: %lf "), regs[i]->d_deflt1);
						_ftprintf_s(stderr, _T("Dflt1: %lf\n"), regs[i]->d_deflt2);
						break;
					case O_STR:
						_ftprintf_s(stderr, _T("Dflt: \"%s\", "), regs[i]->s_deflt1);
						_ftprintf_s(stderr, _T("Dflt1: \"%s\"\n"), regs[i]->s_deflt2);
						break;
				}
				break;
		}
	}
}

void DFile::Print()
{
	_ftprintf_s(stderr, _T("Description file content:\n"));
	for(int i = 0; i<n_regs; i++) {
		switch(regs[i]->args) {
			case 1:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), regs[i]->tag, regs[i]->descr, regs[i]->args);
				switch(regs[i]->o_type) {
					case O_CHAR:
						_ftprintf_s(stderr, _T("Dflt: %c\n"), regs[i]->c_deflt1);
						_ftprintf_s(stderr, _T("Val: %c\n"), *(_TCHAR *) regs[i]->ptr1);
						break;
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d\n"), regs[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Val: %d\n"), *(int *) regs[i]->ptr1);
						break;
					case O_FLT:
						_ftprintf_s(stderr, _T("Dflt: %f\n"), regs[i]->f_deflt1);
						_ftprintf_s(stderr, _T("Val: %f\n"), *(float *) regs[i]->ptr1);
						break;
					case O_DBL:
						_ftprintf_s(stderr, _T("Dflt: %lf\n"), regs[i]->d_deflt1);
						_ftprintf_s(stderr, _T("Val: %lf\n"), *(double *) regs[i]->ptr1);
						break;
					case O_STR:
						_ftprintf_s(stderr, _T("Dflt: \"%s\"\n"), regs[i]->s_deflt1);
						_ftprintf_s(stderr, _T("Val: \"%s\"\n"), *((_TCHAR **)(regs[i]->ptr1)));
						break;
					case O_BOOL:
						_ftprintf_s(stderr, _T("Dflt: \"%s\"\n"), regs[i]->b_deflt1 ? "true" : "false");
						_ftprintf_s(stderr, _T("Val: \"%s\"\n"), *(bool*)(regs[i]->ptr1) ? "true" : "false");
						break;
				}
				break;
			case 2:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), regs[i]->tag, regs[i]->descr, regs[i]->args);
				switch(regs[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d "), regs[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %d\n"), regs[i]->i_deflt2);
						_ftprintf_s(stderr, _T("Val: %d "), *(int *) regs[i]->ptr1);
						_ftprintf_s(stderr, _T("Val2: %d\n"), *(int *) regs[i]->ptr2);
						break;
					case O_FLT:
						_ftprintf_s(stderr, _T("Dflt: %f "), regs[i]->f_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %f\n"), regs[i]->f_deflt2);
						_ftprintf_s(stderr, _T("Val: %f "), *(float *) regs[i]->ptr1);
						_ftprintf_s(stderr, _T("Val2: %f\n"), *(float *) regs[i]->ptr2);
						break;
					case O_DBL:
						_ftprintf_s(stderr, _T("Dflt: %lf "), regs[i]->d_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %lf\n"), regs[i]->d_deflt2);
						_ftprintf_s(stderr, _T("Val: %lf "), *(double *) regs[i]->ptr1);
						_ftprintf_s(stderr, _T("Val2: %lf\n"), *(double *) regs[i]->ptr2);
						break;
					case O_STR:
						_ftprintf_s(stderr, _T("Dflt: \"%s\", "), regs[i]->s_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: \"%s\"\n"), regs[i]->s_deflt2);
						_ftprintf_s(stderr, _T("Val: \"%s\" "), *((_TCHAR **)(regs[i]->ptr1)));
						_ftprintf_s(stderr, _T("Val2: \"%s\"\n"), *((_TCHAR **)(regs[i]->ptr2)));
						break;
					case O_BOOL:
						_ftprintf_s(stderr, _T("Dflt: \"%s\", "), regs[i]->b_deflt1 ? "true" : "false");
						_ftprintf_s(stderr, _T("Dflt2: \"%s\"\n"), regs[i]->b_deflt2 ? "true" : "false");
						_ftprintf_s(stderr, _T("Val: \"%s\", "), *(bool*)(regs[i]->ptr1) ? "true" : "false");
						_ftprintf_s(stderr, _T("Val2: \"%s\"\n"), *(bool*)(regs[i]->ptr2) ? "true" : "false");
						break;
				}
				break;
			case 3:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), regs[i]->tag, regs[i]->descr, regs[i]->args);
				switch(regs[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d, "), regs[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %d, "), regs[i]->i_deflt2);
						_ftprintf_s(stderr, _T("Dflt3: %d\n"), regs[i]->i_deflt3);
						_ftprintf_s(stderr, _T("Val: %d, "), *(int *) regs[i]->ptr1);
						_ftprintf_s(stderr, _T("Val2: %d, "), *(int *) regs[i]->ptr2);
						_ftprintf_s(stderr, _T("Val3: %d\n"), *(int *) regs[i]->ptr3);
						break;
						break;
					case O_BOOL:
						_ftprintf_s(stderr, _T("Dflt: \"%s\", "), regs[i]->b_deflt1 ? "true" : "false");
						_ftprintf_s(stderr, _T("Dflt2: \"%s\", "), regs[i]->b_deflt2 ? "true" : "false");
						_ftprintf_s(stderr, _T("Dflt3: \"%s\"\n"), regs[i]->b_deflt3 ? "true" : "false");
						_ftprintf_s(stderr, _T("Val: \"%s\", "), *(bool*)(regs[i]->ptr1) ? "true" : "false");
						_ftprintf_s(stderr, _T("Val2: \"%s\", "), *(bool*)(regs[i]->ptr2) ? "true" : "false");
						_ftprintf_s(stderr, _T("Val3: \"%s\"\n"), *(bool*)(regs[i]->ptr3) ? "true" : "false");
						break;
				}
				break;
			case 4:
				_ftprintf_s(stderr, _T("<%s> %s: %d vars, "), regs[i]->tag, regs[i]->descr, regs[i]->args);
				switch(regs[i]->o_type) {
					case O_INT:
						_ftprintf_s(stderr, _T("Dflt: %d, "), regs[i]->i_deflt1);
						_ftprintf_s(stderr, _T("Dflt2: %d, "), regs[i]->i_deflt2);
						_ftprintf_s(stderr, _T("Dflt3: %d, "), regs[i]->i_deflt3);
						_ftprintf_s(stderr, _T("Dflt4: %d\n"), regs[i]->i_deflt4);
						_ftprintf_s(stderr, _T("Val: %d, "), *(int *) regs[i]->ptr1);
						_ftprintf_s(stderr, _T("Val2: %d, "), *(int *) regs[i]->ptr2);
						_ftprintf_s(stderr, _T("Val3: %d, "), *(int *) regs[i]->ptr3);
						_ftprintf_s(stderr, _T("Val4: %d\n"), *(int *) regs[i]->ptr4);
						break;
						break;
				}
				break;
		}
	}
}

void DFile::PrepareDescrFile(_TCHAR *name)
{
	FILE *fo;
#ifdef _MSC_VER
	if(_tfopen_s(&fo, name, _T("w"))) {
		_ftprintf_s(stderr, _T("Cannot prepare template file \"%s\".\n"), name);
		exit(1);
	}
#else
  fo = fopen(name, "w");
	if(!fo) {
		fprintf(stderr, "Cannot prepare template file \"%s\".\n", name);
		exit(1);
	}
#endif
	for(int i = 0; i<n_regs; i++) {
		switch(regs[i]->args) {
			case 1:
				//_ftprintf_s(fo, _T("%s\t\t####\t\t# %s\n"), regs[i]->tag, regs[i]->descr);
				if(regs[i]->o_type==O_INT)
					_ftprintf_s(fo, _T("%s\t\t%d\t\t# %s\n"), regs[i]->tag, regs[i]->i_deflt1, regs[i]->descr);
				else if(regs[i]->o_type==O_FLT)
					_ftprintf_s(fo, _T("%s\t\t%f\t\t# %s\n"), regs[i]->tag, regs[i]->f_deflt1, regs[i]->descr);
				else if(regs[i]->o_type==O_DBL)
					_ftprintf_s(fo, _T("%s\t\t%lf\t\t# %s\n"), regs[i]->tag, regs[i]->d_deflt1, regs[i]->descr);
				else if(regs[i]->o_type==O_STR)
					_ftprintf_s(fo, _T("%s\t\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->s_deflt1, regs[i]->descr);
				else if(regs[i]->o_type==O_BOOL)
					_ftprintf_s(fo, _T("%s\t\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->b_deflt1 ? "TRUE" : "FALSE", regs[i]->descr);
				else if(regs[i]->o_type==O_CHAR)
					_ftprintf_s(fo, _T("%s\t\t%c\t\t# %s\n"), regs[i]->tag, regs[i]->c_deflt1, regs[i]->descr);
				break;
			case 2:
				//_ftprintf_s(fo, _T("%s\t\t####\t####\t\t# %s\n"), regs[i]->tag, regs[i]->descr);
				if(regs[i]->o_type==O_INT)
					_ftprintf_s(fo, _T("%s\t\t%d\t%d\t\t# %s\n"), regs[i]->tag, regs[i]->i_deflt1, regs[i]->i_deflt2, regs[i]->descr);
				else if(regs[i]->o_type==O_FLT)
					_ftprintf_s(fo, _T("%s\t\t%f\t%f\t\t# %s\n"), regs[i]->tag, regs[i]->f_deflt1, regs[i]->f_deflt2, regs[i]->descr);
				else if(regs[i]->o_type==O_DBL)
					_ftprintf_s(fo, _T("%s\t\t%lf\t%lf\t\t# %s\n"), regs[i]->tag, regs[i]->d_deflt1, regs[i]->d_deflt2, regs[i]->descr);
				else if(regs[i]->o_type==O_STR)
					_ftprintf_s(fo, _T("%s\t\t%s\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->s_deflt1, regs[i]->s_deflt2, regs[i]->descr);
				else if(regs[i]->o_type==O_BOOL)
					_ftprintf_s(fo, _T("%s\t\t%s\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->b_deflt1 ? "TRUE" : "FALSE", regs[i]->b_deflt2 ? "TRUE" : "FALSE", regs[i]->descr);
				else if(regs[i]->o_type==O_CHAR)
					_ftprintf_s(fo, _T("%s\t\t%c\t%c\t\t# %s\n"), regs[i]->tag, regs[i]->c_deflt1, regs[i]->c_deflt2, regs[i]->descr);
				break;
			case 3:
				//_ftprintf_s(fo, _T("%s\t\t####\t####\t####\t\t# %s\n"), regs[i]->tag, regs[i]->descr);
				if(regs[i]->o_type==O_INT)
					_ftprintf_s(fo, _T("%s\t\t%d\t%d\t%d\t\t# %s\n"), regs[i]->tag, regs[i]->i_deflt1, regs[i]->i_deflt2, regs[i]->i_deflt3, regs[i]->descr);
				else if(regs[i]->o_type==O_FLT)
					_ftprintf_s(fo, _T("%s\t\t%f\t%f\t%f\t\t# %s\n"), regs[i]->tag, regs[i]->f_deflt1, regs[i]->f_deflt2, regs[i]->f_deflt3, regs[i]->descr);
				else if(regs[i]->o_type==O_DBL)
					_ftprintf_s(fo, _T("%s\t\t%lf\t%lf\t%lf\t\t# %s\n"), regs[i]->tag, regs[i]->d_deflt1, regs[i]->d_deflt2, regs[i]->d_deflt3, regs[i]->descr);
				else if(regs[i]->o_type==O_STR)
					_ftprintf_s(fo, _T("%s\t\t%s\t%s\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->s_deflt1, regs[i]->s_deflt2, regs[i]->s_deflt3, regs[i]->descr);
				else if(regs[i]->o_type==O_BOOL)
					_ftprintf_s(fo, _T("%s\t\t%s\t%s\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->b_deflt1 ? "TRUE" : "FALSE", regs[i]->b_deflt2 ? "TRUE" : "FALSE", regs[i]->b_deflt3 ? "TRUE" : "FALSE", regs[i]->descr);
				else if(regs[i]->o_type==O_CHAR)
					_ftprintf_s(fo, _T("%s\t\t%c\t%c\t%c\t\t# %s\n"), regs[i]->tag, regs[i]->c_deflt1, regs[i]->c_deflt2, regs[i]->c_deflt3, regs[i]->descr);
				break;
			case 4:
				//_ftprintf_s(fo, _T("%s\t\t####\t####\t####\t####\t\t# %s\n"), regs[i]->tag, regs[i]->descr);
				if(regs[i]->o_type==O_INT)
					_ftprintf_s(fo, _T("%s\t\t%d\t%d\t%d\t%d\t\t# %s\n"), regs[i]->tag, regs[i]->i_deflt1, regs[i]->i_deflt2, regs[i]->i_deflt3, regs[i]->i_deflt4, regs[i]->descr);
				//else if(regs[i]->o_type==O_FLT)
				//	_ftprintf_s(fo, _T("%s\t\t%f\t%f\t%f\t%f\t\t# %s\n"), regs[i]->tag, regs[i]->f_deflt1, regs[i]->f_deflt2, regs[i]->f_deflt3, regs[i]->f_deflt4, regs[i]->descr);
				//else if(regs[i]->o_type==O_DBL)
				//	_ftprintf_s(fo, _T("%s\t\t%lf\t%lf\t%lf\t%lf\t\t# %s\n"), regs[i]->tag, regs[i]->d_deflt1, regs[i]->d_deflt2, regs[i]->d_deflt3, regs[i]->d_deflt4, regs[i]->descr);
				else if(regs[i]->o_type==O_STR)
					_ftprintf_s(fo, _T("%s\t\t%s\t%s\t%s\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->s_deflt1, regs[i]->s_deflt2, regs[i]->s_deflt3, regs[i]->s_deflt4, regs[i]->descr);
				//else if(regs[i]->o_type==O_BOOL)
				//	_ftprintf_s(fo, _T("%s\t\t%s\t%s\t%s\t%s\t\t# %s\n"), regs[i]->tag, regs[i]->b_deflt1 ? "TRUE" : "FALSE", regs[i]->b_deflt2 ? "TRUE" : "FALSE", regs[i]->b_deflt3 ? "TRUE" : "FALSE", regs[i]->b_deflt4 ? "TRUE" : "FALSE", regs[i]->descr);
				//else if(regs[i]->o_type==O_CHAR)
				//	_ftprintf_s(fo, _T("%s\t\t%c\t%c\t%c\t%c\t\t# %s\n"), regs[i]->tag, regs[i]->c_deflt1, regs[i]->c_deflt2, regs[i]->c_deflt3, regs[i]->c_deflt4, regs[i]->descr);
				break;
		}
	}
	fclose(fo);
	_ftprintf_s(stderr, _T("\n\nTemplate description file \"%s\" written.\n"), name);
}

