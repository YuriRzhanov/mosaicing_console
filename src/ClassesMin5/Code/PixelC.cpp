#include <stdlib.h>
#include <iostream>
//#include <math.h>
#include <string>
#include "PixelC.h"

using namespace std;

#ifdef _MSC_VER
Y_RGB<u_char> Y_RGB<u_char>::INVALID_RGB = Y_RGB<u_char>(Y_Utils::_INVALID_UCHAR, Y_Utils::_INVALID_UCHAR, Y_Utils::_INVALID_UCHAR);

Y_RGB<double> Y_RGB<double>::INVALID_RGBD = Y_RGB<double>(Y_Utils::_INVALID_DOUBLE, Y_Utils::_INVALID_DOUBLE, Y_Utils::_INVALID_DOUBLE);

Y_RGB<u_char> Y_RGB<u_char>::WhitePixel = Y_RGB<u_char>(255,255,255);
Y_RGB<u_char> Y_RGB<u_char>::BlackPixel = Y_RGB<u_char>(0,0,0);
Y_RGB<u_char> Y_RGB<u_char>::Pixel254 = Y_RGB<u_char>(254,254,254);
Y_RGB<u_char> Y_RGB<u_char>::RedPixel = Y_RGB<u_char>(255,0,0);
Y_RGB<u_char> Y_RGB<u_char>::GreenPixel = Y_RGB<u_char>(0,255,0);
Y_RGB<u_char> Y_RGB<u_char>::BluePixel = Y_RGB<u_char>(0,0,255);
Y_RGB<u_char> Y_RGB<u_char>::YellowPixel = Y_RGB<u_char>(255,255,0);
Y_RGB<u_char> Y_RGB<u_char>::CyanPixel = Y_RGB<u_char>(0,255,255);
Y_RGB<u_char> Y_RGB<u_char>::MagentaPixel = Y_RGB<u_char>(255,0,255);
#else

template<>
Y_RGB<u_char> Y_RGB<u_char>::INVALID_RGB = Y_RGB<u_char>(Y_Utils::_INVALID_UCHAR, Y_Utils::_INVALID_UCHAR, Y_Utils::_INVALID_UCHAR);
template<>
Y_RGB<double> Y_RGB<double>::INVALID_RGBD = Y_RGB<double>(Y_Utils::_INVALID_DOUBLE, Y_Utils::_INVALID_DOUBLE, Y_Utils::_INVALID_DOUBLE);
template<>
Y_RGB<u_char> Y_RGB<u_char>::WhitePixel = Y_RGB<u_char>(255,255,255);
template<>
Y_RGB<u_char> Y_RGB<u_char>::BlackPixel = Y_RGB<u_char>(0,0,0);
template<>
Y_RGB<u_char> Y_RGB<u_char>::Pixel254 = Y_RGB<u_char>(254,254,254);
template<>
Y_RGB<u_char> Y_RGB<u_char>::RedPixel = Y_RGB<u_char>(255,0,0);
template<>
Y_RGB<u_char> Y_RGB<u_char>::GreenPixel = Y_RGB<u_char>(0,255,0);
template<>
Y_RGB<u_char> Y_RGB<u_char>::BluePixel = Y_RGB<u_char>(0,0,255);
template<>
Y_RGB<u_char> Y_RGB<u_char>::YellowPixel = Y_RGB<u_char>(255,255,0);
template<>
Y_RGB<u_char> Y_RGB<u_char>::CyanPixel = Y_RGB<u_char>(0,255,255);
template<>
Y_RGB<u_char> Y_RGB<u_char>::MagentaPixel = Y_RGB<u_char>(255,0,255);
#endif

template <class T>
void Y_RGB<T>::AddNoise(double noisePercent)
{
	double r = (1.0+0.01*noisePercent*Y_Utils::d_rand(-1.0,1.0))*p[0];
	double g = (1.0+0.01*noisePercent*Y_Utils::d_rand(-1.0,1.0))*p[1];
	double b = (1.0+0.01*noisePercent*Y_Utils::d_rand(-1.0,1.0))*p[2];
	if(typeid(T)==typeid(u_char)) {
		p[0] = LIMIT_RANGE_CHAR(r);
		p[1] = LIMIT_RANGE_CHAR(g);
		p[2] = LIMIT_RANGE_CHAR(b);
	} else {
		p[0] = D_LIMIT_RANGE_CHAR(r);
		p[1] = D_LIMIT_RANGE_CHAR(g);
		p[2] = D_LIMIT_RANGE_CHAR(b);
	}
}



/*
// Operators
template <class T>
Y_RGB<T> &operator +=(Y_RGB<T>& v, const Y_RGB<T>& w) {
	v.p[0] += w.p[0]; v.p[1] += w.p[1]; v.p[2] += w.p[2];
	return(v);
}
template <class T>
Y_RGB<T> &operator -=(Y_RGB<T>& v, const Y_RGB<T>& w) { 
	v.p[0] = abs(v.p[0]-w.p[0]); v.p[1] = abs(v.p[1]-w.p[1]); v.p[2] = abs(v.p[2]-w.p[2]);
	return(v);
}
template <class T>
Y_RGB<T> &operator *=(Y_RGB<T>& v, double s) {
	v.p[0] = (T) (s*v.p[0]); v.p[1] = (T) (s*v.p[1]); v.p[2] = (T) (s*v.p[2]);
	return(v);
}
template <class T>
Y_RGB<T> operator *(const Y_RGB<T>& v, double s) { 
	Y_RGB<T> w(T(v.p[0]*s), T(v.p[1]*s), T(v.p[2]*s));
	return(w); 
}
template <class T>
Y_RGB<T> operator /(const Y_RGB<T>& v, double s) { 
	return(Y_RGB(T(v.p[0]/s), T(v.p[1]/s), T(v.p[2]/s))); 
}
template <class T>
Y_RGB<T> operator +(const Y_RGB<T>& v, const Y_RGB<T>& w) {
	return(Y_RGB<T>(v.p[0]+w.p[0], v.p[1]+w.p[1], v.p[2]+w.p[2]));
}
template <class T>
Y_RGB<T> operator -(const Y_RGB<T>& v, const Y_RGB<T>& w) {
	return(Y_RGB<T>(abs(v.p[0]-w.p[0]), abs(v.p[1]-w.p[1]), abs(v.p[2]-w.p[2])));
}
*/

