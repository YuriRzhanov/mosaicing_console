#include "Point2.h"

using namespace std;

template <class T>
bool Point2<T>::IsZero() const
{
	if(typeid(T)==typeid(int))
		return(x==0 && y==0);
	else
		return(EPSILON_EQ(0.0, Length()));
	//if(SameType<T,int>::result)
	//	return(x==0 && y==0);
	//else
	//	return(DOUBLE_EQ(0.0, Length()));
}

template <class T>
bool Point2<T>::IsUnit() const
{
	if(typeid(T)==typeid(int))
		return(Length()==1);
	else
		return(EPSILON_EQ(1.0, Length()));
	//if(SameType<T,int>::result)
	//	return(1==Length());
	//else
	//	return(DOUBLE_EQ(1.0, Length()));
}

template <class T>
int Point2<T>::IsParallel(const Point2& v) const
{
	//if(SameType<T,int>::result) {
	if(typeid(T)==typeid(int)) {
		fprintf(stderr, "Not applicable to integers.\n");
		return(0);
	}
	assert(!IsZero() && !v.IsZero());		// both not zero vectors

	Point2 v1, v2;			// temp vectors

	v1.Set(x, y);		// copy to temp vectors
	v2 = v;

	v1.Normalize();			// normalize both of them
	v2.Normalize();

	//double angle = v1.Scalarproduct(v2);	// get value between them
	double angle = DotProduct(v1, v2);	// get value between them

	return(DOUBLE_EQ(-1.0, angle) || DOUBLE_EQ(1.0, angle)); // if cos angle is -1 or 1, the vectors are parallel
}

template <class T>
void Point2<T>::Rotate(double A)
{
	double c = cos(A), s = sin(A);
	
	double x1 = c*x - s*y;
	double y1 = s*x + c*y;
	x = x1; y = y1;
}

template <class T>
void Point2<T>::Rotate(NEWMAT::Matrix& M)
{
	if(M.Ncols()!=2 || M.Nrows()!=2) {
		fprintf(stderr, "Matrix is not 2x2.\n");
		return;
	}
	NEWMAT::ColumnVector p(2);
	p<<x<<y;
	p = M*p;
	x = p(1); y = p(2);
}

/*
template <class T>
void Point2<T>::apply_transform(NEWMAT::Matrix2 *m)
{
	double tmp[3], v[3];
	int i, j;
	
	v[0] = x; v[1] = y; v[2] = w;
	
	for(j = 0; j<3; j++) {
		tmp[j] = 0.0;
		for(i = 0; i<3; i++)
			tmp[j] += m->elem[i][j] * v[i];
	}
	x = tmp[0]; y = tmp[1]; w = tmp[2];
}
*/

// Directional vector on the basis of current vertor vc and previous v
template <class T>
Point2<T> Point2<T>::Direction(const Point2<T>& pc, const Point2<T>& p)
{
	Point2 dir(pc.x-p.x, pc.y-p.y);
	dir.Normalize();
	return(dir);
}

// Linear interpolation between current vector vc (t=1) and previous v (t=0)
template <class T>
Point2<T> Point2<T>::Interpolate(const Point2<T>& pc, const Point2<T>& p, double t)
{
	Point2 tp(p.x+t*(pc.x-p.x), p.y+t*(pc.y-p.y));
	return(tp);
}

/*
// If 'this' intersects with 'S', returns 'true', otherwise 'false'
// 'X' is a cross-section, possibly outside the range of segments
template <class T>
bool Segment2<T>::Intersect(Segment2<T> S, Point2<T>& X) 
{
	Point2<T> L = end-begin;
	// Equation for a ray: r=begin+s*L
	Point2<T> l = L; l.Normalize();
	// Normal to 'this':
	Point2<T> normal(l.y, -l.x);

	Point2<T> SL = S.end-S.begin;
	// Equation for a S-ray: r=S.begin+sS*(S.end-S.begin)
	Point2<T> Sl = SL; Sl.Normalize();
	// Normal to 'S':
	Point2<T> Snormal(Sl.y, -Sl.x);

	Point2<T> v1 = begin-S.begin;
	T sS = v1.Scalarproduct(normal)/SL.Scalarproduct(normal);
	X = S.begin+sS*SL;

	Point2<T> v2 = S.begin-begin;
	T s = v2.Scalarproduct(Snormal)/L.Scalarproduct(Snormal);
	X = begin+s*L;

	if(s>=(T)0 && s<=(T)1 && sS>=(T)0 && sS<=(T)1)
		return(true);
	return(false);
}
*/
