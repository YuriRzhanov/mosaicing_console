#ifndef _BMPIMAGEC_H
#define _BMPIMAGEC_H

#define SPAN 3

// Storage organization: (RGB) (RGB) ...
class BMPImageC;
class DblImageG;
#ifndef CLASSESMIN
class BMPImageB;
#endif
class BMPImageG;

#include <vector>
#include "PixelC.h"
#include "BMPImage.h"
#include "BMPImageG.h"
#include "IntImageG.h"


class BMPImageC : public BMPImage {
	public:
		BMPImageC();
		BMPImageC(int w, int h);
		BMPImageC(int w, int h, u_char *_im);
		BMPImageC(int w, int h, u_char *r, u_char *g, u_char *b);
		BMPImageC(int w, int h, double *d); // Rainbow colouring
		BMPImageC(const char *file, int suppress_warnings = 1);
		BMPImageC(BMPImageG *imG);
		BMPImageC(IntImageG *imG, bool scale = true);
		~BMPImageC();
		void Dimensions(int w, int h);
		BMPImageC *Copy();
#ifndef CLASSESMIN
		BMPImageB *Copy(Y_RGB<u_char> set_clr);
#endif
		void Copy(BMPImageG *s);
		void Copy(BMPImageC *s);
		bool set_pixel(Point2<int> P, Y_RGB<u_char> t) {
			return(set_pixel(P.X(), P.Y(), t)); 
		}
		bool set_pixel(Point2<int> P, Y_RGB<double> t) {
			return(set_pixel(P.X(), P.Y(), t)); 
		}
		bool set_pixel(int pos, u_char r, u_char g, u_char b) {
			if(!Y_Image::valid(pos)) return(false);
			im[SPAN*pos+0] = r; im[SPAN*pos+1] = g; im[SPAN*pos+2] = b; 
			return(true); 
		}
		bool set_pixel(int x, int y, u_char r, u_char g, u_char b) {
			if(!Y_Image::valid(x,y)) 
				return(false);
			im[SPAN*(y*w+x)+0] = r; im[SPAN*(y*w+x)+1] = g; im[SPAN*(y*w+x)+2] = b; return(true); }
		bool set_pixel(int pos, Y_RGB<u_char> t) {
			if(!Y_Image::valid(pos)) 
				return(false);
			memcpy(&im[SPAN*pos], t.p, SPAN); return(true); 
		}
		bool set_pixel(int x, int y, Y_RGB<u_char> t) {
			if(!Y_Image::valid(x,y)) 
				return(false);
			memcpy(&im[SPAN*(y*w+x)], t.p, SPAN); return(true); 
		}
		bool set_pixel(int x, int y, Y_RGB<double> t) {
			if(!Y_Image::valid(x,y)) return(false);
			if(t.Valid()==0)
				set_pixel(x, y, Y_RGB<u_char>::INVALID_RGB);
			else
				set_pixel(x, y, u_char(t.p[0]), u_char(t.p[1]), u_char(t.p[2]));
			return(true); 
		}
		bool set_pixel(int x, int y, Y_RGB<float> v) {
				return(set_pixel(x, y, u_char(v.p[0]), u_char(v.p[1]), u_char(v.p[2]))); }
		bool set_pixel(int x, int y, u_char v) {
				return(set_pixel(x, y, v, v, v)); 
		}
		bool set_pixel(int x, int y, float v) {
				return(set_pixel(x, y, u_char(v), u_char(v), u_char(v))); 
		}
		bool set_pixel(int x, int y, double v) {
				return(set_pixel(x, y, u_char(v), u_char(v), u_char(v))); 
		}
		Y_RGB<u_char> get_pixel(Point2<int> P, int *valid = nullptr) {
			return(get_pixel(P.X(), P.Y(), valid)); 
		}
		Y_RGB<u_char> get_pixel(int pos, int *valid = nullptr) {
			if(!Y_Image::valid(pos)) {
				*valid = 0;
				return(Y_RGB<u_char>::INVALID_RGB);
			}
			return(Y_RGB<u_char>(&im[SPAN*pos])); }
		Y_RGB<u_char> get_pixel(int x, int y, int *valid = nullptr) {
			if(!Y_Image::valid(x,y)) {
				if(valid) *valid = 0;
				return(Y_RGB<u_char>::INVALID_RGB);
			}
			return(Y_RGB<u_char>(&im[SPAN*(y*w+x)])); }
		Y_RGB<double> get_pixel_D2(double x, double y, int *valid = nullptr) {
			return(Bilinear(x, y, valid)); 
		}
		Y_RGB<double> get_pixel_D3(double x, double y, int *valid = nullptr) {
			return(Bicubic(x, y, valid)); 
		}
		Y_RGB<double> get_pixel_D2(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D2(P.X(), P.Y(), valid)); 
		}
		Y_RGB<double> get_pixel_D3(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D3(P.X(), P.Y(), valid)); 
		}
		u_char get_grey_pixel(Point2<int> P, int *valid = nullptr) {
			return(get_grey_pixel(P.X(), P.Y(), valid)); 
		}
		u_char get_grey_pixel(int x, int y, int *valid = nullptr) {
			Y_RGB<u_char> V = get_pixel(x, y, valid);
			return((u_char) (V.Greyscale()+0.5)); 
		}
		double get_grey_pixel_D2(Point2<double> P, int *valid = nullptr) {
			Y_RGB<double> Vd = get_pixel_D2(P, valid);
			return(ToGreyscale(Vd.p[0], Vd.p[1], Vd.p[2])); 
		}
		double get_grey_pixel_D3(Point2<double> P, int *valid = nullptr) {
			Y_RGB<double> Vd = get_pixel_D3(P, valid);
			return(ToGreyscale(Vd.p[0], Vd.p[1], Vd.p[2])); 
		}
		double get_grey_pixel_D2(double x, double y, int *valid = nullptr) {
			Y_RGB<double> Vd = get_pixel_D2(x, y, valid);
			return(ToGreyscale(Vd.p[0], Vd.p[1], Vd.p[2])); 
		}
		double get_grey_pixel_D3(double x, double y, int *valid = nullptr) {
			Y_RGB<double> Vd = get_pixel_D3(x, y, valid);
			return(ToGreyscale(Vd.p[0], Vd.p[1], Vd.p[2])); 
		}

		// operator versions: indices start from 0
		Y_RGB<u_char> operator () (int x, int y) {
			if(!Y_Image::valid(x,y)) return(Y_RGB<u_char>::INVALID_RGB);
			return(Y_RGB<u_char>(&im[SPAN*(y*w+x)]));
		}
		int PutAt(BMPImageC *p, int x, int y);
		BMPImageG *Greyscale(char channel = 'a');
		void MakeGreyscale(char channel = 'a'); // Replace this with its greyscale version. Useful when de-colourising movies
		void SetChannel(BMPImageG* ch, char channel = 'a');
		void SetChannel(DblImageG* ch, int rescale = 0, char channel = 'a');
		int CombineGrayscales(BMPImageG* pR, BMPImageG* pG, BMPImageG* pB);
		double AverageBrightness(Y_RGB<double> clr = Y_RGB<double>::INVALID_RGBD);
		int ShiftAverageTo(u_char level); // Linear shift in brightness to match average and 'level'. Returns number of saturated pixels.
		int Replicate(int factor);
		int Read(const char *file, int suppress_warnings = 0);
		int ReadPPM(const char *file);
		int Save(const char *file);
		int SavePPM(const char *file);
		bool valid(Point2<int>& P) {
			return(valid(P.X(),P.Y()));
		}
		bool valid(int x, int y) {
			// Y_RGB<u_char> checks that pixel is within limits and pixel content
#ifdef _DEBUG
			//int a = Y_Image::valid(x, y);
			bool b = get_pixel(x, y).Valid();
#endif
			return(get_pixel(x, y).Valid()); 
		}
		bool valid(double x, double y);
		int Border(int x, int y);
		int Substitute(Y_RGB<u_char> clr, Y_RGB<u_char> with, Y_Rect<int>* pRect = nullptr); // Change pixels
			// of "clr" with pixels of "with". Returns No of substituted.
		int Validate(Y_RGB<u_char> c_invalid = Y_RGB<u_char>::INVALID_RGB, u_char g_invalid = Y_Utils::_INVALID); 
			// make sure none of pixels give c_invalid or g_invalid
		void SetColour(u_char r, u_char g, u_char b, int override_invalid=1); 
		void SetColour(Y_RGB<u_char> clr, int override_invalid=1); // Set image to homogeneous colour. If 'override_invalid', invalid colour too.
		void SetInvalid();
		BMPImageC** Split(int factor);
		void SetBlack() { memset(im, 0, SPAN*w*h*sizeof(u_char)); }
		void CorrectIllum(BMPImageG *p);
		void CorrectIllum(BMPImageC *p);
		void ValidateByMask(BMPImageG *p);
		BMPImageC *CutSubimage(int at_x, int at_y, int sw, int sh);
		BMPImageC *CutSubimage(Y_Rect<int> r) { return(CutSubimage(r.x, r.y, r.w, r.h)); }
		void Crop(int at_x, int at_y, int sw, int sh);
		void Crop(Y_Rect<int> r) { return(Crop(r.x, r.y, r.w, r.h)); }
		int ZoomToDest_Bilinear(int dest_w, int dest_h, bool show_progress = false);
		int Zoom_Bilinear(double zoom, bool show_progress = false);
		int ZoomToDest_Bicubic(int dest_w, int dest_h, bool show_progress = false);
		int Zoom_Bicubic(double zoom, bool show_progress = false);
		int Translate_Bilinear(double X, double Y, Y_RGB<u_char> bg = Y_RGB<u_char>::INVALID_RGB, bool show_progress = false);
		int Translate_Bicubic(double X, double Y, Y_RGB<u_char> bg = Y_RGB<u_char>::INVALID_RGB, bool show_progress = false);
		int Rotate_Bilinear(double angle_degrees, Y_RGB<u_char> bg = Y_RGB<u_char>::INVALID_RGB, bool show_progress = false);
		int Rotate_Bicubic(double angle_degrees, Y_RGB<u_char> bg = Y_RGB<u_char>::INVALID_RGB, bool show_progress = false);
		int ZoomRotateTranslate_Bilinear(double zoom, double angle_degrees,
			double X, double Y, int flags = DO_ALL);
		int ZoomRotateTranslate_Bicubic(double zoom, double angle_degrees,
			double X, double Y, int flags = DO_ALL);
		void RotateQuarter(int nr_of_quarters); // 1: by pi/2, 2: by pi, 3: by -pi/2
		double DifferenceError(BMPImage *i, int xs, int ys, int *overlap, char *name = nullptr);
		double DifferenceErrorDouble(BMPImage *i, double xs, double ys, 
			int *overlap, const char *name = nullptr);
		// Starting with (x,y) colour all pixels with 'fill' until 'bound'
		int SeedFill(int seedx, int seedy, Y_RGB<u_char> bound, Y_RGB<u_char> fill, int bufferLength = 16384, Y_Rect<int>* pR = nullptr);
		// Starting with (x,y) colour all pixels with 'fill' until any other
		int SeedFill(int seedx, int seedy, Y_RGB<u_char> fill, int bufferLength = 16384, Y_Rect<int>* pR = nullptr);
		// Starting with (x,y) colour all pixels with 'fill' until 'bound' returning vector of locations
		int SeedFill(int seedx, int seedy, Y_RGB<u_char> bound, Y_RGB<u_char> fill, vector<Point2<int> >& locations, int bufferLength = 16384, Y_Rect<int>* pR = nullptr);
		// April 2006: Detrending is moved to class Detrend
		//void Detrend(int detrend_order, double shift = 0.0, int verbose = 0);
		Y_RGB<double> Bilinear(double dx, double dy, int *valid = nullptr);
		Y_RGB<double> Bilinear(double dx, double dy, int tl_i, int tl_j);
		Y_RGB<double> Bicubic(double dx, double dy, int *valid = nullptr);
		void MarkPoint(double x, double y, int rad, Y_RGB<u_char> Pixel);
		// Based on given principal point and coefficients
		//BMPImageC *Undistort_A_Bilinear(Y_Camera *C);
		//BMPImageC *Distort_A_Bilinear(Y_Camera *C);
		//BMPImageC *Undistort_A_Bicubic(Y_Camera *C);
		//BMPImageC *Distort_A_Bicubic(Y_Camera *C);
		void Recode2YU12(u_char *y, u_char *u, u_char *v);
	public:
		//u_char *im_r, *im_g, *im_b;
		u_char *im;
};

#endif
