#pragma once

// Replacement for class Collocation that is created with TransformRecord/RARecord
// Class is needed only for choosing collocations and setting them in TransformRecord/RARecord,
// or defining default collocations based on provided Hmgr (mainly for Fourier-based registration).

// Each collocation set defines 'n' pairs of points related to one relative transform.
// Points 'b' are points in image 'base',
// (could be chosen specifically (w/4,h/4) and (3*w/4,3*h/4)).
// Points 'd' are their projections onto image 'derived'.

#include "Coord2.h"
#include "Hmgr.h"
//#include "Transform.h"

//class TransformRecord;
//class RARecord;

class Collocations {
public:
	Collocations(int _w, int _h, double _fl);
	Collocations(int _n, int _w, int _h, double _fl);
	Collocations(int _n, Hmgr& _hm, int _w, int _h, double _fl);
	Collocations(const Collocations& c);
	~Collocations();
	void SetSizeFocalLength(int _w, int _h, double _fl);
	//void SetToRAR(RARecord& rar); // Copy collocations from this to given RARecord
	//void SetToTransform(TransformRecord& trans); // Copy collocations from this to given TransformRecord
	void Print(FILE* fp);
public:
	int w, h;
	double fl;
	Hmgr hm;

	int n; // Number of collocations
	Coord2* b; // points on base
	Coord2* d; // points on derived
};

