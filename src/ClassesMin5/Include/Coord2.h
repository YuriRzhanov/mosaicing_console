// Image-related stuff
#ifndef _COORD2_H_DEFINED_
#define _COORD2_H_DEFINED_

class FltImageG;

#include "Point2.h"
#include "PixelC.h"
#include "BMPImageG.h"
//#include "FltImageG.h"
#include "DblImageG.h"
#include "BMPImageC.h"
#include "DblImageC.h"
#include "IntImageG.h"

class Coord2 : public Point2<double> {
	public:
		Coord2() : Point2<double>() {w=0; h=0; focal_length=-1.0;}
		Coord2(Point2<double> p) : Point2<double>(p.X(),p.Y()) {
			w=0; h=0; focal_length=-1.0;}
		Coord2(double _x, double _y) : Point2<double>(_x,_y) {w=0; h=0; focal_length = -1.0;}
		void Copy(const Coord2& c);
		void SetSize(int _w, int _h) { w=_w; h=_h; }
		void SetFocalLength(double fl) { focal_length = fl; }
			// All are set in one call
		void SetImage(Y_Image *im) { w=im->w; h=im->h; focal_length=im->focal_length; }
		void SetSizeFocalLength(int _w, int _h, double fl) { w=_w; h=_h; focal_length=fl; }
		void SetSizeFocalLength(Y_Image *im, double fl) { w=im->w; h=im->h; focal_length=fl; }
		// Set normalized coordinates (by focal length)
		void SetN(double xn, double yn) { x = xn*focal_length+0.5*w; y = yn*focal_length+0.5*h; }
		// Normalized coords (by focal length)
		double Xn() { return((x-0.5*w)/focal_length); }
		double Yn() { return((y-0.5*h)/focal_length); }
		Point2<int> ToIntPoint() { return(Point2<int>(int(x+0.5), int(y+0.5))); }
		double Value2(BMPImageG *im, int *vld = nullptr);
		//double Value2(IntImageG *im, int *vld = nullptr);
		Y_RGB<double> Value2(BMPImageC *im, int *vld = nullptr);
		//float Value2(FltImageG *im, int *vld = nullptr);
		double Value2(DblImageG *im, int *vld = nullptr);
		Y_RGB<double> Value2(DblImageC *im, int *vld = nullptr);
		//float Value3(FltImageG *im, int *vld = nullptr);
		double Value3(BMPImageG *im, int *vld = nullptr);
		Y_RGB<double> Value3(BMPImageC *im, int *vld = nullptr);
		double Value3(DblImageG *im, int *vld = nullptr);
		Y_RGB<double> Value3(DblImageC *im, int *vld = nullptr);
		bool Valid() { return(isnan(x)==false && isnan(y)==false); }
		int Outside(Y_Rect<int> R) { // Check, if inside the rectangle
			if(x<R.x || x>(R.x+R.w) || y<R.y || y>(R.y+R.h)) return(1);
			return(0);
			}
		double FromCenter(Y_Rect<int> R) { // Distance from center of given rectangle
			double dx = x-(R.x+0.5*R.w);
			double dy = y-(R.y+0.5*R.h);
			return(sqrt(dx*dx+dy*dy));
			}
		void Grad_Bilinear(BMPImageG *im, double& dx, double& dy, double step, int *vld = nullptr);
		void Grad_Bilinear(DblImageG *im, double& dx, double& dy, double step, int *vld = nullptr);
		void Grad_Bicubic(BMPImageG *im, double& dx, double& dy, double step, int *vld = nullptr);
		void Grad_Bicubic(DblImageG *im, double& dx, double& dy, double step, int *vld = nullptr);
		void GaussGrad(BMPImageG *im, double& dx, double& dy, int *vld);
		double Convolve(int I, int J, BMPImageG *im, int flip);
		static void SetGradKernel(int sz);
		Coord2 Map(int n, double *t); // Apply transformation to *this - general solution
	public:
		int w, h; // Size of associated image
		double focal_length;
		static Coord2 mapped;
		static DblImageG *kernel;
};

#endif
