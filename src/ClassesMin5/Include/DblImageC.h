#ifndef _DBLIMAGEC_H
#define _DBLIMAGEC_H

// Storage organization: (RGB) (RGB) ...
class BMPImageC;
class DblImageC;

#include "PixelC.h"
#include "DblImage.h"
#include "BMPImageC.h"


class DblImageC : public DblImage {
	public:
		DblImageC(int _w, int _h);
		DblImageC(int _w, int _h, double *_im);
		DblImageC(int _w, int _h, double *r, double *g, double *b);
		DblImageC(DblImageC *d);
		DblImageC(BMPImageC *img, int hasInvalid = 0);
			// if 'hasInvalid' is set to 1, BMPImageC has invalid 
			// pixels marked with 0's
		DblImageC(const char *name, bool old = false);
		~DblImageC() { 
			if(d_im) {
				delete [] d_im;
				d_im = nullptr;
			}
			fail = 1;
		}
		void SetRanges(double _min, double _max) { v_min=_min; v_max=_max;
			ranges_set=1; }
		DblImageC *Copy();
		void Copy(DblImageC *img);
		int Read(const char *name, bool old = false); // Triple-wise reading
		int ReadOld(const char *name); // allR-allG-allB reading (prior to Feb 2005)
		int Save(const char *name, bool old = false);
		// Reset to zeroes
		void SetBlack() { memset(d_im, 0, 3*w*h*sizeof(double)); }
		bool set_pixel(int x, int y, double r, double g, double b) {
			if(!Y_Image::valid(x,y)) return(false);
			d_im[3*(y*w+x)+0] = r;
			d_im[3*(y*w+x)+1] = g;
			d_im[3*(y*w+x)+2] = b;
			return(true);
		}
		bool set_pixel(Point2<int> P, double r, double g, double b) {
			return(set_pixel(P.X(), P.Y(), r, g, b)); }
		bool set_pixel(int x, int y, Y_RGB<double> p) {
			if(!Y_Image::valid(x,y)) return(false);
			memcpy(&d_im[3*(y*w+x)], p.p, 3*sizeof(double)); return(true);
		}
		bool set_pixel(Point2<int> P, Y_RGB<double> p) {
			return(set_pixel(P.X(), P.Y(), p)); }
		Y_RGB<double> get_pixel(Point2<int> P) {
			return(get_pixel(P.X(), P.Y())); }
		Y_RGB<double> get_pixel(int x, int y) {
			if(!Y_Image::valid(x,y)) return(Y_RGB<double>::INVALID_RGBD);
			return(Y_RGB<double>(&d_im[3*(y*w+x)]));
		}
		Y_RGB<double> get_pixel_D2(Point2<double> P, int *valid = nullptr) {
			return(get_pixel_D2(P.X(), P.Y(), valid)); }
		Y_RGB<double> get_pixel_D2(double x, double y, int *valid = nullptr) {
			return(Bilinear(x, y, valid)); }
		Y_RGB<double> get_pixel_D3(double x, double y, int *valid = nullptr) {
			return(Bicubic(x, y, valid)); }
		// operator versions: indices start from 0
		Y_RGB<double> operator () (int x, int y) {
			if(!Y_Image::valid(x,y)) return(Y_RGB<double>::INVALID_RGBD);
			return(Y_RGB<double>(&d_im[3*(y*w+x)]));
		}
		int PutAt(DblImageC *p, int x, int y);
		int border(int x, int y);
		int valid(int x, int y);
		int valid(double x, double y);
		void SetInvalid();
		int SaveBMP(const char *name, int rescale=0);
		int SaveBMP(const char *name, double min, double max);
		DblImageC *CutSubimage(int at_x, int at_y, int sw, int sh);
		DblImageC *CutSubimage(Y_Rect<int> r);
		Y_RGB<double> Bilinear(double dx, double dy, int *valid = nullptr);
		// Enforce interpolation assuming that top-left corner is given
		Y_RGB<double> Bilinear(double dx, double dy, int tl_i, int tl_j);
		Y_RGB<double> Bicubic(double dx, double dy, int *valid = nullptr);
		void Log(); // Zeroes become INVALID
		void ScaleShift(double gain, double offset);
		DblImageC** Split(int factor);
		bool Subtract(DblImageC* pDbl);
		bool Add(DblImageC* pDbl);
		bool Multiply(DblImageC* pDbl);
		bool Divide(DblImageC* pDbl);
		DblImageG *Greyscale(char channel = 'a');
		int CombineGrayscales(DblImageG* pR, DblImageG* pG, DblImageG* pB);
		BMPImageC *Colour(int rescale=0);
		BMPImageC *Colour(double min, double max);
		int ZoomToDest_Bilinear(int dest_w, int dest_h);
		int Zoom_Bilinear(double zoom, int flags = DO_ALL);
		int ZoomToDest_Bicubic(int dest_w, int dest_h);
		int Zoom_Bicubic(double zoom, int flags = DO_ALL);
		int Rotate_Bilinear(double angle_degrees, int flags = DO_ALL);
		int Rotate_Bicubic(double angle_degrees, int flags = DO_ALL);
		void RotateQuarter(int nr_of_quarters); // 1: by pi/2, 2: by pi, 3: by -pi/2
		//double difference_error_double(DblImageG *i, double xs, double ys, 
		//	int *overlap, char *name = nullptr);
		void Blur(int kernel, int times = 1); // 'times' - number of times
		void MinMax(double& min, double& max);
	public:
		double *d_im;
};


#endif
