#ifndef _FLTIMAGEG_H
#define _FTLIMAGEG_H

class BMPImageG;
class BMPImageC;
class IntImageG;
class FltImageG;

#include "Coord2.h"
#include "FltImage.h"

class FltImageG : public FltImage {
	public:
		FltImageG(int _w, int _h, float *data = nullptr);
		FltImageG(int _w, int _h, float init);
		FltImageG(FltImageG *f);
		FltImageG(BMPImageG *img, int hasInvalid = 0);
			// if 'hasInvalid' is set to 1, BMPImageG has invalid 
			// pixels marked with 0's
		FltImageG(const char *name, bool old = false);
		~FltImageG() { 
			if(f_im) {
				delete [] f_im;
				f_im = nullptr;
			}
			fail = 1;
		}
		void SetRanges(float _min, float _max) { v_min=_min; v_max=_max; ranges_set=1; }
		FltImageG *Copy();
		void Copy(FltImageG *img);
		int Read(const char *name, bool old = false);
		int Save(const char *name, bool old = false);
		// Reset to zeroes
		void SetBlack() { memset(f_im, 0, w*h*sizeof(float)); }
		void SetColour(float v);
		bool Normalize(float sum = 1.0f); // Make sum of all pixels equal to 'sum'
		bool set_pixel(Point2<int> P, float p) {
			return(set_pixel(P.X(), P.Y(), p)); }
		bool set_pixel(int x, int y, float p) {
			if(!Y_Image::valid(x,y)) return(false);
			f_im[y*w+x] = p; return(true); }
		float get_pixel(Point2<int> P) {
			return(get_pixel(P.X(), P.Y())); }
		float get_pixel(int x, int y) {
			if(!Y_Image::valid(x,y)) return(Y_Utils::_INVALID_FLOAT);
			return(f_im[y*w+x]); }
		float get_pixel(Point2<float> P, int *valid = nullptr) {
			return(get_pixel(P.X(), P.Y(), valid)); }
		float get_pixel(float x, float y, int *valid = nullptr) {
			return(Bilinear(x, y, valid)); }
		// operator versions: indices start from 0
		float& operator () (int x, int y) {
			if(!Y_Image::valid(x,y)) return(Y_Utils::_INVALID_FLOAT);
			return(f_im[y*w+x]);
		}
		int PutAt(FltImageG *p, int x, int y);
		int border(int x, int y);
		int valid(int x, int y) { return(Y_Image::valid(x, y) && !isnan(f_im[y*w+x])); }
		int valid(float x, float y);
		int valid(Point2<float> P) { return(valid(P.x, P.y)); }
		void SetInvalid() { for(int j = 0; j<w*h; j++) f_im[j] = Y_Utils::_INVALID_FLOAT; }
		FltImageG** Split(int factor);
		int SaveBMP(const char *name, int rescale=0, float ignore_value=1.e9);
		int SaveBMP(const char *name, float min, float max);
		int SaveBMPRainbow(const char *name, int type = 0); 
		FltImageG *CutSubimage(int at_x, int at_y, int sw, int sh);
		FltImageG *CutSubimage(Y_Rect<int> r) { return(CutSubimage(r.x, r.y, r.w, r.h)); }
		void Crop(int at_x, int at_y, int sw, int sh);
		void Crop(Y_Rect<int> r) { return(Crop(r.x, r.y, r.w, r.h)); }
		float Bilinear(float dx, float dy, int *valid = nullptr);
		// Enforce interpolation assuming that top-left corner is given
		float Bilinear(float dx, float dy, int tl_i, int tl_j);
		float Bicubic(float dx, float dy, int *valid = nullptr);
		BMPImageG *Greyscale(int rescale=0, float ignore_value=1.e9);
		BMPImageG *Greyscale(float min, float max);
		void ValidateAll();
		int ZoomToDest_Bilinear(int dest_w, int dest_h);
		int Zoom_Bilinear(float zoom, int flags = DO_ALL);
		int ZoomToDest_Bicubic(int dest_w, int dest_h);
		int Zoom_Bicubic(float zoom, int flags = DO_ALL);
		int Rotate_Bilinear(float angle_degrees, int flags = DO_ALL);
		int Rotate_Bicubic(float angle_degrees, int flags = DO_ALL);
		void RotateQuarter(int nr_of_quarters); // 1: by pi/2, 2: by pi, 3: by -pi/2
		float difference_error_float(FltImageG *i, float xs, float ys, 
			int *overlap, char *name = nullptr);
		void Blur(int kernel, int times = 1); // 'times' - number of times
		// Finds min and max, if value is ignore_value, it is ignored
		void MinMax(float& v_min, float& v_max, float ignore_value=1.e9);
		// ... also returns location of extremums
		void MinMax(float& v_min, Point2<int>& p_min, float& v_max, Point2<int>& p_max, float ignore_value=1.e9);
		void Scale(float min, float max); 
		void CutOffPercentile(float& min, float& max, float percentile);
		// April 2006: Detrending is moved to class Detrend
		//void DetrendLaurie(int detrend_order, int verbose = 0);
		// Jan 2006: instead of Detrend(), use GetTrend()/ApplyTrend() sequence
		// Order=0 -> Dim=1; Order=1 -> Dim=3; Order=2 -> Dim=6
		//NEWMAT::ColumnVector GetTrend(int detrend_order);
		//void ApplyTrend(int detrend_order, NEWMAT::ColumnVector& r);
		//void ApplyTrend(NEWMAT::ColumnVector& r); // Dimension of 'r' determins detrend_order 
		static void SetMutualRange(FltImageG *d1, FltImageG *d2, float percentile = 100.0);
		static FltImageG* GaussianKernel(int sz);
		static FltImageG* GaussianKernel(float sigma);
		float Convolve(int I, int J, FltImageG *kernel, int *n = nullptr);
		float Convolve(float X, float Y, FltImageG *kernel, int *n = nullptr);
		FltImageG* Convolve(FltImageG *kernel);
		bool LocalMaximum_4(int i, int j);
		bool LocalMaximum_8(int i, int j);
		FltImageG* operator *=(float s);
		//FltImageG* operator /=(FltImageG* dbl, float s);
		//FltImageG* operator +(FltImageG* d, FltImageG* dbl);
	public:
		float *f_im;
		static float INVALID;
};

#endif
