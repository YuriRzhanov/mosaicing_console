#ifndef _IMAGESEQUENCE_INCLUDED_
#define _IMAGESEQUENCE_INCLUDED_

#include <stdio.h>
#include <string.h>
#include <string>

class ImageRecord {
public:
	ImageRecord() 
		: k(-1),full_path(nullptr),x(0.0),y(0.0),alt(0.0),depth(0.0),pitch(0.0),roll(0.0),yaw(0.0) {
#ifdef _MSC_VER
		strcpy_s(timestamp, 12, "00:00:00:00");
#else
		strcpy(timestamp, "00:00:00:00");
#endif
	}
	ImageRecord(int _k, const char* _full_path) 
		: k(_k),x(0.0),y(0.0),alt(0.0),depth(0.0),pitch(0.0),roll(0.0),yaw(0.0) {
		full_path = _full_path;
#ifdef _MSC_VER
		strcpy_s(timestamp, 12, "00:00:00:00");
#else
		strcpy(timestamp, "00:00:00:00");
#endif
	}
	~ImageRecord() {}
	void Copy(ImageRecord& rec);
	void SetTimestamp(char* _ts) {
		memcpy(timestamp, _ts, 12);
		timestamp[11] = 0;
	}
	void SetNav(double _x, double _y, double _alt, double _depth) {
		x = _x; y = _y; alt = _alt; depth = _depth;
	}
	void SetAtt(double _pitch, double _roll, double _yaw) {
		pitch = _pitch; roll = _roll; yaw = _yaw;
	}
	void SetFullPath(std::string _full_path) {
		full_path = _full_path;
	}
	void SetFullPathFromBasename(const char* basename) {
#ifdef _MSC_VER
		sprintf_s(stg, (size_t) 1024, "%s%06d.bmp", basename, k);
#else
		sprintf(stg, "%s%06d.bmp", basename, k);
#endif
		full_path = stg;
	}
	void SetFromString(const char* _stg);
	int Read(FILE* fp);
	char* SaveStg();
	int Save(FILE* fp);
	// Assuming that file name is "path/prefixDDDDDD.ext", insert a modifier after prefix
	void InsertModifier(char* modifier);
	void InsertModifierBeforeDot(char* modifier);
	// Assuming filename standard, set number to the given
	void SetNumber(int num);
public:
	int k;
	char timestamp[12];
	std::string full_path; // Consists of path/prefixDDDDDD.ext
	double x, y, alt, depth, pitch, roll, yaw;
	static char stg[1024];
	static char buf[1024];
};

class ImageSequence {
public:
	ImageSequence() : n(0), fail(false), w(0), h(0), fl(-1.0), FoV_h(0.0) { }
	ImageSequence(const char* filename);
	ImageSequence(int _n) : n(_n), fail(false), w(0), h(0), fl(-1.0), FoV_h(0.0) {
		records = new ImageRecord[n];
	}
	~ImageSequence() {
		delete [] records;
	}
	int ReadRecords(const char* filename);
	ImageRecord* Record(int k) {
		if(k>=0 && k<n) return(&records[k]);
		return(nullptr);
	}
	const char* Path(int k) {
		if(k>=0 && k<n) return(records[k].full_path.c_str());
		return(nullptr);
	}
	bool SetMetadata(const char* filename);
	bool SetMetadataFromString(const char* stg);
	// Assuming that file name is "path/prefixDDDDDD.ext", insert a modifier after prefix
	static void InsertModifier(char* filename, char* modifier);
	// Assuming filename standard, set number to the given
	static void SetNumber(char* filename, int num);
	int Save(const char* filename);
	void CleanSequence();
	char* Info(const char* name); // Movie-style information about the sequence
	char* Error() { return(buf); }
public:
	int n;
	bool fail;
	ImageRecord* records;
	int w, h;
	double fl, FoV_h;
	static char buf[1024];
};


#endif


