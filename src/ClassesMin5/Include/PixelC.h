#ifndef _PIXEL_C_H
#define _PIXEL_C_H

#include <string.h>
#include "YUtils.h"

// Attempt to develop unified approach to RGB-triple
template <class T>
class Y_RGB {
	public:
		Y_RGB() { p[0]=p[1]=p[2]=0; }
		Y_RGB(T _r, T _g, T _b) { p[0]=_r; p[1]=_g; p[2]=_b; }
		Y_RGB(T v) { p[0]=p[1]=p[2]=v; }
		Y_RGB(T* v) { memcpy(p, v, 3*sizeof(T)); }
		void Set(T _r, T _g, T _b) { p[0]=_r; p[1]=_g; p[2]=_b; }
		void Set(T* v) { memcpy(p, v, 3*sizeof(T)); }
		void Set(int chan, T val) { p[chan] = val; }
		void Set(char chan, T val) { 
			switch(chan) {
				case 'a': p[0] = p[1] = p[2] = val; break;
				case 'r': p[0] = val; break;
				case 'g': p[1] = val; break;
				case 'b': p[2] = val; break;
			}
		}
		T R() { return(p[0]); }
		T G() { return(p[1]); }
		T B() { return(p[2]); }
		bool Valid() { // Every one must be not invalid
			if(sizeof(T)==sizeof(u_char))
				return(p[0]!=Y_Utils::_INVALID_UCHAR || p[1]!=Y_Utils::_INVALID_UCHAR || p[2]!=Y_Utils::_INVALID_UCHAR); 
			else if(sizeof(T)==sizeof(double))
				return(!isnan(p[0]) && !isnan(p[1]) && !isnan(p[2]));
			else if(sizeof(T)==sizeof(u_short))
				return(p[0]!=Y_Utils::_INVALID || p[1]!=Y_Utils::_INVALID || p[2]!=Y_Utils::_INVALID); 
			else
				return(false); // Shouldn't happen...
		}
		void Zero() { p[0]=p[1]=p[2]=0; }
		bool IsZero() { return(p[0]==0 && p[1]==0 && p[2]==0); }
		T Greyscale() { return((T) ToGreyscale(p[0],p[1],p[2])); }
		void Copy(const Y_RGB& v) { p[0]=v.p[0]; p[1]=v.p[1]; p[2]=v.p[2]; }
		void Copy(const Y_RGB *v) { p[0]=v->p[0]; p[1]=v->p[1]; p[2]=v->p[2]; }
		Y_RGB Log() {
			T tmp[3];
			if((double)p[0]<=0.0) tmp[0] = 0.0; else tmp[0] = log((double)p[0]);
			if((double)p[1]<=0.0) tmp[1] = 0.0; else tmp[1] = log((double)p[1]);
			if((double)p[2]<=0.0) tmp[2] = 0.0; else tmp[2] = log((double)p[2]);
			p[0] = tmp[0]; p[1] = tmp[1]; p[2] = tmp[2];
			return(*this);
		}
		void AddNoise(double noisePercent);
		Y_RGB &operator =(const Y_RGB& v) { p[0]=v.p[0]; p[1]=v.p[1]; p[2]=v.p[2]; return(*this); }
		int operator ==(const Y_RGB& c) { return equal(c); }
		int operator !=(const Y_RGB& v) { return !equal(v); }
		int equal(const Y_RGB& c) { return(p[0]==c.p[0] && p[1]==c.p[1] && p[2]==c.p[2]); }
		int ToInt() { return((int) (abs(p[0])+abs(p[1])+abs(p[2]))); }
		double ToDbl() { return(fabs(p[0])+fabs(p[1])+fabs(p[2])); }
		void Print(void) { cout<<"["<<p[0]<<","<<p[1]<<","<<p[2]<<"]\n"; }
		void Print(char *tag) { cout<<tag<<": ["<<p[0]<<","<<p[1]<<","<<p[2]<<"]\n"; }
		T Diff(Y_RGB& v) {
			if(typeid(T)==typeid(u_char))
				return(abs((int)p[0]-v.p[0])+abs((int)p[1]-v.p[1])+abs((int)p[2]-v.p[2]));
			else if(typeid(T)==typeid(int))
				return(abs(p[0]-v.p[0])+abs(p[1]-v.p[1])+abs(p[2]-v.p[2]));
			else if(typeid(T)==typeid(float))
				return(fabs((float)p[0]-v.p[0])+fabs((float)p[1]-v.p[1])+fabs((float)p[2]-v.p[2]));
			else if(typeid(T)==typeid(double))
				return(fabs((double)p[0]-v.p[0])+fabs((double)p[1]-v.p[1])+fabs((double)p[2]-v.p[2]));
			else
				return((T)0);
		}
		friend ostream &operator<<(ostream &ostr, Y_RGB &v)
			{ ostr<<"["<<v.p[0]<<","<<v.p[1]<<","<<v.p[2]<<"]"; return(ostr); }
		friend istream &operator>>(istream &istr, Y_RGB &v)
			{ istr>>v.p[0]>>v.p[1]>>v.p[2]; return(istr); }
		Y_RGB operator +(const Y_RGB& v) {
				double s0 = v.p[0]+p[0]; //if(s0>255.0) s0 = 255.0;
				double s1 = v.p[1]+p[1]; //if(s1>255.0) s1 = 255.0;
				double s2 = v.p[2]+p[2]; //if(s2>255.0) s2 = 255.0;
				return(Y_RGB(T(s0), T(s1), T(s2))); 
			}
		Y_RGB operator -(const Y_RGB& v) {
				double s0 = v.p[0]-p[0]; //if(s0<0.0) s0 = 0.0;
				double s1 = v.p[1]-p[1]; //if(s1<0.0) s1 = 0.0;
				double s2 = v.p[2]-p[2]; //if(s2<0.0) s2 = 0.0;
				return(Y_RGB(T(s0), T(s1), T(s2))); 
			}
		Y_RGB operator /(const double s)
			{ return(Y_RGB(T(p[0]/s), T(p[1]/s), T(p[2]/s))); }
		Y_RGB operator *(const double s) {
				double s0 = s*p[0]; //if(s0>255.0) s0 = 255.0;
				double s1 = s*p[1]; //if(s1>255.0) s1 = 255.0;
				double s2 = s*p[2]; //if(s2>255.0) s2 = 255.0;	
				return(Y_RGB(T(s0), T(s1), T(s2))); 
			}
		Y_RGB operator *=(const double s) {
				double s0 = s*p[0]; //if(s0>255.0) s0 = 255.0; 
				p[0] = T(s0);
				double s1 = s*p[1]; //if(s1>255.0) s1 = 255.0; 
				p[1] = T(s1);
				double s2 = s*p[2]; //if(s2>255.0) s2 = 255.0; 
				p[2] = T(s2);
				return(*this); 
			}
		Y_RGB operator /=(const double s)
			{ p[0]/=s; p[1]/=s; p[2]/=s; return(*this); }
		Y_RGB operator +=(const Y_RGB& v)
			{ p[0]+=v.p[0]; p[1]+=v.p[1]; p[2]+=v.p[2]; return(*this); }
		Y_RGB operator -=(const Y_RGB& v)
			{ p[0]-=v.p[0]; p[1]-=v.p[1]; p[2]-=v.p[2]; return(*this); }
		int Colour2Number() // Range 0:25500 only
			{ return(100*p[0]+10*p[1]+p[2]); }
		int Colour2Number_2() // Range 0:255255255, but remember about 2-digits for G and B!
			{ return(10000*p[0]+100*p[1]+p[2]); }
		void Number2Colour(int n) // Range 0:25500 only
			{ p[0] = n/100; n -=p[0]*100; p[1] = n/10; n -= p[1]*10; p[2] = n; }
		void Number2Colour_2(int n) // Range 0:255255255, but remember about 2-digits for G and B!
			{ p[0] = n/10000; n -=p[0]*10000; p[1] = n/100; n -= p[1]*100; p[2] = n; }

	public:
		T p[3];
		static Y_RGB<u_char> INVALID_RGB;
		static Y_RGB<double> INVALID_RGBD;
		static Y_RGB<u_char> WhitePixel;
		static Y_RGB<u_char> BlackPixel;
		static Y_RGB<u_char> Pixel254;
		static Y_RGB<u_char> RedPixel;
		static Y_RGB<u_char> GreenPixel;
		static Y_RGB<u_char> BluePixel;
		static Y_RGB<u_char> YellowPixel;
		static Y_RGB<u_char> CyanPixel;
		static Y_RGB<u_char> MagentaPixel;
};


#endif
