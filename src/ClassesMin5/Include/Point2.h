// Class to be used instead of: XPoint, Vertex2, Vertex3, Coord
// No relation to any image here

// Doesn't depend on Matrix2 either - instead of apply_transform(),
// Matrix2.apply_to_vector() should be used.

#pragma once

#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <assert.h>
#include "defs.h"

#ifndef _MSC_VER
#include <typeinfo>
#endif

#define WANT_STREAM                  // include.h will get stream fns
#define WANT_MATH                    // include.h will get math fns
                                     // newmatap.h will get include.h
#include "NEWMATAP.H"                // need matrix applications
//#include "NEWMATIO.H"                // need matrix output routines
//using namespace NEWMAT;              // access NEWMAT namespace

using namespace std;

template <class T>
class Point2 {
	public:
		Point2() {x=T(0); y=T(0); w=T(1);}
		Point2(T _x, T _y) {x=(T)_x; y=(T)_y; w=(T)1;}
		Point2(const Point2& p) {x=p.x; y=p.y; w=p.w;}
		void Set(const Point2& p) {x=p.x; y=p.y; w=p.w;}
		void Set(T _x, T _y) {x=_x; y=_y; w=T(1);}
		void Set(T _x, T _y, T _w) {x=_x; y=_y; w=_w;}
		T X() const {return(x);}
		T Y() const {return(y);}
		T W() const {return(w);}
		void Copy(const Point2& p) {x=p.x; y=p.y; w=p.w;}
		Point2 Copy() {return(Point2(*this));}
		Point2<double> Double() { return(Point2<double>((double)x,(double)y,double(w))); }
		Point2<float> Float() { return(Point2<float>((float)x,(float)y,float(w))); }
		bool Equal(const Point2& p) {
			if(typeid(T)==typeid(int))
				return(x==p.x && y==p.y);
			return(EPSILON_EQ((double)x,p.x) && EPSILON_EQ((double)y,p.y));}
		//void Negate(const Point2& p) {x=-p.x; y=-p.y;}
		void Negate() {x=-x; y=-y;}
		T Length() const {return(T(sqrt(x*x+y*y)));}
		bool IsZero() const;
		bool IsUnit() const;
		static T DotProduct(Point2& v1, Point2& v2) {return(v1.x*v2.x+v1.y*v2.y);}
		int IsParallel(const Point2& v) const;
		void Normalize() { T d = Length(); x/=d; y/=d; }
		//void Scale(T s, const Point2& p) {x=s*p.x; y=s*p.y;}
		void Scale(T s) {x=s*x; y=s*y;}
		T Distance(const Point2& p) 
			{ return(T(sqrt((double)(x-p.x)*(x-p.x)+(y-p.y)*(y-p.y))));}
		double DDistance(const Point2& p) 
			{ return(sqrt(double(x-p.x)*(x-p.x)+(y-p.y)*(y-p.y)));}
		void Rotate(double A); // About origin by angle A (radians)
		void Rotate(NEWMAT::Matrix& M);
		//void apply_transform(NEWMAT::Matrix2 *m); // Same as Matrix2.apply_to_vector()
		friend ostream &operator<<(ostream &ostr, Point2 &p)
			{ ostr << "(" << p.x <<"," << p.y << ")"; return(ostr); }
		// Operators
		bool operator ==(const Point2& p) {return Equal(p);}
		bool operator !=(const Point2& p) {return !Equal(p);}
		Point2 operator -() const {return(Point2(-x, -y));}
		Point2 operator +(const Point2& p) const
			{ return Point2(x+p.x, y+p.y); }
		Point2 operator -(const Point2& p) const
			{ return Point2(x-p.x, y-p.y); }
		Point2 operator *(T s)
			{ return Point2(s*x, s*y); }
		Point2 operator /(T s)
			{ return Point2(x/s, y/s); }
		// The following does not work on Linux:
#ifdef _MSC_VER
		friend inline Point2<T> operator *(T s, Point2<T> const & p);
		friend inline Point2<T> operator *(Point2<T> const & p, T s);
		friend inline Point2<T> operator /(Point2<T> const & p, T s);
#endif
		// Assignment operators
		Point2& operator =(const Point2& p)
			{ x=(T)p.x; y=(T)p.y; return *this; }
		Point2& operator *=(T s)
			{ x*=s; y*=s; return *this; }
		Point2& operator /=(T s)
			{ s=(T)1.0/s; return *this*=s; }
		Point2& operator +=(const Point2& p)
			{ x+=p.x; y+=p.y; return *this; }
		Point2& operator -=(const Point2& p)
			{ x-=p.x; y-=p.y; return *this; }
		static Point2 Direction(const Point2& pc, const Point2& p);
		static Point2 Interpolate(const Point2& pc, const Point2& p, double t);
	public:
		T x, y, w;
};

template <class T>
inline Point2<T> operator *(T s, Point2<T> const & p)
	{ return Point2<T>(p.x*s, p.y*s); }
template <class T>
inline Point2<T> operator *(Point2<T> const & p, T s)
	{ return Point2<T>(p.x*s, p.y*s); }
template <class T>
inline Point2<T> operator /(Point2<T> const & p, T s)
	{ return Point2<T>(p.x/s, p.y/s); }

template <class T>
class Segment2 {
	public:
		Segment2() {
			begin.Set((T)0,(T)0);
			end.Set((T)0,(T)0);
			length = (T)0;
			value = (T)0;
		}
		Segment2(Point2<T> _begin, Point2<T> _end) {
			begin = _begin; end = _end;
			//length = (T)sqrt((double)(begin.X()-end.X())*(begin.X()-end.X())+
			//			(begin.Y()-end.Y())*(begin.Y()-end.Y()));
			value = (T)0;
		}
		// Distance from point P to this segment
		T ShortestDistance(Point2<T> P) {
			T dotprod = (P.x-begin.x)*(end.x-begin.x)+(P.y-begin.y)*(end.y-begin.y);
			T len2 = (end.x-begin.x)*(end.x-begin.x)+(end.y-begin.y)*(end.y-begin.y);
			T dx = (begin.x-P.x)+(end.x-begin.x)*dotprod/len2;
			T dy = (begin.y-P.y)+(end.y-begin.y)*dotprod/len2;
			return((T) sqrt((double)dx*dx+dy*dy));
		}
		T Length() {
			length = (T)sqrt((double)(begin.X()-end.X())*(begin.X()-end.X())+
						(begin.Y()-end.Y())*(begin.Y()-end.Y()));
			return(length);
		}
		// If 'this' intersects with 'S', returns 'true', otherwise 'false'
		// 'X' is a cross-section, possibly outside the range of segments
		// 'pS' is returned by request. It's -ve if intersection is prior to beginning of 'this',
		// +ve if after the end of 'this'.
		bool Intersect(Segment2<T> S, Point2<T>& X, T* pS = nullptr) {
			Point2<T> L = end-begin;
			// Equation for a ray: r=begin+s*L
			Point2<T> l = L; l.Normalize();
			// Normal to 'this':
			Point2<T> normal(l.y, -l.x);

			Point2<T> SL = S.end-S.begin;
			// Equation for a S-ray: r=S.begin+sS*(S.end-S.begin)
			Point2<T> Sl = SL; Sl.Normalize();
			// Normal to 'S':
			Point2<T> Snormal(Sl.y, -Sl.x);

			Point2<T> v1 = begin-S.begin;
			//T sS = v1.Scalarproduct(normal)/SL.Scalarproduct(normal);
			T sS = Point2<double>::DotProduct(v1, normal)/Point2<double>::DotProduct(SL, normal);
			X = S.begin+SL*sS;

			Point2<T> v2 = S.begin-begin;
			//T s = v2.Scalarproduct(Snormal)/L.Scalarproduct(Snormal);
			T s = Point2<double>::DotProduct(v2, Snormal)/Point2<double>::DotProduct(L, Snormal);
			X = begin+L*s;

			if(pS)
				*pS = s;

			if(s>=(T)0 && s<=(T)1 && sS>=(T)0 && sS<=(T)1)
				return(true);
			return(false);
		}
	public:
		Point2<T> begin, end;
		T length, value;
};

