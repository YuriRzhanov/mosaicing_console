// Class to hold relative or world 8-parameter transformation
// Must obsolete OptRecord
// October 19, 2005: along 'Homography', member 'Hmgr' is added
//		Methos 'Hmgr()' is renamed to GetHomography()'
//		Method 'SetHmgr()' is renamed to 'SetHomography()'
//		Methos returning Hmgr is named 'GetHmgr()'
//		Methos setting Hmgr is named 'SetHmgr()'

#ifndef _TRANSFORMRECORD_H_
#define _TRANSFORMRECORD_H_

#ifdef Linux
#include <values.h> // For Linux port
#endif

#ifndef CLASSESMIN
#include "BMPImagePyramid.h"
#endif
#include "Coord2.h"
#include "Collocations.h"
#include "Hmgr.h"
#include "RAR.h"

#define TRANS_FATAL	-100
#define TRANS_NONFATAL	-1

class TransformRecord {
	public:
		TransformRecord();
		TransformRecord(const TransformRecord& r);
		~TransformRecord();
		void Reset();
		void Copy(const TransformRecord& r);
		void Copy(TransformRecord* r);
		void MarkSet(char *s) {
#ifdef _MSC_VER
      strcpy_s(mark, 8, s);
#else
      strcpy(mark, s);
#endif 
    }
		// Equivalence of comment and mark:
		int MarkIs(char *s) { return(strcmp(mark, s)==0); }
		// Containment of mark in comment:
		int MarkHas(char *s) { return(strstr(mark, s)!=nullptr); }
		// Check for lesser importance:
		int MarkLess(char *s) { return(mark[0]<s[0]); }
		// Add to existing mark, in front of it
		int MarkAdd(char *s);
		int MarkAdd(char s);
		bool MarkGood(); // has 'S'/'F' or doesn't have 'B'/'X'/'E' and does have 's'
		bool MarkBad(); // has 'B'/'X'/'E'
		void SetSizeFocalLength(int _w, int _h, double _fl) {
			w = _w; h= _h; focal_length = _fl; }
		void SetHmgr(const Hmgr& _hmgr) { hmgr = _hmgr; }
		void SetDeriv(const Hmgr& _hmgr) { D = _hmgr; }
		void SetAttr(int _base_f_n = -1, int _f_n = -1, double _error = -1.0, double _overlap = 0.0);
		int Read(FILE *fp, char t='r', int read_collocations = 1);
		int ReadBin(FILE *fp, char t='r');
		int Write(FILE *fp, char t=0);
		int WriteHmgr(FILE *fp, char t=0); // Only new homography
		int WriteBinHmgr(FILE *fp, char t=0);
		char *WriteStg(int digits = -1);
		void SetCollocations(Collocations* pCol);
		Collocations* GetCollocations();
		int ReadCollocations(const char* stg);
		char* WriteCollocations();
		void WriteCollocations(FILE* fp) {
			if(n_col)
				fprintf(fp, "%s", WriteCollocations());
		}
		void DefaultCollocations();
		int ReadHmgr(const char *file);
		int ReadHmgr(FILE *fp);
		static int ReadHmgrStg(const char *stg, Hmgr& h);
		static int ReadBinHmgrStg(const char *stg, Hmgr& h);
		Hmgr& GetHmgr() {return(hmgr); }
		void Invert();
		int Sequential() { return(abs(f_n-base_f_n)==1); }
		static TransformRecord *ReadRecs(char *hmfile, int *nrec, int *_W, int *_H, double *_focal_length, char t='w');
		static TransformRecord *ReadRecs(char *hmfile, int *nrec, char **ppInfo = nullptr, char t='r');
		static TransformRecord *ReadBinRecs(char *hmfile, int *nrec, char **ppInfo = nullptr, char t='r');
		static int SaveRecs(char *hmfile, TransformRecord *recs, int nrec, char *pInfo = nullptr, char t=0);
		static int SaveBinRecs(char *hmfile, TransformRecord *recs, int nrec, char *pInfo = nullptr, char t=0);
		static TransformRecord *FindRecordFromFrameNumber(TransformRecord *recs, int nrec, int _f_n);
		static TransformRecord *FindRecordFromBaseFrameNumber(TransformRecord *recs, int nrec, int _b_f_n);
		static TransformRecord *FindRecordFromNumbers(TransformRecord *recs, int nrec, int _b_f_n, int _f_n);
		static int FindOrderNumberFromFrameNumber(TransformRecord *recs, int nrec, int _f_n);
		static int FindOrderNumberFromNumbers(TransformRecord *recs, int nrec, int _b_f_n, int _f_n);
		static TransformRecord* CopyAll(TransformRecord *recs, int nrec);
		static void MarkAllEssential(TransformRecord *recs, int nrec, bool mark);
		static char* GetError() { return(buf); }
		static int InsertRecord(TransformRecord*& recs, int& nrec, TransformRecord* record);
		static int DeleteRecord(TransformRecord*& recs, int& nrec, int k);
		static int AddRecordToFile(const char* filename, TransformRecord& rec, bool insist = false);
		static int AddRecordToFile(FILE* fp, TransformRecord& rec, bool insist = false);
	public:
		char type; // 'w' for world, 'r' for relative
		char mark[8];
		int f_n, base_f_n, uniqueTag; // For sequential records 'hm' describes
			// mapping of frame 'f_n' to the previous frame, which is
			// 'base_f_n=f_n-1'. For cross-regs, 'base_f_n' could be any.
		int anchored; // Flag specifying that the frame is already optimized
		int w, h; // frame size
		double focal_length;
		Hmgr hmgr;
		int n_col; // Number of collocations used for calculation of this transform
		Coord2* col_b, *col_d; // Collocations: base and derived
		//NEWMAT::ColumnVector secondDer; // Estimate of 2-nd derivatives in optimal point. Use instead D
		Hmgr D; // 2-nd derivative, showing influence on penalty function
		double av_error, max_error, overlap_percent;
		double PeakParams[6]; // Extent of pixels above 80% and 95% level
			// normalized by peak image size.
		bool essential; // Flag used to separate transforms into part needed to build
			// a mosaic and part additional to that
		static char buf[1024]; // Used as error string too
};

class TransformRecordList {
	public:
		TransformRecordList(int s_N = 0);
		TransformRecordList(char *file);
		~TransformRecordList();
		TransformRecord *Find(int base_n, int derived_n);
		TransformRecord *FindBase(int base_n, int *from=nullptr);
		TransformRecord *FindDerived(int derived_n, int *from=nullptr);
		int Add(TransformRecord *r) { start[n++] = new TransformRecord(*r);
			if(n>N) { fprintf(stderr, "Not enough TransformRecords.\n"); exit(1); }
			return(n-1); }
		TransformRecord *Add(int _base_n, int _derived_n) { start[n] = new TransformRecord;
			start[n]->SetAttr(_base_n, _derived_n, -1.0, 0.0);
			n++;
			if(n>N) { fprintf(stderr, "Not enough RelativeTransforms.\n"); exit(1); }
			return(start[n-1]); }
		TransformRecord *First() { c=0; current = start[c]; return(current); }
		TransformRecord *Next() {
			c++; current = start[c]; return((c>=N) ? nullptr : current); }
	public:
		void Print(const char* filename);
		int N; // Number of allocated records
		int n; // Current number in allocation
		int c; // Current number in retrieval
		TransformRecord **start, *current;
};
	

#endif
