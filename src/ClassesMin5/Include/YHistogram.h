#pragma once

#ifndef _YHISTOGRAM_H_DEFINED_
#define _YHISTOGRAM_H_DEFINED_
// Sum of all entries is 1!

#include <stdlib.h>
#include <iostream>
#ifndef CMIN
//#include "CubicSpline.h"
#endif

template <class T>
class Y_Histogram
{
public:
	Y_Histogram() {
		n = 256; samples = 0; cumulative = false; fail = false;
		low = T(0); high = T(n-1);
		bin = (high-low)/(n-1);
		memset(powerSum, 0, 5*sizeof(double));
		if(n>=0) {
			hist = new T[n];
			memset(hist, 0, n*sizeof(T));
		} else {
			hist = nullptr;
		}
	}
	Y_Histogram(int _n) {
		n = _n; samples = 0; cumulative = false; fail = false;
		low = T(0); high = T(n-1);
		bin = (high-low)/(n-1);
		memset(powerSum, 0, 5*sizeof(double));
		if(n>=0) {
			hist = new T[n];
			memset(hist, 0, n*sizeof(T));
		} else {
			hist = nullptr;
		}
	}
	Y_Histogram(const char* name) {
		n = 0; samples = 0; cumulative = false; hist = nullptr;
		memset(powerSum, 0, 5*sizeof(double));
		fail = (Read(name)==-1);
	}
	Y_Histogram(Y_Histogram& h) {
		n = h.n; samples = h.samples; cumulative = h.cumulative; fail = h.fail;
		low = h.low; high = h.high; bin = h.bin;
		memcpy(powerSum, h.powerSum, 5*sizeof(double));
		if(n>=0) {
			hist = new T[n];
			memcpy(hist, h.hist, n*sizeof(T));
		} else {
			hist = nullptr;
		}
	}
	~Y_Histogram(void) {
		if(hist) delete [] hist;
	}
	void SetLimits(T _low, T _high) {
		low = _low; high = _high;
		bin = (high-low)/(n-1);
	}
	int Add(T v) { // Add 1 to bin 'v'
		int k = int((v-low)/bin);
		if(k<0 || k>(n-1))
			return(-1);
		hist[k] += T(1.0);
		samples++;
		return(0);
	}
	int Add(T num, T v) { // Add 'num' to bin 'v' at once
		int k = int((v-low)/bin);
		if(k<0 || k>(n-1))
			return(-1);
		hist[k] += num;
		samples++;
		return(0);
	}
	int Add(int num, T* v) { // Add 'num' 1's to bins stored in array 'v'
		for(int i = 0; i<num; i++) {
			int k = int((v[i]-low)/bin);
			if(k<0 || k>(n-1))
				return(-1);
			hist[k] += T(1.0);
		}
		samples += num;
		return(0);
	}
	// October 2006: changed from V(), because doesn't compile for T==int
	T V_elt(int i) { // Element number 'i'
		if(i<0 || i>=n) {
			fprintf(stderr, "Index %d out of range.\n", i);
			return(T(0));
		}
		return(hist[i]);
	}
	T V(T v) { // Element corresponding to value 'v'
		int i = int((v-low)/bin);
		if(i<0 || i>=n) {
			fprintf(stderr, "Index %d out of range.\n", i);
			return(T(0));
		}
		return(hist[i]);
	}
	T X(int i) { // Level corresponding to index i
		return(low+bin*i);
	}
	int Idx(T x) { // Index at which certain X is reached
		return(int((x-low)/bin));
	}
	T Sum(int from, int to) {
		T sum = 0;
		for(int k = from; k<=to; k++)
			sum += hist[k];
		return(sum);
	}
	T SumAbove(T level) {
		T sum = 0;
		for(int k = 0; k<n; k++) {
			if((low+bin*k)>level)
				sum += hist[k];
		}
		return(sum);
	}
	T SumBelow(T level) {
		T sum = 0;
		for(int k = 0; k<n; k++) {
			if((low+bin*k)<level)
				sum += hist[k];
		}
		return(sum);
	}
	void Normalize(T requiredSum = 1) {
		T sum = Sum(0, n-1);
		T factor = requiredSum/sum;
		for(int i = 0; i<n; i++)
			hist[i] *= factor;
		// 'samples' becomes senseless
		samples = 0;
	}
	void Zero() {
		memset(hist, 0, n*sizeof(T));
	}
	// From non-cumulative make a cumulative
	Y_Histogram* Cumulative() {
		if(cumulative) {
			fprintf(stderr, "This histogram is already cumulative.\n");
			return(nullptr);
		}
		Y_Histogram* cum = new Y_Histogram(n);
		cum->cumulative = true;
		T sum = T(0);
		for(int i = 0; i<n; i++) {
			sum += hist[i];
			cum->hist[i] = sum;
		}
		return(cum);
	}
	// From cumulative make a non-cumulative
	Y_Histogram* NonCumulative() {
		if(!cumulative) {
			fprintf(stderr, "This histogram is not cumulative.\n");
			return(nullptr);
		}
		Y_Histogram* noncum = new Y_Histogram(n);
		noncum->cumulative = false;
		T prev = T(0);
		for(int i = 0; i<n; i++) {
			noncum->hist[i] = hist[i]-prev;
			prev = hist[i];
		}
		return(noncum);
	}
	int Read(const char* name) {
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, name, "r"))
			return(-1);
#else
    fp = fopen(name, "r");
    if(!fp)
      return(-1);
#endif
		char stg[256];
		fgets(stg, 256, fp);
		int _n;
		if(typeid(T)==typeid(long))
			sscanf_s(stg, "%*s N=%d  L=%d  H=%d  S=%d", &_n, &low, &high, &samples);
		else if(typeid(T)==typeid(int))
			sscanf_s(stg, "%*s N=%d  L=%d  H=%d  S=%d", &_n, &low, &high, &samples);
		else if(typeid(T)==typeid(short))
			sscanf_s(stg, "%*s N=%d  L=%d  H=%d  S=%d", &_n, &low, &high, &samples);
		else if(typeid(T)==typeid(float))
			sscanf_s(stg, "%*s N=%d  L=%f  H=%f  S=%d", &_n, &low, &high, &samples);
		else
			sscanf_s(stg, "%*s N=%d  L=%lf  H=%lf  S=%d", &_n, &low, &high, &samples);
		int k = 0;
		while(fgets(stg, 256, fp)) {
			if(stg[0]=='#' || strlen(stg)<2) continue;
			k++;
		}
		if(_n!=k) {
			fprintf(stderr, "Cannot read histogram.\n");
			fclose(fp);
			return(-1);
		}
		rewind(fp);
		n = k;
		bin = (high-low)/n;
		if(hist) delete [] hist;
		hist = new T[n];
		k = 0;
		cumulative = true; // by default
		while(fgets(stg, 256, fp)) {
			if(stg[0]=='#' || strlen(stg)<2) continue;
			if(typeid(T)==typeid(long))
				sscanf_s(stg, "%d", &hist[k]);
			else if(typeid(T)==typeid(int))
				sscanf_s(stg, "%d", &hist[k]);
			else if(typeid(T)==typeid(short))
				sscanf_s(stg, "%d", &hist[k]);
			else if(typeid(T)==typeid(float))
				sscanf_s(stg, "%f", &hist[k]);
			else
				sscanf_s(stg, "%lf", &hist[k]);
			if(k>0 && hist[k]<hist[k-1])
				cumulative = false;
			k++;
		}
		fclose(fp);
		return(0);
	}
	int ReadRawTwoColumns(const char* name) {
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, name, "r"))
			return(-1);
#else
    fp = fopen(name, "r");
    if(!fp)
      return(-1);
#endif
		char stg[256];
		int count = 0;
		while(fgets(stg, 256, fp)) {
			if(stg[0]!='#' && strlen(stg)>3)
				count++;
		}
		n = count; samples = 0; cumulative = false; fail = false;
		delete [] hist;
		double* histD = new double[n];
		double* X = new double[n];
		rewind(fp);
		count = 0;
		while(fgets(stg, 256, fp)) {
			if(stg[0]!='#' && strlen(stg)>3) {
#ifdef _MSC_VER
				sscanf_s(stg, "%lf %lf", &X[count], &histD[count]);
#else
				sscanf(stg, "%lf %lf", &X[count], &histD[count]);
#endif
				count++;
			}
		}
		hist = new T[n];
		low = T(X[0]); high = T(X[n-1]);
		bin = (high-low)/(n-1);
		for(int i = 0; i<n; i++) {
			hist[i] = T(histD[i]);
			samples += int(hist[i]+0.00000001);
		}
		fclose(fp);
		delete [] histD;
		delete [] X;
		return(0);
	}
	int Save(char* name) {
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, name, "w"))
			return(-1);
#else
    fp = fopen(name, "w");
    if(!fp)
      return(-1);
#endif
		if(typeid(T)==typeid(char))
			fprintf(fp, "# N=%d  L=%d  H=%d  S=%d\n", n, low, high, samples);
		else if(typeid(T)==typeid(long))
			fprintf(fp, "# N=%d  L=%d  H=%d  S=%d\n", n, low, high, samples);
		else if(typeid(T)==typeid(int))
			fprintf(fp, "# N=%d  L=%d  H=%d  S=%d\n", n, low, high, samples);
		else if(typeid(T)==typeid(short))
			fprintf(fp, "# N=%d  L=%d  H=%d  S=%d\n", n, low, high, samples);
		else if(typeid(T)==typeid(float))
			fprintf(fp, "# N=%d  L=%f  H=%f  S=%d\n", n, low, high, samples);
		else
			fprintf(fp, "# N=%d  L=%lf  H=%lf  S=%d\n", n, low, high, samples);
		for(int i = 0; i<n; i++) {
			if(typeid(T)==typeid(char))
				fprintf(fp, "%d\n", (int) hist[i]);
			else if(typeid(T)==typeid(long))
				fprintf(fp, "%d\n", hist[i]);
			else if(typeid(T)==typeid(int))
				fprintf(fp, "%d\n", hist[i]);
			else if(typeid(T)==typeid(short))
				fprintf(fp, "%d\n", hist[i]);
			else if(typeid(T)==typeid(float))
				fprintf(fp, "%.6f\n", hist[i]);
			else
				fprintf(fp, "%.6g\n", hist[i]);
		}
		fclose(fp);
		return(0);
	}
	// Save in a form suitable for Excel
	int SaveForExcel(const char* name) {
		if(samples==0) {
			fprintf(stderr, "Cannot save normalized histogram for Excel.\n");
			return(-1);
		}
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, name, "w"))
			return(-1);
#else
    fp = fopen(name, "w");
    if(!fp)
      return(-1);
#endif
		for(int l = 0; l<n; l++) {
			fprintf(fp, "%.6g\t%.6g\n", double(low+bin*l), double(hist[l]));
		}
		fclose(fp);
		return(0);
	}
	// Save with two columns
	int SaveTwoColumns(const char* name) {
		if(samples==0) {
			fprintf(stderr, "Cannot save normalized histogram.\n");
			return(-1);
		}
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, name, "w"))
			return(-1);
#else
    fp = fopen(name, "w");
    if(!fp)
      return(-1);
#endif
		for(int l = 0; l<n; l++) {
			fprintf(fp, "%.6g\t%.6g\n", double(low+bin*l), double(hist[l]));
		}
		fclose(fp);
		return(0);
	}
	// Remove all measurements outside the specified percentile (0:100)
	void LeavePercentile(double percentile) {
		if(percentile>=100.0) return;
		if(samples==0) {
			fprintf(stderr, "Cannot cut percentile off normalized histogram.\n");
			return;
		}
		int samples_to_cut = samples*(100-int(percentile))/200; // each side
		int accum = 0, k = 0;
		while(accum<samples_to_cut) {
			accum += int(hist[k]);
			samples -= int(hist[k]);
			hist[k] = T(0);
			k++;
		}
		accum = 0; k = n-1;
		while(accum<samples_to_cut) {
			accum += int(hist[k]);
			samples -= int(hist[k]);
			hist[k] = T(0);
			k--;
		}
	}
	void LeaveNotLessThanPercentile(double percentile)
	{
		if(percentile>=100.0) return;
		if(samples==0) {
			fprintf(stderr, "Cannot cut percentile off normalized histogram.\n");
			return;
		}
		int samples_to_cut = samples*(100-int(percentile))/200; // each side

		int last_samples = 0;
		double last_value = 0;
		int accum = 0, k = 0;
		while(accum<samples_to_cut) {
			accum += int(hist[k]);
			last_samples = samples;
			last_value = hist[k];
			samples -= int(hist[k]);
			hist[k] = 0.0;
			k++;
		}
		// Return back last removed:
		samples += last_samples;
		hist[k-1] = last_value;

		accum = 0; k = n-1;
		while(accum<samples_to_cut) {
			accum += int(hist[k]);
			last_samples = samples;
			last_value = hist[k];
			samples -= int(hist[k]);
			hist[k] = 0.0;
			k--;
		}
		// Return back last removed:
		samples += last_samples;
		hist[k+1] = last_value;
	}
	// Return limits between which lies required percent of samples:
	void LeavePercentile(double percentile, T& _low, T& _high) {
		if(samples==0) {
			fprintf(stderr, "Cannot cut percentile off normalized histogram.\n");
			_low = low; _high = high;
			return;
		}
		if(percentile>=100.0) {
			_low = low; _high = high;
			return;
		}
		T sum = T(samples);
		T step = (high-low)/T(100);
		T level = low;
		//T sum_T = SumAbove(level);
		T sum_T = sum;
		while(sum_T > sum*percentile/T(100)) {
			level += step;
			sum_T = SumAbove(level);
		}
		int low_k = 0;
		for(int k = 0; k<n; k++) {
			if(hist[k]>=level) {
				low_k = k;
				break;
			}
		}
		_low = X(low_k);

		level = high;
		//sum_T = SumBelow(level);
		sum_T = sum;
		while(sum_T > sum*percentile/T(100)) {
			level -= step;
			sum_T = SumBelow(level);
		}

		int high_k = n-1;
		for(int k = n-1; k>=0; k--) {
			if(hist[k]>=level) {
				high_k = k;
				break;
			}
		}
		_high = X(high_k);
	}
	// Fill in possible gaps
	/*void Interpolate() {
		double* x = new double[n];
		double* y = new double[n];
		int i, j, count = 0;

		x[0] = 0.0; y[0] = double(hist[0]); // Enforce
		for(i = 1, j = 1; i<n; i++) {
			if(hist[i] != T(0)) {
				x[j] = double(i);
				y[j] = double(hist[i]);
				j++;
			}
		}
		x[j] = double(n); y[j] = double(hist[255]);
		j++;
		CCubicSpline* spline = new CCubicSpline(j, x, y);
		for(i = 1; i<n; i++) {
			if(hist[i] == T(0))
				hist[i] = T(spline->Value(double(i)));
			if(hist[i]<0.0) hist[i] = T(0);
		}
		Normalize();
		delete [] x;
		delete [] y;
		delete spline;
	}*/
	// Return value below which required percent of samples lies (0:100)
	T PercentileBelow(double percent) {
		if(percent<=0.0)
			return(low);
		if(percent>=100.0)
			return(high);
		int i;
		T sum = Sum(0, n-1); // number of samples
		double limit = sum*0.01*percent;
		sum = 0.0;
		for(i = 0; i<n; i++) {
			sum += double(hist[i]);
			if(sum>limit) // could happen only when hist[i]!=0. Hence can interpolate linearly
				return(T(low+bin*(double(i)-(sum-limit)/hist[i])));
		}
		return(high); // Too high percent?
	}
	// Return value above which required percent of samples lies (0:100)
	T PercentileAbove(double percent) {
		T low = PercentileBelow(100.0-percent);
		return(low);
	}
	// Total number of additions to the histogram
	int Samples() {
		return(samples);
	}
	T Min() {
		for(int i = 0; i<n; i++) {
			if(hist[i]>T(0))
				return(low+bin*i);
		}
		return(high);
	}
	T Max() {
		for(int i = n-1; i>=0; i--) {
			if(hist[i]>T(0))
				return(low+bin*i);
		}
		return(low);
	}
	// Average value of all values
	T Average() {
		T sum = Sum(0, n-1);
		return(T(sum/n));
	}

	// Return mean of the distribution (location of centroid)
	// i-th bin includes all samples with values in range [i:i+1],
	// hence average bin value is (0.5+i)
	T Mean() { // 1-st moment
		double sum = 0.0, sum_h = 0.0;
		for(int i = 0; i<n; i++) {
			sum += hist[i]*(0.5+i);
			sum_h += hist[i];
		}
		return(T(sum/sum_h));
	}
	// The following are taken from Excel definitions:
	T Variance() { // 2-nd moment
		double mean = Mean();
		double sum = 0.0, sum_h = 0.0, v;
		for(int i = 0; i<n; i++) {
			v = 0.5+i-mean;
			sum += hist[i]*v*v;
			sum_h += hist[i];
		}
		return(T(sum/sum_h));
	}
	T Variance_A() { // 2-nd moment
		double mean = Mean();
		double sum = 0.0, sum_h = 0.0, v;
		for(int i = 0; i<n; i++) {
			v = 0.5+i-mean;
			sum += hist[i]*v*v;
			sum_h += hist[i];
		}
		return(T(sum/sum_h));
	}
	T Skewness() { // 3-rd moment
		double mean = Mean();
		double var = Variance();
		double std = sqrt(var);
		double sum = 0.0, sum_h = 0.0, v;
		for(int i = 0; i<n; i++) {
			v = 0.5+i-mean;
			sum += hist[i]*v*v*v;
			sum_h += hist[i];
		}
		double third_moment = sum/sum_h;
		//fprintf(stderr, "mean=%lf   var=%lf   third=%lf\n", mean, var, third_moment);
		return(T(third_moment/(std*std*std)));
	}
	T Kurtosis() { // 4-th moment
		double mean = Mean();
		double var = Variance();
		double std = sqrt(var);
		double sum = 0.0, sum_h = 0.0, v;
		for(int i = 0; i<n; i++) {
			v = 0.5+i-mean;
			sum += hist[i]*v*v*v*v;
			sum_h += hist[i];
		}
		double fourth_moment = sum/sum_h;
		return(T(fourth_moment/(std*std*std*std)-3.0));
	}
	void CalculatePowerSums() {
		int i;
		double v;
		for(i = 0; i<5; i++)
			powerSum[i] = 0.0;
		// Actually we have sum over all samples with thir values, 
		// but i-th bin contains hist[i] samples of value i
		for(i = 0; i<n; i++) {
			v = 0.5+i;
			//powerSum[0] += hist[i]*pow((double)i, 0.0); // simply = samples
			powerSum[1] += hist[i]*v;
			powerSum[2] += hist[i]*v*v;
			powerSum[3] += hist[i]*v*v*v;
			powerSum[4] += hist[i]*v*v*v*v;
		}
	}
	T Moment(int m) {
		CalculatePowerSums();
		double moment = 0.0;
		switch(m) {
			case 1:
				moment = powerSum[1]/samples;
				break;
			case 2:
				moment = -powerSum[1]*powerSum[1]/((double)samples*samples)+powerSum[2]/samples;
				break;
			case 3:
				moment = 2.0*powerSum[1]*powerSum[1]*powerSum[1]/((double)samples*samples*samples)-
					3.0*powerSum[1]*powerSum[2]/((double)samples*samples)+powerSum[3]/samples;
				break;
			case 4:
				moment = -3.0*powerSum[1]*powerSum[1]*powerSum[1]*powerSum[1]/((double)samples*samples*samples*samples)+
					6.0*powerSum[1]*powerSum[1]*powerSum[2]/((double)samples*samples*samples)-
					4.0*powerSum[1]*powerSum[3]/((double)samples*samples)+powerSum[4]/samples;
				break;
		}
		return(T(moment));
	}
	Y_Histogram operator + (const Y_Histogram& H) {
		Y_Histogram sum(n);
		sum.SetLimits(low, high);
		cumulative = false;
		if(samples!=0 || H.samples!=0) {
			fprintf(stderr, "Cannot add non-normalized histograms.\n");
			return(sum);
		}
		if(n!=H.n || low!=H.low || high!=H.high) {
			fprintf(stderr, "Cannot add histograms with different characteristics.\n");
			return(sum);
		}
		for(int i = 0; i<n; i++)
			sum.hist[i] = 0.5*(hist[i]+H.hist[i]);
		return(sum);
	}
	Y_Histogram operator - (const Y_Histogram& H) {
		Y_Histogram sub(n);
		sub.SetLimits(low, high);
		cumulative = false;
		if(samples!=0 || H.samples!=0) {
			fprintf(stderr, "Cannot subtract non-normalized histograms.\n");
			return(sub);
		}
		if(n!=H.n || low!=H.low || high!=H.high) {
			fprintf(stderr, "Cannot subtract histograms with different characteristics.\n");
			return(sub);
		}
		for(int i = 0; i<n; i++)
			sub.hist[i] = fabs(hist[i]+H.hist[i]);
		return(sub);
	}
	//double KullbackLeiblerDistance(const Y_Histogram& H) {
	//	if(cumulative) {
	//		fprintf(stderr, "Cannot calculate distance for cumulative histogram\n");
	//		return(1.e39);
	//	}
	//	double sum = 0.0;
	//	for(int i = 0; i<n; i++) {
	//		if(fabs((double)this->V_elt(i))>1.e-15 && fabs((double)H.V_elt(i))>1.e-15)
	//			sum += (double)V_elt(i)*log((double)V_elt(i)/(double)H.V_elt(i));
	//	}
	//	return(sum/log(2.0));
	//}

public:
	int n, samples;
	bool fail;
	T* hist;
	T low, high, bin;
	bool cumulative;
	double powerSum[5];
};

//#ifdef WIN32
//#include "YHistogram.cpp"
//#endif

#endif
