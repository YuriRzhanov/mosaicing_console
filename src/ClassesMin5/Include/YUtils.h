#ifndef _YUTILS_H_DEFINED_
#define _YUTILS_H_DEFINED_

#include <stdio.h>
#include <fstream>
#include <string>
#include <limits.h>
#ifdef _MSC_VER
#include <windows.h> // For SetConsoleTextAttribute()
#endif
#include "defs.h"
#include "YRect.h"

#ifdef _MSC_VER
//#include <windows.h>
//static LARGE_INTEGER	largeInteger;
#include <sys/types.h>
#include <sys/timeb.h>
// The following conflicts with Eigen package:
//#define random() rand()
#define isnan(x) _isnan(x)
#include <float.h>
#else
#include <stdlib.h>
#include <sys/time.h>
#endif

#include <time.h>

using namespace std;

//  Convert to and from 2-digit Binary Coded Decimal
#define  BCDTOINT( bcd )    (((bcd) >> 4) * 10 + ((bcd) & 0xf))
#define  INTTOBCD( i )      ((((i) / 10) << 4) | ((i) % 10))

//#define Dec2HexDigit(d) ((d)<10)?'0'+(d):'a'+(d)-10
//#define HexDigit2Dec(h) ((h)<='9')?(h)-'0':10+(h)-'a'

#define INFO_SIZE 1024

class Y_TopHat {
	public:
		Y_TopHat() {x = 0; y = 0; peak = peak_proc = q1 = q2 = 0.0; bad = 0;}
		Y_TopHat(int s_x, int s_y) {x = s_x; y = s_y;
			peak = peak_proc = q1 = q2 = 0.0; bad = 0;}
		void Set(int s_x, int s_y, double s_p, double s_pp, 
			double s_q1, double s_q2) {x = s_x; y = s_y; peak = s_p;
			peak_proc = s_pp; q1 = s_q1; q2 = s_q2;}
		int x, y, bad;
		double peak, peak_proc;
		double q1, q2;
};

// Forced values for VMRecord: value of -9999.0 means free value
class Y_ForceRecord {
	public:
		Y_ForceRecord() { frame_n=-1; zoom=angle=x_shift=y_shift=-9999.0; }
		Y_ForceRecord(Y_ForceRecord *fr) 
			{ frame_n=fr->frame_n;
			zoom=fr->zoom; angle=fr->angle; x_shift=fr->x_shift; y_shift=fr->y_shift; }
		void Set(int fn, double z, double a, double x, double y)
			{ frame_n = fn; zoom=z; angle=a; x_shift=x; y_shift=y; }
		void Copy(Y_ForceRecord *fr)
			{ frame_n=fr->frame_n;
			zoom=fr->zoom; angle=fr->angle; x_shift=fr->x_shift; y_shift=fr->y_shift; }
		void Read(FILE *fp) 
			{ char stg[256]; fgets(stg, 255, fp); 
#ifdef _MSC_VER
			sscanf_s(stg, "%d %lf %lf %lf %lf", &frame_n, &x_shift, &y_shift, &zoom, &angle); 
#else
			sscanf(stg, "%d %lf %lf %lf %lf", &frame_n, &x_shift, &y_shift, &zoom, &angle); 
#endif
      }
		static int IsForced(double p) { return(EPSILON_EQ(p,9999.0)); }
	public:
		int frame_n;
		// angle in degrees
		double zoom, angle, x_shift, y_shift;
};


#define VMREC_FATAL -100
#define VMREC_NONFATAL -1

class Y_TrajRecord {
	public:
		Y_TrajRecord() {}
		Y_TrajRecord(int fn, 
			double zm, double pm, double rm, double ym, double dxm, double dym, 
			double pc, double rc, double yc, double zc, double dxc, double dyc, 
			double err);
		void SetM(int fn, double z, double p, double r, double y,
			double dx, double dy);
		void SetC(int fn, double z, double p, double r, double y,
			double dx, double dy, double err);
	public:
		int frame_n;
		double pitch_m, roll_m, yaw_m; // Measured by sensor
		double zoom_m, dx_m, dy_m; // Measured by sensor
		double pitch_c, roll_c, yaw_c; // Calculated
		double zoom_c, dx_c, dy_c; // Calculated
		double error;
};

class Y_Utils {
	public:
		// For SGI/Sun/Apple (Most significant byte first) endian=0 (*s=256)
		// For Intel (Least significant byte first) endian=1 (*s=1)
		static int get_endian() { char c[2]; u_short *s; c[0] = 1; c[1] = 0;
			s = (u_short *) c; return(*s == 1); }
		static bool IncreaseIntBufferIfNeeded(int*& buffer, int& allocatedLength, int currentLength, int slack, int factor);
		static void CheckValid(int day, int mon, int year);
		static char *ExtractParam(const char *stg, const char *tag);
		static int bresenham(Point2<int> start, Point2<int> end, Point2<int> *pp, int N);
		static int brescirc(Point2<int> center, int radius, Y_Rect<int> R, Point2<int> *pp, int N);
		static int XiaolinWuLine(Point2<int> start, Point2<int> end, Point2<int> *pp, double* pFactor, int N);
		static int XiaolinWuCircle(Point2<int> center, int radius, Y_Rect<int> R, Point2<int> *pp, double* pFactor, int N);
		static int plotpts(Point2<int> center, Point2<int> pt, Y_Rect<int> R, Point2<int> *p, int N);
		static int GetReadings(const char *message, double& pitch, double& roll, double& yaw);
		static int GetPosition(const char *message, double& x, double& y, double& z);
		static long TimecodeToFrame(const char *tc);
		static char *FrameToTimecode(long fr);
		static long TimecodeToFrameDropped(const char *tc);
		static char *FrameToTimecodeDropped(long fr);
		static void PrintResponse(int i, int& printout, char s = 0);
		static void ToLower(unsigned char* stg);
		static void ToUpper(unsigned char* stg);
		static unsigned char Dec2HexDigit(unsigned int d);
		static unsigned int HexDigit2Dec(unsigned char h);
		static int bin2dec(const unsigned char *b);
		// Returned string has to be freed if storage is not supplied
		static unsigned char *hex2bin(const unsigned char *h, unsigned char *storage = nullptr);
		static unsigned int hex2dec(const unsigned char *h);
		static unsigned char *dec2hex(unsigned int d, unsigned char *storage = nullptr);
		static unsigned char *dec2bin(unsigned int d, unsigned char *storage = nullptr);
		static void four1(double data[], int nn, int isign);
		// isign=-1 for forward FFT, +1 for backward FFT
		static void fourn(double data[], int nn[], int ndim, int isign);
		static double GammaFunction(double x);
		// Random numbers
		static void Seed(unsigned int seed_value = 0);
		static double d_rand(double r_min, double r_max);
		static int i_rand(int i_min, int i_max);
		static double gauss(double mean, double stdev);
		static bool randomSet(int setLen, int totalLen, int* pSet);
		static void sort(long n, int *array);
		static void sort_double(long n, double *array);
		static void Swap2Chars(u_char *c1,u_char *c2) {
			u_char temp = *c1; *c1 = *c2; *c2 = temp; }
		static void Swap2Byte(u_char *buffer) {
			Swap2Chars(&buffer[0], &buffer[1]); }
		static void Swap4Byte(u_char *buffer) {
			Swap2Chars(&buffer[0], &buffer[3]);
			Swap2Chars(&buffer[1], &buffer[2]); }
		static void Swap8Byte(u_char *buffer) {
			Swap2Chars(&buffer[0], &buffer[7]);
			Swap2Chars(&buffer[1], &buffer[6]);
			Swap2Chars(&buffer[2], &buffer[5]);
			Swap2Chars(&buffer[3], &buffer[4]); }
		static int SubstituteChars(char* stg, char from, char with);
		static int HasInfo(const char *filename);
		static char *GetInfo(const char *filename);
		static char *GetComment(const char *filename);
		static Y_Rect<int> *GetFrameSize(const char *filename);
		static Y_Rect<int> *GetFrameSizeFromInfo(const char *pInfo);
		static char* GetMovieName(const char *pInfo);
		static double GetFocalLength(const char *pInfo);
		static int GetTrackLength(const char *pInfo);
		static double GetFoV(const char *pInfo);
		static void GetFrameSize(const char* stg, int& w, int& h);
		static int ExtractIntValue(const char *stg, const char* tag);
		static double ExtractDoubleValue(const char *stg, const char* tag);
		static int AddCommentToInfo(char* pInfo, const char* comment);
		static int AddProducedLineToInfo(char* pInfo, const char* comment);
		static void WriteInfo(char *pInfo, const char* movieName, int trackLength, int width, int height,
			double focalLength, double FoV);
		static int CountTokens(char* str, char start, char stop);

#ifdef _MSC_VER
		static unsigned short GetConsoleTextAttribute(HANDLE hConsole = nullptr);
#else
		static unsigned short GetConsoleTextAttribute();
#endif
		static void ConsoleWhite();
		static void ConsoleRed();
		static void ConsoleGreen();
		static void ConsoleBlue();
		static void ResetConsole();

		static u_char _INVALID_UCHAR;
		static u_short _INVALID;
		static u_short _INVALID_USHORT;
		static int _INVALID_INT;
		static float _INVALID_FLOAT;
		static double _INVALID_DOUBLE;
		static u_short consoleTextAttribute;
};

/*
class Y_Timer
{
public:
	Y_Timer(int& MilliSeconds) : m_elapsed(MilliSeconds)
	{
		::QueryPerformanceCounter(&m_Start);
	}

	~Y_Timer(void)
	{
		LARGE_INTEGER stop;
		LARGE_INTEGER freq;
		::QueryPerformanceCounter(&stop);
		::QueryPerformanceFrequency(&freq);

		stop.QuadPart -= m_Start.QuadPart;
		stop.QuadPart *= 1000;
		stop.QuadPart /= freq.QuadPart;

		if(stop.HighPart!=0) 
			m_elapsed = -1;
		else 
			m_elapsed = stop.LowPart;
	}
private:
	LARGE_INTEGER m_Start;
	int& m_elapsed;
};
*/

#endif
