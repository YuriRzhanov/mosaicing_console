#include "stdafx.h"
#include "ConsoleProgress.h"

bool CoRegister(BMPImageG* pBmpG1, int fn1, BMPImageG* pBmpG2, int fn2, TransformRecord* trans, Y_LDRParams params, const CoRegParams& coRegParams)
{
	Y_LDRRegister* pReg = new Y_LDRRegister((coRegParams.useSIFT) ? "SIFT" : "SURF", pBmpG1, pBmpG2, &params);

	pReg->SetDebug(bool(coRegParams.debug), nullptr, ".");
	pReg->SetPerspective(coRegParams.perspectiveModel);
	int num_matches = pReg->DoRegister("BF");

	//if(m_fpSaveFeatures)
	//	SaveFeaturesToFile(fn1, fn2, pReg);
	pReg->GetTransform(trans, fn1, fn2);
	trans->av_error = pReg->PerPixelError();
	bool success = !pReg->fail;
	delete pReg;
	return(success);
}

bool CoRegister(BMPImageG* pBmpG1, int fn1, BMPImageG* pBmpG2, int fn2, RARecord* rra, Y_LDRParams params, const CoRegParams& coRegParams)
{
	Y_LDRRegister* pReg = new Y_LDRRegister((coRegParams.useSIFT) ? "SIFT" : "SURF", pBmpG1, pBmpG2, &params);

	pReg->SetDebug(bool(coRegParams.debug), nullptr, ".");
	pReg->SetPerspective(coRegParams.perspectiveModel);
	int num_matches = pReg->DoRegister("BF");

	//if(m_fpSaveFeatures)
	//	SaveFeaturesToFile(fn1, fn2, pReg);
	pReg->GetRRA(rra, fn1, fn2);
	rra->av_error = pReg->PerPixelError();
	if(pReg->fail)
		memset(rra->PeakParams, 0, 4*sizeof(double));
	else
		memcpy(rra->PeakParams, pReg->m_stats, 4*sizeof(double));
	bool success = !pReg->fail;
	delete pReg;
	return(success);
}

int CoRegAVI(Y_Movie64* pAVI, FILE* fp_log, Y_LDRParams params, const CoRegParams& coRegParams)
{
	int numFrames = pAVI->GetNumVideoFrames();
	BMPImageC* pBmpC1, *pBmpC2;
	TransformRecord trans;
	RARecord rra;
	bool success;
	int status = 1;

	ConsoleProgress* pCP = new ConsoleProgress(numFrames);
	for(int i = 0; i<numFrames-1; i++) {
		if(i<coRegParams.frameFrom || (i+1)>coRegParams.frameTo) continue; // out of specified range
		pAVI->ReadVideoFrame(&pBmpC1, i);
		if(!pBmpC1 || pBmpC1->fail) {
			cerr << "Cannot extract frame " << i << "\n";
			goto END;
		}
		pAVI->ReadVideoFrame(&pBmpC2, i+1);
		if(!pBmpC2 || pBmpC2->fail) {
			cerr << "Cannot extract frame " << i+1 << "\n";
			goto END;
		}
		BMPImageG* pBmpG1 = pBmpC1->Greyscale(); delete pBmpC1;
		pBmpG1->Validate(); pBmpG1->SetFocalLength(coRegParams.focalLength);
		BMPImageG* pBmpG2 = pBmpC2->Greyscale(); delete pBmpC2;
		pBmpG2->Validate(); pBmpG2->SetFocalLength(coRegParams.focalLength);
		if(coRegParams.perspectiveModel) {
			trans.Reset();
			success = CoRegister(pBmpG1, i, pBmpG2, i+1, &trans, params, coRegParams);
			TransformRecord::AddRecordToFile(fp_log, trans, true);
		} else {
			rra.Reset();
			success = CoRegister(pBmpG1, i, pBmpG2, i+1, &rra, params, coRegParams);
			RARecord::AddRecordToFile(fp_log, rra, true);
		}
		delete pBmpG1;
		delete pBmpG2;
		pCP->Next();
	}
	delete pCP;
	status = 0;
END:
	return(status);
}
