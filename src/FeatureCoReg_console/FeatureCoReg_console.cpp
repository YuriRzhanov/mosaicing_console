// FeatureCoReg_console.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Descr.h"

using namespace std;

extern char* program;

bool AddStringToOutputFile(FILE* fp, char* stg);
int CoRegAVI(Y_Movie64* pAVI, FILE* fp_log, Y_LDRParams params, const CoRegParams& coRegParams);

int main(int argc, char* argv[])
{
	program = argv[0];

	int debug = 0;
	char* pAVIName = nullptr;
	char* pRRAFileName = nullptr;
	double focalLength = 1000.0;
	int numOctaves = 3;
	int numLevels = 5;
	int firstOctave = 1;
	double contrastThreshold = 0.04;
	double edgeThreshold = 10.0;
	double sigma = 1.6;
	double ratioThreshold = 0.7;
	int allowedProximity = 4;
	double tolerance = 4.0;
	double maxScale = 1.2;
	int frameFrom = 0, frameTo = 1000000000;
	double requiredOverlap = 80.0;
	double detectorThreshold = 400.0;
	bool useSIFT = false;
	bool useANN = false;
	bool perspectiveModel = false;
	int numberGood = 0, numberBad = 0;

	DFile f(argv[1], "FCR.IN", 2);
	f.Reg("Debug", "Debug level", &debug, debug);
	f.Reg("AVIName", "AVI file name", &pAVIName, pAVIName);
	f.Reg("RRAName", "RRA file name", &pRRAFileName, pRRAFileName);
	f.Reg("FocalLength", "Focal length in pixels", &focalLength, focalLength);
	f.Reg("NumOctaves", "Number of octaves", &numOctaves, numOctaves);
	f.Reg("FirstOctave", "First octave", &firstOctave, firstOctave);
	f.Reg("ContrastThreshold", "Contrast threshold", &contrastThreshold, contrastThreshold);
	f.Reg("EdgeThreshold", "Edge threshold", &edgeThreshold, edgeThreshold);
	f.Reg("Sigma", "Sigma", &sigma, sigma);
	f.Reg("RatioThreshold", "Ratio threshold", &ratioThreshold, ratioThreshold);
	f.Reg("AllowedProximity", "Allowed proximity", &allowedProximity, allowedProximity);
	f.Reg("NumLevels", "Number of levels", &numLevels, numLevels);
	f.Reg("Tolerance", "Tolerance in pixels", &tolerance, tolerance);
	f.Reg("MaxScale", "Maximum scale change", &maxScale, maxScale);
	f.Reg("FrameRange", "Frames to use", &frameFrom, frameFrom, &frameTo, frameTo);
	f.Reg("RequiredOverlap", "Required overlap between consecutive frames", &requiredOverlap, requiredOverlap);
	f.Reg("DetectorThreshold", "Threshold for feature detection", &detectorThreshold, detectorThreshold);
	f.Reg("UseSIFT", "Use SIFT instead of SURF", &useSIFT, useSIFT);
	f.Reg("UseANN", "Use approx nearest neighbor", &useANN, useANN);
	f.Reg("PerspectiveModel", "use perspective model insted or rigid affine", &perspectiveModel, perspectiveModel);
	f.Process();

	FILE* fp_log;
	if(fopen_s(&fp_log, pRRAFileName, "w")) 
	{
		cerr << "Cannot write to " << pRRAFileName << "\n";
		exit(1);
	}

	Y_Movie64* pAVI  = new Y_Movie64(pAVIName);
	if(pAVI->isOk()==false) {
		cerr << "Cannot open " << pAVIName << "\n";
		exit(1);
	}
	int width = pAVI->GetWidth();
	int height = pAVI->GetHeight();
	frameTo = std::min(frameTo, int(pAVI->GetNumVideoFrames()-1));

	double FoV_h = Rad2Deg(2.0*atan2(0.5*double(width), focalLength));

	// These are set from the dialog:
	pAVI->m_pMovieInfo->m_fl = focalLength;
	pAVI->m_pMovieInfo->m_FoV = FoV_h;
	char Info[1024];
	memcpy(Info, pAVI->m_pMovieInfo->Info(), 1024);
	Y_Utils::AddProducedLineToInfo(Info, program);
	AddStringToOutputFile(fp_log, Info);

	Y_LDRParams params;
	params.m_RANSACTolerance = tolerance;
	params.m_allowedProximity = allowedProximity;
	params.m_contrastThreshold = contrastThreshold;
	params.m_edgeThreshold = edgeThreshold;
	params.m_hessianThreshold = detectorThreshold;

	CoRegParams coRegParams;
	coRegParams.debug = debug;
	coRegParams.frameFrom = frameFrom;
	coRegParams.frameTo = frameTo;
	coRegParams.focalLength = focalLength;
	coRegParams.perspectiveModel = perspectiveModel;
	coRegParams.useSIFT = useSIFT;
	coRegParams.useANN = useANN;

	int status = CoRegAVI(pAVI, fp_log, params, coRegParams);

	fclose(fp_log);
	delete pAVI;

	return 0;
}

// Open output file, add to the end of it
bool AddStringToOutputFile(FILE* fp, char* stg)
{
	fseek(fp, 0, SEEK_END);
	fprintf(fp, "%s", stg);
	return(true);
}
