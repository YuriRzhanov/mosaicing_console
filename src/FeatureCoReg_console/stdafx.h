// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



#include "LibYMovie64.h"
#include "RAR.h"
#include "LDRRegister.h"


typedef struct _CoRegParams {
	int debug;
	int frameFrom, frameTo;
	double focalLength;
	bool perspectiveModel;
	bool useSIFT;
	bool useANN;
} CoRegParams;
