#include "LDRDetect.h"

#if CV_VERSION_MAJOR==3
#include "opencv2/xfeatures2d/nonfree.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#endif

using namespace std;
using namespace cv;
#if CV_VERSION_MAJOR==3
using namespace xfeatures2d;
#endif

char Y_LDRDetect::errorDescr[ERR_LEN];

int Y_LDRDetect::DoDetect()
{
	if(m_pParams->m_splitFactor<=1) {
		//cv::Ptr<cv::FeatureDetector> detector = cv::Ptr<cv::FeatureDetector>(new cv::SurfFeatureDetector());
		//cv::Ptr<cv::FeatureDetector> detector = cv::FeatureDetector::create(m_method);
		//detector->detect(*(m_pIm->m_pMat), m_keypoints);
		if(strncmp(m_method, "SIFT", 4)==0) {
#if CV_VERSION_MAJOR==3
			Ptr<SIFT> detector = SIFT::create(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			detector->detect(*(m_pIm->m_pMat), m_keypoints);
			Ptr<SIFT> extractor = SIFT::create(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			extractor->compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#else
			SiftFeatureDetector detector(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			detector.detect(*(m_pIm->m_pMat), m_keypoints);
			SiftDescriptorExtractor extractor(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			extractor.compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#endif
		} else if(strncmp(m_method, "SURF", 4)==0) {
#if CV_VERSION_MAJOR==3
			Ptr<SURF> detector = SURF::create(m_pParams->m_hessianThreshold, m_pParams->m_octaves,
				m_pParams->m_nOctaveLayers);
			detector->detect(*(m_pIm->m_pMat), m_keypoints);
			Ptr<SURF> extractor = SURF::create(m_pParams->m_octaves, m_pParams->m_nOctaveLayers);
			extractor->compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#else
			SurfFeatureDetector detector(m_pParams->m_hessianThreshold, m_pParams->m_octaves,
				m_pParams->m_nOctaveLayers);
			detector.detect(*(m_pIm->m_pMat), m_keypoints);
			SurfDescriptorExtractor extractor(m_pParams->m_octaves, m_pParams->m_nOctaveLayers);
			extractor.compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#endif
		} else if(strncmp(m_method, "ORB", 3)==0) {
#if CV_VERSION_MAJOR==3
			Ptr<ORB> detector = ORB::create((int)m_pParams->m_nFeatures);
			detector->detect(*(m_pIm->m_pMat), m_keypoints);
			// Descriptors for ORB???
			Ptr<ORB> extractor = ORB::create(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			extractor->compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#else
			OrbFeatureDetector detector(int(m_pParams->m_nFeatures));
			detector.detect(*(m_pIm->m_pMat), m_keypoints);
			// Descriptors for ORB???
			SiftDescriptorExtractor extractor(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			extractor.compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#endif
		//} else if(strncmp(m_method, "DENSE", 5)==0) {
		//	DenseFeatureDetector detector(m_pParams->m_initFeatureScale, m_pParams->m_featureScaleLevels,
		//		m_pParams->m_featureScaleMul, m_pParams->m_initXyStep, m_pParams->m_initImgBound,
		//		m_pParams->m_varyXyStepWithScale, m_pParams->m_varyImgBoundWithScale);
		//	detector.detect(*(m_pIm->m_pMat), m_keypoints);
		//	// Descriptors for DENSE???
		//	SiftDescriptorExtractor extractor(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
		//		m_pParams->m_edgeThreshold, m_pParams->m_sigma);
		//	extractor.compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#ifdef USE_DAISY
		} else if(strncmp(m_method, "DAISY", 5)==0) {
			// Keypoints are extracted by SIFT:
			SiftFeatureDetector detector(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			detector.detect(*(m_pIm->m_pMat), m_keypoints);
			// Descriptors come from daisy:
			daisy* daisyDescr = new daisy();
			daisyDescr->verbose(debug);
			daisyDescr->set_image(m_pIm->m_pMat);
			daisyDescr->set_parameters(m_pParams->m_rad, m_pParams->m_radq, m_pParams->m_thq, m_pParams->m_histq);
			daisyDescr->set_normalization(m_pParams->m_nrm_type);
			daisyDescr->initialize_single_descriptor_mode();
			m_descriptors = daisyDescr->get_specified_descriptors(m_keypoints);
			delete daisyDescr;
#endif
#ifdef USE_KAZE
		} else if(strncmp(m_method, "KAZE", 4)==0) {
			KAZEParams params;
			params.use_upright = false;
			params.use_extended = false;
			params.descriptor_mode = 1;
			params.save_keypoints = false;
			params.img_width = m_pIm->m_pMat->cols;
			params.img_height = m_pIm->m_pMat->rows;
		    KAZE evolution(&params);
			evolution.Set_Detector_Threshold(params.dthreshold2);
			evolution.Create_Nonlinear_Scale_Space(*(m_pIm->m_pMat));
			evolution.Feature_Detection(m_keypoints);
			evolution.Feature_Description(m_keypoints, m_descriptors);
#endif
		} else {
			fail = true;
			return(-1);
		}
		// validate all keypoints
		for(vector<cv::KeyPoint>::iterator iter = m_keypoints.begin(); iter!=m_keypoints.end(); ++iter)
			iter->class_id = 0;
	} else {
		m_pParams->m_splitOverlap += m_pParams->m_splitOverlap%2; // should be even
		int length = (m_pIm->m_pMat->cols+(m_pParams->m_splitFactor-1)*m_pParams->m_splitOverlap)/m_pParams->m_splitFactor, len;
		for(int i = 0, start = 0; i<m_pParams->m_splitFactor; i++) {
			if(i<(m_pParams->m_splitFactor-1))
				len = length;
			else
				len = m_pIm->m_pMat->cols-start;
			Mat imagePart(*(m_pIm->m_pMat), Rect(start, 0, len, m_pIm->m_pMat->rows));
			std::vector<cv::KeyPoint> kp;
			Mat dc;
            if(strncmp(m_method, "SIFT", 4)==0) {
#if CV_VERSION_MAJOR==3
			    Ptr<SIFT> detector = SIFT::create(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				    m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			    detector->detect(imagePart, kp);
			    Ptr<SIFT> extractor = SIFT::create(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				    m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			    extractor->compute(imagePart, kp, dc);
#else
				SiftFeatureDetector detector(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
					m_pParams->m_edgeThreshold, m_pParams->m_sigma);
				detector.detect(imagePart, kp);
				SiftDescriptorExtractor extractor(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
					m_pParams->m_edgeThreshold, m_pParams->m_sigma);
				extractor.compute(imagePart, kp, dc);
#endif
		    } else if(strncmp(m_method, "SURF", 4)==0) {
#if CV_VERSION_MAJOR==3
			    Ptr<SURF> detector = SURF::create(m_pParams->m_hessianThreshold, m_pParams->m_octaves,
				    m_pParams->m_nOctaveLayers);
			    detector->detect(imagePart, kp);
			    Ptr<SURF> extractor = SURF::create(m_pParams->m_octaves, m_pParams->m_nOctaveLayers);
			    extractor->compute(*(m_pIm->m_pMat), kp, dc);
#else
				SurfFeatureDetector detector(m_pParams->m_hessianThreshold, m_pParams->m_octaves,
					m_pParams->m_nOctaveLayers);
				detector.detect(imagePart, kp);
				SurfDescriptorExtractor extractor(m_pParams->m_octaves, m_pParams->m_nOctaveLayers);
				extractor.compute(*(m_pIm->m_pMat), m_keypoints, m_descriptors);
#endif
		    } else if(strncmp(m_method, "ORB", 3)==0) {
#if CV_VERSION_MAJOR==3
                Ptr<ORB> detector = ORB::create((int)m_pParams->m_nFeatures);
			    detector->detect(imagePart, kp);
			    // Descriptors for ORB???
			    Ptr<ORB> extractor = ORB::create(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
				    m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			    extractor->compute(imagePart, kp, dc);
#else
				OrbFeatureDetector detector((int)m_pParams->m_nFeatures);
				detector.detect(imagePart, kp);
				// ??? Descriptors for ORB???
				SiftDescriptorExtractor extractor(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
					m_pParams->m_edgeThreshold, m_pParams->m_sigma);
				extractor.compute(imagePart, kp, dc);
#endif
			//} else if(strncmp(m_method, "DENSE", 5)==0) {
			//	DenseFeatureDetector detector(m_pParams->m_initFeatureScale, m_pParams->m_featureScaleLevels,
			//		m_pParams->m_featureScaleMul, m_pParams->m_initXyStep, m_pParams->m_initImgBound,
			//		m_pParams->m_varyXyStepWithScale, m_pParams->m_varyImgBoundWithScale);
			//	detector.detect(imagePart, kp);
			//	// ??? Descriptors for DENSE???
			//	SiftDescriptorExtractor extractor(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
			//		m_pParams->m_edgeThreshold, m_pParams->m_sigma);
			//	extractor.compute(imagePart, kp, dc);
#ifdef USE_KAZE
			} else if(strncmp(m_method, "DAISY", 5)==0) {
				// Keypoints are extracted by SIFT:
				SiftFeatureDetector detector(0, m_pParams->m_nOctaveLayers, m_pParams->m_contrastThreshold, 
					m_pParams->m_edgeThreshold, m_pParams->m_sigma);
				detector.detect(imagePart, kp);
				// Descriptors come from daisy:
				daisy* daisyDescr = new daisy();
				daisyDescr->verbose(debug);
				daisyDescr->set_image(&imagePart);
				daisyDescr->set_parameters(m_pParams->m_rad, m_pParams->m_radq, m_pParams->m_thq, m_pParams->m_histq);
				daisyDescr->set_normalization(m_pParams->m_nrm_type);
				daisyDescr->initialize_single_descriptor_mode();
				dc = daisyDescr->get_specified_descriptors(kp);
				delete daisyDescr;
#endif
#ifdef USE_KAZE
			} else if(strncmp(m_method, "KAZE", 4)==0) {
				KAZEParams params;
				params.use_upright = false;
				params.use_extended = false;
				params.descriptor_mode = 1;
				params.save_keypoints = false;
				params.img_width = imagePart.cols;
				params.img_height = imagePart.rows;
				KAZE evolution(&params);
				evolution.Set_Detector_Threshold(params.dthreshold2);
				evolution.Create_Nonlinear_Scale_Space(imagePart);
				evolution.Feature_Detection(m_keypoints);
				evolution.Feature_Description(m_keypoints, m_descriptors);
#endif
			} else {
				fail = true;
				return(-1);
			}
			if(i==0)
				m_descriptors = dc; // New descriptor matrix
			else {
				Mat all;
				vconcat(m_descriptors, dc, all); // Add descriptors to the existing matrix
				m_descriptors = all;
			}
			// Shift keypoint locations according to the split
			for(vector<cv::KeyPoint>::iterator iter = kp.begin(); iter!=kp.end(); ++iter) {
				iter->pt.x += start;
				iter->class_id = 0; // validate
			}
			m_keypoints.insert(m_keypoints.end(), kp.begin(), kp.end());
			kp.clear();
			start += len-m_pParams->m_splitOverlap;
		}
	}
	return((int)m_keypoints.size());
}

void Y_LDRDetect::ComputeDescriptors(vector<cv::KeyPoint>& userKeypoints)
{
#if CV_VERSION_MAJOR==3
    Ptr<SIFT> extractor = SIFT::create();
	extractor->compute(*(m_pIm->m_pMat), userKeypoints, m_descriptors);
#else
	SiftDescriptorExtractor extractor;
	extractor(*(m_pIm->m_pMat), Mat(), userKeypoints, m_descriptors, true);
#endif
}

vector<cv::KeyPoint> Y_LDRDetect::ReadKeypointsFile(char* name)
{
	vector<cv::KeyPoint> keypoints;
    FILE *fp;
	if(fopen_s(&fp, name, "r")) {
		return(keypoints);
	}
    keypoints = ReadKeypoints(fp);
	fclose(fp);
	return(keypoints);
}

vector<cv::KeyPoint> Y_LDRDetect::ReadKeypoints(FILE* fp)
{
	float x, y, s, a;
    vector<cv::KeyPoint> keypoints;
	while(fscanf_s(fp, "%f %f %f %f", &x, &y, &s, &a)==4) {
		cv::KeyPoint key(x, y, s, a);
		keypoints.push_back(key);
	}
	return(keypoints);
}

int Y_LDRDetect::WriteKeypointsFile(char* name, const vector<cv::KeyPoint>& keypoints)
{
    FILE *fp;
	if(fopen_s(&fp, name, "w")) {
		return(-1);
	}
    int status = WriteKeypoints(fp, keypoints);
	fclose(fp);
	return(status);
}

int Y_LDRDetect::WriteKeypoints(FILE* fp, const vector<cv::KeyPoint>& keypoints)
{
	for(int i = 0; i<int(keypoints.size()); i++) {
		fprintf(fp, "%10.4f %10.4f %10.4f %10.4f\n", keypoints[i].pt.x, keypoints[i].pt.y, keypoints[i].size, keypoints[i].angle);
	}
	return(0);
}

void Y_LDRDetect::MarkPoint(BMPImageC* pBMPC, KeyPoint key, int radius, Y_RGB<u_char> Pixel)
{
	int I = int(key.pt.x+0.5), J = int(key.pt.y+0.5);
	double d2, r2 = (radius-1)*(radius-1)/4;

	if(radius<2) {
		pBMPC->set_pixel(I, J, Pixel);
	} else {
		for(int j = J-radius; j<=J+radius; j++) {
			for(int i = I-radius; i<=I+radius; i++) {
				d2 = double(i-I)*(i-I)+double(j-J)*(j-J);
				if(d2<=r2) {
					pBMPC->set_pixel(i, j, Pixel);
				}
			}
		}
	}
}

int Y_LDRDetect::SaveMarked(char* name, Y_LDRImage* pIm, int radius, vector<cv::KeyPoint>& keypoints, Y_RGB<u_char> Pixel)
{
	BMPImageC* pBMPC = pIm->ToBMPC();
	for(vector<cv::KeyPoint>::iterator iter = keypoints.begin(); iter!=keypoints.end(); ++iter)
		Y_LDRDetect::MarkPoint(pBMPC, *iter, radius, Pixel);
	pBMPC->Save(name);
	delete pBMPC;
	return(0);
}

