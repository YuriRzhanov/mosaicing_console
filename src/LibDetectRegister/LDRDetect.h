#pragma once

#include "opencv/cv.h"
#if CV_VERSION_MAJOR==3
#include "opencv2/features2d.hpp"
//#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#else
#include "opencv2/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#endif

#include "LDRParams.h"
#include "LDRImage.h"

#define ERR_LEN 1024

class Y_LDRDetect {
public:
	Y_LDRDetect(char* method, Y_LDRImage* pIm, Y_LDRParams* pParams) : 
		fail(false), m_pIm(pIm), m_pParams(pParams), debug(0), fp_log(stderr)
	{
		memcpy(m_method, method, strlen(method));
		fail = false;
		memset(errorDescr, 0, ERR_LEN);
	}
	~Y_LDRDetect()
	{
		m_keypoints.clear();
	}
	void SetDebug(int _debug, FILE* _fp_log) {
		debug = _debug;
		fp_log = _fp_log;
	}
	int DoDetect();
	void ComputeDescriptors(vector<cv::KeyPoint>& keypoints);
	std::vector<cv::KeyPoint> GetKeypoints() { return(m_keypoints); }
	static vector<cv::KeyPoint> ReadKeypointsFile(char* name);
	static vector<cv::KeyPoint> ReadKeypoints(FILE* fp);
	static int WriteKeypointsFile(char* name, const vector<cv::KeyPoint>& keypoints);
	static int WriteKeypoints(FILE* fp, const vector<cv::KeyPoint>& keypoints);

	static void MarkPoint(BMPImageC* pBMPC, KeyPoint k, int radius, Y_RGB<u_char> Pixel);
	static int SaveMarked(char* name, Y_LDRImage* pIm, int radius, vector<cv::KeyPoint>& keypoints, Y_RGB<u_char> Pixel = Y_RGB<u_char>::YellowPixel);
public:
	bool fail;
	char m_method[16];
	Y_LDRImage* m_pIm;
	Y_LDRParams* m_pParams;
	std::vector<cv::KeyPoint> m_keypoints;
	Mat m_descriptors;
	int debug;
	FILE* fp_log;
	static char errorDescr[ERR_LEN];
};
