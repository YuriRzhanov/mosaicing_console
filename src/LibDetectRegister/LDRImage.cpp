#include "LDRImage.h"

using namespace std;
using namespace cv;

Y_LDRImage::Y_LDRImage(int rows, int cols) : fail(false), m_focalLength(-1.0)
{
	m_pMat = new Mat(rows, cols, CV_8UC1, Scalar(0));
	m_pMask = new Mat(rows, cols, CV_8UC1, Scalar(1));
}

Y_LDRImage::Y_LDRImage(BMPImageG* pBMPG) : fail(false)
{
	m_pMat = new Mat(pBMPG->h, pBMPG->w, CV_8UC1);
	//for(int i = 0; i<m_pMat->cols*m_pMat->rows; i++) {
	//	m_pMat->data[i] = pBMPG->im[i];
	//}
	memcpy(m_pMat->data, pBMPG->im, m_pMat->cols*m_pMat->rows);
	m_focalLength = pBMPG->focal_length;
	m_pMask = new Mat(pBMPG->h, pBMPG->w, CV_8UC1, Scalar(1));
	for(int j = 0; j<m_pMask->rows; j++) {
		for(int i = 0; i<m_pMask->cols; i++) {
			if(!pBMPG->valid(i,j))
				m_pMask->at<u_char>(j,i) = 0;
		}
	}
}

Y_LDRImage::Y_LDRImage(IntImageG* pIntG) : fail(false)
{
	m_pMat = new Mat(pIntG->h, pIntG->w, CV_32SC1);
	memcpy(m_pMat->data, pIntG->i_im, m_pMat->cols*m_pMat->rows*sizeof(int));
	m_focalLength = pIntG->focal_length;
	m_pMask = new Mat(pIntG->h, pIntG->w, CV_8UC1, Scalar(1));
	for(int j = 0; j<m_pMask->rows; j++) {
		for(int i = 0; i<m_pMask->cols; i++) {
			if(!pIntG->valid(i,j))
				m_pMask->at<u_char>(j,i) = 0;
		}
	}
}

Y_LDRImage::Y_LDRImage(FltImageG* pFltG) : fail(false)
{
	m_pMat = new Mat(pFltG->h, pFltG->w, CV_32FC1);
	memcpy(m_pMat->data, pFltG->f_im, m_pMat->cols*m_pMat->rows*sizeof(float));
	m_focalLength = pFltG->focal_length;
	m_pMask = new Mat(pFltG->h, pFltG->w, CV_8UC1, Scalar(1));
	for(int j = 0; j<m_pMask->rows; j++) {
		for(int i = 0; i<m_pMask->cols; i++) {
			if(!pFltG->valid(i,j))
				m_pMask->at<u_char>(j,i) = 0;
		}
	}
}

Y_LDRImage::Y_LDRImage(DblImageG* pDblG) : fail(false)
{
	m_pMat = new Mat(pDblG->h, pDblG->w, CV_64FC1);
	memcpy(m_pMat->data, pDblG->d_im, m_pMat->cols*m_pMat->rows*sizeof(double));
	m_focalLength = pDblG->focal_length;
	m_pMask = new Mat(pDblG->h, pDblG->w, CV_8UC1, Scalar(1));
	for(int j = 0; j<m_pMask->rows; j++) {
		for(int i = 0; i<m_pMask->cols; i++) {
			if(!pDblG->valid(i,j))
				m_pMask->at<u_char>(j,i) = 0;
		}
	}
}

Y_LDRImage::~Y_LDRImage()
{
	delete m_pMat;
	delete m_pMask;
}

Y_LDRImage* Y_LDRImage::Copy()
{
	Y_LDRImage* pImgCopy = new Y_LDRImage;
	pImgCopy->fail = fail;
	m_pMat->copyTo(*(pImgCopy->m_pMat));
	pImgCopy->m_focalLength = m_focalLength;
	m_pMask->copyTo(*(pImgCopy->m_pMask));
	return(pImgCopy);
}

BMPImageG* Y_LDRImage::ToBMPG()
{
	BMPImageG* pBMPG = NULL;
	if(m_pMat->type()==CV_8UC1) {
		pBMPG = new BMPImageG(m_pMat->cols, m_pMat->rows);
		memcpy(pBMPG->im, m_pMat->data, m_pMat->cols*m_pMat->rows);
	} else if(m_pMat->type()==CV_32SC1) {
		IntImageG* pIntG = new IntImageG(m_pMat->cols, m_pMat->rows, (int*)m_pMat->data);
		pBMPG = pIntG->Greyscale(1);
		delete pIntG;
	} else if(m_pMat->type()==CV_32FC1) {
		FltImageG* pFltG = new FltImageG(m_pMat->cols, m_pMat->rows, (float*)m_pMat->data);
		pBMPG = pFltG->Greyscale(1);
		delete pFltG;
	} else if(m_pMat->type()==CV_64FC1) {
		DblImageG* pDblG = new DblImageG(m_pMat->cols, m_pMat->rows, (double*)m_pMat->data);
		pBMPG = pDblG->Greyscale(1);
		delete pDblG;
	}
	return(pBMPG);
}

BMPImageC* Y_LDRImage::ToBMPC()
{
	BMPImageG* pBMPG = ToBMPG();
	BMPImageC* pBMPC = new BMPImageC(pBMPG);
	delete pBMPG;
	return(pBMPC);
}

int Y_LDRImage::SaveBMP(char* name)
{
	BMPImageG* pBMPG = ToBMPG();
	pBMPG->Save(name);
	delete pBMPG;
	return(0);
}

