#pragma once

// Encapsulation of Image structure used in OpenCV
#include "BMPImageG.h"
#include "IntImageG.h"
#include "FltImageG.h"
#include "DblImageG.h"
#include "opencv2/core/core.hpp"

using namespace cv;

class Y_LDRImage {
public:
	Y_LDRImage() { m_pMat = nullptr; }
	Y_LDRImage(int rows, int cols);
	Y_LDRImage(BMPImageG* pBMPG);
	Y_LDRImage(IntImageG* pIntG);
	Y_LDRImage(FltImageG* pFltG);
	Y_LDRImage(DblImageG* pDblG);
	~Y_LDRImage();
	Y_LDRImage* Copy(); // new copy
	BMPImageG* ToBMPG();
	BMPImageC* ToBMPC();
	int SaveBMP(char* name);
	bool valid(int i, int j) { 
		return(i>=0 && i<m_pMat->cols && j>=0 && j<m_pMat->rows && m_pMat->at<u_char>(j,i)!=0); 
	}
public:
	Mat* m_pMat;
	Mat* m_pMask;
	bool fail;
	double m_focalLength;
};
