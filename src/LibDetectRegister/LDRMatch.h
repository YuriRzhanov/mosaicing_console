#pragma once

//#include "opencv2/core/core.hpp"
//#include "opencv2/features2d/features2d.hpp"
//#include "opencv2/nonfree/nonfree.hpp"
//#include "opencv2/imgproc/imgproc.hpp"

#include "LDRDetect.h"
#include "YMatch.h"

class Y_LDRMatch {
public:
	Y_LDRMatch(Y_LDRDetect* pDetect1, Y_LDRDetect* pDetect2, char* method = "BF") : fail(false)
	{
		vector<vector<DMatch> > matches;
		if(strcmp(method, "BF")==0) {
			//BFMatcher matcher(NORM_L2, true); // 'true' allows for cross-match
			BFMatcher matcher(NORM_L2);
			matcher.knnMatch(pDetect1->m_descriptors, pDetect2->m_descriptors, matches, 2);
		} else if(strcmp(method, "Flann")==0) {
			FlannBasedMatcher matcher;
			matcher.knnMatch(pDetect1->m_descriptors, pDetect2->m_descriptors, matches, 2);
		} else {
			fail = true;
			return;
		}
		for(vector<vector<DMatch> >::iterator iter = matches.begin(); iter!=matches.end(); ++iter) {
			vector<DMatch> match = *iter;
			if(match.size()>1) 
			{
				if(match[0].distance < pDetect1->m_pParams->m_ratioThreshold*match[1].distance) {
					if(pDetect1->m_keypoints[match[0].queryIdx].class_id!=-1 && 
							pDetect2->m_keypoints[match[0].trainIdx].class_id!=-1) {
						Y_Match _match;
						_match.SetLeft(pDetect1->m_keypoints[match[0].queryIdx].pt.x, pDetect1->m_keypoints[match[0].queryIdx].pt.y);
						_match.SetRight(pDetect2->m_keypoints[match[0].trainIdx].pt.x, pDetect2->m_keypoints[match[0].trainIdx].pt.y);
						_match.score = match[0].distance; _match.good = true;
						m_matches.push_back(_match);
					}
				}
			}
		}
	}
	~Y_LDRMatch() {}
public:
	//char m_method[16];
	vector<Y_Match> m_matches;
	bool fail;
};
