#pragma once

#ifdef USE_DAISY
#include "daisy/daisy.h"
#endif

#ifdef USE_KAZE
#include "opencv/ml.hpp" // Needs to be included to avoid problems with KAZE!
#include "KAZE.h"
#endif

class Y_LDRParams {
public:
	Y_LDRParams() : m_nOctaveLayers(3), m_contrastThreshold(0.04), m_edgeThreshold(10.0), m_sigma(1.6)
	, m_hessianThreshold(400.0), m_octaves(5), m_nFeatures(100), m_initFeatureScale(1.0f)
	, m_featureScaleLevels(1), m_featureScaleMul(0.1f), m_initXyStep(6), m_initImgBound(0)
	, m_varyXyStepWithScale(true), m_varyImgBoundWithScale(false)
#ifdef USE_DAISY
	, m_rad(15), m_radq(3), m_thq(8), m_histq(8), m_nrm_type(daisy::NRM_PARTIAL)
#endif

	, m_ratioThreshold(0.7), m_allowedProximity(4), m_RANSACTolerance(4.0), m_splitFactor(1), m_splitOverlap(30), m_use_ANN(false)
	{}
public:
	int m_nOctaveLayers; // SIFT, SURF
	double m_contrastThreshold; // SIFT
	double m_edgeThreshold; // SIFT
	double m_sigma; // SIFT
	double m_ratioThreshold; // SIFT
	double m_hessianThreshold; // SURF
	int m_octaves; // SURF
	size_t m_nFeatures; // ORB
	float m_initFeatureScale; // DENSE
	int m_featureScaleLevels; // DENSE
	float m_featureScaleMul; // DENSE
	int m_initXyStep; // DENSE
	int m_initImgBound; // DENSE
	bool m_varyXyStepWithScale; // DENSE
	bool m_varyImgBoundWithScale; // DENSE
#ifdef USE_DAISY
	int m_rad; // DAISY
	int m_radq; // DAISY
	int m_thq; // DAISY
	int m_histq; // DAISY
	int m_nrm_type; // DAISY
#endif

	int m_allowedProximity; // How close to boundaries keypoints are allowed
	double m_RANSACTolerance; // how close should be points to the model
	bool m_use_ANN; // If 'true', use Approximate nearest neighbor scheme. If 'false', use brute force matching.
	int m_splitFactor; // If !=1, original images are split into parts and
		// processed independently. Keypoints are then merged and matches are
		// searched for globally.
	int m_splitOverlap; // split images overlap by this number of pixels.
		// I.e. image #0 extends to the right by splitOverlap/2
		// Image #1 starts earlier by splitOverlap/2, and is by splitOverlap pixels wider, etc.
};

