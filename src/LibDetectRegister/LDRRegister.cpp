#include "LDRRegister.h"

using namespace std;
using namespace cv;

double OverlapPolygons(Hmgr& hm, Y_Image* im1, Y_Image* im2);

char Y_LDRRegister::name[NAME_LEN];
char Y_LDRRegister::errorDescr[ERR_LEN];

Y_LDRRegister::Y_LDRRegister(char* method, Y_LDRImage* _pIm1, Y_LDRImage* _pIm2, Y_LDRParams* _params) :
	m_imgType((Y_Image::IMAGE_TYPE)999), m_pIm1(_pIm1), m_pIm2(_pIm2), m_pBMP1(nullptr), m_pBMP2(nullptr), 
	m_pInt1(nullptr), m_pInt2(nullptr), m_pFlt1(nullptr), m_pFlt2(nullptr), m_pDbl1(nullptr), m_pDbl2(nullptr), 
	m_pIm(nullptr), m_pParams(_params), fail(false), debug(0), 
	fp_log(stderr), currentDir(nullptr), perspectiveModel(false),
	m_pCol(nullptr)
{
	memset(m_method, 0, 16);
	memcpy(m_method, method, strlen(method));
	memset(errorDescr, 0, ERR_LEN);
	if(m_pIm1->m_pMat->cols!=m_pIm2->m_pMat->cols || m_pIm1->m_pMat->rows!=m_pIm2->m_pMat->rows) {
		if(fp_log) fprintf(fp_log, "Images have different dimensions?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different dimensions?...");
		fail = true;
		return;
	}
	m_pIm = new Y_Image(m_pIm1->m_pMat->cols, m_pIm1->m_pMat->rows);
	m_pIm->SetFocalLength(m_pIm1->m_focalLength);
}

Y_LDRRegister::Y_LDRRegister(char* method, BMPImageG* _pBMP1, BMPImageG* _pBMP2, Y_LDRParams* _params) :
	m_imgType(Y_Image::BMP_TYPE), m_pIm1(nullptr), m_pIm2(nullptr), m_pBMP1(_pBMP1), m_pBMP2(_pBMP2), 
	m_pInt1(nullptr), m_pInt2(nullptr), m_pFlt1(nullptr), m_pFlt2(nullptr), m_pDbl1(nullptr), m_pDbl2(nullptr), 
	m_pIm(nullptr), m_pParams(_params), fail(false), debug(0), 
	fp_log(stderr), currentDir(nullptr), perspectiveModel(false),
	m_pCol(nullptr)
{
	memcpy(m_method, method, strlen(method));
	memset(errorDescr, 0, ERR_LEN);
	if(!m_pBMP1 || !m_pBMP2) {
		if(fp_log) fprintf(fp_log, "Images are not set?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images are not set?...");
		fail = true;
		return;
	}
	if(m_pBMP1->w!=m_pBMP2->w || m_pBMP1->h!=m_pBMP2->h) {
		if(fp_log) fprintf(fp_log, "Images have different dimensions?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different dimensions?...");
		fail = true;
		return;
	}
	m_pIm = new Y_Image(m_pBMP1->w, m_pBMP1->h);
	if(m_pBMP1->focal_length <= 0.0) {
		if(fp_log) fprintf(fp_log, "Images have no focal length?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have no focal length?...");
		fail = true;
		return;
	}
	if(!EPSILON_EQ(m_pBMP1->focal_length,m_pBMP2->focal_length)) {
		if(fp_log) fprintf(fp_log, "Images have different focal lengths?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different focal lengths?...");
		fail = true;
		return;
	}
	m_pIm->SetFocalLength(m_pBMP1->focal_length);
}

Y_LDRRegister::Y_LDRRegister(char* method, IntImageG* _pInt1, IntImageG* _pInt2, Y_LDRParams* _params) :
	m_imgType(Y_Image::INT_TYPE), m_pIm1(nullptr), m_pIm2(nullptr), m_pBMP1(nullptr), m_pBMP2(nullptr), 
	m_pInt1(_pInt1), m_pInt2(_pInt2), m_pFlt1(nullptr), m_pFlt2(nullptr), m_pDbl1(nullptr), m_pDbl2(nullptr), 
	m_pIm(nullptr), m_pParams(_params), fail(false), debug(0), 
	fp_log(stderr), currentDir(nullptr), perspectiveModel(false),
	m_pCol(nullptr)
{
	memcpy(m_method, method, strlen(method));
	memset(errorDescr, 0, ERR_LEN);
	if(!m_pInt1 || !m_pInt2) {
		if(fp_log) fprintf(fp_log, "Images are not set?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images are not set?...");
		fail = true;
		return;
	}
	if(m_pInt1->w!=m_pInt2->w || m_pInt1->h!=m_pInt2->h) {
		if(fp_log) fprintf(fp_log, "Images have different dimensions?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different dimensions?...");
		fail = true;
		return;
	}
	m_pIm = new Y_Image(m_pInt1->w, m_pInt1->h);
	if(m_pInt1->focal_length <= 0.0) {
		if(fp_log) fprintf(fp_log, "Images have no focal length?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have no focal length?...");
		fail = true;
		return;
	}
	if(!EPSILON_EQ(m_pInt1->focal_length,m_pInt2->focal_length)) {
		if(fp_log) fprintf(fp_log, "Images have different focal lengths?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different focal lengths?...");
		fail = true;
		return;
	}
	m_pIm->SetFocalLength(m_pInt1->focal_length);
}

Y_LDRRegister::Y_LDRRegister(char* method, FltImageG* _pFlt1, FltImageG* _pFlt2, Y_LDRParams* _params) :
	m_imgType(Y_Image::FLT_TYPE), m_pIm1(nullptr), m_pIm2(nullptr), m_pBMP1(nullptr), m_pBMP2(nullptr), 
	m_pInt1(nullptr), m_pInt2(nullptr), m_pFlt1(_pFlt1), m_pFlt2(_pFlt2), m_pDbl1(nullptr), m_pDbl2(nullptr),
	m_pIm(nullptr), m_pParams(_params), fail(false), debug(0), 
	fp_log(stderr), currentDir(nullptr), perspectiveModel(false),
	m_pCol(nullptr)
{
	memcpy(m_method, method, strlen(method));
	memset(errorDescr, 0, ERR_LEN);
	if(!m_pFlt1 || !m_pFlt2) {
		if(fp_log) fprintf(fp_log, "Images are not set?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images are not set?...");
		fail = true;
		return;
	}
	if(m_pFlt1->w!=m_pFlt2->w ||m_pFlt1->h!=m_pFlt2->h) {
		if(fp_log) fprintf(fp_log, "Images have different dimensions?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different dimensions?...");
		fail = true;
		return;
	}
	m_pIm = new Y_Image(m_pFlt1->w,m_pFlt1->h);
	if(m_pFlt1->focal_length <= 0.0) {
		if(fp_log) fprintf(fp_log, "Images have no focal length?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have no focal length?...");
		fail = true;
		return;
	}
	if(!EPSILON_EQ(m_pFlt1->focal_length,m_pFlt2->focal_length)) {
		if(fp_log) fprintf(fp_log, "Images have different focal lengths?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different focal lengths?...");
		fail = true;
		return;
	}
	m_pIm->SetFocalLength(m_pFlt1->focal_length);
}

Y_LDRRegister::Y_LDRRegister(char* method, DblImageG* _pDbl1, DblImageG* _pDbl2, Y_LDRParams* _params) :
	m_imgType(Y_Image::DBL_TYPE), m_pIm1(nullptr), m_pIm2(nullptr), m_pBMP1(nullptr), m_pBMP2(nullptr), 
	m_pInt1(nullptr), m_pInt2(nullptr), m_pFlt1(nullptr), m_pFlt2(nullptr), m_pDbl1(_pDbl1), m_pDbl2(_pDbl2),
	m_pIm(nullptr), m_pParams(_params), fail(false), debug(0), 
	fp_log(stderr), currentDir(nullptr), perspectiveModel(false),
	m_pCol(nullptr)
{
	memcpy(m_method, method, strlen(method));
	memset(errorDescr, 0, ERR_LEN);
	if(!m_pDbl1 || !m_pDbl2) {
		if(fp_log) fprintf(fp_log, "Images are not set?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images are not set?...");
		fail = true;
		return;
	}
	if(m_pDbl1->w!=m_pDbl2->w || m_pDbl1->h!=m_pDbl2->h) {
		if(fp_log) fprintf(fp_log, "Images have different dimensions?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different dimensions?...");
		fail = true;
		return;
	}
	m_pIm = new Y_Image(m_pDbl1->w, m_pDbl1->h);
	if(m_pDbl1->focal_length <= 0.0) {
		if(fp_log) fprintf(fp_log, "Images have no focal length?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have no focal length?...");
		fail = true;
		return;
	}
	if(m_pDbl1->focal_length != m_pDbl1->focal_length) {
		if(fp_log) fprintf(fp_log, "Images have different focal lengths?...\n");
		sprintf_s(errorDescr, ERR_LEN, "Images have different focal lengths?...");
		fail = true;
		return;
	}
	m_pIm->SetFocalLength(m_pDbl1->focal_length);
}

Y_LDRRegister::~Y_LDRRegister()
{
	if(m_pIm) delete m_pIm;
	if(m_pCol) delete m_pCol;
	// Yuri: 8/11/2017
	if(m_pIm1) delete m_pIm1;
	if(m_pIm2) delete m_pIm2;
	m_matches.clear();
}

void Y_LDRRegister::SetDebug(int _debug, FILE* _fp_log, char* _currentDir)
{
	debug = _debug;
	fp_log = _fp_log;
	if(debug) {
		if(_currentDir) currentDir = _currentDir;
		else currentDir = ".";
	}
}

void Y_LDRRegister::SetPerspective(bool _perspectiveModel)
{
	perspectiveModel = _perspectiveModel;
}

int Y_LDRRegister::DoRegister(char* matchMethod)
{
	try {
		if(m_imgType==Y_Image::UNKNOWN_TYPE) {
			int kkk = 0;
		} else if(m_imgType==Y_Image::BMP_TYPE) {
			m_pIm1 = new Y_LDRImage(m_pBMP1);
			m_pIm2 = new Y_LDRImage(m_pBMP2);
		} else if(m_imgType==Y_Image::INT_TYPE) {
			m_pIm1 = new Y_LDRImage(m_pInt1);
			m_pIm2 = new Y_LDRImage(m_pInt2);
		} else if(m_imgType==Y_Image::FLT_TYPE) {
			m_pIm1 = new Y_LDRImage(m_pFlt1);
			m_pIm2 = new Y_LDRImage(m_pFlt2);
		} else if(m_imgType==Y_Image::DBL_TYPE) {
			m_pIm1 = new Y_LDRImage(m_pDbl1);
			m_pIm2 = new Y_LDRImage(m_pDbl2);
		} else if(m_imgType==999) { // Native format
			int kkk = 0;
		} else {
			strcpy_s(errorDescr, ERR_LEN, "Unknown image type?...\n");
			fail = true;
			return(-1);
		}
	} catch(std::bad_alloc& ba) {
		fail = true;
		sprintf_s(errorDescr, ERR_LEN, "Bad allocation: %s", ba.what());
		return(-1);
	}

	Y_LDRDetect* pLDRDetect1 = new Y_LDRDetect(m_method, m_pIm1, m_pParams);
	pLDRDetect1->SetDebug(debug, fp_log);
	pLDRDetect1->DoDetect();
	m_keypoints1 = pLDRDetect1->GetKeypoints();
	if(perspectiveModel) {
		if(m_keypoints1.size()<4)
		{
			fail = true;
			return(0);
		}
	}
	else
	{
		if(m_keypoints1.size()<2)
		{
			fail = true;
			return(0);
		}
	}
	Y_LDRDetect* pLDRDetect2 = new Y_LDRDetect(m_method, m_pIm2, m_pParams);
	pLDRDetect2->SetDebug(debug, fp_log);
	pLDRDetect2->DoDetect();
	m_keypoints2 = pLDRDetect2->GetKeypoints();
	if(perspectiveModel) {
		if(m_keypoints2.size()<4)
		{
			fail = true;
			return(0);
		}
	}
	else
	{
		if(m_keypoints2.size()<2)
		{
			fail = true;
			return(0);
		}
	}

	// Discard keypoints too close to image boundaries (that can be different 
	// from the rectangle bounds!)
	DiscardBoundaryKeypoints(m_keypoints1, m_pIm1, m_pParams->m_allowedProximity);
	DiscardBoundaryKeypoints(m_keypoints2, m_pIm2, m_pParams->m_allowedProximity);

	if(debug && fp_log) {
		fprintf(fp_log, "Num: %d\n", int(m_keypoints1.size()));
		fprintf(fp_log, "Num: %d\n", int(m_keypoints2.size()));
	}
	if(debug) {
		sprintf_s(name, NAME_LEN, "%s/keys1.%s.txt", currentDir, m_method);
		Y_LDRDetect::WriteKeypointsFile(name, m_keypoints1);
		sprintf_s(name, NAME_LEN, "%s/keys2.%s.txt", currentDir, m_method);
		Y_LDRDetect::WriteKeypointsFile(name, m_keypoints2);
		sprintf_s(name, NAME_LEN, "%s/AllPoints-1-%s.bmp", currentDir, m_method);
		Y_LDRDetect::SaveMarked(name, m_pIm1, 5, m_keypoints1);
		sprintf_s(name, NAME_LEN, "%s/AllPoints-2-%s.bmp", currentDir, m_method);
		Y_LDRDetect::SaveMarked(name, m_pIm2, 5, m_keypoints2);
	}

	Y_LDRMatch LDRMatch(pLDRDetect1, pLDRDetect2, matchMethod);
	m_matches = LDRMatch.m_matches;
	if(debug) {
		sprintf_s(name, NAME_LEN, "%s/AllMatches-%s.txt", currentDir, m_method);
		WriteMatchFile(name, m_matches);
		sprintf_s(name, NAME_LEN, "%s/AllMatches-1-%s.bmp", currentDir, m_method);
		SaveMatchesMarkedNumbered(1, name, m_pIm1, 5, m_matches);
		sprintf_s(name, NAME_LEN, "%s/AllMatches-2-%s.bmp", currentDir, m_method);
		SaveMatchesMarkedNumbered(2, name, m_pIm2, 5, m_matches);
		sprintf_s(name, NAME_LEN, "%s/MatchesVectors-%s.bmp", currentDir, m_method);
		SaveMatchesVectors(name, 5, m_matches);
	}
	delete pLDRDetect1;
	delete pLDRDetect2;
	return(int(m_matches.size()));
}

void Y_LDRRegister::DiscardBoundaryKeypoints(vector<KeyPoint>& keypoints, Y_LDRImage* pIm, int prox)
{
	int i, j;
	double dist2_to_invalid, dist2;

	for(int n = 0; n<(int)keypoints.size(); n++) {
		cv::KeyPoint k = keypoints[n];
		if(k.pt.x<prox || k.pt.x>(pIm->m_pMat->cols-prox-1) || k.pt.y<prox || k.pt.y>(pIm->m_pMat->rows-prox-1))
			k.class_id = -1; // invalidate
		else { // Find closest invalid pixel
			dist2_to_invalid = 1000000.0;
			for(j = (int)k.pt.y-prox; j<=(int)k.pt.y+prox; j++) {
				for(i = (int)k.pt.x-prox; i<=(int)k.pt.x+prox; i++) {
					if(pIm->valid(i,j)==false) {
						dist2 = (k.pt.x-i)*(k.pt.x-i)+(k.pt.y-j)*(k.pt.y-j);
						if(dist2<dist2_to_invalid)
							dist2_to_invalid = dist2;
					}
				}
			}
			if(dist2_to_invalid<double(prox*prox))
				k.class_id = -1; // invalidate
		}
	}
}

// Do RANSAC for specified model, find correct matches, then use them for homography determination
bool Y_LDRRegister::GetHmgr(Hmgr* pHm)
{
	if(m_pIm->focal_length <= 0.0) // Warn
		if(fp_log) fprintf(fp_log, "Focal length has not been set!...\n");
	int N = int(m_matches.size());
	if(perspectiveModel) {
		if(N<8)
			return(fail=true);
	} else {
		if(N<4)
			return(fail=true);
	}
	m_pCol = new Collocations(m_pIm->w, m_pIm->h, m_pIm->focal_length);

	fail = RANSAC_and_MLE(m_pParams->m_RANSACTolerance, m_pIm, *pHm, m_pCol);

	return(fail);
}

void Y_LDRRegister::GetRRA(RARecord* pRRA, int fn1, int fn2, double maxScale)
{
	fail = GetHmgr(&m_hm);
	if(!fail) {
		pRRA->SetNums(fn1, fn2);
		pRRA->MarkSet("s");
		double zoom = 1.0, dx = 0.0, dy = 0.0;
		double Y = (m_hm.h(1,1)<0.0) ? M_PI : 0.0;
		double hm_error = m_hm.RigidAffineParams(zoom, Y, dx, dy);
		pRRA->Set(zoom, Rad2Deg(Y), dx*m_pIm->focal_length, dy*m_pIm->focal_length);
		// Zoom is compared with the allowed scale and the record is marked as bad, if it's outside the range:
		if(zoom>1.0) {
			if(zoom > maxScale) {
				pRRA->MarkSet("b");
				pRRA->zoom = 1.0; // to make display easier
				fail = true;
			}
		} else {
			if(zoom < 1.0/maxScale) {
				pRRA->MarkSet("b");
				pRRA->zoom = 1.0; // to make display easier
				fail = true;
			}
		}
		// If shift it too large, reduce it
		if(pRRA->x_shift<-m_pIm->w || pRRA->x_shift>m_pIm->w) {
			pRRA->MarkSet("b");
			pRRA->x_shift = 0.0;
			fail = true;
		}
		if(pRRA->y_shift<-m_pIm->h || pRRA->y_shift>m_pIm->h) {
			pRRA->MarkSet("b");
			pRRA->y_shift = 0.0;
			fail = true;
		}
		pRRA->overlap_percent = OverlapPolygons(m_hm, m_pIm, m_pIm);
		//m_pCol->SetToRAR(*pRRA);
		if(fail) {
			if(m_pCol) {
				delete m_pCol;
				m_pCol = nullptr;
			}
			pRRA->n_col = 0;
		} else {
			pRRA->SetCollocations(m_pCol);
		}
	} else { //create dummy RARecord
		pRRA->SetNums(fn1, fn2);
		pRRA->MarkSet("b");
		pRRA->Set(1.0, 0.0, 0.0, 0.0);
		pRRA->n_col = 0; // No collocations
		if(m_pCol) {
			delete m_pCol;
			m_pCol = nullptr;
		}
		fail = true;
	}
}

void Y_LDRRegister::GetTransform(TransformRecord* pTransform, int fn1, int fn2, double maxScale)
{
	fail = GetHmgr(&m_hm);
	if(!fail) {
		pTransform->SetAttr(fn1, fn2);
		pTransform->MarkSet("s");
		pTransform->SetHmgr(m_hm);
		// maxScale check?
		pTransform->overlap_percent = OverlapPolygons(m_hm, m_pIm, m_pIm);
		//m_pCol->SetToTransform(*pTransform);
		pTransform->SetCollocations(m_pCol);
	} else { //create dummy TransformRecord
		Hmgr dummy_hm;
		pTransform->SetAttr(fn1, fn2);
		pTransform->MarkSet("b");
		pTransform->SetHmgr(dummy_hm);
		pTransform->overlap_percent = 0.0;
		pTransform->n_col = 0; // No collocations
		if(m_pCol) {
			delete m_pCol;
			m_pCol = nullptr;
		}
		fail = true;
	}
}

bool Y_LDRRegister::RANSAC_and_MLE(double tolerance, Y_Image* pIm, Hmgr& hm, Collocations* pCol)
{
	bool success = false;
	Hmgr ransac_best_hm;
	int n_good_matches = 0;
	if(!perspectiveModel)
		success = RANSAC_RigidAffine(m_matches, pIm, tolerance, ransac_best_hm, n_good_matches, debug);
	else
		success = RANSAC_Perspective(m_matches, pIm, tolerance, ransac_best_hm, n_good_matches, debug);
	int Nm = int(m_matches.size());
	if(success) {
		Coord2* c1 = new Coord2[Nm];
		Coord2* c2 = new Coord2[Nm];

		for(int i = 0, k = 0; k<Nm; k++) {
			Y_Match* match = &m_matches[k];
			if(match->good) { // Use k-th pair for i-th coord's
				c1[i].Set(match->left.x, match->left.y);
				c2[i].Set(match->right.x, match->right.y);
				match->mark = 'G';
				i++;
			} else {
				match->mark = 'B';
			}
		}
		if(!perspectiveModel) {
			hm = RigidAffineFromMatches(c1, c2, n_good_matches, pIm);
			ChooseCollocations(m_matches, Nm, pIm, hm, MIN(3, n_good_matches), pCol);
		} else {
			hm = PerspectiveFromMatches(c1, c2, n_good_matches, pIm);
			ChooseCollocations(m_matches, Nm, pIm, hm, MIN(5, n_good_matches), pCol);
		}
		CalculateStats(c1, c2, n_good_matches, pIm, hm); // Some idea about the registration quality

		delete [] c1;
		delete [] c2;

		//if(debug) {
		//	sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches.txt", currentDir);
		//	Y_SIFTKeyMatch::Y_WriteMatchFileDetail(name, m_matches);
		//	BMPImageC* pBMPC;
		//	if(m_imgType==Y_Image::BMP_TYPE) {
		//		pBMPC = new BMPImageC(m_pBMP1);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-1.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 1);
		//		delete pBMPC;
		//		pBMPC = new BMPImageC(m_pBMP2);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-2.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 2);
		//		delete pBMPC;
		//	} else if(m_imgType==Y_Image::INT_TYPE) {
		//		BMPImageG* pBMPG = m_pInt1->Greyscale(1);
		//		pBMPC = new BMPImageC(pBMPG);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-1.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 1);
		//		delete pBMPG;
		//		delete pBMPC;
		//		pBMPG = m_pInt2->Greyscale(1);
		//		pBMPC = new BMPImageC(pBMPG);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-2.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 2);
		//		delete pBMPG;
		//		delete pBMPC;
		//	} else if(m_imgType==Y_Image::FLT_TYPE) {
		//		BMPImageG* pBMPG = m_pFlt1->Greyscale(1);
		//		pBMPC = new BMPImageC(pBMPG);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-1.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 1);
		//		delete pBMPG;
		//		delete pBMPC;
		//		pBMPG = m_pFlt2->Greyscale(1);
		//		pBMPC = new BMPImageC(pBMPG);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-2.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 2);
		//		delete pBMPG;
		//		delete pBMPC;
		//	} else if(m_imgType==Y_Image::DBL_TYPE) {
		//		BMPImageG* pBMPG = m_pDbl1->Greyscale(1);
		//		pBMPC = new BMPImageC(pBMPG);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-1.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 1);
		//		delete pBMPG;
		//		delete pBMPC;
		//		pBMPG = m_pDbl2->Greyscale(1);
		//		pBMPC = new BMPImageC(pBMPG);
		//		sprintf_s(name, NAME_SIFT_LEN, "%s/GoodMatches-2.bmp", currentDir);
		//		SaveMatchesMarkedNumbered(pBMPC, name, 5, m_matches, 2);
		//		delete pBMPG;
		//		delete pBMPC;
		//	}
		//}
	}
	return(!success);
}

// Transform is estimated using adaptive RANSAC.
// Each time 2 random matches are chosen, solution is built
// and number of inliers is estimated.
// Adaptive RANSAC (Hartley/Zisserman slides)
bool Y_LDRRegister::RANSAC_RigidAffine(vector<Y_Match>& matches, Y_Image* pIm, 
	double Tol, Hmgr& hm, int& n_good_matches, int debug)
{
	int i;
	int Nm = int(matches.size()), N0 = 1000000, sample_count = 0, max_inliers = 0, best_inliers;
	double p = 0.99; // probability
	int s = 2, rnd[2]; // sample size
	int cols = 9, rows = s; 
	int outer_nr_of_tries = 0;
	Coord2 C1, C2, tc, c1[2], c2[2];
	double sse, best_sse = 0.0; // Minimum squared error
	Hmgr best_hm;
	NEWMAT::ColumnVector best_errors(Nm); // Best errors
	//NEWMAT::ColumnVector C(s);
	Y_Match* match_set[2], *match;

	C1.SetImage(pIm);
	C2.SetImage(pIm);
	tc.SetImage(pIm);

	while(5*N0>sample_count || sample_count<10) {
		// Choose a sample and count # of inliers

		// Choose s points at random, making sure that the same points do not appear twice.
		// Occasionally, points can be wrongly matched to the only point, and the loop goes forever.
		// Break it if # of tries is > 5*#-of-matches
		bool appear_twice = true;
		int nr_of_tries = 0;
		do {
			nr_of_tries++;
			//C = RandomSet(s, Nm);
			Y_Utils::randomSet(s, Nm, rnd);
			match_set[0] = &matches[rnd[0]];
			match_set[1] = &matches[rnd[1]];
			appear_twice = FoundSameMatch(match_set, 2);
			if(nr_of_tries>=2*Nm) // Something is seriously wrong
				return(false);
		} while(appear_twice);

		for(i = 0; i<s; i++) {
			match = &matches[rnd[i]];
			c1[i].Set(match->left.x, match->left.y);
			c2[i].Set(match->right.x, match->right.y);
		}
		Hmgr hm = RigidAffineFromMatches(c1, c2, 2, pIm);
		if(hm.IsSingular()) {
			outer_nr_of_tries++;
			if(outer_nr_of_tries>10) // Cannot construct non-singular matrix!!!???
				return(false);
			continue; // probably is bad anyway
		}
		Hmgr inv_hm = hm.Invert();

		// Calculate errors and # of inliers for the solution
		NEWMAT::ColumnVector errors(Nm);
		int inliers = 0;
		for(i = 0; i<Nm; i++) {
			match = &matches[i];
			C1.Set(match->left.x, match->left.y);
			C2.Set(match->right.x, match->right.y);
			tc = inv_hm.Map(C1);
			errors(i+1) = C2.DDistance(tc);
			if(errors(i+1) < Tol)
				inliers++;
			else
				errors(i+1) = Tol;
		}
		//cout<<"errors:"<<endl<<setw(10)<<setprecision(7)<<errors.t();
		sse = NEWMAT::DotProduct(errors, errors);
		if(sample_count==0 || best_sse>sse) { // very first, just use it
			best_hm = hm;
			best_sse = sse;
			best_errors = errors;
			best_inliers = inliers;
		}
		// Monitor progress
		if(inliers > max_inliers) {
			max_inliers = inliers;
			// Reestimate number of trials needed
			double epsilon = 1.0-double(max_inliers)/Nm;
			N0 = int(log(1.0-p)/log(1.0-pow(1.0-epsilon, double(s))));
		}
		sample_count++;

		//printf("%d  %d (%d)    %d\n", sample_count, inliers, max_inliers, N0);
	}
	Hmgr inv_best_hm = best_hm.Invert();
	
	if(debug && fp_log) fprintf(fp_log, "Iter=%d (Pts=%d In=%d) Needed=%d\n", sample_count, Nm, max_inliers, N0);

	// Set the outliers
	int outlier = 0;
	double error;
	for(int i = 0; i<Nm; i++) {
		match = &matches[i];
		C1.Set(match->left.x, match->left.y);
		C2.Set(match->right.x, match->right.y);
		tc = inv_best_hm.Map(C1);
		error = C2.DDistance(tc);
		if(error<Tol) {
			match->good = true;
			match->mark = 'G';
			n_good_matches++;
		} else {
			match->good = false;
			match->mark = 'B';
		}
	}

	hm = best_hm;
	return(true);
}

// Transform is estimated using adaptive RANSAC.
// Each time 4 random matches are chosen, solution is built
// and number of inliers is estimated.
// Adaptive RANSAC (Hartley/Zisserman slides)
bool Y_LDRRegister::RANSAC_Perspective(vector<Y_Match>& matches, Y_Image* pIm, 
	double Tol, Hmgr& hm, int& n_good_matches, int debug)
{
	int i;
	int Nm = (int)matches.size(), N0 = 1000000, sample_count = 0, max_inliers = 0, best_inliers;
	double p = 0.99; // probability
	int s = 4, rnd[4]; // sample size
	int cols = 9, rows = s; 
	int outer_nr_of_tries = 0;
	Coord2 C1, C2, tc, c1[4], c2[4];
	double sse, best_sse = 0.0; // Minimum squared error
	Hmgr best_hm;
	NEWMAT::ColumnVector best_errors(Nm); // Best errors
	//NEWMAT::ColumnVector C(s);
	Y_Match* match_set[4], *match;

	C1.SetImage(pIm);
	C2.SetImage(pIm);
	tc.SetImage(pIm);

	while(5*N0>sample_count || sample_count<10) {
		// Choose a sample and count # of inliers

		// Choose s points at random, making sure that the same points do not appear twice.
		// Occasionally, points can be wrongly matched to the only point, and the loop goes forever.
		// Break it if # of tries is > 5*#-of-matches
		bool appear_twice = true;
		int nr_of_tries = 0;
		do {
			nr_of_tries++;
			//C = RandomSet(s, Nm);
			Y_Utils::randomSet(s, Nm, rnd);
			match_set[0] = &matches[rnd[0]];
			match_set[1] = &matches[rnd[1]];
			match_set[2] = &matches[rnd[2]];
			match_set[3] = &matches[rnd[3]];
			appear_twice = FoundSameMatch(match_set, 4);
			if(nr_of_tries>=2*Nm) // Something is seriously wrong
				return(false);
		} while(appear_twice);

		for(i = 0; i<s; i++) {
			match = &matches[rnd[i]];
			c1[i].Set(match->left.x, match->left.y);
			c2[i].Set(match->right.x, match->right.y);
		}
		Hmgr hm = PerspectiveFromMatches(c1, c2, 4, pIm);
		if(hm.IsSingular()) {
			outer_nr_of_tries++;
			if(outer_nr_of_tries>10) // Cannot construct non-singular matrix!!!???
				return(false);
			continue; // probably is bad anyway
		}
		Hmgr inv_hm = hm.Invert();

		// Calculate errors and # of inliers for the solution
		NEWMAT::ColumnVector errors(Nm);
		int inliers = 0;
		for(i = 0; i<Nm; i++) {
			match = &matches[i];
			C1.Set(match->left.x, match->left.y);
			C2.Set(match->right.x, match->right.y);
			tc = inv_hm.Map(C1);
			errors(i+1) = C2.DDistance(tc);
			if(errors(i+1) < Tol)
				inliers++;
			else
				errors(i+1) = Tol;
		}
		//cout<<"errors:"<<endl<<setw(10)<<setprecision(7)<<errors.t();
		sse = NEWMAT::DotProduct(errors, errors);
		if(sample_count==0 || best_sse>sse) { // very first, just use it
			best_hm = hm;
			best_sse = sse;
			best_errors = errors;
			best_inliers = inliers;
		}
		// Monitor progress
		if(inliers > max_inliers) {
			max_inliers = inliers;
			// Reestimate number of trials needed
			double epsilon = 1.0-double(max_inliers)/Nm;
			N0 = (int) (log(1.0-p)/log(1.0-pow(1.0-epsilon, double(s))));
		}
		sample_count++;

		//printf("%d  %d (%d)    %d\n", sample_count, inliers, max_inliers, N0);
	}
	Hmgr inv_best_hm = best_hm.Invert();
	
	if(debug && fp_log) fprintf(fp_log, "Iter=%d (Pts=%d In=%d) Needed=%d\n", sample_count, Nm, max_inliers, N0);

	// Set the outliers
	int outlier = 0;
	double error;
	for(int i = 0; i<Nm; i++) {
		match = &matches[i];
		C1.Set(match->left.x, match->left.y);
		C2.Set(match->right.x, match->right.y);
		tc = inv_best_hm.Map(C1);
		error = C2.DDistance(tc);
		if(error<Tol) {
			match->good = true;
			match->mark = 'G';
			n_good_matches++;
		} else {
			match->good = false;
			match->mark = 'B';
		}
	}

	hm = best_hm;
	return(true);
}

Hmgr Y_LDRRegister::RigidAffineFromMatches(Coord2* c1, Coord2* c2, int n_mark, Y_Image* pIm)
{
	NEWMAT::Matrix B(2*n_mark,4);
	NEWMAT::ColumnVector A(2*n_mark), P(4);

	for(int i = 0; i<n_mark; i++) {
		c1[i].SetImage(pIm);
		c2[i].SetImage(pIm);

		B(2*i+1,1) = c2[i].Xn(); B(2*i+1,2) = -c2[i].Yn(); B(2*i+1,3) = 1.0; B(2*i+1,4) = 0.0; A(2*i+1) = c1[i].Xn();
		B(2*i+2,1) = c2[i].Yn(); B(2*i+2,2) =  c2[i].Xn(); B(2*i+2,3) = 0.0; B(2*i+2,4) = 1.0; A(2*i+2) = c1[i].Yn();
	}

	if(n_mark==2) {
		// If B is singular, return singular homography
		if(fabs(B.Determinant()) < HMGR_EPS) {
			Hmgr hm;
			hm.Zero();
			return(hm);
		}
		P = B.i()*A;
	} else {
		NEWMAT::Matrix C = B.t()*B;
		NEWMAT::Matrix D = C.i()*B.t();
		P = D*A;
	}
	Hmgr hm;
	// Construct homography from computed vector
	hm.Set(P(1), -P(2), P(3), P(2), P(1), P(4), 0.0, 0.0, 1.0);
	hm.h /= hm.h(3,3);
	return(hm);
}

Hmgr Y_LDRRegister::PerspectiveFromMatches(Coord2* c1, Coord2* c2, int n_mark, Y_Image* pIm)
{
	NEWMAT::Matrix B(2*n_mark,8);
	NEWMAT::ColumnVector A(2*n_mark), P(8);

	for(int j = 0; j<n_mark; j++) {
		c1[j].SetImage(pIm);
		c2[j].SetImage(pIm);

		B(2*j+1,1) = c2[j].Xn(); B(2*j+1,2) = c2[j].Yn(); B(2*j+1,3) = 1.0; 
		B(2*j+1,4) = 0.0; B(2*j+1,5) = 0.0; B(2*j+1,6) = 0.0;
		B(2*j+1,7) = -c2[j].Xn()*c1[j].Xn();
		B(2*j+1,8) = -c2[j].Yn()*c1[j].Xn();
		A(2*j+1) = c1[j].Xn();
		B(2*j+2,1) = B(2*j+2,2) = B(2*j+2,3) = 0.0;
		B(2*j+2,4) = c2[j].Xn(); B(2*j+2,5) = c2[j].Yn(); B(2*j+2,6) = 1.0;
		B(2*j+2,7) = -c2[j].Xn()*c1[j].Yn();
		B(2*j+2,8) = -c2[j].Yn()*c1[j].Yn();
		A(2*j+2) = c1[j].Yn();
	}

	if(n_mark==4) {
		// If B is singular, return singular homography
		if(fabs(B.Determinant()) < HMGR_EPS) {
			Hmgr hm;
			hm.Zero();
			return(hm);
		}
		P = B.i()*A;
	} else {
		NEWMAT::Matrix C = B.t()*B;
		NEWMAT::Matrix D = C.i()*B.t();
		P = D*A;
	}
	Hmgr hm;
	// Construct homography from computed vector
	hm.Set(P(1), P(2), P(3), P(4), P(5), P(6), P(7), P(8), 1.0);
	hm.h /= hm.h(3,3);
	return(hm);
}

//// Choose 's' different numbers from 'N' possibilities
//NEWMAT::ColumnVector Y_LDRRegister::RandomSet(int s, int n)
//{
//	NEWMAT::ColumnVector C(s); C = -1;
//	int k = 0;
//	double ratio = double(n-1)/RAND_MAX;
//	while(k<s) {
//		// Pick random number from 0 to n-1
//		//int r = Y_Utils::i_rand(0, n-1);
//		int r = int(ratio*rand());
//		// Check that it hasn't been picked yet
//		bool picked = false;
//		for(int i = 0; i<k; i++) {
//			if(int(C(i+1))==r) picked = true;
//		}
//		if(!picked) {
//			C(k+1) = r;
//			k++;
//		}
//	}
//	return(C);
//}

// Check that the same match does not appear twice
bool Y_LDRRegister::FoundSameMatch(Y_Match* match_set[], int n)
{
	for(int i = 0; i<n-1; i++) {
		for(int j = i+1; j<n; j++) {
			if(EPSILON_EQ(match_set[i]->left.x,match_set[j]->left.x) && EPSILON_EQ(match_set[i]->right.x,match_set[j]->right.x))
				return(true);
		}
	}
	return(false);
}

void Y_LDRRegister::ChooseCollocations(vector<Y_Match>& matches, int num_matches, Y_Image* pIm, Hmgr& hm, int num_col, Collocations* pCol)
{
	Y_Match* match;
	double* d = new double[num_matches];
	pCol->n = num_col;
	pCol->b = new Coord2[num_col];
	pCol->d = new Coord2[num_col];

	Coord2 C1, C2, tc;
	C1.SetImage(pIm);
	C2.SetImage(pIm);
	tc.SetImage(pIm);

	for(int i = 0; i<num_matches; i++) {
		match = &matches[i];
		if(match->good) {
			C1.Set(match->left.x, match->left.y);
			C2.Set(match->right.x, match->right.y);
			tc = hm.Map(C2);
			d[i] = C1.DDistance(tc);
		}
	}
	int found = 0;
	while(found<num_col) {
		double min_d = 1.e39;
		int best_pt = -1;
		for(int i = 0; i<num_matches; i++) {
			match = &matches[i];
			if(match->good && d[i]<min_d) {
				min_d = d[i];
				best_pt = i;
			}
		}
		if(best_pt!=-1) {
			pCol->b[found].Set(matches[best_pt].left.x, matches[best_pt].left.y);
			pCol->d[found].Set(matches[best_pt].right.x, matches[best_pt].right.y);
			d[best_pt] = 1.e39;
			found++;
		}
	}
	delete [] d;
}

// Yuri: July 2011: needs to be done for double image as well
double Y_LDRRegister::PerPixelError(char* filename)
{
	if(fail) // Registration is invalid
		return(-1);

	BMPImageG *mosaic = new BMPImageG(2*m_pBMP1->w, 2*m_pBMP1->h);
	mosaic->SetBlack();
	int x_off = (mosaic->w-m_pBMP1->w)/2, y_off = (mosaic->h-m_pBMP1->h)/2;
	mosaic->PutAt(m_pBMP1, x_off, y_off);
	//if(debug)
	//	mosaic->Save("Y_SIFTRegister_mos1.bmp", 1);
	Y_Rect<int> mos_r = mosaic->GetRect();
	
	Coord2 c, tc;
		c.SetImage(m_pBMP1);
		tc.SetImage(m_pBMP1);
	double v, v1, v2;
	int vld1, vld2;
	double weight = 0.5; // both images give the same contributions to the mosaic
	
	Hmgr inv_hm = m_hm.Invert();
	
	// Find images of frame corners
	Y_Rect<int> r = m_pBMP2->GetRect();
	
	double totalError = 0.0; 
	int count = 0, all_pixels = 0;
	double maxError = 0.0, v_dif;

	for(int j = 0; j<mosaic->h; j++) {
		for(int i = 0; i<mosaic->w; i++) {
			v1 = mosaic->get_pixel_D3(i, j, &vld1);
			if(vld1) all_pixels++; // valid mosaic pixel, hence valid base pixel
			c.Set(i-x_off, j-y_off);
			tc = inv_hm.Map(c);
			if(!tc.Outside(r)) {
				v2 = tc.Value3(m_pBMP2, &vld2);
				if(vld1 && vld2) {
					v_dif = fabs(v1-v2);
					if(v_dif>maxError)
						maxError = v_dif;
					totalError += v_dif;
					count++; // Count all pixels in overlap
					v = weight*v1+(1.0-weight)*v2;
					mosaic->set_pixel(i, j, (u_char)v);
				} else if(vld2) {
					mosaic->set_pixel(i, j, v2);
				}
			}
		}
	}
	double perPixelError = totalError/count;
	double overlapPercent = 100.0*count/all_pixels; // should be close to polygon-based estimate
	if(debug>0) {
		sprintf_s(name, NAME_LEN, "Error=%lf  Count=%d  Total=%.2lf  MaxError=%.2lf  Overlap=%.2lf\n", 
			perPixelError, count, totalError, maxError, overlapPercent);
	}
	if(filename) { // Expects to be saved
		mosaic->Save(filename);
	}
	delete mosaic;
	return(perPixelError);
}

void Y_LDRRegister::CalculateStats(Coord2* c1, Coord2* c2, int n_good_matches, 
	Y_Image* pIm, Hmgr& hm)
{
	memset(m_stats, 0, 4*sizeof(double));
	Coord2 tc;
	tc.SetImage(pIm);

	double sum = 0.0, sumsq = 0.0, d;

	for(int i = 0; i<n_good_matches; i++) {
		tc = hm.Map(c2[i]);
		d = c1[i].DDistance(tc);
		m_stats[0] = MAX(m_stats[0], d);
		sum += d;
		sumsq += d*d;
	}
	m_stats[1] = sum/n_good_matches;
	m_stats[2] = sqrt(sumsq/n_good_matches-m_stats[1]*m_stats[1]);
	m_stats[3] = double(n_good_matches);
}

vector<Y_Match> Y_LDRRegister::CopyMatches()
{
	vector<Y_Match> copy_matches;
	Y_Match::SetSize(m_pIm->w, m_pIm->h);
	Y_Match::SetFocalLength(m_pIm->focal_length);
	for(int i = 0; i<(int)m_matches.size(); i++) {
		Y_Match match = m_matches[i];
		copy_matches.push_back(match);
	}
	return(copy_matches);
}

void Y_LDRRegister::SaveMatchesMarked(int set, char* name, int radius, 
	vector<Y_Match>& matches, Y_RGB<u_char> Pixel)
{
	if(set==1) { // Left image
		BMPImageC* pBMPC = m_pIm1->ToBMPC();
		for(vector<Y_Match>::iterator iter = matches.begin(); iter!=matches.end(); iter++) {
			KeyPoint key; key.pt.x = float(iter->left.x); key.pt.y = float(iter->left.y);
			Y_LDRDetect::MarkPoint(pBMPC, key, radius, Pixel);
		}
		pBMPC->Save(name);
		delete pBMPC;
	} else {
		BMPImageC* pBMPC = m_pIm2->ToBMPC();
		for(vector<Y_Match>::iterator iter = matches.begin(); iter!=matches.end(); iter++) {
			KeyPoint key; key.pt.x = float(iter->right.x); key.pt.y = float(iter->right.y);
			Y_LDRDetect::MarkPoint(pBMPC, key, radius, Pixel);
		}
		pBMPC->Save(name);
		delete pBMPC;
	}
}

static int bresenham(Point2<int> start, Point2<int> end, Point2<int> *pp, int N);

void Y_LDRRegister::SaveMatchesMarkedNumbered(int set, char* name, Y_LDRImage* pIm, int radius, vector<Y_Match>& matches)
{
	Point2<int> p;
	int n = int(matches.size());
	BMPImageC* pBMPC = pIm->ToBMPC();
	// Remap images by brightening
	Y_RGB<u_char> newClrs[256], clr;
	newClrs[0].Set(0,0,0);
	int i, j, v;
	for(i = 1; i<256; i++) {
		j = i>>3;
		j = j+128;
		newClrs[i].Set(u_char(j), u_char(j), u_char(j));
	}
	BMPImageC* pC = new BMPImageC(pBMPC->w, pBMPC->h);
	for(j = 0; j<pBMPC->h; j++) {
		for(i = 0; i<pBMPC->w; i++) {
			v = int(pBMPC->get_grey_pixel(i,j));
			pC->set_pixel(i, j, newClrs[v]);
		}
	}
	// Add matches
	cv::KeyPoint key;
	if(set==1) {
		for(i = 0; i<n; i++) {
			clr.Number2Colour(i);
			key.pt.x = float(matches[i].left.x); key.pt.y = float(matches[i].left.y); 
			Y_LDRDetect::MarkPoint(pC, key, radius, clr);
		}
	} else {
		for(i = 0; i<n; i++) {
			clr.Number2Colour(i);
			key.pt.x = float(matches[i].right.x); key.pt.y = float(matches[i].right.y); 
			Y_LDRDetect::MarkPoint(pC, key, radius, clr);
		}
	}
	pC->Save(name);
	delete pC;
}

void Y_LDRRegister::SaveMatchesVectors(char* name, int radius, vector<Y_Match>& matches)
{
	BMPImageC* pBMPC = new BMPImageC(2*m_pIm1->m_pMat->cols, m_pIm1->m_pMat->rows);
	BMPImageC* pBMPC1 = m_pIm1->ToBMPC();
	pBMPC->PutAt(pBMPC1, 0, 0);
	delete pBMPC1;
	BMPImageC* pBMPC2 = m_pIm2->ToBMPC();
	pBMPC->PutAt(pBMPC2, m_pIm1->m_pMat->cols, 0);
	delete pBMPC2;

	Point2<int> start, end;
	Point2<int>* br_p = new Point2<int>[pBMPC->w+pBMPC->h];
	Y_RGB<u_char> V;

	for(vector<Y_Match>::iterator iter = matches.begin(); iter!=matches.end(); iter++) {
		start.Set(int(iter->left.x+0.5), int(iter->left.y+0.5));
		end.Set(int(iter->right.x+0.5)+m_pIm1->m_pMat->cols, int(iter->right.y+0.5));
		int n = Y_Utils::bresenham(start, end, br_p, pBMPC->w+pBMPC->h);
		V.Set(u_char(Y_Utils::i_rand(0,255)), u_char(Y_Utils::i_rand(0,255)), u_char(Y_Utils::i_rand(0,255)));
		for(int j = 0; j<n; j++)
			pBMPC->set_pixel(br_p[j], V);
	}

	pBMPC->Save(name);
	delete pBMPC;
}

// If score_set is true, use already calculated score (in some other metric)
int Y_LDRRegister::WriteMatchFile(char *name, std::vector<Y_Match>& matches, bool score_set)
{
	FILE *fp;
	if(fopen_s(&fp, name, "w")) {
		if(fp_log) fprintf(fp_log, "Could not open file");
		return(-1);
	}
	fprintf(fp, "# N    x1     y1     x2     y2     Distance     Mark\n");
	for(int i = 0; i<int(matches.size()); i++) {
		Y_Match* match = &matches[i];
		match->mark = match->good ? 'G' : 'B';
		if(score_set) {
			fprintf(fp, "%4d %10.4lf %10.4lf %10.4lf %10.4lf %10.2lf       %c\n", i,
				match->left.x, match->left.y, match->right.x, match->right.y, match->score, match->mark);
		} else {
			double d = sqrt((match->left.x-match->right.x)*(match->left.x-match->right.x)+
				(match->left.y-match->right.y)*(match->left.y-match->right.y));
			fprintf(fp, "%4d %10.4lf %10.4lf %10.4lf %10.4lf %10.2lf       %c\n", i,
				match->left.x, match->left.y, match->right.x, match->right.y, d, match->mark);
		}
	}
	fclose(fp);
	return(0);
}


