#pragma once

#include "YMatch.h"
#include "Transform.h"
#include "RAR.h"
#include "Hmgr.h"
#include "Collocations.h"

#include "LDRDetect.h"
#include "LDRMatch.h"

using namespace cv;

#define NAME_LEN 1024

class Y_LDRRegister {
public:
	Y_LDRRegister(char* method, Y_LDRImage* _pIm1, Y_LDRImage* _pIm2, Y_LDRParams* pParams);
	Y_LDRRegister(char* method, BMPImageG* _pBMP1, BMPImageG* _pBMP2, Y_LDRParams* pParams);
	Y_LDRRegister(char* method, IntImageG* _pInt1, IntImageG* _pInt2, Y_LDRParams* pParams);
	Y_LDRRegister(char* method, FltImageG* _pFlt1, FltImageG* _pFlt2, Y_LDRParams* pParams);
	Y_LDRRegister(char* method, DblImageG* _pDbl1, DblImageG* _pDbl2, Y_LDRParams* pParams);
	~Y_LDRRegister();
	void SetDebug(int debug, FILE* fp_log = NULL, char* currentDir = NULL);
	void SetPerspective(bool perspectiveModel); // 'true' for perspective model, 'false' for rigid affine
	int DoRegister(char* matchMethod = "BF"); // returns number of matches
	std::vector<cv::KeyPoint> GetKeypoints1() { return(m_keypoints1); }
	std::vector<cv::KeyPoint> GetKeypoints2() { return(m_keypoints2); }
	std::vector<Y_Match> GetMatches() { return(m_matches); }
	bool GetHmgr(Hmgr* pHm);
	Collocations* GetCollocations() { return(m_pCol); } // can be called after GetHmgr()
	void GetRRA(RARecord* pRRA, int fn1, int fn2, double maxScale = 2.0);
	void GetTransform(TransformRecord* pTransform, int fn1, int fn2, double maxScale = 2.0);
	double PerPixelError(char* name = NULL); // Can be called if necessary once DoSIFTRegister() is done
	bool SetCollocations(RARecord* pRRA) { 
		if(m_pCol) {
			pRRA->SetCollocations(m_pCol);
			return(true);
		}
		return(false);
	}
	bool SetCollocations(TransformRecord* pTransform) {
		if(m_pCol) {
			pTransform->SetCollocations(m_pCol);
			return(true);
		}
		return(false);
	}
	bool RANSAC_and_MLE(double tolerance, Y_Image* m_pIm, Hmgr& hm, Collocations* pCol);
	bool RANSAC_RigidAffine(vector<Y_Match>& matches, Y_Image* m_pIm, double Tol, Hmgr& hm, int& n_good_matches, int debug);
	bool RANSAC_Perspective(vector<Y_Match>& matches, Y_Image* m_pIm, double Tol, Hmgr& hm, int& n_good_matches, int debug);
	Hmgr RigidAffineFromMatches(Coord2* c1, Coord2* c2, int n_mark, Y_Image* m_pIm);
	Hmgr PerspectiveFromMatches(Coord2* c1, Coord2* c2, int n_mark, Y_Image* m_pIm);
	//ColumnVector RandomSet(int s, int n);
	bool FoundSameMatch(Y_Match* match_set[], int n);
	void ChooseCollocations(vector<Y_Match>& matches, int num_matches, Y_Image* m_pIm, Hmgr& hm, int num_col, Collocations* pCol);
	int NumGoodMatches() {
		int numGoodMatches = 0;
		for(int i = 0; i<(int)m_matches.size(); i++) {
			if(m_matches[i].good)
				numGoodMatches++;
		}
		return(numGoodMatches);
	}
	void DiscardBoundaryKeypoints(vector<KeyPoint>& keys, Y_LDRImage* pIm, int allowed_proximity);
	int WriteMatchFile(char *name, std::vector<Y_Match>& matches, bool score_set = false);
	void SaveMatchesMarked(int set, char* name, int radius, vector<Y_Match>& matches, 
		Y_RGB<u_char> Pixel = Y_RGB<u_char>::YellowPixel);
	void SaveMatchesMarkedNumbered(int set, char* name, Y_LDRImage* pIm, int radius, vector<Y_Match>& matches);
	void SaveMatchesVectors(char* name, int radius, vector<Y_Match>& matches);
	void CalculateStats(Coord2* c1, Coord2* c2, int n_good_matches, Y_Image* pIm, Hmgr& hm);
	char* GetError() { return(Y_LDRRegister::errorDescr); }
	vector<Y_Match> CopyMatches();

public:
	char m_method[16];
	Y_Image::IMAGE_TYPE m_imgType;
	Y_LDRImage* m_pIm1, *m_pIm2;
	BMPImageG *m_pBMP1, *m_pBMP2;
	IntImageG *m_pInt1, *m_pInt2;
	FltImageG *m_pFlt1, *m_pFlt2;
	DblImageG *m_pDbl1, *m_pDbl2;
	Y_Image* m_pIm;
	Y_LDRParams* m_pParams;
	bool fail;
	int debug;
	FILE* fp_log;
	char* currentDir;
	bool perspectiveModel;
	std::vector<cv::KeyPoint> m_keypoints1;
	std::vector<cv::KeyPoint> m_keypoints2;
	vector<Y_Match> m_matches;
	Collocations* m_pCol;
	Hmgr m_hm;
	double m_stats[4]; // max error, average error, stdev of errors, num matches
	static char name[NAME_LEN];
	static char errorDescr[ERR_LEN];
};
