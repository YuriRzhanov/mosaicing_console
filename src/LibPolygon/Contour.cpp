#include "LibPolygon.h"

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

namespace libpolygon
{
	vector<CContour*> CContour::GPCVertexListPtrToCContourVec(gpc_vertex_list* pGPCContours, 
		int* pHoleStatuses, int n)
	{		
		std::vector<libpolygon::CContour*> contoursVec;
		contoursVec.reserve(n);
		for(int i = 0; i<n; ++i)
			contoursVec.push_back(new CContour(pGPCContours[i], pHoleStatuses[i]));
		return contoursVec;
	}

	double CContour::Area()
	{
		double area = 0.0;
		vector<Point2<double> > v = GetVerticesCopyVector();

		for(int i = 0; i<m_pGPCContour->num_vertices-1; i++) {
			area += v[i].x * v[i+1].y;
			area -= v[i].y * v[i+1].x;
		}
		area += v[m_pGPCContour->num_vertices-1].x * v[0].y;
		area -= v[m_pGPCContour->num_vertices-1].y * v[0].x;
		return((area<0.0) ? -0.5*area : 0.5*area);
	}
	
	// Yuri: contour that is a bounding box for *this*
	// w,h,tl_x,tl_y are filled in if storage is provided
	// Note: returned contour has to be deleted by the calling routine
	CContour* CContour::BoundingBox(double* w, double* h, double* tl_x, double* tl_y)
	{
		double x_min = 1.e39, x_max = -1.e39, y_min = 1.e39, y_max = -1.e39;
		// Every vertex of this polygon must be inside this BB
		vector<Point2<double> > V = GetVerticesCopyVector();
		int n_v = (int)V.size();

		for(int i = 0; i<n_v; i++) {
			Point2<double> v = V.at(i);
			x_min = (x_min>v.x) ? v.x : x_min;
			x_max = (x_max<v.x) ? v.x : x_max;
			y_min = (y_min>v.y) ? v.y : y_min;
			y_max = (y_max<v.y) ? v.y : y_max;
		}

		V.clear();
		V.push_back(Point2<double>(x_min,y_min));
		V.push_back(Point2<double>(x_max,y_min));
		V.push_back(Point2<double>(x_max,y_max));
		V.push_back(Point2<double>(x_min,y_max));

		CContour* bb = new CContour(V, 0);
		if(w) *w = x_max-x_min;
		if(h) *h = y_max-y_min;
		if(tl_x) *tl_x = x_min;
		if(tl_y) *tl_y = y_min;
		return(bb);
	}

	void CContour::BoundingBoxSize(double& w, double& h)
	{
		double x_min = 1.e39, x_max = -1.e39, y_min = 1.e39, y_max = -1.e39;
		// Every vertex of this polygon must be inside this BB
		vector<Point2<double> > V = GetVerticesCopyVector();
		int n_v = (int)V.size();

		for(int i = 0; i<n_v; i++) {
			Point2<double> v = V[i];
			x_min = (x_min>v.x) ? v.x : x_min;
			x_max = (x_max<v.x) ? v.x : x_max;
			y_min = (y_min>v.y) ? v.y : y_min;
			y_max = (y_max<v.y) ? v.y : y_max;
		}
		V.clear();

		w = x_max-x_min;
		h = y_max-y_min;
	}

	void CContour::BoundingBoxRect(Y_Rect<double>& R)
	{
		R.Set(1.e39, 1.e39, -1.e39, -1.e39);
		// Every vertex of this polygon must be inside this BB
		vector<Point2<double> > V = GetVerticesCopyVector();
		int n_v = (int)V.size();

		for(int i = 0; i<n_v; i++) {
			Point2<double> v = V[i];
			R.x = (R.x>v.x) ? v.x : R.x;
			R.w = (R.w<v.x) ? v.x : R.w;
			R.y = (R.y>v.y) ? v.y : R.y;
			R.h = (R.h<v.y) ? v.y : R.h;
		}
		V.clear();

		R.w = R.w-R.x;
		R.h = R.h-R.y;
	}
		
	// computes if a vertex lies inside of a contour
	// the code is taken from "Solution 2", located at: 
	// http://astronomy.swin.edu.au/~pbourke/geometry/insidepoly/		
	bool CContour::isVertexInside(Point2<double>& v)
	{
		int counter = 0;
		int i;
		double xinters;
		Point2<double> v1, v2;

		vector<Point2<double> > Vertices = GetVerticesCopyVector();
		int N = (int) Vertices.size();

		v1 = Vertices[0];
		for(i = 1; i<=N; i++) {
			v2 = Vertices[i % N];
			if(v.y > min(v1.y, v2.y)) {
				if(v.y <= max(v1.y, v2.y)) {
					if(v.x <= max(v1.x, v2.x)) {
						if(!EQ(v1.y,v2.y)) {
							xinters = (v.y-v1.y)*(v2.x-v1.x)/(v2.y-v1.y)+v1.x;
							if(EQ(v1.x,v2.x) || v.x <= xinters)
								counter++;
						}
					}
				}
			}
			v1 = v2;
		}
		return(counter % 2 != 0);
	}

	// Returns contour representing convex hull of the point cloud
	CContour* CContour::ConvexHull(vector<Point2<double> > V)
	{
		// sort vertices by X, and then by Y
		sort(V.begin(), V.end(), Greater());
		// Allocate array of vertices:
		Point2<double>* pts = new Point2<double>[(int)V.size()+10];
		for(int i = 0; i<(int)V.size(); i++)
			pts[i] = V.at(i);
		// ... and for result:
		Point2<double>* pHull = new Point2<double>[(int)V.size()+10];

		int N_hull = chainHull_2D(pts, (int)V.size(), pHull);
		// All points except for the last are added to the contour:
		vector<Point2<double> > Hull;
		for(int j = 0; j<N_hull-1; j++)
			Hull.push_back(pHull[j]);
		CContour* pContour = new CContour(Hull, 0); // '0' because not a hole

		delete [] pts;
		delete [] pHull;
		return(pContour);
	}

// isLeft(): tests if a point is Left|On|Right of an infinite line.
//	Input:  three points P0, P1, and P2
//	Return: >0 for P2 left of the line through P0 and P1
//			=0 for P2 on the line
//			<0 for P2 right of the line
	inline double CContour::isLeft(Point2<double> P0, Point2<double> P1, Point2<double> P2)
	{
		return (P1.x - P0.x)*(P2.y - P0.y) - (P2.x - P0.x)*(P1.y - P0.y);
	}
//===================================================================
 

// chainHull_2D(): Andrew's monotone chain 2D convex hull algorithm
//	 Input:  P[] = an array of 2D points 
//				   presorted by increasing x- and y-coordinates
//			 n = the number of points in P[]
//	 Output: H[] = an array of the convex hull vertices (max is n)
//	 Return: the number of points in H[]
	int CContour::chainHull_2D(Point2<double>* P, int n, Point2<double>* H)
	{
		// the output array H[] will be used as the stack
		int	bot = 0, top = (-1);  // indices for bottom and top of the stack
		int	i;				// array scan index

		// Get the indices of points with min x-coord and min|max y-coord
		int minmin = 0, minmax;
		double xmin = P[0].x;
		for(i = 1; i<n; i++)
			if(!EQ(P[i].x,xmin)) break;
		minmax = i-1;
		if(minmax == n-1) {	   // degenerate case: all x-coords == xmin
			H[++top] = P[minmin];
			if(!EQ(P[minmax].y,P[minmin].y)) // a nontrivial segment
				H[++top] = P[minmax];
			H[++top] = P[minmin];		   // add polygon endpoint
			return(top+1);
		}

		// Get the indices of points with max x-coord and min|max y-coord
		int maxmin, maxmax = n-1;
		double xmax = P[n-1].x;
		for(i = n-2; i>=0; i--)
			if (!EQ(P[i].x,xmax)) break;
		maxmin = i+1;

		// Compute the lower hull on the stack H
		H[++top] = P[minmin];	  // push minmin point onto stack
		i = minmax;
		while(++i <= maxmin) {
			// the lower line joins P[minmin] with P[maxmin]
			if(isLeft(P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin)
				continue;		  // ignore P[i] above or on the lower line

			while(top > 0) {		// there are at least 2 points on the stack
				// test if P[i] is left of the line at the stack top
				if(isLeft(H[top-1], H[top], P[i]) > 0)
					break;		 // P[i] is a new hull vertex
				else
					top--;		 // pop top point off stack
			}
			H[++top] = P[i];	   // push P[i] onto stack
		}

		// Next, compute the upper hull on the stack H above the bottom hull
		if(maxmax != maxmin)	  // if distinct xmax points
			H[++top] = P[maxmax];  // push maxmax point onto stack
		bot = top;				 // the bottom point of the upper hull stack
		i = maxmin;
		while(--i >= minmax) {
			// the upper line joins P[maxmax] with P[minmax]
			if(isLeft(P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
				continue;		  // ignore P[i] below or on the upper line

			while(top > bot) {	// at least 2 points on the upper stack
				// test if P[i] is left of the line at the stack top
				if(isLeft(H[top-1], H[top], P[i]) > 0)
					break;		 // P[i] is a new hull vertex
				else
					top--;		 // pop top point off stack
			}
			H[++top] = P[i];	   // push P[i] onto stack
		}
		if(minmax != minmin)
			H[++top] = P[minmin];  // push joining endpoint onto stack

		return(top+1);
	}

}