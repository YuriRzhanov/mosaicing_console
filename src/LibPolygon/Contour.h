#pragma once

#include "LibPolygon.h"

namespace libpolygon
{
	using namespace std;
	using namespace gpc;

	class CContour
	{
	public:		
		/*!
		\brief Constructs an empty contour
		*/
		CContour()
			: fail(false), m_pGPCContour(NULL), m_HoleStatus(0), m_colour(0)
		{
			m_pGPCContour = new gpc_vertex_list();
			m_V.Set(0,0,0);
		}

		/*!
		\brief Copy Constructor
		*/
		CContour(CContour& Contour)
			: fail(false), m_pGPCContour(NULL), m_HoleStatus(Contour.m_HoleStatus), m_colour(Contour.m_colour), 
			m_V(Contour.m_V)
		{
			m_pGPCContour = new gpc_vertex_list();
			m_pGPCContour->num_vertices = Contour.m_pGPCContour->num_vertices;
			// allocate memory for the vertices
			m_pGPCContour->vertex = new gpc_vertex[Contour.m_pGPCContour->num_vertices];		
			// copy the vertices
			for(int i = 0; i<Contour.m_pGPCContour->num_vertices; ++i) {
				m_pGPCContour->vertex[i].x = Contour.m_pGPCContour->vertex[i].x;
				m_pGPCContour->vertex[i].y = Contour.m_pGPCContour->vertex[i].y;
			}
		}

		/*!
		\brief Constructs a new contour from the given vector of vertices.
		\param Vertices - vector of instances of CVertex class
		\param HoleStatus - indicates whether this contour is a hole or a filled body (0 - filled body, 1 - hole)
		*/
		CContour(vector<Point2<double> >& Vertices, int HoleStatus)
			: fail(false), m_pGPCContour(NULL), m_HoleStatus(HoleStatus), m_colour(0)
		{
			m_pGPCContour = new gpc_vertex_list();
			m_pGPCContour->num_vertices = (int)Vertices.size();
			// allocate memory for the vertices
			m_pGPCContour->vertex = new gpc_vertex[Vertices.size()];
			// copy the vertices
			for(int i = 0; i<(int)Vertices.size(); ++i) {
				m_pGPCContour->vertex[i].x = Vertices[i].x;
				m_pGPCContour->vertex[i].y = Vertices[i].y;
			}
			m_V.Set(0,0,0);
		}

		/*!
		\brief Constructs a new contour from the given vector of vertices.
		\param Vertices - vector of instances of CVertex class
		\param HoleStatus - indicates whether this contour is a hole or a filled body (0 - filled body, 1 - hole)
		*/
		CContour(vector<Point2<int> >& Vertices, int HoleStatus)
			: fail(false), m_pGPCContour(NULL), m_HoleStatus(HoleStatus), m_colour(0)
		{
			m_pGPCContour = new gpc_vertex_list();
			m_pGPCContour->num_vertices = (int)Vertices.size();
			// allocate memory for the vertices
			m_pGPCContour->vertex = new gpc_vertex[Vertices.size()];
			// copy the vertices
			for(int i = 0; i<(int)Vertices.size(); ++i) {
				m_pGPCContour->vertex[i].x = (double)Vertices[i].x;
				m_pGPCContour->vertex[i].y = (double)Vertices[i].y;
			}
			m_V.Set(0,0,0);
		}

		/*!
		\brief Constructs a copy of a contour from the gpc list of vertices
		\GPCContour - a gpc_vertex_list structure
		\HoleStatus - indicates whether or not this new contour is a hole

		New instance of the gpc_vertex_list is created. The vertices are copied from pGPCContour to the new conatiner
		*/
		CContour(gpc_vertex_list& GPCContour, int HoleStatus)
			: fail(false), m_pGPCContour(NULL), m_HoleStatus(HoleStatus), m_colour(0)
		{
			m_pGPCContour = new gpc_vertex_list();
			m_pGPCContour->num_vertices = GPCContour.num_vertices;
			// allocate memory for the vertices
			m_pGPCContour->vertex = new gpc_vertex[GPCContour.num_vertices];		
			// copy the vertices
			for(int i = 0; i<GPCContour.num_vertices; ++i) {
				m_pGPCContour->vertex[i].x = GPCContour.vertex[i].x;
				m_pGPCContour->vertex[i].y = GPCContour.vertex[i].y;
			}
			m_V.Set(0,0,0);
		}

		~CContour()
		{
			if(m_pGPCContour) {
				delete [] m_pGPCContour->vertex;
				delete m_pGPCContour;
				m_pGPCContour = NULL;
			}
		}

		/*
		\brief Returns vector of vertices comprising the current contour.
		"Copy" means that the new instances of vertices are created, and modifications to the returned vector won't do anything to the current contour
		*/
		std::vector<Point2<double> > GetVerticesCopyVector()
		{
			vector<Point2<double> > verticesVec;
			int n = m_pGPCContour->num_vertices;
			verticesVec.reserve(n);
			for(int i = 0; i<n; ++i)
				verticesVec.push_back(Point2<double>(m_pGPCContour->vertex[i].x, m_pGPCContour->vertex[i].y));
			return verticesVec;
		}

		/*
		\brief Returns vector of vertices comprising the current contour.
		"Copy" means that the new instances of vertices are created, and modifications to the returned vector won't do anything to the current contour
		*/
		Point2<double>* GetVerticesCopy()
		{
			int n = m_pGPCContour->num_vertices;
			Point2<double>* pVertices = new Point2<double>[n];
			for(int i = 0; i<n; ++i)
				pVertices[i].Set(m_pGPCContour->vertex[i].x, m_pGPCContour->vertex[i].y);
			return pVertices;
		}

		
		/*!
		\brief This functions converts a pointer to an array of gpc contours of known size into a vector of pointers to CContour objects
		\param pGPCContours - pointer to the array of gpc_vertex_list structures (gpc contours)
		\param pHoleStatuses - pointer to the array of ints conaining the "hole" flags for each contour
		\param n - number of contours in the pGPCContours
		\return std::vector of pointers to CContour objects
		*/
		static vector<CContour*> GPCVertexListPtrToCContourVec(gpc_vertex_list* pGPCContours, 
			int* pHoleStatuses, int n);

		// Yuri: area of the contour
		double Area();

		// Yuri: number of vertices in this contour
		int NumVertices()
		{
			return(m_pGPCContour->num_vertices);
		}

		// Yuri: returning i-th vertex
		Point2<double> Vertex(int i)
		{
			if(i<0 || i>=m_pGPCContour->num_vertices) {
				fprintf(stderr, "Index %d out of range.\n", i);
				return(Point2<double>(0.0,0.0));
			}
			return(Point2<double>(m_pGPCContour->vertex[i].x, m_pGPCContour->vertex[i].y));
		}

		// Yuri: returning pointer to a copy of i-th vertex
		Point2<double>* PtrVertex(int i)
		{
			if(i<0 || i>=m_pGPCContour->num_vertices) {
				fprintf(stderr, "Index %d out of range.\n", i);
				return(NULL);
			}
			Point2<double>* pVertex = new Point2<double>(m_pGPCContour->vertex[i].x, m_pGPCContour->vertex[i].y);
			return(pVertex);
		}

		bool DeleteVertex(int I)
		{
			bool status = false;
			// allocate memory for the vertices
			gpc_vertex* pVertices = new gpc_vertex[m_pGPCContour->num_vertices-1];		
			// copy the vertices
			for(int i = 0, j = 0; i<m_pGPCContour->num_vertices; ++i) {
				if(i!=I) {
					pVertices[j].x = m_pGPCContour->vertex[i].x;
					pVertices[j].y = m_pGPCContour->vertex[i].y;
					j++;
					status = true;
				}
			}
			m_pGPCContour->num_vertices--;
			delete [] m_pGPCContour->vertex;
			m_pGPCContour->vertex = pVertices;
			return(status);
		}

		// Yuri: contour that is a bounding box for *this*
		// w,h,tl_x,tl_y are filled in if storage is provided
		CContour* BoundingBox(double* w = NULL, double* h = NULL, double* tl_x = NULL, double* tl_y = NULL);
		void BoundingBoxSize(double& w, double& h);
		void BoundingBoxRect(Y_Rect<double>& R);

		// Yuri: 
		bool isVertexInside(Point2<double>& v);

		// Given cloud of vertices, return contour representing convex hull
		static CContour* ConvexHull(vector<Point2<double> > V);

		unsigned char R() { return (unsigned char)((m_colour>>16)&0xff); }
		unsigned char G() { return (unsigned char)((m_colour>>8)&0xff); }
		unsigned char B() { return (unsigned char)(m_colour&0xff); }
		unsigned char A() { return (unsigned char)255; }
		Y_RGB<u_char> Colour() { return(m_V); }

		// Yuri: 
		Point2<double> Centroid()
		{
			Point2<double> c(0.0, 0.0);
			int n = NumVertices();
			for(int i = 0; i<n; i++) {
				c.x += m_pGPCContour->vertex[i].x;
				c.y += m_pGPCContour->vertex[i].y;
			}
			c /= n;
			return(c);
		}

	private:
		static inline double isLeft(Point2<double> P0, Point2<double> P1, Point2<double> P2);
		static int chainHull_2D(Point2<double>* P, int n, Point2<double>* H);

		// Sort by X's. If X's are equal, sort by Y's.
		struct Greater {
			bool operator()(Point2<double> p1, Point2<double> p2) 
			{
				if(EQ(p1.x,p2.x))
					return(p1.y>p2.y);
				return(p1.x>p2.x);
			}
		};


	public:
		bool fail;
		/// indicates whether or not the current contour is a hole (0 - filled body, 1 - hole)
		int m_HoleStatus;
		// colour, 3 bytes containing R,G,B
		unsigned int m_colour;
		Y_RGB<u_char> m_V;
		/// Contains the vertices of the current contour
		gpc_vertex_list* m_pGPCContour;
	};
}
