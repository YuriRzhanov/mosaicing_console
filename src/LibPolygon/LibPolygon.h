#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <stdio.h>

#include "gpc.h"

#include "defs.h"
#include "Point2.h"
#include "PixelC.h"
#include "Contour.h"
#include "Polygon.h"
#include "Operations.h"

namespace libpolygon
{		
	using namespace std;
	using namespace gpc;
}
