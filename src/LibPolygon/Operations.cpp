#include "LibPolygon.h"

namespace libpolygon
{
	void Intersection(CPolygon* pA, CPolygon* pB, CPolygon* pC)
	{
		gpc_polygon_clip(GPC_INT, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
	}	

	CPolygon* Intersection(CPolygon* pA, CPolygon* pB)
	{
		CPolygon* pC = new CPolygon();
		gpc_polygon_clip(GPC_INT, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
		return(pC);
	}	


	void Difference(CPolygon* pA, CPolygon* pB, CPolygon* pC)
	{
		gpc_polygon_clip(GPC_DIFF, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
	}

	CPolygon* Difference(CPolygon* pA, CPolygon* pB)
	{
		CPolygon* pC = new CPolygon();
		gpc_polygon_clip(GPC_DIFF, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
		return(pC);
	}


	void XOR(CPolygon* pA, CPolygon* pB, CPolygon* pC)
	{
		gpc_polygon_clip(GPC_XOR, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
	}

	CPolygon* XOR(CPolygon* pA, CPolygon* pB)
	{
		CPolygon* pC = new CPolygon();
		gpc_polygon_clip(GPC_XOR, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
		return(pC);
	}


	void Union(CPolygon* pA, CPolygon* pB, CPolygon* pC)
	{
		gpc_polygon_clip(GPC_UNION, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
	}

	CPolygon* Union(CPolygon* pA, CPolygon* pB)
	{
		CPolygon* pC = new CPolygon();
		gpc_polygon_clip(GPC_UNION, pA->m_pGPCPoly,pB->m_pGPCPoly, pC->m_pGPCPoly);
		return(pC);
	}
}