#pragma once

#include "LibPolygon.h"

namespace libpolygon
{
	using namespace std;
	using namespace gpc;

	/*
	\brief Calculates the intersection of two polygons 
	\param pA - first operand
	\param pB - second operand
	\param pC - result C=A*B	
	*/
	void Intersection(CPolygon* pA, CPolygon* pB, CPolygon* pC);
	CPolygon* Intersection(CPolygon* pA, CPolygon* pB);

	/*
	\brief Calculates the difference of two polygons 
	\param pA - first operand
	\param pB - second operand
	\param pC - result C=A-B	
	*/
	void Difference(CPolygon* pA, CPolygon* pB, CPolygon* pC);
	CPolygon* Difference(CPolygon* pA, CPolygon* pB);

	/*
	\brief Calculates the Exclusive Or of two polygons 
	\param pA - first operand
	\param pB - second operand
	\param pC - result C=A/B	
	*/
	void XOR(CPolygon* pA, CPolygon* pB, CPolygon* pC);
	CPolygon* XOR(CPolygon* pA, CPolygon* pB);

	/*
	\brief Calculates the union of two polygons 
	\param pA - first operand
	\param pB - second operand
	\param pC - result C=A+B	
	*/
	void Union(CPolygon* pA, CPolygon* pB, CPolygon* pC);
	CPolygon* Union(CPolygon* pA, CPolygon* pB);
}
