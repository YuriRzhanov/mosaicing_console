#include "LibPolygon.h"

namespace libpolygon
{
	using namespace std;

	CPolygon::CPolygon()
		: m_pGPCPoly(NULL)
	{
		m_pGPCPoly = new gpc_polygon();
	}

	CPolygon::CPolygon(string FileName)
		: fail(false)
	{
		FILE* pInputFile;		
		m_pGPCPoly = new gpc_polygon();
#ifdef _MSC_VER
		if(fopen_s(&pInputFile, FileName.c_str(), "r")) {
			fail = true;
			return;
		}
#else
    pInputFile = fopen(FileName.c_str(), "r");
		if(!pInputFile) {
			fail = true;
			return;
		}
#endif
		gpc_read_polygon(pInputFile, 0, m_pGPCPoly);            
		fclose(pInputFile);
	}

	CPolygon::~CPolygon()
	{
		if(m_pGPCPoly) {
			gpc_free_polygon(m_pGPCPoly);
			delete m_pGPCPoly;
			m_pGPCPoly = NULL;
		}
	}

	void CPolygon::AddContour(CContour& Contour)
	{
		gpc_add_contour(m_pGPCPoly, Contour.m_pGPCContour, Contour.m_HoleStatus);
	}

	int CPolygon::Save(string FileName)
	{
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, FileName.c_str(), "w")) 
			return(-1);
#else
    fp = fopen(FileName.c_str(), "w");
		if(!fp) 
			return(-1);
#endif
		Save(fp);
		fclose(fp);
		return(0);
	}
	int CPolygon::Save(char* FileName)
	{
		FILE* fp;
#ifdef _MSC_VER
		if(fopen_s(&fp, FileName, "w")) 
			return(-1);
#else
    fp = fopen(FileName, "w");
		if(!fp) 
			return(-1);
#endif
		Save(fp);
		fclose(fp);
		return(0);
	}
	void CPolygon::Save(FILE* fp, char* tag)
	{
		if(tag) fprintf(fp, "%s\n", tag);
		gpc_write_polygon(fp, 0, m_pGPCPoly); 
	}
	void CPolygon::Print(FILE* fp, char* tag)
	{
		if(tag) fprintf(fp, "%s\n", tag);
		fprintf(fp, "# of contours: %d\n", m_pGPCPoly->num_contours);
		for(int c = 0; c<m_pGPCPoly->num_contours; c++) {
			fprintf(fp, "\t# of vertices: %d (hole:%d)\n", m_pGPCPoly->contour[c].num_vertices, m_pGPCPoly->hole[c]);
			for(int v = 0; v<m_pGPCPoly->contour[c].num_vertices; v++)
				fprintf(fp, "\t\t%8.2lf\t%8.2lf\n",
				m_pGPCPoly->contour[c].vertex[v].x,
				m_pGPCPoly->contour[c].vertex[v].y);
		}
	}

	vector<CContour*> CPolygon::GetContoursCopy()
	{
		return CContour::GPCVertexListPtrToCContourVec(
			m_pGPCPoly->contour, m_pGPCPoly->hole, m_pGPCPoly->num_contours);
	}
	
	// Yuri: areas for each contour are added with the sign reflecting hole status
	double CPolygon::Area()
	{
		double area = 0.0;
		vector<CContour*> contours = GetContoursCopy();
		
		for(vector<CContour*>::iterator i = contours.begin(); i!=contours.end(); ++i)
			area += ((*i)->m_HoleStatus) ? -(*i)->Area() : (*i)->Area();
		/*
		double a;
		for(int i = 0; i<(int)contours.size(); i++) {
			CContour* c = contours.at(i);
			a = c->Area();
			if(c->m_HoleStatus)
				area -= a;
			else
				area += a;
		}
		*/
		for(vector<CContour*>::iterator j = contours.begin(); j!=contours.end(); ++j)
			delete (*j);
		return(area);
	}

	int CPolygon::NumNonHoles()
	{
		int non_holes = 0;
		vector<CContour*> contours = GetContoursCopy();
		
		for(vector<CContour*>::iterator i = contours.begin(); i!=contours.end(); ++i)
			non_holes += ((*i)->m_HoleStatus == 0);
		for(vector<CContour*>::iterator j = contours.begin(); j!=contours.end(); ++j)
			delete (*j);
		return(non_holes);
	}

	// Returns polygon representing convex hull of the point cloud
	CPolygon* CPolygon::ConvexHull(vector<Point2<double> > V)
	{
		// sort vertices by X, and then by Y
		sort(V.begin(), V.end(), Greater());
		// Allocate array of vertices:
		Point2<double>* pts = new Point2<double>[(int)V.size()+10];
		for(int i = 0; i<(int)V.size(); i++)
			pts[i] = V.at(i);
		// ... and for result:
		Point2<double>* pHull = new Point2<double>[(int)V.size()+10];

		int N_hull = chainHull_2D(pts, (int)V.size(), pHull);
		// All points except for the last are added to the contour:
		vector<Point2<double> > Hull;
		for(int j = 0; j<N_hull-1; j++)
			Hull.push_back(pHull[j]);
		CContour* pContour = new CContour(Hull, 0); // '0' because not a hole

		CPolygon* pPolygon = new CPolygon();
		pPolygon->AddContour(*pContour);
		delete pContour;
		delete [] pts;
		delete [] pHull;
		return(pPolygon);
	}

// isLeft(): tests if a point is Left|On|Right of an infinite line.
//	Input:  three points P0, P1, and P2
//	Return: >0 for P2 left of the line through P0 and P1
//			=0 for P2 on the line
//			<0 for P2 right of the line
	inline double CPolygon::isLeft(Point2<double> P0, Point2<double> P1, Point2<double> P2)
	{
		return (P1.x - P0.x)*(P2.y - P0.y) - (P2.x - P0.x)*(P1.y - P0.y);
	}
//===================================================================
 

// chainHull_2D(): Andrew's monotone chain 2D convex hull algorithm
//	 Input:  P[] = an array of 2D points 
//				   presorted by increasing x- and y-coordinates
//			 n = the number of points in P[]
//	 Output: H[] = an array of the convex hull vertices (max is n)
//	 Return: the number of points in H[]
	int CPolygon::chainHull_2D(Point2<double>* P, int n, Point2<double>* H)
	{
		// the output array H[] will be used as the stack
		int	bot = 0, top = (-1);  // indices for bottom and top of the stack
		int	i;				// array scan index

		// Get the indices of points with min x-coord and min|max y-coord
		int minmin = 0, minmax;
		double xmin = P[0].x;
		for(i = 1; i<n; i++)
			if(!EQ(P[i].x,xmin)) break;
		minmax = i-1;
		if(minmax == n-1) {	   // degenerate case: all x-coords == xmin
			H[++top] = P[minmin];
			if(!EQ(P[minmax].y,P[minmin].y)) // a nontrivial segment
				H[++top] = P[minmax];
			H[++top] = P[minmin];		   // add polygon endpoint
			return(top+1);
		}

		// Get the indices of points with max x-coord and min|max y-coord
		int maxmin, maxmax = n-1;
		double xmax = P[n-1].x;
		for(i = n-2; i>=0; i--)
			if(!EQ(P[i].x,xmax)) break;
		maxmin = i+1;

		// Compute the lower hull on the stack H
		H[++top] = P[minmin];	  // push minmin point onto stack
		i = minmax;
		while(++i <= maxmin) {
			// the lower line joins P[minmin] with P[maxmin]
			if(isLeft(P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin)
				continue;		  // ignore P[i] above or on the lower line

			while(top > 0) {		// there are at least 2 points on the stack
				// test if P[i] is left of the line at the stack top
				if(isLeft(H[top-1], H[top], P[i]) > 0)
					break;		 // P[i] is a new hull vertex
				else
					top--;		 // pop top point off stack
			}
			H[++top] = P[i];	   // push P[i] onto stack
		}

		// Next, compute the upper hull on the stack H above the bottom hull
		if(maxmax != maxmin)	  // if distinct xmax points
			H[++top] = P[maxmax];  // push maxmax point onto stack
		bot = top;				 // the bottom point of the upper hull stack
		i = maxmin;
		while(--i >= minmax) {
			// the upper line joins P[maxmax] with P[minmax]
			if(isLeft(P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
				continue;		  // ignore P[i] below or on the upper line

			while(top > bot) {	// at least 2 points on the upper stack
				// test if P[i] is left of the line at the stack top
				if(isLeft(H[top-1], H[top], P[i]) > 0)
					break;		 // P[i] is a new hull vertex
				else
					top--;		 // pop top point off stack
			}
			H[++top] = P[i];	   // push P[i] onto stack
		}
		if(minmax != minmin)
			H[++top] = P[minmin];  // push joining endpoint onto stack

		return(top+1);
	}

	// Return contour that represents a bounding box for a polygon possibly consisting of several contours:
	// w,h,tl_x,tl_y are filled in if storage is provided
	// Note: returned contour has to be deleted by the calling routine
	CContour* CPolygon::BoundingBox(double* w, double* h, double* tl_x, double* tl_y)
	{
		double x_min = 1.e39, x_max = -1.e39, y_min = 1.e39, y_max = -1.e39;
		vector<CContour*> contours = GetContoursCopy();
		
		for(int n = 0; n<(int)contours.size(); n++) {
			CContour* pC = contours.at(n);
			// Every vertex of this contour must be inside this BB
			vector<Point2<double> > V = pC->GetVerticesCopyVector();
			int n_v = (int)V.size();

			for(int i = 0; i<n_v; i++) {
				Point2<double> v = V.at(i);
				x_min = (x_min>v.x) ? v.x : x_min;
				x_max = (x_max<v.x) ? v.x : x_max;
				y_min = (y_min>v.y) ? v.y : y_min;
				y_max = (y_max<v.y) ? v.y : y_max;
			}
		}

		vector<Point2<double> > V;
		V.push_back(Point2<double>(x_min,y_min));
		V.push_back(Point2<double>(x_min,y_max));
		V.push_back(Point2<double>(x_max,y_max));
		V.push_back(Point2<double>(x_max,y_min));
		CContour* bb = new CContour(V, 0);
		if(w) *w = x_max-x_min;
		if(h) *h = y_max-y_min;
		if(tl_x) *tl_x = x_min;
		if(tl_y) *tl_y = y_min;

		for(vector<CContour*>::iterator j = contours.begin(); j!=contours.end(); ++j)
			delete (*j);
		return(bb);
	}

}