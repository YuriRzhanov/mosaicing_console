#pragma once

#include "LibPolygon.h"

namespace libpolygon
{
	using namespace std;
	using namespace gpc;

	class CPolygon
	{
	public:		
		/*!
		\brief Creates an empty polygon
		*/
		CPolygon();

		/*!
		\brief Loads the polygon from a file
		\param FileName - name of the file from which to load the polygon
		*/
		CPolygon(string FileName);

		/*!
		\brief Destructs the current polygon
		*/
		~CPolygon();


		/*!
		\brief Add a contour to the current polygon
		\param Contour - CContour object to add

		New memory is allocated for adding the contour so further operations on the current polygon won't affect the given contour
		*/
		void AddContour(CContour& Contour);

		/*!
		\brief Saves the current polygon into ascii file
		\param FileName - name of the file in which to save the polygon
		*/
		int Save(string FileName);
		int Save(char* FileName);
		void Save(FILE* fp, char* tag = NULL);
		void Print(FILE* fp = stdout, char* tag = NULL);

		/*
		\brief Returns a vector of pointers to contour instances. Each contour is a separate instance from the current polygon
		\return Vector of pointers CContour instances. These instances are independent from the current polygon's content, so modifying them won't change the current polygon.
		*/
		vector<CContour*> GetContoursCopy();

		// Yuri: area of a polygon, including all contours
		double Area();

		// Given cloud of vertices, return polygon representing convex hull
		static CPolygon* ConvexHull(vector<Point2<double> > V);

		// Yuri: number of contours in the polygon
		int NumContours()
		{
			return(m_pGPCPoly->num_contours);
		}

		// Yuri
		int NumNonHoles();
		CContour* BoundingBox(double* w, double* h, double* tl_x, double* tl_y);

	private:
		static inline double isLeft(Point2<double> P0, Point2<double> P1, Point2<double> P2);
		static int chainHull_2D(Point2<double>* P, int n, Point2<double>* H);

		// Sort by X's. If X's are equal, sort by Y's.
		struct Greater {
			bool operator()(Point2<double> p1, Point2<double> p2) 
			{
				if(EQ(p1.x,p2.x))
					return(p1.y>p2.y);
				return(p1.x>p2.x);
			}
		};
	public:		
		gpc_polygon* m_pGPCPoly;
		bool fail;
	};
}
