#include "LibYMovie64.h"
using namespace std;

Y_Movie64::Y_Movie64(char* name) : 
	m_pMovieInfo(nullptr), m_bitDepth(0), m_numChannels(1), m_currentVideoFrame(0)
{
	fail = (m_videoCapture.open(name)) ? 0 : 1;
	if(!fail)
	{
		m_numVideoFrames = int(m_videoCapture.get(CV_CAP_PROP_FRAME_COUNT)+0.1);
		m_width = int(m_videoCapture.get(CV_CAP_PROP_FRAME_WIDTH)+0.1);
		m_height = int(m_videoCapture.get(CV_CAP_PROP_FRAME_HEIGHT)+0.1);
		m_fps = m_videoCapture.get(CV_CAP_PROP_FPS);
		long fourcc = long(m_videoCapture.get(CV_CAP_PROP_FOURCC));
		long format = long(m_videoCapture.get(CV_CAP_PROP_FORMAT));
		cv::Mat frame;
		bool success = m_videoCapture.read(frame); // Extract properties
		const int depth = frame.depth();
		switch(depth)
		{
		case CV_8U:
		case CV_8S:
			m_bitDepth = 8;
			break;
		case CV_16U:
		case CV_16S:
			m_bitDepth = 16;
			break;
		case CV_32S:
		case CV_32F:
			m_bitDepth = 32;
			break;
		case CV_64F:
			m_bitDepth = 64;
			break;
		default:
			cout << "No such depth\n";
		}
		const int mtype = frame.type();
		switch(mtype)
		{
		case CV_8UC1:
		case CV_8SC1:
		case CV_16UC1:
		case CV_16SC1:
		case CV_32SC1:
		case CV_32FC1:
		case CV_64FC1:
			m_numChannels = 1;
			break;
		case CV_8UC2:
		case CV_8SC2:
		case CV_16UC2:
		case CV_16SC2:
		case CV_32SC2:
		case CV_32FC2:
		case CV_64FC2:
			m_numChannels = 2;
			break;
		case CV_8UC3:
		case CV_8SC3:
		case CV_16UC3:
		case CV_16SC3:
		case CV_32SC3:
		case CV_32FC3:
		case CV_64FC3:
			m_numChannels = 3;
			break;
		case CV_8UC4:
		case CV_8SC4:
		case CV_16UC4:
		case CV_16SC4:
		case CV_32SC4:
		case CV_32FC4:
		case CV_64FC4:
			m_numChannels = 4;
			break;
		default:
			cout << "No such type\n";
		}
		// Return to the beginning:
		m_videoCapture.set(CV_CAP_PROP_POS_FRAMES, 0);
		m_pMovieInfo = new Y_Movie64Info(name);
		m_pMovieInfo->m_width = m_width; m_pMovieInfo->m_height = m_height;
		m_pMovieInfo->m_numFrames = m_numVideoFrames; m_pMovieInfo->m_fps = m_fps;
		m_pMovieInfo->m_bitDepth = m_bitDepth; m_pMovieInfo->m_numChannels = m_numChannels;
		m_pMovieInfo->SetTypeAndHandler(format, fourcc);
		int kkk = 0;
	}
}

Y_Movie64::Y_Movie64(char* name, unsigned int width, unsigned int height, unsigned int bitDepth, 
	unsigned int numChannels, double FPS, char* _fourcc) : 
	m_pMovieInfo(nullptr), m_currentVideoFrame(0)
{
	cv::Size sz(width, height);
	m_bitDepth = bitDepth;
	m_numChannels = numChannels;
	m_fps = FPS;
	//CV_FOURCC
	long fourcc = Y_Movie64Info::FromFourChars(_fourcc);
	fail = (m_videoWriter.open(name, fourcc, FPS, sz, m_numChannels==3)) ? 0 : 1;
		int kkk = 0;
}

Y_Movie64::~Y_Movie64()
{
	if(m_videoCapture.isOpened())
		m_videoCapture.release();
	if(m_videoWriter.isOpened())
		m_videoWriter.release();
	if(m_pMovieInfo)
		delete m_pMovieInfo;
}

int Y_Movie64::ReadVideoFrame(char* pFrameData, int n)
{
	if(n<-1 || n>int(m_numVideoFrames-1))
	{
		cout << "No such frame\n";
		return(-1);
	}
	if(m_videoCapture.isOpened())
	{
		cv::Mat frame;
		bool success = m_videoCapture.read(frame);
		int numBytes = 1;
		switch(m_bitDepth)
		{
		case 8:
			numBytes = 1; break;
		case 16:
			numBytes = 2; break;
		case 24:
			numBytes = 3; break;
		case 64:
			numBytes = 4; break;
		default:
			cout << "No such bit depth\n";
		}
		memcpy(pFrameData, frame.data, frame.cols*frame.rows*m_numChannels*numBytes);
		return(0);
	}
	return(-1);
}

int Y_Movie64::WriteVideoFrame(char* pFrameData, int bufferSize)
{
	if(m_videoWriter.isOpened())
	{
		
	}
	return(-1);
}

int Y_Movie64::ReadVideoFrame(BMPImageG** ppImage, int n)
{
	if(!fail)
	{
		m_videoCapture.set(CV_CAP_PROP_POS_FRAMES, n);
		cv::Mat frame;
		bool success = m_videoCapture.read(frame); // Extract properties
		m_currentVideoFrame = int(m_videoCapture.get(CV_CAP_PROP_POS_FRAMES)+0.1);
		*ppImage = OpenCVImageToBMPImageG(frame);
		return(0);
	}
	return(-1);
}

int Y_Movie64::ReadVideoFrame(BMPImageC** ppImage, int n)
{
	if(!fail)
	{
		m_videoCapture.set(CV_CAP_PROP_POS_FRAMES, n);
		cv::Mat frame;
		bool success = m_videoCapture.read(frame); // Extract properties
		m_currentVideoFrame = int(m_videoCapture.get(CV_CAP_PROP_POS_FRAMES)+0.1);
		*ppImage = OpenCVImageToBMPImageC(frame);
		return(0);
	}
	return(-1);
}

int Y_Movie64::ReadVideoFrame(cv::Mat** ppFrame, int n)
{
	if(!fail)
	{
		m_videoCapture.set(CV_CAP_PROP_POS_FRAMES, n);
		cv::Mat* pFrame = new cv::Mat;
		bool success = m_videoCapture.read(*pFrame); // Extract properties
		m_currentVideoFrame = int(m_videoCapture.get(CV_CAP_PROP_POS_FRAMES)+0.1);
    *ppFrame = pFrame;
		return(0);
	}
	return(-1);
}

int Y_Movie64::WriteVideoFrame(BMPImageG* pImage)
{
	// Check sizes and number of channels
	cv::Mat* pFrame = BMPImageGToOpenCVImage(pImage);
	m_videoWriter.write(*pFrame);
	delete pFrame;
	return(0);
}

int Y_Movie64::WriteVideoFrame(BMPImageC* pImage)
{
	// Check sizes and number of channels
	cv::Mat* pFrame = BMPImageCToOpenCVImage(pImage);
	m_videoWriter.write(*pFrame);
	delete pFrame;
	return(0);
}

BMPImageG* Y_Movie64::OpenCVImageToBMPImageG(const cv::Mat& img)
{
	BMPImageG* pBMP = new BMPImageG(img.cols, img.rows);
	for(int i = 0; i<img.cols*img.rows; i++) {
		pBMP->im[i] = img.data[i];
	}
	return(pBMP);
}

BMPImageC* Y_Movie64::OpenCVImageToBMPImageC(const cv::Mat& img)
{
	BMPImageC* pBMP = new BMPImageC(img.cols, img.rows);
	for(int i = 0; i<img.cols*img.rows; i++) {
		pBMP->im[3*i+0] = img.data[3*i+2];
		pBMP->im[3*i+1] = img.data[3*i+1];
		pBMP->im[3*i+2] = img.data[3*i+0];
	}
	return(pBMP);
}

BMPImageC* Y_Movie64::OpenCVImageGrayToBMPImageC(const cv::Mat& img)
{
	BMPImageC* pBMP = new BMPImageC(img.cols, img.rows);
	for(int i = 0; i<img.cols*img.rows; i++) {
		pBMP->im[3*i+0] = img.data[i];
		pBMP->im[3*i+1] = img.data[i];
		pBMP->im[3*i+2] = img.data[i];
	}
	return(pBMP);
}

cv::Mat* Y_Movie64::BMPImageGToOpenCVImage(const BMPImageG* pBMPG)
{
	cv::Mat* pImg = new cv::Mat(pBMPG->h, pBMPG->w, CV_8UC1);
	for(int i = 0; i<pImg->cols*pImg->rows; i++) {
		pImg->data[i] = pBMPG->im[i];
	}
	return(pImg);
}

cv::Mat* Y_Movie64::BMPImageCToOpenCVImage(const BMPImageC* pBMPC)
{
	cv::Mat* pImg = new cv::Mat(pBMPC->h, pBMPC->w, CV_8UC3);
	for(int i = 0; i<pImg->cols*pImg->rows; i++) {
		pImg->data[3*i+2] = pBMPC->im[3*i+0];
		pImg->data[3*i+1] = pBMPC->im[3*i+1];
		pImg->data[3*i+0] = pBMPC->im[3*i+2];
	}
	return(pImg);
}
