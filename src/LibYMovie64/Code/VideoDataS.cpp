//#include "stdafx.h"

// Enforce 8-byte alignment
#ifdef _MSC_VER
#include <pshpack8.h>
#else
#pragma pack(push,8)
#endif
#include "VideoDataS.h"
#ifdef _MSC_VER
#include <poppack.h>
#else
#pragma pack(pop)
#endif

Y_VideoDataS::Y_VideoDataS(char* name, int w, int h, int channels, int bpp) 
{
	m_mode = MODE_WRITE;
	m_header.magic[0] = 'Y'; m_header.magic[1] = '_'; m_header.magic[2] = 'V'; m_header.magic[3] = 'D';
	m_header.numFrames = 0;
	memset(&m_header.comment, 0, STR_SIZE); 
	m_header.w = w; m_header.h = h; m_header.channels = channels; m_header.bpp = bpp; 
	m_header.fps = 5.0f; m_header.empty = 0.0f;
	m_header.fl = 1.0f; m_header.FoV_h = 2.0f*atan(0.5f*m_header.w/m_header.fl);
	memset(&m_header.unused, 0, NUM_UNUSED*sizeof(int)); 
	m_fp = nullptr; m_buffer = nullptr;
	int len = (int)strlen(name);
	m_name = new char[len+1];
	memset(m_name, 0, len+1);
#ifdef _MSC_VER
	strncpy_s(m_name, len+1, name, len);
#else
	strncpy(m_name, name, len);
#endif
	memset(&m_frameInfo, 0, STR_SIZE);
	m_frameSize = m_header.w*m_header.h*m_header.channels*m_header.bpp/8;
	m_fullFrameSize = STR_SIZE+m_frameSize;
	m_currentFrameNum = 0;
	fail = 0;
	int l = sizeof(VideoDataSHeader);

	int kkk = 0;
}

Y_VideoDataS::Y_VideoDataS(char* name) 
{
	m_mode = MODE_READ;
	fail = 0;
#ifdef _MSC_VER
	if(fopen_s(&m_fp, name, "rb")) {
		fail = -1;
		return;
	}
#else
  m_fp = fopen(name, "rb");
	if(!m_fp) {
		fail = -1;
		return;
	}
#endif
	if(fread(&m_header, sizeof(VideoDataSHeader), 1, m_fp)!=1) {
		//sprintf_s(m_error, STR_SIZE, "Cannot read header.\n");
		fail = -1; fclose(m_fp); m_fp = nullptr; return;
	}
	if(m_header.magic[0]!='Y' || m_header.magic[1]!='_' || m_header.magic[2]!='V' || m_header.magic[3]!='D') {
		//sprintf_s(m_error, STR_SIZE, "Wrong magic.\n");
		fail = -1; fclose(m_fp); m_fp = nullptr; return;
	}
	if(m_header.w==0 || m_header.h==0) {
		//sprintf_s(m_error, STR_SIZE, "Zero dimensions.\n");
		fail = -1; fclose(m_fp); m_fp = nullptr; return;
	}
	int len = (int)strlen(name);
	m_name = new char[len+1];
	memset(m_name, 0, len+1);
#ifdef _MSC_VER
	strncpy_s(m_name, len+1, name, len);
#else
	strncpy(m_name, name, len);
#endif
	m_frameSize = m_header.w*m_header.h*m_header.channels*m_header.bpp/8;
	m_fullFrameSize = STR_SIZE+m_frameSize;
	m_buffer = new char[m_frameSize];
	if(m_buffer==nullptr) {
		//sprintf_s(m_error, STR_SIZE, "Cannot allocate buffer.\n");
		fail = -1; fclose(m_fp); m_fp = nullptr; return;
	}
	memset(m_buffer, 0, m_frameSize);
	m_currentFrameNum = 0;
	memset(m_frameInfo, 0, sizeof(char)*STR_SIZE);
	memset(m_error, 0, sizeof(char)*STR_SIZE);

	//__int64 ReturnData;
	//LARGE_INTEGER li;
	//HANDLE h = ::GetFileHandle();
	//if(GetFileSizeEx(h, &li)==0) {
	//	m_fileSize = li.QuadPart;
	//}

	//if(GetFileSizeEx(
	//WIN32_FILE_ATTRIBUTE_DATA fileInfo;
	//if(GetFileAttributesEx(name, GetFileExInfoStandard, (void*)&fileInfo)) {
	//	sprintf_s(m_error, STR_SIZE, "Cannot find file size");
	//	fail = 1;
	//}

#ifdef _MSC_VER
	struct __stat64 st = { 0 };
	if(_stat64((const char*)name, &st)==0) {
		m_fileSize = st.st_size;
	}
#else
	struct stat64 st = { 0 };
	if(stat64((const char*)name, &st)==0) {
		m_fileSize = st.st_size;
	}
#endif
	if(m_header.numFrames<=0) { // Fix # of frames
		m_header.numFrames = (int)((m_fileSize-sizeof(VideoDataSHeader))/m_fullFrameSize);
	}
}

Y_VideoDataS::~Y_VideoDataS() 
{
	if(Close()==-1) {
		//sprintf_s(m_error, STR_SIZE, "Cannot close instance.\n");
	}
	delete [] m_name;
	delete [] m_buffer;
	fail = -1;
}

int Y_VideoDataS::Close()
{
	int status = -1;
	if(m_fp) {
		if(m_mode==MODE_WRITE) {
#ifdef _MSC_VER
		_fseeki64(m_fp, 0, SEEK_SET);
#else
		lseek64(fileno(m_fp), 0, SEEK_SET);
#endif
			if(fwrite(&m_header, sizeof(VideoDataSHeader), 1, m_fp)!=1) {
				//sprintf_s(m_error, STR_SIZE, "Cannot save header.\n");
			}
		}
		fclose(m_fp);
		m_fp = nullptr;
		status = 0;
	}
	return(status);
}

bool Y_VideoDataS::ValidFrameForReading(int n)
{
	__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
	return((offset+m_fullFrameSize)<=m_fileSize);
}
	
int Y_VideoDataS::ReadFrame(int n, BMPImageG** pBMPG)
{
	if(m_mode==MODE_WRITE) {
		//sprintf_s(m_error, "File is open for writing.\n");
		return(1);
	}
	__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
	if((offset+m_fullFrameSize)>m_fileSize) {
		//sprintf_s(m_error, "File is too small for frame %d.\n", n);
		return(1);
	}

#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	if(fread(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame info.\n");
		return(1);
	}
	if(fread(m_buffer, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame data.\n");
		return(1);
	}
	*pBMPG = new BMPImageG(m_header.w, m_header.h, (u_char*)m_buffer);
	return(0);
}

int Y_VideoDataS::ReadFrame(int n, BMPImageC** pBMPC)
{
	if(m_mode==MODE_WRITE) {
		//sprintf_s(m_error, "File is open for writing.\n");
		return(1);
	}
	__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
	if((offset+m_fullFrameSize)>m_fileSize) {
		//sprintf_s(m_error, "File is too small for frame %d.\n", n);
		return(1);
	}

#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	if(fread(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame info.\n");
		return(1);
	}
	if(fread(m_buffer, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame data.\n");
		return(1);
	}
	*pBMPC = new BMPImageC(m_header.w, m_header.h, (u_char*)m_buffer);
	return(0);
}

int Y_VideoDataS::WriteFrame(BMPImageG*& pBMP, char* pInfo, int n)
{
	if(m_mode==MODE_READ) {
		//sprintf_s(m_error, "File is open for reading.\n");
		return(1);
	}
	if(m_fp==nullptr) {
		if(m_name==nullptr) {
			//sprintf_s(m_error, "Name is not set.\n");
			return(1);
		}
#ifdef _MSC_VER
	if(fopen_s(&m_fp, m_name, "wb")) {
			sprintf_s(m_error, STR_SIZE, "Cannot open %s for writing.\n", m_name);
			return(1);
	}
#else
  m_fp = fopen(m_name, "wb");
	if(!m_fp) {
			sprintf(m_error, "Cannot open %s for writing.\n", m_name);
			return(1);
	}
#endif
		if(fwrite(&m_header, sizeof(VideoDataSHeader), 1, m_fp)!=1) {
			//sprintf_s(m_error, "Cannot save header.\n");
			return(1);
		}
	}
	if(n<0 || n==(m_currentFrameNum+1)) { // append to the end
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*m_currentFrameNum;
		m_header.numFrames++;
		m_currentFrameNum++;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	} else { // overwrite existing
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
		m_currentFrameNum = n;
	}
	if(!pInfo)
		memset(&m_frameInfo, 0, STR_SIZE);
	else
		memcpy(&m_frameInfo, pInfo, STR_SIZE);
	if(fwrite(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame info.\n");
		return(1);
	}
	if(fwrite(pBMP->im, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame data.\n");
		return(1);
	}
	return(0);
}

int Y_VideoDataS::WriteFrame(BMPImageC*& pBMP, char* pInfo, int n)
{
	if(m_mode==MODE_READ) {
		//sprintf_s(m_error, "File is open for reading.\n");
		return(1);
	}
	if(m_fp==nullptr) {
		if(m_name==nullptr) {
			//sprintf_s(m_error, "Name is not set.\n");
			return(1);
		}
#ifdef _MSC_VER
	if(fopen_s(&m_fp, m_name, "wb")) {
			sprintf_s(m_error, STR_SIZE, "Cannot open %s for writing.\n", m_name);
			return(1);
	}
#else
  m_fp = fopen(m_name, "wb");
	if(!m_fp) {
			sprintf(m_error, "Cannot open %s for writing.\n", m_name);
			return(1);
	}
#endif
		if(fwrite(&m_header, sizeof(VideoDataSHeader), 1, m_fp)!=1) {
			//sprintf_s(m_error, "Cannot save header.\n");
			return(1);
		}
	}
	if(n<0 || n==(m_currentFrameNum+1)) { // append to the end
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*m_currentFrameNum;
		m_header.numFrames++;
		m_currentFrameNum++;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	} else { // overwrite existing
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
		m_currentFrameNum = n;
	}
	if(!pInfo)
		memset(&m_frameInfo, 0, STR_SIZE);
	else
		memcpy(&m_frameInfo, pInfo, STR_SIZE);
	if(fwrite(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame info.\n");
		return(1);
	}
	if(fwrite(pBMP->im, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame data.\n");
		return(1);
	}
	return(0);
}

//#########################
int Y_VideoDataS::ReadFrame(int n, IntImageG** pIntG)
{
	if(m_mode==MODE_WRITE) {
		//sprintf_s(m_error, "File is open for writing.\n");
		return(1);
	}
	__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
	if((offset+m_fullFrameSize)>m_fileSize) {
		//sprintf_s(m_error, "File is too small for frame %d.\n", n);
		return(1);
	}

#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	if(fread(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame info.\n");
		return(1);
	}
	if(fread(m_buffer, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame data.\n");
		return(1);
	}
	*pIntG = new IntImageG(m_header.w, m_header.h, (int*)m_buffer);
	return(0);
}

int Y_VideoDataS::WriteFrame(IntImageG*& pIntG, char* pInfo, int n)
{
	if(m_mode==MODE_READ) {
		//sprintf_s(m_error, "File is open for reading.\n");
		return(1);
	}
	if(m_fp==nullptr) {
		if(m_name==nullptr) {
			//sprintf_s(m_error, "Name is not set.\n");
			return(1);
		}
#ifdef _MSC_VER
	if(fopen_s(&m_fp, m_name, "wb")) {
			sprintf_s(m_error, STR_SIZE, "Cannot open %s for writing.\n", m_name);
			return(1);
	}
#else
  m_fp = fopen(m_name, "wb");
	if(!m_fp) {
			sprintf(m_error, "Cannot open %s for writing.\n", m_name);
			return(1);
	}
#endif
		if(fwrite(&m_header, sizeof(VideoDataSHeader), 1, m_fp)!=1) {
			//sprintf_s(m_error, "Cannot save header.\n");
			return(1);
		}
	}
	if(n<0 || n==(m_currentFrameNum+1)) { // append to the end
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*m_currentFrameNum;
		m_header.numFrames++;
		m_currentFrameNum++;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	} else { // overwrite existing
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
		m_currentFrameNum = n;
	}
	if(!pInfo)
		memset(&m_frameInfo, 0, STR_SIZE);
	else
		memcpy(&m_frameInfo, pInfo, STR_SIZE);
	if(fwrite(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame info.\n");
		return(1);
	}
	if(fwrite((char*)pIntG->i_im, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame data.\n");
		return(1);
	}
	return(0);
}

int Y_VideoDataS::ReadFrame(int n, DblImageG** pDblG)
{
	if(m_mode==MODE_WRITE) {
		//sprintf_s(m_error, "File is open for writing.\n");
		return(1);
	}
	__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
	if((offset+m_fullFrameSize)>m_fileSize) {
		//sprintf_s(m_error, "File is too small for frame %d.\n", n);
		return(1);
	}

#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	if(fread(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame info.\n");
		return(1);
	}
	if(fread(m_buffer, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot read frame data.\n");
		return(1);
	}
	*pDblG = new DblImageG(m_header.w, m_header.h, (double*)m_buffer);
	return(0);
}

int Y_VideoDataS::WriteFrame(DblImageG*& pDblG, char* pInfo, int n)
{
	if(m_mode==MODE_READ) {
		//sprintf_s(m_error, "File is open for reading.\n");
		return(1);
	}
	if(m_fp==nullptr) {
		if(m_name==nullptr) {
			//sprintf_s(m_error, "Name is not set.\n");
			return(1);
		}
#ifdef _MSC_VER
		if(fopen_s(&m_fp, m_name, "wb")) {
			sprintf_s(m_error, STR_SIZE, "Cannot open %s for writing.\n", m_name);
			return(1);
		}
#else
    m_fp = fopen(m_name, "wb");
		if(!m_fp) {
			sprintf(m_error, "Cannot open %s for writing.\n", m_name);
			return(1);
		}
#endif
		if(fwrite(&m_header, sizeof(VideoDataSHeader), 1, m_fp)!=1) {
			//sprintf_s(m_error, "Cannot save header.\n");
			return(1);
		}
	}
	if(n<0 || n==(m_currentFrameNum+1)) { // append to the end
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*m_currentFrameNum;
		m_header.numFrames++;
		m_currentFrameNum++;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
	} else { // overwrite existing
		__int64 offset = sizeof(VideoDataSHeader)+m_fullFrameSize*n;
#ifdef _MSC_VER
		_fseeki64(m_fp, offset, SEEK_SET);
#else
		lseek64(fileno(m_fp), offset, SEEK_SET);
#endif
		m_currentFrameNum = n;
	}
	if(!pInfo)
		memset(&m_frameInfo, 0, STR_SIZE);
	else
		memcpy(&m_frameInfo, pInfo, STR_SIZE);
	if(fwrite(&m_frameInfo, STR_SIZE, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame info.\n");
		return(1);
	}
	if(fwrite((char*)pDblG->d_im, m_frameSize, 1, m_fp)!=1) {
		//sprintf_s(m_error, "Cannot save frame data.\n");
		return(1);
	}
	return(0);
}

