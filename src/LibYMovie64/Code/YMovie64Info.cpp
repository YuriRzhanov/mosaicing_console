#include <iostream>
#include <string.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include "YMovie64Info.h"

char Y_Movie64Info::buf[1024];

Y_Movie64Info::Y_Movie64Info(char* _name)
{
	m_type[0] = 0;
	m_handler[0] = 0;
	SetName(_name);
	m_width = 100; m_height = 100;
	m_numFrames = 0;
	m_fps = 0.0;
	m_FoV = 42.5; 
	m_fl = 0.5*double(m_width)/tan(0.5*Deg2Rad(m_FoV));
	has_text = false;
}

Y_Movie64Info::~Y_Movie64Info()
{
}

void Y_Movie64Info::SetName(char* _name)
{
	if(!_name)
		m_name.clear();
	else
		m_name = _name; 
}

char* Y_Movie64Info::Info()
{
#ifdef _MSC_VER
	sprintf_s(buf, 1024, "#Info \"%s\":\n#   Microsoft AVI file format.\n"
		"#   Track length=%d   Frame size: (%d*%d)\n"
		"#   Type: %s   Compression: %s\n"
		"#   Comment: <FL=%.3lf  FoV=%.2lf>\n", 
		m_name.c_str(), m_numFrames, m_width, m_height, 
		m_type, m_handler, 
		m_fl, m_FoV);
#else
	sprintf(buf, "#Info \"%s\":\n#   Microsoft AVI file format.\n"
		"#   Track length=%d   Frame size: (%d*%d)\n"
		"#   Type: %s   Compression: %s\n"
		"#   Comment: <FL=%.3lf  FoV=%.2lf>\n", 
		m_name.c_str(), m_numFrames, m_width, m_height, 
		m_type, m_handler, 
		m_fl, m_FoV);
#endif
	return(buf);
}


char *Y_Movie64Info::ToFourChars(long l)
{
	static char s[5];
	for(int i = 0; i<4; i++) {
		s[i] = char(l>>(8*i));
	}
	s[4] = 0;
	return(s);
}
		
long Y_Movie64Info::FromFourChars(char *fc)
{
	long ckid = 0;
	for(int i = 0; i<4; i++) {
		ckid += long(fc[i]<<(8*i));
	}
	return(ckid);
}

void Y_Movie64Info::SetTypeAndHandler(long t, long h)
{
#ifdef _MSC_VER
	strcpy_s(m_type, 8, ToFourChars(t));
	strcpy_s(m_handler, 8, ToFourChars(h));
#else
	strcpy(m_type, ToFourChars(t));
	strcpy(m_handler, ToFourChars(h));
#endif
}

