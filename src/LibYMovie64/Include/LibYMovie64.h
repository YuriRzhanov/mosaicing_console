#ifndef LIBYMOVIE64_H
#define LIBYMOVIE64_H

#include <iostream>

#ifdef _MSC_VER
#include <windows.h>
#include <vfw.h>
#endif

#include <time.h>
#include "YMovie64Info.h"
#include "TimeCode.h"
#include "BMPImageG.h"
#include "BMPImageC.h"
#include "opencv2/opencv.hpp"

class Y_Movie64 {
public:
	/*!
	\brief Constructor opens existing movie file
	\param Name - name of the existing movie file (extension must be specified)
	*/
	Y_Movie64(char* name);

	/*!
	\brief Constructor creates new movie file to write into. 
	\param name - name of the file (should include extension)
	\param width - the width of the frame in pixels
	\param height - the height of the frame in pixels			
	\param bitDepth - number of bits that will be used per pixel (24 bits is equal to 3 bytes - most widely used for RGB color representation)
	\param FPS - frames per second rate
	*/
	Y_Movie64(char* name, unsigned int width, unsigned int height, unsigned int bitDepth, unsigned int numChannels, 
		double FPS, char* fourcc = "DIB ");

	~Y_Movie64();

	/*!
	\brief Reads n-th video frame data from the file to the buffer _pFrameData
	\param pFrameData - the buffer to receive the data (must be allocated in a caller routine)
	\param n - frame to read, if -1, then current frame
	\return int - Size of the buffer if succeeded
	\return -1 - if failed
	*/			
	int ReadVideoFrame(char* pFrameData, int n = -1);

	/*!
	\brief Writes the frame to the end of the AVI file
	\param pFrameData - a pointer to the frame data			
	\return 1 - if succeeded
	\return -1 - if failed
	*/
	int WriteVideoFrame(char* pFrameData, int bufferSize = 0);

	int GetCurrentVideoFrame()
	{
		if(!fail)
		{
			m_currentVideoFrame = int(m_videoCapture.get(CV_CAP_PROP_POS_FRAMES)+0.1);
			return(m_currentVideoFrame);
		}
		return(-1);
	}
	unsigned int GetWidth() const { return(m_width); }
	unsigned int GetHeight() const { return(m_height); }
	unsigned int GetBitDepth() const {return(m_bitDepth); }
	double GetFPS() const {return(m_fps); }
	unsigned int GetNumVideoFrames() const {return(m_numVideoFrames); }
	/*!
	\brief Checks the status of the current Y_AviMovie object
	\return false - if the error has been encountered, true - otherwise
	*/
	bool isOk() const { return(fail==0); }
	Y_Movie64Info* GetInfo() const { return(m_pMovieInfo); }

	// Interface to Classes4 image libraries
	int ReadVideoFrame(BMPImageG** ppImage, int N = -1);
	int ReadVideoFrame(BMPImageC** ppImage, int N = -1);
	int WriteVideoFrame(BMPImageG* pImage);
	int WriteVideoFrame(BMPImageC* pImage);
	int ReadVideoFrame(cv::Mat** ppImg, int N = -1);

	BMPImageG* OpenCVImageToBMPImageG(const cv::Mat& img);
	BMPImageC* OpenCVImageToBMPImageC(const cv::Mat& img);
	BMPImageC* OpenCVImageGrayToBMPImageC(const cv::Mat& img);
	cv::Mat* BMPImageGToOpenCVImage(const BMPImageG* pBMPG);
	cv::Mat* BMPImageCToOpenCVImage(const BMPImageC* pBMPC);

public:
	Y_Movie64Info*	m_pMovieInfo; // All the info about the movie

protected:
	cv::VideoCapture m_videoCapture;
	cv::VideoWriter m_videoWriter;
	unsigned int m_currentVideoFrame;
	unsigned int m_width;
	unsigned int m_height;
	unsigned int m_bitDepth; ///< Number of bits used per pixel (24 bits is equal to 3 bytes - most widely used for RGB color representation)
	unsigned int m_numChannels;
	double m_fps; ///< frames per second rate - not int!
	unsigned int m_numVideoFrames;
	int fail;
	string m_errorString; ///< contains the current error status
	struct tm recDate;
	CTimeCode tc;
	time_t rec_time;
};

#endif // LIBYMOVIE_H