//#pragma once
#ifndef _TIMECODE_H_
#define _TIMECODE_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#ifndef _MSC_VER
#include <string.h> // for Linux memset()
#include <cstring> // for Linux strlen()
#endif

using namespace std;
//  Convert to and from 2-digit Binary Coded Decimal
//#define  BCDTOINT( bcd )    (((bcd) >> 4) * 10 + ((bcd) & 0xf))
//#define  INTTOBCD( i )      ((((i) / 10) << 4) | ((i) % 10))

#define STGLEN 16
class CTimeCode
{
	public:		
		/// type of the variable storing each element of the timecode
		typedef int TVal;
		typedef std::vector<TVal> TValVec;
		typedef TValVec::iterator TValVecIt;
		/// storage for the timecode
		TValVec m_TimeCode;
		char stg[STGLEN];
		bool negative;

		CTimeCode():
			m_TimeCode(4), negative(false)
		{
			memset(stg, 0, sizeof(char)*STGLEN);
		}

		CTimeCode(TVal Hour, TVal Minute, TVal Second, TVal Frame):
			m_TimeCode(4), negative(false)
		{
			m_TimeCode[0] = Hour;
			m_TimeCode[1] = Minute;
			m_TimeCode[2] = Second;
			m_TimeCode[3] = Frame;
			memset(stg, 0, sizeof(char)*STGLEN);
		}

		CTimeCode(TVal Hour, TVal Minute, double Second):
			m_TimeCode(4), negative(false)
		{
			m_TimeCode[0] = Hour;
			m_TimeCode[1] = Minute;
			m_TimeCode[2] = (int)Second;
			m_TimeCode[3] = (int)((Second-m_TimeCode[2])*30.0);
			memset(stg, 0, sizeof(char)*STGLEN);
		}

		CTimeCode(const CTimeCode &TimeCode)
		{		
			m_TimeCode[0] = TimeCode.m_TimeCode[0];
			m_TimeCode[1] = TimeCode.m_TimeCode[1];
			m_TimeCode[2] = TimeCode.m_TimeCode[2];
			m_TimeCode[3] = TimeCode.m_TimeCode[3];
			//copy(TimeCode.m_TimeCode.begin(), TimeCode.m_TimeCode.end(), m_TimeCode.m_TimeCode.begin());
			negative = TimeCode.negative;
			memset(stg, 0, sizeof(char)*STGLEN);
		}

		void Set(TVal Hour, TVal Minute, TVal Second, TVal Frame)
		{
			m_TimeCode[0] = Hour;
			m_TimeCode[1] = Minute;
			m_TimeCode[2] = Second;
			m_TimeCode[3] = Frame;
		}

		TVal& operator[](int Elem) 
		{
			if((Elem<0)||(Elem>3))
#ifdef _MSC_VER
				throw(exception("Time code container boundaries exceeded"));
#else
        throw 1;
#endif
			return m_TimeCode[Elem];
		}

		void operator=(CTimeCode& TimeCode)
		{
			copy(TimeCode.m_TimeCode.begin(), TimeCode.m_TimeCode.end(), m_TimeCode.begin());
		}

		bool operator==(CTimeCode& TimeCode)
		{
			return equal(m_TimeCode.begin(), m_TimeCode.end(), TimeCode.m_TimeCode.begin());		
		}

		bool operator!=(CTimeCode& TimeCode)
		{
			long this_frame = this->ToFrame();
			long other_frame = TimeCode.ToFrame();
			return(this_frame!=other_frame);
		}

		void SetTo(TVal Val)
		{
			fill(m_TimeCode.begin(), m_TimeCode.end(), Val);
		}

		bool operator < (CTimeCode& TimeCode)
		{
			long this_frame = this->ToFrame();
			long other_frame = TimeCode.ToFrame();
			return(this_frame<other_frame);
		}

		bool operator > (CTimeCode& TimeCode)
		{
			long this_frame = this->ToFrame();
			long other_frame = TimeCode.ToFrame();
			return(this_frame>other_frame);
		}

		long ToFrame()
		{
			return((long)(((m_TimeCode[0]*60+m_TimeCode[1])*60+m_TimeCode[2])*30+m_TimeCode[3]));
		}

		void FromFrame(long fr)
		{
			m_TimeCode[0] = (int) fr%108000; fr -= m_TimeCode[0]*108000;
			m_TimeCode[1] = (int) fr%3600;   fr -= m_TimeCode[1]*3600;
			m_TimeCode[2] = (int) fr%60;     fr -= m_TimeCode[2]*60;
			m_TimeCode[3] = (int) fr;
		}

		long ToFrame_Dropped()
		{
			int TensOfMin = m_TimeCode[0]*6+m_TimeCode[1]/10;
			int RestOfMin = m_TimeCode[0]*60+m_TimeCode[1]-10*TensOfMin;
			long fr = 17982*TensOfMin;
			if(RestOfMin==0) { // 0-th minute doesn't drop frames
				fr += m_TimeCode[2]*30+m_TimeCode[3];
			} else {
				fr += 1800+(RestOfMin-1)*1798;
				if(m_TimeCode[2]!=0)
					fr += m_TimeCode[2]*30+m_TimeCode[3]-2; // Drops 2 frames in the beginning of a minute
				else { // No seconds, so possibly drop frame(s)
					if(m_TimeCode[3]>2)
						fr += m_TimeCode[3]-2;
				}
			}
			return(fr);
		}

		void FromFrame_Dropped(long fr)
		{
			int TensOfMinutes = int(fr/17982);
			// First minute in every 10 has 1800 frames, the rest have 1798, so
			// 1800+1798*9=17982 frames every 10 minutes
			int rest = fr-TensOfMinutes*17982;
	
			if(rest<1800) { // The rest is less than one minute
				m_TimeCode[1] = 10*TensOfMinutes;
			} else { // The rest is more than one minute
				int add_min = 1+int((rest-1800)/1798);
				m_TimeCode[1] = 10*TensOfMinutes+add_min;
				int dropped = (m_TimeCode[1]-int(m_TimeCode[1]/10))*2; // # of dropped frames over this many minutes
				//rest -= 1800+(add_m-1)*1798;
				rest = fr-m_TimeCode[1]*60*30+dropped;
			}
			m_TimeCode[2] = int(rest/30);
			m_TimeCode[3] = rest-m_TimeCode[2]*30;
			m_TimeCode[0] = int(m_TimeCode[1]/60);
			m_TimeCode[1] -= m_TimeCode[0]*60;
		}

		char* ToString(char separator = ':')
		{
#ifdef _MSC_VER
			sprintf_s(stg, "%02d%c%02d%c%02d%c%02d", m_TimeCode[0], separator, m_TimeCode[1], separator, 
			//StringCbPrintfA(stg, sizeof(stg), "%02d%c%02d%c%02d%c%02d", m_TimeCode[0], separator, m_TimeCode[1], separator, 
				m_TimeCode[2], separator, m_TimeCode[3]);
#else
			sprintf(stg, "%02d%c%02d%c%02d%c%02d", m_TimeCode[0], separator, m_TimeCode[1], separator, 
				m_TimeCode[2], separator, m_TimeCode[3]);
#endif
			return(stg);
		}

		void FromString(char* tc)
		{
			char *p = tc;
			negative = false;
			if(tc[0] == '-') {
				negative = true;
				p++;
			}
#ifdef _MSC_VER
			if(strlen(p)==11)
				sscanf_s(p, "%d:%d:%d:%d", &m_TimeCode[0], &m_TimeCode[1], &m_TimeCode[2], &m_TimeCode[3]);
			else if(strlen(p)==8) {
				sscanf_s(p, "%d:%d:%d", &m_TimeCode[0], &m_TimeCode[1], &m_TimeCode[2]);
				m_TimeCode[3] = 0;
			} else {
				fprintf_s(stderr, "Cannot convert string to timecode.\n");
				m_TimeCode[0] = 0; m_TimeCode[1] = 0; m_TimeCode[2] = 0; m_TimeCode[3] = 0;
			}
#else
			if(strlen(p)==11)
				sscanf(p, "%d:%d:%d:%d", &m_TimeCode[0], &m_TimeCode[1], &m_TimeCode[2], &m_TimeCode[3]);
			else if(strlen(p)==8) {
				sscanf(p, "%d:%d:%d", &m_TimeCode[0], &m_TimeCode[1], &m_TimeCode[2]);
				m_TimeCode[3] = 0;
			} else {
				fprintf(stderr, "Cannot convert string to timecode.\n");
				m_TimeCode[0] = 0; m_TimeCode[1] = 0; m_TimeCode[2] = 0; m_TimeCode[3] = 0;
			}
#endif
		}

		bool Good()
		{
			if(m_TimeCode[0]<0 || m_TimeCode[1]<0 || m_TimeCode[2]<0 || m_TimeCode[3]<0) return(false);
			if(m_TimeCode[0]>24 || m_TimeCode[1]>60 || m_TimeCode[2]>60 || m_TimeCode[3]>30) return(false);
			return(true);
		}

		char* ToString_Dropped(char separator)
		{
#ifdef _MSC_VER
			sprintf_s(stg, STGLEN, "%02d%c%02d%c%02d%c%02d", m_TimeCode[0], separator, m_TimeCode[1], separator,
				m_TimeCode[2], separator, m_TimeCode[3]);
#else
			sprintf(stg, "%02d%c%02d%c%02d%c%02d", m_TimeCode[0], separator, m_TimeCode[1], separator,
				m_TimeCode[2], separator, m_TimeCode[3]);
#endif
			return(stg);
		}

		void FromString_Dropped(char* tc)
		{
#ifdef _MSC_VER
			sscanf_s(tc, "%d:%d:%d:%d", &m_TimeCode[0], &m_TimeCode[1], &m_TimeCode[2], &m_TimeCode[3]);
#else
			sscanf(tc, "%d:%d:%d:%d", &m_TimeCode[0], &m_TimeCode[1], &m_TimeCode[2], &m_TimeCode[3]);
#endif
		}
	};		

typedef vector<CTimeCode> TTimeCodeVec;
typedef vector<CTimeCode>::iterator TTimeCodeVecIt;

#endif // _TIMECODE_H_