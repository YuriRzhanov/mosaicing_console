#pragma once

#include <stdio.h>
#include <string.h>
#ifdef _MSC_VER
#include <windows.h>
#endif
#include "defs.h"
#include "BMPImageC.h"
#include "IntImageG.h"
#include "DblImageG.h"

/*
// For compile time determination of type
template<typename X, typename Y>
struct SameType
{
   enum { result = 0 };
};
 
template<typename T>
struct SameType<T, T>
{
   enum { result = 1 };
};
*/

#define STR_SIZE 128
#define NUM_UNUSED 214

// Altogether 1 Kb. Each frame has additional 128 bytes.
typedef struct VideoDataHeader_ {
	char magic[4]; // Y_VD
	int numFrames;
	char comment[STR_SIZE];
	int w, h, channels, bpp; // bpp=8 for u_char, 32 for int, 64 for double
	float fps, empty;
	float fl, FoV_h;
	int unused[NUM_UNUSED];
} VideoDataHeader;

template <class T>
class Y_VideoData {
public:
	enum { MODE_READ = 0, MODE_WRITE = 1 };
public:
	Y_VideoData(char* name, int w, int h, int channels);
	Y_VideoData(char* name);
	~Y_VideoData();
	int Close();
	void SetBPP(int bpp) { m_header.bpp = bpp; }
	void SetFPS(float fps) { m_header.fps = fps; }
	int GetNumFrames() { return(m_header.numFrames); }
	int GetNumChannels() { return(m_header.channels); }
	int GetWidth() { return(m_header.w); }
	int GetHeight() { return(m_header.h); }
	int GetBPP() { return(m_header.bpp); }
	float GetFPS() { return(m_header.fps); }
	int GetCurrentFrameNum() { return(m_currentFrameNum); }
	bool ValidFrameForReading(int n);
	int ReadFrame(int n, BMPImageG** pBMPG);
	int ReadFrame(int n, BMPImageC** pBMPC);
	int WriteFrame(BMPImageG*& pBMPG, char* pInfo = nullptr, int n = -1);
	int WriteFrame(BMPImageC*& pBMPC, char* pInfo = nullptr, int n = -1);
	int ReadFrame(int n, IntImageG** pIntG);
	//int ReadFrame(int n, IntImageC** pIntC);
	int WriteFrame(IntImageG*& pIntG, char* pInfo = nullptr, int n = -1);
	//int WriteFrame(IntImageC*& pIntC, char* pInfo = nullptr, int n = -1);
	int ReadFrame(int n, DblImageG** pDblG);
	int ReadFrame(int n, DblImageC** pDblC);
	int WriteFrame(DblImageG*& pDblG, char* pInfo = nullptr, int n = -1);
	int WriteFrame(DblImageC*& pDblC, char* pInfo = nullptr, int n = -1);

public:
	int fail;
	int m_mode;
	char* m_name;
	FILE* m_fp;
	__int64 m_fileSize;
	int m_frameSize, m_fullFrameSize;
	VideoDataHeader m_header;
	int m_currentFrameNum;
	char m_frameInfo[STR_SIZE];
	char m_error[STR_SIZE];
	T* m_buffer;
};
