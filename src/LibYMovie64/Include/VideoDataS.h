#pragma once

#include <stdio.h>
#include <string.h>
#ifdef _MSC_VER
#include <windows.h>
#else
#define _LARGEFILE64_SOURCE
#include <sys/types.h>
#include <unistd.h>
#endif
#include "BMPImageC.h"
#include "IntImageG.h"
#include "DblImageG.h"

#define STR_SIZE 128
#define NUM_UNUSED 214

// Altogether 1 Kb. Each frame has additional 128 bytes.
typedef struct VideoDataSHeader_ {
	char magic[4]; // Y_VD
	int numFrames;
	char comment[STR_SIZE];
	int w, h, channels, bpp; // bpp=8 for u_char, 32 for int, 64 for double
	float fps, empty;
	float fl, FoV_h;
	int unused[NUM_UNUSED];
} VideoDataSHeader;

class Y_VideoDataS {
public:
	enum { MODE_READ = 0, MODE_WRITE = 1 };
public:
	Y_VideoDataS(char* name, int w, int h, int channels, int bpp);
	Y_VideoDataS(char* name);
	~Y_VideoDataS();
	int Close();
	void SetBPP(int bpp) { m_header.bpp = bpp; }
	void SetFPS(float fps) { m_header.fps = fps; }
	void SetFocalLength(float fl) { m_header.fl = fl; m_header.FoV_h = 2.0f*atan(0.5f*m_header.w/fl); }
	void SetFoV(float FoV) { m_header.FoV_h = FoV; m_header.fl = 0.5f*m_header.w/tan(0.5f*FoV); }
	bool IsColour() { return(m_header.channels==3); }
	int GetNumFrames() { return(m_header.numFrames); }
	int GetNumChannels() { return(m_header.channels); }
	int GetWidth() { return(m_header.w); }
	int GetHeight() { return(m_header.h); }
	int GetBPP() { return(m_header.bpp); }
	float GetFPS() { return(m_header.fps); }
	float GetFocalLength() { return(m_header.fl); }
	float GetFoV() { return(m_header.FoV_h); }
	int GetCurrentFrameNum() { return(m_currentFrameNum); }
	bool ValidFrameForReading(int n);
	int ReadFrame(int n, BMPImageG** pBMPG);
	int ReadFrame(int n, BMPImageC** pBMPC);
	int WriteFrame(BMPImageG*& pBMPG, char* pInfo = nullptr, int n = -1);
	int WriteFrame(BMPImageC*& pBMPC, char* pInfo = nullptr, int n = -1);
	int ReadFrame(int n, IntImageG** pIntG);
	//int ReadFrame(int n, IntImageC** pIntC);
	int WriteFrame(IntImageG*& pIntG, char* pInfo = nullptr, int n = -1);
	//int WriteFrame(IntImageC*& pIntC, char* pInfo = nullptr, int n = -1);
	int ReadFrame(int n, DblImageG** pDblG);
	//int ReadFrame(int n, DblImageC** pDblC);
	int WriteFrame(DblImageG*& pDblG, char* pInfo = nullptr, int n = -1);
	//int WriteFrame(DblImageC*& pDblC, char* pInfo = nullptr, int n = -1);

public:
	int fail;
	int m_mode;
	char* m_name;
	FILE* m_fp;
	__int64 m_fileSize;
	int m_frameSize, m_fullFrameSize;
	VideoDataSHeader m_header;
	int m_currentFrameNum;
	char m_frameInfo[STR_SIZE];
	char m_error[STR_SIZE];
	char* m_buffer;
};
