#ifndef YMOVIE64INFO_H
#define YMOVIE64INFO_H

#include "defs.h"

//#ifndef _MSC_VER
//#define nullptr NULL
//#endif
//
//#ifndef Deg2Rad
//#define Deg2Rad(a)	((a)*M_PI/180.0)
//#endif
//#ifndef Rad2Deg
//#define Rad2Deg(a)	((a)*180.0/M_PI)
//#endif

using namespace std;

class Y_Movie64Info
{
public:
	Y_Movie64Info(char* _name = nullptr);
	~Y_Movie64Info();
	void SetName(char* _name);
	static char *ToFourChars(long l);
	static long FromFourChars(char *fc);
	void SetTypeAndHandler(long t, long h);
	// Same as in AVIMovie and CAVIon
	char* Info();
public:
	static char buf[1024];
	string m_name;
	char m_type[8];
	char m_handler[8];
	int m_width, m_height;
	int m_bitDepth;
	int m_numChannels;
	double m_fps;
	int m_numFrames;
	double m_fl;
	double m_FoV;
	bool has_text;
	bool used_for_reading; // Indicates what for file was open
};

#endif // YMOVIE64INFO_H
