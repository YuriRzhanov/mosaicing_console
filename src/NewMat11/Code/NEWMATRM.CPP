//$$newmatrm.cpp                         rectangular matrix operations

// Copyright (C) 1991,2,3,4: R B Davies

#define WANT_MATH

#include "NEWMAT.H"
#include "NEWMATRM.H"

#ifdef use_namespace
namespace NEWMAT {
#endif

#ifdef DO_REPORT
#define REPORT { static ExeCounter ExeCount(__LINE__,12); ++ExeCount; }
#else
#define REPORT {}
#endif


// operations on rectangular matrices


void RectMatrixRow::Reset (const Matrix& M, Y_INT row, Y_INT skip, Y_INT length)
{
   REPORT
   RectMatrixRowCol::Reset
      ( M.Store()+row*M.Ncols()+skip, length, 1, M.Ncols() );
}

void RectMatrixRow::Reset (const Matrix& M, Y_INT row)
{
   REPORT
   RectMatrixRowCol::Reset( M.Store()+row*M.Ncols(), M.Ncols(), 1, M.Ncols() );
}

void RectMatrixCol::Reset (const Matrix& M, Y_INT skip, Y_INT col, Y_INT length)
{
   REPORT
   RectMatrixRowCol::Reset
      ( M.Store()+col+skip*M.Ncols(), length, M.Ncols(), 1 );
}

void RectMatrixCol::Reset (const Matrix& M, Y_INT col)
{
   REPORT
   RectMatrixRowCol::Reset( M.Store()+col, M.Nrows(), M.Ncols(), 1 );
}


double RectMatrixRowCol::SumSquare() const
{
   REPORT
   long double sum = 0.0; Y_INT i = n; double* s = store; Y_INT d = spacing;
   // while (i--) { sum += (long_double)*s * *s; s += d; }
   if (i) for(;;)
      { sum += (long double)*s * *s; if (!(--i)) break; s += d; }
   return (double)sum;
}

double RectMatrixRowCol::operator*(const RectMatrixRowCol& rmrc) const
{
   REPORT
   long double sum = 0.0; Y_INT i = n;
   double* s = store; Y_INT d = spacing;
   double* s1 = rmrc.store; Y_INT d1 = rmrc.spacing;
   if (i!=rmrc.n)
   {
      Tracer tr("newmatrm");
      Throw(InternalException("Dimensions differ in *"));
   }
   // while (i--) { sum += (long_double)*s * *s1; s += d; s1 += d1; }
   if (i) for(;;)
      { sum += (long double)*s * *s1; if (!(--i)) break; s += d; s1 += d1; }
   return (double)sum;
}

void RectMatrixRowCol::AddScaled(const RectMatrixRowCol& rmrc, double r)
{
   REPORT
   Y_INT i = n; double* s = store; Y_INT d = spacing;
   double* s1 = rmrc.store; Y_INT d1 = rmrc.spacing;
   if (i!=rmrc.n)
   {
      Tracer tr("newmatrm");
      Throw(InternalException("Dimensions differ in AddScaled"));
   }
   // while (i--) { *s += *s1 * r; s += d; s1 += d1; }
   if (i) for (;;)
      { *s += *s1 * r; if (!(--i)) break; s += d; s1 += d1; }
}

void RectMatrixRowCol::Divide(const RectMatrixRowCol& rmrc, double r)
{
   REPORT
   Y_INT i = n; double* s = store; Y_INT d = spacing;
   double* s1 = rmrc.store; Y_INT d1 = rmrc.spacing;
   if (i!=rmrc.n)
   {
      Tracer tr("newmatrm");
      Throw(InternalException("Dimensions differ in Divide"));
   }
   // while (i--) { *s = *s1 / r; s += d; s1 += d1; }
   if (i) for (;;) { *s = *s1 / r; if (!(--i)) break; s += d; s1 += d1; }
}

void RectMatrixRowCol::Divide(double r)
{
   REPORT
   Y_INT i = n; double* s = store; Y_INT d = spacing;
   // while (i--) { *s /= r; s += d; }
   if (i) for (;;) { *s /= r; if (!(--i)) break; s += d; }
}

void RectMatrixRowCol::Negate()
{
   REPORT
   Y_INT i = n; double* s = store; Y_INT d = spacing;
   // while (i--) { *s = - *s; s += d; }
   if (i) for (;;) { *s = - *s; if (!(--i)) break; s += d; }
}

void RectMatrixRowCol::Zero()
{
   REPORT
   Y_INT i = n; double* s = store; Y_INT d = spacing;
   // while (i--) { *s = 0.0; s += d; }
   if (i) for (;;) { *s = 0.0; if (!(--i)) break; s += d; }
}

void ComplexScale(RectMatrixCol& U, RectMatrixCol& V, double x, double y)
{
   REPORT
   Y_INT n = U.n;
   if (n != V.n)
   {
      Tracer tr("newmatrm");
      Throw(InternalException("Dimensions differ in ComplexScale"));
   }
   double* u = U.store; double* v = V.store; 
   Y_INT su = U.spacing; Y_INT sv = V.spacing;
   //while (n--)
   //{
   //   double z = *u * x - *v * y;  *v =  *u * y + *v * x;  *u = z;
   //   u += su;  v += sv;
   //}
   if (n) for (;;)
   {
      double z = *u * x - *v * y;  *v =  *u * y + *v * x;  *u = z;
      if (!(--n)) break;
      u += su;  v += sv;
   }
}

void Rotate(RectMatrixCol& U, RectMatrixCol& V, double tau, double s)
{
   REPORT
   //  (U, V) = (U, V) * (c, s)  where  tau = s/(1+c), c^2 + s^2 = 1
   Y_INT n = U.n;
   if (n != V.n)
   {
      Tracer tr("newmatrm");
      Throw(InternalException("Dimensions differ in Rotate"));
   }
   double* u = U.store; double* v = V.store;
   Y_INT su = U.spacing; Y_INT sv = V.spacing;
   //while (n--)
   //{
   //   double zu = *u; double zv = *v;
   //   *u -= s * (zv + zu * tau); *v += s * (zu - zv * tau);
   //   u += su;  v += sv;
   //}
   if (n) for(;;)
   {
      double zu = *u; double zv = *v;
      *u -= s * (zv + zu * tau); *v += s * (zu - zv * tau);
      if (!(--n)) break;
      u += su;  v += sv;
   }
}


// misc procedures for numerical things

double pythag(double f, double g, double& c, double& s)
// return z=sqrt(f*f+g*g), c=f/z, s=g/z
// set c=1,s=0 if z==0
// avoid floating point overflow or divide by zero
{
   if (f==0 && g==0) { c=1.0; s=0.0; return 0.0; }
   double af = f>=0 ? f : -f;
   double ag = g>=0 ? g : -g;
   if (ag<af)
   {
      REPORT
      double h = g/f; double sq = sqrt(1.0+h*h);
      if (f<0) sq = -sq;           // make return value non-negative
      c = 1.0/sq; s = h/sq; return sq*f;
   }
   else
   {
      REPORT
      double h = f/g; double sq = sqrt(1.0+h*h);
      if (g<0) sq = -sq;
      s = 1.0/sq; c = h/sq; return sq*g;
   }
}

#ifdef use_namespace
}
#endif


