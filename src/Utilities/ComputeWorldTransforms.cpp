#include "Transform.h"

using namespace std;

// Input transforms 'rel_trans' are relative
// Output transforms 'recs' are world transforms built sequentially
// Error function calculated in optimization will be very small for those
// links that have been built sequentially. However when branches coming from
// different directions meet, there is a big error, contributing to the total
// penalty function.
// September 2007: instead of 'verbose' flag, FILE for debug output is included.
//
int ComputeWorldTransforms(TransformRecord *rel_trans, int N_links,
	TransformRecord *world_trans, int N_max_frames,
	int anchor_frame_n, Hmgr& anchor_hm, double focal_length,
	char* anchor_mark, FILE* fp_debug)
{
	int W = rel_trans[0].w, H = rel_trans[0].h;
	int N_frames = 0;
	world_trans[N_frames].SetHmgr(anchor_hm);
	world_trans[N_frames].SetAttr(anchor_frame_n, anchor_frame_n, -1.0, 0.0);
	world_trans[N_frames].anchored = 1;
	world_trans[N_frames].essential = true;
	N_frames++;
	int found_another = 1;
	Hmgr to_world, rel, inv_rel;
	if(fp_debug) {
		fprintf(fp_debug, "%5d %5d (anchor)\n", N_frames, anchor_frame_n);
	}

	// Generation number describing order in which mosaic is built.
	// Frame numbers not necessarily start with 0, so array FrameGen must
	// be as big as the biggest frame number. Find this number first.
	int biggest_fn = 0;
	for(int b = 0; b<N_links; b++) {
		biggest_fn = max(biggest_fn, rel_trans[b].base_f_n);
		biggest_fn = max(biggest_fn, rel_trans[b].f_n);
	}
	int* FrameGen = new int[biggest_fn+1];
	for(int k = 0; k<biggest_fn+1; k++) FrameGen[k] = -1;
	FrameGen[anchor_frame_n] = 0;
	
	while(found_another) {
		found_another = 0;
		// Go through all currently existing world records
		for(int i = 0; i<N_frames; i++) {
			// Go through all transforms, search for transforms
			// involving this world record
			for(int j = 0; j<N_links; j++) {
				if(rel_trans[j].base_f_n==world_trans[i].f_n) {
					// Make sure that the frame we are about to add hasn't beed already added
					int already_added = 0;
					for(int k = 0; k<N_frames; k++) {
						if(world_trans[k].f_n==rel_trans[j].f_n) already_added = 1;
					}
					// If transform is bad, don't follow it
					if(rel_trans[j].MarkBad())
					//if(rel_trans[j].MarkHas("B") || rel_trans[j].MarkHas("X"))
						already_added = 1;
					// Already used relative transforms are marked as 'essential'
					if(rel_trans[j].essential)
						already_added = 1;
					if(!already_added) {
						to_world = world_trans[i].GetHmgr();
						rel = rel_trans[j].GetHmgr();
						to_world = to_world * rel;
						world_trans[N_frames].SetHmgr(to_world);
						world_trans[N_frames].SetAttr(rel_trans[j].f_n, rel_trans[j].f_n, -1.0, 0.0);
						world_trans[N_frames].MarkSet("-"); world_trans[N_frames].type = 'w'; 
						world_trans[N_frames].essential = true;
						if(fp_debug)
							fprintf(fp_debug, "%5d %5d (attached to %5d)\n", N_frames, rel_trans[j].f_n, world_trans[i].f_n);
						// FrameGen of the predecessor should not be -1!
						if(FrameGen[rel_trans[j].base_f_n]==-1)
							printf("Wrong FrameGen!\n");
						FrameGen[rel_trans[j].f_n] = FrameGen[rel_trans[j].base_f_n]+1;
						if(anchor_mark) {
							// Mark world trans as anchored if necessary
							// 'i' is the # new frame is attached to
							// 'j' is the # of connecting link
							if(world_trans[i].anchored && rel_trans[j].MarkHas(anchor_mark))
								world_trans[N_frames].anchored = 1;
						}
						// Don't use this transform any more:
						rel_trans[j].essential = true;
						//rel_trans[j].base_f_n = -1;
						//rel_trans[j].f_n = -1;

						N_frames++;
						found_another = 1; 
					}
				}
				if(rel_trans[j].f_n==world_trans[i].f_n) {
					// Make sure that the frame we are about to add hasn't beed already added
					int already_added = 0;
					for(int k = 0; k<N_frames; k++) {
						if(world_trans[k].f_n==rel_trans[j].base_f_n) already_added = 1;
					}
					// If transform is bad, don't follow it
					if(rel_trans[j].MarkBad())
					//if(rel_trans[j].MarkHas("B") || rel_trans[j].MarkHas("X"))
						already_added = 1;
					// Already used relative transforms are marked as 'essential'
					if(rel_trans[j].essential)
						already_added = 1;
					if(!already_added) {
						to_world = world_trans[i].GetHmgr();
						rel = rel_trans[j].GetHmgr();
						inv_rel = rel.Invert();
						to_world = to_world * inv_rel;
						world_trans[N_frames].SetHmgr(to_world);
						world_trans[N_frames].SetAttr(rel_trans[j].base_f_n, rel_trans[j].base_f_n, -1.0, 0.0);
						world_trans[N_frames].MarkSet("-"); world_trans[N_frames].type = 'w'; 
						world_trans[N_frames].essential = true;
						if(fp_debug)
							fprintf(fp_debug, "%5d %5d (attached to %5d)\n", N_frames, rel_trans[j].f_n, world_trans[i].f_n);
						// FrameGen of the predecessor should not be -1!
						if(FrameGen[rel_trans[j].f_n]==-1)
							printf("Wrong FrameGen!\n");
						FrameGen[rel_trans[j].base_f_n] = FrameGen[rel_trans[j].f_n]+1;
						if(anchor_mark) {
							// Mark world trans as anchored if necessary
							// 'i' is the # new frame is attached to
							// 'j' is the # of connecting link
							if(world_trans[i].anchored && rel_trans[j].MarkHas(anchor_mark))
								world_trans[N_frames].anchored = 1;
						}
						// Don't use this transform any more:
						rel_trans[j].essential = true;
						//rel_trans[j].base_f_n = -1;
						//rel_trans[j].f_n = -1;

						N_frames++;
						found_another = 1; 
					}
				}
			}
		}
	}

	if(fp_debug) {
		fprintf(fp_debug, "\n\nOrder:\n");
		for(int i = 0; i<N_frames; i++)
			fprintf(fp_debug, "%d %d\n", i, world_trans[i].f_n);

		fprintf(fp_debug, "\nGenerations:\n");
		for(int i = 0; i<biggest_fn+1; i++)
			fprintf(fp_debug, "%d %d\n", i, FrameGen[i]);

		//fprintf(fp_debug, "\nAnchoring:\n");
		//for(int i = 0; i<N_frames; i++)
		//	fprintf(fp_debug, "%d %d %d\n", i, world_trans[i].f_n, world_trans[i].anchored);
	}
	// Rearrange in ascending order:
	TransformRecord *tmp = new TransformRecord[N_frames];
	for(int i = 0; i<N_frames; i++) {
		tmp[i].Copy(world_trans[i]);
	}
	for(int l = 0; l<N_frames; l++) {
		// Choose smallest number
		int smallest = 10000, order = -1;
		for(int m = 0; m<N_frames; m++) {
			if(tmp[m].f_n<smallest) {
				smallest = tmp[m].f_n;
				order = m;
			}
		}
		// Copy 
		world_trans[l].Copy(tmp[order]);
		world_trans[l].w = W; world_trans[l].h = H; world_trans[l].focal_length = focal_length;
		// Remove from the list
		tmp[order].f_n = 10000;
	}
	delete [] tmp;
	delete [] FrameGen;
	return(N_frames);
}

// Simple version for construction of world transforms from cost function evaluation routine
int ComputeWorldTransforms_Simple(TransformRecord *rel_trans, int N_links,
	TransformRecord *world_trans, int N_max_frames,
	int anchor_frame_n)
{
	int W = rel_trans[0].w, H = rel_trans[0].h;
	double focal_length = rel_trans[0].focal_length;
	int N_frames = 0;
	Hmgr anchor_hm; // unit
	world_trans[N_frames].SetHmgr(anchor_hm);
	world_trans[N_frames].SetAttr(anchor_frame_n, anchor_frame_n, -1.0, 0.0);
	world_trans[N_frames].anchored = 1;
	N_frames++;
	int found_another = 1;
	Hmgr to_world, rel, inv_rel;

	while(found_another) {
		found_another = 0;
		// Go through all currently existing world records
		for(int i = 0; i<N_frames; i++) {
			// Go through all transforms, search for transforms
			// involving this world record
			for(int j = 0; j<N_links; j++) {
				if(rel_trans[j].base_f_n==world_trans[i].f_n) {
					// Make sure that the frame we are about to add hasn't beed already added
					int already_added = 0;
					for(int k = 0; k<N_frames; k++) {
						if(world_trans[k].f_n==rel_trans[j].f_n) already_added = 1;
					}
					// If transform is bad, don't follow it
					if(rel_trans[j].MarkBad())
					//if(rel_trans[j].MarkHas("B") || rel_trans[j].MarkHas("X"))
						already_added = 1;
					// Already used relative transforms are marked as 'essential'
					if(rel_trans[j].essential)
						already_added = 1;
					if(!already_added) {
						to_world = world_trans[i].GetHmgr();
						rel = rel_trans[j].GetHmgr();
						to_world = to_world * rel;
						world_trans[N_frames].SetHmgr(to_world);
						world_trans[N_frames].SetAttr(rel_trans[j].f_n, rel_trans[j].f_n, -1.0, 0.0);
						world_trans[N_frames].type = 'w'; 
						world_trans[N_frames].essential = true;
						// Don't use this transform any more:
						rel_trans[j].essential = true;

						N_frames++;
						found_another = 1; 
					}
				}
				if(rel_trans[j].f_n==world_trans[i].f_n) {
					// Make sure that the frame we are about to add hasn't beed already added
					int already_added = 0;
					for(int k = 0; k<N_frames; k++) {
						if(world_trans[k].f_n==rel_trans[j].base_f_n) already_added = 1;
					}
					// If transform is bad, don't follow it
					if(rel_trans[j].MarkBad())
					//if(rel_trans[j].MarkHas("B") || rel_trans[j].MarkHas("X"))
						already_added = 1;
					// Already used relative transforms are marked as 'essential'
					if(rel_trans[j].essential)
						already_added = 1;
					if(!already_added) {
						to_world = world_trans[i].GetHmgr();
						rel = rel_trans[j].GetHmgr();
						inv_rel = rel.Invert();
						to_world = to_world * inv_rel;
						world_trans[N_frames].SetHmgr(to_world);
						world_trans[N_frames].SetAttr(rel_trans[j].base_f_n, rel_trans[j].base_f_n, -1.0, 0.0);
						world_trans[N_frames].type = 'w'; 
						world_trans[N_frames].essential = true;
						// Don't use this transform any more:
						rel_trans[j].essential = true;

						N_frames++;
						found_another = 1; 
					}
				}
			}
		}
	}

	// Rearrange in ascending order:
	TransformRecord *tmp = new TransformRecord[N_frames];
	for(int i = 0; i<N_frames; i++) {
		tmp[i].Copy(world_trans[i]);
	}
	for(int l = 0; l<N_frames; l++) {
		// Choose smallest number
		int smallest = 10000, order = -1;
		for(int m = 0; m<N_frames; m++) {
			if(tmp[m].f_n<smallest) {
				smallest = tmp[m].f_n;
				order = m;
			}
		}
		// Copy 
		world_trans[l].Copy(tmp[order]);
		world_trans[l].w = W; world_trans[l].h = H; world_trans[l].focal_length = focal_length;
		// Remove from the list
		tmp[order].f_n = 10000;
	}
	delete [] tmp;
	return(N_frames);
}
