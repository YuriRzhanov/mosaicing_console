#include "Transform.h"
//#include "LoopDescriptor.h"

// Estimate encompass only transforms that are marked as 'essential'.
// Typically calculating world transforms from relative sets almost all of them
// as essential, so if we forget a few, no big deal. However this provides
// with a mechanism to avoid those that are not needed.
// Return number of 'encompassed' frames.
int EstimateExtentAndAnchorPosition(TransformRecord* worldtrans, int N_frames,
		int anchor_frame_n, Y_Rect<int>& frame_size, double focal_length,
		Y_Rect<int>& mosaic_size, Point2<int>& anchor_position) 
{
	TransformRecord *anchor_frame = TransformRecord::FindRecordFromFrameNumber(
		worldtrans, N_frames, anchor_frame_n);
	if(!anchor_frame) {
		printf("Cannot find anchor frame\n");
		return(-1);
	}
	
	// Find projection of center of anchor frame:
	Coord2 c(0.5*frame_size.w, 0.5*frame_size.h); 
		c.SetSize(frame_size.w, frame_size.h);
		c.SetFocalLength(focal_length);
	Hmgr hm_anchor = anchor_frame->GetHmgr();
	Coord2 c_image = hm_anchor.Map(c);
	//TRACE("C_image: %lf %lf\n", c_image.X(), c_image.Y());
	
	
	Y_Rect<double> extent(10000.0, 10000.0, -10000.0, -10000.0);
	Hmgr hm;
	Coord2 corner, corner_image; 
		corner.SetSize(frame_size.w, frame_size.h);
		corner.SetFocalLength(focal_length);
	
	// For each frame in the provided range estimate positions
	// of all four corners:
	int num_encompassed = 0;
	for(int i = 0; i<N_frames; i++) {
		if(worldtrans[i].essential == false) continue;
		hm = worldtrans[i].GetHmgr();
		corner.Set(0, 0); 
		corner_image = hm.Map(corner);
		extent.Encompass(corner_image);
		corner.Set(frame_size.w-1, 0); 
		corner_image = hm.Map(corner);
		extent.Encompass(corner_image);
		corner.Set(0, frame_size.h-1); 
		corner_image = hm.Map(corner);
		extent.Encompass(corner_image);
		corner.Set(frame_size.w-1, frame_size.h-1); 
		corner_image = hm.Map(corner);
		extent.Encompass(corner_image);
		num_encompassed++;
	}
	
	// Estimate 5 percent of extent:
	double width_5 = (extent.w-extent.x)*0.05;
	double height_5 = (extent.h-extent.y)*0.05;
	// Increase size:
	extent.x -= width_5; extent.w += width_5;
	extent.y -= height_5; extent.h += height_5;
	
	// Restore width/height:
	extent.w -= extent.x; extent.h -= extent.y;
	
	// This now keeps anchor position:
	extent.x = -extent.x+c_image.X();
	extent.y = -extent.y+c_image.Y();

	//TRACE("MOSAIC-SIZE		%d\t%d\n", int(extent.w), int(extent.h));
	//TRACE("START			%d\t%d\n", int(extent.x), int(extent.y));

	mosaic_size.Set(0, 0, int(extent.w), int(extent.h));
	anchor_position.Set(int(extent.x), int(extent.y));
	return(num_encompassed);
}

/*
int EstimateExtentAndAnchorPosition(TransformRecord worldtrans[], int N_frames, int anchor_frame_n,
	LoopDescriptor* loop_descr, Y_Rect<int>& frame, double fl, 
	Y_Rect<int>& mosaic_size, Point2<int>& anchor_position) 
{
	TransformRecord *anchor_frame = TransformRecord::FindRecordFromFrameNumber(
		worldtrans, N_frames, anchor_frame_n);
	if(!anchor_frame) {
		printf("Cannot find anchor frame\n");
		return(-1);
	}
	
	// Find projection of center of anchor frame:
	Coord2 c(0.5*frame.w, 0.5*frame.h); 
		c.SetSize(frame.w, frame.h);
		c.SetFocalLength(fl);
	Hmgr hm_anchor = anchor_frame->GetHmgr();
	Coord2 c_image = hm_anchor.Map(c);
	//TRACE("C_image: %lf %lf\n", c_image.X(), c_image.Y());
	
	
	Y_Rect<double> extent(100000.0, 100000.0, -100000.0, -100000.0);
	Hmgr hm;
	Coord2 corner, corner_image; 
		corner.SetSize(frame.w, frame.h);
		corner.SetFocalLength(fl);
	
	// For each frame in the loop calculate positions
	// of all four corners and find bounding box for all of them:
	for(int i = 0; i<N_frames; i++) {
		//Check frame number against loop descruption
		if(!loop_descr->FrameInLoop(worldtrans[i].f_n)) continue;
		hm = worldtrans[i].GetHmgr();

		//printf("%.3lf %.3lf %.3lf %.3lf %.3lf %.3lf \n", 
		//	hm[0], hm[1], hm[2], hm[3], hm[4], hm[5]);

		corner.Set(0, 0); 
		corner_image = hm.Map(corner);
		//printf("     %.3lf  %.3lf\n", corner_image.x, corner_image.y);
		extent.Encompass(corner_image);
		corner.Set(frame.w-1, 0); 
		corner_image = hm.Map(corner);
		//printf("     %.3lf  %.3lf\n", corner_image.x, corner_image.y);
		extent.Encompass(corner_image);
		corner.Set(0, frame.h-1); 
		corner_image = hm.Map(corner);
		//printf("     %.3lf  %.3lf\n", corner_image.x, corner_image.y);
		extent.Encompass(corner_image);
		corner.Set(frame.w-1, frame.h-1); 
		corner_image = hm.Map(corner);
		//printf("     %.3lf  %.3lf\n", corner_image.x, corner_image.y);
		extent.Encompass(corner_image);
	}
	
	// Estimate 5 percent of extent:
	double width_5 = (extent.w-extent.x)*0.05;
	double height_5 = (extent.h-extent.y)*0.05;
	// Increase size:
	extent.x -= width_5; extent.w += width_5;
	extent.y -= height_5; extent.h += height_5;
	
	// Restore width/height:
	extent.w -= extent.x; extent.h -= extent.y;
	
	// This now keeps anchor position:
	extent.x = -extent.x+c_image.X();
	extent.y = -extent.y+c_image.Y();

	//TRACE("MOSAIC-SIZE		%d\t%d\n", int(extent.w), int(extent.h));
	//TRACE("START			%d\t%d\n", int(extent.x), int(extent.y));

	mosaic_size.w = int(extent.w); mosaic_size.h = int(extent.h);
	anchor_position.x = int(extent.x); anchor_position.y = int(extent.y);
	return(0);
}
*/
