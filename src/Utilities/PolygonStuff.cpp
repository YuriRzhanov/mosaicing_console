#include "stdafx.h"
#include "Point2.h"
#include "BMPImage.h"
#include "Hmgr.h"
#include "LibPolygon.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace libpolygon;
using namespace NEWMAT;

extern int debug;

// Given image, its homography, and starting coordinate, return footprint polygon
CPolygon* PolygonFromHmgr(Hmgr& hm, Y_Image* im, Coord2* start = NULL)
{
	Coord2 c; 
	c.SetImage(im);
	CPolygon* pPolygon = new CPolygon();
	vector<Point2<int> > v;
	c.Set(0,             0); c = hm.Map(c); if(start) c += *start; v.push_back(Point2<int>(int(c.X()+0.5),int(c.Y()+0.5)));
	c.Set(im->w-1,       0); c = hm.Map(c); if(start) c += *start; v.push_back(Point2<int>(int(c.X()+0.5),int(c.Y()+0.5)));
	c.Set(im->w-1, im->h-1); c = hm.Map(c); if(start) c += *start; v.push_back(Point2<int>(int(c.X()+0.5),int(c.Y()+0.5)));
	c.Set(0,       im->h-1); c = hm.Map(c); if(start) c += *start; v.push_back(Point2<int>(int(c.X()+0.5),int(c.Y()+0.5)));
	CContour* pContour = new CContour(v, 0); // '0' because not a hole
	pPolygon->AddContour(*pContour);
	delete pContour;
	return(pPolygon);
}

double OverlapPolygons(Hmgr& hm, Y_Image* im1, Y_Image* im2)
{
	Hmgr unit;
	CPolygon* Base = PolygonFromHmgr(unit, im1);
	CPolygon* Deri = PolygonFromHmgr(hm, im2);
	//CPolygon* pIntersection = new CPolygon();
	//gpc_cpp::Intersection(Base, Deri, pIntersection);
	CPolygon* pIntersection = libpolygon::Intersection(Base, Deri);

	double overlap_percent = 100.0*pIntersection->Area()/Base->Area();
	delete Base;
	delete Deri;
	delete pIntersection;
	return(overlap_percent);
}

double OverlapPolygons(Hmgr& hm1, Y_Image* im1, Hmgr& hm2, Y_Image* im2)
{
	Hmgr unit;
	CPolygon* P1 = PolygonFromHmgr(hm1, im1);
	CPolygon* P2 = PolygonFromHmgr(hm2, im2);
	//CPolygon* pIntersection = new CPolygon();
	//gpc_cpp::Intersection(P1, P2, pIntersection);
	CPolygon* pIntersection = libpolygon::Intersection(P1, P2);
	//P1->Print(stdout, "P1");
	//P2->Print(stdout, "P2");
	//pIntersection->Print(stdout, "Intersection");

	double overlap_percent = 100.0*pIntersection->Area();
	if(P1->Area()>P2->Area())
		overlap_percent /= P2->Area();
	else
		overlap_percent /= P1->Area();
	delete P1;
	delete P2;
	delete pIntersection;
	return(overlap_percent);
}

// Check whether footprint intersects with the requested mosaic canvas
bool FrameOverlapsCanvas(Y_Rect<int>& mosaic_size, Y_Image* im, Hmgr& hm, Coord2& start)
{
	CPolygon* pCanvas = new CPolygon();
	vector<Point2<int>> v;
	v.push_back(Point2<int>(0, 0));
	v.push_back(Point2<int>(mosaic_size.w-1, 0));
	v.push_back(Point2<int>(mosaic_size.w-1, mosaic_size.h-1));
	v.push_back(Point2<int>(0, mosaic_size.h-1));
	CContour* pContour = new CContour(v, 0);
	pCanvas->AddContour(*pContour);
	//pCanvas->Print(stdout);

	CPolygon* pFootprint = PolygonFromHmgr(hm, im, &start);
	//pFootprint->Print(stdout);
	CPolygon* pIntersection = libpolygon::Intersection(pFootprint, pCanvas);
	int k = pIntersection->NumContours();
	delete pContour;
	delete pCanvas;
	delete pFootprint;
	delete pIntersection;

	return(k>=1);
}

// In Utilities/PolygonStuff.cpp
// Intersect footprint with mosaicPolygon consisting of zero/one/many contours.
// Return intersection polygon. For each contour optimal graph cut is required.
CPolygon* IntersectionWithMosaicPolygon(CPolygon* pFootprint, CPolygon* pMosaic)
{
	CPolygon* pIntersection = new CPolygon();
	libpolygon::Intersection(pFootprint, pMosaic, pIntersection);
	return(pIntersection);
}

// In Utilities/PolygonStuff.cpp
// Union current footrpint with mosaicPolygon consisting of zero/one/several contours.
CPolygon* AddFootprintToMosaicPolygon(CPolygon* pFootprint, CPolygon* pMosaic)
{
	CPolygon* pUnion = new CPolygon();
	libpolygon::Union(pFootprint, pMosaic, pUnion);
	delete pMosaic;
	return(pUnion);
}

