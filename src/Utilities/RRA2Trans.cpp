#include "Transform.h"

// Convert RRA's to relative transformations.
// If "no_zoom" is specified, ignore whatever zoom RARecord has
TransformRecord* RRA2Trans(RARecord rel_recs[], int N_records, double focal_length, bool no_zoom)
{
	TransformRecord *trans = new TransformRecord[N_records];
	RARecord rar;
	Hmgr rel_hm;
	Collocations* pCol;
	for(int i = 0; i<N_records; i++) {
		if(no_zoom) {
			rar.Copy(rel_recs[i]);
			rar.zoom = 1.0;
			//rel_hm = Hmgr::FromRecord(rar, focal_length);
			rel_hm = rar.GetHmgr();
		} else {
			//rel_hm = Hmgr::FromRecord(rel_recs[i], focal_length);
			rel_hm = rel_recs[i].GetHmgr();
		}
		trans[i].SetHmgr(rel_hm);
		trans[i].SetAttr(rel_recs[i].base_f_n, rel_recs[i].f_n, rel_recs[i].av_error, 
			rel_recs[i].overlap_percent);
		trans[i].MarkSet(rel_recs[i].mark);
		trans[i].w = rel_recs[i].w; trans[i].h = rel_recs[i].h; trans[i].focal_length = rel_recs[i].focal_length;
		// Copying collocations
		/* Replaced with Get/SetCollocations()
		if(rel_recs[i].n_col>0) {
			trans[i].n_col = rel_recs[i].n_col;
			trans[i].col_b = new Coord2[trans[i].n_col];
			trans[i].col_d = new Coord2[trans[i].n_col];
			for(int j = 0; j<trans[i].n_col; j++) {
				trans[i].col_b[j].Copy(rel_recs[i].col_b[j]);
				trans[i].col_d[j].Copy(rel_recs[i].col_d[j]);
			}
		}
		*/
		pCol = rel_recs[i].GetCollocations();
		trans[i].SetCollocations(pCol);
		delete pCol;
		// What should happen here????
		//...
		memcpy(trans[i].PeakParams, rel_recs[i].PeakParams, 6*sizeof(double));
		// The following is needed only when old SVM model is used...
		//trans[i].PeakParams[0] = rel_recs[i].PeakParams[0];
		//trans[i].PeakParams[1] = rel_recs[i].PeakParams[1];
		//trans[i].PeakParams[2] = rel_recs[i].PeakParams[3];
		//trans[i].PeakParams[3] = rel_recs[i].PeakParams[4];
	}
	return(trans);
}

