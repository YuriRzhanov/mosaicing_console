#include "Transform.h"

TransformRecord* RRA2Trans(RARecord rel_recs[], int N_records, double focal_length, bool no_zoom = false);
int ComputeWorldTransforms(TransformRecord *rel_trans, int N_links,
	TransformRecord *world_trans, int N_max_frames,
	int anchor_frame_n, Hmgr& anchor_hm, double focal_length,
	char* anchor_mark = NULL, FILE* fp_debug = NULL); // From Utilities


int RRAToWorldTrans(RARecord rel_recs[], int N_records, Y_Rect<int> frame, double fl, 
	int anchor_frame_n, Hmgr& start_hm, bool no_zoom, bool verbose,
	TransformRecord** worldtrans, int N_max_frames)
{
	// Convert RARs to transforms
	TransformRecord* trans = RRA2Trans(rel_recs, N_records, fl);

	// World transform for the anchor frame is 'start_hm'
	int anchor_twice = 1;
	int N_frames = ComputeWorldTransforms(trans, N_records, *worldtrans, N_max_frames,
		anchor_frame_n, start_hm, fl, NULL, NULL);

	delete [] trans;
	return(N_frames);
}

